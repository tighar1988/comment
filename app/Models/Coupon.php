<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Coupon extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'max_usage',
        'starts_at',
        'expires_at',
        'coupon_type',
        'amount',
        'shopify_coupon_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'json',
    ];

    public function scopeSearch($query, $search = null)
    {
        if (! empty($search)) {
            $query->where('code', 'like', "%{$search}%");
        }

        return $query;
    }

    public function isTypeShipping()
    {
        return $this->coupon_type == 'S';
    }

    public function isTypeFixed()
    {
        return $this->coupon_type == 'F';
    }

    public function isTypePercentage()
    {
        return $this->coupon_type == 'P';
    }

    public function getShopifyType()
    {
        if ($this->isTypeShipping()) {
            return 'shipping';
        } elseif ($this->isTypeFixed()) {
            return 'fixed_amount';
        } elseif ($this->isTypePercentage()) {
            return 'percentage';
        }

        return null;
    }

    public static function getCoupon($code)
    {
        $code = strtoupper(trim($code));

        return static::where('code', $code)->first();
    }

    public function isActive()
    {
        if ($this->starts_at != 0 && $this->starts_at > time()) {
            return false;
        }

        if ($this->expires_at != 0 && $this->expires_at < time()) {
            return false;
        }

        if ($this->max_usage != 0 && $this->use_count >= $this->max_usage) {
            return false;
        }

        return true;
    }

    public function isValid($user, $subtotal)
    {
        // if it's an only once per customer, make sure it wasn't used
        if (isset($this->options['onlyOnce']) && $this->options['onlyOnce'] == '1') {
            $exists = app('App\Models\Order')->where('customer_id', '=', $user->id)
                ->where('coupon', '=', $this->code)->first();
            if (! is_null($exists)) {
                return false;
            }
        }

        // check if this is a promotional coupon that is valid for a limited time since the user signs up
        if (isset($this->options['newUsersPeriod']) && is_numeric($this->options['newUsersPeriod'])) {

            $validUntil = $user->created_at->addDays($this->options['newUsersPeriod']);
            $now = Carbon::now();

            if ($now->greaterThan($validUntil)) {
                return false;
            }
        }

        // check if the coupon can be used only by one user
        if (isset($this->options['userId']) && is_numeric($this->options['userId'])) {
            if ($this->options['userId'] != $user->id) {
                return false;
            }
        }

        // check subtotal is greater than minimum purchase amount
        if (isset($this->options['minPurchaseAmount']) && is_numeric($this->options['minPurchaseAmount']) && $this->options['minPurchaseAmount'] > 1) {
            if ($this->options['minPurchaseAmount'] > $subtotal) {
                return false;
            }
        }

        return $this->isActive();
    }

    public function saveCoupon(array $options = [])
    {
        $this->code = strtoupper(trim($this->code));

        if ($this->isTypeShipping()) {
            $this->amount = 0;
        }

        $this->starts_at = empty($this->starts_at) ? 0 : strtotime($this->starts_at);
        $this->expires_at = empty($this->expires_at) ? 0 : strtotime($this->expires_at);
        $this->max_usage = empty($this->max_usage) ? 0 : $this->max_usage;

        parent::save();
    }

    public function increaseUseCount()
    {
        if (is_null($this->use_count)) {
            $this->use_count = 0;
        }

        $this->use_count++;
        $this->save();
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'code', 'coupon');
    }
}
