<?php

namespace App\Facebook;

use App\Jobs\SendFacebookMessage;
use Facebook\Facebook as FacebookSDK;
use App\Facebook\MessengerTemplates;
use App\Jobs\SendMessengerText;

class FacebookMessenger
{
    /**
     * Facebook\Facebook
     *
     * @var object
     */
    protected $fb;

    /**
     * The Facebook Page access token.
     *
     * @var string
     */
    protected $pageAccessToken;

    public function __construct($pageAccessToken = null)
    {
        $this->fb = new FacebookSDK([
            'app_id' => fb_app_id(),
            'app_secret' => fb_app_secret(),
            'default_graph_version' => fb_api_version(),
            'http_client_handler' => new CurlHttpClient,
        ]);

        if (! is_null($pageAccessToken)) {
            $this->setPageAccessToken($pageAccessToken);
        }
    }

    public function setPageAccessToken($accessToken)
    {
        $this->pageAccessToken = $accessToken;
        $this->fb->setDefaultAccessToken($accessToken);
    }

    public function sendTextMessage($userRef, $messageText)
    {
        if (empty($userRef)) {
            return;
        }

        return $this->callSendAPI([
            'recipient' => [
                'user_ref' => $userRef,
            ],
            'message' => [
                'text' => $messageText,
            ],
        ]);
    }

    public function callSendAPI($message)
    {
        if (empty($this->pageAccessToken) || empty($message)) {
            return;
        }

        return $this->fb->post('/me/messages', $message);
    }

    public static function notifyForInvalidComment($customer, $post)
    {
        if (empty($customer->user_ref)) {
            return;
        }

        $template = (new MessengerTemplates)->invalidComment($customer, $post);
        dispatch(new SendMessengerText(shop_id(), [$template]));
    }

    public static function notifyItemWaitlisted($customer)
    {
        if (empty($customer->user_ref)) {
            return;
        }

        $template = (new MessengerTemplates)->itemWaitlisted($customer);
        dispatch(new SendMessengerText(shop_id(), [$template]));
    }

    public static function notifyWaitlistItemActivated($customer, $variant)
    {
        if (empty($customer->user_ref)) {
            return;
        }

        $templates = new MessengerTemplates;
        $products = $templates->waitlistActivated($customer, $variant);
        $button = $templates->waitlistActivatedButton($customer);
        dispatch(new SendMessengerText(shop_id(), [$products, $button]));
    }

    public static function notifyNoEmail($customer)
    {
        if (empty($customer->user_ref)) {
            return;
        }

        $template = (new MessengerTemplates)->noEmail($customer);
        dispatch(new SendMessengerText(shop_id(), [$template]));
    }

    public static function notifyGotOrder($customer)
    {
        if (empty($customer->user_ref)) {
            return;
        }

        $templates = new MessengerTemplates;
        $products = $templates->gotOrder($customer);
        $button = $templates->payOrderButton($customer);
        dispatch(new SendMessengerText(shop_id(), [$products, $button]));
    }

    public static function notifyPaymentReceipt($customer, $order)
    {
        if (empty($customer->user_ref)) {
            return;
        }

        $templates = new MessengerTemplates;
        $products = $templates->paymentReceipt($customer, $order);
        $text = $templates->paymentReceiptText($customer);
        dispatch(new SendMessengerText(shop_id(), [$products, $text]));
    }

    public static function notifyOrderFulfilled($order)
    {
        $customer = $order->customer;
        if (empty($customer->user_ref)) {
            return;
        }

        $template = (new MessengerTemplates)->orderFulfilled($customer, $order);
        $job = new SendMessengerText(shop_id(), [$template]);

        if ($order->local_pickup && shop_id() == 'julesandjamesboutique') {
            $job->delay(12*60*60); // 12 hours
        }

        dispatch($job);
    }
}
