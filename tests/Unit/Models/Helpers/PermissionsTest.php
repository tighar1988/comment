<?php

namespace Tests\Unit\Models\Helpers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Helpers\Permissions;
use Mockery;

class PermissionsTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $user = Mockery::mock('App\Models\User');
        $permissions = new Permissions($user);

        $this->assertInstanceOf(Permissions::class, $permissions);
    }

    /** @test */
    public function it_lists_all_possible_permissions()
    {
        $permissions = Permissions::list();
        $this->assertTrue(is_array($permissions));
        $this->assertTrue(count($permissions) > 0);
    }

    /** @test */
    public function it_lists_all_possible_permissions_keys()
    {
        $keys = Permissions::permissionKeys();
        $this->assertTrue(is_array($keys));
        $this->assertTrue(count($keys) > 0);
    }

    /** @test */
    public function it_checks_user_has_permission_when_superadmin()
    {
        $user = Mockery::mock('App\Models\User');
        $user->shouldReceive('isSuperAdmin')->once()->andReturn(true);
        $permissions = new Permissions($user);

        $this->assertTrue($permissions->has('write-post'));
    }

    /** @test */
    public function when_not_useradmin_and_has_all_permissions()
    {
        $user = Mockery::mock('App\Models\User');
        $user->shouldReceive('isSuperAdmin')->once()->andReturn(false);
        $user->shouldReceive('getAttribute')->with('permissions')->andReturn(['all-permissions' => true]);
        $permissions = new Permissions($user);

        $this->assertTrue($permissions->has('write-post'));
    }

    /** @test */
    public function when_not_useradmin_and_does_not_have_all_permissions()
    {
        $user = Mockery::mock('App\Models\User');
        $user->shouldReceive('isSuperAdmin')->once()->andReturn(false);
        $user->shouldReceive('getAttribute')->with('permissions')->andReturn(['all-permissions' => false]);
        $permissions = new Permissions($user);

        $this->assertFalse($permissions->has('write-post'));
    }

    /** @test */
    public function when_not_useradmin_and_has_specific_permission()
    {
        $user = Mockery::mock('App\Models\User');
        $user->shouldReceive('isSuperAdmin')->once()->andReturn(false);
        $user->shouldReceive('getAttribute')->with('permissions')->andReturn(['write-post' => true]);
        $permissions = new Permissions($user);

        $this->assertTrue($permissions->has('write-post'));
    }

    /** @test */
    public function when_not_useradmin_and_does_not_have_specific_permission()
    {
        $user = Mockery::mock('App\Models\User');
        $user->shouldReceive('isSuperAdmin')->once()->andReturn(false);
        $user->shouldReceive('getAttribute')->with('permissions')->andReturn(['write-post' => false]);
        $permissions = new Permissions($user);

        $this->assertFalse($permissions->has('write-post'));
    }
}
