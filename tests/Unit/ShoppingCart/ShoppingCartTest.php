<?php

namespace Tests\Unit\ShoppingCart;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Checkout\Checkout;
use App\Models\ShopUser;
use App\Models\Coupon;
use Mockery;
use App\ShoppingCart\ShoppingCart;
use App\ShoppingCart\CartItem;
use DB;

class ShoppingCartTest extends TestCase
{
    /**
     * Helper method to build a CartItem
     */
    public static function buildCartItem($values = [])
    {
        $cartItem = new CartItem(null);
        $cartItem->id                   = $values['id'] ?? 10;
        $cartItem->name                 = $values['name'] ?? 'Lavender Shirt';
        $cartItem->price                = $values['price'] ?? 22;
        $cartItem->cost                 = $values['cost'] ?? 10;
        $cartItem->image                = $values['image'] ?? 'https://example.com/image.jpg';
        $cartItem->color                = $values['color'] ?? 'blue';
        $cartItem->size                 = $values['size'] ?? 'medium';
        $cartItem->created_at           = $values['created_at'] ?? 1500647192;
        $cartItem->product_id           = $values['product_id'] ?? 30;
        $cartItem->inventory_id         = $values['inventory_id'] ?? 90;
        $cartItem->comment_id           = $values['comment_id'] ?? 1224;
        $cartItem->order_source         = $values['order_source'] ?? 1;
        $cartItem->product_style        = $values['product_style'] ?? 'Style';
        $cartItem->product_brand        = $values['product_brand'] ?? 'Brand';
        $cartItem->product_brand_style  = $values['product_brand_style'] ?? 'Brand Style';
        $cartItem->product_description  = $values['product_description'] ?? 'Product Description';
        $cartItem->weight               = $values['weight'] ?? 7;
        $cartItem->shopify_inventory_id = $values['shopify_inventory_id'] ?? null;
        $cartItem->charge_taxes         = $values['charge_taxes'] ?? true;

        return $cartItem;
    }

    /** @test */
    public function it_runs()
    {
        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([]);
        $customer->shouldReceive('getOption')->andReturn(null);

        $couponModel = Mockery::mock('App\Models\Coupon');

        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertInstanceOf(ShoppingCart::class, $shoppingCart);

        $this->assertEquals(0, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(10, $shoppingCart->total);

        $this->assertEquals(0, $shoppingCart->count());
        $this->assertEquals(0, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(1000, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_one_item_in_the_cart()
    {
        $cartItem = static::buildCartItem(['price' => 22, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $couponModel = Mockery::mock('App\Models\Coupon');

        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(22, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(32, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(22, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3200, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_fixed_coupon()
    {
        $cartItem = static::buildCartItem(['price' => 25, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 25)->andReturn(true);
        $coupon->shouldReceive('isTypeShipping')->andReturn(false);
        $coupon->shouldReceive('isTypeFixed')->andReturn(true);
        $coupon->shouldReceive('getAttribute')->with('code')->andReturn('FIXED_AMOUNT_15');
        $coupon->shouldReceive('getAttribute')->with('amount')->andReturn(15);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);


        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(25, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals('FIXED_AMOUNT_15', $shoppingCart->coupon_code);
        $this->assertEquals(15, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(20, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(10, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(2000, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_percentage_coupon()
    {
        $cartItem = static::buildCartItem(['price' => 33, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 33)->andReturn(true);
        $coupon->shouldReceive('isTypeShipping')->andReturn(false);
        $coupon->shouldReceive('isTypeFixed')->andReturn(false);
        $coupon->shouldReceive('isTypePercentage')->andReturn(true);
        $coupon->shouldReceive('getAttribute')->with('code')->andReturn('PERCENTAGE_25');
        $coupon->shouldReceive('getAttribute')->with('amount')->andReturn(25);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);


        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(33, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals('PERCENTAGE_25', $shoppingCart->coupon_code);
        $this->assertEquals(8.25, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(34.75, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(24.75, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3475, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_shipping_coupon()
    {
        $cartItem = static::buildCartItem(['price' => 33, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 33)->andReturn(true);
        $coupon->shouldReceive('isTypeShipping')->andReturn(true);
        $coupon->shouldReceive('isTypeFixed')->andReturn(false);
        $coupon->shouldReceive('isTypePercentage')->andReturn(false);
        $coupon->shouldReceive('getAttribute')->with('code')->andReturn('SHIPPING_COUPON');

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);


        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(33, $shoppingCart->subtotal);
        $this->assertEquals(0, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals('SHIPPING_COUPON', $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(33, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(33, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3300, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_fixed_coupon_greater_than_total()
    {
        $cartItem = static::buildCartItem(['price' => 23, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 23)->andReturn(true);
        $coupon->shouldReceive('isTypeShipping')->andReturn(false);
        $coupon->shouldReceive('isTypeFixed')->andReturn(true);
        $coupon->shouldReceive('isTypePercentage')->andReturn(false);
        $coupon->shouldReceive('getAttribute')->with('code')->andReturn('AMOUNT_50');
        $coupon->shouldReceive('getAttribute')->with('amount')->andReturn(50);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);


        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(23, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals('AMOUNT_50', $shoppingCart->coupon_code);
        $this->assertEquals(50, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(0, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(0, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(0, $shoppingCart->totalInCents());
        $this->assertEquals(17, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_invalid_coupon()
    {
        $cartItem = static::buildCartItem(['price' => 21, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('setOption');
        $customer->shouldReceive('save');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 21)->andReturn(false);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);


        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(21, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(31, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(21, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3100, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_apply_balance_with_0_balance()
    {
        $cartItem = static::buildCartItem(['price' => 25, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(true);
        $customer->shouldReceive('getAttribute')->with('balance')->andReturn(0);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $couponModel = Mockery::mock('App\Models\Coupon');

        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(25, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(35, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(25, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3500, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_apply_balance_paid_fully_with_balance()
    {
        $cartItem = static::buildCartItem(['price' => 20.95, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => true,
            'shop.shipping-cost'          => null,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(true);
        $customer->shouldReceive('getAttribute')->with('balance')->andReturn(50);
        $customer->shouldReceive('getAttribute')->with('local_pickup')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $couponModel = Mockery::mock('App\Models\Coupon');

        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(20.95, $shoppingCart->subtotal);
        $this->assertEquals(10, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(50, $shoppingCart->apply_balance);
        $this->assertEquals(30.95, $shoppingCart->deduced_balance);
        $this->assertEquals(true, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(0, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(0, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(0, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_apply_balance_paid_partially_with_balance()
    {
        $cartItem = static::buildCartItem(['price' => 18.85, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => true,
            'shop.shipping-cost'          => 10,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(true);
        $customer->shouldReceive('getAttribute')->with('balance')->andReturn(12);
        $customer->shouldReceive('getAttribute')->with('local_pickup')->andReturn(true);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $couponModel = Mockery::mock('App\Models\Coupon');

        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(18.85, $shoppingCart->subtotal);
        $this->assertEquals(0, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(12, $shoppingCart->apply_balance);
        $this->assertEquals(12, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(6.85, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(6.85, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(685, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_taxes()
    {
        $cartItem = static::buildCartItem(['price' => 21, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '333444',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.030000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.070000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.035000);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('333444')->andReturn($taxRate);


        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(21, $shoppingCart->subtotal);
        $this->assertEquals(4, $shoppingCart->shipping_price);
        $this->assertEquals(0.63, $shoppingCart->state_tax);
        $this->assertEquals(1.47, $shoppingCart->county_tax);
        $this->assertEquals(0.74, $shoppingCart->municipal_tax);
        $this->assertEquals(2.84, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(27.84, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(21, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(2784, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_two_cart_items()
    {
        $cartItem1 = static::buildCartItem(['price' => 21, 'charge_taxes' => false]);
        $cartItem2 = static::buildCartItem(['price' => 17, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => '1',
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '333444',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem1,
            $cartItem2,
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.010000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.050000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.035000);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('333444')->andReturn($taxRate);


        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(38, $shoppingCart->subtotal);
        $this->assertEquals(5, $shoppingCart->shipping_price);
        $this->assertEquals(0.34, $shoppingCart->state_tax);
        $this->assertEquals(0.85, $shoppingCart->county_tax);
        $this->assertEquals(0.60, $shoppingCart->municipal_tax);
        $this->assertEquals(1.79, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(44.79, $shoppingCart->total);

        $this->assertEquals(2, $shoppingCart->count());
        $this->assertEquals(38, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(4479, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_ordered_in_the_last_24_hours()
    {
        $cartItem = static::buildCartItem(['price' => 25, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => 12,
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => true,
            'shop.state'                  => null,
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('orderedInLast24hours')->andReturn(true);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $couponModel = Mockery::mock('App\Models\Coupon');

        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, Mockery::mock('App\Models\TaxRate'));

        $this->assertEquals(25, $shoppingCart->subtotal);
        $this->assertEquals(0, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(25, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(25, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(2500, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_taxes_applied_on_the_discounted_subtotal()
    {
        $cartItem1 = static::buildCartItem(['price' => 21, 'charge_taxes' => true]);
        $cartItem2 = static::buildCartItem(['price' => 22, 'charge_taxes' => false]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '333444',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem1, $cartItem2,
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.030000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.070000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.035000);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 43)->andReturn(true);
        $coupon->shouldReceive('isTypeShipping')->andReturn(false);
        $coupon->shouldReceive('isTypeFixed')->andReturn(true);
        $coupon->shouldReceive('getAttribute')->with('code')->andReturn('FIXED_AMOUNT_10');
        $coupon->shouldReceive('getAttribute')->with('amount')->andReturn(10);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);

        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('333444')->andReturn($taxRate);


        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(43, $shoppingCart->subtotal);
        $this->assertEquals(4, $shoppingCart->shipping_price);
        $this->assertEquals(0.45, $shoppingCart->state_tax);
        $this->assertEquals(1.05, $shoppingCart->county_tax);
        $this->assertEquals(0.53, $shoppingCart->municipal_tax);
        $this->assertEquals(2.03, $shoppingCart->tax_total);
        $this->assertEquals('FIXED_AMOUNT_10', $shoppingCart->coupon_code);
        $this->assertEquals(10, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(39.03, $shoppingCart->total);
        $this->assertEquals(2, $shoppingCart->count());
        $this->assertEquals(33, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3903, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_four_items()
    {
        $cartItem1 = static::buildCartItem(['price' => 25, 'charge_taxes' => true]);
        $cartItem2 = static::buildCartItem(['price' => 26, 'charge_taxes' => true]);
        $cartItem3 = static::buildCartItem(['price' => 27, 'charge_taxes' => true]);
        $cartItem4 = static::buildCartItem(['price' => 28, 'charge_taxes' => false]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '333444',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem1, $cartItem2, $cartItem3, $cartItem4
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.025000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.050000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.033000);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 106)->andReturn(true);
        $coupon->shouldReceive('isTypeShipping')->andReturn(false);
        $coupon->shouldReceive('isTypeFixed')->andReturn(true);
        $coupon->shouldReceive('getAttribute')->with('code')->andReturn('FIXED_AMOUNT_20');
        $coupon->shouldReceive('getAttribute')->with('amount')->andReturn(20);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);

        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('333444')->andReturn($taxRate);


        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(106, $shoppingCart->subtotal);
        $this->assertEquals(0, $shoppingCart->shipping_price);
        $this->assertEquals(1.45, $shoppingCart->state_tax);
        $this->assertEquals(2.90, $shoppingCart->county_tax);
        $this->assertEquals(1.91, $shoppingCart->municipal_tax);
        $this->assertEquals(6.26, $shoppingCart->tax_total);
        $this->assertEquals('FIXED_AMOUNT_20', $shoppingCart->coupon_code);
        $this->assertEquals(20, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(92.26, $shoppingCart->total);
        $this->assertEquals(4, $shoppingCart->count());
        $this->assertEquals(86, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(9226, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_taxes_for_special_shop()
    {
        set_shop_database('somethingheliotrope');

        $cartItem = static::buildCartItem(['price' => 33, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => null,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '444333',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.030000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.070000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.035000);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('444333')->andReturn($taxRate);


        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(33, $shoppingCart->subtotal);
        $this->assertEquals(4, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(2.31, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(2.31, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(39.31, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(33, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3931, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_no_free_shipping_maximum()
    {
        $cartItem = static::buildCartItem(['price' => 24, 'charge_taxes' => false]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => 0,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '444333',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.030000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.070000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.035000);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('444333')->andReturn($taxRate);

        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(24, $shoppingCart->subtotal);
        $this->assertEquals(4, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(28, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(24, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(2800, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_no_free_shipping_maximum_20()
    {
        $cartItem = static::buildCartItem(['price' => 25, 'charge_taxes' => false]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => 20,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '444333',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.030000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.070000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.035000);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('444333')->andReturn($taxRate);

        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(25, $shoppingCart->subtotal);
        $this->assertEquals(0, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(25, $shoppingCart->total);

        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(25, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(2500, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_with_no_free_shipping_maximum_after_coupon()
    {
        $cartItem1 = static::buildCartItem(['price' => 55, 'charge_taxes' => false]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => 50,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => null,
            'shop.state'                  => 'CA',
            'shop.zip_code'               => '333444',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('CA');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem1,
        ]);

        $taxRate = Mockery::mock('App\Models\TaxRate');
        $taxRate->shouldReceive('getAttribute')->with('estimated_special_rate')->andReturn(0.025000);
        $taxRate->shouldReceive('getAttribute')->with('state_rate')->andReturn(0.020000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_county_rate')->andReturn(0.050000);
        $taxRate->shouldReceive('getAttribute')->with('estimated_city_rate')->andReturn(0.033000);

        ShoppingCart::applyCoupon(10);

        $coupon = Mockery::mock('App\Models\Coupon');
        $coupon->shouldReceive('isValid')->with($customer, 55)->andReturn(true);
        $coupon->shouldReceive('isTypeShipping')->andReturn(false);
        $coupon->shouldReceive('isTypeFixed')->andReturn(true);
        $coupon->shouldReceive('getAttribute')->with('code')->andReturn('FIXED_AMOUNT_10');
        $coupon->shouldReceive('getAttribute')->with('amount')->andReturn(10);

        $couponModel = Mockery::mock('App\Models\Coupon');
        $couponModel->shouldReceive('find')->with(10)->andReturn($coupon);

        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('333444')->andReturn($taxRate);


        ShoppingCart::applyBalance();
        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(55, $shoppingCart->subtotal);
        $this->assertEquals(4, $shoppingCart->shipping_price);
        $this->assertEquals(0, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(0, $shoppingCart->tax_total);
        $this->assertEquals('FIXED_AMOUNT_10', $shoppingCart->coupon_code);
        $this->assertEquals(10, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(49, $shoppingCart->total);
        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(45, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(4900, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_taxes_for_pinklinoshop()
    {
        set_shop_database('pinklionboutique');

        $cartItem1 = static::buildCartItem(['price' => 25, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => 75,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => false,
            'shop.state'                  => 'TX',
            'shop.zip_code'               => '444333',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('TX');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem1,
        ]);

        $couponModel = Mockery::mock('App\Models\Coupon');

        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('444333')->andReturn(null);

        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(25, $shoppingCart->subtotal);
        $this->assertEquals(4, $shoppingCart->shipping_price);
        $this->assertEquals(2.39, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(2.39, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(31.39, $shoppingCart->total);
        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(25, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(3139, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }

    /** @test */
    public function it_calculates_taxes_for_KY_state()
    {
        $cartItem1 = static::buildCartItem(['price' => 22, 'charge_taxes' => true]);

        $this->mockShopSettings([
            'multiple-locations-enabled'  => false,
            'shop.free-shipping-maximum'  => 75,
            'local-pickup-enabled'        => null,
            'shop.shipping-cost'          => '4',
            'shop.variable-shipping-cost' => null,
            '24hr-free-shipping-enabled'  => false,
            'shop.state'                  => 'KY',
            'shop.zip_code'               => '444333',
        ]);

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getOption')->andReturn(null);
        $customer->shouldReceive('getAttribute')->with('state')->andReturn('KY');
        $customer->shouldReceive('hasBalance')->andReturn(false);
        $customer->shouldReceive('cartItems')->andReturn([
            $cartItem1,
        ]);

        $couponModel = Mockery::mock('App\Models\Coupon');

        $taxRateModel = Mockery::mock('App\Models\TaxRate');
        $taxRateModel->shouldReceive('getMasterTaxRate')->with('444333')->andReturn(null);

        $shoppingCart = new ShoppingCart($customer, $couponModel, $taxRateModel);

        $this->assertEquals(22, $shoppingCart->subtotal);
        $this->assertEquals(4, $shoppingCart->shipping_price);
        $this->assertEquals(1.32, $shoppingCart->state_tax);
        $this->assertEquals(0, $shoppingCart->county_tax);
        $this->assertEquals(0, $shoppingCart->municipal_tax);
        $this->assertEquals(1.32, $shoppingCart->tax_total);
        $this->assertEquals(null, $shoppingCart->coupon_code);
        $this->assertEquals(0, $shoppingCart->coupon_discount);
        $this->assertEquals(0, $shoppingCart->apply_balance);
        $this->assertEquals(0, $shoppingCart->deduced_balance);
        $this->assertEquals(false, $shoppingCart->isFullyPaidWithBalance);
        $this->assertEquals(27.32, $shoppingCart->total);
        $this->assertEquals(1, $shoppingCart->count());
        $this->assertEquals(22, $shoppingCart->totalWithoutShippingAndTax());
        $this->assertEquals(2732, $shoppingCart->totalInCents());
        $this->assertEquals(0, $shoppingCart->unusedCouponAmount());
    }
}
