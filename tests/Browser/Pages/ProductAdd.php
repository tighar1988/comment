<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class ProductAdd extends BasePage
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/products/add';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@style'        => 'style',
            '@product_name' => 'product_name',
            '@brand'        => 'brand',
            '@brand_style'  => 'brand_style',
            '@size'         => 'size',
            '@color'        => 'color',
            '@cost'         => 'cost',
            '@retail_price' => 'retail_price',
            '@description'  => 'description',
        ];
    }

    public function addProduct(Browser $browser, $options = [])
    {
        $id = $options['id'] ?? time();

        $browser->maximize()
                ->type('style', $options['style'] ?? "SKU{$id}")
                ->type('product_name', $options['product_name'] ?? "Black Shirt {$id}")
                ->type('brand', $options['brand'] ?? "Brand {$id}")
                ->type('brand_style', $options['brand_style'] ?? "Brand Style {$id}")
                // ->select('size', $options['size'] ?? 'size')
                // ->select('color', $options['color'] ?? 'color')
                ->type('cost', $options['cost'] ?? 10)
                ->type('retail_price', $options['retail_price'] ?? 20)
                ->type('description', $options['description'] ?? 'A very cool shirt.')
                ->press('Add Product');
    }
}
