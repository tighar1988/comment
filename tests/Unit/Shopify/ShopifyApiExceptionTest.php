<?php

namespace Tests\Unit\Shopify;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Shopify\ShopifyApiException;

class ShopifyApiExceptionTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $headers = ['http_status_message' => 'Unauthorized', 'http_status_code' => 401];
        $response = null;
        $params = ['q' => true];

        $exception = new ShopifyApiException('GET', '/shopify', $params, $headers, $response);

        $this->assertInstanceOf(ShopifyApiException::class, $exception);
        $this->assertEquals('GET', $exception->getMethod());
        $this->assertEquals('/shopify', $exception->getPath());
        $this->assertEquals($params, $exception->getParams());
        $this->assertEquals($headers, $exception->getResponseHeaders());
        $this->assertEquals($response, $exception->getResponse());
    }
}
