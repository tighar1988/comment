<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\DatabaseMigrateMaster;
use Mockery;

class DatabaseMigrateMasterTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(DatabaseMigrateMaster::class, new DatabaseMigrateMaster);
    }
}
