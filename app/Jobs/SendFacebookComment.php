<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Traits\ReconnectShopDB;
use App\Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Log;

class SendFacebookComment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    protected $shop;

    protected $parentId;

    protected $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop, $parentId, $message)
    {
        $this->shop     = $shop;
        $this->parentId = $parentId;
        $this->message  = $message;
    }

    /**
     * Send facebook comment
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->reconnectDB($this->shop);

            $pageToken = shop_setting('facebook.page.page_access_token');
            if (empty($pageToken)) {
                return;
            }

            $fb = (new Facebook)->getClient();
            $fb->setDefaultAccessToken($pageToken);
            $fb->post("/{$this->parentId}/comments", ['message' => $this->message]);

        } catch (FacebookResponseException $e) {
            // if the comment was deleted we can't reply to it, so we don't need to log the error
            if (! str_contains($e->getMessage(), 'Unsupported post request.')) {
                throw $e;
            }
        }
    }
}
