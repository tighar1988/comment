<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class GiftCard extends Drop
{
    // gift_card.balance
    // gift_card.code
    // gift_card.currency
    // gift_card.customer
    // gift_card.enabled
    // gift_card.expired
    // gift_card.expires_on
    // gift_card.initial_value
    // gift_card.properties
    // gift_card.url
}
