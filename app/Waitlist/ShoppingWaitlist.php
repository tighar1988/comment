<?php

namespace App\Waitlist;

use App\Waitlist\WaitlistItem;
use App\Models\ShopUser;
use App\Models\Waitlist;

class ShoppingWaitlist
{
    /**
     * The cart items.
     *
     * @var array
     */
    public $items;

    public function __construct(ShopUser $customer)
    {
        $items = Waitlist::forCustomer($customer->id)->with('variant.product.image')->get();

        $this->items = array_map(function($item) {
            return new WaitlistItem($item);
        }, $items->all());
    }

    public static function removeItem($itemId)
    {
        Waitlist::destroy($itemId);
    }
}
