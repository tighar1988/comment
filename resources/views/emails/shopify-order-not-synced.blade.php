<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><br/>

    This order could not be synced to Shopify because it contains a CommentSold product that does not exist in Shopify.
    <br/>
    You will need to fulfill this order directly from CommentSold.
    <br/><br/>

    <a href="{{ url("/admin/orders?order={$orderId}") }}">View Order</a>
    <br/>
</div>
