@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Facebook Setup</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>@yield('section')</li>
    </ol>
</section>
<section class="content settings-boxes">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-pills">
                    <li class="{{ activeLink('admin/facebook-setup') }}"><a href="/admin/facebook-setup">Groups</a></li>
                    <li class="{{ activeLink('admin/facebook-setup/pages') }}"><a href="/admin/facebook-setup/pages">Pages</a></li>
                    <li class="{{ activeLink('admin/facebook-setup/messenger') }}"><a href="/admin/facebook-setup/messenger">Messenger</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <?php $facebookId = shop_setting('facebook.user_id'); ?>

                    @if (! empty($facebookId))
                        <?php $facebookName = shop_setting('facebook.name'); ?>
                        <input type="hidden" id="facebook-userid" value="{{ $facebookId }}">
                        <input type="hidden" id="facebook-name" value="{{ $facebookName }}">

                        @if ($facebookName)
                            You are connected as <a target="_blank" href="https://facebook.com/{{ $facebookId }}">{{ $facebookName }}</a>.
                        @else
                            You are connected with this <a target="_blank" href="https://facebook.com/{{ $facebookId }}">facebook account</a>.
                        @endif

                        <br/><a class="btn btn-info btn-sm" onclick="return facebookConnect(event);"><i class="fa fa-fw fa-facebook"></i> Re-Connect Facebook</a>
                    @else
                        Press this button to connect your Facebook Account.<br/>
                        We will ask permission to manage your groups so that we can post products for you
                        <br/><br/>

                        <a class="btn btn-info btn-md" onclick="return facebookConnect(event);"><i class="fa fa-fw fa-facebook"></i> Connect Facebook</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if (! empty($facebookId))
        @yield('setup')
    @endif

</section>
@endsection

@section('scripts')
<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({
            appId: '{!! fb_app_id() !!}',
            xfbml: true,
            version: '{{ fb_api_version() }}'
        });
    };

    (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function facebookConnect(e) {
        e.preventDefault();
        FB.login(function(response) {
            if (response.authResponse.userID && response.authResponse.accessToken && response.authResponse.grantedScopes) {

                var s = response.authResponse.grantedScopes;
                if (s.indexOf('public_profile') == -1 || s.indexOf('email') == -1 || s.indexOf('user_managed_groups') == -1 || s.indexOf('publish_actions') == -1 || s.indexOf('manage_pages') == -1 || s.indexOf('pages_messaging') == -1 || s.indexOf('read_page_mailboxes') == -1 || s.indexOf('pages_messaging_subscriptions') == -1 || s.indexOf('publish_pages') == -1) {
                    alert('In order to connect your Facebook account you need to accept all the permissions.');
                    return;
                }

                var userId = response.authResponse.userID;
                var currentId = $('#facebook-userid').val();
                var currentName = $('#facebook-name').val();

                // it's a different facebook account
                if (currentId && currentId != userId) {
                    var message = 'You are trying to connect with a different account than '+currentName+'. Are you sure?';
                    if (! confirm(message)) {
                        return;
                    }
                }

                $.ajax({
                    type:'post',
                    url:'/admin/api/facebook-setup/connect',
                    data: {
                        user_id: userId,
                        access_token: response.authResponse.accessToken
                    },
                    success: function(message) {
                        if (message.status == 1) {
                            alert('Facebook connected');
                            location.reload();
                        } else {
                            alert('An unexpected error occured.');
                        }
                    }
                });
            }
        },
        {
            @if (shop_id() == 'bktest' || shop_id() == 'shop1' || shop_id() == 'my_shop')
                scope: 'public_profile, email, publish_actions, user_managed_groups, manage_pages, pages_messaging, read_page_mailboxes, pages_messaging_subscriptions, publish_pages',
            @else
                scope: 'public_profile, email, publish_actions, user_managed_groups, manage_pages, pages_messaging, read_page_mailboxes, pages_messaging_subscriptions, publish_pages',
            @endif
            return_scopes: true
        });
    }
</script>
@endsection
