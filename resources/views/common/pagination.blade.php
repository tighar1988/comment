<div class="row">
    <div class="col-sm-5">
        <div class="dataTables_info" role="status" aria-live="polite">
            Showing {{ $paginator->firstItem() ?? 0 }} to {{ $paginator->lastItem() ?? 0 }} of {{ $paginator->total() }} entries
        </div>
    </div>
    <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers">
            <ul class="pagination pagination-sm">

                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <li class="paginate_button previous disabled"><a href="#">Previous</a></li>
                @else
                    <li class="paginate_button previous"><a href="{{ $paginator->previousPageUrl() }}">Previous</a></li>
                @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="paginate_button disabled"><a href="#">{{ $element }}</a></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="paginate_button active"><a href="#">{{ $page }}</a></li>
                            @elseif ($page != 0)
                                <li class="paginate_button"><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="paginate_button next"><a href="{{ $paginator->nextPageUrl() }}">Next</a></li>
                @else
                    <li class="paginate_button next disabled"><a href="#">Next</a></li>
                @endif

            </ul>
        </div>
    </div>
</div>
