<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'seo_title',
        'seo_description',
        'url',
    ];

    public function scopeHandle($query, $handle)
    {
        $handle = trim($handle, '/ ');

        return $query->where('url', '=', $handle);
    }

    public function scopeUrl($query, $url)
    {
        $url = trim($url, '/ ');

        return $query->where('url', '=', $url);
    }

    public function getUrl()
    {
        return '/pages/' . trim($this->url, ' /');
    }
}
