<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class Orders extends Drop
{
    protected $orders;

    public function __construct($orders)
    {
        $this->orders = $orders;
    }

    public function item_count()
    {
        return count($this->orders);
    }

    public function items()
    {
        $items = [];

        foreach ($this->orders as $order) {
            $items[] = new Order($order);
        }

        return $items;
    }
}
