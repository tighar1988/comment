<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateShopFormRequest;
use App\Http\Requests\HearAboutUsFormRequest;
use App\Http\Requests\BillingPlanFormRequest;
use Artisan;

class CreateShopController extends Controller
{
    public function index()
    {
        $user = request()->user();
        if (empty($user->hear_about_us)) {
            $hideSteps = true;
            return view('admin.initial-setup.hear-about-us', compact('hideSteps'));

        } elseif (empty($user->billing_plan) || $user->billing_plan == 'boutique_hub') {
            $hideSteps = true;
            $billingPlan = $user->billing_plan;
            return view('admin.initial-setup.billing-plan', compact('hideSteps', 'billingPlan'));

        } else {
            return view('admin.initial-setup.create-shop');
        }
    }

    public function setHearAboutUs(HearAboutUsFormRequest $request)
    {
        $user = $request->user();
        if ($request->input('hear_about_us') == 'other') {
            $user->hear_about_us = 'other:' . $request->input('other_source');
        } elseif ($request->input('hear_about_us') == 'from-a-friend') {
            $user->hear_about_us = 'friend:' . $request->input('other_source');
        } else {
            $user->hear_about_us = $request->input('hear_about_us');
        }

        $user->save();
        return back();
    }

    public function setBillingPlan(BillingPlanFormRequest $request)
    {
        $user = $request->user();
        $user->billing_plan = $request->input('billing_plan');
        $user->save();
        return back();
    }

    public function newShop(CreateShopFormRequest $request)
    {
        $shopName = strtolower($request->input('shop_name'));

        Artisan::call('database:create', ['name' => $shopName]);
        Artisan::call('database:migrate-shop', ['shop' => $shopName]);

        $user = $request->user();
        $user->shop_name = $shopName;
        $user->save();

        shop_setting_set('shop.fees-billing-plan', $user->billing_plan);

        // todo: make this a post request not an ajax request
        return response()->json(['status' => 1]);
    }

    public function card()
    {
        return view('admin.initial-setup.card-setup');
    }

    public function setCard()
    {
        $user = request()->user();
        $user->createAsStripeCustomer(request('stripeToken'));

        flash()->overlay('Awesome Job! Your are now a Commentsold member!', 'Shop Created');
        return redirect('/admin');
    }
}
