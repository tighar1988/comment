<?php

/**
 * This file is part of the Liquid package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Liquid
 */

namespace App\OnlineStore\Vendor\Liquid\Tag;

use App\OnlineStore\Vendor\Liquid\AbstractBlock;
use App\OnlineStore\Vendor\Liquid\Context;
use App\OnlineStore\Vendor\Liquid\Liquid;
use App\OnlineStore\Vendor\Liquid\Regexp;

/**
 * Creates a comment; everything inside will be ignored
 *
 * Example:
 *
 *     {% comment %} This will be ignored {% endcomment %}
 */
class TagComment extends AbstractBlock
{
	/**
	 * Parses the given tokens
	 *
	 * @param array $tokens
	 *
	 * @throws \Liquid\LiquidException
	 * @return void
	 */
	public function parse(array &$tokens) {
		if (! is_array($tokens)) {
			return parent::parse($tokens);
		}

		$startRegexp = new Regexp('/^' . Liquid::get('TAG_START') . '/');
		$tagRegexp = new Regexp('/^' . Liquid::get('TAG_START') . '\s*(\w+)\s*(.*)?' . Liquid::get('TAG_END') . '$/');

		$nrCommentTags = 1;
		$nrEndcommentTags = 0;
		$skipTags = true;
		while (count($tokens) && $skipTags) {
			$token = array_shift($tokens);

			// skip everything until the first 'endcomment' tag
			if ($startRegexp->match($token)) {
				if ($tagRegexp->match($token)) {
					if ($tagRegexp->matches[1] == 'comment') {
						$nrCommentTags++;
					}

					$tagName = strtolower($tagRegexp->matches[1]);

					if ($tagName == 'endcomment') {
						$nrEndcommentTags++;

						if ($nrCommentTags > 1) {
							$nrCommentTags--;
							$nrEndcommentTags--;
						}

						if ($nrCommentTags == 0 || $nrCommentTags == $nrEndcommentTags) {
							$skipTags = false;
						}
					}
				}
			}
		}
	}
}
