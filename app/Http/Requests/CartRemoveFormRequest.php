<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Cart;

class CartRemoveFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (starts_with($this->item_id, 'session')) {
            return true;
        }

        // make sure the user can only delete items from his own cart
        $item = Cart::find($this->item_id);

        return $item && $this->user()->id == $item->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'item_id' => 'required|numeric',
        ];

        if (starts_with($this->item_id, 'session')) {
            $rules['item_id'] = 'required';
        }

        return $rules;
    }
}
