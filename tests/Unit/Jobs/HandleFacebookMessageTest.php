<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Jobs\HandleFacebookMessage;

class HandleFacebookMessageTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(HandleFacebookMessage::class, new HandleFacebookMessage([]));
    }
}
