<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Helpers\Permissions;
use Laravel\Cashier\Billable;
use App\Models\StripeCharges;
use App\Mails\ShopMailer;
use Exception;
use DB;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'master';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'permissions', 'register_link', 'billing_plan', 'trial_started_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'permissions' => 'json',
    ];

    public static function getByShopName($shop)
    {
        $shop = str_replace('_', '-', $shop);

        return static::where('shop_name', $shop)->where('superadmin', '=', true)->first();
    }

    public function scopeList($query)
    {
        return $query->orderBy('id', 'asc');
    }

    public function getTeam()
    {
        return static::orderBy('id', 'asc')->where('superadmin', '=', false)->where('shop_name', '=', $this->shop_name)->get();
    }

    public function isSuperAdmin()
    {
        return $this->superadmin == true;
    }

    public function permissions()
    {
        return new Permissions($this);
    }

    public function addPrepaidCredit($amount)
    {
        $amount = floatval(abs($amount));
        static::whereId($this->id)->update(['prepaid_credit' => DB::raw("COALESCE(prepaid_credit, 0) + {$amount}")]);

        $this->prepaid_credit += $amount;
    }

    public function reducePrepaidCredit($amount)
    {
        $amount = floatval(abs($amount));

        static::whereId($this->id)->where('prepaid_credit', '>', 0)->update([
            'prepaid_credit' => DB::raw("GREATEST(COALESCE(prepaid_credit, 0) - {$amount}, 0)"),
        ]);

        $this->prepaid_credit = max($this->prepaid_credit - $amount, 0);
    }

    public function getPrepaidCredit()
    {
        $credit = static::find($this->id)->prepaid_credit;

        return floatval($credit);
    }

    public function autoBuyPrepaidCredit($rateAmount)
    {
        try {
            if (! shop_setting('shipping.auto-buy-prepaid-credit', false)) {
                log_info("[prepaid-credit] Auto buy prepaid credit is disabled. Exiting..");
                return false;
            }

            $shopOwner = $this;
            $rateAmount = floatval($rateAmount);
            if ($rateAmount < $shopOwner->getPrepaidCredit()) {
                log_info("[prepaid-credit] User has enough credit for rate amount $".amount($rateAmount).". Exiting..");
                return false;
            }

            // charge the shop owner
            $amount = max($rateAmount, 100);
            log_info("[prepaid-credit] Trying to charge \${$amount} for prepaid credit.");
            $charge = $shopOwner->charge(dollars_to_cents($amount), [
                'statement_descriptor' => 'CommentSold Shipping'
            ]);
            $shopOwner->addPrepaidCredit($amount);

            StripeCharges::log($amount, $charge, $shopOwner);
            (new ShopMailer)->sendPrepaidCreditEmail($shopOwner, $amount);
            return true;

        } catch (Exception $e) {

            if ($e instanceof \Stripe\Error\Card) {
                shop_setting_set('shipping.auto-buy-prepaid-credit', false);
                (new ShopMailer)->sendPrepaidCreditFailedEmail($shopOwner, $amount);
            }

            ShopMailer::notifyAdmin($e);
            log_error("[prepaid-credit] [exception] {$e}");
            return false;
        }
    }

    public function trialExpired()
    {
        if ($this->trialDaysLeft() > 0) {
            return false;
        }

        return true;
    }

    public function trialDaysLeft()
    {
        if (empty($this->trial_started_at) || ! is_numeric($this->trial_started_at)) {
            return 0;
        }

        $secondsPassed = time() - $this->trial_started_at;

        $daysPassed = intval($secondsPassed / (60*60*24));

        $trialLength = 15;
        if (shop_id() == 'kimonokorner') {
            $trialLength = 30;
        }
        $daysLeft = max(0, $trialLength - $daysPassed);

        return $daysLeft;
    }

    public function monthlyFeeWaived()
    {
        return $this->billing_plan == 'boutique_hub';
    }

    public function hasSubscriptionPlan()
    {
        if (! empty($this->billing_plan)) {
            return true;
        }

        return false;
    }

    public function monthlyFee()
    {
        $plan = strtolower(trim($this->billing_plan));

        if ($plan == 'starter') {
            return 49; // $49/month
        }

        if ($plan == 'business') {
            return 149; // $149/month
        }

        if ($plan == 'boutique_hub_starter') {
            return 24.99; // $24.99/month
        }

        if ($plan == 'boutique_hub_business') {
            return 99; // $99/month
        }

        return 0;
    }
}
