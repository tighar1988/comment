<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PayPal Error</title>
        <link href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ cdn('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .full-height {
                height: 100vh;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            .content {
                text-align: center;
            }
            .title {
                font-size: 100px;
            }
            .message {
                margin: 0px 10px;
            }
            .alert-danger {
                color: #fff;
                border-color: #d73925;
                background-color: #dd4b39 !important;
                font-size: 16px;
            }
            .fa {
                color: #dd4b39;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Error</div>
                <div class="message">
                    <div class="alert alert-danger">
                        <strong></i> Danger!</strong> {{ $errorMessage }}
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
