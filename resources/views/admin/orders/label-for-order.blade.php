<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <h3 class="subheading">Order Shipping Label</h3>

        @if ($order->tracking_number)
            <h3 class="subheading">This order already has a label! </h3>
            <strong>Tracking Number:</strong> {{ $order->tracking_number }} <br/>
            <strong>Label:</strong> <a target="_blank" href="/admin/orders/{{ $order->id }}/print-label">Open</a>
            @if (usps_enabled() && $order->canRefundLabel())
                <br/><br/>
                <button title="Refund Label" class="btn btn-danger btn-sm" data-loading-text="Refunding Label..." onclick="refundLabel({{ $order->id }}, this)">Refund Label</button>
            @endif
            <br/><hr/><br/>
        @endif

        @if (! shop_setting('goshippo.api_token') && ! usps_enabled())
            The goShippo.com service is not enabled. Please go to <a href="/admin/setup">Setup</a> and enable goShippo.com

        @else
            @if ($order->local_pickup)
                <h4>Delivery is set to "Local Pickup". You don't need a shipping label for this order.</h4>
            @else

                <h4>Label From Address:</h4>
                <address>
                    @if (shop_setting('shop.from_name'))
                        <strong>{{ shop_setting('shop.from_name') }}</strong><br>
                        {{ shop_setting('shop.street_address') }}<br>
                        {{ shop_setting('shop.city') }}, {{ shop_setting('shop.state') }}, {{ shop_setting('shop.country_code', 'US') }}, {{ shop_setting('shop.zip_code') }}<br>
                        <abbr title="Phone">P:</abbr> {{ shop_setting('shop.phone') }}
                    @else
                        You need a From Address in order to create labels. Please go to <a href="/admin/setup">Setup</a> to set your address.
                    @endif
                </address>

                <h4>Label To Address:</h4>
                <address>
                    <strong>{{ $order->ship_name }}</strong><br>
                    @if ($order->apartment) Apt. {{ $order->apartment }}, @endif {{ $order->street_address }}<br>
                    {{ $order->city }}, {{ $order->state }}, {{ $order->getCountryCode() }}, {{ $order->zip }}<br>
                </address>

                @if (! usps_enabled() || (usps_enabled() && empty($order->tracking_number)))
                <div class="parcel-dimensions">
                    <h4>Parcel Weight:</h4>
                    <input type="text" class="parcel-weight form-control" value="{{ $totalWeight }}" placeholder="Weight (oz)" name="parcel_weight"> oz ({{ $nrProducts }} {{ str_plural('item', $nrProducts) }})

                    <h4>Parcel Dimensions (in inches):</h4>
                    <input value="12" type="text" class="parcel-length form-control" placeholder="Length (in)" name="parcel_length">
                    <input value="10" type="text" class="parcel-width form-control" placeholder="Width (in)" name="parcel_width">
                    <input value="1" type="text" class="parcel-height form-control" placeholder="Height (in)" name="parcel_height">

                    <br/><br/>
                    <button title="Buy Label" class="btn btn-success btn-sm" data-loading-text="Creating Label..." onclick="buyLabel({{ $order->id }}, this)">Create Label</button>

                    <button type="button" class="btn btn-default m-r-10" onclick="closeDetails();">Cancel</button>

                    <div class="checkbox">
                        <label><input type="checkbox" class="bypass-validation" name="bypass_address_validation"> Bypass Address Validation</label>
                    </div>

                    <span class="help-block" style="display: none;"></span>

                    <br/><br/>
                </div>
                @endif

            @endif
        @endif
    </div>
</div>
