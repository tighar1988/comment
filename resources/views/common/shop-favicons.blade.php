<link rel="apple-touch-icon" sizes="57x57" href="{{ cdn('/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ cdn('/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ cdn('/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ cdn('/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ cdn('/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ cdn('/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ cdn('/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ cdn('/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ cdn('/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ cdn('/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ cdn('/favicon.ico') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ cdn('/favicon.ico') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ cdn('/favicon.ico') }}">
