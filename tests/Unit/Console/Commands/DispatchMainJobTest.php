<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\DispatchMainJob;
use Mockery;

class DispatchMainJobTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(DispatchMainJob::class, new DispatchMainJob);
    }
}
