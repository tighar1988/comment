<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><hr/><br/>

    Your Free Trial will expire in 3 days. After that your card will be charged according to your subscription plan every 30 days.
    <br/><br/>
</div>
