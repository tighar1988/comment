<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasCard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::user()->stripe_id && Auth::user()->superadmin) {
            // force the user to create a shop first
            return redirect('/admin/setup/step-1');
        }

        return $next($request);
    }
}
