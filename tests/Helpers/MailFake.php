<?php

namespace Tests\Helpers;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Contracts\Mail\MailQueue as MailQueueContract;
use Illuminate\Support\Testing\Fakes\MailFake as IlluminateMailFake;

class MailFake extends IlluminateMailFake implements MailQueueContract
{
    /**
     * Queue a new e-mail message for sending.
     *
     * @param  string|array  $view
     * @param  array  $data
     * @param  \Closure|string  $callback
     * @param  string|null  $queue
     * @return mixed
     */
    public function queue($view, array $data = [], $callback = null, $queue = null)
    {
        //
    }

    /**
     * Queue a new e-mail message for sending after (n) seconds.
     *
     * @param  int  $delay
     * @param  string|array  $view
     * @param  array  $data
     * @param  \Closure|string  $callback
     * @param  string|null  $queue
     * @return mixed
     */
    public function later($delay, $view, array $data = [], $callback = null, $queue = null)
    {
        // mock later method
        if (! $view instanceof Mailable) {
            return;
        }

        $this->mailables[] = $view;
    }
}
