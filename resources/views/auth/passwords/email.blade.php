@extends('layouts.app')

@section('content')
<div class="login-box">
    <div class="login-box-body">
        <div class="login-logo">
            <a href="/"><img src="{{ cdn('/images/login-logo.png') }}" alt="CommentSold"></a>
        </div>
        <p class="login-box-msg">Reset Password</p>
        <form role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}

            @if (session('status'))
                <div class="alert alert-info">
                    {{ session('status') }}
                </div>
            @endif

            <div class="form-group has-feedback @hasError('email')">
                <input type="email" name="email" value="{{ old('email') }}" required autofocus class="form-control" placeholder="E-Mail Address">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @error('email')
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat m-b-20">Send Password Reset Link</button>
                </div>
            </div>
        </form>
        <a href="{{ url('/login') }}">Login</a><br>
    </div>
</div>
@endsection
