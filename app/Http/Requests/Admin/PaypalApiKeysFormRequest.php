<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Checkout\PayPal;

class PaypalApiKeysFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'     => 'required|min:3',
            'client_secret' => 'required|min:3',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (! PayPal::keysAreValid($this->client_id, $this->client_secret)) {
                if (filter_var($this->client_id, FILTER_VALIDATE_EMAIL)) {
                    $validator->errors()->add('client_id', 'Your PayPal email and password cannot be used as API keys.');
                } else {
                    $validator->errors()->add('client_id', 'Invalid API keys. Make sure you are adding the LIVE api keys.');
                }
            }
        });
    }
}
