<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('inventory_id')->unsigned()->nullable();
            $table->double('price')->nullable();
            $table->double('cost')->nullable();
            $table->text('prod_name')->nullable();
            $table->text('color')->nullable();
            $table->text('size')->nullable();
            $table->integer('comment_id')->unsigned()->nullable();
            $table->integer('returned_date')->nullable();
            $table->double('refund_issued')->nullable();
            $table->integer('order_source')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_products');
    }
}
