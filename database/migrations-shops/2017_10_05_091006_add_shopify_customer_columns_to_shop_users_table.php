<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopifyCustomerColumnsToShopUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_users', function (Blueprint $table) {
            $table->string('shopify_customer_id')->nullable();
            $table->boolean('accepts_marketing')->nullable();
            $table->boolean('verified_email')->nullable();
            $table->string('first_name')->after('name')->nullable();
            $table->string('last_name')->after('name')->nullable();
            $table->string('password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_users', function (Blueprint $table) {
            $table->dropColumn(['shopify_customer_id', 'accepts_marketing', 'verified_email', 'first_name', 'last_name']);
        });
    }
}
