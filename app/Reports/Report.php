<?php

namespace App\Reports;

use Carbon\Carbon;
use App\Models\Waitlist;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\ShopUser;
use App\Models\ScanLog;

class Report
{
    public $startDate;

    public $endDate;

    public $startDateTime;

    public $endDateTime;

    public $start;

    public $end;

    public $nrTotalOrders;

    public $nrOrdersPaid;

    public $nrOrdersFulfilled;

    public $ordersRevenue;

    public $stripeRevenue;

    public $paypalRevenue;

    public $stateTax;

    public $countyTax;

    public $municipalTax;

    public $shippingCost;

    public $shippingCharged;

    public $orderHR;

    public $grossProfit;

    public $msgrOrders;

    public $adminOrders;

    public $commentOrders;

    public $instagramCommentOrders;

    public $onlineStoreOrders;

    public $shippedReturnCost;

    public $averageOrder;

    public $returnsTotal;

    public $returnsCount;

    public $revenue;

    public $cogs;

    public $inventoryValue = 0;

    public $inventoryCost = 0;

    public $vendorReturns = [];

    public $nrNewCustomers = 0;

    public $nrTotalCustomers = 0;

    public $nrMessengerUsers = 0;

    public $nrFulfilled = 0;

    public $packers = [];

    public $nrWaitlist = 0;

    public $nrOrderProductsPercentage = 0;

    public $nrWaitlistPercentage = 0;

    public $nrOpTotal = 0;

    public $nrOpPaid = 0;

    public $nrOpFulfilled = 0;

    public $totalCustomerCredits = 0;

    public function __construct($startDate = null, $endDate = null)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function generate()
    {
        // multiple start and end dates formats
        $this->startDate = is_null($this->startDate) ? current_day() : $this->startDate;
        $this->endDate = is_null($this->endDate) ? current_day() : $this->endDate;
        $this->startDateTime = strtotime($this->startDate.' 00:00:00');
        $this->endDateTime = strtotime($this->endDate.' 23:59:59');
        $this->start = Carbon::createFromTimestamp($this->startDateTime)->toDateTimeString();
        $this->end = Carbon::createFromTimestamp($this->endDateTime)->toDateTimeString();

        log_info("[reporting] Getting report between {$this->startDate} and {$this->endDate}");

        $this->calculateCustomers();
        $this->calculatePackers();
        $this->calculateInventoryReport();
        $this->calculateReport();
        $this->calculateProductsReport();
    }

    public function calculateCustomers()
    {
        $this->nrNewCustomers = ShopUser::between($this->start, $this->end)->count();
        $this->nrTotalCustomers = ShopUser::count();
        $this->nrMessengerUsers = ShopUser::whereNotNull('messenger_id')->count();
        $this->totalCustomerCredits = ShopUser::sum('balance');
    }

    public function calculatePackers()
    {
        $this->nrFulfilled = ScanLog::between($this->start, $this->end)->nrFulfilled()->first()->count ?? 0;
        $this->packers = ScanLog::between($this->start, $this->end)->packers()->get();
    }

    public function calculateInventoryReport()
    {
        foreach (Inventory::all() as $item) {
            if ($item->quantity > 0) {
                $this->inventoryValue += $item->price * $item->quantity;
                $this->inventoryCost += $item->cost * $item->quantity;
            }
        }
    }

    public function calculateReport()
    {
        $orders = Order::between($this->startDateTime, $this->endDateTime)->get();
        $this->nrTotalOrders = $orders->count();

        $this->nrOrdersPaid = $orders->filter(function ($order) {
            return $order->isPaid();
        })->count();

        $this->nrOrdersFulfilled = $orders->filter(function ($order) {
            return $order->isFulfilled();
        })->count();

        $this->stripeRevenue = $orders->filter(function ($order) {
            return strpos($order->payment_ref, 'PAY') === false;
        })->sum('payment_amt');

        $this->paypalRevenue  = $orders->filter(function ($order) {
            return strpos($order->payment_ref, 'PAY') !== false;
        })->sum('payment_amt');

        $this->ordersRevenue   = $orders->sum('payment_amt');
        $this->stateTax        = $orders->sum('state_tax');
        $this->countyTax       = $orders->sum('county_tax');
        $this->municipalTax    = $orders->sum('municipal_tax');
        $this->shippingCost    = $orders->sum('ship_cost');
        $this->shippingCharged = $orders->sum('ship_charged');
        $this->averageOrder    = $orders->average('payment_amt');

        $this->orderHR = $this->getOrderHR($orders);

        $this->vendorReturns = OrderProduct::reportVendorReturns($this->startDateTime, $this->endDateTime);

        $ids = $orders->filter(function ($order) { return $order->isPaid(); })->pluck('id');
        $this->nrOpPaid = OrderProduct::whereIn('order_id', $ids)->count();

        $ids = $orders->filter(function ($order) { return $order->isFulfilled(); })->pluck('id');
        $this->nrOpFulfilled = OrderProduct::whereIn('order_id', $ids)->count();

        $this->nrOpTotal = $this->nrOpPaid + $this->nrOpFulfilled;
    }

    public function calculateProductsReport()
    {
        $products = OrderProduct::reportProducts($this->startDateTime, $this->endDateTime);

        $this->cogs = $products->sum('cost');
        $this->revenue = $products->sum('price');

        $this->commentOrders = $products->filter(function ($product) {
            return $product->order_source == OrderProduct::SOURCE_FACEBOOK_COMMENT;
        })->count();

        $this->instagramCommentOrders = $products->filter(function ($product) {
            return $product->order_source == OrderProduct::SOURCE_INSTAGRAM_COMMENT;
        })->count();

        $this->onlineStoreOrders = $products->filter(function ($product) {
            return $product->order_source == OrderProduct::SOURCE_STORE;
        })->count();

        $this->msgrOrders = $products->filter(function ($product) {
            return $product->order_source == OrderProduct::SOURCE_FACEBOOK_MESSENGER;
        })->count();

        $this->adminOrders = $products->filter(function ($product) {
            return $product->order_source == OrderProduct::SOURCE_ADMIN;
        })->count();

        $this->shippedReturnCost = $products->filter(function ($product) {
            return $product->returned_date != '';
        })->sum('refund_issued');

        $this->returnsTotal = $products->filter(function ($product) {
            return $product->returned_date != '';
        })->sum('price');

        $this->returnsCount = $products->filter(function ($product) {
            return $product->returned_date != '';
        })->count();

        $this->nrOrderProducts = $products->count();

        /* Payments received, minus */
        $this->grossProfit = $this->ordersRevenue - ($this->stateTax + $this->countyTax + $this->municipalTax + $this->cogs + $this->shippingCost);

        $this->nrWaitlist = Waitlist::between($this->start, $this->end)->count();
        $opWaitlistSum = $this->nrOrderProducts + $this->nrWaitlist;
        if ($opWaitlistSum != 0) {
            $this->nrOrderProductsPercentage = $this->nrOrderProducts / $opWaitlistSum * 100;
            $this->nrWaitlistPercentage = $this->nrWaitlist / $opWaitlistSum * 100;
        }
    }

    public function getOrderHR($orders)
    {
        $orderHR = [];
        for ($i = 1; $i < 25; $i++) {
            $orderHR['t'.sprintf('%02d', $i)] = 0;
        }

        foreach ($orders as $order) {
            $epoch = $order->payment_date - (60 * 60 * 5);
            $dt = new \DateTime("@$epoch");
            if (isset($orderHR['t' . $dt->format('H')])) {
                $orderHR['t' . $dt->format('H')] += $order->payment_amt;
            } else {
                $orderHR['t' . $dt->format('H')] = $order->payment_amt;
            }
        }

        ksort($orderHR);
        $orderHR = implode(',', $orderHR);
        return $orderHR;
    }
}
