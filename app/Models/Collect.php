<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;
use DB;

class Collect extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'collection_product';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'collection_id',
        'featured',
        'position',
        'sort_value',
        'created_at',
        'updated_at',
        'shopify_collect_id',
    ];
}
