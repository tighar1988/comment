@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => $shopUrl])
            <img src="https://commentsold.com/images/poppyanddot-logo.png">
        @endcomponent
    @endslot

    @slot('subcopy')
        Thank you so much for shopping with Poppy and Dot. We do require that this invoice is paid within ONE HOUR (yes, 1 hour so please hurry!) or else it will be AUTOMATICALLY cancelled and given to the next person on the waitlist.<br/><br/>

        If you have any questions or if there are any errors on this invoice, please contact us at customerservice@poppyanddot.com<br/><br/>

        To view our Return Policy, click here: <a href="http://www.poppyanddot.com/pages/shipping-and-returns">http://www.poppyanddot.com/pages/shipping-and-returns</a><br/><br/>

        *Note: All "Blow out Sale" and "Grab Bag" items are final sale. Please check the items Instagram listing to see if your item was final sale.<br/><br/>

        Thank you for shopping with Poppy and Dot!<br/><br/>

        Please login at <a href="{{ $shopUrl }}">My Account</a> to update or pay for your order.<br/><br/>

        XoXo<br/><br/>

        www.PoppyandDot.com<br/><br/>

        Instagram: @poppyanddot<br/><br/>
    @endslot

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ $shopName }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
