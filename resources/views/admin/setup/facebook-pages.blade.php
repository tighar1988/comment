@extends('admin.setup.facebook-setup')

@section('section', 'Facebook Pages')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body lr-m-15">

                <h3 style="margin-top: 0px;">Pages</h3>

                To process comments from Facebook Pages please enable a Facebook Page.<br/><br/>
                @if (empty($pages))
                    You don't have access to any pages with your Facebook account. Visit <a href="https://www.facebook.com/">Facebook</a> and create a Facebook page.
                @else
                    Below is a list of pages in your account. Please choose what Page to process comments from. Only one page can be enabled at a time.
                    <br/><br/>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Page</th>
                                        <th>Category</th>
                                        <th>Manage</th>
                                    </tr>
                                    @foreach ($pages as $page)
                                        <tr>
                                            <td style="width:33%;">
                                                <a class="" href="https://www.facebook.com/{{ $page['id'] }}" target="_blank">{{ $page['name'] }}</a>
                                                @if ($page['id'] == $pageId)
                                                    &nbsp;<strong>(Enabled)</strong>
                                                @endif
                                            </td>
                                            <td style="width:33%;">{{ $page['category'] }}</td>
                                            <td style="width:33%;">
                                                @if ($page['id'] == $pageId)
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/facebook-setup/pages/disable-page">
                                                        <input type="hidden" name="page_id" value="{{ $page['id'] }}">
                                                        <button type="submit" data-loading-text="Disabling Page.." class="btn btn-danger">Disable Page</button>
                                                    </form>
                                                @else
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/facebook-setup/pages/enable-page">
                                                        <input type="hidden" name="page_id" value="{{ $page['id'] }}">
                                                        <button type="submit" data-loading-text="Enabling Page.." class="btn btn-default">Enable Page</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
