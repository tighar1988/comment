<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mails\ShopMailer;
use App\Models\Order;
use App\Models\User;
use App\Models\PayPalComissionCharge;
use Exception;
use App\Checkout\Fees;
use App\Jobs\Traits\ReconnectShopDB;

class CollectPayPalFees implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    /**
     * The shop database name.
     *
     * @var string
     */
    protected $shop;

    public $shopOwner;

    public $lastTime;

    public $newLastTime;

    public $ordersTotal;

    public $feePercentage;

    public $feesTotal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop)
    {
        $this->shop = $shop;
    }

    /**
     * Collect PayPal Fees job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->reconnectDB($this->shop);

            $this->lastTime = shop_setting('paypal:last-collection-time');
            $this->shopOwner = User::getByShopName($this->shop);
            if (is_null($this->lastTime)) {
                $this->lastTime = $this->shopOwner->created_at->getTimestamp() ?? null;
            }

            $fees = new Fees(shop_id(), shop_setting('shop.fees-billing-plan'));
            $this->feePercentage = $fees->getFeesPercentage();

            $this->ordersTotal = Order::getPayPalOrdersTotalSince($this->lastTime)->sum('total');
            if ($fees->isCustomShop() || $this->shop == 'fashionten') {
                // for specific shops, apply the fee without the shipping or tax
                $this->ordersTotal -= Order::getPayPalOrdersTotalSince($this->lastTime)->sum('ship_charged');
                $this->ordersTotal -= Order::getPayPalOrdersTotalSince($this->lastTime)->sum('tax_total');
                if ($this->ordersTotal < 0) {
                    $this->ordersTotal = 0;
                }
            }

            $this->newLastTime = time();

            // don't bill shops with no activity
            if ($this->ordersTotal == 0) {
                log_info('[paypal-fees] No PayPal orders to calculate fees on since '. style_date($this->lastTime) . ' Exiting..');
                return;
            }

            // calculate the total fees
            $this->feesTotal = round($this->feePercentage * $this->ordersTotal, 2);

            if ($this->feesTotal < 50) {
                log_info('[paypal-fees] Comission fee ($'.amount($this->feesTotal).') is less than $50. Exiting..');
                return;
            }

            // charge the shop owner
            log_info("[paypal-fees] Trying to charge \${$this->feesTotal} ({$this->feePercentage} * \${$this->ordersTotal}) since {$this->lastTime}.");
            $this->shopOwner->charge(dollars_to_cents($this->feesTotal), [
                'statement_descriptor' => 'CommentSold PayPal'
            ]);

            shop_setting_set('paypal:last-collection-time', $this->newLastTime);
            shop_setting_set('paypal:card-declined', false);

            PayPalComissionCharge::logPaid($this);
            (new ShopMailer)->sendPayPalFeesEmail($this->shopOwner, $this->feesTotal, $this->lastTime);

        } catch (Exception $e) {

            if ($e instanceof \Stripe\Error\Card && $e->getStripeCode() == 'card_declined') {
                shop_setting_set('paypal:card-declined', true);
                PayPalComissionCharge::logFailed($this, $e->getMessage());
                (new ShopMailer)->sendPayPalFeesFailedEmail($this->shopOwner, $this->feesTotal, $this->lastTime);

            } else {
                PayPalComissionCharge::logFailedHidden($this, $e->getMessage());
            }

            ShopMailer::notifyAdmin($e);
            log_error("[paypal-fees] [exception] {$e}");
        }
    }
}
