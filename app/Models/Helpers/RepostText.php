<?php

namespace App\Models\Helpers;

class RepostText
{
    protected $product;

    protected $message;

    protected $price;

    protected $sizes;

    protected $lines;

    public function __construct($product, $message)
    {
        $this->product = $product;
        $this->message = $this->normalizeNewlines($message);
        $this->lines   = $this->separateLines($message);

        $this->price = $this->getPrice();
        $this->sizes = $this->getSizes();
    }

    public function normalizeNewlines($message)
    {
        $message = str_replace("\r\n", "\n", $message);
        $message = str_replace("\r", "\n", $message);
        return $message;
    }

    public function getPrice()
    {
        return $this->product->variants[0]->price ?? 0;
    }

    public function getSizes()
    {
        $sizes = [];
        foreach ($this->product->variants as $variant) {
            if ($variant->quantity > 0) {
                if (isset($sizes[$variant->color])) {
                    $sizes[$variant->color] .= ", " . $variant->size;
                } else {
                    $sizes[$variant->color] = $variant->size;
                }
            }
        }

        return $sizes;
    }

    public function separateLines($message)
    {
        return explode("\n", $message);
    }

    public function getRepostMessage()
    {
        if ($key = $this->getLineKey('Available in')) {
            $line = $this->lines[$key];
            $identation = $this->getLineIdentation($line);

            $finalSale = stripos($line, '(final sale)') !== false ? ' (final sale)': '';
            $line = $identation . 'Still available for only $' . amount($this->price) . $finalSale . '!' . "\n\n" .
                            $this->variantsText($identation);

            $this->lines[$key] = $line;
            $this->removeOldVariantsText($key);

        } elseif ($key = $this->getLineKey('Available for only')) {
            $line = $this->lines[$key];
            $identation = $this->getLineIdentation($line);

            $finalSale = stripos($line, '(final sale)') !== false ? ' (final sale)': '';
            $line = $identation . 'Still available for only $' . amount($this->price) . $finalSale . '!' . "\n\n" .
                            $this->variantsText($identation);
            $this->lines[$key] = $line;
            $this->removeOldVariantsText($key);

        } else {
            $line = 'Still available for only $' . amount($this->price) . '!' . "\n\n" . $this->variantsText();
            array_unshift($this->lines, $line);
        }

        if ($key = $this->getLineKey('To order')) {
            $line = $this->lines[$key];
            $line = preg_replace('/\s*\([a-zA-Z0-9_,\'"\s]*\)/', '', $line);
            $this->lines[$key] = $line;
        }

        return implode("\n", $this->lines);
    }

    public function getLineKey($words)
    {
        $words = strtolower($words);

        foreach ((array) $this->lines as $key => $line) {
            if (strpos(trim(strtolower($line)), $words) !== false) {
                return $key;
            }
        }

        return null;
    }

    public function variantsText($identation = '')
    {
        $variantsText = '';
        foreach ($this->sizes as $color => $size) {
            if (isset($color) && $color != '') {
                $variantsText .= $identation . "$color: $size\n";
            } else {
                $variantsText .= $identation . "$size\n";
            }
        }

        return rtrim($variantsText, "\n");
    }

    public function removeOldVariantsText($key)
    {
        // remove variants if it's repost of a repost
        for ($i = 1; $i <= (2 * count($this->sizes)); $i++) {
            if (! isset($this->lines[$key + $i])) {
                continue;
            }
            $line = $this->lines[$key + $i];
            foreach ($this->sizes as $color => $size) {
                if ((isset($color) && $color != '' && stripos($line, "$color: ") !== false) ||
                    (trim(strtolower($line)) == strtolower($size))
                ) {
                    unset($this->lines[$key + $i]);
                    break;
                }
            }
        }
    }

    public function getLineIdentation($line)
    {
        $chars = str_split($line);
        $identation = '';
        foreach ($chars as $char) {
            if ($char != " " && $char != "\t") {
                break;
            }

            $identation .= $char;
        }

        return $identation;
    }
}
