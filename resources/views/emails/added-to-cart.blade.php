<div style="font-family: Verdana; font-size: 12px;">

	Hello {{ $customerName }},
	<br/><hr/><br/>

	Thank you for your order. Your new item was added to your shopping cart. <b>Please be advised</b> - You have {{ $expire }} hours to pay for this item or it expires. Items from the shopping cart expire after {{ $expire }} hours.
	<br/><br/>

	@if ($customerStreetAddress)

		<strong>Shipping Address:</strong>
		<br/>

		{{ $customerStreetAddress }}<br/>

		@if ($customerApartment)
			{{ $customerApartment }} <br/>
		@endif

		{{ $customerCity }} {{ $customerState }} {{ $customerZip }}
		<br/><br/>

		If you want to change your address, please login at <a href="{{ $shopUrl }}">My Account</a> and update.

	@else
		At this time, we do not have your shipping address, please login at <a href="{{ $shopUrl }}">My Account</a> and update your shipping address.
	@endif


	{{-- todo: show cart items, and waitlist items; similar code exists in waitlist email, extract common logic to new view  --}}

{{--
	<br/><br/>
	<strong>Products in Order</strong>
	<hr/>
	@if (count( $customer->orders_products ) > 0)
		@foreach ( $customer->orders_products  as $xprod)
			 @if ($xprod['waitlist'] == 't') continue; @endif

			<div style="width: 200px; text-align: center; float: left; margin-bottom: 25px;">
				@if (isset($xprod['filename']))
					<img src="{{$uri}}/catalog/thumbs/{{$xprod['filename']}}" style="width: 150px;" />
				@endif
			</div>

			<div style="float: left;">
				{{$xprod['prod_name']}}
				<br/>

				{{$xprod['color']}}
				<br/>

				{{$xprod['size']}}
				<br/>

				<strong>Price:</strong> ${{number_format($xprod['price'],2)}}

				@if ($xprod['waitlist'] == 't')
					<br/>
					*** Currently on waitlist ***
				@endif
			</div>
			<br style="clear: both;" />
		@endforeach
	@endif --}}

	<br/><br/>
	Please login at <a href="{{ $shopUrl }}">My Account</a> to update or pay for your order.
</div>
