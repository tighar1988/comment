<?php

namespace App\Models\Helpers;

trait SoldMatcher
{
    /**
     * Catch a sale by looking at the word "SOLD"
     *
     * @return boolean
     */
    public function commentedSold()
    {
        return stripos($this->message, 'sold') !== false;
    }

    public function extractEmail()
    {
        $words = [];
        $lines = explode("\n", str_replace(',' ,' ', $this->message));
        foreach ($lines as $line) {
            $words = array_merge($words, explode(' ', $line));
        }

        foreach ($words as $word) {
            $word = trim($word, ' .;-!@#$%^&*()_+{}|"\'?><');
            if (filter_var($word, FILTER_VALIDATE_EMAIL)) {
                return $word;
            }
        }

        return null;
    }

    public function getEmailWithoutVariants($email)
    {
        if (is_null($email)) {
            return null;
        }

        $email = strtolower($email);

        $tmp = explode('@', $email);
        if (! isset($tmp[0], $tmp[1])) {
            return $email;
        }

        // if part before @ contains a '.' character, return only what comes after it
        $poz = strrpos($tmp[0], '.');
        if ($poz !== false) {
            return substr($tmp[0], $poz).'@'.$tmp[1];
        }

        return $email;
    }

    public function findVariantMatch($product)
    {
        $variants = $product->variants;

        // for products with only one inventory item, it is enough to comment 'sold' only
        if ($variants->count() == 1) {
            return $variants->first();
        }

        // sort variants based on their size
        $variants = $variants->sortByDesc(function ($variant, $key) {
            return strlen($variant->size);
        });

        $message = $this->message;
        $message = strtolower($message);

        // try to match the comment without the email and without the word 'sold'
        $email = $this->extractEmail();

        $email = $this->getEmailWithoutVariants($email);
        if (! empty($email)) {
            $message = str_replace($email, '', $message);
        }

        $message = str_replace('sold', '', $message);

        $nrColors = $this->getNrColors($variants);
        $nrSizes = $this->getNrSizes($variants);

        foreach ($variants as $variant) {
            $matchColor = true;
            $matchSize = true;

            $size = strtolower($variant->size);
            $color = strtolower($variant->color);

            // only check variant color is not empty and there are more than one colors
            if ($nrColors > 1 && $color != '') {
                if (! $this->contains($message, $color)) {
                    $matchColor = false;
                }

                if (in_array($color, ['grey', 'gray']) && $this->contains($message, ['grey', 'gray'])) {
                    $matchColor = true;
                }

                if (! is_null($containing = $this->containingColor($color, $variants))) {
                    if ($this->contains($message, $containing)) {
                        $matchColor = false;
                    }
                }
            }

            // only check variant size is not empty and there are more than one sizes
            if ($nrSizes > 1 && $size != '') {
                if (! $this->contains($message, $size)) {
                    $matchSize = false;
                }

                if (in_array($size, ['xl', 'xlarge']) && $this->contains($message, ['xl', 'xlarge'])) {
                    $matchSize = true;
                }

                // do double-match checking for common mistakes
                if ($size == 'medium' && $this->contains($message, 'med') ||
                    $size == 'large' && $this->contains($message, 'lg') ||
                    $size == 's' && $this->contains($message, 'small') ||
                    $size == 'm' && $this->contains($message, 'medium') ||
                    $size == 'l' && $this->contains($message, 'large') ||
                    $size == '1xl' && $this->contains($message, '1x') ||
                    $size == '2xl' && $this->contains($message, '2x') ||
                    $size == '3xl' && $this->contains($message, '3x')
                ) {
                    $matchSize = true;
                }

                if ($matchSize && $this->contains($message, 'x-'.$size)) {
                    $matchSize = false;
                }
            }

            if ($matchColor && $matchSize) {
                return $variant;
            }
        }

        return false;
    }

    public function contains($message, $options)
    {
        foreach ((array) $options as $option) {
            if (is_string($option) && strlen($option) <= 2) {
                // for short options match with spaces
                if ($this->containsWithDelimiter($message, $option)) {
                    return true;
                }
            } else {
                if (str_contains($message, $option)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function containsWithDelimiter($message, $option)
    {
        $option = preg_quote($option, '/');

        // starts with option followed by space
        if (preg_match('/^'.$option.'[\s\.\/,]/i', $message)) {
            return true;
        }

        // contains option
        if (preg_match('/[\s\.\/,#]'.$option.'[\s\.\/,]/i', $message)) {
            return true;
        }

        // ends with space then option
        if (preg_match('/[\s\.\/,#]'.$option.'$/i', $message)) {
            return true;
        }

        return false;
    }

    /**
     * Check to see if this color is included in another color.
     */
    protected function containingColor($color, $variants)
    {
        foreach ($variants as $variant) {
            $containing = strtolower($variant->color);

            if ($containing != $color && str_contains($containing, $color)) {
                return $containing;
            }
        }

        return null;
    }

    protected function getNrColors($variants)
    {
        $colors = [];
        foreach ($variants as $variant) {
            if ($variant->color != '') {
                $colors[] = strtolower($variant->color);
            }
        }

        return count(array_unique($colors));
    }

    protected function getNrSizes($variants)
    {
        $sizes = [];
        foreach ($variants as $variant) {
            if ($variant->size != '') {
                $sizes[] = strtolower($variant->size);
            }
        }

        return count(array_unique($sizes));
    }
}
