<?php

namespace App\Jobs;

use App\Facebook\FacebookMessenger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facebook\MessengerTemplates;
use App\Jobs\Traits\ReconnectShopDB;
use App\Shopify\ShopifyClient;
use App\Shopify\Shopify;
use App\Models\Master;
use App\Models\ShopUser;
use App\Models\Order;
use App\Models\Inventory;
use App\Mails\ShopMailer;
use Log;
use Exception;
use DB;
use Redis;

class HandleShopifyWebhook implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    /**
     * The $_REQUEST array from the web request
     *
     * @var array
     */
    protected $request;

    protected $topic;

    protected $sha256;

    protected $shop;

    protected $data;

    protected $shopify;

    /**
     * Create a new job instance.
     *
     * @param array $request
     *
     * @return void
     */
    public function __construct($request, $topic, $sha256, $shop, $data)
    {
        $this->request = $request;
        $this->topic   = $topic;
        $this->sha256  = $sha256;
        $this->shop    = $shop;
        $this->data    = $data;
    }

    /**
     * Handle the Shopify webhook.
     *
     * @return void
     */
    public function handle()
    {
        $shopDB = Redis::connection('cache')->get("shopify-shop:{$this->shop}");
        $this->reconnectDB($shopDB);

        $this->shopify = new Shopify;
        $client = $this->shopify->getClient();

        if (! $client->isValidWebhook($this->data, $this->sha256)) {
            ShopMailer::notifyAdmin("Invalid shopify webhook, {$this->shop}, {$this->topic}");
            log_notice("[shopify-webhook] Invalid webhook, {$this->shop}, {$this->topic}");
            return;
        }

        if ($this->topic == 'products/update') {
            $this->handleProductUpdate();

        } elseif ($this->topic == 'products/delete') {
            $this->handleProductDelete();

        } elseif ($this->topic == 'products/create') {
            $this->handleProductCreate();

        } elseif ($this->topic == 'orders/fulfilled') {
            $this->handleOrderFulfilled();

        } elseif ($this->topic == 'orders/create') {
            $this->handleOrderCreate();

        } elseif ($this->topic == 'refunds/create') {
            $this->handleOrderRefund();

        } else {
            log_error("[shopify-webhook] Received unknown topic: {$this->topic}");
            return;
        }
    }

    public function handleProductUpdate()
    {
        $this->shopify->importProduct($this->request);
    }

    public function handleProductDelete()
    {
        $this->shopify->deleteProduct($this->request);
    }

    public function handleProductCreate()
    {
        $this->shopify->importProduct($this->request);
    }

    public function handleOrderFulfilled()
    {
        $this->shopify->fulfillOrder($this->request);
    }

    public function handleOrderCreate()
    {
        $orderId   = $this->request['id'] ?? null;
        $orderName = $this->request['name'] ?? null;
        $items     = $this->request['line_items'] ?? [];

        // don't update quantity for orders created in CommentSold
        $order = Order::where('shopify_order_id', '=', $orderId)->first();
        if ($order) {
            return;
        }

        foreach ($items as $item) {
            $quantity  = $item['quantity'] ?? null;
            $variantId = $item['variant_id'] ?? null;

            if (is_null($quantity) || is_null($variantId)) {
                continue;
            }

            $variant = Inventory::where('shopify_inventory_id', '=', $variantId)->first();
            if (! $variant) {
                continue;
            }

            $message = "Shopify order created {$orderName} ({$orderId}).";
            $variant->decrementQuantity($message, $quantity, false);
        }
    }

    public function handleOrderRefund()
    {
        $orderId = $this->request['order_id'] ?? null;
        $items   = $this->request['refund_line_items'] ?? [];
        $restock = $this->request['restock'] ?? null;

        if (! $restock) {
            return;
        }

        foreach ($items as $item) {
            $quantity  = $item['line_item']['quantity'] ?? null;
            $variantId = $item['line_item']['variant_id'] ?? null;

            if (is_null($quantity) || is_null($variantId)) {
                continue;
            }

            $variant = Inventory::where('shopify_inventory_id', '=', $variantId)->first();
            if (! $variant) {
                continue;
            }

            $message = "Shopify order refunded ({$orderId}).";
            $variant->incrementQuantity($message, $quantity, false);
        }
    }
}
