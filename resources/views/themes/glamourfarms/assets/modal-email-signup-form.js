jQuery(document).ready(function() {

	function setCookieEPop(cname,cvalue,exdays) {
		var d = new Date();
		var d2 = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		d2.setTime(d2.getTime()+(exdays*60*60*1000));
		var expires = 'expires='+d.toUTCString();
		var expires2 = 'expires='+d2.toUTCString();
		var dompath = 'path=/';
		document.cookie = cname+'='+cvalue+'; '+expires+'; '+dompath;
	}

	function getCookieEPop(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
			}
			return '';
		}

		function deleteCookieEPop(cname) {
			var name = cname + '=';
			document.cookie = 'ccemfpu1=; expires=Thu, 01 Jan 1970 00:00:00 UTC';
		}

		function checkCookieEPop() {
			var ccemfpu1 = getCookieEPop('ccemfpu1');
			if (ccemfpu1 != '') {    		
				return '';        		
			} 		
			else {
				ccemfpu1 = 'ccemfpu1';
				if (ccemfpu1 != '' && ccemfpu1 != null) {
    		
    				var html1 = $('html');
					var body1 = $('body');
					var overlay1 = '<div id="modal-overlay"></div>';
					var lockedClass1 = 'is-locked';
					var id1 = $('#emailformpopup1');
					if (!html1.hasClass(lockedClass1)) {
						html1.addClass(lockedClass1);
					}
					body1.append(overlay1);
					//resize and position
					modalcenter1 = function () {
						var top1;
						var left1;
						top1 = Math.max($(window).height()-id1.outerHeight(),0)/2;
						left1 = Math.max($(window).width()-id1.outerWidth(),0)/2;
						id1.css({top:top1+$(window).scrollTop(),left:left1+$(window).scrollLeft(),marginTop:'20px'});
					};
					id1.css({width:'auto',height:'auto'});			
					modalcenter1();
					$(window).bind('resize.id1',modalcenter1);

					//transition effect
					id1.fadeIn(500);	
    		
    				setCookieEPop('ccemfpu1',ccemfpu1,1);
    		
				}
			}
		}

		checkCookieEPop();


		//if modal close is clicked
		$('#modal-overlay').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();
    		$('.modal-window').fadeOut(500);
    		var html1 = $('html');
			var overlay1 = document.getElementById('modal-overlay');
    		var lockedClass1 = 'is-locked';
			if (html1.hasClass(lockedClass1)) {
    			html1.removeClass(lockedClass1);
 			}
 			overlay1.parentNode.removeChild(overlay1);
		});


		//if modal close is clicked
		$('.modal-close').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();
    		$('.modal-window').fadeOut(500);
    		var html1 = $('html');
			var overlay1 = document.getElementById('modal-overlay');
    		var lockedClass1 = 'is-locked';
			if (html1.hasClass(lockedClass1)) {
    			html1.removeClass(lockedClass1);
 			}
 			overlay1.parentNode.removeChild(overlay1);
		});

		//if modal close is clicked
		$('.modal-close2').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();
    		$('.modal-window').fadeOut(500);
    		var html1 = $('html');
			var overlay1 = document.getElementById('modal-overlay');
    		var lockedClass1 = 'is-locked';
			if (html1.hasClass(lockedClass1)) {
    			html1.removeClass(lockedClass1);
 			}
 			overlay1.parentNode.removeChild(overlay1);
		});
		
});