<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>:: CommentSold ::</title>
        @include('common.admin-favicons')
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ cdn('/css/bootstrap.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ cdn('/css/admin-styles.css?v=5') }}" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">

        @yield('content')

        <script type="text/javascript" src="{{ cdn('/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ cdn('/js/bootstrap.min.js') }}"></script>
        {{ script_raven() }}
    </body>
</html>
