<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ShopNotCreated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // don't let the user create multiple shops
        if (Auth::user()->shop_name) {
            return redirect('/admin/setup/step-2');
        }

        return $next($request);
    }
}
