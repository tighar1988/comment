<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class BlockSettings extends Drop
{
    protected $block;

    public function __construct($block)
    {
        $this->block = $block;
    }

    public function handle_key($key)
    {
        if ($key == 'image') {
            return new SchemaImage($this->block);
        } elseif ($key == 'menu') {
            //
        }

        if (isset($this->block['settings'][$key])) {
            return $this->block['settings'][$key];
        }

        return null;
    }
}
