@extends('admin.store.store')

@section('section', 'Blog Posts')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body r-p">
                <a href="/admin/store/blog-posts/add" class="btn btn-success add-btn btn-sm">Add Blog Post</a>
                <table id="blog-posts-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Blog</th>
                            <th>Author</th>
                            <th>Date</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($posts as $post)
                            <tr>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->blog->title ?? null }}</td>
                                <td>{{ $post->author }}</td>
                                <td>{{ style_date($post->created_at) }}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-h fa-lg"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                            <li><a href="/admin/store/blog-posts/edit/{{ $post->id }}" class="link">Edit</a></li>
                                            <li><a href="/admin/store/blog-posts/delete/{{ $post->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <br/>
                <a href="/admin/store/blogs" style="margin-left: 10px;" class="btn btn-danger btn-sm">Manage Blogs</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#blog-posts-table').DataTable({
            "bStateSave": true
        });
    });
</script>
@endsection
