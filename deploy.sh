#!/bin/bash

# Update version before trying to deploy
version=`cat /var/www/commentsold.com/lastVersion`
version=$((version+1))

echo "Version is $version - zipping commentsold.com"

echo $version > /var/www/commentsold.com/lastVersion

cd /var/www/commentsold.com

zip /var/www/commentsold_v${version}.zip -r * .[^.]* -x *.git* -x .env

