#!/bin/bash

docker service create --env-file percona.env \
--detach \
--network cs_sql \
--name percona_init \
--endpoint-mode dnsrr \
--constraint 'node.labels.type == sql' \
--mount src=MySQLInit,dst=/var/lib/mysql \
percona/percona-xtradb-cluster:5.7.17

docker service create --env-file percona.env \
--detach \
--network cs_sql \
--env CLUSTER_JOIN=percona,percona_init \
--name percona \
--mode global \
--endpoint-mode dnsrr \
--constraint 'node.labels.type == sql' \
--mount src=MySQLStorageVolume,dst=/var/lib/mysql \
percona/percona-xtradb-cluster:5.7.17

docker service rm percona_init
