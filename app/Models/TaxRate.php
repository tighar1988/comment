<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaxRate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tax_rates';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
        'zip_code',
        'tax_region_name',
        'state_rate',
        'estimated_combined_rate',
        'estimated_county_rate',
        'estimated_city_rate',
        'estimated_special_rate',
        'risk_level',
    ];

    public static function master()
    {
        $taxRates = new static;
        $taxRates->setConnection('master');
        return $taxRates;
    }

    public function getMasterTaxRate($zipCode)
    {
        return static::master()->getTaxRate($zipCode);
    }

    public function getTaxRate($zipCode)
    {
        $taxRate = static::where('zip_code', $zipCode)->first();

        // get only the first part of the zip (in case of extended zip codes)
        if (is_null($taxRate)) {
            $zipCode = trim(explode('-', $zipCode)[0]);
            $taxRate = static::where('zip_code', $zipCode)->first();
        }

        return $taxRate;
    }

    public function updateTaxRate($taxRate)
    {
        static::master()->updateOrCreate(
            ['zip_code' => $taxRate[1]],
            [
                'state'                   => $taxRate[0],
                'tax_region_name'         => $taxRate[2],
                'state_rate'              => $taxRate[3],
                'estimated_combined_rate' => $taxRate[4],
                'estimated_county_rate'   => $taxRate[5],
                'estimated_city_rate'     => $taxRate[6],
                'estimated_special_rate'  => $taxRate[7],
                'risk_level'              => $taxRate[8],
            ]
        );
    }

    public function getMasterMaxTaxRates()
    {
        $zipCodes = MultipleStoreLocation::pluck('zip_code');
        $taxRates = static::master()->whereIn('zip_code', $zipCodes)->get();
        $maxRate = null;

        foreach ($taxRates as $taxRate) {
            if (is_null($maxRate)) {
                $maxRate = $taxRate;
                continue;
            }

            if ($this->sumTax($taxRate) > $this->sumTax($maxRate)) {
                $maxRate = $taxRate;
            }
        }

        return $maxRate;
    }

    public function sumTax($taxRate)
    {
        $stateTax = max(($taxRate->estimated_special_rate ?? 0), ($taxRate->state_rate ?? 0));
        $countyTax = $taxRate->estimated_county_rate ?? 0;
        $municipalTax = $taxRate->estimated_city_rate ?? 0;

        return $stateTax + $countyTax + $municipalTax;
    }
}
