<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use Carbon\Carbon;
use App\Models\AppLog;

class CommentSoldJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $this->clearLogs();
    }

    /**
     * Delete old logs.
     */
    public function clearLogs()
    {
        $twoWeeksAgo = Carbon::now()->subDays(7)->toDateString();

        AppLog::where('created_at', '<', $twoWeeksAgo)->delete();
    }
}
