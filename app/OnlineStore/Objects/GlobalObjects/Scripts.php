<?php

namespace App\OnlineStore\Objects\GlobalObjects;

use App\OnlineStore\Objects\Drop;

class Scripts extends Drop
{
    /**
     * The scripts object returns information about a store's active scripts.
     *
     * To access information about a script, use the syntax scripts.type, where type
     * is the script type. There can be only one active script of a particular type.
     *
     * Currently, the only script type is cart_calculate_line_items.
     *
     * To learn more about Shopify Scripts, visit the help content for the Script Editor app or the API documentation.
     *
     * {% if scripts.cart_calculate_line_items %}
     *     <p>We are currently running a {{ scripts.cart_calculate_line_items.name }} promotion!</p>
     * {% endif %}
     */
    public function handle_key($key)
    {

    }
}
