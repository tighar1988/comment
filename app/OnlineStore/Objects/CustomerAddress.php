<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class CustomerAddress extends Drop
{
    protected $customer;

    public function __construct($customer)
    {
        $this->customer = $customer;
    }

    public function first_name()
    {
        return $this->customer->firstName();
    }

    public function last_name()
    {
        return $this->customer->lastName();
    }

    public function street_address()
    {
        return $this->customer->street_address ?? null;
    }

    public function address1()
    {
        return $this->customer->street_address ?? null;
    }

    public function apartment()
    {
        return $this->customer->apartment ?? null;
    }

    public function address2()
    {
        return $this->customer->apartment ?? null;
    }

    public function street()
    {
        return $this->customer->street_address ?? null;
    }

    public function company()
    {
        return null;
    }

    public function city()
    {
        return $this->customer->city ?? null;
    }

    public function province()
    {
        return $this->customer->stateName();
    }

    public function state()
    {
        return $this->customer->state ?? null;
    }

    public function province_code()
    {
        return $this->customer->state ?? null;
    }

    public function zip()
    {
        return $this->customer->zip ?? null;
    }

    public function country()
    {
        return 'USA';
    }

    public function country_code()
    {
        return 'USA';
    }

    public function phone()
    {
        return $this->customer->phone_number ?? null;
    }

    public function id()
    {
        return $this->customer->id ?? null;
    }
}
