<?php

namespace App\OnlineStore\LiquidFilters;

class MathFilters
{
    /**
     * Returns the absolute value of a number.
     *
     * Input
     * {{ -17 | abs }}
     *
     * Output
     * 17
     * abs will also work on a string if the string only contains a number.
     *
     * Input
     * {{ "-19.86" | abs }}
     *
     * Output
     * 19.86
     */
    public static function abs($input)
    {
        if (is_numeric($input)) {
            return abs($input);
        }

        return $input;
    }

    /**
     * Rounds an output up to the nearest integer.
     *
     * Input
     * {{ 4.6 | ceil }}
     * {{ 4.3 | ceil }}
     *
     * Output
     * 5
     * 5
     */
    public static function _ceil()
    {
        // defined in StandardFilters.php
    }

    /**
     * Divides an output by a number. The output is rounded down to the nearest integer.
     *
     * Input
     * <!-- product.price = 200 -->
     * {{ product.price | divided_by: 10 }}
     *
     * Output
     * 20
     */
    public static function _divided_by()
    {
        // defined in StandardFilters.php
    }

    /**
     * Rounds an output down to the nearest integer.
     *
     * Input
     * {{ 4.6 | floor }}
     * {{ 4.3 | floor }}
     *
     * Output
     * 4
     * 4
     */
    public static function _floor()
    {
        // defined in StandardFilters.php
    }

    /**
     * Subtracts a number from an output.
     *
     * Input
     * <!-- product.price = 200 -->
     * {{ product.price | minus: 15 }}
     *
     * Output
     * 185
     */
    public static function _minus()
    {
        // defined in StandardFilters.php
    }

    /**
     * Adds a number to an output.
     *
     * Input
     * <!-- product.price = 200 -->
     * {{ product.price | plus: 15 }}
     *
     * Output
     * 215
     */
    public static function _plus()
    {
        // defined in StandardFilters.php
    }

    /**
     * Rounds the output to the nearest integer or specified number of decimals.
     *
     * Input
     * {{ 4.6 | round }}
     * {{ 4.3 | round }}
     * {{ 4.5612 | round: 2 }}
     *
     * Output
     * 5
     * 4
     * 4.56
     */
    public static function _round()
    {
        // defined in StandardFilters.php
    }

    /**
     * Multiplies an output by a number.
     *
     * Input
     * <!-- product.price = 200 -->
     * {{ product.price | times: 1.15 }}
     *
     * Output
     * 230
     */
    public static function _times()
    {
        // defined in StandardFilters.php
    }

    /**
     * Divides an output by a number and returns the remainder.
     *
     * Input
     * {{ 12 | modulo:5 }}
     *
     * Output
     * 2
     */
    public static function _modulo()
    {
        // defined in StandardFilters.php
    }
}


