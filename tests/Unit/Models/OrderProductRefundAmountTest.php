<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\OrderProduct;
use App\Models\Order;

class OrderProductRefundAmountTest extends TestCase
{
    /** @test */
    public function it_refunds_order_with_one_product_without_other_costs()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 0;
        $order->total = 10;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 10.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_only_shipping_cost()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 5;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 0;
        $order->total = 15;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 10.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_only_tax()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 1.30;
        $order->coupon_discount = 0;
        $order->apply_balance = 0;
        $order->total = 11.30;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 11.30);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_only_coupon()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 5;
        $order->apply_balance = 0;
        $order->total = 5;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 5.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_only_partial_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 2;
        $order->total = 8;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 10.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_only_full_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 10;
        $order->total = 0;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 10.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_ship_plus_tax()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 5;
        $order->tax_total = 1.20;
        $order->coupon_discount = 0;
        $order->apply_balance = 0;
        $order->total = 16.20;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 11.20);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_ship_plus_coupon()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 5;
        $order->tax_total = 0;
        $order->coupon_discount = 3;
        $order->apply_balance = 0;
        $order->total = 12;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 7.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_ship_plus_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 5;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 3;
        $order->total = 12;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 10.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_tax_plus_coupon()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 1.30;
        $order->coupon_discount = 3;
        $order->apply_balance = 0;
        $order->total = 8.30;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 8.30);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_tax_plus_partial_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 1.30;
        $order->coupon_discount = 0;
        $order->apply_balance = 5;
        $order->total = 6.30;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 11.30);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_tax_plus_full_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 1.30;
        $order->coupon_discount = 0;
        $order->apply_balance = 11.30;
        $order->total = 0;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 11.30);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_coupon_plus_partial_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 3;
        $order->apply_balance = 5;
        $order->total = 2;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 7.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_coupon_plus_full_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 3;
        $order->apply_balance = 7;
        $order->total = 0;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 7.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_ship_plus_tax_puls_coupon()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 5;
        $order->tax_total = 1.20;
        $order->coupon_discount = 3;
        $order->apply_balance = 0;
        $order->total = 13.20;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 8.20);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_ship_plus_tax_plus_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 5;
        $order->tax_total = 1.20;
        $order->coupon_discount = 0;
        $order->apply_balance = 6;
        $order->total = 10.20;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 11.20);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_ship_plus_coupon_plus_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 3;
        $order->tax_total = 0;
        $order->coupon_discount = 3;
        $order->apply_balance = 3;
        $order->total = 7;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 7.0);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_tax_plus_coupon_plus_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 0;
        $order->tax_total = 2.20;
        $order->coupon_discount = 3;
        $order->apply_balance = 3;
        $order->total = 6.20;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 9.20);
    }

    /** @test */
    public function it_refunds_order_with_one_product_with_ship_plus_tax_plus_coupon_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 10;
        $order->ship_charged = 5;
        $order->tax_total = 2.20;
        $order->coupon_discount = 3;
        $order->apply_balance = 3;
        $order->total = 11.20;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 9.20);
    }

    /** @test */
    public function it_refunds_order_with_multiple_products_with_no_extra_costs()
    {
        $order = new Order;
        $order->subtotal = 20;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 0;
        $order->total = 20;

        $op = new OrderProduct;
        $op->price = 10;
        $this->assertEquals($op->getRefundAmount($order), 10.0);
    }

    /** @test */
    public function it_refunds_order_with_multiple_products_with_only_ship()
    {
        $order = new Order;
        $order->subtotal = 30;
        $order->ship_charged = 5;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 0;
        $order->total = 35;

        $op = new OrderProduct;
        $op->price = 15;
        $this->assertEquals($op->getRefundAmount($order), 15.0);
    }

    /** @test */
    public function it_refunds_order_with_multiple_products_with_only_tax()
    {
        $order = new Order;
        $order->subtotal = 30;
        $order->ship_charged = 0;
        $order->tax_total = 3.30;
        $order->coupon_discount = 0;
        $order->apply_balance = 0;
        $order->total = 33.30;

        $op = new OrderProduct;
        $op->price = 15;
        $this->assertEquals($op->getRefundAmount($order), 16.65);
    }

    /** @test */
    public function it_refunds_order_with_multiple_products_with_only_coupon()
    {
        $order = new Order;
        $order->subtotal = 30;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 12;
        $order->apply_balance = 0;
        $order->total = 18;

        $op = new OrderProduct;
        $op->price = 15;
        $this->assertEquals($op->getRefundAmount($order), 9.0);
    }

    /** @test */
    public function it_refunds_order_with_multiple_products_with_only_partial_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 30;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 9;
        $order->total = 21;

        $op = new OrderProduct;
        $op->price = 15;
        $this->assertEquals($op->getRefundAmount($order), 15.0);
    }

    /** @test */
    public function it_refunds_order_with_multiple_products_with_only_full_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 30;
        $order->ship_charged = 0;
        $order->tax_total = 0;
        $order->coupon_discount = 0;
        $order->apply_balance = 30;
        $order->total = 0;

        $op = new OrderProduct;
        $op->price = 15;
        $this->assertEquals($op->getRefundAmount($order), 15.0);
    }

    /** @test */
    public function it_refunds_order_with_multiple_products_with_only_ship_tax_coupon_apply_balance()
    {
        $order = new Order;
        $order->subtotal = 30;
        $order->ship_charged = 5;
        $order->tax_total = 2.20;
        $order->coupon_discount = 3;
        $order->apply_balance = 7;
        $order->total = 27.20;

        $op = new OrderProduct;
        $op->price = 15;
        $this->assertEquals($op->getRefundAmount($order), 14.60);
    }
}
