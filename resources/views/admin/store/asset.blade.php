@extends('admin.store.store')

@section('section', 'assets')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body r-p">
                <a href="/admin/store/assets/add" class="btn btn-success add-btn btn-sm">Add Asset File</a>
                <table id="assets-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>File Name</th>
                            <th>Last Modified</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($assets as $asset)
                            <tr>
                                <td>{{ $asset->file_name }}</td>
                                <td>{{ style_date($asset->updated_at) }}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-h fa-lg"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                            <li><a href="/admin/store/assets/delete/{{ $asset->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#assets-table').DataTable({
            "bStateSave": true
        });
    });
</script>
@endsection
