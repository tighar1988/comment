<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

/**
 * The address object contains information entered by a customer in Shopify's checkout pages.
 * Note that a customer can enter two addresses: billing address or shipping address.
 *
 * When accessing attributes of the address object, you must specify which address you want to target.
 * This is done by using either shipping_address or billing_address before the attribute.
 *
 * address can be used in email templates, the order status page of the checkout, as well as in apps such as Order Printer.
 */
class Address extends Drop
{
    /**
     * Returns the values of the First Name and Last Name fields of the address.
     */
    public function name()
    {
        return 'name';
    }

    public function first_name()
    {
        return 'first_name';
    }

    public function last_name()
    {
        return 'last_name';
    }

    public function address1()
    {
        return 'address1';
    }

    public function address2()
    {
        return 'address2';
    }

    public function street()
    {
        return 'street';
    }

    public function company()
    {
        return 'company';
    }

    public function city()
    {
        return 'city';
    }

    public function province()
    {
        return 'province';
    }

    public function province_code()
    {
        return 'province_code';
    }

    public function zip()
    {
        return 'zip';
    }

    public function country()
    {
        return 'USA';
    }

    public function country_code()
    {
        return 'USA';
    }

    public function phone()
    {
        return 'phone';
    }
}
