<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'handle',
    ];

    public function scopeHandle($query, $handle)
    {
        return $query->where('handle', '=', $handle);
    }

    public function items()
    {
        return $this->hasMany('App\Models\MenuItem', 'menu_id')->orderBy('position', 'ASC');
    }

    public function itemNames()
    {
        $names = [];

        foreach ($this->items as $item) {
            $names[] = $item->name;
        }

        return implode(', ', $names);
    }
}
