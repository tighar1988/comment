<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Coupon;
use Carbon\Carbon;
use Mockery;

class CouponTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Coupon::class, new Coupon);
    }

    /** @test */
    public function is_type_shipping()
    {
        $coupon = new Coupon;
        $coupon->coupon_type = 'S';
        $this->assertTrue($coupon->isTypeShipping());
    }

    /** @test */
    public function is_type_fixed()
    {
        $coupon = new Coupon;
        $coupon->coupon_type = 'F';
        $this->assertTrue($coupon->isTypeFixed());
    }

    /** @test */
    public function is_type_percentage()
    {
        $coupon = new Coupon;
        $coupon->coupon_type = 'P';
        $this->assertTrue($coupon->isTypePercentage());
    }

    /** @test */
    public function is_active()
    {
        $coupon = new Coupon;
        $coupon->starts_at = time() - 100;
        $coupon->expires_at = time() + 100;
        $coupon->max_usage = 10;
        $coupon->use_count = 5;
        $this->assertTrue($coupon->isActive());
    }

    /** @test */
    public function is_not_active_because_of_starts_at()
    {
        $coupon = new Coupon;
        $coupon->starts_at = time() + 100;
        $this->assertFalse($coupon->isActive());
    }

    /** @test */
    public function is_not_active_because_of_expires_at()
    {
        $coupon = new Coupon;
        $coupon->expires_at = time() - 100;
        $this->assertFalse($coupon->isActive());
    }

    /** @test */
    public function is_not_active_because_of_max_usage()
    {
        $coupon = new Coupon;
        $coupon->max_usage = 10;
        $coupon->use_count = 10;
        $this->assertFalse($coupon->isActive());
    }

    /** @test */
    public function is_not_valid_because_it_was_used_only_once()
    {
        $coupon = new Coupon;
        $coupon->options = [
            'onlyOnce' => '1',
        ];

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('id')->andReturn(33);

        $order = Mockery::mock('App\Models\Order');
        $order->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('first')->andReturn(true);
        app()->instance('App\Models\Order', $order);

        $this->assertFalse($coupon->isValid($customer, 10));
    }

    /** @test */
    public function is_not_valid_because_new_users_period()
    {
        $coupon = new Coupon;
        $coupon->options = [
            'newUsersPeriod' => '10',
        ];

        $createdAt = Carbon::now()->subDays(15);
        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('created_at')->andReturn($createdAt);

        $this->assertFalse($coupon->isValid($customer, 10));
    }

    /** @test */
    public function is_not_valid_because_it_was_used_by_the_customer()
    {
        $coupon = new Coupon;
        $coupon->options = [
            'userId' => '12',
        ];

        $customer = Mockery::mock('App\Models\ShopUser');
        $customer->shouldReceive('getAttribute')->with('id')->andReturn(13);

        $this->assertFalse($coupon->isValid($customer, 10));
    }

    /** @test */
    public function is_not_valid_because_it_of_min_purchase_amount()
    {
        $coupon = new Coupon;
        $coupon->options = [
            'minPurchaseAmount' => '20',
        ];

        $customer = Mockery::mock('App\Models\ShopUser');

        $this->assertFalse($coupon->isValid($customer, 19));
    }
}
