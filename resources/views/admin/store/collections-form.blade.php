@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($collection) ? true : false; ?>
<section class="content-header">
    <h1>Store</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/collections"><i class="fa fa-th-large"></i>Store</a></li>
        <li><a href="/admin/store/collections">Collections</a></li>
        @if ($isEdit)
            <li class="active">{{ $collection->title }}</li>
        @else
            <li class="active">Add</li>
        @endif
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}" enctype="multipart/form-data">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('title')">
                                    <label>Collection Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="e.g. Tops, Dresses" value="{{ old('title', $collection->title ?? '') }}">
                                    @error('title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('url')">
                                    <label>URL</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ shop_url('collections/') }}</span>
                                        <input type="text" class="form-control" name="url" value="{{ old('url', $collection->url ?? '') }}">
                                    </div>
                                    @error('url')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('content')">
                                    <label>Content</label>
                                    <textarea class="form-control" name="content" rows="3">{{ old('content', $collection->content ?? '') }}</textarea>
                                    @error('content')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('image')">
                                    <label>Collection Image</label>
                                    @if (isset($collection->image))
                                        <br/><img height="100px" src="{{ $collection->imageUrl() }}">
                                        <div class="text-muted">{{ $collection->image }}</div>
                                    @endif
                                    <input type="file" class="form-control" name="image" placeholder="Image File" value="" />
                                    @error('image')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('seo_title')">
                                    <label>SEO Collection Title <small class="text-muted">(max 70 characters)</small></label>
                                    <input type="text" class="form-control" name="seo_title" placeholder="" maxlength="70" value="{{ old('seo_title', $collection->seo_title ?? '') }}">
                                    @error('seo_title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('seo_description')">
                                    <label>SEO Meta Description <small class="text-muted">(max 160 characters)</small></label>
                                    <textarea class="form-control" maxlength="160" name="seo_description" rows="3">{{ old('seo_description', $collection->seo_description ?? '') }}</textarea>
                                    @error('seo_description')
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success pull-right">@if($isEdit) Update Collection @else Add Collection @endif</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{ cdn('/ckeditor/ckeditor.js') }}"></script>
<script>CKEDITOR.replace('content');</script>
@endsection
