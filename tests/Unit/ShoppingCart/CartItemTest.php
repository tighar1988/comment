<?php

namespace Tests\Unit\ShoppingCart;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Checkout\Checkout;
use App\Models\ShopUser;
use Mockery;
use App\ShoppingCart\CartItem;

class CartItemTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(CartItem::class, new CartItem(null));
    }

    /** @test */
    public function it_returns_expire_at()
    {
        $cartItem = new CartItem(null);
        $cartItem->created_at = 1500647192;

        $this->assertEquals(1500647192 + 36000, $cartItem->expireAt(10));
    }

    /** @test */
    public function it_returns_sale_price()
    {
        $cartItem = new CartItem(null);
        $cartItem->price = 20;
        $cartItem->sale_price = 15;

        $this->assertEquals(15, $cartItem->price());
    }

    /** @test */
    public function it_returns_price_when_sale_price_is_0()
    {
        $cartItem = new CartItem(null);
        $cartItem->price = 20;
        $cartItem->sale_price = 0;

        $this->assertEquals(20, $cartItem->price());
    }

    /** @test */
    public function it_returns_price_when_sale_price_larger()
    {
        $cartItem = new CartItem(null);
        $cartItem->price = 20;
        $cartItem->sale_price = 25;

        $this->assertEquals(20, $cartItem->price());
    }
}
