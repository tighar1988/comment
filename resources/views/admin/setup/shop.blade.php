@extends('admin.setup.setup')

@section('section', 'Shop')

@section('setup')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="form">
                    <div class="form-group">
                        <label class="control-label">Shop URL</label>
                        <input type="text" class="form-control" value="{{ shop_url() }}" readonly>
                    </div>
                </form>

                <hr/>

                <form class="form" role="form" method="POST" action="{{ url('/admin/setup/shop-name') }}">
                    <div class="form-group @hasError('shop_name')">
                        <label class="control-label">Shop Name</label>
                        <input type="text" class="form-control" name="shop_name" value="{{ old('shop-name', shop_name()) }}">
                        @error('shop_name')
                    </div>
                    <button class="btn btn-success" type="submit">Update Shop Name</button>
                </form>

                <hr/>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/shop-description') }}">
                    <div class="form-group @hasError('shop_description')">
                        <label class="control-label">Shop Description</label>
                        <textarea class="form-control" name="shop_description">{{ old('shop-description', shop_description()) }}</textarea>
                        @error('shop_description')
                    </div>
                    <button class="btn btn-success" type="submit">Update Shop Description</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/shop-address') }}">

                    <span class="settings-info">Your shop address (shipping from address).</span>

                    <div class="form-group @hasError('from_name')">
                        <label class="control-label">From Name</label>
                        <input type="text" class="form-control" name="from_name" value="{{ old('from_name', shop_setting('shop.from_name')) }}">
                        @error('from_name')
                    </div>

                    <div class="form-group @hasError('company_name')">
                        <label class="control-label">Company Name</label>
                        <input type="text" class="form-control" name="company_name" value="{{ old('company_name', shop_setting('shop.company_name')) }}">
                        @error('company_name')
                    </div>

                    <div class="form-group @hasError('street_address')">
                        <label class="control-label">Street Address</label>
                        <input type="text" class="form-control" name="street_address" value="{{ old('street_address', shop_setting('shop.street_address')) }}">
                        @error('street_address')
                    </div>

                    <div class="form-group @hasError('city')">
                        <label class="control-label">City</label>
                        <input type="text" class="form-control" name="city" value="{{ old('city', shop_setting('shop.city')) }}">
                        @error('city')
                    </div>

                    <?php $countryCode = shop_setting('shop.country_code', 'US'); ?>
                    <div class="form-group @hasError('state')">
                        <label class="control-label">State</label>
                        <select class="form-control" name="state">
                            <?php $selected = old('state', shop_setting('shop.state')); ?>
                            @if (is_null($selected))
                                <option value="" selected="selected">State..</option>
                            @endif
                            @if ($countryCode == 'CA')
                                <option @if ($selected == 'AB') selected="selected" @endif value="AB">Alberta</option>
                                <option @if ($selected == 'BC') selected="selected" @endif value="BC">British Columbia</option>
                                <option @if ($selected == 'MB') selected="selected" @endif value="MB">Manitoba</option>
                                <option @if ($selected == 'NB') selected="selected" @endif value="NB">New Brunswick</option>
                                <option @if ($selected == 'NL') selected="selected" @endif value="NL">Newfoundland</option>
                                <option @if ($selected == 'NT') selected="selected" @endif value="NT">Northwest Territories</option>
                                <option @if ($selected == 'NS') selected="selected" @endif value="NS">Nova Scotia</option>
                                <option @if ($selected == 'NU') selected="selected" @endif value="NU">Nunavut</option>
                                <option @if ($selected == 'ON') selected="selected" @endif value="ON">Ontario</option>
                                <option @if ($selected == 'PE') selected="selected" @endif value="PE">Prince Edward Island</option>
                                <option @if ($selected == 'QC') selected="selected" @endif value="QC">Quebec</option>
                                <option @if ($selected == 'SK') selected="selected" @endif value="SK">Saskatchewan</option>
                                <option @if ($selected == 'YT') selected="selected" @endif value="YT">Yukon</option>
                            @else
                                @include('common.states', compact('selected'))
                            @endif
                        </select>
                        @error('state')
                    </div>

                    @if ($countryCode == 'CA')
                        <div class="form-group">
                            <label class="control-label">Country</label>
                            <input type="text" class="form-control" value="Canada" readonly>
                        </div>
                    @endif

                    <div class="form-group @hasError('zip_code')">
                        <label class="control-label">Zip Code</label>
                        <input type="text" class="form-control" name="zip_code" value="{{ old('zip_code', shop_setting('shop.zip_code')) }}">
                        @error('zip_code')
                    </div>

                    <div class="form-group @hasError('phone')">
                        <label class="control-label">Company Phone</label>
                        <input type="text" class="form-control" name="phone" value="{{ old('phone', shop_setting('shop.phone')) }}">
                        @error('phone')
                    </div>

                    <div class="form-group @hasError('company_email')">
                        <label class="control-label">Company Email</label>
                        <input type="text" class="form-control" name="company_email" value="{{ old('company_email', shop_setting('shop.company_email')) }}">
                        @error('company_email')
                    </div>

                    <button class="btn btn-success" type="submit">Update Shop Address</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row" id="timezone">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/timezone') }}">

                    <span class="settings-info">Your shop timezone.</span>

                    <div class="form-group @hasError('timezone')">
                        <label class="control-label">Timezone</label>
                        <select class="form-control" name="timezone">
                            <?php $selected = old('timezone', shop_setting('shop.timezone')); ?>
                            @if (is_null($selected))
                                <option value="" selected="selected">Timezone..</option>
                            @endif
                            @include('common.timezones', compact('selected'))
                        </select>
                        @error('timezone')
                    </div>

                    <button class="btn btn-success" type="submit">Update Timezone</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/expire') }}">
                    <div class="form-group @hasError('expire')">
                        <label class="control-label text-left">Expire shopping cart items after # hours</label>
                        <input type="text" class="form-control" name="expire" value="{{ old('expire', expire()) }}">
                        @error('expire')
                    </div>
                    <button class="btn btn-success" type="submit">Update Expire</button>
                </form>

                <hr/>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/remind') }}">
                    <div class="form-group @hasError('remind')">
                        <label class="control-label text-left">Send reminder email (to checkout) every # hours</label>
                        <input type="text" class="form-control" name="remind" value="{{ old('remind', remind()) }}">
                        @error('remind')
                    </div>
                    <button class="btn btn-success" type="submit">Update Remind</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
