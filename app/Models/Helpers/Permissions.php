<?php

namespace App\Models\Helpers;

use App\Models\User;

class Permissions
{
    /**
     * The user.
     *
     * @var App\Models\User
     */
    protected $user;

    /**
     * The list of all possible permissions.
     *
     * @return array
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function list()
    {
        return [
            'view-dashboard'         => 'View Dashboard',
            'view-reporting'         => 'View Reporting',
            'view-reporting-packers' => 'View Reporting - Packers Overview',
            'manage-products'        => 'Manage Products',
            'manage-posts'           => 'Manage Posts',
            'manage-orders'          => 'Manage Orders',
            'manage-coupons'         => 'Manage Coupons',
            'manage-customers'       => 'Manage Customers',
            'view-waitlist'          => 'View Waitlist',
            'view-shopping-cart'     => 'View Shopping Cart',
            'view-inventory-logs'    => 'View Inventory Logs',
            'manage-team'            => 'Manage Team',
            'manage-store-themes'    => 'Manage Store Themes',
        ];
    }

    public static function permissionKeys()
    {
        return array_keys(static::list());
    }

    public function has($permission)
    {
        if ($this->user->isSuperAdmin()) {
            return true;
        }

        if (isset($this->user->permissions['all-permissions']) && $this->user->permissions['all-permissions'] == true) {
            return true;
        }

        if (isset($this->user->permissions[$permission]) && $this->user->permissions[$permission] == true) {
            return true;
        }

        return false;
    }
}
