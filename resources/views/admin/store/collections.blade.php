@extends('admin.store.store')

@section('section', 'Collections')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body r-p">
                <a href="/admin/store/collections/add" class="btn btn-success add-btn btn-sm">Add Collection</a>
                <table id="collections-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Url</th>
                            <th>Last Modified</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($collections as $collection)
                            <tr>
                                <td>{{ $collection->title }}</td>
                                <td>{{ $collection->getUrl() }}</td>
                                <td>{{ style_date($collection->updated_at) }}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-h fa-lg"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                            <li><a href="/admin/store/collections/edit/{{ $collection->id }}" class="link">Edit</a></li>
                                            <li><a href="/admin/store/collections/delete/{{ $collection->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#collections-table').DataTable();
    });
</script>
@endsection
