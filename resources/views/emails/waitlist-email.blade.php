<div style="font-family: Verdana; font-size: 12px;">

    Hello {{ $customerName }},
    <br/><hr/><br/>

@if (isset($shopId) && $shopId == 'fashionten')
    Thank you for your comment. Your item is currently sold out and on the waitlist. As an overstock company, we cannot guarantee an item will become available again, but if it does, we will notify you!
    <br/><br/>
@else
    Thank you for your order. Your item is currently waitlisted. We will notify you when it's back in stock!
    <br/><br/>
@endif

    {{-- todo: show items in cart and items in waitlist --}}

{{--
    <br/><br/>
    <strong>Products in Cart</strong>
    <hr/>

    @if (count($order['orders_products']) > 0))
        @foreach ($order['orders_products'] as $xprod)
            <div style="width: 200px; text-align: center; float: left; margin-bottom: 25px;">
                @if (isset($xprod['filename']))
                    <img src="{{ $uri}}/catalog/thumbs/{{ $xprod['filename'] }}" style="width: 150px;" />
                @endif
            </div>
            <div style="float: left;">
                {{ $xprod['prod_name'] }}
                <br/>

                {{ $xprod['color'] }}
                <br/>

                {{ $xprod['size'] }}
                <br/>

                <strong>Price:</strong> ${{number_format($xprod['price'],2)}}

                @if ($xprod['waitlist'] == 't')
                    <br/>
                    *** Currently on waitlist ***
                @endif
            </div>

            <br style="clear: both;" />
        @endforeach
    @endif
--}}

    <br/><br/>
</div>
