@extends('admin.store.store')

@section('section', 'Preferences')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form class="form" role="form" method="POST" action="{{ url('/admin/store/preferences') }}">
                    <span class="settings-info">Title and meta description</span>
                    <div><small class="text-muted">The title and meta description help define how your store shows up on search engines.</small></div>
                    <br/>

                    <div class="form-group @hasError('page_title')">
                        <label class="control-label">Homepage title</label> <small class="text-muted">70 characters allowed</small>
                        <input type="text" class="form-control" maxlength="70" name="page_title" value="{{ old('page_title', shop_setting('online-store.page_title')) }}">
                        @error('page_title')
                    </div>

                    <div class="form-group @hasError('page_description')">
                        <label class="control-label">Homepage meta description</label> <small class="text-muted">160 characters allowed</small>
                        <input type="text" class="form-control" maxlength="160" name="page_description" value="{{ old('page_description', shop_setting('online-store.page_description')) }}">
                        @error('page_description')
                    </div>

                    <button class="btn btn-success" data-loading-text="Saving.." type="submit">Save</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
