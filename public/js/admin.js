function swapSchedule(obj) {
    var val = obj.value;
    if (val == 2) {
        $('#later-date').show();
    } else {
        $('#later-date').hide();
    }
}

// $(document).ready(function() {
//     $('.confirm-delete').on('click', function(e) {
//         e.preventDefault();
//         var action = $(this).attr('href');
//         $('#confirm-delete-modal').find('form').attr('action', action);
//         $('#confirm-delete-modal').modal('show');
//     });
// });

function alertMessage(message, title) {
    var $modal = $('#alert-modal');
    $('.modal-title', $modal).html(title);
    $('.modal-body p', $modal).html(message);
    $modal.modal('show');
}

function confirm_action(element) {
    var action = $(element).attr('href');
    var title = $(element).attr('data-title') || 'Confirm';
    var body = $(element).attr('data-body') || 'Are you sure to take this action?';
    var button = $(element).attr('data-button') || 'Confirm';
    var loadingText = $(element).attr('data-loading-text') || 'loading..';

    $('#confirm-action-modal .modal-title').text(title);
    $('#confirm-action-modal .modal-body p').text(body);
    $('#confirm-action-modal').find('form').attr('action', action);
    $('#confirm-action-modal').find('button[type="submit"]').text(button);
    $('#confirm-action-modal').find('button[type="submit"]').attr('data-loading-text', loadingText);
    $('#confirm-action-modal').modal('show');
    return false;
}

function confirm_delete(element) {
    var action = $(element).attr('href');
    var title = $(element).attr('data-title') || 'Delete';

    $('#confirm-delete-modal .modal-title').text(title);
    $('#confirm-delete-modal').find('form').attr('action', action);
    $('#confirm-delete-modal').modal('show');
    return false;
}

function showPreloader(element) {
    $('.detail-row').remove();
    $(element).closest('tr').after('<tr class="detail-row"><td colspan="6" class="p-20">Loading..</td></tr>');
}

function showDetails(element, html, className) {
    $('.detail-row').remove();
    $(element).closest('tr').after('<tr class="detail-row '+ className +'"><td colspan="6" class="p-20">'+html+'</td></tr>');
}

function closeDetails() {
    $('.detail-row').remove();
}

function productsOrder(id, element) {
    showPreloader(element);
    $.ajax({
        type:'get',
        url:'/admin/orders/'+id+'/products',
        success: function(html) {
            showDetails(element, html, 'viewProductsData');
        }
    });
}

function editOrder(id, element) {
    showPreloader(element);
    $.ajax({
        type:'get',
        url:'/admin/orders/'+id+'/edit',
        success: function(html) {
            showDetails(element, html, 'editOrderData');
        }
    });
}

function updateOrder(orderId, form) {
    $.ajax({
        type:'post',
        url:'/admin/orders/'+orderId+'/update',
        data: $(form).serialize(),
        success: function(response) {
            if (response.status == 1) {
                $(form).find('.message').text('Order Updated!').show();
            } else {
                $(form).find('.message').text('An error occured!').show();
            }
        }
    });

    return false;
}

function fulfillOrder(id) {
    var conf = confirm("Are you sure you want to fulfill this order?");
    if (conf == true) {
        $.ajax({
            type:'post',
            url:'/admin/orders/'+id+'/fulfill',
            success: function(response) {
                if (response.status == 1) {
                    alert('Order is fulfilled!');
                    location.reload();
                } else {
                    alertMessage('This order is already fulfilled! Do not ship this order!', 'Order Fulfilled');
                    if (typeof errorSound == 'object') {
                        errorSound.play();
                    }
                }
            }
        });
    }
}

function pickUpOrder(id) {
    var conf = confirm("Are you sure you want mark the order as Picked Up?");
    if (conf == true) {
        $.ajax({
            type:'post',
            url:'/admin/orders/'+id+'/pick-up',
            success: function(response) {
                if (response.status == 1) {
                    alert('Order is picked up!');
                    location.reload();
                } else {
                    alertMessage('This order is already picked up! Do not give away this order!', 'Order Picked Up');
                    if (typeof errorSound == 'object') {
                        errorSound.play();
                    }
                }
            }
        });
    }
}

function createLabel(id, element) {
    showPreloader(element);
    $.ajax({
        type:'get',
        url:'/admin/orders/'+id+'/create-label',
        success: function(html) {
            showDetails(element, html, 'createLabelData');
        }
    });
}

function refundLabel(id, buttonElement) {
    if (confirm("Are you sure to invalidate and refund this label?")) {
        $(buttonElement).button('loading');

        $.ajax({
            type:'post',
            url:'/admin/orders/'+id+'/refund-label',
            data: {},
            success: function(response) {
                if (response.status == 1) {
                    alert('The label was invalidated and the prepaid credit was added back to your account.');
                    $(buttonElement).remove();
                } else {
                    $(buttonElement).button('reset');
                    alert('An unexpected error occured.');
                }
            }
        });
    }
}

function buyLabel(id, buttonElement) {
    $(buttonElement).button('loading');

    var dimensions = $(buttonElement).closest('.parcel-dimensions');
    var weight = $(dimensions).find('.parcel-weight');
    var length = $(dimensions).find('.parcel-length');
    var width = $(dimensions).find('.parcel-width');
    var height = $(dimensions).find('.parcel-height');
    var bypassValidation = 0;
    if ($(dimensions).find('.bypass-validation').is(':checked')) {
        bypassValidation = 1;
    }

    if (!weight || !length || !width || !height) {
        alert('All parcel details (weight, length, width, height) are required.');
        return;
    }

    $.ajax({
        type:'post',
        url:'/admin/orders/'+id+'/buy-label',
        data: {
            weight: $(weight).val(),
            length: $(length).val(),
            width: $(width).val(),
            height: $(height).val(),
            bypass_validation: bypassValidation,
        },
        success: function(response) {
            if (response.status == 1) {
                var helpBlock = $(buttonElement).closest('.parcel-dimensions').find('.help-block');
                var html = '';

                if (response.rate.errors) {
                    $(buttonElement).button('reset');
                    $(helpBlock).show();
                    for (var i = 0; i < response.rate.errors.length; i++) {
                        html += '<strong>'+response.rate.errors[i]+'</strong>';
                    }
                    $(helpBlock).html(html);

                } else {
                    var message = "Are you sure to buy label for $" + response.rate.amount;
                    message += " from " + response.rate.provider + "? (" + response.rate.servicelevel_name + ")";

                    if (confirm(message)) {
                        $.ajax({
                            type:'post',
                            url:'/admin/orders/'+id+'/confirm-buy-label',
                            data: {
                                auto_charge: 1,
                                rate_id: response.rate.object_id,
                                weight: $(weight).val(),
                                length: $(length).val(),
                                width: $(width).val(),
                                height: $(height).val(),
                                bypass_validation: bypassValidation,
                            },
                            success: function(response) {
                                if (response.label.errors) {
                                    $(buttonElement).button('reset');
                                    $(helpBlock).show();
                                    var html = '';
                                    for (var i = 0; i < response.label.errors.length; i++) {
                                        html += '<strong>'+response.label.errors[i]+'</strong>';
                                    }
                                    $(helpBlock).html(html);
                                } else {
                                    if (response.status == 1) {
                                        $(weight).prop('disabled', true);
                                        $(length).prop('disabled', true);
                                        $(width).prop('disabled', true);
                                        $(height).prop('disabled', true);
                                        $(buttonElement).remove();
                                        $(dimensions).find('.bypass-validation').closest('.checkbox').remove();

                                        var html = 'Label purchased! <br/>';

                                        if (typeof response.label != 'undefined') {
                                            html += 'Tracking Number: ' + response.label.tracking_number + '<br/>';
                                        }
                                        html += 'Open Label: <a target="_blank" href="/admin/orders/' + id + '/print-label">Open</a><br/>';
                                        $(helpBlock).show();
                                        $(helpBlock).html(html);

                                    } else {
                                        alert('An unexpected error occured.');
                                    }
                                }
                            }
                        });
                    } else {
                        $(buttonElement).button('reset');
                    }
                }
            } else {
                $(buttonElement).button('reset');
                alert('An unexpected error occured.');
            }
        }
    });
}

function returnOrder(id, element) {
    showPreloader(element);
    $.ajax({
        type:'get',
        url:'/admin/orders/'+id+'/return',
        success: function(html) {
            showDetails(element, html, 'initiateReturnData');
        }
    });
}

function returnProduct(orderProductId, element) {
    $(element).button('loading');
    var returnToInventory = confirm('Also add back to inventory?');
    $.ajax({
        type:'post',
        url:'/admin/orders/return-product/'+orderProductId,
        data: {
            back_to_inventory: returnToInventory,
        },
        success: function(response) {
            if (response.status == 1) {
                alert(response.message);
                $(element).hide();
                $(element).closest('td').find('.refund-as-credit').show();
                if (response.location) {
                    $(element).closest('td').find('.item-location').show().html('Location: ' + response.location);
                }
            } else if (response.status == 2) {
                alert(response.message);
            } else {
                alert('Unknown error occured. Please refresh and try again.');
            }
        }
    });
}

function refundAsAccountCredit(orderProductId) {
    $.ajax({
        type:'post',
        url:'/admin/orders/refund-product/'+orderProductId,
        success: function(response) {
            alert('Amount refunded and the customer was notified!');
            location.reload();
        }
    });
}
