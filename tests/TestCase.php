<?php

namespace Tests;

use Mockery;
use Validator;
use App\Models\User;

abstract class TestCase extends \Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function __call($method, $args)
    {
        if (in_array($method, ['get', 'post', 'put', 'patch', 'delete'])) {
            return $this->call($method, $args[0]);
        }
        throw new BadMethodCallException;
    }

    /**
     * Helper test function to build a product object.
     */
    public static function product()
    {
        $product = Mockery::mock('App\Models\Product');
        $product->shouldReceive('getAttribute')->with('variants')->andReturn(collect(func_get_args()));
        return $product;
    }

    /**
     * Helper test function to build an order object.
     */
    public static function mockOrder($attributes = [])
    {
        $order = Mockery::mock('App\Models\Order');

        foreach ($attributes as $attribute => $value) {
            if (str_contains($attribute, '()')) {
                $method = str_replace('()', '', $attribute);
                $order->shouldReceive($method)->andReturn($value);
            } else {
                $order->shouldReceive('getAttribute')->with($attribute)->andReturn($value);
            }
        }

        return $order;
    }

    /**
     * Helper test function to build a shop user object.
     */
    public static function mockShopUser($attributes = [])
    {
        $shopUser = Mockery::mock('App\Models\ShopUser');

        foreach ($attributes as $attribute => $value) {
            if (str_contains($attribute, '()')) {
                $method = str_replace('()', '', $attribute);
                $shopUser->shouldReceive($method)->andReturn($value);
            } else {
                $shopUser->shouldReceive('getAttribute')->with($attribute)->andReturn($value);
            }
        }

        return $shopUser;
    }

    /**
     * Helper test function to build a shopping cart object.
     */
    public static function mockShoppingCart($attributes = [])
    {
        $shoppingCart = Mockery::mock('App\Models\ShoppingCart');

        foreach ($attributes as $method => $value) {
            $shoppingCart->shouldReceive($method)->andReturn($value);
        }

        return $shoppingCart;
    }

    /**
     * Helper test function to mock shop setting.
     */
    public function mockShopSetting($key, $default)
    {
        $setting = Mockery::mock('App\Models\ShopSettings');
        $setting->shouldReceive('getSetting')->with($key)->andReturn($default);

        app()->instance('App\Models\ShopSettings', $setting);
    }

    /**
     * Helper test function to mock shop settings.
     */
    public function mockShopSettings($settings = [])
    {
        $setting = Mockery::mock('App\Models\ShopSettings');

        foreach ($settings as $key => $default) {
            $setting->shouldReceive('getSetting')->with($key)->andReturn($default);
        }

        app()->instance('App\Models\ShopSettings', $setting);
    }

    /**
     * Generate a random admin user object.
     */
    public function makeAdmin()
    {
        return factory(User::class)->make();
    }

    /**
     * Helper test function to mock shop setting.
     */
    public function mockShopLog($level = 'info', $message = null)
    {
        $log = Mockery::mock('App\Models\ShopLog');
        $log->shouldReceive($level)->with($message);

        app()->instance('App\Models\ShopLog', $log);
    }

    /**
     * Mock the 'unique' validation rule on form requests.
     */
    public function mockUniqueValidation()
    {
        $verifier = Mockery::mock('\Illuminate\Validation\PresenceVerifierInterface');
        $verifier->shouldReceive('getCount')->once()->andReturn(0);
        $verifier->shouldReceive('setConnection');
        Validator::setPresenceVerifier($verifier);
    }

    public function setUp()
    {
        parent::setUp();

        $this->mockShopLog();
        set_shop_database('test-shop');
    }

    public function arrayHasKeys(array $array, array $keys)
    {
        if (count($keys) !== count($array)) {
            return false;
        }

        foreach ($keys as $key) {
            if (! array_key_exists($key, $array)) {
                return false;
            }
        }

        return true;
    }

    public function tearDown()
    {
        parent::tearDown();

        //
    }
}
