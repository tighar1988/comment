@extends('admin.setup.invoices.invoices')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body stripe-charges">
                <h3>
                    Subscription Plan
                    @if ($user->trialDaysLeft() > 0)
                        <span class="label label-warning">Free Trial</span>
                    @elseif ($user->monthlyFee() > 0)
                        <span class="label label-info">${{ amount($user->monthlyFee()) }} / month</span>
                    @endif
                </h3>
                <p class="text-muted">
                    This are the charges made to your card for your subscription plan.<br/>
                </p>

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Card</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($charges->isEmpty())
                                <tr><td colspan="4">There are no charges.</td></tr>
                            @endif
                            @foreach($charges as $charge)
                                <tr>
                                    <td>${{ amount($charge->amount) }}</td>
                                    <td>{{ apply_timezone($charge->created_at) }}</td>
                                    <td>{{ $charge->cardBrand() }} {{ $charge->cardLast4() }}</td>
                                    <td>
                                        @if ($charge->status == 'failed')
                                            Failed <small class="text-muted">({{ $charge->cardError() }})</small>
                                        @else
                                            Paid
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <br/>
                {{ pagination_links($charges) }}

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection
