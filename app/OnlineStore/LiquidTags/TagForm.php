<?php

namespace App\OnlineStore\LiquidTags;

use App\OnlineStore\Vendor\Liquid\AbstractBlock;
use App\OnlineStore\Vendor\Liquid\Context;
use App\OnlineStore\CurrentView;
use App\OnlineStore\Objects\Form;

class TagForm extends AbstractBlock
{
    /**
     * Render the block.
     *
     * @param Context $context
     *
     * @return string
     */
    public function render(Context $context)
    {
        $action = '/';

        if (! empty(CurrentView::$currentPath)) {
            $action = '/' . ltrim(CurrentView::$currentPath, '/');
        }

        $formId = trim($this->markup, ' \'"');
        if ($formId == 'customer_login') {
            $action = '/customers/login';
        } elseif ($formId == 'create_customer') {
            $action = '/customers/register';
        } elseif ($formId == 'recover_customer_password') {
            $action = '/customers/recover-password';
        }

        // todo: add support for the other form types
        // 'activate_customer_password'
        // 'customer_address'
        // 'guest_login'
        // 'contact'
        // 'new_comment'
        // 'storefront_password'

        $context->set('form', new Form($formId));

        $form = '<form accept-charset="UTF-8" action="'.$action.'" method="POST">'."\n";
        $form .= '    <input name="form_type" type="hidden" value="'.$this->markup.'" />'."\n";
        $form .= '    <input name="utf8" type="hidden" value="" />'."\n";

        if ($formId == 'reset_customer_password') {
            $token = request()->route('token');
            $form .= '<input type="hidden" name="token" value="'.$token.'">';
        }

        return $form . parent::render($context);
    }

    /**
     * An action to execute when the end tag is reached
     */
    protected function endTag()
    {
        $this->nodelist[] = '</form>';
    }
}
