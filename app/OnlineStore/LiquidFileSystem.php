<?php

namespace App\OnlineStore;

use App\OnlineStore\Vendor\Liquid\LiquidException;
use App\OnlineStore\Vendor\Liquid\FileSystem;

class LiquidFileSystem implements FileSystem
{
    protected static $instance = null;

    protected $cache = [];

    protected $liquid;

    public function __construct()
    {
        $this->liquid = app('App\Models\Liquid');
    }

    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    public function getFileContents($file, $type, $theme)
    {
        if ($type != 'config' && $type != 'locales') {
            $file .= '.liquid';
        }

        if (isset($this->cache[$file])) {
            return $this->cache[$file];
        }

        $liquid = $this->liquid->file("{$file}")->type($type)->theme($theme)->first();
        if ($liquid) {
            $this->cache[$file] = $liquid->content;
            return $this->cache[$file];
        }

        if ($file == 'checkout/customer-information.liquid') {
            $path = resource_path("views/themes/common/{$type}/{$file}");
        } else {
            $path = resource_path("views/themes/{$theme}/{$type}/{$file}");
        }

        if (file_exists($path)) {
            $this->cache[$file] = file_get_contents($path);
            return $this->cache[$file];
        }

        $this->cache[$file] = '';
        return $this->cache[$file];
    }

    public function readConfigFile($file)
    {
        return $this->getFileContents($file, 'config', get_theme());
    }

    public function readLayoutFile($file)
    {
        return $this->getFileContents($file, 'layout', get_theme());
    }

    public function readLocalesFile($file)
    {
        return $this->getFileContents($file, 'locales', get_theme());
    }

    public function readSnippetsFile($file)
    {
        return $this->getFileContents($file, 'snippets', get_theme());
    }

    public function readSectionsFile($file)
    {
        return $this->getFileContents($file, 'sections', get_theme());
    }

    public function readTemplatesFile($file)
    {
        return $this->getFileContents($file, 'templates', get_theme());
    }
}
