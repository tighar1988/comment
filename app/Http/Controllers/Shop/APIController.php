<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\Post;
use App\Models\Cart;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\DeliveryMethodFormRequest;
use App\Http\Requests\WaitlistRemoveFormRequest;
use App\Models\ShopUser;
use App\Models\Coupon;
use App\Models\GiftCard;
use App\Models\Waitlist;
use App\ShoppingCart\ShoppingCart;
use App\Waitlist\ShoppingWaitlist;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;
use App\Checkout\Checkout;

class APIController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function signin(Request $request)
    {
        try {
            config(['services.facebook.client_id' => fb_app_id()]);
            config(['services.facebook.client_secret' => fb_app_secret()]);
            config(['services.facebook.app_access_token' => fb_app_access_token()]);
            $facebookUser = Socialite::driver('facebook')->userFromToken(request('accessToken'));

            $customer = ShopUser::findOrCreateUser($facebookUser);
            $token = JWTAuth::fromUser($customer);

            $cart = $customer->shoppingCart();
            $shippingPrice = $cart->shippingPrice();

            return [
                'jwt' => $token, 'email' => $customer->email, 'freeShipping' => $shippingPrice, 'vip' => 0,
            ];

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 401);
        }
    }

    public function signout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());

        return [
            'success' => 1,
        ];
    }

    public function getEmail()
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user) {

            return [
                'email' => $user->email,
            ];
        } else {
            return response()->json('error', 404);
        }
    }

    public function productsFeed(Request $request)
    {
        $data = $request->all();
        $limit = $data['limit'] ?? 10;
        $skip = $data['skip'] ?? 0;
        $posts = Post::take($limit)->skip($skip)->orderBy('created_time', 'asc')->get();
        $posts->load('product.images', 'product.inventory');

        $responseArr = [];
        foreach ($posts as $post) {
            $inventoryArray = [];
            $productQty = '';
            $data = $post->supplement;
            $data = json_decode($data);
            $inventories = $post->product->inventory;

            foreach ($inventories as $inventory) {
                $productQty += intval($inventory->quantity);
                $inventoryArray[] = [
                    "price"        => $inventory->price,
                    "inventory_id" => $inventory->id,
                    "quantity"     => $inventory->quantity,
                    "color"        => $inventory->color,
                    "size"         => $inventory->size,
                ];
            }
            $response = [
                "product_id"   => $post->prod_id ?? null,
                "post_id"      => $post->id ?? null,
                "created_at"   => $post->created_time ?? null,
                "product_name" => $post->product->product_name ?? null,
                "description"  => $data->message ?? null,
                "quantity"     => $productQty,
                "style"        => $post->product->style ?? null,
                "filename"     => $post->product->image ? product_image($post->product->image->filename) : null,
                "inventory"    => $inventoryArray,
            ];

            $responseArr[] = $response;
        }

        return $responseArr;
    }

    public function product($shop, $productId)
    {
        $posts = Post::where('prod_id', $productId)->get();

        $posts->load(['comments' => function ($query) {
            $query->where('is_order', 1)->orderBy('stamped', 'asc');
        }, 'product.images', 'product.inventory']);

        $responseArr = [];

        foreach ($posts as $post) {
            $inventoryArray = [];
            $commentsArray = [];
            $productQty = '';
            $data = $post->supplement;
            $data = json_decode($data);

            foreach ($post->product->inventory as $inventory) {
                $productQty += intval($inventory->quantity);
                $inventoryArray[] = [
                    "price"        => $inventory->price,
                    "inventory_id" => $inventory->id,
                    "quantity"     => $inventory->quantity,
                    "color"        => $inventory->color,
                    "size"         => $inventory->size,
                ];
            }

            foreach ($post->comments as $comment) {
                $commentsArray[] = [
                    "com_id"  => $comment->id,
                    "user_id" => $comment->user_id,
                    "content" => $comment->content,
                ];
            }

            $response = [
                "product_id"   => $post->prod_id ?? null,
                "product_name" => $post->product->product_name ?? null,
                "description"  => $data->message ?? null,
                "quantity"     => $productQty,
                "style"        => $post->product->style ?? null,
                "filename"     => $post->product->image ? product_image($post->product->image->filename) : null,
                "inventory"    => $inventoryArray,
                "comments"     => $commentsArray,
            ];

            $responseArr[] = $response;
        }

        return $responseArr;
    }

    public function productsSearch(Request $request)
    {
        $data = $request->all();
        $limit = $data['limit'] ?? 10;
        $inventories = Inventory::take($limit)->skip($data['skip'])
            ->where([(strtolower($data['instock']) == 'true' ? ['quantity', '>', 0] : ['quantity', '>=', 0]), ['size', '=', strtolower($data['size'])]])->get();
        $filteredProducts = [];
        foreach ($inventories as $inventory) {
            $filteredProducts[] = [
                'product_name' => $inventory->product->product_name ?? null,
                'product_id'   => $inventory->product_id ?? null,
                'quantity'     => $inventory->quantity ?? null,
                'price'        => $inventory->price ?? null,
                'style'        => $inventory->product->style ?? null,
                "filename"     => $inventory->product->image ? product_image($inventory->product->image->filename) : null,
            ];
        }

        return $filteredProducts;
    }

    public function cartProducts(Request $request)
    {
        $data = $request->all();
        try {
            if (isset($data['inventory_id'])) {
                $customer = $user = JWTAuth::parseToken()->authenticate();
                $variant = Inventory::findOrFail($data['inventory_id']);
                if ($variant->quantity > 0) {
                    Cart::mobileAppAddItem($customer, $variant);

                    return response()->json(['success' => true, 'message' => 'added to Cart']);
                } else {
                    Waitlist::mobileAddItem($customer, $variant);

                    return response()->json(['success' => true, 'message' => 'added to Waitlist']);
                }

            }
        } catch (Exception $e) {

            return response()->json(['status' => 0, 'message' => 'Error, product does  not exist, Please verify your data!']);
        }
    }

    public function cart()
    {
        $customer = JWTAuth::parseToken()->authenticate();
        $waitlistCount = $customer->newWaitlistCount();
        $customer->setOption('last-visit-time', time());
        $customer->save();
        $cart = $customer->shoppingCart();
        $localPickupEnabled = local_pickup_enabled();
        $cartItems = [];
        foreach ($cart->items as $item) {
            $cartItems[] = [
                "cart_id"      => $item->id,
                "product_id"   => $item->product_id ?? null,
                "inventory_id" => $item->inventory_id ?? null,
                "created_at"   => $item->created_at ?? null,
                "price"        => $item->price ?? null,
                "product_name" => $item->name ?? null,
                "color"        => $item->color ?? null,
                "size"         => $item->size,
                "com_id"       => $item->comment_id ?? null,
                "style"        => $item->product_style ?? null,
                "order_source" => $item->order_source ?? null,
                "filename"     => $item->image ? product_image($item->image) : null,
                'waitlist'     => $waitlistCount ?? null,
            ];
        }

        return [
            'is_local'        => $localPickupEnabled ? true : false,
            'street'          => $customer->street_address ?? null,
            'apt'             => $customer->apartment ?? null,
            'city'            => $customer->city ?? null,
            'state'           => $customer->state ?? null,
            'zip'             => $customer->zip ?? null,
            'email'           => $customer->email ?? null,
            'ship_charged'    => $cart->shipping_price ?? null,
            'subtotal'        => $cart->subtotal ?? null,
            'facebook_id'     => $customer->facebook_id ?? null,
            'state_tax'       => $cart->state_tax ?? null,
            "municipal_tax"   => $cart->municipal_tax ?? null,
            "county_tax"      => $cart->county_tax ?? null,
            "tax_total"       => $cart->tax_total,
            "coupon"          => isset($cart->coupon_discount) ? true : false,
            "coupon_discount" => $cart->coupon_discount,
            "products"        => $cartItems,
        ];
    }

    public function getCards()
    {
        $customer = JWTAuth::parseToken()->authenticate();

        return ['cards' => $customer->cards()];
    }

    public function addCard()
    {
        try {
            $customer = JWTAuth::parseToken()->authenticate();
            $customer->addNewCard(request('id'));

            return ['success' => 1];

        } catch (Exception $e) {
            return response()->json(['status' => 0]);
        }
    }

    public function removeCard($shop, $id)
    {
        try {
            $card = Card::findOrFail($id);
            $user = JWTAuth::parseToken()->authenticate();
            if ($user->id !== $card->user_id) {
                throw new Exception("Card does not belong to user");
            }

            $card->delete();

            return [
                'success' => "Card deleted",
            ];

        } catch (Exception $e) {

            return response()->json(['status' => 0, 'message' => 'Error, card could not be deleted. Please verify your data!']);
        }
    }

    public function updateCard(Request $request, $shop, $id)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;
            $card = Card::where([['user_id', $user->id]])->findOrFail($id);
            $defaultCard = $card->default;

            if (strtolower($request->default) == 'true') {
                $defaultCard = true;
                Card::where('user_id', $userId)->update(['default' => false]);
            }
            if (strtolower($request->default) == 'false') {
                $defaultCard = false;
            }
            $card->update([
                'card_id'   => $request->card_id,
                'brand'     => $request->brand,
                'last_four' => $request->last_four,
                'exp_month' => $request->exp_month,
                'exp_year'  => $request->exp_year,
                'default'   => $defaultCard,
            ]);

            return [
                'success' => "Card updated",
            ];
        } catch (Exception $e) {
            return response()->json(['status' => 0, 'message' => 'Error, card could not be updated. Please verify your data!']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkoutWithExistingCard(Request $request)
    {
        try {
            $customer = JWTAuth::parseToken()->authenticate();
            $customer->checkout()->withExistingCard(request('card_id'));

            return response()->json(['status' => 1]);

        } catch (Exception $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkoutPaypal()
    {
        try {
            $paymentId = request('paymentId');
            $customer = JWTAuth::parseToken()->authenticate();
            $checkoutObj = New Checkout($customer);;
            $checkoutObj->checkoutWithPayPalMobile($paymentId, $customer);

            return response('Success', 200);

        } catch (Exception $e) {
            return response()->json(['status' => 404, 'message' => $e->getMessage()]);
        }
    }

    public function checkoutWithAccountCredit()
    {
        try {
            $customer = JWTAuth::parseToken()->authenticate();
            $customer->checkout()->withAccountCredit();
            ShoppingCart::setApplyBalanceMobile($customer, false);

            return response()->json(['status' => 1]);

        } catch (Exception $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @return array
     */
    public function getBalance()
    {
        $customer = JWTAuth::parseToken()->authenticate();

        return [
            'balance' => $customer->balance . "$",
        ];
    }

    public function getAddress($userId)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $userAddress = [
            "street"       => $user->street_address ?? null,
            "apartment"    => $user->apartment ?? null,
            "city"         => $user->city ?? null,
            "state"        => $user->state ?? null,
            "zip"          => $user->zip ?? null,
            "country code" => $user->country_code ?? null,
            "name"         => $user->name ?? null,
        ];

        return $userAddress;
    }

    public function updateEmail(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user['email'] = trim(request('email'));
        $user->save();
        if ($user->save()) {
            return [

                'success' => "User email updated",
            ];
        } else {

            return response()->json('error', 404);
        }
    }

    public function userEmail(Request $request)
    {
        $user = User::findOrFail(request('user'));
        if ($request->type == 'GET') {
            return response()->json(['email' => trim($user->email)]);
        } else {
            $user->email = $request->email;
            $user->save();

            return response()->json(['success' => true]);
        }
        log_info("Customer with fbid {$user->facebook_id} ({$user->name}) updated email to {$user->email}");

        return response()->json(['success' => true]);
    }

    public function setAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'street'       => 'required|max:255',
            'city'         => 'required|max:255',
            'country_code' => 'required|in:US,CA',
            'state'        => 'required|max:100',
            'zip'          => 'required|max:100',
            'apartment'    => 'nullable|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json('error', 404);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user['street_address'] = $request->street;
        $user['state'] = $request->state;
        $user['zip'] = $request->zip;
        $user['apartment'] = $request->apt ?? null;
        $user['city'] = $request->city ?? null;
        $user['country_code'] = $request->country_code ?? null;
        if ($user->save()) {
            return response()->json(['success' => "Address setted successfully"]);
        } else {
            return response()->json('error', 404);
        }
    }

    public function setDeliveryMethod(DeliveryMethodFormRequest $request)
    {
        if ($method = $request->input('method')) {
            $user = JWTAuth::parseToken()->authenticate();
            $user->local_pickup = $method == 'Local' ? true : false;

            $user->save();

            return response()->json(['status' => 'Delivery method updated to ' . $method]);
        } else {
            return response()->json(['status' => 422]);
        }
    }

    public function applyCoupon()
    {
        $couponCode = request('couponId');
        if (! empty($couponCode)) {
            $customer = JWTAuth::parseToken()->authenticate();
            $coupon = Coupon::getCoupon($couponCode);

            if ($coupon && $coupon->isValid($customer, ShoppingCart::subtotalFor($customer))) {
                ShoppingCart::setApplyCouponMobile($customer, $coupon->id);
                return response()->json(['status' => 'Coupon applied successfully']);
            } else {
                return response()->json(['error' => 'Enter valid coupon']);
            }

        } else {
            return response()->json(['status' => 'Enter coupon']);
        }
    }

    public function applyGiftCard(Request $request)
    {
        $customer = JWTAuth::parseToken()->authenticate();
        $code = str_replace(' ', '', request('code'));
        if (empty($code)) {
            return response()->json(['status' => 'Provide code']);
        }

        $giftcard = GiftCard::code($code)->first();
        if (is_null($giftcard) || !$giftcard->isValid()) {
            return response()->json(['status' => 0]);
        }

        $giftcard->redeemed_at = time();
        $giftcard->redeemed_by_customer_id = $customer->id;
        $giftcard->used = true;
        $giftcard->save();

        $customer->addBalance($giftcard->value, "Customer redeemed \$" . amount($giftcard->value) . " giftcard. Giftcard #{$giftcard->id}");

        return response()->json(['status' => 1]);
    }

    public function applyBalance()
    {
        $customer = JWTAuth::parseToken()->authenticate();
        if (!$customer->hasBalance()) {
            return response()->json(['status' => 0, 'message' => "You don't have any credit in your account!"]);
        }
        ShoppingCart::setApplyBalanceMobile($customer, true);
        $cart = $customer->shoppingCart();
        $cart->applyBalance();

        if ($cart->isFullyPaidWithBalance) {
            return response()->json(['status' => 1, 'message' => 'Credit applied - thanks!']);

        } else {
            $message = 'Your credit has been applied, your remaining balance due is $' . amount($cart->total - $cart->deduced_balance) . ' - Please note you still need to checkout and pay the remaining balance';

            return response()->json(['status' => 1, 'message' => $message]);
        }
    }

    public function waitlist()
    {
        $customer = JWTAuth::parseToken()->authenticate();
        $waitlists = $customer->waitlist();

        $responseArr = [];
        $waitlists = $waitlists->items;

        foreach ($waitlists as $waitlist) {
            $currentWaitlist = Waitlist::findOrFail($waitlist->id);
            $product_id = $currentWaitlist->product_id;
            $response = [
                "product_id"   => $product_id,
                "product_name" => $waitlist->name ?? null,
                "price"        => $waitlist->price ?? null,
                "color"        => $waitlist->color ?? null,
                "size"         => $waitlist->size ?? null,
                "filename"     => $waitlist->image ? product_image($waitlist->image) : null,
                "created_at"   => $waitlist->created_at ?? null,
            ];
            $responseArr[] = $response;
        }

        return $responseArr;
    }

    public function removeFromWaitlist(WaitlistRemoveFormRequest $request)
    {
        ShoppingWaitlist::removeItem(request('item_id'));

        return response()->json(['status' => 1]);
    }

    public function paidOrders()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $orders = $user->ordersPaid;

        return [
            'paid-orders' => $orders,
        ];
    }

    public function orders(Request $request)
    {
        $data = $request->all();
        $user = JWTAuth::parseToken()->authenticate();
        $skip = $data['skip'] ?? 0;
        $orders = Order::take(10)->skip($skip)->where('customer_id', $user->id)->get();
        $orders = $orders->load('orderProducts.product', 'orderProducts.variant');
        $responseArr = [];

        if ($orders) {
            $orderStatus = [
                Order::STATUS_PAID       => 'Paid',
                Order::STATUS_FULFILLED  => 'FULFILLED',
                Order::STATUS_RETURNED   => 'RETURNED',
                Order::STATUS_PROCESSING => 'PROCESSING',
                Order::STATUS_PRINTED    => 'PRINTED',
            ];

            foreach ($orders as $order) {
                $cartItems = [];
                foreach ($order->orderProducts as $product) {
                    $items = [
                        'product_id'   => $product->product_id ?? null,
                        'product_name' => $product->prod_name ?? null,
                        'size'         => $product->size ?? null,
                        'color'        => $product->color ?? null,
                        'price'        => $product->price ?? null,
                        'filename'     => $product->product_filename ? product_image($product->product_filename) : null,
                        'description'  => $product->product_description ?? null,

                    ];
                    $cartItems[] = $items;
                }

                $cartDetails = [
                    'status'        => $orderStatus[$order->order_status] ?? null,
                    'order_id'      => $order->id ?? null,
                    'date'          => Carbon::parse($order->created_at)->timestamp ?? null,
                    'subtotal'      => $order->subtotal ?? null,
                    'tax_total'     => $order->tax_total ?? null,
                    'ship_charged'  => $order->ship_chanreged ?? null,
                    'apply_balance' => $order->apply_balance ?? null,
                    'apartment'     => $order->apartment ?? null,
                    'street'        => $order->street_address ?? null,
                    'state'         => $order->state ?? null,
                    'city'          => $order->city ?? null,
                    'country_code'  => $order->country_code ?? null,
                    'products'      => $cartItems,
                ];

                $responseArr[] = $cartDetails;
            }

            return $responseArr;
        }
    }

    public function fulfilledOrders(Request $request)
    {
        $data = $request->all();
        $limit = $data['limit'] ?? 10;
        $skip = $data['skip'] ?? 0;

        $orders = Order::search(request('q'), request('order'))
            ->fulfilled()->with('customer', 'orderProducts')->take($limit)->skip($skip)->get();

        return [
            'orders' => $orders,
        ];
    }

    public function removeFromCart($shop, $cartId)
    {
        $customer = JWTAuth::parseToken()->authenticate();

        // validation that the item id belongs to the current user
        $item = Cart::findOrFail($cartId);
        if ($customer->id != $item->user_id) {
            return response()->json('Unauthorized', 401);
        }

        $message = "[mobile-app] Customer #{$customer->id} removed item from shopping cart. Adding back to inventory.";
        Cart::removeItem($item->id, $message);

        return response()->json(['status' => 1]);
    }

    public function messengerId(Request $request)
    {
        $customer = $request->user();

        return response()->json(['messenger_id' => $customer->messenger_id]);
    }

    public function stripeCustomerId()
    {
        $customer = JWTAuth::parseToken()->authenticate();
        if (!$customer) {
            return response()->json('error', 404);
        }

        return response()->json(['customerID' => $customer->stripe_id]);
    }
}
