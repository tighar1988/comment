@include('admin.customers.add-to-cart-modal')

<h3 style="padding-left: 10px;">Shopping Cart <button style="margin-right: 15px;" class="pull-right btn btn-success btn-sm add-to-cart-btn">Add Product To Cart</button></h3>

@if (count($cart->items))

    @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
    <form method="POST" action="/admin/customers/{{ $customer->id }}/create-and-confirm-order">
        <button type="submit" onclick="return confirm('Do you really want to create the order? It will mark the order as paid.');" style="margin-right: 15px;" class="pull-right btn btn-sm btn-danger">Confirm & Create this order</button>
    </form>
    @endif

    <div class="row">
        <div class="col-md-2 col-md-offset-1">
            <div>Subtotal: ${{ amount($cart->subtotal) }}</div>
            <div>Shipping price: <span>{{ $cart->shipping_price == 0 ? 'Free Shipping!' : '$'.amount($cart->shipping_price) }}</span></div>
            <div>Total taxes: <span>${{ amount($cart->tax_total) }}</span></div>
            @if ($cart->coupon_discount)
                <div>Coupon Discount: <span>- ${{ amount($cart->coupon_discount) }}</span></div>
            @endif
            @if ($cart->deduced_balance)
                Account Credit: <span>- ${{ amount($cart->deduced_balance) }}</span>
            @endif
            <div>Total order: <strong>${{ amount($cart->total) }}</strong></div>
        </div>
        <div class="col-md-2">
            @if (local_pickup_enabled() && $customer->local_pickup)
                Shipping Method: <strong>Local Pickup</strong>
            @else
                <span class="value align-right"><strong>Shipping to:</strong> <br/>
                    @if ($customer->street_address)
                        {{ $customer->street_address}}, <br/>
                        {{ ($customer->apartment != '') ? $customer->apartment . ", " : '' }}
                        {{ $customer->city }}, {{ $customer->state }}, {{ $customer->zip }}
                    @else
                        We do not have {{ $customer->name }}'s address!
                    @endif
                </span>
            @endif
        </div>
    </div>
    <br/>
@endif

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Image</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Color</th>
                <th>Size</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            @if (count($cart->items))
                @foreach ($cart->items as $item)
                    <tr>
                        <td><img width="125px" height="125px" src="{{ product_thumb($item->image) }}" alt="{{ $item->name }}"></td>
                        <td>{{ $item->name }}</td>
                        <td>${{ amount($item->price) }}</td>
                        <td>{{ $item->color }}</td>
                        <td>{{ $item->size }}</td>
                        <td class="text-center">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-ellipsis-h fa-lg"></i>
                                </a>
                                <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                    <li><a href="/admin/customers/remove-from-cart/{{ $item->id }}" class="link" onclick="return confirm_delete(this);">Remove from Cart</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="6">The customer has no products in his shopping cart at the moment.</td></tr>
            @endif
        </tbody>
    </table>
</div>
