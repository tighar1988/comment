<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Image as ImageModel;
use Image;
use Storage;
use DB;

class UpdateImagesProportions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:update-proportions {shop}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the width and height for exising images.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shop = $this->argument('shop');
        $this->info("Updating image proportions for shop {$shop}..");

        set_shop_database($shop);
        DB::setDefaultConnection('shop');
        DB::reconnect();

        $bar = $this->output->createProgressBar(ImageModel::count());
        $shop = shop_id();

        ImageModel::orderBy('id')->chunk(100, function ($images) use ($bar, $shop) {
            foreach ($images as $image) {
                if (! str_contains($image->filename, ['.shopify.com', '.bigcommerce.com']) && (is_null($image->image_width) || is_null($image->image_height))) {

                    // fetch the image
                    $contents = Storage::get('/'.$shop.'/products/'.$image->filename);
                    $img = Image::make($contents);

                    // update the image proportions
                    $image->image_width = $img->width();
                    $image->image_height = $img->height();
                    $image->save();
                }

                $bar->advance();
            }
        });

        $bar->finish();
        $this->info("Image proportions update complete..");
    }
}
