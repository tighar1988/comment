@if (count($orders))
    @include('shop.orders-list')
@else
    You have no fulfilled orders at the moment.
@endif
