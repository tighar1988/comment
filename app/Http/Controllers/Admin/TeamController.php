<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Models\User;
use App\Models\Helpers\Permissions;
use Auth;

class TeamController extends Controller
{
    public function index()
    {
        $permissions = Permissions::list();
        $users = Auth::user()->getTeam();
        return view('admin.team.admin-grid', compact('users', 'permissions'));
    }

    public function create()
    {
        $permissions = Permissions::list();
        return view('admin.team.admin-form', compact('permissions'));
    }

    public function store(UserFormRequest $request)
    {
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->shop_name = Auth::user()->shop_name;
        $user->superadmin = false; // only the shop owner can be a superadmin

        if ($request->has('all_permissions')) {
            $user->permissions = ['all-permissions' => true];
        } else {
            $acl = [];
            foreach (Permissions::permissionKeys() as $permission) {
                $acl[$permission] = $request->has($permission) ? true : false;
            }
            $user->permissions = $acl;
        }

        $user->save();

        flash('Team member added!');
        return redirect('/admin/team');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $permissions = Permissions::list();
        return view('admin.team.admin-form', compact('user', 'permissions'));
    }

    public function update($id, UserFormRequest $request)
    {
        // the superadmin can update only people from his team
        $users = Auth::user()->getTeam();
        $toUpdate = $users->where('id', $id)->first();

        $toUpdate->name = $request->input('name');
        $toUpdate->email = $request->input('email');

        if (! empty($request->input('password'))) {
            $toUpdate->password = bcrypt($request->input('password'));
        }

        if ($request->has('all_permissions')) {
            $toUpdate->permissions = ['all-permissions' => true];
        } else {
            $acl = [];
            foreach (Permissions::permissionKeys() as $permission) {
                $acl[$permission] = $request->has($permission) ? true : false;
            }
            $toUpdate->permissions = $acl;
        }

        $toUpdate->save();

        flash('Team member updated!');
        return redirect('/admin/team');
    }

    public function destroy($id)
    {
        // the superadmin can't delete his own account, or people who are not on his team
        $users = Auth::user()->getTeam();
        $toDelete = $users->where('id', $id)->first();

        User::destroy($toDelete->id);
        flash('The member account was deleted!');
        return redirect('/admin/team');
    }
}
