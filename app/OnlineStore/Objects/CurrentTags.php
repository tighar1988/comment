<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class CurrentTags extends Drop
{
    /**
     * Tags inside the current_tags array will always display in alphabetical order.
     * It is not possible to manually change the order.
     */
    public function toLiquid()
    {
        if (! isset($this->cache['current_tags'])) {
            // todo: sort
            $this->cache['current_tags'] = [];
            // $this->cache['current_tags'] = ['blue', 'solid'];
        }

        return $this->cache['current_tags'];
    }
}
