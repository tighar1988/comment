<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use PDOException;
use Config;
use DB;

class DatabaseClearFailedJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:clear-failed-jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleare the failed_jobs table on all shops.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Clearing 'failed_jobs' for all shops..");

        // cleare failed_jobs for the master db
        $this->info("Clearing 'failed_jobs' for database: commentsold");
        DB::connection('master')->table('failed_jobs')->truncate();

        foreach (all_shops() as $shop) {
            try {
                $this->info("Clearing 'failed_jobs' for database: {$shop}");

                set_shop_database($shop);
                DB::setDefaultConnection('shop');
                DB::reconnect();

                DB::connection('shop')->table('failed_jobs')->truncate();

            } catch (QueryException $e) {
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                $this->error($e->getMessage());
            }
        }

        $this->info("Clear complete..");
    }
}
