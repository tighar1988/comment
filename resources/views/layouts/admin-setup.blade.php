<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" / >
    <title>Commentsold</title>

    <link rel="stylesheet" href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ cdn('/css/bootstrap.min.css') }}" type="text/css" />

    <style>
        body {
            background-color: #f4efe5;
            color: #282c45;
        }
        .title-text {
            margin-top: 40px;
        }
        .title-text h3 {
            font-weight: bold;
        }
        .wrapper {
            padding: 40px 10px;
            margin-top: 20px;
        }
        .btn {
            margin-top: 20px;
        }
        form {
            margin-top: 20px;
        }
        li {
            text-align: center;
        }
        li.active {
            color: #fff;
            background-color: #337ab7;
            padding: 10px 15px;
        }
    </style>
</head>
<body>
    <div class="container">
        @if (! isset($hideSteps))
        <div class="row">
            <div class="title-text col-md-6 col-md-offset-3 text-center">
                <h2>Commentsold</h2>
                <h3>Just a quick thing to set up your account...</h3>
                <br/>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6 col-md-offset-3">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                    <li class="{{ activeLink('admin/setup/step-1') }}">
                        <h4 class="list-group-item-heading">Step 1</h4>
                        <p class="list-group-item-text">Choose Subdomain</p>
                    </li>
                    <li class="{{ activeLink('admin/setup/step-2') }}">
                        <h4 class="list-group-item-heading">Step 2</h4>
                        <p class="list-group-item-text">Complete Details</p>
                    </li>
                </ul>
            </div>
        </div>
        @endif
        @yield('content')
    </div>

    @include('common.logged-as-user')
    <script type="text/javascript" src="{{ cdn('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/bootstrap.min.js') }}"></script>
    @include('common.admin-intercom')
    {{ script_raven() }}
    @yield('scripts')
</body>
</html>
