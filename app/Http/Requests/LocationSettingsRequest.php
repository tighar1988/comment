<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location'  => 'required|max:255',
            'state'     => 'required|max:255',
            'zip_code'  => 'required|exists:master.tax_rates,zip_code',
            'address'   => 'required|max:100',
        ];
    }
}
