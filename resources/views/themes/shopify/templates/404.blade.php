<div class="page-width">
  <div class="empty-page-content text-center">
    <h1>404 Page Not Found</h1>
    <p>The page you requested does not exist.</p>
    <p>
      <a href="/home" class="btn btn--has-icon-after">Continue shopping{% include 'icon-arrow-right' %}</a>
    </p>
  </div>
</div>
