@extends('admin.setup.setup')

@section('section', 'Embed')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form class="form" id="embed-code-form" role="form" method="POST" action="{{ url('/admin/setup/embed/update') }}">
                    <span class="settings-info">Embed Code</span>
                    <span class="settings-muted text-muted">
                        You can embed any custom code on your shop page. For example, you can add your Google Analytics, Facebook Pixel or Zendesk. This will be added at the bottom of the page.
                    </span>

                    <div class="form-group @hasError('embed_code')">
                        <label class="control-label">Code</label>
                        <textarea class="form-control" rows="10" id="embed-code" name="embed_code">{{ old('embed_code', shop_setting('embed-code')) }}</textarea>
                        @error('embed_code')
                        <span class="help-block"><strong></strong></span>
                    </div>

                    <button class="btn btn-success" type="submit">Update Code</button>
                </form>

            </div>
        </div>
    </div>
</div>
<div style="display: none;" id="validate-code"></div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#embed-code-form').submit(function() {
            try {
                var code = $('#embed-code').val();

                // validate for js errors; a js error will be thrown if invalid
                $('#validate-code').html(code);

                // validate that the nr of open tags is the same as the nr of closed tags
                checkTags(code, ['script', 'span', 'div', 'strong']);
                return true;

            } catch(e) {
                $('.form-group', $(this)).addClass('has-error');
                $('.help-block strong', $(this)).html('There is a error in your code. Please fix your code and try again.');
                return false;
            }
        });
    });

    function checkTags(code, tags) {
        for (var i = 0; i < tags.length; i++) {
            var nrOpen = (code.match(new RegExp('<' + tags[i] + '>', 'gi')) || []).length;
            var nrClosed = (code.match(new RegExp('<\/' + tags[i] + '>', 'gi')) || []).length;

            if (nrOpen != nrClosed) {
                throw new Error('Tags mismatch');
            }
        }
    }
</script>
@endsection
