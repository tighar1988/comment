<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;

class Section extends Drop
{
    protected $sectionName;

    protected $schema;

    public function __construct($sectionName)
    {
        $this->sectionName = $sectionName;

        if (isset(CurrentView::$schemas[$sectionName])) {
            $this->schema = CurrentView::$schemas[$sectionName];
        }
    }

    /**
     * For static sections, returns the section's file name without ".liquid".
     * For dynamic sections, returns a dynamically generated ID.
     */
    public function id()
    {
        // todo: add support for dynamic sections
        return $this->sectionName;
    }

    /**
     * Returns an object of the section settings set in the theme editor.
     * Retrieve setting values by referencing the setting's unique id.
     *
     * Input
     * <h2>{{ section.settings.heading }}</h2>
     * <a href="{{ section.settings.featured_collection.url }}">This week's best selling items</a>
     *
     * Output
     * <h2>Weekly promotion</h2>
     * <a href="/collections/new-this-week">This week's best selling items</a>
     */
    public function settings()
    {
        if (isset($this->schema->settings)) {
            return new SchemaSettings($this->schema->settings, $this->schema, $this->sectionName);
        }

        return null;
    }

    /**
     * Returns an array of the section's blocks.
     */
    public function blocks()
    {
        $blocks = [];

        if (isset(CurrentView::$settingsData['current']['sections'][$this->sectionName]['blocks'])) {
            foreach (CurrentView::$settingsData['current']['sections'][$this->sectionName]['blocks'] as $blockId => $block) {
                $blocks[] = new Block($blockId, $block);
            }
        }

        // the blocks in the section file
        // if (isset($this->schema->blocks)) {
        //     foreach ((array)$this->schema->blocks as $block) {
        //         $blocks[] = new Block($block, $this->schema);
        //     }
        // }

        return $blocks;
    }
}
