<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Instagram\Instagram;
use App\Jobs\Traits\ReconnectShopDB;
use App\Models\InstagramPost;
use App\Models\Master;
use App\Models\Product;
use Exception;
use Log;
use DB;
use Redis;

class HandleInstagramWebhook implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    /**
     * The $_REQUEST array from the web request
     *
     * @var array
     */
    protected $request;

    /**
     * Create a new job instance.
     *
     * @param array $request
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Handle the incoming Instagram webhook
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->request as $entry) {

            if (! isset($entry['object_id'])) {
                Log::info("[instagram][webhook] Unknown user id: " . json_encode($entry));
                continue;
            }

            $shop = Redis::connection('cache')->get("instagramId:{$entry['object_id']}");
            if (empty($shop)) {
                Log::info("[instagram][webhook] Could not map object_id to shop db: " . json_encode($entry));
                continue;
            }

            $this->reconnectDB($shop);

            if ($this->isNewPost($entry)) {
                $this->handleNewPost($entry);

            } else {
                log_error("[instagram][webhook] Received unknown request: " . json_encode($entry));
                continue;
            }
        }
    }

    public function isNewPost($entry)
    {
        if (! isset($entry['object']) || ! isset($entry['changed_aspect'])) {
            return false;
        }

        if ($entry['object'] != 'user' || $entry['changed_aspect'] != 'media') {
            return false;
        }

        return true;
    }

    /**
     * Handle when the user makes a new Instagram post.
     */
    public function handleNewPost($entry)
    {
        $instagramId = $entry['object_id'];
        $mediaId = $entry['data']['media_id'] ?? null;

        if (empty($mediaId)) {
            log_info("[instagram][webhook] Empty media id. Exiting..");
            return;
        }

        $post = (new Instagram)->mediaById($mediaId);
        if (! isset($post['id'])) {
            log_info("[instagram][webhook] Post id missing. Exiting..");
            return;
        }

        $alreadyLinked = InstagramPost::where('instagram_id', '=', $post['id'])->first();
        if ($alreadyLinked) {
            log_info("[instagram][webhook] Post #{$post['id']} already linked. Exiting..");
            return;
        }

        $caption = $post['caption']['text'] ?? null;
        if (empty($caption)) {
            log_info("[instagram][webhook] Post #{$post['id']} has no caption. Exiting..");
            return;
        }

        $products = Product::pluck('style', 'id');
        foreach ($products as $productId => $sku) {
            if (empty($sku)) {
                continue;
            }

            if (! $this->captionContainsSKU($caption, $sku)) {
                continue;
            }

            InstagramPost::create([
                'product_id'   => $productId,
                'instagram_id' => $post['id'],
                'created_time' => $post['created_time'] ?? null,
                'image'        => $post['images']['thumbnail']['url'] ?? null,
                'post_data'    => $post,
            ]);

            log_info("[instagram][webhook] The post #{$post['id']} was linked to the product #{$productId}, sku {$sku}.");
            return;
        }

        log_info("[instagram][webhook] We could not find a product to link for post #{$post['id']}.");
    }

    public function captionContainsSKU($caption, $sku)
    {
        $sku = preg_quote($sku, '/');

        // starts with sku followed by space
        if (preg_match('/^#'.$sku.'[\s\.,]/i', $caption)) {
            return true;
        }

        // contains sku
        if (preg_match('/[\s\.,]#'.$sku.'[\s\.,]/i', $caption)) {
            return true;
        }

        // ends with space then sku
        if (preg_match('/[\s\.,]#'.$sku.'$/i', $caption)) {
            return true;
        }

        return false;
    }
}
