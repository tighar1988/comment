<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Product;

class ProductTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Product::class, new Product);
    }

    /** @test */
    public function it_suggests_sku1_for_the_first_product()
    {
        $lastSKU = null;
        $this->assertEquals(Product::incrementSKU($lastSKU), 'sku1');
    }

    /** @test */
    public function it_increments_sku1_to_sku2()
    {
        $this->assertEquals(Product::incrementSKU('sku1'), 'sku2');
    }

    /** @test */
    public function it_increments_uppercase_sku1_to_sku2()
    {
        $this->assertEquals(Product::incrementSKU('SKU1'), 'sku2');
    }

    /** @test */
    public function it_increments_sku2_to_sku3()
    {
        $this->assertEquals(Product::incrementSKU('sku2'), 'sku3');
    }

    /** @test */
    public function it_increments_sku9_to_sku10()
    {
        $this->assertEquals(Product::incrementSKU('sku9'), 'sku10');
    }

    /** @test */
    public function it_increments_sku10_to_sku11()
    {
        $this->assertEquals(Product::incrementSKU('sku10'), 'sku11');
    }

    /** @test */
    public function it_increments_sku20_to_sku21()
    {
        $this->assertEquals(Product::incrementSKU('sku20'), 'sku21');
    }

    /** @test */
    public function it_casts_attributes_to_boolean()
    {
        $product = new Product;
        $product->charge_taxes = 1;
        $product->publish_product = 1;

        $this->assertSame(true, $product->charge_taxes);
        $this->assertSame(true, $product->publish_product);
    }

    /** @test */
    public function is_gift_card()
    {
        $product = new Product;
        $product->product_type = 'giftcard';
        $this->assertTrue($product->isGiftCard());

        $product = new Product;
        $product->product_type = null;
        $this->assertFalse($product->isGiftCard());
    }
}
