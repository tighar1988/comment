<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class Article extends Drop
{
    // article.author
    // article.comments
    // article.comments_count
    // article.comments_enabled
    // article.comment_post_url
    // article.content
    // article.created_at
    // article.excerpt
    // article.excerpt_or_content
    // article.id
    // article.handle
    // article.image
    // article.image.src
    // article.moderated
    // article.published_at
    // article.tags
    // article.title
    // article.url
    // article.user.account_owner
    // article.user.bio
    // article.user.email
    // article.user.first_name
    // article.user.last_name
    // article.user.homepage
}
