<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Post;
use App\Models\PayPalComissionCharge;
use Auth;
use App\Http\Requests\Admin\ShopNameFormRequest;
use App\Http\Requests\Admin\ShopDescriptionFormRequest;
use App\Http\Requests\Admin\ExpireFormRequest;
use App\Http\Requests\Admin\RemindFormRequest;
use App\Http\Requests\Admin\PasswordFormRequest;
use App\Http\Requests\Admin\PaypalApiKeysFormRequest;
use App\Http\Requests\Admin\ShippingCostFormRequest;
use App\Http\Requests\Admin\VariableShippingCostFormRequest;
use App\Http\Requests\Admin\ShopAddressFormRequest;
use App\Http\Requests\Admin\TimezoneFormRequest;
use App\Http\Requests\Admin\GoShippoApiTokenFormRequest;
use App\Http\Requests\Admin\TemplateFormRequest;
use App\Http\Requests\Admin\RepostMinQuantityFormRequest;
use App\Http\Requests\Admin\DaysSinceLastPostFormRequest;
use App\Http\Requests\Admin\ToggleSwitchFormRequest;
use App\Http\Requests\Admin\PrepaidCreditFormRequest;
use App\Http\Requests\Admin\ConnectShopifyFromRequest;
use App\Http\Requests\Admin\FreeShippingMaximumFormRequest;
use App\Models\ShopUser;
use App\Models\ShopSettings;
use App\Models\StripeCharges;
use App\Facebook\Template;
use App\Mails\ShopMailer;
use Facebook;
use Exception;

class SettingsController extends Controller
{
    public function shop()
    {
        return view('admin.setup.shop');
    }

    public function shipping()
    {
        return view('admin.setup.shipping.shipping-cost');
    }

    public function prepayLabels()
    {
        return view('admin.setup.shipping.prepay-labels');
    }

    public function account()
    {
        $user = request()->user();
        return view('admin.setup.account', compact('user'));
    }

    public function billing()
    {
        $user = request()->user();
        return view('admin.setup.billing', compact('user'));
    }

    public function instagram()
    {
        return view('admin.setup.instagram');
    }

    public function templatesFacebook()
    {
        return view('admin.setup.templates-facebook');
    }

    public function templatesEmails()
    {
        return view('admin.setup.templates-emails');
    }

    public function embed()
    {
        return view('admin.setup.embed');
    }

    public function labels()
    {
        return view('admin.setup.labels');
    }

    public function repost()
    {
        $enabledGroups = fb_enabled_group_ids();
        $groups = empty($enabledGroups) ? [] : Facebook::getMerchantGroups();
        $repostGroups = ShopSettings::repostGroups();
        $times = ShopSettings::repostTimes();
        return view('admin.setup.repost', compact('times', 'enabledGroups', 'groups', 'repostGroups'));
    }

    public function shopify()
    {
        return view('admin.setup.shopify');
    }

    public function invoices()
    {
        $charges = PayPalComissionCharge::visible()->orderBy('id', 'DESC')->paginate(10);
        return view('admin.setup.invoices.paypal-comission-charges', compact('charges'));
    }

    public function subscriptionPlan()
    {
        $user = request()->user();
        $charges = StripeCharges::subscription()->paginate(10);
        return view('admin.setup.invoices.subscription-plan', compact('charges', 'user'));
    }

    public function stripeCharges()
    {
        $charges = StripeCharges::labels()->paginate(10);
        return view('admin.setup.invoices.prepaid-labels', compact('charges'));
    }

    public function updateCard()
    {
        $user = request()->user();
        $user->updateCard(request('stripeToken'));

        if (shop_setting('paypal:card-declined') == true) {
            // reset the card declined notification
            shop_setting_set('paypal:card-declined', false);
            flash('Card Updated! We will try to charge your card for PayPal comission fees in the next 24 hours.');

        } elseif (shop_setting('subscription-plan:card-declined') == true) {
            // reset the card declined notification
            shop_setting_set('subscription-plan:card-declined', false);
            flash('Card Updated! We will try to charge your card again in next 24 hours.');

        } else {
            flash('Card Updated!');
        }

        return back();
    }

    public function shopName(ShopNameFormRequest $request)
    {
        shop_setting_set('shop.name', request('shop_name'));
        flash('Shop Name Updated!');
        return back();
    }

    public function shopDescription(ShopDescriptionFormRequest $request)
    {
        shop_setting_set('shop.description', request('shop_description'));
        flash('Description Updated!');
        return back();
    }

    public function shopAddress(ShopAddressFormRequest $request)
    {
        shop_setting_set('shop.from_name', request('from_name'));
        shop_setting_set('shop.company_name', request('company_name'));
        shop_setting_set('shop.street_address', request('street_address'));
        shop_setting_set('shop.city', request('city'));
        shop_setting_set('shop.state', request('state'));
        shop_setting_set('shop.zip_code', request('zip_code'));
        shop_setting_set('shop.phone', request('phone'));
        shop_setting_set('shop.company_email', request('company_email'));
        flash('Address Updated!');
        return back();
    }

    public function timezone(TimezoneFormRequest $request)
    {
        shop_setting_set('shop.timezone', request('timezone'));
        flash('Timezone Updated!');
        return back();
    }

    public function expire(ExpireFormRequest $request)
    {
        shop_setting_set('shopping-cart:expire', request('expire'));
        flash('Expire Updated!');
        return back();
    }

    public function remind(RemindFormRequest $request)
    {
        shop_setting_set('shopping-cart:remind', request('remind'));
        flash('Remind Updated!');
        return back();
    }

    public function shippingCost(ShippingCostFormRequest $request)
    {
        shop_setting_set('shop.shipping-cost', request('shipping_cost'));
        flash('Shipping Cost Updated!');
        return back();
    }

    public function freeShippingMaximum(FreeShippingMaximumFormRequest $request)
    {
        shop_setting_set('shop.free-shipping-maximum', request('shipping_maximum'));
        flash('Free Shipping Maximum Updated!');
        return back();
    }

    public function variableShippingCost(VariableShippingCostFormRequest $request)
    {
        shop_setting_set('shop.variable-shipping-cost', request('variable_shipping_cost'));
        flash('Variable Shipping Cost Updated!');
        return back();
    }

    public function updatePassword(PasswordFormRequest $request)
    {
        $user = request()->user();
        $user->password = bcrypt(request('new_password'));
        $user->save();
        flash('Password Updated!');
        return back();
    }

    public function paypalKeys(PaypalApiKeysFormRequest $request)
    {
        shop_setting_set('paypal.client_id', request('client_id'));
        shop_setting_set('paypal.client_secret', request('client_secret'));
        flash('Paypal Keys Updated!');
        return back();
    }

    public function goshippoApiToken(GoShippoApiTokenFormRequest $request)
    {
        shop_setting_set('goshippo.api_token', request('api_token'));
        flash('GoShippo Api Token Updated!');
        return back();
    }

    public function enable24hrFreeShipping()
    {
        shop_setting_set('24hr-free-shipping-enabled', true);
        flash('24hr Free Shipping Enabled!');
        return back();
    }

    public function disable24hrFreeShipping()
    {
        shop_setting_set('24hr-free-shipping-enabled', false);
        flash('24hr Free Shipping Disabled!');
        return back();
    }

    public function stripeConnect()
    {
        if (request('error') == 'access_denied') {
            flash(request('error_description'), 'danger');
            return redirect('/admin/setup/billing');
        }

        $code = request('code');
        if (empty($code)) {
            flash('No stripe code specified.', 'danger');
            return redirect('/admin/setup/billing');
        }

        $token_request_body = [
            'client_secret' => config('services.stripe.secret'),
            'grant_type'    => 'authorization_code',
            'client_id'     => config('services.stripe.client_id'),
            'code'          => $code,
        ];

        $req = curl_init('https://connect.stripe.com/oauth/token');
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $response = json_decode(curl_exec($req), true);
        curl_close($req);

        if (isset($response['error'])) {
            flash($response['error_description'], 'danger');
            return redirect('/admin/setup/billing');

        } else {
            shop_setting_set('stripe.access_token', $response['access_token']);
            shop_setting_set('stripe.livemode', $response['livemode']);
            shop_setting_set('stripe.refresh_token', $response['refresh_token']);
            shop_setting_set('stripe.token_type', $response['token_type']);
            shop_setting_set('stripe.stripe_publishable_key', $response['stripe_publishable_key']);
            shop_setting_set('stripe.stripe_user_id', $response['stripe_user_id']);
            shop_setting_set('stripe.scope', $response['scope']);

            // reset stripe_id, and clear cards in case they connect a different stripe account
            ShopUser::refreshStripeData();

            flash('Stripe account connected successfully.');
            return redirect('/admin/setup/billing');
        }
    }

    public function enableLocalPickup()
    {
        shop_setting_set('local-pickup-enabled', true);
        flash('Local Pickup Enabled!');
        return back();
    }

    public function disableLocalPickup()
    {
        shop_setting_set('local-pickup-enabled', false);
        flash('Local Pickup Disabled!');
        return back();
    }

    public function updateTemplate(TemplateFormRequest $request)
    {
        $key = request('key');
        $template = request($key);

        (new Template)->setTemplate($key, mysql_utf8($template));

        $commentKeys = ['invalid_comment', 'item_waitlisted', 'waitlist_activated', 'order_without_email', 'new_order'];
        if (in_array($key, $commentKeys) && Template::containsLink($template)) {
            flash('Template updated! There is a chance Facebook will mark the comment as spam because it contains a link.', 'danger');
        } else {
            flash('Template updated!');
        }

        return back();
    }

    public function updateEmbedCode()
    {
        shop_setting_set('embed-code', request('embed_code'));
        flash('The embed code was updated!');
        return back();
    }

    public function enableLabels()
    {
        shop_setting_set('labels-enabled', true);
        flash('Labels Enabled!');
        return back();
    }

    public function disableLabels()
    {
        shop_setting_set('labels-enabled', false);
        flash('Labels Disabled!');
        return back();
    }

    public function enableRepost()
    {
        shop_setting_set('repost-enabled', true);
        flash('Auto Scheduler Enabled!');
        return back();
    }

    public function disableRepost()
    {
        shop_setting_set('repost-enabled', false);
        flash('Auto Scheduler Disabled!');
        return back();
    }

    public function repostMinQuantity(RepostMinQuantityFormRequest $request)
    {
        shop_setting_set('repost.minimum-quantity', request('minimum_quantity'));
        flash('Minimum quantity updated!');
        return back();
    }

    public function repostDaysSinceLastPost(DaysSinceLastPostFormRequest $request)
    {
        shop_setting_set('repost.days-since-last-post', request('number_of_days'));
        flash('Number of days updated!');
        return back();
    }

    public function repostAddTime()
    {
        $times = ShopSettings::repostTimes();
        $times[] = ['hour' => request('hour'), 'min'  => request('min'), 'ampm' => request('ampm')];
        shop_setting_set('repost.times', json_encode($times));
        flash('Time Added!');
        return back();
    }

    public function repostRemoveTime()
    {
        $times = ShopSettings::repostTimes();
        foreach ($times as $key => $time) {
            if ($time->ampm == request('ampm') && $time->hour == request('hour') && $time->min == request('min')) {
                unset($times[$key]);
                break;
            }
        }
        shop_setting_set('repost.times', json_encode(array_values($times)));
        flash('Time Removed!');
        return back();
    }

    public function repostEnableGroup()
    {
        $repostGroups = ShopSettings::repostGroups();
        $repostGroups[] = request('group_id');
        shop_setting_set('repost.facebook-groups', json_encode($repostGroups));
        flash('Enabled repost for group!');
        return back();
    }

    public function repostDisableGroup()
    {
        $repostGroups = ShopSettings::repostGroups();

        if (($key = array_search(request('group_id'), $repostGroups)) !== false) {
            unset($repostGroups[$key]);
        }

        shop_setting_set('repost.facebook-groups', json_encode($repostGroups));
        flash('Disabled repost for group!');
        return back();
    }

    public function toggleSwitch(ToggleSwitchFormRequest $request)
    {
        $state = $request->input('state') == 'true' ? true : false;
        shop_setting_set($request->input('key'), $state);
        return response()->json(['success' => true]);
    }

    public function buyPrepaidCredit(PrepaidCreditFormRequest $request)
    {
        try {
            $shopOwner = $request->user();
            $amount = $request->input('prepaid_credit');

            log_info("[prepaid-credit] Trying to charge \${$amount}.");
            $charge = $shopOwner->charge(dollars_to_cents($amount), [
                'statement_descriptor' => 'CommentSold Shipping'
            ]);
            $shopOwner->addPrepaidCredit($amount);

            StripeCharges::log($amount, $charge, $shopOwner);
            (new ShopMailer)->sendPrepaidCreditEmail($shopOwner, $amount);

            flash('Prepaid credit added!');
            return back();

        } catch (Exception $e) {
            if (! $e instanceof \Stripe\Error\Card) {
                ShopMailer::notifyAdmin($e);
                log_error("[prepaid-credit] [exception] {$e}");
            }

            return back()->withInput()->withErrors(['prepaid_credit' => $e->getMessage()]);
        }
    }
}
