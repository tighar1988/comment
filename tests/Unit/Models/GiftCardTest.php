<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\GiftCard;

class GiftCardTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(GiftCard::class, new GiftCard);
    }

    /** @test */
    public function it_generates_code()
    {
        $giftCard = new GiftCard;
        $this->assertEquals(16, strlen($giftCard->generateCode()));
    }

    /** @test */
    public function is_used()
    {
        $giftCard = new GiftCard;
        $giftCard->used = 0;
        $this->assertFalse($giftCard->isUsed());

        $giftCard->used = 1;
        $this->assertTrue($giftCard->isUsed());
    }

    /** @test */
    public function is_disabled()
    {
        $giftCard = new GiftCard;
        $giftCard->disabled = 0;
        $this->assertFalse($giftCard->isDisabled());

        $giftCard->disabled = 1;
        $this->assertTrue($giftCard->isDisabled());
    }

    /** @test */
    public function isValid()
    {
        $giftCard = new GiftCard;
        $giftCard->used = 0;
        $giftCard->disabled = 0;
        $this->assertTrue($giftCard->isValid());

        $giftCard->used = 1;
        $giftCard->disabled = 0;
        $this->assertFalse($giftCard->isValid());

        $giftCard->used = 0;
        $giftCard->disabled = 1;
        $this->assertFalse($giftCard->isValid());

        $giftCard->used = 1;
        $giftCard->disabled = 1;
        $this->assertFalse($giftCard->isValid());
    }

    /** @test */
    public function preview_code()
    {
        $giftCard = new GiftCard;
        $giftCard->code = 'A2B14C3D5F6E7G8H';

        $this->assertEquals('A2B1 4C3D 5F6E 7G8H', $giftCard->previewCode());
    }
}
