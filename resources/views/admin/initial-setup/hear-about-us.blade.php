@extends('layouts.admin-setup')

@section('content')
<div class="row">
    <div class="title-text col-md-6 col-md-offset-3 text-center">
        <h2>Commentsold</h2>
        <h3>Where did you hear about us?</h3>
        <br/>
    </div>
</div>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <form class="" role="form" method="POST" action="/admin/setup/hear-about-us">

            <div class="form-group @hasError('hear_about_us')">
                <select class="form-control" id="hear-about-us" name="hear_about_us">
                    <option value="">Please Select</option>
                    <option value="google">Google</option>
                    <option value="facebook">Facebook</option>
                    <option value="boutiquehub">BoutiqueHub</option>
                    <option value="lesley">Lesley</option>
                    <option value="leo">Leo</option>
                    <option value="another-customer">Another Customer</option>
                    <option @if (old('hear_about_us') == 'from-a-friend') selected="selected" @endif value="from-a-friend">From A Friend</option>
                    <option @if (old('hear_about_us') == 'other') selected="selected" @endif value="other">Other</option>
                </select>
                @error('hear_about_us')
            </div>

            <div class="row" @if (old('hear_about_us') != 'other' && old('hear_about_us') != 'from-a-friend') style="display: none;" @endif id="other-source-row">
                <div class="col-md-12">
                    <div class="form-group @hasError('other_source')">
                        <textarea maxlength="300" class="form-control" name="other_source" rows="3">{{ old('other_source') }}</textarea>
                        @error('other_source')
                    </div>
                </div>
            </div>

            <button type="submit" style="margin-top: 0px;" class="btn btn-success" data-loading-text="Loading.." onClick="fbq('trackCustom', 'Hear About Us');">Next</button>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('form').submit(function() {
            $('button[type="submit"]', $(this)).button('loading');
        });

        $('#hear-about-us').change(function() {
            if ($(this).val() == 'other' || $(this).val() == 'from-a-friend') {
                $('#other-source-row').show();
            } else {
                $('#other-source-row').hide();
            }
        });
    });
</script>
@endsection
