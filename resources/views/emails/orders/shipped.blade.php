@component('mail::message', ['shopName' => $shopName, 'shopUrl' => $shopUrl])

<div style="text-align: right">
    <strong>Dispatch Confirmation</strong><br/>
    Order #{{ $orderId }}
</div>

Hello {{ $shipName }}, <br/>

We thought you'd like to know that {{ $shopName }} dispatched your item(s). Your order is on the way, and can no longer be changed. If you need to return an item or manage other orders, please visit <a href="{{ $shopUrl }}">{{ $shopUrl }}</a> <br/>

Your order was sent to: <br/>
{{ $shipName }} <br/>
{{ $streetAddress }} <br/>
@if ($apartment)
    {{ $apartment }} <br/>
@endif
{{ $city }}, {{ $state }} {{ $zip }} <br/>
U.S. <br/>

@if ($trackingNumber)
Your USPS tracking number is <a href="https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels={{ $trackingNumber }}">{{ $trackingNumber }}</a>. Depending on the USPS service, it's possible that the tracking information might not be visible immediately. <br/><br/>
@endif

### Delivery Information
@component('mail::table')
    | Product       | Price         |
    |:------------- | -------------:|
    @foreach ($orderProducts as $product)
        | {{ $product['name'] }} | ${{ amount($product['price']) }} |
    @endforeach
    | | Subtotal: ${{ amount($subtotal) }} |
    | | Shipping: ${{ amount($shipCharged) }} |
    | | Tax: ${{ amount($taxTotal) }} |
    @if ($couponDiscount)
        | | Coupon Discount: -${{ amount($couponDiscount) }} |
    @endif
    @if ($applyBalance)
        | | Account Balance: -${{ amount($applyBalance) }} |
    @endif
    | | Total: <strong>${{ amount($total) }}</strong> |
@endcomponent

If you need further assistance with your order, please contact {{ $shopEmail }}.<br/>

We hope to see you again soon.<br/>
{{ $shopName }}<br/><br/>
@endcomponent
