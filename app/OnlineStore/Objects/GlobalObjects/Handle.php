<?php

namespace App\OnlineStore\Objects\GlobalObjects;

use App\OnlineStore\Objects\Drop;

class Handle extends Drop
{
    /**
     * The handle object returns the handle of the page that is being viewed.
     *
     * {% if handle contains 'hide-from-search' %}
     *     <meta name="robots" content="noindex">
     * {% endif %}
     */
    public function toLiquid()
    {

    }
}
