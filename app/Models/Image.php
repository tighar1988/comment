<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'filename',
        'is_main',
        'shopify_image_id',
        'position',
        'image_width',
        'image_height',
    ];

    public static function newImage($productId, $filename, $shopifyImageId = null)
    {
        static::create([
            'product_id' => $productId,
            'filename'   => $filename,
            'is_main'    => static::isMain($productId),
            'shopify_image_id' => $shopifyImageId,
        ]);
    }

    public static function newProductImage($productId, $filename, $width = null, $height = null)
    {
        static::create([
            'product_id'   => $productId,
            'filename'     => $filename,
            'is_main'      => static::isMain($productId),
            'image_width'  => $width,
            'image_height' => $height,
        ]);
    }

    public static function isMain($productId)
    {
        // set image as main only if there are not other images
        $isMain = true;
        if (static::where('product_id', $productId)->count()) {
            $isMain = false;
        }

        return $isMain;
    }
}
