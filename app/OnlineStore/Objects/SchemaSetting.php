<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;

class SchemaSetting extends Drop
{
    protected $setting;

    protected $schema;

    public function __construct($setting, $schema)
    {
        $this->setting = $setting;
        $this->schema = $schema;
    }

    public function __toString()
    {
        if (! isset($this->setting->id)) {
            return '';
        }

        if (! isset($this->schema->name)) {
            return '';
        }

        $section = strtolower($this->schema->name);
        $id = $this->setting->id;

        if (! isset(CurrentView::$settingsData['current']['sections'][$section]['settings'][$id])) {

            // if (isset($this->setting->type, $this->setting->default) && $this->setting->type == 'checkbox') {
            //     if (is_bool($this->setting->default)) {
            //         return $this->setting->default ? 'true' : 'false';
            //     }
            // }

            return '';
        }

        $value = CurrentView::$settingsData['current']['sections'][$section]['settings'][$id];

        return (string) $value;
    }

    public function handle_key($key)
    {
        if ($key == 'aspect_ratio') {
            return 1;
        }

        return null;
    }
}
