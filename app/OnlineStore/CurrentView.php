<?php

namespace App\OnlineStore;

use App\OnlineStore\Objects\Drop;

class CurrentView
{
    /**
     * @var string
     */
    public static $currentPath;

    /**
     * @var boolean
     */
    public static $isSearch = false;

    /**
     * @var numeric
     */
    public static $currentPage;

    /**
     * @var numeric
     */
    public static $currentOffset;

    /**
     * @var numeric
     */
    public static $numberItems;

    /**
     * @var string
     */
    public static $searchQuery;

    /**
     * @var string
     */
    public static $queryString;

    /**
     * @var string
     */
    public static $fullUrl;

    /**
     * @var numeric
     */
    public static $totalItems;

    /**
     * @var string
     */
    public static $collectionHandle;

    /**
     * @var string
     */
    public static $productHandle;

    /**
     * @var \App\Models\Product
     */
    public static $collectionProduct;

    /**
     * @var string
     */
    public static $sectionName;

    /**
     * @var  array
     */
    public static $schemas = [];

    /**
     * @var array
     */
    public static $settingsData;

    /**
     * @var array
     */
    public static $settingsSchema;

    /**
     * @var string
     */
    public static $pageTitle;
}
