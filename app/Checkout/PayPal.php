<?php

namespace App\Checkout;

use App\Models\Order;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Payee;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Amount;
use PayPal\Api\PaymentOptions;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use Exception;

class PayPal
{
    protected $clientId;

    protected $clientSecret;

    protected $apiContext;

    protected $payeeEmail;

    public function __construct()
    {
        $this->payeeEmail = shop_setting('paypal.identity.email');

        if (empty($this->payeeEmail)) {

            // use the shop's REST API app
            $this->clientId = shop_setting('paypal.client_id');
            $this->clientSecret = shop_setting('paypal.client_secret');
        } else {

            // use the official CommentSold REST API app
            $this->clientId = config('services.paypal.client_id');
            $this->clientSecret = config('services.paypal.client_secret');
        }

        // verify that paypal api keys are set
        if (empty($this->clientId) || empty($this->clientSecret)) {
            throw new Exception('PayPal API keys are not set.');
        }

        // paypal api context
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential($this->clientId, $this->clientSecret)
        );

        $this->apiContext->setConfig([
            'mode'           => env('APP_ENV') == 'testing' ? 'SANDBOX' : 'LIVE',
            'log.LogEnabled' => true,
            'log.FileName'   => storage_path('paypal/PayPal.log'),
            'log.LogLevel'   => 'INFO', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
        ]);
    }

    public function getApprovalLink($cart, $returnUrl = '/paypal-payment')
    {
        // paypal payer
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        // redirect urls
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(shop_url($returnUrl))
            ->setCancelUrl(shop_url('/paypal-payment-cancel'));

        // shopping cart total amount
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($cart->total);

        // payment options
        $paymentOptions = new PaymentOptions;
        $paymentOptions->setAllowedPaymentMethod('IMMEDIATE_PAY');

        // transaction
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription(shop_name() . ' order.')
            ->setPaymentOptions($paymentOptions);

        // set payee
        if (! empty($this->payeeEmail)) {
            $payee = new Payee;
            $payee->setEmail($this->payeeEmail);
            $transaction->setPayee($payee);
        }

        // create the payment
        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        $payment->create($this->apiContext);
        return $payment->getApprovalLink();
    }

    public function executePayment($paymentId, $payerID, $cart)
    {
        $payment = Payment::get($paymentId, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerID);

        $paymentOptions = new PaymentOptions;
        $paymentOptions->setAllowedPaymentMethod('IMMEDIATE_PAY');

        $transaction = new Transaction();
        $amount = new Amount();
        $amount->setCurrency('USD');
        $amount->setTotal($cart->total);

        $transaction->setAmount($amount);
        $transaction->setPaymentOptions($paymentOptions);

        $execution->addTransaction($transaction);

        return $payment->execute($execution, $this->apiContext);
    }

    public function validatePaymentMobile($paymentId, $cart)
    {
        $payment = Payment::get($paymentId, $this->apiContext);

        if (! $payment) {
            throw new  Exception('Payment does not exist');
        }

        $transactions = $payment->getTransactions();
        $payeeEmail = $payment->transactions[0]->payee->getEmail();

        if ($transactions[0]->getAmount()->total != $cart->total) {
            throw new  Exception('The total amount does not match');
        }

        if ($transactions[0]->getAmount()->currency != 'USD') {
            throw new  Exception('The currency must be USD');
        }

        if ($payeeEmail != shop_setting('shop.company_email')) {
            throw new  Exception('The company email does not match');
        }

        if ($payment->getState() != 'approved') {
            throw new  Exception('The payment was not approved.');
        }

        if (Order::where('payment_ref', $paymentId)->count() > 0) {
            throw new  Exception('The order was already created.');
        }

        return $payment;
    }

    public static function keysAreValid($clientId, $clientSecret)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.paypal.com/v1/oauth2/token");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$clientSecret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        $result = curl_exec($ch);
        curl_close($ch);

        if(! empty($result)) {
            $json = json_decode($result);
            if (isset($json->access_token)) {
                return true;
            }
        }

        return false;
    }
}
