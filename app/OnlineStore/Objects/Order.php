<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Order as OrderModel;

class Order extends Drop
{
    protected $order;

    public function __construct(OrderModel $order)
    {
        $this->order = $order;
    }

    public function order_products()
    {
        $items = [];

        foreach ($this->order->orderProducts as $item) {
            $items[] = new OrderProduct($item);
        }

        return $items;
    }

    public function is_local_pickup()
    {
        return $this->order->local_pickup;
    }

    public function street_address()
    {
        return $this->order->street_address;
    }

    public function apartment()
    {
        return $this->order->apartment;
    }

    public function city()
    {
        return $this->order->city;
    }

    public function state()
    {
        return $this->order->state;
    }

    public function zip()
    {
        return $this->order->zip;
    }

    public function total()
    {
        return $this->order->total;
    }

    public function subtotal()
    {
        return $this->order->subtotal;
    }

    public function ship_charged()
    {
        return $this->order->ship_charged;
    }

    public function tax_total()
    {
        return $this->order->tax_total;
    }

    public function apply_balance()
    {
        return $this->order->apply_balance;
    }

    public function coupon_discount()
    {
        return $this->order->coupon_discount;
    }

    /**
     * Returns the custom cart attributes for the order, if there are any. You can add
     * as many custom attributes to your cart as you like.
     *
     * When you're looping through attributes, use {{ attribute | first }} to get the
     * name of the attribute, and {{ attribute | last }} to get its value.
     *
     * Input
     * {% if order.attributes %}
     *   <p>Order notes:</p>
     *   <ul>
     *     {% for attribute in order.attributes %}
     *       <li><strong>{{ attribute | first }}</strong>: {{ attribute | last }}</li>
     *     {% endfor %}
     *   </ul>
     * {% endif %}
     *
     * Output
     * <p>Order notes:</p>
     * <ul>
     *   <li><strong>Message to merchant</strong>: I love your products! Thanks!</li>
     * </ul>
     */
    public function attributes()
    {
        // todo: implement
    }

    /**
     * Returns the billing address of the order.
     */
    public function billing_address()
    {
        // todo: implement
    }

    /**
     * Returns true if an order is canceled, or false if it is not.
     */
    public function cancelled()
    {
        // todo: implement
        return false;
    }

    /**
     * Returns the timestamp of when an order was canceled. Use the date filter to format the timestamp.
     */
    public function cancelled_at()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns one of the following cancellation reasons, if an order was canceled:
     *
     * items unavailable
     * fraudulent order
     * customer changed/cancelled order
     * other
     */
    public function cancel_reason()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the translated output of an order's order.cancel_reason.
     *
     * Input
     * English: {{ order.cancel_reason }}
     * French: {{ order.cancel_reason_label }}
     *
     * Output
     * English: Items unavailable
     * French: Produits indisponibles
     */
    public function cancel_reason_label()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the timestamp of when an order was created. Use the date filter to format the timestamp.
     */
    public function created_at()
    {
        return strtotime($this->order->created_at->toDateTimeString());
    }

    /**
     * Returns the customer associated with the order.
     */
    public function customer()
    {
        // todo: implement
    }

    /**
     * Returns a unique URL that the customer can use to access the order.
     *
     * Input
     * {{ order.name | link_to: order.customer_url }}
     *
     * Output
     * <a href="http://johns-apparel.myshopify.com/account/orders/d94ec4a1956f423dc4907167c9ef0413">#1235</a>
     */
    public function customer_url()
    {
        return shop_url('/customers/account/orders/'.$this->order->id);
    }

    /**
     * Returns an array of discounts for an order.
     *
     * Input
     * {% for discount in order.discounts %}
     *   Code: {{ discount.code }}
     *   Savings: {{ discount.savings | money }}
     * {% endfor %}
     *
     * Output
     * Code: SUMMER16
     * Savings: -$20.00
     */
    public function discounts()
    {
        // todo: implement
        return [];
    }

    /**
     * Returns the email address associated with an order.
     */
    public function email()
    {
        return $this->order->email;
    }

    /**
     * Returns the financial status of an order. The possible values are:
     *
     * pending
     * authorized
     * paid
     * partially_paid
     * refunded
     * partially_refunded
     * voided
     */
    public function financial_status()
    {
        // todo: implement
        return 'paid';
    }

    /**
     * Returns the translated output of an order's financial_status.
     *
     * Input
     * English: {{ order.financial_status }}
     * French: {{ order.financial_status_label }}
     *
     * Output
     * English: Paid
     * French: Payée
     */
    public function financial_status_label()
    {
        // todo: implement
        return 'Paid';
    }

    /**
     * Returns the fulfillment status of an order.
     */
    public function fulfillment_status()
    {
        if ($this->order->isFulfilled()) {
            return 'Fulfilled';
        }

        return 'Unfulfilled';
    }

    /**
     * Returns the translated output of an order's fulfillment_status.
     *
     * Input
     * English: {{ order.fulfillment_status }}
     * French: {{ order.fulfillment_status_label }}
     *
     * Output
     * English: Unfulfilled
     * French: Non confirmée
     */
    public function fulfillment_status_label()
    {
        if ($this->order->isFulfilled()) {
            return 'Fulfilled';
        }

        return 'Unfulfilled';
    }

    /**
     * Returns an array of line items for the order.
     */
    public function line_items()
    {
        // todo: implement
    }

    /**
     * (POS only) Returns the physical location of the order. You can configure
     * locations in the Locations settings of your Shopify admin.
     */
    public function location()
    {
        // todo: implement
    }

    /**
     * Returns the name of the order in the format set in the Standards and formats
     * section of the General settings of your Shopify admin.
     *
     * Input
     * {{ order.name }}
     *
     * Output
     * #1025
     */
    public function name()
    {
        return '#'.$this->order->id;
    }

    /**
     * Returns the note associated with a customer order.
     *
     * Tip
     * To show an order note in an email notification or an Order Printer template, use {{ note }}. To show an order note in a template file like customers/account.liquid, use {{ order.note }}.
     *
     * Input
     * Special instructions: {{ order.note }}
     *
     * Output
     * Special instructions: Please deliver after 5 PM
     */
    public function note()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the integer representation of the order name.
     *
     * Input
     * {{ order.order_number }}
     *
     * Output
     * 1025
     */
    public function order_number()
    {
        return $this->order->id;
    }

    /**
     * Returns the shipping address of the order.
     */
    public function shipping_address()
    {
        // todo: implement
    }

    /**
     * Returns an array of shipping_method variables from the order.
     */
    public function shipping_methods()
    {
        // todo: implement
    }

    /**
     * Returns the shipping price of an order. Use a money filter to show the result as a monetary amount.
     */
    public function shipping_price()
    {
        return dollars_to_cents($this->order->ship_charged);
    }

    /**
     * Returns the subtotal price of an order. Use a money filter to show the result as a monetary amount.
     */
    public function subtotal_price()
    {
        return dollars_to_cents($this->order->subtotal);
    }

    /**
     * Returns an array of tax_line variables for an order.
     *
     * Input
     * {% for tax_line in order.tax_lines %}
     *   Tax ({{ tax_line.title }} {{ tax_line.rate | times: 100 }}%): {{ tax_line.price | money }}
     * {% endfor %}
     *
     * Output
     * Tax (GST 14.0%): $25
     */
    public function tax_lines()
    {
        // todo: implement
    }

    /**
     * Returns the order's tax price. Use a money filter to show the result as a monetary amount.
     */
    public function tax_price()
    {
        return dollars_to_cents($this->order->tax_total);
    }

    /**
     * Returns the total price of an order. Use a money filter to show the result as a monetary amount.
     */
    public function total_price()
    {
        return dollars_to_cents($this->order->total);
    }

    /**
     * Returns an array of transactions from the order.
     */
    public function transactions()
    {
        // todo: implement
    }

    /**
     * Returns an array of all of the order's tags. The tags are returned in alphabetical order.
     *
     * Input
     * {% for tag in order.tags %}
     *     {{ tag }}
     * {% endfor %}
     *
     * Output
     * new
     * leather
     * sale
     * special
     */
    public function tags()
    {
        // todo: implement
        return [];
    }
}
