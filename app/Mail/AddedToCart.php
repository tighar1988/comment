<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddedToCart extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $shopFromEmail = $this->data['shopFromEmail'] ?? config('mail.from.address');

        $mailable = $this->subject("Item added to your order: " . ($this->data['productName'] ?? null))
            ->replyTo($this->data['shopEmail'], $this->data['shopName'])
            ->from($shopFromEmail, $this->data['shopName']);

        if (isset($this->data['shopId']) && $this->data['shopId'] == 'poppyanddot') {
            $mailable->markdown('emails.added-to-cart-markdown')->with($this->data);
        } else {
            $mailable->view('emails.added-to-cart')->with($this->data);
        }

        return $mailable;
    }
}
