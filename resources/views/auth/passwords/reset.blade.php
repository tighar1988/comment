@extends('layouts.app')

@section('content')
<div class="login-box">
    <div class="login-box-body">
        <div class="login-logo">
            <a href="/"><img src="{{ cdn('/images/login-logo.png') }}" alt="CommentSold"></a>
        </div>
        <p class="login-box-msg">Reset Password</p>
        <form role="form" method="POST" action="{{ url('/password/reset') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group has-feedback @hasError('email')">
                <input type="email" name="email" value="{{ $email or old('email') }}" required autofocus class="form-control" placeholder="E-Mail Address">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @error('email')
            </div>

            <div class="form-group has-feedback @hasError('password')">
                <input type="password" class="form-control" name="password" required placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password')
            </div>

            <div class="form-group has-feedback @hasError('password_confirmation')">
                <input type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password_confirmation')
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat m-b-20">Reset Password</button>
                </div>
            </div>
        </form>
        <a href="{{ url('/login') }}">Login</a><br>
    </div>
</div>
@endsection
