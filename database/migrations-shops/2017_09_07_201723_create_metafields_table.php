<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetafieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metafields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('namespace')->nullable();
            $table->string('key')->nullable();
            $table->text('value')->nullable();
            $table->string('value_type')->nullable();
            $table->text('description')->nullable();
            $table->string('owner_resource')->nullable();
            $table->integer('owner_id')->unsigned()->nullable();
            $table->string('shopify_owner_id')->nullable();
            $table->string('shopify_metafield_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('metafields');
    }
}
