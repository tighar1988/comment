<?php

namespace App\Models\Helpers;

use App\Models\Card;
use App\Models\ShopUser;
use App\Checkout\Fees;

trait StripeConnectBillable
{
    public static function refreshStripeData()
    {
        Card::truncate();

        ShopUser::query()->update([
            'stripe_id'      => null,
            'card_brand'     => null,
            'card_last_four' => null,
            'trial_ends_at'  => null
        ]);
    }

    public function hasStripeId()
    {
        return ! is_null($this->stripe_id);
    }

    public function cards()
    {
        $cards = $this->hasMany('App\Models\Card', 'user_id')->orderBy('default', 'DESC')->orderBy('created_at', 'DESC')->get();

        // delete expired cards from the database
        foreach ($cards as $key => $card) {
            if ($card->hasExpired()) {
                $card->delete();
                $cards->forget($key);
            }
        }

        return $cards;
    }

    public function getOrCreateStripeCustomer()
    {
        if ($this->hasStripeId()) {
            return \Stripe\Customer::retrieve($this->stripe_id, shop_setting('stripe.access_token'));

        } else {
            $customer = \Stripe\Customer::create([
                'email' => $this->email,
                'description' => $this->facebook_id
            ], shop_setting('stripe.access_token'));

            $this->stripe_id = $customer->id;
            $this->save();
            return $customer;
        }
    }

    public function addNewCard($token)
    {
        $customer = $this->getOrCreateStripeCustomer();

        $token = \Stripe\Token::retrieve($token, ['api_key' => shop_setting('stripe.access_token')]);
        $card = $customer->sources->create(['source' => $token]);

        Card::store($this->id, $card);
        $this->card_brand = $card->brand;
        $this->card_last_four = $card->last4;
        $this->save();

        return $card->id;
    }

    public function charge($cardId, $amount, $feeAmount)
    {
        $options = [
            'amount'   => $amount,
            'currency' => 'usd',
            'customer' => $this->stripe_id,
            'source'   => $cardId,
            'application_fee' => $feeAmount,
        ];

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        return \Stripe\Charge::create($options, ['stripe_account' => shop_setting('stripe.stripe_user_id')]);
    }

    public function chargeToken($token, $amount, $feeAmount)
    {
        $options = [
            'amount'   => $amount,
            'currency' => 'usd',
            'source'   => $token,
            'application_fee' => $feeAmount,
        ];

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        return \Stripe\Charge::create($options, ['stripe_account' => shop_setting('stripe.stripe_user_id')]);
    }

    public function refund($charge, array $options = [])
    {
        // todo: add refund option
        // $options['charge'] = $charge;
        // return \Stripe\Refund::create($options, ['api_key' => shop_setting('stripe.access_token')]);
    }
}
