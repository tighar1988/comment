<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Menu as MenuModel;

class Linklist extends Drop
{
    protected $menu;

    public function __construct(MenuModel $menu)
    {
        $this->menu = $menu;
    }

    /**
     * Returns the handle of the linklist.
     */
    public function handle()
    {
        return $this->menu->handle;
    }

    /**
     * Returns the id of the linklist.
     */
    public function id()
    {
        return $this->menu->id;
    }

    /**
     * Returns an array of links in the linklist.
     *
     * Input
     * {% for link in linklists.main-menu.links %}
     *       <a href="{{ link.url }}">{{ link.title }}</a>
     * {% endfor %}
     *
     * Output
     * <a href="/">Home</a>
     * <a href="/collections/all">Catalog</a>
     * <a href="/blogs/news">Blog</a>
     * <a href="/pages/about-us">About Us</a>
     */
    public function links()
    {
        $links = [];
        foreach ($this->menu->items as $menuItem) {
            $links[] = new Link($menuItem);
        }

        return $links;
    }

    /**
     * Returns the title of the linklist.
     */
    public function title()
    {
        return $this->menu->title;
    }
}
