<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Image as ImageModel;
use App\Models\Product as ProductModel;

class Image extends Drop
{
    protected $image;

    protected $product;

    public function __construct(ImageModel $image, ProductModel $product)
    {
        $this->image = $image;
        $this->product = $product;
    }

    public function __toString()
    {
        return $this->src();
    }

    /**
     * Returns the alt tag of the image, set in the Products page of the Admin.
     */
    public function alt()
    {
        return $this->product->product_name;
    }

    /**
     * Returns true if the image has been associated with a variant. Returns false
     * otherwise. This can be used in cases where you want to create a gallery of
     * images that are not associated with variants.
     */
    public function attached_to_variant()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the id of the image.
     */
    public function id()
    {
        return $this->image->id;
    }

    /**
     * Returns the id of the image's product.
     */
    public function product_id()
    {
        return $this->product->id;
    }

    /**
     * Returns the position of the image, starting at 1. This is the same as outputting forloop.index.
     */
    public function position()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the relative path of the product image. This is the same as outputting {{ image }}.
     *
     * Input
     * {% for image in product.images %}
     *     {{ image.src  }}
     *     {{ image }}
     * {% endfor %}
     *
     * Output
     * products/my_image.jpg
     * products/my_image.jpg
     *
     * To return the URL of the image on Shopify's Content Delivery Network (CDN), use the appropriate URL filter.
     *
     * To see a full list of available image sizes, see image size parameters.
     *
     * Shown below is an example of loading a product image using the img_url filter.
     *
     * Input
     * {{ image | img_url: "medium" }}
     *
     * Output
     * //cdn.shopify.com/s/files/1/0087/0462/products/shirt14_medium.jpeg?v=1309278311
     */
    public function src()
    {
        if (str_contains($this->image->filename, 'bigcommerce.com')) {
            return $this->image->filename;
        }

        if (str_contains($this->image->filename, 'cdn.shopify.com')) {
            return $this->image->filename;
        }

        return $this->image->filename;
    }

    /**
     * Returns an array of the variant ids that the image is associated with.
     *
     * Input
     * {% for image in product.images %}
     *     {% for variant in image.variants %}
     *             {{ image.src }} - used for the variant: {{ variant.title }}
     *       {% endfor %}
     * {% endfor %}
     *
     * Output
     * products/red-shirt.jpeg - used for the variant: Red
     * products/green-shirt.jpg - used for the variant: Green
     * products/yellow-shirt.jpg - used for the variant: Yellow
     */
    public function variants()
    {
        $variants = [];

        foreach ($this->product->inventory as $variant) {
            $variants[] = new Variant($variant, $this->product);
        }

        return $variants;
    }

    /**
     * Returns the height of the image in pixels.
     */
    public function height()
    {
        return $this->image->image_height;
    }

    /**
     * Returns the width of the image in pixels.
     */
    public function width()
    {
        return $this->image->image_width;
    }

    /**
     * Returns the aspect ratio (width / height) of the image.
     */
    public function aspect_ratio()
    {
        if (is_null($this->image->image_height) || is_null($this->image->image_width)) {
            return 1;
        }

        if ($this->image->image_height > 0) {
            return $this->image->image_width / $this->image->image_height;
        }

        return 1;
    }
}
