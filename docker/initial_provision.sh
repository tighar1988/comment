#!/bin/bash

DOTOKEN=`cat do_token`

BASE=cs-web-
NODES="1 2 3"

for i in $NODES; do docker-machine create --driver digitalocean \
--digitalocean-image  ubuntu-16-04-x64 \
--digitalocean-private-networking \
--digitalocean-size "4gb" \
--digitalocean-backups \
--digitalocean-access-token $DOTOKEN ${BASE}$i; done

for i in $NODES; do docker-machine ssh ${BASE}$i "docker login -u brandonkruse -p hot2dog; docker create volume MySQLStorageVolume; docker create volume MySQLInit"; done
