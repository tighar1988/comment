<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShopUser;
use App\Models\Inventory;
use App\Models\Cart;
use App\Models\BalanceMovement;
use App\Waitlist\ShoppingWaitlist;
use App\ShoppingCart\ShoppingCart;
use App\Checkout\CheckoutException;
use App\Http\Requests\Admin\CustomerBalanceFormRequest;
use Datatables;
use App\Mails\ShopMailer;
use App\Exceptions\ShopifyWebhookLockedException;

class CustomersController extends Controller
{
    public function index()
    {
        return view('admin.customers.list');
    }

    public function datatable()
    {
        $model = ShopUser::query()->select('id', 'facebook_id', 'name', 'email', 'state', 'city', 'street_address', 'apartment', 'zip', 'phone_number', 'balance', 'instagram_data');

        return Datatables::eloquent($model)
                ->editColumn('balance', '${{ amount($balance) }}')
                ->addColumn('facebook_url', '{{ facebook_profile($facebook_id) }}')
                ->setRowAttr(['data-customerId' => '{{ $id }}'])
                ->make(true);
    }

    public function detail($id)
    {
        $customer = ShopUser::findOrFail($id);
        $waitlist = $customer->waitlist();
        $cart = $customer->shoppingCart();
        return view('admin.customers.detail', compact('customer', 'waitlist', 'cart'));
    }

    public function removeFromWaitlist($waitlistId)
    {
        ShoppingWaitlist::removeItem($waitlistId);
        flash('Item removed from Waitlist!');
        return back();
    }

    public function removeFromCart($cartId)
    {
        try {
            ShoppingCart::removeItem($cartId, 'Admin removed item from shopping cart.');
            flash('Item removed from Shopping Cart!');
        } catch (ShopifyWebhookLockedException $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shopify] $e");
            flash('Shopify connection error. Please try again!', 'danger');
        }

        return back();
    }

    public function addToCart($customerId, $inventoryId)
    {
        try {
            Cart::adminAddItem(ShopUser::find($customerId), Inventory::find($inventoryId));
            flash('Item added to Shopping Cart!');
        } catch (ShopifyWebhookLockedException $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shopify] $e");
            flash('Shopify connection error. Please try again!', 'danger');
        }

        return back();
    }

    public function createAndConfirmOrder($customerId)
    {
        try {
            $customer = ShopUser::find($customerId);
            $customer->checkout()->withAdminCreatedOrder();
            flash('The order was created!');
            return back();

        } catch(CheckoutException $e) {
            flash($e->getMessage());
            return back();
        }
    }

    public function addBalance($customerId, CustomerBalanceFormRequest $request)
    {
        $amount = request('balance');
        $note = request('note') ?? 'Admin added credit.';
        $note = '['.$request->user()->name.'] ' . $note;

        $user = ShopUser::find($customerId);
        $user->addBalance($amount, $note);

        (new ShopMailer)->sendAwardedAccountCreditEmail($user, $amount);

        return response()->json(['status' => 1, 'balance' => amount($user->balance)]);
    }

    public function subtractBalance($customerId, CustomerBalanceFormRequest $request)
    {
        $amount = request('balance');
        $note = request('note') ?? 'Admin subtracted credit.';
        $note = '['.$request->user()->name.'] ' . $note;

        $user = ShopUser::find($customerId);
        $user->reduceBalance($amount, $note);

        return response()->json(['status' => 1, 'balance' => amount($user->balance)]);
    }
}
