<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\Login;
use Tests\Browser\Pages\ProductAdd;

class ProductTest extends DuskTestCase
{
    /**
     * @group product
     */
    public function testAddProduct()
    {
        $this->browse(function ($browser) {

            $id = time();

            // add a new product
            $browser->visit(new Login)
                    ->signIn()
                    ->visit(new ProductAdd)
                    ->addProduct(['id' => $id])
                    ->assertSee("SKU{$id}")
                    ->assertSee('Done uploading images, continue to inventory');

            // add inventory quantity
            $browser->clickLink('Done uploading images, continue to inventory')
                    ->assertSee('Variants')
                    ->press('.add-qty')
                    ->pause(200)
                    ->type('.quantity', 5)
                    ->press('Add Quantity')
                    ->waitFor('.tooltip')
                    ->assertSeeIn('.qty-text', 5);
        });
    }
}
