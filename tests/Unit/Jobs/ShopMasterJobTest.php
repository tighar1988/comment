<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Jobs\ShopMasterJob;

class ShopMasterJobTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(ShopMasterJob::class, new ShopMasterJob('shop'));
    }
}
