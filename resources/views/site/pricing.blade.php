<!DOCTYPE html>
<html lang="en">
<head>
    @include('site.web-header')
</head>
<body id="page-top" class="index">
    @include('site.nav')

    <!-- Header -->
    <header class="inner-header">
        <div class="container">
            <div class="intro-text">
                <div class="intro-heading">Find the perfect plan for you.</div>
            </div>
        </div>
    </header>

    <section class="bs-pricing-four">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="bs bs-pricing">
                        <h3 class="category">Starter</h3>
                        <h1 class="bs-caption"><small>$</small>49/month</h1>
                        <ul>
                            <li><b>Facebook</b> integration</li>
                            <li><b>Automatic</b> invoices</li>
                            <li><b>5%</b> of sales</li>
                            <li><b>24 hour</b> email support</li>
                        </ul>
                        <a onClick="fbq('trackCustom', 'Register Click');" href="/register" class="btn btn-info">Free Trial</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="bs bs-pricing">
                        <h3 class="category">Business</h3>
                        <h1 class="bs-caption"><small>$</small>149/month</h1>
                        <ul>
                            <li><b>Monthly Fee Waived</b> if you invite 3 friends!</li>
                            <li><b>Facebook</b> support (Groups, Pages and Messenger)</li>
                            <li><b>Instagram</b> integration</li>
                            <li><b>Scheduled</b> Facebook posts</li>
                            <li><b>Inventory</b> management</li>
                            <li><b>Shopify</b> integration</li>
                            <li><b>Integrated</b> shipping/label printing</li>
                            <li><b>Automatic</b> invoices</li>
                            <li><b>3%</b> of sales</li>
                            <li><b>Realtime</b> chat support</li>
                        </ul>
                        <a onClick="fbq('trackCustom', 'Register Click');" href="/register" class="btn btn-info">Free Trial</a>
                        <div class="papular">
                            <div class="papular-text">Popular</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="bs bs-pricing">
                        <h3 class="category">Enterprise</h3>
                        <h1 class="bs-caption">Contact Us</h1>
                        <ul>
                            <li>Starter+Business features plus</li>
                            <li><b>Inventory</b> management</li>
                            <li><b>iPhone/Android</a> app</li>
                            <li><b>Custom</b> development/integration</li>
                            <li><b>Phone/Chat</b> support</li>

                        </ul>
                        <a onClick="fbq('trackCustom', 'Register Click');" href="/register" class="btn btn-info">Free Trial</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <div class="text-right"><img src="{{ cdn('/images/left-pointer.png') }}" alt="" class="img-responsive left-arrow"></div>
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Have any question or need help?</h2>
                    <p class="text">Contact our cutomer support team if you have any further questions.<br>
                        We are here to help you out!
                    </p>
                    <a href="/contact" class="page-scroll btn btn-xl">Contact Us</a>
                </div>
            </div>
        </div>
    </section>

    @include('site.footer')
    </body>
</html>
