<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\ImportDivas;
use Mockery;

class ImportDivasTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(ImportDivas::class, new ImportDivas);
    }
}
