<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PayPal\PayPalIdentity;
use PayPal\Exception\PayPalConnectionException;

class PayPalController extends Controller
{
    public function identity(PayPalIdentity $identity)
    {
        $returnUrl = url('admin/setup/paypal-identity-callback');

        return redirect($identity->getLoginUrl($returnUrl));
    }

    public function identityCallback(PayPalIdentity $identity)
    {
        try {
            $token = $identity->getAccessToken(request('code'));
            shop_setting_set('paypal.identity.access-token', $token->getAccessToken());
            shop_setting_set('paypal.identity.refresh-token', $token->getRefreshToken());

            $userInfo = $identity->getUserinfo($token->getAccessToken());
            shop_setting_set('paypal.identity.family_name', $userInfo->getFamilyName());
            shop_setting_set('paypal.identity.name', $userInfo->getName());
            shop_setting_set('paypal.identity.account_type', $userInfo->getAccountType());
            shop_setting_set('paypal.identity.given_name', $userInfo->getGivenName());
            shop_setting_set('paypal.identity.user_id', $userInfo->getUserId());
            shop_setting_set('paypal.identity.address', $userInfo->getAddress());
            shop_setting_set('paypal.identity.verified_account', $userInfo->getVerifiedAccount());
            shop_setting_set('paypal.identity.language', $userInfo->getLanguage());
            shop_setting_set('paypal.identity.zoneinfo', $userInfo->getZoneinfo());
            shop_setting_set('paypal.identity.locale', $userInfo->getLocale());
            shop_setting_set('paypal.identity.phone_number', $userInfo->getPhoneNumber());
            shop_setting_set('paypal.identity.email', $userInfo->getEmail());
            shop_setting_set('paypal.identity.age_range', $userInfo->getAgeRange());
            shop_setting_set('paypal.identity.birthday', $userInfo->getBirthday());

            flash('PayPal was connected successfully.');
            return redirect('/admin/setup/billing');

        } catch (PayPalConnectionException $e) {
            flash('An error occured while trying to connect PayPal.', 'danger');
            return redirect('/admin/setup/billing');
        }
    }
}
