<?php

namespace App\Checkout;

class Fees
{
    /**
     * The shop database name.
     *
     * @var string
     */
    protected $shop;

    /**
     * The shop's fee plan.
     *
     * @var string
     */
    protected $plan;

    /**
     * Shops where we apply custom fees.
     *
     * @var array
     */
    protected $customShops = [
        'pinkcoconut',
        'lindylous',
    ];

    /**
     * The shop database name.
     *
     * @param string $shop
     * @param string $plan
     */
    public function __construct($shop, $plan = null)
    {
        $this->shop = $shop;
        $this->plan = $plan;
    }

    /**
     * Calculate the fees from the shopping cart in cents.
     *
     * @param  \App\ShoppingCart $cart
     * @return float
     */
    public function getFromCart($cart)
    {
        // don't apply fees to shipping or tax for some shops
        if ($this->isCustomShop()) {
            $cents = $cart->toCents($cart->totalWithoutShippingAndTax());

        } else {
            $cents = $cart->totalInCents();
        }

        return intval(round($this->getFeesPercentage() * $cents));
    }

    public function getFeesPercentage()
    {
        if ($this->shop == 'theboutiquehub') {
            return 0;
        }

        // 2% for specific shops
        if ($this->isCustomShop()) {
            return 2 / 100;
        }

        if (! is_null($this->plan)) {
            if ($this->plan == 'starter') {
                return 5 / 100; // 5% for the $49/month 'Starter' plan
            } elseif ($this->plan == 'business') {
                return 3 / 100; // 3% for the $149/month 'Business' plan
            } elseif ($this->plan == 'boutique_hub_starter') {
                return 5 / 100; // 5% for $24.99/month
            } elseif ($this->plan == 'boutique_hub_business') {
                return 3 / 100; // 3% for $99/month
            }
        }

        // 3% lifetime value for early birds
        return 3 / 100;
    }

    public function isCustomShop()
    {
        return in_array($this->shop, $this->customShops);
    }
}
