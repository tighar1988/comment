<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\PayPalComissionCharge;

class PayPalComissionChargeTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(PayPalComissionCharge::class, new PayPalComissionCharge);
    }
}
