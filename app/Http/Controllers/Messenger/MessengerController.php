<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Jobs\HandleFacebookMessage;
use Illuminate\Http\Request;
use Log;

class MessengerController extends Controller
{
    /**
     * Facebook Messenger Webhook
     *
     * @return \Illuminate\Http\Response
     */
    public function incoming(Request $request)
    {
        Log::info($request->input());

        // Validate the webhook
        // if ($request->has('hub_challenge')) {
        //     return $request->input('hub_challenge');
        // }

        // Ignore reading updates or our responses
        if (! $request->has('entry')) {
            return;
        }

        // Make sure this is a page subscription
        if ($request->input('object') !== 'page') {
            return;
        }

        dispatch(new HandleFacebookMessage($request->all()));
        // try {
        //     (new HandleFacebookMessage($request->all()))->handle();
        // } catch (\Exception $e) {
        //     Log::info($e->getMessage());
        // }
    }
}
