<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mails\ShopMailer;
use App\Models\ShopSettings;
use App\Jobs\FacebookHarvestGroups;
use App\Jobs\ShoppingCartExpire;
use App\Jobs\CollectPayPalFees;
use App\Jobs\FacebookScheduledPost;
use App\Jobs\InstagramHarvest;
use App\Jobs\UpdateRedisCache;
use App\Jobs\FacebookScheduleRepost;
use App\Jobs\WaitlistActivate;
use App\Models\Master;
use DB;
use Carbon\Carbon;
use Config;
use Exception;
use App\Jobs\Traits\ReconnectShopDB;

class ShopMasterJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    /**
     * The shop database name.
     *
     * @var string
     */
    protected $shop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop)
    {
        $this->shop = $shop;
    }

    public function handle()
    {
        try {
            $startTime = microtime(true);
            $this->reconnectDB($this->shop);

            if ($this->jobIsLocked()) {
                return;
            }

            shop_setting_set('shopbot:job-locked', true);
            log_info("[shopbot] Starting shop master job.");

            $this->runSubJob('waitlist', WaitlistActivate::class, 15); // run every 15 minute
            $this->runSubJob('harvest', FacebookHarvestGroups::class); // run every 1 minute
            $this->runSubJob('schedule', FacebookScheduledPost::class); // run every 1 minute
            $this->runSubJob('expire', ShoppingCartExpire::class, 15); // run every 15 minutes
            $this->runSubJob('instagram', InstagramHarvest::class); // run every 1 minute
            $this->runSubJob('redis-cache', UpdateRedisCache::class); // run every 1 minute
            $this->runSubJob('repost', FacebookScheduleRepost::class, 24*60*1); // run every 1 day

            log_info("[shopbot] Master job completed in (" . elapsed_time($startTime) . ") seconds.");
            shop_setting_set('shopbot:job-locked', false);

        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shopbot] [exception] {$e}");
        }
    }

    /**
     * Make sure only one job is running at a time.
     *
     * @return bollean
     */
    public function jobIsLocked()
    {
        $key = ShopSettings::where('key', 'shopbot:job-locked')->first();
        $locked = $key->value ?? null;

        if (! $locked) {
            return false;

        } else {

            $this->jobLockedForTooLong($key);
            log_notice("[shopbot] Last master job still running. Exiting..");
            return true;
        }
    }

    /**
     * If job is locked for more than 15 minutes.
     */
    public function jobLockedForTooLong($key)
    {
        $updatedAt = $key->updated_at ?? null;
        $now = Carbon::now();

        if (! is_null($updatedAt) && $now->greaterThan($updatedAt)) {
            $minutesPassed = 4 + $now->diffInMinutes($updatedAt);

            if ($minutesPassed >= 15 && $minutesPassed % 15 == 0) {
                // send an email every 15 minutes; not every minute
                ShopMailer::notifyAdmin("Job locked for {$minutesPassed} min for shop: '{$this->shop}'.");
            }
        }
    }

    /**
     * We use a 'try - catch' so that if a sub-job fails, we don't stop the other sub-jobs.
     */
    public function runSubJob($key, $job, $delayMinutes = null)
    {
        try {
            $startTime = microtime(true);

            if ($this->shouldDelay($key, $delayMinutes)) {
                return;
            }

            log_info("[shopbot][{$key}] Starting sub-job.");
            (new $job)->handle($this->shop);
            log_info("[shopbot][{$key}] Sub-job completed in (" . elapsed_time($startTime) . ") seconds.");

        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shopbot][{$key}] [exception] {$e}");
        }
    }

    public function shouldDelay($key, $delayMinutes = null)
    {
        if (! is_null($delayMinutes)) {
            $lastRun = shop_setting("shopbot:{$key}:last-run");

            if (is_null($lastRun)) {
                shop_setting_set("shopbot:{$key}:last-run", time());
                $minutesPassed = 0;
            } else {
                $minutesPassed = round((time() - $lastRun) / 60, 2);
            }

            if ($minutesPassed < $delayMinutes) {

                $delayTime = $delayMinutes < 60 ? $delayMinutes.' minutes' : round($delayMinutes/60, 2).' hours';
                $passedTime = $minutesPassed < 60 ? $minutesPassed.' minutes' : round($minutesPassed/60, 2).' hours';
                log_info("[shopbot][{$key}] Skipping sub-job. {$delayTime} not passed. {$passedTime} passed.");

                return true;
            }

            shop_setting_set("shopbot:{$key}:last-run", time());
        }

        return false;
    }
}
