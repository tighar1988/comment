<pre>
    {{ $notification }} <br/>
    Type: {{ $exception }} <br/>
    ShopDb: {{ $shopDb }} <br/>
    UserName: {{ $userName }} <br/>
    UserEmail: {{ $userEmail }} <br/>
    RequestUri: {{ $requestUri }} <br/>
    ReferereUri: {{ $refererUri }} <br/>
    QueryString: {{ $queryString }} <br/>
    RequestMethod: {{ $requestMethod }} <br/>
    AjaxRequest: @if ($ajaxRequest) Yes @endif <br/>
    RedirectUrl: {{ $redirectUrl }} <br/>
    RequestScheme: {{ $requestScheme }} <br/>
    RemoteAddr: {{ $remoteAddr }} <br/>
    ServerAddr: {{ $serverAddr }} <br/>
    HttpCookie: {{ $httpCookie }} <br/>
    HttpUserAgent: {{ $httpUserAgent }} <br/>
    ServerName: {{ $serverName }} <br/>
    RequestTime: {{ style_date($requestTime) }} <br/>
</pre>
