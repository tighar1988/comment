@extends('layouts.admin')

@section('content')
<?php
    $shopifyEnabled = shopify_enabled();
    if (shop_id() == 'glamourfarms') {
        $shopifyEnabled = false;
    }
?>

<section class="content-header">
    <h1>Posts</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Posts</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">
                    <div class="table-responsive">
                        <table id="posts-table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Image Posted</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Link to Post</th>
                                    <th>Posted</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    @if ($shopifyEnabled)
        var shopify_is_enabled = true;
    @else
        var shopify_is_enabled = false;
    @endif

    $(document).ready(function() {
        $('#posts-table').DataTable({
            "oLanguage" : {"sEmptyTable": "No posts found. Please go to the <a href='/admin/products'>Products page</a> to schedule a post"},
            "processing": true,
            "serverSide": true,
            "order": [[ 0, "desc" ]],
            "ajax": "/admin/posts/datatable",
            "bStateSave": true,
            columns: [
                { data: 'post_id', 'searchable': false, render: function(data, type, row) {
                    return '<img width="125px" height="125px" src="'+row.thumb+'" alt="'+row.product_style+'">';
                }},
                { data: 'product_style', 'searchable': true, 'orderable': true, render: function(data, type, row) {
                    if (shopify_is_enabled) {
                        return '<a href="/admin/products/'+row.product_id+'/variants">'+row.product_style+'</a>';
                    } else {
                        return '<a href="/admin/products/edit/'+row.product_id+'">'+row.product_style+'</a>';
                    }
                }},
                { data: 'inventory_quantity', 'searchable': false, 'orderable': true},
                { data: 'fb_id', 'searchable': false, 'orderable': true, render: function(data, type, row) {
                    if (row.fb_id) {
                        return '<a href="https://www.facebook.com/'+row.fb_id+'" target="_blank">View</a>';
                    }

                    return '';
                }},
                { data: 'scheduled_time', 'searchable': false, 'orderable': true,  render: function(data, type, row) {
                    var text = '';
                    if (row.is_scheduled) {
                        text = 'Scheduled for ' + row.scheduled_time;
                        if (! row.image) {
                            text += '<br/><strong style="color: red;">The post is missing an image. Plese edit the post to update the image.</strong>';
                        }
                        if (row.error_type == 'permission_error') {
                            text += '<br/><strong style="color: red;">There was a permission error when trying to post. Please make sure the group is linked to the Facebooko page.</strong>';
                        }

                    } else {
                        text = row.created_time;
                    }

                    return text;
                }},
                { data: null, 'searchable': false, 'orderable': false, className: 'text-center', render: function(data, type, row) {
                    var html = '';
                    html += '<div class="dropdown">';
                    html +=     '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
                    html +=         '<i class="fa fa-ellipsis-h fa-lg"></i>';
                    html +=     '</a>';
                    html +=     '<ul class="dropdown-menu pull-right">';

                    if (row.is_scheduled) {
                        html += '<li><a href="/admin/posts/edit/'+row.post_id+'" class="link">Edit Post</a></li>';
                    }

                    {{--
                        if (! row.is_scheduled) {
                            html += '<li><a title="Post to Facebook" href="/admin/posts/comment/'+row.post_id+'" class="fa fa-facebook">  Comment Remaining Inventory</a></li>';
                        }
                    --}}

                    html += '<li><a href="/admin/posts/delete/'+row.post_id+'" onclick="return confirm_delete(this);" class="link">Remove</a></li>';

                    html +=     '</ul>';
                    html += '</div>';
                    return html;
                }}
            ]
        });
    });
</script>
@endsection

