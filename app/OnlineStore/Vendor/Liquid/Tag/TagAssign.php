<?php

/**
 * This file is part of the Liquid package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Liquid
 */

namespace App\OnlineStore\Vendor\Liquid\Tag;

use App\OnlineStore\Vendor\Liquid\AbstractTag;
use App\OnlineStore\Vendor\Liquid\Liquid;
use App\OnlineStore\Vendor\Liquid\LiquidException;
use App\OnlineStore\Vendor\Liquid\FileSystem;
use App\OnlineStore\Vendor\Liquid\Regexp;
use App\OnlineStore\Vendor\Liquid\Context;

/**
 * Performs an assignment of one variable to another
 *
 * Example:
 *
 *     {% assign var = var %}
 *     {% assign var = "hello" | upcase %}
 */
class TagAssign extends AbstractTag
{
	/**
	 * @var string The variable to assign from
	 */
	private $from;

	/**
	 * @var string The variable to assign to
	 */
	private $to;

	/**
	 * Constructor
	 *
	 * @param string $markup
	 * @param array $tokens
	 * @param FileSystem $fileSystem
	 *
	 * @throws \Liquid\LiquidException
	 */
	public function __construct($markup, array &$tokens, FileSystem $fileSystem = null) {
		$syntaxRegexp = new Regexp('/(\w+)\s*=\s*(' . Liquid::get('QUOTED_FRAGMENT') . '+)/');

		$filterSeperatorRegexp = new Regexp('/' . Liquid::get('FILTER_SEPARATOR') . '\s*(.*)/');
		$filterSplitRegexp = new Regexp('/' . Liquid::get('FILTER_SEPARATOR') . '/');
		$filterNameRegexp = new Regexp('/\s*(\w+)/');
		$filterArgumentRegexp = new Regexp('/(?:' . Liquid::get('FILTER_ARGUMENT_SEPARATOR') . '|' . Liquid::get('ARGUMENT_SEPARATOR') . ')\s*(' . Liquid::get('QUOTED_FRAGMENT') . ')/');

		$this->filters = array();

		if ($filterSeperatorRegexp->match($markup)) {
			$filters = $filterSplitRegexp->split($filterSeperatorRegexp->matches[1]);

			foreach ($filters as $filter) {
				$filterNameRegexp->match($filter);
				if (! isset($filterNameRegexp->matches[1])) {
					continue;
				}
				$filtername = $filterNameRegexp->matches[1];

				$filterArgumentRegexp->matchAll($filter);
				if (! isset($filterArgumentRegexp->matches[1])) {
					continue;
				}
				$matches = Liquid::arrayFlatten($filterArgumentRegexp->matches[1]);

				array_push($this->filters, array($filtername, $matches));
			}
		}

		if ($syntaxRegexp->match($markup)) {
			$this->to = $syntaxRegexp->matches[1];
			$this->from = $syntaxRegexp->matches[2];
		} else {
			throw new LiquidException("Syntax Error in 'assign' - Valid syntax: assign [var] = [source]");
		}
	}

	/**
	 * Renders the tag
	 *
	 * @param Context $context
	 *
	 * @return string|void
	 */
	public function render(Context $context) {
		$output = $context->get($this->from, true);

		$currentFilter = 0;
		foreach ($this->filters as $filter) {
			$currentFilter++;
			list($filtername, $filterArgKeys) = $filter;

			$filterArgValues = array();

			foreach ($filterArgKeys as $arg_key) {
				$filterArgValues[] = $context->get($arg_key);
			}

			// if default is last filter, with only one default argument
			if ($filtername == 'default' && $currentFilter == count($this->filters) && count($filterArgKeys) == 1) {
				$filterArgValues = [$context->get($filterArgKeys[0], true)];
				$output = $context->invoke($filtername, $output, $filterArgValues);
				continue;
			}

			if (is_object($output)) {
				if ($filtername == 'json' && method_exists($output, 'toJson')) {
					$output = $output->toJson();

				} elseif (method_exists($output, 'toLiquid')) {
					$output = $output->toLiquid();
				}

				if (is_object($output) && method_exists($output, '__toString')) {
					$output = (string) $output;
				}
			}

			$output = $context->invoke($filtername, $output, $filterArgValues);
		}

		$context->set($this->to, $output, true);
	}
}
