<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>App Status</title>
    <link href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ cdn('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ cdn('/plugins/select/select2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ cdn('/css/admin-styles.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
        .navbar-static-top {
            color: #808080;
            background: #f2f2f2;
        }
        .navbar .container {
            height: 75px;
        }
        .navbar-brand {
            height: 75px;
            color: #999999;
            font-size: 30px;
            font-weight: 200;
            line-height: 45px;
        }
        .navbar-nav li,
        .navbar-nav a {
            height: 75px;
        }
        .navbar-nav>li>a {
            line-height: 45px;
            color: #b3b3b3;
            font-weight: 400;
            text-decoration: none;
            font-size: 16px;
        }
        .status-alert {
            height: 61px;
            line-height: 29px;
        }
        .disable-shop, .enable-shop {
            margin-left: 5px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/app-status">CommentSold Status</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/app-status/logs">Logs</a></li>
                <li><a href="/app-status/statistics">Statistics</a></li>
                <li><a href="/app-status/phpinfo">PhpInfo</a></li>
            </ul>
        </div>
    </nav>

    @yield('content')

    <script type="text/javascript" src="{{ cdn('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/plugins/select/select2.full.js') }}"></script>
    @yield('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            // button loading on form submits
            $('.loading-btn').submit(function() {
                $('button[type="submit"]', $(this)).button('loading');
            });
        });
    </script>
</body>

