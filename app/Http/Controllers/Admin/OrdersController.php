<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Inventory;
use App\Models\OrderProduct;
use App\Models\ScanLog;
use App\Models\User;
use App\Shipping\GoShippo;
use App\Shipping\Endicia;
use App\Models\StripeCharges;
use Exception;
use App\Mails\ShopMailer;
use App\Facebook\FacebookMessenger;
use Storage;
use App\Exceptions\ShopifyWebhookLockedException;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::search(request('q'), request('order'))
                        ->with('customer', 'orderProducts')->paginate(10);

        // show associated orders
        $inventoryId = request('with-inventory');
        $variant = null;
        if (is_numeric($inventoryId)) {
            $variant = Inventory::find($inventoryId);
            $orders = Order::search(request('q'), request('order'))
                        ->whereExists(function($query) use ($inventoryId) {
                            $query->select(\DB::raw(1))
                                ->from('orders_products')
                                ->whereRaw('orders.id = orders_products.order_id')
                                ->where('orders_products.inventory_id', '=', $inventoryId);
                        })
                        ->paginate(10);
        }

        return view('admin.orders.orders-grid', compact('orders', 'variant'));
    }

    public function open()
    {
        $orders = Order::search(request('q'), request('order'))
                        ->paid()->with('customer', 'orderProducts')->paginate(10);

        return view('admin.orders.orders-grid', compact('orders'));
    }

    public function fulfilled()
    {
        $orders = Order::search(request('q'), request('order'))
                        ->fulfilled()->with('customer', 'orderProducts')->paginate(10);

        return view('admin.orders.orders-grid', compact('orders'));
    }

    public function products($id)
    {
        $order = Order::with('customer', 'orderProducts.variant')->find($id);
        $order->sortOrderProducts();

        $scanLogs = ScanLog::whereIn('item_type', ['order', 'order-fulfilled'])->where('item_id', '=', $id)->get();
        return view('admin.orders.products', compact('order', 'scanLogs'));
    }

    public function edit($id)
    {
        $order = Order::with('customer')->find($id);
        return view('admin.orders.edit-order', compact('order'));
    }

    public function update($id)
    {
        $stateValue = request('state');
        if (request('country_code') == 'CA') {
            $stateValue = request('state_text');
        }

        // todo: add validation form request
        $order = Order::find($id);
        $order->ship_name      = request('ship_name');
        $order->email          = request('email');
        $order->street_address = request('street_address');
        $order->apartment      = request('apartment');
        $order->city           = request('city');
        $order->country_code   = request('country_code');
        $order->state          = $stateValue;
        $order->zip            = request('zip');
        $order->save();

        if (request('update_customer')) {
            $customer = $order->customer;
            $customer->name           = request('ship_name');
            $customer->email          = request('email');
            $customer->street_address = request('street_address');
            $customer->apartment      = request('apartment');
            $customer->city           = request('city');
            $customer->country_code   = request('country_code');
            $customer->state          = $stateValue;
            $customer->zip            = request('zip');
            $customer->save();
        }

        return response()->json(['status' => 1]);
    }

    public function fulfill($id)
    {
        $order = Order::with('orderProducts')->find($id);
        if ($order->isFulfilled()) {
            return response()->json(['status' => 0]);
        }

        $order->fulfill();

        (new ShopMailer)->sendOrderShippedEmail($order);
        FacebookMessenger::notifyOrderFulfilled($order);

        $picker = request()->user();
        ScanLog::logFulfilled($picker, $id);

        return response()->json(['status' => 1]);
    }

    public function pickUp($id)
    {
        $order = Order::find($id);
        if ($order->hasDetail('picked-up')) {
            return response()->json(['status' => 0]);
        }

        $order->setDetail('picked-up', true);
        $order->save();

        return response()->json(['status' => 1]);
    }

    public function return($id)
    {
        $order = Order::with('orderProducts')->find($id);
        return view('admin.orders.return-order', compact('order'));
    }

    public function printLocalPickup()
    {
        if (request('orders')) {
            $orders = Order::processing(request('orders'))->with('orderProducts.variant')
                ->where('local_pickup', '=', true)->get();

        } else {
            $orders = Order::processing()->with('orderProducts.variant')
                ->where('local_pickup', '=', true)->get();

            $orders = Order::sortOrders($orders);
        }

        $inventoryId = request('with-inventory');
        if (is_numeric($inventoryId)) {
            $orders = $orders->filter(function ($order) use ($inventoryId) {
                return $order->orderProducts->contains('inventory_id', $inventoryId);
            });
        }

        $orders->load('location');
        return view('admin.orders.print-local-pickup', compact('orders'));
    }

    public function printLabels()
    {
        if (request('orders')) {
            $order = Order::processing(request('orders'))->first();
            if (empty($order->label_url)) {
                $orders = Order::processing(request('orders'))->where('local_pickup', '=', false)->get();
            } else {
                $orders = Order::where('label_url', '=', $order->label_url)->where('local_pickup', '=', false)->get();
            }

        } else {
            $orders = Order::processing()->with('orderProducts.variant')
                ->where('local_pickup', '=', false)
                ->whereNotNull('label_url')->get();

            $orders = $orders->filter(function ($order) {
                return ! str_contains($order->label_url, '.pdf');
            });

            $orders = Order::sortOrders($orders);
        }

        $orders = $orders->groupBy(function ($item, $key) {
            return $item->label_url;
        });

        $inventoryId = request('with-inventory');
        if (is_numeric($inventoryId)) {
            $orders = $orders->filter(function ($multiOrder) use ($inventoryId) {
                foreach ($multiOrder as $order) {
                    if ($order->orderProducts->contains('inventory_id', $inventoryId)) {
                        return true;
                    }
                }
                return false;
            });
        }

        if (shop_id() == 'glamourfarms') {
            $orders = $orders->sortBy(function ($item, $key) {
                return count($item);
            });
        }

        return view('admin.orders.print-labels', compact('orders'));
    }

    public function printLabel($orderId)
    {
        $order = Order::find($orderId);
        if (str_contains($order->label_url, '.pdf')) {
            return redirect($order->label_url);
        }

        return view('admin.orders.print-one-label', compact('order'));
    }

    public function createLabel($id)
    {
        $order = Order::with('orderProducts')->find($id);
        $totalWeight = Inventory::whereIn('id', $order->orderProducts->pluck('inventory_id'))->sum('weight');

        // if the weight is not present in inventory (ex: the product was deleted)
        if ($totalWeight == 0) {
            $totalWeight = $order->orderProducts->sum('weight');
        }

        $nrProducts = $order->orderProducts->count();
        return view('admin.orders.label-for-order', compact('order', 'totalWeight', 'nrProducts'));
    }

    public function buyLabel($id)
    {
        try {
            if (request('is_multi')) {
                $orders = Order::whereIn('id', explode(',', request('multi_order_ids')))->with('orderProducts.variant')->get();
            } else {
                $orders = [Order::with('orderProducts.variant')->find($id)];
            }

            foreach ($orders as $order) {
                foreach ($order->orderProducts as $orderProduct) {
                    if (shop_id() != 'stb_boutique' && isset($orderProduct->variant->weight) && $orderProduct->variant->weight == 0) {
                        return ['status' => 1, 'rate' => ['errors' => ['A product in the order has 0 weight. Please update the weight in inventory before creating the label.']]];
                    }
                }
            }

            $order = Order::find($id);
            if (usps_enabled()) {
                $shopOwner = User::getByShopName(shop_id());
                $rate = (new Endicia($shopOwner))->buyLabelFor($order, request()->input());
            } else {
                if (empty(shop_setting('goshippo.api_token'))) {
                    return ['status' => 1, 'rate' => ['errors' => ['In order to buy labels, you need to enable goShippo in the Setup tab.']]];
                }
                $rate = (new GoShippo)->buyLabelFor($order, request()->input());
            }

            if (isset($rate['amount'])) {
                $rate['amount'] = amount($rate['amount']);
            }

            return ['status' => 1, 'rate' => $rate];

        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shipping] [buyLabel] {$e}");
            return ['status' => 0];
        }
    }

    public function autoBuyPrepaidCredit()
    {
        $shopOwner = User::getByShopName(shop_id());
        $shopOwner->autoBuyPrepaidCredit(request('rate_amount'));
        return ['status' => 1];
    }

    public function confirmBuyLabel($id)
    {
        try {
            $order = Order::find($id);

            if (usps_enabled()) {

                if (! empty($order->tracking_number)) {
                    return ['status' => 1, 'label' => ['errors' => ['This order already has a label. Please refund the first label before buying the second one.']]];
                }

                $shopOwner = User::getByShopName(shop_id());
                $label = (new Endicia($shopOwner))->confirmBuyLabel($order, request()->input());

                if (isset($label['errors'][0]) && str_contains($label['errors'][0], 'There is not enough money in the account')) {
                    ShopMailer::notifyAdmin("There isn't enough money in the Endicia master account.");
                }

                if (isset($label['image'])) {
                    $filename = shop_id().'/labels/'.uniqid().'-'.$order->id.'.gif';
                    Storage::put($filename, base64_decode($label['image']), 'public');
                    $label['label_url'] = Storage::url($filename);
                }

                if (isset($label['label_url']) && isset($label['tracking_number'])) {
                    $order->tracking_number = $label['tracking_number'];
                    $order->label_url = $label['label_url'];
                    $order->setDetails($label['details']);
                    $order->shipped_date = time();
                    $order->save();

                    $order->addToField('ship_cost', $label['ship_cost']);
                    $order->addToField('cs_ship_charged', $label['cs_ship_charged']);

                    $shopOwner->reducePrepaidCredit($label['ship_cost']);

                    // alert admin in case we are charged more than our customers
                    if ($label['ship_cost'] < $label['cs_ship_charged']) {
                       ShopMailer::notifyAdmin('We are losing money on the label $'.amount($label['ship_cost']).', cost $'.amount($label['cs_ship_charged']).', order #'.$order->id.'.');
                    }

                    log_info("Subtracted label prepaid credit surcharge \$".amount($label['ship_cost']).", cost \$".amount($label['cs_ship_charged'])." on order #{$order->id}");
                }

            } else {
                $label = (new GoShippo)->confirmBuyLabel($order, request('rate_id'));
                if (isset($label['label_url']) && isset($label['tracking_number'])) {
                    $order->tracking_number = $label['tracking_number'];
                    $order->label_url = $label['label_url'];
                    $order->ship_cost = $label['ship_cost'];
                    $order->save();
                }
            }

            // update the label url and tracking number on the multi orders
            if (request('is_multi')) {
                Order::whereIn('id', explode(',', request('multi_order_ids')))->update([
                    'label_url'       => $order->label_url,
                    'tracking_number' => $order->tracking_number,
                ]);
            }

            return ['status' => 1, 'label' => $label];
        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shipping] [confirmBuyLabel] {$e}");
            return ['status' => 0];
        }
    }

    public function refundLabel($id)
    {
        try {
            $order = Order::find($id);
            if (! $order->canRefundLabel()) {
                return ['status' => 0];
            }

            $shopOwner = User::getByShopName(shop_id());
            $refunded = (new Endicia($shopOwner))->refundLabel($order);

            if ($refunded) {
                $order->ship_cost = null;
                $order->cs_ship_charged = null;
                $order->tracking_number = null;
                $order->label_url = null;

                $shopOwner->addPrepaidCredit($order->details['ship_cost']);
                log_info("Refunded label, added back prepaid credit \$".amount($order->details['ship_cost']).", cost \$".amount($order->details['cs_ship_charged'])." on order #{$order->id}");

                $tmp = $order->details;
                unset($tmp['ship_cost']);
                unset($tmp['cs_ship_charged']);
                unset($tmp['pic']);

                $order->details = $tmp;
                $order->save();

                return ['status' => 1];

            } else {
                return ['status' => 0];
            }

        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shipping] [refundLabel] {$e}");
            return ['status' => 0];
        }
    }

    public function massCreateLabels()
    {
        // print only processing labels that don't have a label
        $orders = Order::processing()->with('customer', 'orderProducts')
            ->whereNull('label_url')
            ->where('local_pickup', '=', false)
            ->get();

        // calculate the weight for each order
        foreach ($orders as $order) {
            $totalWeight = Inventory::whereIn('id', $order->orderProducts->pluck('inventory_id'))->sum('weight');
            // if the weight is not present in inventory (ex: the product was deleted)
            if ($totalWeight == 0) {
                $totalWeight = $order->orderProducts->sum('weight');
            }

            $order->total_weight = $totalWeight;
        }

        if (shop_setting('shipping.combine-orders')) {
            $orders = $orders->groupBy(function ($item, $key) {
                if (shop_id() == 'cheekys') {
                    // don't combine 'bitty' orders
                    foreach ($item->orderProducts as $op) {
                        if (str_contains(strtolower($op->product_style), 'bitty') || str_contains(strtolower($op->prod_name), 'bitty')) {
                            return uniqid();
                        }
                    }
                }

                return "{$item->customer_id}:{$item->street_address}:{$item->apartment}:{$item->city}:{$item->state}:{$item->zip}:{$item->ship_name}";
            })->sortByDesc(function ($item, $key) {
                return count($item);
            });
        }

        return view('admin.orders.mass-create-labels', compact('orders'));
    }

    public function markLabelsAsPrinted()
    {
        $orders = Order::processing()
            ->whereNotNull('label_url')
            ->where('local_pickup', '=', false)
            ->where('order_status', '!=', Order::STATUS_FULFILLED)
            ->update(['order_status' => Order::STATUS_PRINTED]);

        return back()->with('labels-printed', true);
    }

    public function markLocalPickupAsPrinted()
    {
        $orders = Order::processing()
            ->where('local_pickup', '=', true)
            ->where('order_status', '!=', Order::STATUS_FULFILLED)
            ->update(['order_status' => Order::STATUS_PRINTED]);

        return back()->with('local-pickup-printed', true);
    }

    public function returnProduct($id)
    {
        $orderProduct = OrderProduct::find($id);
        if ($orderProduct->returned_date > 0) {
            return response()->json(['status' => 0]);
        }

        $orderProduct->returned_date = time();
        $orderProduct->save();

        if (request('back_to_inventory') === 'true') {
            $variant = $orderProduct->variant;
            if (! is_null($variant)) {
                try {
                    $variant->incrementQuantity('Item Returned');
                    // todo: also show tag
                    return response()->json(['status' => 1, 'location' => $variant->location, 'message' => 'Product added back to inventory!']);

                } catch (ShopifyWebhookLockedException $e) {
                    ShopMailer::notifyAdmin($e);
                    log_error("[shopify] $e");
                    return response()->json(['status' => 2, 'message' => 'Shopify connection error. Product could not be added back to inventory. Please refresh and try again!']);
                }

            } else {
                return response()->json(['status' => 1, 'message' => 'Product marked as returned. It could not be added back to inventory because the product was deleted!']);
            }
        }

        return response()->json(['status' => 1, 'message' => 'Product marked as returned without adding it back to inventory!']);
    }

    public function refundProduct($id)
    {
        $orderProduct = OrderProduct::find($id);
        if ($orderProduct->refund_issued > 0) {
            return response()->json(['status' => 0]);
        }

        $order = $orderProduct->order;
        $orderProduct->refund_issued = $orderProduct->getRefundAmount($order);
        $orderProduct->save();

        $message = 'Order Return: refunded for order #' . $order->id;
        $order->customer->addBalance($orderProduct->refund_issued, $message);

        (new ShopMailer)->sendAwardedAccountCreditEmail($order->customer, $orderProduct->refund_issued);

        return response()->json(['status' => 1]);
    }

    public function processAllOpenOrders()
    {
        Order::processAllOpenOrders();
        flash('Status updated to Processing for all Open Orders!');
        return back();
    }

    public function processSelectedOpenOrders()
    {
        Order::processSelectedOpenOrders(explode(',', request('order_ids')));
        flash('Status updated to Processing for selected Open Orders!');
        return back();
    }

    public function scanBarcode()
    {
        $picker = request()->user();
        ScanLog::log($picker, request('barcode'));
        return response()->json(['status' => 1]);
    }
}
