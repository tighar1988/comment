<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;

class Settings extends Drop
{
    protected $filesystem;

    public function __construct($filesystem)
    {
        $this->filesystem = $filesystem;

        CurrentView::$settingsData = json_decode($this->filesystem->readConfigFile('settings_data.json'), true);
        CurrentView::$settingsSchema = json_decode($this->filesystem->readConfigFile('settings_schema.json'), true);
    }

    public function handle_key($key)
    {
        return CurrentView::$settingsData['current'][$key] ?? null;
    }
}
