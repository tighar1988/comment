<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code'        => 'required|max:30|unique_coupon',
            'coupon_type' => 'required|in:S,P,F',
            'max_usage'   => 'required|integer',
            'days_valid'  => 'nullable|integer',
            'description' => 'nullable|max:255',
        ];

        if (! empty($this->starts_at) && ! empty($this->expires_at)) {
            $rules['starts_at'] = 'before:expires_at';
        }

        if ($this->minimum_purchase == '1') {
            $rules['minimum_purchase_amount'] = 'required|numeric|min:1';
        }

        if ($this->new_users_period == '1') {
            $rules['max_usage'] = '';
        }

        if ($this->coupon_type == 'P' || $this->coupon_type == 'F') {
            $rules['amount'] = 'required|numeric';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'code.required'        => 'You must provide a coupon code.',
            'coupon_type.required' => 'You must provide a coupon type.',
            'amount.required'      => 'You must provide an amount.',
        ];
    }
}
