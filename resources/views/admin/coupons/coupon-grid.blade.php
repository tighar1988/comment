@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Coupons</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Coupons</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">
                    <a href="/admin/coupons/add" class="btn btn-success add-btn btn-sm">Add Coupon</a>
                    <div class="table-responsive">
                        <table id="coupons-table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Active</th>
                                    <th>Type/Amount</th>
                                    <th>Max Usage</th>
                                    <th>Start</th>
                                    <th>Expires</th>
                                    <th># of orders</th>
                                    <th>Description</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#coupons-table').DataTable({
            "oLanguage" : {"sEmptyTable": "No coupons found. Please <a href='/admin/coupons/add'>Create one</a>"},
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/coupons/datatable",
            "order": [],
            "bStateSave": true,
            columns: [
                { data: 'code'},
                { data: 'is_active', 'searchable': false, render: function(data, type, row) {
                    if (parseInt(row.is_active) == 1) {
                        return '<span class="label label-info">Active</span>';
                    } else {
                        return '<span class="label label-danger">Inactive</span>';
                    }
                }},
                { data: 'coupon_type', 'searchable': false, render: function(data, type, row) {
                    var text = '';
                    if (row.coupon_type == 'S') {
                        text = 'Free Shipping';
                    } else if (row.coupon_type == 'F') {
                        text = 'Flat off - $'+row.amount;
                    } else if (row.coupon_type == 'P') {
                        text = 'Percent off - %'+row.amount;
                    }

                    if (typeof row.options.onlyOnce != 'undefined' && row.options.onlyOnce == '1') {
                        text += ' (Once per customer)';
                    }

                    if (typeof row.options.minPurchaseAmount != 'undefined' && ! isNaN(parseInt(row.options.minPurchaseAmount))) {
                        text += ' (Minimum purchase amount <strong>$'+parseFloat(row.options.minPurchaseAmount).toFixed(2)+'</strong>)';
                    }

                    if (typeof row.options.userId != 'undefined' && typeof row.options.userName != 'undefined') {
                        text += ' ('+row.options.userName+') #'+row.options.userId+')';
                    }

                    if (typeof row.options.newUsersPeriod != 'undefined') {
                        text += ' (Days valid after registering: '+row.options.newUsersPeriod+')';
                    }

                    return text;
                }},
                { data: 'max_usage', 'searchable': false, render: function(data, type, row) {
                    if (typeof row.options.newUsersPeriod != 'undefined' || parseInt(row.max_usage) == 0) {
                        return 'Unlimited';
                    }

                    return parseInt(row.max_usage);
                }},
                { data: 'starts_at', 'searchable': false, render: function(data, type, row) {
                    return row.starts_at_date;
                }},
                { data: 'expires_at', 'searchable': false, render: function(data, type, row) {
                    return row.expires_at_date;
                }},
                { data: 'order_count', 'searchable': false },
                { data: 'options', 'searchable': false, render: function(data, type, row) {
                    if (typeof row.options.description != 'undefined') {
                        return row.options.description;
                    }
                    return '';
                }},
                { data: null, 'searchable': false, 'orderable': false, className: 'text-center', render: function(data, type, row) {
                    var html = '';
                    html += '<div class="dropdown">';
                    html +=     '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
                    html +=         '<i class="fa fa-ellipsis-h fa-lg"></i>';
                    html +=     '</a>';
                    html +=     '<ul class="dropdown-menu pull-right">';
                    html +=         '<li><a href="/admin/coupons/delete/'+row.id+'" onclick="return confirm_delete(this);">Remove</a></li>';
                    html +=     '</ul>';
                    html += '</div>';
                    return html;
                }}
            ]
        });
    });
</script>
@endsection
