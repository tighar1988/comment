<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class AddVariantFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'color' => 'nullable|max:255',
            'size'  => 'nullable|max:255',
            'cost'  => 'required|numeric|min:1',
            'price' => 'required|numeric|min:1',
        ];

        if (shop_id() == 'shopentourageclothing') {
            $rules['cost'] = 'required|numeric|min:0';
            $rules['price'] = 'required|numeric|min:0';
        }

        if ($this->product_type == 'giftcard') {
            $rules['cost'] = 'required|numeric|min:0';
        }

        return $rules;
    }
}
