<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use DB;

class Order extends Model
{
    /**
     * The order 'paid' status.
     *
     * @var integer
     */
    const STATUS_PAID = 1;

    /**
     * The order 'fulfilled' (or shipped) status.
     *
     * @var integer
     */
    const STATUS_FULFILLED = 2;

    /**
     * The order 'returned' status.
     *
     * @var integer
     */
    const STATUS_RETURNED = 3;

    /**
     * The order 'processing' status.
     *
     * @var integer
     */
    const STATUS_PROCESSING = 4;

    /**
     * The order 'printed' status. This is a 'processing' order with a printed label.
     *
     * @var integer
     */
    const STATUS_PRINTED = 5;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'street_address',
        'apartment',
        'city',
        'state',
        'country_code',
        'zip',
        'email',
        'order_status',
        'payment_ref',
        'transaction_id',
        'payment_amt',
        'ship_charged',
        'ship_cost',
        'subtotal',
        'total',
        'fbid',
        'ship_name',
        'payment_date',
        'shipped_date',
        'tracking_number',
        'state_tax',
        'county_tax',
        'municipal_tax',
        'tax_total',
        'coupon',
        'coupon_discount',
        'calculated_zip',
        'apply_balance',
        'local_pickup',
        'store_location_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'array',
    ];

    public function setDetails($details)
    {
        foreach ((array) $details as $key => $value) {
            $this->setDetail($key, $value);
        }
    }

    public function setDetail($key, $value)
    {
        $details = $this->details;
        if (! is_array($details)) {
            $details = [];
        }

        $details[$key] = $value;

        $this->details = $details;
    }

    public function hasDetail($detail)
    {
        if (isset($this->details[$detail])) {
            return true;
        }

        return false;
    }

    public function getDetail($detail)
    {
        if (isset($this->details[$detail])) {
            return $this->details[$detail];
        }

        return null;
    }

    public function isLocalPickup()
    {
        return (bool) $this->local_pickup;
    }

    public function isPaid()
    {
        return $this->order_status == static::STATUS_PAID || $this->order_status == static::STATUS_PROCESSING || $this->order_status == static::STATUS_PRINTED;
    }

    public function isProcessing()
    {
        return $this->order_status == static::STATUS_PROCESSING;
    }

    public function isPrinted()
    {
        return $this->order_status == static::STATUS_PRINTED;
    }

    public function isFulfilled()
    {
        return ($this->order_status == static::STATUS_FULFILLED) || ($this->order_status == static::STATUS_RETURNED);
    }

    public function fulfill()
    {
        $this->order_status = static::STATUS_FULFILLED;
        $this->save();
    }

    public function paidCount()
    {
        return $this->paid()->count();
    }

    public function fulfilledCount()
    {
        return $this->fulfilled()->count();
    }

    public function firstFulfilledAt()
    {
        $fulfilledAt = null;

        $firstOrder = $this->fulfilled()->first();
        if ($firstOrder && $firstOrder->updated_at instanceof \Carbon\Carbon) {
            $fulfilledAt = $firstOrder->updated_at->getTimestamp();
        }

        return $fulfilledAt;
    }

    public function scopePaid($query)
    {
        return $query->where('order_status', static::STATUS_PAID)
            ->orWhere('order_status', static::STATUS_PROCESSING)
            ->orWhere('order_status', static::STATUS_PRINTED);
    }

    public function scopeSinceTime($query, $since)
    {
        return $query->where('payment_date', '>', strtotime($since));
    }

    public function scopeProcessing($query, $orderIds = null)
    {
        if (! empty($orderIds)) {
            $ids = [];
            foreach (explode(',', $orderIds) as $id) {
                if (! empty(trim($id))) {
                    $ids[] = trim($id);
                }
            }
            return $query->whereIn('id', $ids);
        }

        return $query->where('order_status', static::STATUS_PROCESSING);
    }

    public function scopeSearch($query, $search, $order = null)
    {
        if (! is_null($order) && is_numeric($order)) {
            $query->where('id', '=', $order);

        } elseif (! is_null($search)) {
            $query->where('street_address', 'LIKE', "%{$search}%")
            ->orWhere('apartment', 'LIKE', "%{$search}%")
            ->orWhere('city', 'LIKE', "%{$search}%")
            ->orWhere('state', 'LIKE', "%{$search}%")
            ->orWhere('zip', 'LIKE', "%{$search}%")
            ->orWhere('email', 'LIKE', "%{$search}%")
            ->orWhere('payment_ref', 'LIKE', "%{$search}%")
            ->orWhere('ship_name', 'LIKE', "%{$search}%")
            ->orWhere('total', 'LIKE', "%{$search}%");

            if (is_numeric($search)) {
                $query->orWhere('id', '=', $search);
            }
        }

        return $query;
    }

    public function scopeFulfilled($query)
    {
        return $query->where('order_status', static::STATUS_FULFILLED)
                    ->orWhere('order_status', static::STATUS_RETURNED);
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->whereBetween('payment_date', [$start, $end]);
    }

    public function orderProducts()
    {
        return $this->hasMany('App\Models\OrderProduct', 'order_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\ShopUser', 'customer_id');
    }

    public static function processAllOpenOrders()
    {
        // not received product ids
        $notReceived = Product::notReceived()->pluck('id')->toArray();

        // get skipped orders ids
        $skipOrders = static::where('order_status', static::STATUS_PAID)
            ->join('orders_products', 'orders.id', '=', 'orders_products.order_id')
            ->whereIn('orders_products.product_id', $notReceived)
            ->pluck('orders.id')
            ->toArray();
        $skipOrders = array_unique($skipOrders);

        // updated orders that are not skipped
        static::where('order_status', static::STATUS_PAID)
            ->whereNotIn('id', $skipOrders)
            ->update(['order_status' => static::STATUS_PROCESSING]);
    }

    public static function processSelectedOpenOrders($orderIds)
    {
        static::where('order_status', static::STATUS_PAID)
            ->whereIn('id', $orderIds)
            ->update(['order_status' => static::STATUS_PROCESSING]);
    }

    public static function getPayPalOrdersTotalSince($sinceTime)
    {
        $query = DB::table('orders');

        $query->where('payment_ref', 'like', "%PAY-%");

        if (! is_null($sinceTime)) {
            $query->where('payment_date', '>=', $sinceTime);
        }

        return $query;
    }

    public function getBarcode()
    {
        $orderId = $this->id;

        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG;
        return base64_encode($generator->getBarcode('order-' . $orderId, $generator::TYPE_CODE_128, '2'));
    }

    public function paymentMethod()
    {
        if (strpos($this->payment_ref, 'PAY-') !== false) {
            return 'PayPal';
        } elseif ($this->payment_ref == 'CREDIT') {
            return 'Account Credit';
        } else {
            return 'Credit Card';
        }
    }

    public function canRefundLabel()
    {
        if (empty($this->cs_ship_charged)) {
            return false;
        }

        if (! isset($this->details['pic'])) {
            return false;
        }

        return true;
    }

    public function addToField($field, $amount)
    {
        $amount = floatval(abs($amount));
        static::whereId($this->id)->update([$field => DB::raw("COALESCE({$field}, 0) + {$amount}")]);

        $this->{$field} += $amount;
    }

    public function reduceFromField($field, $amount)
    {
        $amount = floatval(abs($amount));

        static::whereId($this->id)->where($field, '>', 0)->update([
            $field => DB::raw("GREATEST(COALESCE({$field}, 0) - {$amount}, 0)"),
        ]);

        $this->{$field} = max($this->{$field} - $amount, 0);
    }

    public function sortOrderProducts()
    {
        if (count($this->orderProducts) > 1) {
            $this->orderProducts = $this->orderProducts->sortBy(function ($orderProduct, $key) {
                return $orderProduct->sortKey();
            });
        }
    }

    public static function sortOrders($orders)
    {
        // first sort all order products in all orders
        foreach ($orders as $order) {
            $order->sortOrderProducts();
        }

        // sort by the first item in the order
        return $orders->sortBy(function ($order, $key) {
            return isset($order->orderProducts[0]) ? $order->orderProducts[0]->sortKey() : null;
        });
    }

    public function location()
    {
        return $this->hasOne('App\Models\MultipleStoreLocation', 'id', 'store_location_id')->withTrashed();
    }

    public function getCountryCode()
    {
        if (empty($this->country_code)) {
            return 'US';
        }

        return $this->country_code;
    }
}
