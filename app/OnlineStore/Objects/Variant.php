<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Inventory as InventoryModel;
use App\Models\Product as ProductModel;
use App\OnlineStore\CurrentView;

class Variant extends Drop
{
    protected $inventory;

    protected $product;

    public function __construct(InventoryModel $inventory, ProductModel $product)
    {
        $this->inventory = $inventory;
        $this->product = $product;
    }

    public function toJson()
    {
        $featuredImage = $this->image();
        if (is_object($featuredImage)) {
            if (str_contains($featuredImage->src(), 'cdn.shopify.com')) {
                $featuredImage = $featuredImage->src();
            } else {
                $featuredImage = cdn_shop($featuredImage->src(), 'images');
            }
        }

        return [
            'id'                   => $this->id(),
            'title'                => $this->title(),
            'option1'              => $this->option1(),
            'option2'              => $this->option2(),
            'option3'              => $this->option3(),
            'sku'                  => $this->sku(),
            'requires_shipping'    => $this->requires_shipping(),
            'taxable'              => $this->taxable(),
            'featured_image'       => $featuredImage,
            'available'            => $this->available(),
            'name'                 => $this->name(),
            'public_title'         => $this->title(),
            'options'              => $this->options(),
            'price'                => $this->price(),
            'weight'               => $this->weight(),
            'compare_at_price'     => $this->compare_at_price(),
            'inventory_quantity'   => $this->inventory_quantity(),
            'inventory_management' => $this->inventory_management(),
            'inventory_policy'     => $this->inventory_policy(),
            'barcode'              => $this->barcode(),
        ];
    }

    public function __toString()
    {
        return (string) $this->inventory->id;
    }

    /**
     * Returns true if the variant is available for purchase, or false if it not. For a
     * variant to be available, its variant.inventory_quantity must be greater than zero
     * or variant.inventory_policy must be set to continue. A variant with no
     * variant.inventory_management is also considered available.
     */
    public function available()
    {
        if ($this->product->product_type == 'giftcard') {
            return true;
        }

        return $this->inventory->quantity > 0;
    }

    /**
     * Returns the variant's barcode.
     */
    public function barcode()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the variant's compare at price. Use a money filter to return the value in a monetary format.
     */
    public function compare_at_price()
    {
        return dollars_to_cents($this->inventory->price);
    }

    /**
     * Returns the variant's unique ID.
     */
    public function id()
    {
        return $this->inventory->id;
    }

    /**
     * Returns the image object associated with the variant.
     *
     * Input
     * {{ variant.image.src }}
     *
     * Output
     * products/red-shirt.jpeg
     */
    public function image()
    {
        if ($this->product->image) {
            return new Image($this->product->image, $this->product);
        }

        return null;
    }

    /**
     * Returns true if the variant has incoming inventory.
     */
    public function incoming()
    {
        return false;
    }

    /**
     * Returns the variant's inventory tracking service.
     */
    public function inventory_management()
    {
        // todo: implement
        return 'shopify';
    }

    /**
     * Returns the string continue if the "Allow users to purchase this item, even
     * if it is no longer in stock." checkbox is checked in the variant options in
     * the Admin. Returns deny if it is unchecked.
     */
    public function inventory_policy()
    {
        // todo: implement
        return 'deny';
    }

    /**
     * Returns the variant's inventory quantity.
     */
    public function inventory_quantity()
    {
        return $this->inventory->quantity;
    }

    /**
     * Returns the date when the next incoming inventory will arrive.
     */
    public function next_incoming_date()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the value of the variant's first option.
     */
    public function option1()
    {
        if (empty($this->inventory->color)) {
            if (! empty($this->inventory->size)) {
                // return size as the first option
                return $this->inventory->size;
            } else {
                return [''];
            }
        }

        return $this->inventory->color;
    }

    /**
     * Returns the value of the variant's second option.
     */
    public function option2()
    {
        if (empty($this->inventory->color)) {
            // don't return option 2 if we return it in option 1
            return null;
        }

        if (empty($this->inventory->size)) {
            return null;
        }

        return $this->inventory->size;
    }

    /**
     * Returns the value of the variant's third option.
     */
    public function option3()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the variant's price. Use a money filter to return the value in a monetary format.
     */
    public function price()
    {
        return dollars_to_cents($this->inventory->priceOrSalePrice());
    }

    /**
     * Returns true if the variant is set to require shipping, or false if it does not.
     */
    public function requires_shipping()
    {
        // todo: implement
        if ($this->product->product_type == 'giftcard') {
            return false;
        }

        return true;
    }

    /**
     * Returns true if the variant is currently selected by the ?variant= URL parameter, or false if it is not.
     */
    public function selected()
    {
        if (isset(CurrentView::$queryString['variant']) && is_numeric(CurrentView::$queryString['variant'])) {
            return CurrentView::$queryString['variant'];
        }

        return null;
    }

    /**
     * Returns the variant's SKU.
     */
    public function sku()
    {
        // todo: implement sku's by variant
        return $this->product->style;
    }

    /**
     * Returns true if taxes are charged for the variant, or false if they are not.
     */
    public function taxable()
    {
        // todo: implement taxable by variant
        return $this->product->isTaxable();
    }

    /**
     * Returns the concatenation of all the variant's option values, joined by / characters.
     *
     * Input
     * <!-- If variant's option1, option2, and option3 are "Red", "Small", "Wool", respectively -->
     * {{ variant.title }}
     *
     * Output
     * Red / Small / Wool
     */
    public function title()
    {
        return implode('/', $this->options());
    }

    /**
     * Returns a URL that is unique to only one product variant. The variant ID is used as the unique identifier.
     *
     * Input
     * {{ variant.url }}
     *
     * Output
     * http://my-store.myshopify.com/products/t-shirt?variant=12345678
     */
    public function url()
    {
        return '/products/'.$this->product->url.'?variant='.$this->variant->id;
    }

    /**
     * Returns the variant's weight in grams. Use the weight_with_unit filter to convert it
     * to your store's weight format or the weight unit configured on the variant.
     */
    public function weight()
    {
        return round($this->inventory->weight / 28.3495); // 1 oz = 28.3495 g
    }

    /**
     * Returns the unit for the weight configured on the variant. Works well paired with
     * the weight_in_unit attribute and the weight_with_unit filter.
     */
    public function weight_unit()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the weight of the product converted to the unit configured on the variant.
     * Works well paired with the weight_unit attribute and the weight_with_unit filter.
     */
    public function weight_in_unit()
    {
        // todo: implement
        return null;
    }

    /**
     * Return the product name with the options.
     *
     * Example: "Belled Sweetness Floral Dress - Teal - S"
     */
    public function name()
    {
        $name = $this->product->product_name;

        if (! empty($this->options())) {
            $name .= ' - ' . implode('-', $this->options());
        }

        return $name;
    }

    /**
     * Return the product options.
     *
     * Example: ['Blue', 'S']
     */
    public function options()
    {
        $options = [];

        if (! empty($this->inventory->color)) {
            $options[] = $this->inventory->color;
        }

        if (! empty($this->inventory->size)) {
            $options[] = $this->inventory->size;
        }

        if (empty($options)) {
            return [''];
        }

        return $options;
    }
}
