<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><br/>

    This product's inventory is not tracked by Shopify.
    <br/>
    Please update the product's Inventory Policy in Shopify to "Shopify tracks this product's inventory".
    <br/><br/>

    {{ $productName }} #{{ $productSku }} @if ($variantName) ({{ $variantName }}) @endif

    <a href="{{ url("/admin/products/{$productId}/variants") }}">View Product</a>
    <br/>
</div>
