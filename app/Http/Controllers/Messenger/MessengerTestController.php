<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Facebook\FacebookMessenger;
use Log;
use DB;
use Exception;

class MessengerTestController extends Controller
{
    /**
     * Facebook Messenger Test Webhook
     *
     * @return \Illuminate\Http\Response
     */
    public function incoming(Request $request)
    {
        Log::info('------------- Test Webhook -------------');
        Log::info($request->input());
        Log::info('----------------------------------------');

        // validate the webhook
        if ($request->has('hub_challenge')) {
            return $request->input('hub_challenge');
        }

        // Ignore reading updates or our responses
        if (! $request->has('entry')) {
            return;
        }

        try {
            set_shop_database('pinkcoconut');
            DB::setDefaultConnection('shop');
            DB::reconnect();
            $messenger = new FacebookMessenger('EAADAj92E5RkBAJXTaLCvDKbAwIWz2EIVsUAQdTGfqtgABGhoW8C3ONnzbLvuKHEgVrHgXf6sijFOx3mZApJZAzZB3SC0thaWCWo2wqQ2DKC4jZBUzd7VM3AK83nicKTyHJU6Msw8G6ZAR8KXKloQ8YZC45p5eXAUjhnsSm0CIZAtwZDZD');

            foreach ($request['entry'] as $entry) {
                foreach ($entry['messaging'] as $msg) {

                    /* Match recipient to on of our own customer's pages. For now, we know what commentsold page is, and their app-scoped user-id */
                    if ($msg['recipient']['id'] != "1700128273612246") {
                        continue;
                    }

                    if (isset($msg['message']['text']) && strtolower(trim($msg['message']['text'])) == 'post') {

                        $messenger->callSendAPI([
                            'recipient' => [
                                'id' => $msg['sender']['id'],
                            ],
                            'message' => [
                                'text' => "I got you my friend! The upcoming posts this week start on Friday at 3pm! We will let you know when it's available!",
                            ],
                        ]);
                    }

                }
            }

        } catch (Exception $e) {
            Log::info($e->getMessage());
        }
    }
}

