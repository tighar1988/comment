<?php

namespace App\OnlineStore\Vendor\Liquid\Tag;

use App\OnlineStore\Vendor\Liquid\AbstractTag;
use App\OnlineStore\Vendor\Liquid\Document;
use App\OnlineStore\Vendor\Liquid\Context;
use App\OnlineStore\Vendor\Liquid\Liquid;
use App\OnlineStore\Vendor\Liquid\LiquidException;
use App\OnlineStore\Vendor\Liquid\FileSystem;
use App\OnlineStore\Vendor\Liquid\Regexp;
use App\OnlineStore\Vendor\Liquid\Template;
use App\OnlineStore\CurrentView;
use App\OnlineStore\Objects\Section;

class TagSection extends AbstractTag
{
    /**
     * @var string The name of the template
     */
    private $sectionName;

    /**
     * @var bool True if the variable is a collection
     */
    private $collection;

    /**
     * @var mixed The value to pass to the child template as the template name
     */
    private $variable;

    /**
     * @var Document The Document that represents the included template
     */
    private $document;

    /**
     * @var string The Source Hash
     */
    protected $hash;

    /**
     * Constructor
     *
     * @param string $markup
     * @param array $tokens
     * @param FileSystem $fileSystem
     *
     * @throws \Liquid\LiquidException
     */
    public function __construct($markup, array &$tokens, FileSystem $fileSystem = null) {
        $regex = new Regexp('/("[^"]+"|\'[^\']+\')(\s+(with|for)\s+(' . Liquid::get('QUOTED_FRAGMENT') . '+))?/');

        if ($regex->match($markup)) {
            $this->sectionName = substr($regex->matches[1], 1, strlen($regex->matches[1]) - 2);
            CurrentView::$sectionName = $this->sectionName;

            if (isset($regex->matches[1])) {
                $this->collection = (isset($regex->matches[3])) ? ($regex->matches[3] == "for") : null;
                $this->variable = (isset($regex->matches[4])) ? $regex->matches[4] : null;
            }

            $this->extractAttributes($markup);

        } else {
            throw new LiquidException("Error in tag 'section' - Valid syntax: section '[section]' (with|for) [object|collection]");
        }

        parent::__construct($markup, $tokens, $fileSystem);
    }

    /**
     * Parses the tokens
     *
     * @param array $tokens
     *
     * @throws \Liquid\LiquidException
     */
    public function parse(array &$tokens) {
        if ($this->fileSystem === null) {
            throw new LiquidException("No file system");
        }

        // read the source of the template and create a new sub document
        $source = $this->fileSystem->readSectionsFile($this->sectionName);

        $this->hash = md5($source);

        $cache = Template::getCache();

        if (isset($cache)) {
            if (($this->document = $cache->read($this->hash)) != false && $this->document->checkIncludes() != true) {
            } else {
                $templateTokens = Template::tokenize($source);
                $this->document = new Document($templateTokens, $this->fileSystem);
                $cache->write($this->hash, $this->document);
            }
        } else {
            $templateTokens = Template::tokenize($source);
            $this->document = new Document($templateTokens, $this->fileSystem);
        }
    }

    /**
     * check for cached includes
     *
     * @return boolean
     */
    public function checkIncludes() {
        $cache = Template::getCache();

        if ($this->document->checkIncludes() == true) {
            return true;
        }

        $source = $this->fileSystem->readSectionsFile($this->sectionName);

        if ($cache->exists(md5($source)) && $this->hash == md5($source)) {
            return false;
        }

        return true;
    }

    /**
     * Renders the node
     *
     * @param Context $context
     *
     * @return string
     */
    public function render(Context $context) {
        $className = '';
        if (isset(CurrentView::$schemas[$this->sectionName]->class)) {
            $className = ' '.CurrentView::$schemas[$this->sectionName]->class;
        }

        $result = '<div id="shopify-section-'.$this->sectionName.'" class="shopify-section'.$className.'">';

        $variable = $context->get($this->variable);

        $context->push();

        $context->set('section', new Section($this->sectionName));

        foreach ($this->attributes as $key => $value) {
            $context->set($key, $context->get($value));
        }

        if ($this->collection) {
            foreach ($variable as $item) {
                $context->set($this->sectionName, $item);
                $result .= $this->document->render($context);
            }
        } else {
            if (!is_null($this->variable)) {
                $context->set($this->sectionName, $variable);
            }

            $result .= $this->document->render($context);
        }

        $context->pop();

        return $result.'</div>';
    }
}
