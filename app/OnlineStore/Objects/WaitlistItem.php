<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Waitlist\WaitlistItem as WaitlistItemObject;

class WaitlistItem extends Drop
{
    protected $item;

    public function __construct(WaitlistItemObject $item)
    {
        $this->item = $item;
    }

    public function id()
    {
        return $this->item->id;
    }

    public function name()
    {
        return $this->item->name;
    }

    public function price()
    {
        return $this->item->price;
    }

    public function image()
    {
        return $this->item->image;
    }

    public function color()
    {
        return $this->item->color;
    }

    public function size()
    {
        return $this->item->size;
    }

    public function created_at()
    {
        return $this->item->created_at;
    }
}
