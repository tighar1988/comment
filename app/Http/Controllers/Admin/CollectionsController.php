<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Collection;
use App\Http\Requests\Admin\CollectionFormRequest;

class CollectionsController extends Controller
{
    public function index()
    {
        $collections = Collection::all();
        return view('admin.store.collections', compact('collections'));
    }

    public function create()
    {
        return view('admin.store.collections-form');
    }

    public function store(CollectionFormRequest $request)
    {
        $collection = new Collection($request->input());

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->storePubliclyAs(shop_id().'/collection', $fileName);
            $collection->image = $fileName;
        }

        $collection->save();
        flash('Collection Saved!');
        return redirect('/admin/store/collections');
    }

    public function edit($collectionId)
    {
        $collection = Collection::find($collectionId);
        return view('admin.store.collections-form', compact('collection'));
    }

    public function update($collectionId, CollectionFormRequest $request)
    {
        $collection = Collection::find($collectionId);
        $collection->fill($request->input());

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->storePubliclyAs(shop_id().'/collection', $fileName);
            $collection->image = $fileName;
        }

        $collection->save();
        flash('Collection Updated!');
        return redirect('/admin/store/collections');
    }

    public function destroy($collectionId)
    {
        Collection::destroy($collectionId);
        flash('Collection Deleted!');
        return redirect('/admin/store/collections');
    }
}
