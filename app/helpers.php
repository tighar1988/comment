<?php

use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\HtmlString;
use App\Models\ShopSettings;
use App\Models\Master;
use Illuminate\Support\Facades\Storage;
use App\Facebook\Template;

function shop_setting($key, $default = null)
{
    // load all current settings in the php session, then if it exists load the $key from the php session

    // todo: refactor this to not make a new query for every setting, maybe load all the common shop settings in a static variable on the shop settings class the first time this function is called
    $value = resolve('App\Models\ShopSettings')->getSetting($key);

    if (is_null($value)) {
        return $default;
    }

    return $value;
}

function shop_setting_set($key, $value)
{
    // set to null the current php session that contains settings

    // todo: refresh shop settings cache for the current request
    return ShopSettings::updateOrCreate(['key' => $key], ['value' => $value]);
}

/**
 * Get the Facebook app_id
 *
 * @return string
 */
function fb_app_id()
{
    return shop_setting('facebook.client_id') ?? config('services.facebook.client_id');
}

/**
 * Get the Facebook app_secret
 *
 * @return string
 */
function fb_app_secret()
{
    return shop_setting('facebook.client_secret') ?? config('services.facebook.client_secret');
}

/**
 * Get the Facebook App Accesss Token
 *
 * @return string
 */
function fb_app_access_token()
{
    return shop_setting('facebook.app_access_token') ?? config('services.facebook.app_access_token');
}

/**
 * Get the Facebook API version
 *
 * @return string
 */
function fb_api_version()
{
    return 'v2.8';
}

/**
 * The Facebook Page ID
 *
 * @return string
 */
function fb_page_id()
{
    return shop_setting('facebook.page_id');
}

/**
 * The Facebook Page Access Token
 *
 * @return string
 */
function fb_page_access_token()
{
    return shop_setting('facebook.page_access_token');
}

/**
 * The enabled Facebook Group IDs.
 *
 * @return array
 */
function fb_enabled_group_ids()
{
    $groups = json_decode(shop_setting('facebook.group_ids'));
    $groups = is_array($groups) ? $groups : [];
    return array_unique($groups);
}

/**
 * Return how many days the shop owner's facebook access token is still valid for.
 *
 * @return int
 */
function fb_token_valid_days()
{
    $lastTime = shop_setting('facebook.time-connected');
    if (is_null($lastTime) || ! is_numeric($lastTime)) {
        return 0;
    }

    $now = time();
    $daysPassed = intval(abs($now - $lastTime) / 86400);

    // 60 is the normal valid time of a long term facebook access token
    $validDays = 60 - $daysPassed;

    return $validDays > 0 ? $validDays : 0;
}

/**
 * Get the Facebook Id of the Group Owner
 *
 * This is set in admin, by clicking 'Connect Facebook'
 *
 * todo: maybe move 'Connect Facebook' into a settings area, so the shop owner does not
 * click it my mistake (he could be logged in admin from a different pc, where there is
 * a different facebook account); let's make sure the user does not click it by mistake
 *
 * @return string
 */
function fb_group_owner_id()
{
    return shop_setting('facebook.user_id');
}

/**
 * Return the CDN url for the file
 *
 * @param  string $file
 * @return string
 */
function cdn($file)
{
    return env('CDN_URI', '').'/'.ltrim($file, '/');
}

/**
 * Return the CDN url for the file
 *
 * @param  string $file
 * @return string
 */
function cdn_shop($file, $type = 'assets')
{
    if (str_contains($file, ['bigcommerce.com', 'cdn.commentsold.com', '.shopify.com'])) {
        return $file;
    }

    $url = cdn(shop_id() . '/'. $type .'/' . ltrim($file, '/').'?v=1506034852');
    return str_replace(['https:', 'http:'], '', $url);
}

/**
 * Dump function
 */
function d()
{
    array_map(function ($x) {
        (new
         Dumper)->dump($x);
    }, func_get_args());
}

function amount($money)
{
    return number_format(floatval($money), 2);
}

function percentage($nr)
{
    return number_format(floatval($nr), 2);
}

function percentageOf($value, $total)
{
    // avoid division by zero
    if ($total == 0) {
        return '0.00';
    }

    return number_format($value / $total  * 100, 2);
}

function activeLink($path, $cssClass = 'active')
{
    if (is_array($path)) {
        foreach ($path as $p) {
            if (app('request')->is($p)) {
                return " {$cssClass}";
            }
        }
        return '';

    } else {
        return app('request')->is($path) ? " {$cssClass}" : '';
    }
}

function form_action()
{
    return url('/' . app('request')->path());
}

function selected($name, $id)
{
    return old($name) == $id ? 'selected="selected"' : '';
}

function schema_name($name)
{
    $name = trim($name);
    return str_replace('-', '_', $name);
}

function shop_url($path = '/')
{
    $path = ltrim($path, '/');

    $customDomain = config('shop.custom-domain');
    if (! is_null($customDomain)) {
        return rtrim($customDomain, '/').'/'.$path;
    }

    $shop = shop_id();
    if ($shop) {
        $shop = str_replace('_', '-', $shop);
        $schema = env('APP_SCHEMA', 'http');
        $domain = rtrim(env('APP_DOMAIN'), '/');
        return "{$schema}://{$shop}.{$domain}/{$path}";
    }

    return app('request')->root().'/'.$path;
}

function shop_id()
{
    return app('config')->get('database.connections.shop.database');
}

function set_shop_database($shop)
{
    $db = schema_name($shop);
    Config::set('database.connections.shop.database', $db);
    Config::set('queue.failed.database', 'shop');
}

function reconnect_shop_database($shop)
{
    set_shop_database($shop);
    DB::setDefaultConnection('shop');
    DB::reconnect();
}

function all_shops()
{
    return Master::shopsList();
}

function all_shops_and_users()
{
    return Master::shopsListAndUsers();
}

function shop_name()
{
    return removeNewLines(shop_setting('shop.name', 'My Shop'));
}

function shop_description()
{
    return removeNewLines(shop_setting('shop.description', 'My Shop Description'));
}

function removeNewLines($text)
{
    $text = str_replace("\r\n", ' ', $text);
    $text = str_replace("\n", ' ', $text);
    return str_replace("\r", ' ', $text);
}

function local_pickup_enabled()
{
    return shop_setting('local-pickup-enabled', false);
}

function free_shipping_24hr_enabled()
{
    return shop_setting('24hr-free-shipping-enabled', false);
}

function expire()
{
    return shop_setting('shopping-cart:expire', 24);
}

function remind()
{
    return shop_setting('shopping-cart:remind', 12);
}

function product_image($filename)
{
    if (empty($filename)) {
        return cdn('/images/default.png');
    }

    if (starts_with($filename, 'https://cdn.shopify.com') || str_contains($filename, '.bigcommerce.com')) {
        return $filename;
    }

    return Storage::url(shop_id() . '/products/' . $filename);
}

function product_thumb($filename)
{
    if (empty($filename)) {
        return cdn('/images/default.png');
    }

    if (starts_with($filename, 'https://cdn.shopify.com')) {
        $poz = strrpos($filename, '.');
        if (substr($filename, $poz - 6, 6) == '_1000x') {
            $filename = substr_replace($filename, '_290x290', $poz - 6, 6);
        } else {
            $filename = substr_replace($filename, '_290x290', $poz, 0);
        }
        return $filename;
    }

    if (str_contains($filename, '.bigcommerce.com')) {
        return $filename;
    }

    return Storage::url(shop_id() . '/products/thumbs/' . $filename);
}

function image($filename, $size = null)
{
    if (empty($filename)) {
        return cdn('/images/default.png');
    }

    $url = '/images/' . shop_id() . '/' . $filename;
    if (! is_null($size)) {
        $url .= '?size='.$size;
    }

    return cdn($url);
}

function current_day()
{
    return date('m/d/Y');
}

function current_time()
{
    // todo: use a consistent time format everywhere
    // format: January 5th, 2017 10:23 AM
    return date('F jS, Y g:i A');
}

function get_user_timezone()
{
    static $timezone = null;

    // cache the timezone so we don't make too many db calls
    if (is_null($timezone)) {
        $timezone = shop_setting('shop.timezone');
    }

    return $timezone;
}

function apply_timezone($timestamp)
{
    if (! is_numeric($timestamp)) {
        $timestamp = strtotime($timestamp);
    }

    $timezone = get_user_timezone();
    if (empty($timezone)) {
        return style_date($timestamp);
    }

    $date = new \DateTime;
    $date->setTimestamp($timestamp);
    $date->setTimezone(new \DateTimeZone($timezone));
    return $date->format('F jS, Y g:i A');
}

function style_date($timestamp)
{
    // todo: add shop timezone when displaying the date
    // \Carbon\Carbon::createFromFormat('n/j/Y H:i a', $date, shop_setting('shop.timezone'));

    if (! is_numeric($timestamp)) {
        $timestamp = strtotime($timestamp);
    }

    // todo: use a consistent date format everywhere
    // format: January 5th, 2017 10:23 AM
    return date('F jS, Y g:i A', $timestamp);
}

function shop_redirect($path)
{
    $url = app('request')->root().'/'.ltrim($path, '/');
    return redirect($url);
}

function shop_email()
{
    $shopEmail = shop_setting('shop.company_email');
    if (! empty($shopEmail) && filter_var($shopEmail, FILTER_VALIDATE_EMAIL)) {
        return $shopEmail;
    }

    return 'support@commentsold.com';
}

function log_master_info($message)
{
    resolve('App\Models\AppLog')->info($message);
}

function log_info($message)
{
    resolve('App\Models\ShopLog')->info($message);
}

function log_notice($message)
{
    resolve('App\Models\ShopLog')->notice($message);
}

function log_error($message)
{
    resolve('App\Models\ShopLog')->error($message);
}

function card_image($brand)
{
    $brand = ucfirst(strtolower($brand));

    if ($brand == 'Visa') {
        return '/images/icons/Visa-56.png';

    } elseif ($brand == 'Mastercard') {
        return '/images/icons/Mastercard-56.png';

    } elseif ($brand == 'Amex') {
        return '/images/icons/Amex-56.png';

    } elseif ($brand == 'Discover') {
        return '/images/icons/Discover-56.png';
    }

    return '';
}

function facebook_profile($facebookId)
{
    return 'https://www.facebook.com/' . $facebookId;
}

function pagination_links($collection, $search = null)
{
    if (is_array($search)) {
        foreach ($search as $key => $value) {
            if (! empty($value)){
                $collection->appends($key, $value);
            }
        }
    } elseif (! empty($search)) {
        $collection->appends('q', $search);
    }

    return $collection->links('common.pagination');
}

/**
 * Returns elapsed time in seconds
 */
function elapsed_time($start)
{
    $milliseconds = round((microtime(true) - $start) * 1000, 2);
    $seconds = round($milliseconds / 1000, 4);
    return number_format($seconds, 4);
}

/**
 * Show the Sentry js library for monitoring javascript errors.
 */
function script_raven()
{
    if (! app()->environment('testing')) {
        $html = '<script src="https://cdn.ravenjs.com/3.12.1/raven.min.js"></script>';
        $html .= '<script>Raven.config("https://15babbd3af3b410ebfa75a345656f4f6@sentry.io/147620").install()</script>';

        return new HtmlString($html);
    }
}

/**
 * Remove 4 byte UTF8 characters (because mysql throws errors for them).
 *
 * @param  string $text
 * @return string
 */
function mysql_utf8($text)
{
    return preg_replace('/[\xF0-\xF7].../s', '', $text);
}

/**
 * Return a message template.
 *
 * @param  string $key
 * @return string
 */
function template($key)
{
    return (new Template)->template($key);
}

/**
 * Return the current shop theme.
 *
 * @param  string $view
 * @return \Illuminate\Contracts\View\View
 */
function theme($view, $data = [])
{
    $theme = get_theme();

    if ($theme == 'default') {

        $file = app('App\Models\Liquid')->where('file_name', '=', $view)->first();
        if ($file) {
            $data['template'] = $file->content;
            return view("themes.liquid", compact('data'));

        } else {
            $data['template'] = str_replace('templates.', '', $view);
            return view("themes.default.{$view}", $data);
        }

    } else {
        $data['theme'] = $theme;
        $data['template'] = str_replace('templates.', '', $view);
        return view("themes.{$theme}.{$view}", $data);
    }
}

function get_theme($shopId = null)
{
    if (is_null($shopId)) {
        $shopId = shop_id();
    }

    // todo: return it dynamically from the shop settings
    if ($shopId == 'glamourfarms') {
        return 'glamourfarms';
    }

    if ($shopId == 'shopentourageclothing') {
        return 'entourageclothing';
    }

    if ($shopId == 'shop1') {
        // return 'entourage';
        // return 'shopzigzagstripe';
        // return 'glamourfarms';
        return 'cheekys';
    }

    // local dev shop
    if ($shopId == 'glamourfarms_local') {
        return 'glamourfarms';
    }

    if ($shopId == 'cheekys') {
        return 'cheekys';
    }

    return null;
}

function theme_body_id($title)
{
    $title = strtolower($title);
    $title = str_replace('.', '-', $title);
    $title = str_replace(' ', '-', $title);
    return trim($title);
}

function shop_seo_description()
{
    // todo: add this to shop settings and in Setup
    return 'We offer chic &amp; contemporary styles, with a little bit of sass, to complement every woman&#39;s natural beauty. We hope you enjoy your shopping experience.';
}

function from_email_address()
{
    // todo: move this to a shop_setting

    if (shop_id() == 'pinkcoconut') {
        return 'pinkcoconutonline@gmail.com';
    }

    if (shop_id() == 'lindylous') {
        return 'smile@lindylousboutique.com';
    }

    if (shop_id() == 'divas') {
        return 'support@shopdiscountdivas.com';
    }

    return 'support@commentsold.com';
}

function dollars_to_cents($dollars)
{
    $cents = $dollars * 100;
    $cents = (string) $cents;

    return intval($cents);
}

function zebra_enabled()
{
    // todo: move this to a shop setting
    if (shop_id() == 'fashionten' || shop_id() == 'divas' || shop_id() == 'cheekys' || shop_id() == 'julesandjamesboutique' || shop_id() == 'pinkcoconut' || shop_id() == 'bktest' || shop_id() == 'glamourfarms' || shop_id() == 'fashion15' || shop_id() == 'shopbhb') {
        return true;
    }

    return false;
}

function usps_enabled()
{
    // todo: move this to a shop setting

    if (shop_id() == 'bktest' || shop_id() == 'bellaroseboutique' || shop_id() == 'glamourdefined' || shop_id() == 'divas' || shop_id() == 'glamourfarms' || shop_id() == 'fashionten' || shop_id() == 'shopentourageclothing' || shop_id() == 'mavenmarket' || shop_id() == 'stellabeeboutique' || shop_id() == 'chloelanecollection' || shop_id() == 'shopbhb' || shop_id() == 'julesandjamesboutique' || shop_id() == 'angelsboutique' || shop_id() == 'jennarationboutique') {
        return true;
    }

    return false;
}

function unique_options($options)
{
    if (! is_array($options)) {
        return [];
    }

    $lowercase = array_unique(array_map('strtolower', $options));
    $unique = [];

    foreach ($lowercase as $lower) {
        foreach ($options as $option) {
            if ($lower == strtolower($option)) {
                $unique[] = $option;
                break;
            }
        }
    }

    return $unique;
}

function shopify_enabled()
{
    return (bool) shop_setting('shopify.enabled');
}

function shopify_url()
{
    $url = shop_setting('shopify.shopify-shop-name');
    return ! empty($url) ? $url : 'myshopify.com';
}

function multiple_locations_enabled()
{
    return shop_setting('multiple-locations-enabled', false);
}

function cache_header($keys = '', $time = 3600)
{
    $shopId = shop_id();
    $surrogateKey = $shopId;

    if (is_string($keys)) {
        $keys = explode(' ', $keys);
    }

    foreach ((array)$keys as $key) {
        $key = trim($key);
        if (! empty($key)) {
            $surrogateKey .= " {$shopId}:{$key}";
        }
    }

	return [
        'Cache-Control' => "max-age={$time}, stale-while-revalidate=240, stale-if-error=2400, public",
		'Surrogate-Key' => $surrogateKey,
		'Surrogate-Control' => "max-age=$time, stale-while-revalidate=240, stale-if-error=2400"
	];
}

/**
 * Is the store visible on the homepage, or hidden and only visible on /store?
 */
function store_visible()
{
    if (shop_id() == 'cheekys') {
        return true;
    }

    return false;
}
