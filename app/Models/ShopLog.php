<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\LineFormatter;

class ShopLog extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level',
        'message',
        'created_at',
    ];

    protected function log($level, $message)
    {
        $shop = shop_id();
        if (empty($shop)) {
            $shop = 'unknown';
        }

        $handler = new RotatingFileHandler(storage_path("logs/shops/{$shop}/shop.log"), 0, Logger::INFO, true, 0777);
        $handler->setFormatter(new LineFormatter(null, null, true, true));

        $log = new Logger($shop);
        $log->pushHandler($handler, 0, Logger::INFO);
        $log->info($message);

        // static::create([
        //     'level'      => $level,
        //     'message'    => mysql_utf8($message),
        //     'created_at' => Carbon::now(),
        // ]);
    }

    public function info($message)
    {
        $this->log('info', $message);
    }

    public function notice($message)
    {
        $this->log('notice', $message);
    }

    public function warning($message)
    {
        $this->log('warning', $message);
    }

    public function error($message)
    {
        $this->log('error', $message);
    }
}
