<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;

class PageTitle extends Drop
{
    public function __toString()
    {
        if (! empty(CurrentView::$pageTitle)) {
            return CurrentView::$pageTitle;
        }

        if (! isset($this->cache['page_title'])) {
            $this->cache['page_title'] = shop_setting('online-store.page_title', 'Homepage title');
        }

        return $this->cache['page_title'];
    }
}
