<!DOCTYPE html>
<html lang="en">
<head>
    <!-- http://templateninja.net/themes/clotheshop/v1.2/ -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="description" content="responsive clothing store template">
    <meta name="author" content="afriq yasin ramadhan">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>Clotheshop</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="logo">
                    <h1><a href="index.html">Clothe<span>shop</span> </a></h1>
                    <p>Clean and simple shopping cart</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="account">
                    <ul>
                        <li id="your-account">
                            <div class="hidden-xs">
                                <h4><a href="#">Your Account</a></h4>
                                <p>Welcome, <a href="login.html">log in</a></p>
                            </div>
                            <div class="visible-xs">
                                <a href="login.html" class="btn btn-primary"><i class="fa fa-user"></i></a>
                            </div>
                        </li>
                        <li>
                            <div class="hidden-xs">
                                <h4><a href="cart.html">Cart</a></h4>
                                <p><strong>3 Product(s)</strong></p>
                            </div>
                            <div class="visible-xs">
                                <a href="cart.html" class="btn btn-primary"><span class="cart-item">3</span> <i class="fa fa-shopping-cart"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="nav-menus">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="index.html">Home</a></li>
                        <li><a href="#">Acessories</a></li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Girl <b class="caret"></b></a>
                            <ul class="dropdown-menu" id="menu1">
                                <li><a href="#">Shirts</a></li>
                                <li><a href="#">Pants</a></li>
                                <li><a href="#">Skirts</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Edition</a></li>
                        <li><a href="#">Authorized Dealer</a></li>
                        <li><a href="about.html">About</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h2>Best Seller <small>Most sold product in this month</small></h2>
                </div>
            </div>
        </div>
        <div class="row product-container">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail product-item">
                    <a href="product_detail.html"><img alt="" src="img/product1.jpg"></a>
                    <div class="caption">
                        <h5>Casual Rock Pants</h5>
                        <p>$54.00</p>
                        <p>Available</p>
                    </div>
                    <div class="product-item-badge">New</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail product-item">
                    <a href="product_detail.html"><img alt="" src="img/product2.jpg"></a>
                    <div class="caption">
                        <h5>T-shirt</h5>
                        <p class="product-item-price">$54.00</p>
                        <p>Available</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail product-item">
                    <a href="product_detail.html"><img alt="" src="img/product3.jpg"></a>
                    <div class="caption">
                        <h5>Casual Rock Pants</h5>
                        <p><del>$54.00</del> $32.00</p>
                        <p>Available</p>
                    </div>
                    <div class="product-item-badge badge-sale">Sale</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail product-item">
                    <a href="product_detail.html"><img alt="" src="img/product4.jpg"></a>
                    <div class="caption">
                        <h5>Casual Rock T-Shirt</h5>
                        <p>$54.00</p>
                        <p>Available</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 footer">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h3><span>Contact Info</span></h3>
                            <address>
                                No. 22, Bantul, Yogyakarta, Indonesia<br>
                                Call Us : (0274) 4411005<br>
                                Email : avriqq@gmail.com<br>
                            </address>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h3><span>Customer Support</span></h3>
                            <ul class="list-unstyled list-star">
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Payment Option</a></li>
                                <li><a href="#">Booking Tips</a></li>
                                <li><a href="#">Information</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h3><span>Discover our store</span></h3>
                            <ul class="list-unstyled list-star">
                                <li><a href="#">California</a></li>
                                <li><a href="#">Bali</a></li>
                                <li><a href="#">Singapore</a></li>
                                <li><a href="#">Canada</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 copyright">
                <div class="row">
                    <div class="col-md-6 col-sm-6 copyright-left">
                        <p>Copyright &copy; Clotheshop 2012-2014. All right reserved.</p>
                    </div>
                    <div class="col-md-6 col-sm-6 copyright-right">
                        <ul class="list-unstyled list-social">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>
