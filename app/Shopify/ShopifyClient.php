<?php

namespace App\Shopify;

use App\Shopify\ShopifyCurlException;
use App\Shopify\ShopifyApiException;
use Exception;
use Log;

class ShopifyClient
{
    public $shopDomain;

    protected $token;

    protected $appKey;

    protected $appSecret;

    protected $redirectUrl;

    protected $lastResponseHeaders = null;

    public function __construct($shopDomain)
    {
        $this->shopDomain  = $shopDomain;
        $this->appKey      = config('services.shopify.app_key');
        $this->appSecret   = config('services.shopify.app_secret');
        $this->redirectUrl = config('services.shopify.redirect');
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Get the URL required to request authorization.
     */
    public function getAuthorizeUrl($nonce)
    {
        $scopes = 'read_products, write_products, read_customers, write_customers, read_orders, write_orders, read_draft_orders, write_draft_orders, read_fulfillments, write_fulfillments, read_shipping, write_shipping, read_analytics, read_reports, write_reports, read_price_rules, write_price_rules';

        return "https://{$this->shopDomain}.myshopify.com/admin/oauth/authorize?client_id={$this->appKey}&scope=" . urlencode($scopes) . "&redirect_uri=" . urlencode($this->redirectUrl) . "&state={$nonce}";
    }

    /**
     * Once the User has authorized the app, call this with the code to get the access token.
     *
     * POST to  POST https://SHOP_NAME.myshopify.com/admin/oauth/access_token
     */
    public function getAccessToken($code)
    {
        $url = "https://{$this->shopDomain}/admin/oauth/access_token";
        $payload = "client_id={$this->appKey}&client_secret={$this->appSecret}&code={$code}";

        $response = $this->curlHttpApiRequest('POST', $url, '', $payload, []);
        $response = json_decode($response, true);

        if (isset($response['access_token'])) {
            return $response['access_token'];
        }

        return null;
    }

    public function callsMade()
    {
        return $this->shopApiCallLimitParam(0);
    }

    public function callLimit()
    {
        return $this->shopApiCallLimitParam(1);
    }

    public function callsLeft()
    {
        return $this->callLimit() - $this->callsMade();
    }

    public function callAPI($method, $path, $params = [])
    {
        for ($i = 0; $i < 10; $i++) {
            try {

                return $this->call($method, $path, $params);

            } catch (ShopifyApiException $e) {
                // todo: if error contains api overflow error
                Log::error($e->getMessage());
                Log::error($e->getResponse());
                sleep(1);
            }
        }

        throw new Exception("Could not make API call for {$method}, {$path}.");
    }

    public function call($method, $path, $params = [])
    {
        $baseurl = "https://{$this->shopDomain}/";

        $url = $baseurl.ltrim($path, '/');
        $query = in_array($method, ['GET', 'DELETE']) ? $params : [];
        $payload = in_array($method, ['POST', 'PUT']) ? json_encode($params) : [];
        $request_headers = in_array($method, ['POST', 'PUT']) ? ["Content-Type: application/json; charset=utf-8", 'Expect:'] : [];

        // add auth headers
        $request_headers[] = 'X-Shopify-Access-Token: ' . $this->token;
        $response = $this->curlHttpApiRequest($method, $url, $query, $payload, $request_headers);
        $response = json_decode($response, true);

        if (isset($response['errors']) or ($this->lastResponseHeaders['http_status_code'] >= 400)) {
            throw new ShopifyApiException($method, $path, $params, $this->lastResponseHeaders, $response);
        }

        return (is_array($response) and (count($response) > 0)) ? array_shift($response) : $response;
    }

    public function validateSignature($query)
    {
        if (! is_array($query) || empty($query['hmac']) || ! is_string($query['hmac'])) {
            return false;
        }

        $dataString = [];
        foreach ($query as $key => $value) {
            $key = str_replace('=', '%3D', $key);
            $key = str_replace('&', '%26', $key);
            $key = str_replace('%', '%25', $key);
            $value = str_replace('&', '%26', $value);
            $value = str_replace('%', '%25', $value);

            if ($key != 'hmac') {
                $dataString[] = $key . '=' . $value;
            }
        }

        sort($dataString);

        $string = implode("&", $dataString);
        if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
            $signature = hash_hmac('sha256', $string, $this->appSecret);
        } else {
            $signature = bin2hex(mhash(MHASH_SHA256, $string, $this->appSecret));
        }

        return $query['hmac'] == $signature;
    }

    /**
     * Verify the webhook signature.
     *
     * @return bool
     */
    public function isValidWebhook($data, $hmacHeader)
    {
        $calculatedHmac = base64_encode(hash_hmac('sha256', $data, $this->appSecret, true));

        return ($hmacHeader == $calculatedHmac);
    }

    protected function curlHttpApiRequest($method, $url, $query='', $payload='', $request_headers=[])
    {
        $url = $this->curlAppendQuery($url, $query);
        $ch = curl_init($url);
        $this->curlSetopts($ch, $method, $payload, $request_headers);
        $response = curl_exec($ch);
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($errno) throw new ShopifyCurlException($error, $errno);
        list($message_headers, $message_body) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
        $this->lastResponseHeaders = $this->curlParseHeaders($message_headers);
        return $message_body;
    }

    protected function curlAppendQuery($url, $query)
    {
        if (empty($query)) return $url;
        if (is_array($query)) return "$url?".http_build_query($query);
        else return "$url?$query";
    }

    protected function curlSetopts($ch, $method, $payload, $request_headers)
    {
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_USERAGENT, 'ohShopify-php-api-client');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, $method);
        if (!empty($request_headers)) curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);

        if ($method != 'GET' && !empty($payload))
        {
            if (is_array($payload)) $payload = http_build_query($payload);
            curl_setopt ($ch, CURLOPT_POSTFIELDS, $payload);
        }
    }

    protected function curlParseHeaders($message_headers)
    {
        $header_lines = preg_split("/\r\n|\n|\r/", $message_headers);
        $headers = [];
        list(, $headers['http_status_code'], $headers['http_status_message']) = explode(' ', trim(array_shift($header_lines)), 3);
        foreach ($header_lines as $header_line)
        {
            list($name, $value) = explode(':', $header_line, 2);
            $name = strtolower($name);
            $headers[$name] = trim($value);
        }
        return $headers;
    }

    protected function shopApiCallLimitParam($index)
    {
        if ($this->lastResponseHeaders == null)
        {
            throw new Exception('Cannot be called before an API call.');
        }

        $params = explode('/', $this->lastResponseHeaders['http_x_shopify_shop_api_call_limit']);
        return (int) $params[$index];
    }
}
