<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Image as ImageModel;
use Image;
use Storage;
use DB;

class ImportExternalImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:external-images {shop}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import shopify and bigcommerce images into our system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shop = $this->argument('shop');
        $this->info("Image import for shop {$shop}..");

        set_shop_database($shop);
        DB::setDefaultConnection('shop');
        DB::reconnect();

        $bar = $this->output->createProgressBar(ImageModel::count());
        $shop = shop_id();

        ImageModel::orderBy('id')->chunk(100, function ($images) use ($bar, $shop) {
            foreach ($images as $image) {
                if (str_contains($image->filename, ['.shopify.com', '.bigcommerce.com'])) {

                    $contents = file_get_contents($image->filename);

                    // get extension
                    $extension = pathinfo($image->filename, PATHINFO_EXTENSION);
                    if (str_contains($extension, '?')) {
                        $extension = substr($extension, 0, strpos($extension, '?'));
                    }

                    // generate new filename
                    $newFilename = str_random(40).'.'.$extension;

                    // save the original image
                    Storage::put($shop.'/products/'.$newFilename, $contents, ['visibility' => 'public']);

                    $img = Image::make($contents);

                    // update the image filename in the db
                    $image->filename = $newFilename;
                    $image->image_width = $img->width();
                    $image->image_height = $img->height();
                    $image->save();

                    // resize the image and save it
                    if ($img->width() != $img->height()) {
                        $cutout = min($img->width(), $img->height());
                        $img->crop($cutout, $cutout);
                    }
                    $img->resize(290, 290);
                    Storage::put($shop.'/products/thumbs/'.$newFilename, $img->stream()->__toString(), 'public');
                }

                $bar->advance();
            }
        });

        $bar->finish();
        $this->info("Image import complete..");
    }
}
