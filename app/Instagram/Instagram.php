<?php

namespace App\Instagram;

use App\Instagram\InstagramProvider;

class Instagram
{
    protected $clientId;

    protected $clientSecret;

    protected $redirect;

    protected $token;

    /**
     * @var InstagramProvider
     */
    protected $provider;

    public function __construct()
    {
        $this->clientId     = config('services.instagram.client_id');
        $this->clientSecret = config('services.instagram.client_secret');
        $this->redirect     = config('services.instagram.redirect');

        $this->token = shop_setting('instagram.token');
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function makeProvider()
    {
        return new InstagramProvider(request(), $this->clientId, $this->clientSecret, $this->redirect);
    }

    public function getProvider()
    {
        if (is_null($this->provider)) {
            $this->provider = $this->makeProvider();
        }

        return $this->provider;
    }

    public function redirect()
    {
        return $this->getProvider()->redirect();
    }

    public function user()
    {
        return $this->getProvider()->user();
    }

    public function redirectShopUser($shop)
    {
        $this->redirect .= "?shop={$shop}";
        $provider = $this->makeProvider();
        $provider->setScopes(['basic']);

        return $provider->redirect();
    }

    public function shopUser($shop)
    {
        $this->redirect .= "?shop={$shop}";
        $provider = $this->makeProvider();
        $provider->setScopes(['basic']);

        return $provider->user();
    }

    public function mediaRecent()
    {
        return $this->getProvider()->mediaRecent($this->token);
    }

    public function mediaById($mediaId)
    {
        return $this->getProvider()->mediaById($this->token, $mediaId);
    }

    public function comments($mediaId)
    {
        return $this->getProvider()->comments($this->token, $mediaId);
    }

    public function getUserByName($userName)
    {
        return $this->getProvider()->userByName($this->token, $userName);
    }
}
