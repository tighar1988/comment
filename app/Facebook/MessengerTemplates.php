<?php

namespace App\Facebook;

use App\Facebook\Template;
use App\Models\Cart;

class MessengerTemplates
{
    /**
     * Customer synced messenger (first time synced only) - Send Intro Text.
     */
    public function firstOptin()
    {
        return "Welcome Shopper! Thanks for connecting! We will send you status updates about your order and your comments!";
    }

    /**
     * Customer has no email set up.
     */
    public function noEmail($customer)
    {
        $message = 'Welcome new shopper! Please go to ' . shop_url('/account') . ' to login, view your cart and update your email with us. Thanks';

        return $this->standardText($customer->user_ref, $message);
    }

    /**
     * Customer made an invalid comment.
     */
    public function invalidComment($customer, $post)
    {
        $message = (new Template)->forInvalidComment($post->product);
        $message .= ' Please re-comment on the main post here: https://www.facebook.com/' . $post->fb_id;

        return $this->standardText($customer->user_ref, $message);
    }

    /**
     * Customer waitlisted.
     */
    public function itemWaitlisted($customer)
    {
        if (shop_id() == 'fashionten') {
            $message = "Thank you for your comment. Your item is currently sold out and on the waitlist. As an overstock company, we cannot guarantee an item will become available again, but if it does, we will notify you!";
        } else {
            $message = "Thank you for your comment. Your item is currently sold out and on the waitlist. If it becomes available, we will notify you!";
        }

        return $this->standardText($customer->user_ref, $message);
    }

    /**
     * Customer added item to cart.
     */
    public function gotOrder($customer)
    {
        $items = Cart::forCustomer($customer->id)
            ->orderBy('id', 'DESC')
            ->with('variant.product.image')
            ->take(10)->get();

        if (count($items) == 0) {
            return;
        }

        $elements = [];
        foreach ($items as $item) {
            $elements[] = [
                'title'     => ($item->variant->product->product_name ?? null),
                'image_url' => product_image($item->variant->product->image->filename ?? null),
                'subtitle'  => '$'.($item->variant->price ?? null),
                'item_url'  => shop_url('/account'),
            ];
        }

        $json = [
            'recipient' => [
                'user_ref' => $customer->user_ref,
            ],
            'message' => [
                'attachment' => [
                    'type' => 'template',
                    'payload' => [
                        'template_type' =>'generic',
                        'elements'      => $elements,
                    ]
                ]
            ]
        ];

        return $json;
    }

    /**
     * Waitlist Activated.
     */
    public function waitlistActivated($customer, $variant)
    {
        $product = $variant->product;

        $json = [
            'recipient' => [
                'user_ref' => $customer->user_ref,
            ],
            'message' => [
                'attachment' => [
                    'type' => 'template',
                    'payload' => [
                        'template_type' =>'generic',
                        'elements'      => [
                            [
                                'title'     => ($product->product_name ?? null),
                                'image_url' => product_image($product->image->filename ?? null),
                                'subtitle'  => '$'.($variant->price ?? null),
                                'item_url'  => shop_url('/account'),
                            ]
                        ],
                    ]
                ]
            ],
            'tag' => 'ISSUE_RESOLUTION',
        ];

        return $json;
    }

    /**
     * Order Fulfilled.
     */
    public function orderFulfilled($customer, $order)
    {
        if ($order->local_pickup) {
            $message = "Your order (#{$order->id}) has just been fulfilled and is ready for pickup!";
            if (shop_id() == 'shopentourageclothing') {
                $message = "Your order (#{$order->id}) has been shipped from our warehouse to your local store. Please allow 3-5 business days before going to pickup.";
            }
            $title = 'Local Pickup';
            $url = shop_url('/account');
        } else {
            $message = "Your order (#{$order->id}) has just been fulfilled and is being shipped out today!";
            $title = 'Shipped';
            $url = shop_url('/account');

            if ($order->tracking_number) {
                $title = 'Track Package';
                $url =  'https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels='.$order->tracking_number;
            }
        }

        $json = [
            'recipient' => [
                'user_ref' => $customer->user_ref,
            ],
            'message' => [
                'attachment' => [
                    'type' => 'template',
                    'payload' => [
                        'template_type' =>'generic',
                        'elements' => [[
                            'title' => $message,
                            // 'image_url' => '',
                            // 'subtitle'  => '',
                            // 'item_url'  => '',
                            'buttons' => [[
                                'type' => 'web_url',
                                'title' => $title,
                                'url'   => $url,
                            ]],
                        ]],
                    ]
                ]
            ],
            'tag' => 'SHIPPING_UPDATE',
        ];

        return $json;
    }

    /**
     * Payment receipt.
     */
    public function paymentReceipt($customer, $order)
    {
        $orderProducts = $order->orderProducts;

        $elements = [];
        foreach ($orderProducts as $orderProduct) {
            $elements[] = [
                'title'     => $orderProduct->prod_name,
                'subtitle'  => $orderProduct->sizeColor(),
                'quantity'  => 1,
                'price'     => amount($orderProduct->price),
                'currency'  => 'USD',
                'image_url' => product_image($orderProduct->product_filename),
            ];
        }

        $json = [
            'recipient' => [
                'user_ref' => $customer->user_ref,
            ],
            'message' => [
                'attachment' => [
                    'type' => 'template',
                    'payload' => [
                        'template_type' =>'receipt',
                        'recipient_name' => $order->local_pickup ? 'Local' : 'Shipped',
                        'order_number' => $order->id,
                        'currency' => 'USD',
                        'payment_method' => $order->paymentMethod(),
                        'elements' => $elements,
                        'summary' => [
                            'subtotal'      => amount($order->subtotal),
                            'shipping_cost' => amount($order->ship_charged),
                            'total_tax'     => amount($order->tax_total),
                            'total_cost'    => amount($order->total),
                        ],
                    ]
                ]
            ],
        ];

        if (! $order->local_pickup) {
            $json['message']['attachment']['payload']['address'] = [
                'street_1'    => $order->street_address,
                'city'        => $order->city,
                'postal_code' => $order->zip,
                'state'       => $order->state,
                'country'     => $order->getCountryCode(),
            ];
        }

        return $json;
    }

    public function waitlistActivatedButton($customer)
    {
        $text = 'Congratulations, an item on your waitlist is back in stock! Click below to pay! Remember, you have ' . expire() . ' hours to pay before the item expires';
        $title = 'Pay order';
        return $this->button($customer->user_ref, $text, $title);
    }

    public function payOrderButton($customer)
    {
        $text = 'Congrats! You snagged one! Click below to checkout!';
        $title = 'Pay order';
        return $this->button($customer->user_ref, $text, $title);
    }

    public function paymentReceiptText($customer)
    {
        $message = "Thank you for your payment - your package is being put together by our fulfillment team. We'll send you a notification and tracking info when it's ready!";

        return $this->standardText($customer->user_ref, $message);
    }

    public function button($userRef, $text, $title)
    {
        $json = [
            'recipient' => [
                'user_ref'=> $userRef,
            ],
            'message' => [
                'attachment' => [
                    'type' => 'template',
                    'payload' => [
                        'template_type' =>'button',
                        'text' => $text,
                        'buttons' => [[
                            'type'                 => 'web_url',
                            'url'                  => shop_url('/account'),
                            'title'                => $title,
                            'webview_height_ratio' => 'tall',
                        ]]
                    ]
                ]
            ]
        ];

        return $json;
    }

    public function standardText($userRef, $text)
    {
        return [
            'recipient' => [
                'user_ref' => $userRef,
            ],
            'message' => [
                'text' => $text,
            ],
        ];
    }
}
