//Quick View radio option show
var product_id='';
var name='';
  $(".quick_shop").click(function(){
    product_id=$(this).attr("data-fancybox-href");
    $('.productQuickView').removeClass('myElementClass');
    var first_element=$("div.radio-wrapper input:first-child").val();
    $("label[for='"+first_element+"']").addClass('myElementClass');
    $(".product-single__photos img").attr("src", $(this).parents(".du-product-block").find(".product-single__thumbnail:eq(0)").attr("data-href"));
  });
  $(".myClass").click(function(event){
    console.log($(this).val());
    window.quick_view_product_id = product_id;
    if($("div"+product_id).find("#"+$(this).val()).hasClass("disabled"))
    {
      // return false;
      $("div"+product_id).find('.add-to-cart-text').html("Add to Waitlist (Sold Out)");
      $("div"+product_id).find('.btn-addtocart').attr('data-isWaitlist', true);

    } else {
      $("div"+product_id).find('.add-to-cart-text').html("Add to Basket");
      $("div"+product_id).find('.btn-addtocart').attr('data-isWaitlist', false);
    }

    $(".productQuickView").each(function(index,element){
      $(element).removeClass("myElementClass");
    });

    $("label[for='"+$(this).val()+"']").addClass('myElementClass');
    console.log($(this).next("label").attr("class"));
    name=$(this).val();
    console.log(name);
    $(product_id).find("#productSelect > option").each(function(index,element){
      $(element).removeAttr("selected");
    });
//     console.log($(this).parent(".radio-wrapper").next("#productSelect"));
//     $(this).nextAll().find("#productSelect option").each(function(index,element){
//       console.log(element);
//     });
    $("div"+product_id).each(function(index,element){
      if($(element).is(":visible"))
      {
        console.log(element);
        $(element).find("#productSelect option").each(function(index,element){
          console.log(element);
          console.log(name);
          if(name==$(element).text())
          {
            console.log(product_id);
            console.log("selected");
            console.log(element);
            var data=$(element).attr("data-sku");
            var value=$(element).val();
            var html='<option selected="selected" data-sku="'+data+'" value="'+value+'">'+$(element).text()+'</option>';
            console.log(html);
            $(element).replaceWith(html);
    //         $(element).attr("selected","selected");
          }
        });
      }
    });
    console.log($("div"+product_id).length);
    $(this).parent(".radio-wrapper").next("#productSelect option").each(function(index,element){
      if(name==$(element).text())
      {
        console.log(product_id);
        console.log("selected");
        console.log(element);
        var data=$(element).attr("data-sku");
        var value=$(element).val();
        var html='<option selected="selected" data-sku="'+data+'" value="'+value+'">'+$(element).text()+'</option>';
        console.log(html);
        $(element).replaceWith(html);
//         $(element).attr("selected","selected");
      }
    });
  });

//Show product featured image
  $(".product-single__thumbnail").click(function(){
    var stImg = document.createElement("img");
    stImg.src = $(this).attr("data-href");

    stImg.onload = function(){
      $(".product-single__photos img").attr("src", stImg.src);
    }

  });

//Select small option after click quick view modal close
  $(document).on("click", ".fancybox-close", function() {
    $('.productQuickView').removeClass('myElementClass');
    $("label:contains('Small')").addClass('myElementClass');
    $(product_id).find("#productSelect > option").each(function(index,element){
      if($(element).text()=="Small")
      {
        var data=$(element).attr("data-sku");
        var value=$(element).val();
        var html='<option selected="selected" data-sku="'+data+'" value="'+value+'">'+$(element).text()+'</option>';
        $(element).replaceWith(html);
      }
    });
  });

//Select small option in quick view on document ready
  $(document).ready(function(){
    $('.productQuickView').removeClass('myElementClass');
    $("#productSelect").find('option[text="Small"]').attr("selected","selected");
    $("label:contains('Small')").addClass('myElementClass');
    $(".quickViewSelectBox").addClass("hidden");
  });

//Close add to cart success message
$(document).on("click", "#AddToCart", function() {
  setTimeout(function() {
//       $(".ajaxified-cart-feedback").hide();
    timber.RightDrawer.close();
    }, 5000);
});

setInterval(function(){
        $('label[for="One Size"]').addClass('myElementClass');
          $('label[for="Default"]').addClass('myElementClass');
      }, 100);
$(".radio-wrapper").each(function(index,element){
          if($(element).find('.myClass').length==0)
             {
              $(element).find('.single-option-radio__label').hide();
           }
        });
//Modified on 29/8/2016 for add class disabled to add to cart button if all variants are sold out
$(document).on("click", "span.quick_shop", function(event) {
  var target_div_id=$(this).attr("data-fancybox-href");
  var count=0;
  var no_of_class=$("div"+target_div_id).find(".myClass").length;
  $("div"+target_div_id).find(".myClass").each(function(index,element){
    if($(element).hasClass("disabled"))
    {
      count++;
    }
  });
  if(count==no_of_class)
  {
    $("div"+target_div_id).find("#AddToCart").addClass("disabled");
//     $("#AddToCart").attr('disabled', 'disabled');
//     $("#AddToCart").prop("disabled", true);
  }
});

//04-11-2016
var quickViewProductHandle="";
var quickViewProductId="";
var productVariantSize="";
var productVariantColor="";
var productSizeColor="";
var productPrice="";
jQuery("div.collection-product-info span").click(function(){
  quickViewProductId=jQuery(this).attr("data-id");
  quickViewProductHandle=jQuery(this).attr("data-handle");
  var productSize=['S','M','L'];
  var count=0;
  var innerCount=0;
  console.log(quickViewProductId);
  console.log(quickViewProductHandle);
  jQuery.getJSON('/products/'+quickViewProductHandle+'.js', function(product) {
    console.log(product.variants);
    var productVariants=product.variants;
    while(count<productSize.length)
    {
      console.log(productSize[count]);
      count++;
    }
  });
  jQuery("#product-"+quickViewProductId+" .sizeForSingleProduct label").each(function(index,element){
    if(jQuery(this).hasClass("myElementClass"))
    {
      console.log(jQuery(this).text());
      productVariantSize=jQuery(this).text();
    }
  });
  jQuery("#product-"+quickViewProductId+" ul.colorVarient li").each(function(index,element){
    if(jQuery(this).find("a").hasClass("selected"))
    {
      console.log(jQuery(this).attr("data-color"));
      productVariantColor=jQuery(this).attr("data-color");
    }
  });
  jQuery("#product-"+quickViewProductId+" #productSelect option").each(function(index,element){
    console.log(jQuery(this).text());
    productSizeColor=productVariantSize+" / "+productVariantColor;
    if(jQuery(this).text() == productSizeColor)
    {
      console.log("get");
      jQuery(this).attr("selected","selected");
    }
  });
});

//SET SIZE AND COLOR WHEN SIZE OPTION CHANGED
jQuery(".myClass").click(function(){
  console.log(jQuery(this).val());
  console.log("vvvv = "+quickViewProductHandle);
  var inner_count=0;
  var colorContainerArray=[];
  var selectedColor="";
  console.log(jQuery("#product-"+quickViewProductId+" ul.colorVarient li").length);
  if(jQuery("#product-"+quickViewProductId+" ul.colorVarient li").length>0)
  {
    jQuery("#product-"+quickViewProductId+" #productSelect option").each(function(index,element){
      jQuery(this).removeAttr('selected');
    });
    jQuery("#product-"+quickViewProductId+" .sizeForSingleProduct label").each(function(index,element){
      if(jQuery(this).hasClass("myElementClass"))
      {
        console.log(jQuery(this).text());
        productVariantSize=jQuery(this).text();
      }
    });
  //   jQuery.getJSON('/products/'+quickViewProductHandle+'.js', function(product) {
  //     console.log(product.variants);
  //     var productVariants=product.variants;
      var count=0;
      jQuery("#product-"+quickViewProductId+" ul.colorVarient li").each(function(index,element){
        if(jQuery(this).find("a").hasClass("selected"))
        {
          jQuery(this).find("a").removeClass("selected");
        }
        else if(jQuery(this).find("a").hasClass("not-selected"))
        {
          jQuery(this).find("a").removeClass("not-selected");
        }
      });
      jQuery("#product-"+quickViewProductId+" .productColorAndSizeVariant  option").each(function(index,element){
        if(jQuery(this).attr("data-size")==productVariantSize)
        {
          if(jQuery(this).attr("data-available")==="true")
          {
            if(count==0)
            {
              console.log("add class selected = "+jQuery(this).attr("data-color"));
              jQuery("#product-"+quickViewProductId+" ul.colorVarient li[data-color='" + jQuery(this).attr("data-color") + "']").find("a").addClass("selected");
              selectedColor=jQuery(this).attr("data-color");
              productVariantColor=selectedColor;
              productPrice=jQuery(this).attr("data-price")/100;
              count++;
            }
            colorContainerArray.push(jQuery(this).attr("data-color"));

          }
//           colorContainerArray.push(jQuery(this).attr("data-color"));
        }
      });
      jQuery("#product-"+quickViewProductId+" ul.colorVarient li").each(function(index,element){
        if(jQuery.inArray( jQuery(this).attr("data-color"), colorContainerArray )==-1)
        {
          jQuery(this).find("a").addClass("not-selected");
        }
      });
      productSizeColor=productVariantSize+" / "+productVariantColor;
      jQuery("#product-"+quickViewProductId+" #productSelect option").each(function(index,element){
        console.log(jQuery(this).text());
        productSizeColor=productVariantSize+" / "+productVariantColor;
        var dataSku="";
        var dataValue="";
        var targetHtml="";
        if(jQuery(this).text() == productSizeColor)
        {
          dataSku=jQuery(this).attr("data-sku");
          dataValue=jQuery(this).val();
          targetHtml='<option data-sku="'+dataSku+'" value="'+dataValue+'" selected>'+productSizeColor+'</option>';
          jQuery(this).replaceWith(targetHtml);

    //       jQuery(this).attr("selected","selected");
        }
  //       else
  //       {
  //         jQuery(this).removeAttr('selected');
  //       }
    });
    jQuery("#product-"+quickViewProductId+" #ProductPrice").html("$ "+productPrice);
      jQuery("#product-"+quickViewProductId+" #productSelect option").trigger("change");
  }
//   });
});

// jQuery("ul.colorVarient li").click(function(){
//   jQuery("ul.colorVarient li").each(function(index,element){
//     if(jQuery(this).find("a").hasClass("selected"))
//     {
//       jQuery(this).find("a").removeClass("selected");
//     }
//   });
//   jQuery(this).find("a").addClass("selected");
// });

//SET SIZE AND COLOR WHEN COLOR OPTION CHANGED
jQuery("ul.colorVarient li").click(function(){
//   jQuery("ul.colorVarient li").each(function(index,element){
//     if(jQuery(this).find("a").hasClass("selected"))
//     {
//       jQuery(this).find("a").removeClass("selected");
//     }
//   });
//   jQuery(this).find("a").addClass("selected");
  console.log(jQuery(this).attr("data-color"));
  productVariantColor=jQuery(this).attr("data-color");
  jQuery("#product-"+quickViewProductId+" .sizeForSingleProduct label").each(function(index,element){
    if(jQuery(this).hasClass("myElementClass"))
    {
      console.log(jQuery(this).text());
      productVariantSize=jQuery(this).text();
    }
  });

  jQuery("#product-"+quickViewProductId+" .productColorAndSizeVariant  option").each(function(index,element){
    if((jQuery(this).attr("data-size")==productVariantSize) && (jQuery(this).attr("data-color")==productVariantColor))
    {
      if(jQuery(this).attr("data-available")==="true")
      {
        console.log("change");
        jQuery("#product-"+quickViewProductId+" #productSelect option").each(function(index,element){
          jQuery(this).removeAttr('selected');
        });
        productSizeColor=productVariantSize+" / "+productVariantColor;
        jQuery("#product-"+quickViewProductId+" #productSelect option").each(function(index,element){
          var dataSku="";
          var dataValue="";
          var targetHtml="";
          if(jQuery(this).text() == productSizeColor)
          {
            dataSku=jQuery(this).attr("data-sku");
            dataValue=jQuery(this).val();
            targetHtml='<option data-sku="'+dataSku+'" value="'+dataValue+'" selected>'+productSizeColor+'</option>';
            jQuery(this).replaceWith(targetHtml);

          }
        });
        productPrice=jQuery(this).attr("data-price")/100;
        console.log(productPrice);

        jQuery("#product-"+quickViewProductId+" #ProductPrice").html("$ "+productPrice);
        jQuery("#product-"+quickViewProductId+" ul.colorVarient li").each(function(index,element){
          if(jQuery(this).find("a").hasClass("selected"))
          {
            jQuery(this).find("a").removeClass("selected");
          }
          else if(jQuery(this).find("a").hasClass("not-selected"))
          {
            jQuery(this).find("a").removeClass("not-selected");
          }
        });
//   		jQuery("#product-"+quickViewProductId+" ul.colorVarient li").each(function(index,element){
//           if(jQuery(this).attr("data-color")==productVariantColor)
//           {
//             jQuery(this).find("a").addClass("selected");
//           }
//         });
        colorContainerArray=[];
        jQuery("#product-"+quickViewProductId+" .productColorAndSizeVariant  option").each(function(index,element){
          if(jQuery(this).attr("data-size")==productVariantSize)
          {
            if(jQuery(this).attr("data-available")==="true")
            {
              if(jQuery(this).attr("data-color")==productVariantColor)
              {
                console.log("add class selected = "+jQuery(this).attr("data-color"));
                jQuery("#product-"+quickViewProductId+" ul.colorVarient li[data-color='" + jQuery(this).attr("data-color") + "']").find("a").addClass("selected");

              }
              colorContainerArray.push(jQuery(this).attr("data-color"));

            }
//             colorContainerArray.push(jQuery(this).attr("data-color"));
          }
        });
        jQuery("#product-"+quickViewProductId+" ul.colorVarient li").each(function(index,element){
          if(jQuery.inArray( jQuery(this).attr("data-color"), colorContainerArray )==-1)
          {
            jQuery(this).find("a").addClass("not-selected");
          }
        });

      }
    }
  });
});
