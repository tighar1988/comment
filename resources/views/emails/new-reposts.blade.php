<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><hr/><br/>

    Here are the scheduled posts setup for reposts:

    <br/><hr/><br/>

    @foreach ($posts as $post)
        <img src="{{ $post['image'] }}" style="width: 150px;" />
        <br/><br/>

        Message we are posting: <br/>
        <pre>{{ $post['message'] }}</pre>

        <br/><a href="{{ $post['edit_url'] }}">Edit the post here</a>
        <br/><br/>
    @endforeach
</div>
