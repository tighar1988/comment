<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->text('street_address')->nullable();
            $table->text('apartment')->nullable();
            $table->text('city')->nullable();
            $table->text('state')->nullable();
            $table->text('zip')->nullable();
            $table->text('email')->nullable();
            $table->integer('order_status')->nullable();
            $table->text('payment_ref')->nullable();
            $table->double('payment_amt')->nullable();
            $table->double('ship_charged')->nullable();
            $table->double('ship_cost')->nullable();
            $table->double('subtotal')->nullable();
            $table->double('total')->nullable();
            $table->text('fbid')->nullable();
            $table->text('ship_name')->nullable();
            $table->integer('payment_date')->nullable();
            $table->integer('shipped_date')->nullable();
            $table->text('tracking_number')->nullable();
            $table->double('state_tax')->nullable();
            $table->double('county_tax')->nullable();
            $table->double('municipal_tax')->nullable();
            $table->double('tax_total')->nullable();
            $table->text('coupon')->nullable();
            $table->double('coupon_discount')->nullable();
            $table->text('calculated_zip')->nullable();
            $table->double('apply_balance')->nullable();
            $table->boolean('local_pickup')->default(false);
            $table->text('picker')->nullable();
            $table->text('packer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
