<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Inventory;
use Illuminate\Support\Facades\Auth;

class CartAddFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|numeric|min:1',
            'id' => 'required|numeric',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $error = null;
            $variant = Inventory::find($this->id);
            $productName = $variant->product->product_name ?? null;
            $productType = $variant->product->product_type ?? null;

            if (! $variant) {
                $error = 'We could not find the product!';

            } elseif ($variant->quantity <= 0 && $productType != 'giftcard') {
                $error = "The product {$productName} - ".$variant->variantName()." is already sold out.";

            } elseif ($variant->quantity < $this->quantity  && $productType != 'giftcard') {
                $error = "You can only add {$variant->quantity} {$productName} to the cart.";
            }

            // todo: add case where we consider the amount of items in the cart more properly

            if (is_null($error) && ! Auth::check() && $productType != 'giftcard') {
                $cart = session('online-store.cart', []);
                if (isset($cart[$variant->id]) && $variant->quantity < ($cart[$variant->id]['quantity'] + $this->quantity)) {
                    $error = "You can only add in total {$variant->quantity} {$productName} to the cart.";
                }
            }

            if (! is_null($error)) {
                $validator->errors()->add('status', 422);
                $validator->errors()->add('message', 'Cart Error');
                $validator->errors()->add('description', $error);
            }
        });
    }
}
