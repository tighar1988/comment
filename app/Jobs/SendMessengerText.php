<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Traits\ReconnectShopDB;
use App\Models\Master;
use App\Facebook\FacebookMessenger;
use Facebook\Exceptions\FacebookResponseException;
use DB;

class SendMessengerText implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    protected $shop;

    protected $messages;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop, array $messages)
    {
        $this->shop = $shop;
        $this->messages = $messages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->reconnectDB($this->shop);

        $messenger = new FacebookMessenger(fb_page_access_token());

        foreach ($this->messages as $message) {
            try {
                $messenger->callSendAPI($message);
                log_info('[messenger][send] Sending message to user_ref ' . ($message['recipient']['user_ref'] ?? null));

            } catch (FacebookResponseException $e) {
                if (str_contains($e->getMessage(), "(#200) This person isn't available right now.")) {
                    log_info('[messenger][send] (#200) Person not available ' . ($message['recipient']['user_ref'] ?? null));
                } else {
                    throw $e;
                }
            }
        }
    }
}
