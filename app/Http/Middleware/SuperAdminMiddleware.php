<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if super-admin is logged in allow access
        if (session('superadmin.logged-in')) {
            return $next($request);
        }

        if ($request->getUser() == env('APP_SUPERADMIN_USER') && $request->getPassword() == env('APP_SUPERADMIN_PASSWORD')) {
            session(['superadmin.logged-in' => true]);
            return $next($request);
        } else {
            return new Response('Invalid credentials.', 401, ['WWW-Authenticate' => 'Basic']);
        }
    }
}
