@extends('admin.store.store')

@section('section', 'Pages')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body r-p">
                <a href="/admin/store/pages/add" class="btn btn-success add-btn btn-sm">Add Page</a>
                <table id="pages-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Url</th>
                            <th>Last Modified</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pages as $page)
                            <tr>
                                <td>{{ $page->title }}</td>
                                <td>{{ $page->getUrl() }}</td>
                                <td>{{ style_date($page->updated_at) }}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-h fa-lg"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                            <li><a href="/admin/store/pages/edit/{{ $page->id }}" class="link">Edit</a></li>
                                            <li><a href="/admin/store/pages/delete/{{ $page->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#pages-table').DataTable();
    });
</script>
@endsection
