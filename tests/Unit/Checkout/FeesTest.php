<?php

namespace Tests\Unit\Reports;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Checkout\Checkout;
use App\Models\ShopUser;
use Mockery;
use App\Checkout\Fees;

class FeesTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Fees::class, new Fees(shop_id()));
    }

    /** @test */
    public function is_not_custom_shop()
    {
        $fees = new Fees(shop_id());
        $this->assertFalse($fees->isCustomShop());
    }

    /** @test */
    public function normal_fee_percentage()
    {
        $fees = new Fees(shop_id());
        $this->assertEquals(0.03, $fees->getFeesPercentage());
    }

    /** @test */
    public function custom_shop_fee_percentage()
    {
        $fees = new Fees('pinkcoconut');
        $this->assertEquals(0.02, $fees->getFeesPercentage());
    }

    /** @test */
    public function it_has_boutique_hub_fee_plan()
    {
        $fees = new Fees('test_shop', 'boutique_hub');
        $this->assertEquals(0.03, $fees->getFeesPercentage());
    }

    /** @test */
    public function it_has_starter_fee_plan()
    {
        $fees = new Fees('test_shop', 'starter');
        $this->assertEquals(0.05, $fees->getFeesPercentage());
    }

    /** @test */
    public function it_has_business_fee_plan()
    {
        $fees = new Fees('test_shop', 'business');
        $this->assertEquals(0.03, $fees->getFeesPercentage());
    }

    /** @test */
    public function it_has_boutique_hub_starter_fee_plan()
    {
        $fees = new Fees('test_shop', 'boutique_hub_starter');
        $this->assertEquals(0.05, $fees->getFeesPercentage());
    }

    /** @test */
    public function it_has_boutique_hub_business_fee_plan()
    {
        $fees = new Fees('test_shop', 'boutique_hub_business');
        $this->assertEquals(0.03, $fees->getFeesPercentage());
    }
}
