@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($blogPost) ? true : false; ?>
<section class="content-header">
    <h1>@if ($isEdit) Editing Blog Post '{{ $blogPost->title }}' @else Add New Blog Post @endif</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/blog-posts"><i class="fa fa-th-large"></i> Blog Posts</a></li>
        <li class="active">@if ($isEdit) Edit Blog Post @else Add New Blog Post @endif</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}" enctype="multipart/form-data">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('title')">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" value="{{ old('title', $blogPost->title ?? '') }}">
                                    @error('title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('content')">
                                    <label>Content</label>
                                    <textarea class="form-control" name="content" rows="3">{{ old('content', $blogPost->content ?? '') }}</textarea>
                                    @error('content')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('excerpt')">
                                    <label>Excerpt</label>
                                    <textarea class="form-control" name="excerpt" rows="3">{{ old('excerpt', $blogPost->excerpt ?? '') }}</textarea>
                                    @error('excerpt')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('visibility')">
                                    <label class="control-label">Visibility</label>
                                    <div class="radio">
                                        <label><input type="radio" @if(old('visibility', $blogPost->visibility ?? null) == 1) checked="checked" @endif value="1" name="visibility">Visible</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" @if(old('visibility', $blogPost->visibility ?? null) == 2) checked="checked" @endif value="2" name="visibility">Hidden</label>
                                    </div>
                                    @error('visibility')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('featured_image')">
                                    <label>Featured Image</label>
                                    @if (isset($blogPost->featured_image))
                                        <br/><img height="100px" src="{{ $blogPost->imageUrl() }}">
                                        <div class="text-muted">{{ $blogPost->featured_image }}</div>
                                    @endif
                                    <input type="file" class="form-control" name="featured_image" placeholder="Image File" value="" />
                                    @error('featured_image')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('author')">
                                    <label>Author</label>
                                    <input type="text" class="form-control" name="author" value="{{ old('author', $blogPost->author ?? $user->name) }}">
                                    @error('author')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('blog_id')">
                                    <label>Blog</label>
                                    <select name="blog_id" class="form-control">
                                        @foreach ($blogs as $blog)
                                            <option value="{{ $blog->id }}" {{ old('blog_id', $blog->id ?? null) == $blog->id ? 'selected' : '' }}>{{ $blog->title }}</option>
                                        @endforeach
                                    </select>
                                    @error('blog_id')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('seo_page_title')">
                                    <label class="control-label">SEO Page Title</label> <small class="text-muted">70 characters allowed</small>
                                    <input type="text" class="form-control" maxlength="70" name="seo_page_title" value="{{ old('seo_page_title', $blogPost->seo_page_title ?? null) }}">
                                    @error('seo_page_title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('seo_meta_description')">
                                    <label class="control-label">SEO Page Description</label> <small class="text-muted">160 characters allowed</small>
                                    <input type="text" class="form-control" maxlength="160" name="seo_meta_description" value="{{ old('seo_meta_description', $blogPost->seo_meta_description ?? null) }}">
                                    @error('seo_meta_description')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('handle')">
                                    <label class="control-label">Handle</label>
                                    <input type="text" placeholder="news" class="form-control" maxlength="160" name="handle" value="{{ old('handle', $blogPost->handle ?? null) }}">
                                    @error('handle')
                                </div>
                            </div>
                        </div>

                        <button type="submit" data-loading-text="Saving.." class="btn btn-success">Save</button>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script src="{{ cdn('/ckeditor/ckeditor.js') }}"></script>
<script>CKEDITOR.replace('content');</script>
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
@endsection
