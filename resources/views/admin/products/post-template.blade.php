<?php
    if ($colors != '') {
        $colors = ', Color (' . $colors . ')';
    }
?>

{!! strip_tags($product->product_name) !!}. {!! strip_tags($product->description) !!}

@if ($sizes)
Available in {{ $sizes }} for only ${{ amount($price) }}!
@if (shop_id() == 'glamourfarms')

TO ORDER: Comment "Sold, Size{{ $colors }}". One order per comment.
@else
To order: Comment Sold, Size{{ $colors }}
@endif
@else
Available for only ${{ amount($price) }}!
@if (shop_id() == 'glamourfarms')

TO ORDER: Comment "Sold{{ $colors }}". One order per comment.
@else
To order: Comment Sold{{ $colors }}
@endif
@endif

First time shopping with us? Click the link immediately after commenting

@if (shop_id() == 'glamourfarms' || shop_id() == 'pinkcoconut' || shop_id() == 'lindylous' || shop_id() == 'divas')
Go to {{ shop_url('/account') }} to view/pay your invoice. Invoice must be paid within {{ expire() }} hours or your item will go to the next person in line.

@endif
Style #{{ $product->style }}

@if (shop_id() == 'pinkcoconut')
For customer service please email pinkcoconutonline@gmail.com
@endif
