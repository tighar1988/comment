<?php

namespace App\Instagram;

use App\Models\Helpers\SoldMatcher;

class InstagramShopperComment
{
    use SoldMatcher;

    public $comment;

    public $id;

    public $fromId;

    public $fromUsername;

    public $fromName;

    public $fromProfilePicture;

    public $createdTime;

    public $message;

    public function __construct($comment)
    {
        $this->comment = $comment;

        $this->id                 = $comment['id'];
        $this->fromId             = $comment['from']['id'] ?? null;
        $this->fromUsername       = $comment['from']['username'] ?? null;
        $this->fromName           = $comment['from']['full_name'] ?? null;
        $this->fromProfilePicture = $comment['from']['profile_picture'] ?? null;
        $this->createdTime        = $comment['created_time'] ?? null;
        $this->message            = $comment['text'] ?? null;
    }
}
