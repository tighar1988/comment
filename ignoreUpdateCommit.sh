#!/bin/bash

git add storage/labels
git add storage/catalog
git commit storage/labels -m "updating labels `date`"
git commit storage/catalog -m "updating pics/thumbs `date`"
git push

# don't look at these files anymore 
find storage/labels -type f -exec git update-index --assume-unchanged {} +
find storage/catalog -type f -exec git update-index --assume-unchanged {} +

