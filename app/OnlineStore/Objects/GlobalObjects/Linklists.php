<?php

namespace App\OnlineStore\Objects\GlobalObjects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\Objects\Linklist;

class Linklists extends Drop
{
    protected $cache = [];

    protected $menu;

    public function __construct()
    {
        $this->menu = app('App\Models\Menu');
    }

    /**
     * The linklists object returns the set of the menus and links in your store.
     * You can access a menu by calling its handle on the linklists object.
     * <ul>
     *     {% for link in linklists.categories.links %}
     *         <li>{{ link.title | link_to: link.url }}</li>
     *     {% endfor %}
     *</ul>
     */
    public function handle_key($key)
    {
        $key = trim($key, '/ ');
        if (str_contains($key, 'collections/')) {
            $key = str_replace('collections/', '', $key);
        }

        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        $menu = $this->menu->handle($key)->with('items')->first();
        $this->cache[$key] = is_null($menu) ? null : new Linklist($menu);
        return $this->cache[$key];
    }
}
