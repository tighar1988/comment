<?php

namespace App\OnlineStore\LiquidFilters;
use App\OnlineStore\CurrentView;

/**
 * General filters serve many different purposes including formatting, converting, and applying CSS classes.
 */
class AdditionalFilters
{
    public static $fileSystem;

    protected static $locale;

    /**
     * Converts a timestamp into another date format.
     *
     * Input
     * {{ article.published_at | date: "%a, %b %d, %y" }}
     *
     * Output
     * Tue, Apr 22, 14
     * date accepts the same parameters as Ruby's strftime method. You can find a list of
     * the shorthand formats in Ruby's documentation or use a site like strfti.me.
     */
    public static function _date()
    {
        // defined in StandardFilters.php
    }

    /**
     * The time_tag filter converts a timestamp into an HTML <time> tag:
     *
     * Input
     * {{ article.published_at | time_tag }}
     *
     * Output
     * <time datetime="2016-02-24T14:47:51Z">Wed, 24 Feb 2016 09:47:51 -0500</time>
     * To customize the time output, you can pass date parameters to the time_tag filter.
     * This doesn't affect the value of the datetime attribute set in the <time> tag:
     *
     * Input
     * {{ article.published_at | time_tag: '%b %d, %Y' }}
     *
     * Output
     * <time datetime="2016-02-24T14:47:51Z">Feb 24, 2016</time>
     * Custom datetime
     * You can pass a datetime parameter with date parameters to use a custom format for
     * the datetime attribute of the output time tag:
     *
     * Input
     * {{ article.published_at | datetime: '%b %d, %Y' }}
     *
     * Output
     * <time datetime="Feb 24, 2016">Wed, 24 Feb 2016 09:47:51 -0500</time>
     * Localized date format
     * You can pass a format parameter to the filter to use a date format defined in your theme's locale settings:
     * In theme / locales / en.json
     * "date_formats": {
     *   "month_day_year": "%B %d, %Y"
     * }
     *
     * Input
     * {{ article.published_at | time_tag: format: 'month_day_year' }}
     *
     * Output
     * <time datetime="2016-02-24T14:47:51Z">February 24, 2016</time>
     */
    public static function time_tag($input, $format = null)
    {
        if (! is_numeric($input)) {
            $input = strtotime($input);
        }

        $format = is_null($format) ? '%B %d, %Y' : $format;

        return '<time datetime="'.strftime('%Y-%m-%d %H:%M:%S', $input).'">'.strftime($format, $input).'</time>';
    }

    /**
     * Sets a default value for any variable with no assigned value. Can be used with strings, arrays, and hashes.
     * The default value is returned if the variable resolves to nil or an empty string "". A string containing
     * whitespace characters will not resolve to the default value.
     *
     * Input
     * Dear {{ customer.name | default: "customer" }}
     *
     * Output
     * <!-- If customer.name is nil -->
     * Dear customer
     * <!-- If customer.name is "" -->
     * Dear customer
     * <!-- If customer.name is "   " -->
     * Dear
     */
    public static function default($input, $default = null)
    {
        if (! empty($input)) {
            return $input;
        }

        return $default;
    }

    /**
     * Outputs default error messages for the form.errors variable. The messages returned are dependent
     * on the strings returned by form.errors.
     *
     * Input
     * {% if form.errors %}
     *       {{ form.errors | default_errors }}
     * {% endif %}
     *
     * Output
     * <!-- if form.errors returned "email" -->
     * Please enter a valid email address.
     */
    public static function default_errors($errors)
    {
        $errors = (array) $errors;

        $html = '';
        foreach ($errors as $error) {
            if ($error == 'email') {
                $html .= '<li>Please enter a valid email address.</li>';
            } elseif ($error == 'author') {
                $html .= '<li>Please enter a valid author.</li>';
            } elseif ($error == 'body') {
                $html .= '<li>Please enter a valid value.</li>';
            } elseif ($error == 'form') {
                $html .= '<li>Please enter a valid form value.</li>';
            } elseif (is_string($error)) {
                $html .= "<li>{$error}</li>";
            }
        }

        if (empty($html)) {
            return null;
        }

        return '<div class="errors"><ul>'.$html.'</ul></div>';
    }

    /**
     * Creates a set of links for paginated results. Used in conjunction with the paginate variable.
     *
     * Input
     * {{ paginate | default_pagination }}
     *
     * Output
     * <span class="page current">1</span>
     * <span class="page"><a href="/collections/all?page=2" title="">2</a></span>
     * <span class="page"><a href="/collections/all?page=3" title="">3</a></span>
     * <span class="deco">…</span>
     * <span class="page"><a href="/collections/all?page=17" title="">10</a></span>
     * <span class="next"><a href="/collections/all?page=2" title="">Next »</a></span>
     * Default pagination uses the labels Next » and « Previous for links to the next and previous pages. You can override these labels by passing new words to the default_pagination filter:
     *
     * Input
     * {{ paginate | default_pagination: next: 'Older', previous: 'Newer' }}
     *
     * Output
     * <span class="page current">1</span>
     * <span class="page"><a href="/collections/all?page=2" title="">2</a></span>
     * <span class="page"><a href="/collections/all?page=3" title="">3</a></span>
     * <span class="next"><a href="/collections/all?page=2" title="">Older</a></span>
     */
    public static function default_pagination()
    {
        $html = '';
        $currentPage   = CurrentView::$currentPage;
        $currentOffset = CurrentView::$currentOffset;
        $numberItems   = CurrentView::$numberItems;

        $totalItems    = CurrentView::$totalItems;
        $path          = CurrentView::$currentPath;
        $q             = CurrentView::$queryString;

        if (! is_array($q)) {
            $q = [];
        }
        if (! isset($q['page'])) {
            $q['page'] = $currentPage;
        }

        $totalPages = ceil($totalItems / $numberItems);
        $lastPage = $totalPages;

        $onFirstPage = true;
        if ($currentPage > 1) {
            $onFirstPage = false;
        }

        if (! $onFirstPage) {
            $q['page'] = $currentPage - 1;
            $html .= '<span class="prev"><a href="/'.$path.'?'.http_build_query($q).'" title="">&laquo; Previous</a></span>';
        }
        if ($currentPage > 3) {
            $q['page'] = 1;
            $html .= '<span class="page"><a href="/'.$path.'?'.http_build_query($q).'" title="">1</a></span>';
        }
        if ($currentPage > 4) {
            $html .= '<span class="deco">...</span>';
        }
        if ($currentPage - 2 > 0) {
            $q['page'] = $currentPage - 2;
            $html .= '<span class="page"><a href="/'.$path.'?'.http_build_query($q).'" title="">'.($currentPage - 2).'</a></span>';
        }
        if ($currentPage - 1 > 0) {
            $q['page'] = $currentPage - 1;
            $html .= '<span class="page"><a href="/'.$path.'?'.http_build_query($q).'" title="">'.($currentPage - 1).'</a></span>';
        }

        $html .= '<span class="page current">'.$currentPage.'</span>';

        if ($currentPage + 1 < $lastPage) {
            $q['page'] = $currentPage + 1;
            $html .= '<span class="page"><a href="/'.$path.'?'.http_build_query($q).'" title="">'.($currentPage + 1).'</a></span>';
        }
        if ($currentPage + 2 < $lastPage) {
            $q['page'] = $currentPage + 2;
            $html .= '<span class="page"><a href="/'.$path.'?'.http_build_query($q).'" title="">'.($currentPage + 2).'</a></span>';
        }
        if ($currentPage < $lastPage - 3) {
            $html .= '<span class="deco">...</span>';
        }
        if ($currentPage < $lastPage) {
            $q['page'] = $lastPage;
            $html .= '<span class="page"><a href="/'.$path.'?'.http_build_query($q).'" title="">'.$lastPage.'</a></span>';
        }
        if ($currentPage < ($lastPage - 1)) {
            $q['page'] = $currentPage + 1;
            $html .= '<span class="next"><a href="/'.$path.'?'.http_build_query($q).'" title="">Next &raquo;</a></span>';
        }

        // reset the values
        CurrentView::$currentPage   = null;
        CurrentView::$currentOffset = null;
        CurrentView::$numberItems   = null;
        CurrentView::$totalItems    = null;

        return $html;
    }

    /**
     * Use the format_address filter on an address to print the elements of the address in order according to
     * their locale. The filter will only print the parts of the address that have been provided. This filter
     * works on the addresses page for customers who have accounts in your store, or on your store's address:
     *
     * Input
     * {{ address | format_address }}
     *
     * Output
     * <p>
     *   Elizabeth Gonzalez<br>
     *   1507 Wayside Lane<br>
     *   San Francisco<br>
     *   CA<br>
     *   94103<br>
     *   United States
     * </p>
     *
     * Input
     * {{ address | format_address }}
     *
     * Output
     * <p>
     *   Feng Sun<br>
     *   No. 2094, 1006, Hui Dong Xin Cun<br>
     *   Nanhui District<br>
     *   201300, Shanghai<br>
     *   China
     * </p>
     */
    public static function format_address()
    {
        // todo: implement
    }

    /**
     * Wraps words inside search results with an HTML <strong> tag with the class highlight if it matches the submitted search.terms.
     *
     * Input
     * {{ item.content | highlight: search.terms }}
     *
     * Output
     * <!-- If the search term was "Yellow" -->
     * <strong class="highlight">Yellow</strong> shirts are the best!
     */
    public static function highlight($input, $highlights)
    {
        foreach ((array)$highlights as $highlight) {
            $input = str_replace($highlight, '<strong class="highlight">'.$highlight.'</strong>', $input);
        }

        return $input;
    }

    /**
     * Wraps a tag link in a <span> with the class active if that tag is being used to filter a collection.
     *
     * Input
     * <!-- collection.tags = ["Cotton", "Crew Neck", "Jersey"] -->
     * {% for tag in collection.tags %}
     *     {{ tag | highlight_active | link_to_tag: tag }}
     * {% endfor %}
     *
     * Output
     * <a title="Show products matching tag Cotton" href="/collections/all/cotton"><span class="active">Cotton</span></a>
     * <a title="Show products matching tag Crew Neck" href="/collections/all/crew-neck">Crew Neck</a>
     * <a title="Show products matching tag Jersey" href="/collections/all/jersey">Jersey</a>
     */
    public static function highlight_active_tag()
    {
        // todo: implement
    }

    /**
     * Converts a string into JSON format.
     *
     * Input
     * var content = {{ pages.page-handle.content | json }};
     *
     * Output
     * var content = "\u003Cp\u003E\u003Cstrong\u003EYou made it! Congratulations on starting your own e-commerce store!\u003C/strong\u003E\u003C/p\u003E\n\u003Cp\u003EThis is your shop\u0026#8217;s \u003Cstrong\u003Efrontpage\u003C/strong\u003E, and it\u0026#8217;s the first thing your customers will see when they arrive. You\u0026#8217;ll be able to organize and style this page however you like.\u003C/p\u003E\n\u003Cp\u003E\u003Cstrong\u003ETo get started adding products to your shop, head over to the \u003Ca href=\"/admin\"\u003EAdmin Area\u003C/a\u003E.\u003C/strong\u003E\u003C/p\u003E\n\u003Cp\u003EEnjoy the software,  \u003Cbr /\u003E\nYour Shopify Team.\u003C/p\u003E";
     *
     * Tip
     * You do not have to wrap the Liquid output in quotations - the json filter will add them in. The  json filter will also escape quotes as needed inside the output.
     * The json filter can also used to make Liquid objects readable by JavaScript:
     * var json_product = {{ collections.featured.products.first | json }};
     * var json_cart = {{ cart | json }};
     */
    public static function json($input)
    {
        return json_encode($input);
    }

    /**
     * Formats the product variant's weight. The weight unit is set in General Settings.
     *
     * Input
     * {{ product.variants.first.weight | weight_with_unit }}
     *
     * Output
     * 24.0 kg
     * The unit can be overridden by passing it into the filter. This is useful in the case of product variants which can each have their own unit.
     *
     * Input
     * {{ variant.weight | weight_with_unit: variant.weight_unit }}
     *
     * Output
     * 52.9 lb
     */
    public static function weight_with_unit()
    {
        // todo: implement
    }

    /**
     * Takes a placeholder name and outputs a placeholder SVG illustration. An optional argument can be supplied to include a custom class attribute on the SVG tag.
     *
     * Input
     * {{ 'collection-1' | placeholder_svg_tag }}
     *
     * Output
     * <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 525.5 525.5">...omitted for brevity...</svg>
     */
    public static function placeholder_svg_tag()
    {
        // todo: implement
    }

    /**
     * Translate key from the theme's locales folder,
     *
     * https://help.shopify.com/themes/development/internationalizing/translation-filter
     *
     * @param  string $key
     * @return string
     */
    public static function t($key, ...$args)
    {
        if (is_null(static::$locale)) {
            if (! is_null(static::$fileSystem)) {
                static::$locale = json_decode(static::$fileSystem->readLocalesFile('en.default.json'), true);
            } else {
                static::$locale = [];
            }
        }

        $value = array_get(static::$locale, $key, $key);

        for ($i = 0; $i < count($args) - 1; $i+=2) {
            $value = str_replace("{{ {$args[$i]} }}", $args[$i+1], $value);
        }

        return $value;
    }

    public static function customer_logout_link($name)
    {
        return '<a href="/logout">'.$name.'</a>';
    }

    public static function customer_login_link($name)
    {
        return '<a href="/login">'.$name.'</a>';
    }

    public static function customer_register_link($name)
    {
        return '<a href="/account">'.$name.'</a>';
    }

}


