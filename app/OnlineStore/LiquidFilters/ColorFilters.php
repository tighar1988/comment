<?php

namespace App\OnlineStore\LiquidFilters;

class ColorFilters
{
    /**
     * Converts a CSS color string to CSS rgb() format.
     *
     * Input
     * {{ '#7ab55c' | color_to_rgb }}
     *
     * Output
     * rgb(122, 181, 92)
     * If the input color has an alpha component, then the output will be in CSS rgba() format.
     * {{ 'hsla(100, 38%, 54%, 0.5)' | color_to_rgb }}
     *
     * Output
     * rgba(122, 181, 92, 0.5)
     */
    public static function color_to_rgb()
    {
        // todo: implement
    }

    /**
     * Converts a CSS color string to CSS hsl() format.
     *
     * Input
     * {{ '#7ab55c' | color_to_hsl }}
     *
     * Output
     * hsl(100, 38%, 54%)
     * If the input color has an alpha component, then the output will be in CSS rgba() format.
     * {{ 'rgba(122, 181, 92, 0.5)' | color_to_hsl }}
     *
     * Output
     * hsla(100, 38%, 54%, 0.5)
     */
    public static function color_to_hsl()
    {
        // todo: implement
    }

    /**
     * Converts a CSS color string to hex6 format.
     *
     * Input
     * {{ 'rgb(122, 181, 92)' | color_to_hex }}
     *
     * Output
     * #7ab55c
     * Hex output is always in hex6 format. If there is an alpha channel in the input color, it will not appear in the output.
     * {{ 'rgba(122, 181, 92, 0.5)' | color_to_hex }}
     *
     * Output
     * #7ab55c
     */
    public static function color_to_hex()
    {
        // todo: implement
    }

    /**
     * Extracts a component from the color. Valid components are alpha, red, green, blue, hue, saturation and lightness.
     *
     * Input
     * {{ '#7ab55c' | color_extract: 'red' }}
     *
     * Output
     * 122
     */
    public static function color_extract()
    {
        // todo: implement
    }

    /**
     * Calculates the perceived brightness of the given color. Uses W3C recommendations for
     * calculating perceived brightness, for the purpose of ensuring adequate contrast.
     *
     * Input
     * {{ '#7ab55c' | color_brightness }}
     *
     * Output
     * 153.21
     */
    public static function color_brightness()
    {
        // todo: implement
    }

    /**
     * Modifies the given component of a color.
     * Red, green and blue values should be a number between 0 and 255
     * Alpha should be a decimal number between 0 and 1
     * Hue should be between 0 and 360 degrees
     * Saturation and lightness should be a value between 0 and 100 percent.
     *
     * Input
     * {{ '#7ab55c' | color_modify: 'red', 255 }}
     *
     * Output
     * #ffb55c
     * The filter will return a color type that includes the modified format — for example,
     * if you modify the alpha channel, the filter will return the color in rgba() format,
     * even if your input color was in hex format.
     *
     * Input
     * {{ '#7ab55c' | color_modify: 'alpha', 0.85 }}
     *
     * Output
     * rgba(122, 181, 92, 0.85)
     */
    public static function color_modify()
    {
        // todo: implement
    }

    /**
     * Lightens the input color. Takes a value between 0 and 100 percent.
     *
     * Input
     * {{ '#7ab55c' | color_lighten: 30 }}
     *
     * Output
     * #d0e5c5
     */
    public static function color_lighten()
    {
        // todo: implement
    }

    /**
     * Darkens the input color. Takes a value between 0 and 100 percent.
     *
     * Input
     * {{ '#7ab55c' | color_darken: 30 }}
     *
     * Output
     * #355325
     */
    public static function color_darken()
    {
        // todo: implement
    }

    /**
     * Saturates the input color. Takes a value between 0 and 100 percent.
     *
     * Input
     * {{ '#7ab55c' | color_saturate: 30 }}
     *
     * Output
     * #6ed938
     */
    public static function color_saturate()
    {
        // todo: implement
    }

    /**
     * Desaturates the input color. Takes a value between 0 and 100 percent.
     *
     * Input
     * {{ '#7ab55c' | color_desaturate: 30 }}
     *
     * Output
     * #869180
     */
    public static function color_desaturate()
    {
        // todo: implement
    }

    /**
     * Blends together two colors. Blend factor should be a value value between 0 and 100 percent.
     *
     * Input
     * {{ '#7ab55c' | color_mix: '#ffc0cb', 50 }}
     *
     * Output
     * #bdbb94
     * If one input has an alpha component, but the other does not, an alpha component of 1 will
     * be assumed for the input without an alpha component.
     *
     * Input
     * {{ 'rgba(122, 181, 92, 0.75)' | color_mix: '#ffc0cb', 50 }}
     *
     * Output
     * rgba(189, 187, 148, 0.875)
     */
    public static function color_mix()
    {
        // todo: implement
    }

    /**
     * Theme settings return hexadecimal values for color settings by default. Use the hex_to_rgba
     * filter to convert the value into RGBA. It defaults to an opacity of 1, but you can specify
     * an opacity between 0 and 1. Shorthand hex values are accepted as well, for example, #812.
     *
     * Input
     * .site-footer h1 {
     *     color: {{ settings.color_name }};
     * }
     * .site-footer p {
     *     color: {{ settings.color_name | hex_to_rgba: 0.5 }};
     * }
     *
     * Output
     * .site-footer h1 {
     *     color: #812dd3;
     * }
     * .site-footer p {
     *     color: rgba(129, 45, 211, 0.5);
     * }
     */
    public static function hex_to_rgba()
    {
        // todo: implement
    }
}


