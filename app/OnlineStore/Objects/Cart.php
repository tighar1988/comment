<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\ShoppingCart\ShoppingCart;

class Cart extends Drop
{
    protected $cart;

    public function __construct(ShoppingCart $cart = null)
    {
        // todo: add a session shopping cart
        $this->cart = $cart;
    }

    /**
     * cart.attributes allow the capturing of more information on the cart page.
     * This is done by giving an input a nameattribute with the following syntax:
     *
     * attributes[attribute-name]
     *
     * Shown below is a basic example of how to use an HTML input of type "text" to capture information on the cart page.
     *
     * <label>What is your Pet's name?</label>
     * <input type="text" name="attributes[your-pet-name]" value="{{ cart.attributes.your-pet-name }}" />
     *
     * cart.attributes can be accessed in order email templates, the order status page of the checkout,
     * as well as in apps such as Order Printer.
     *
     * Input
     * {{ attributes.your-pet-name }}
     *
     * Output
     * Haku
     *
     * For more examples on how to use cart attributes, see Ask a customer for additional information.
     */
    public function attributes()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the number of items inside the cart.
     *
     * Input
     * {{ cart.item_count }} {{ cart.item_count | pluralize: 'Item', 'Items' }} ({{ cart.total_price | money }})
     *
     * Output
     * 25 Items ($53.00)
     */
    public function item_count()
    {
        if (is_null($this->cart)) {
            return 0;
        }

        return $this->cart->count();
    }

    /**
     * Returns all of the line items in the cart.
     */
    public function items()
    {
        if (is_null($this->cart)) {
            return [];
        }

        // group by inventory id
        $items = [];
        foreach ($this->cart->items as $item) {
            if (! isset($items[$item->inventory_id])) {
                $items[$item->inventory_id] = new LineItem($item);
            } else {
                $items[$item->inventory_id]->increaseQuantity();
            }
        }

        return array_values($items);
    }

    /**
     * cart.note allows the capturing of more information on the cart page.
     *
     * This is done by submitting the cart form with an HTML textarea and wrapping the cart.note output.
     * <label>Gift note:</label>
     * <textarea name="note">{{ cart.note }}</textarea>
     *
     * Note
     * There can only be one instance of {{ cart.note }} on the cart page. If there are multiple instances,
     * the one that comes latest in the Document Object Model (DOM) will be submitted with the form.
     *
     * cart.note can be accessed in order email templates, the order status page of the checkout, as well
     * as in apps such as Order Printer. For examples on how to use cart notes, see Ask a customer for
     * additional information.
     *
     * Input
     * {{ note }}
     *
     * Output
     * Hope you like the gift, Kylea!
     */
    public function note()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the original total of the cart before discounts were applied.
     */
    public function original_total_price()
    {
        return dollars_to_cents($this->cart->total ?? 0);
    }

    /**
     * Returns the total price of all of the items in the cart.
     */
    public function total_price()
    {
        return dollars_to_cents($this->cart->total ?? 0);
    }

    /**
     * Returns the total price of all of the items in the cart without shipping and tax.
     */
    public function total_without_shipping_and_tax()
    {
        if ($this->cart) {
            return dollars_to_cents($this->cart->totalWithoutShippingAndTax());
        }

        return 0;
    }

    /**
     * Returns the total weight of all of the items in the cart.
     */
    public function total()
    {
        return dollars_to_cents($this->cart->total ?? 0);
    }

    public function subtotal()
    {
        return dollars_to_cents($this->cart->subtotal ?? 0);
    }

    public function total_weight()
    {
        return null;
    }

    public function shipping_price()
    {
        return dollars_to_cents($this->cart->shipping_price ?? 0);
    }

    public function tax_total()
    {
        return dollars_to_cents($this->cart->tax_total ?? 0);
    }

    public function coupon_discount()
    {
        return dollars_to_cents($this->cart->coupon_discount ?? 0);
    }

    public function deduced_balance()
    {
        return dollars_to_cents($this->cart->deduced_balance ?? 0);
    }

    public function is_fully_paid_with_balance()
    {
        return (bool) $this->cart->isFullyPaidWithBalance ?? null;
    }
}
