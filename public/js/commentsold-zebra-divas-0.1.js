var available_printers = null;
var selected_category = null;
var default_printer = null;
var selected_printer = null;
//var format_start = "^XA^MMT^LL203^FO0,50^A0N36,36^FD";
//var format_start = "^XA^MMT^LL203^FO0,00^BCN,100,Y,N,N^^FD";
//var format_start_precode = "^XA^MMT^BY1,2.5^FO00,00^BCN,100,Y,N,N,A^FD";
var format_start_precode = "^XA^MMT^LH30,30^FO20,10^AFN,56,30^FR^FD"
var format_middle = "^FS^BY1,2.7^FO20,50^BCN,100,N,N,N,A^FD";
var format_end = "^XZ";
var default_mode = true;
var build_string = "";

function zebra_setup_web_print()
{
	//$('#printer_select').on('change', onPrinterSelected);
	console.log("Loading Printer Information...");
	default_mode = true;
	selected_printer = null;
	available_printers = null;
	selected_category = null;
	default_printer = null;
	
	BrowserPrint.getDefaultDevice('printer', function(printer)
	{
		default_printer = printer
		if((printer != null) && (printer.connection != undefined))
		{
			selected_printer = printer;
			document.getElementById('printer').innerHTML = "Found a Zebra! Printer ID:" + printer.name;
			console.log("Found a Zebra bitches! Printer ID:" + printer.name);
			checkPrinterStatus(function (text) {
				if (text != "Ready to Print") {
					document.getElementById('printer').innerHTML = text;
				}
			});



		}
		BrowserPrint.getLocalDevices(function(printers)
			{
				available_printers = printers;
				var printers_available = false;
				if (printers != undefined)
				{
					for(var i = 0; i < printers.length; i++)
					{
						if (printers[i].connection == 'usb')
						{
							var opt = document.createElement("option");
							opt.innerHTML = printers[i].connection + ": " + printers[i].uid;
							opt.value = printers[i].uid;
							printers_available = true;
						}
					}
				}
				
				if(!printers_available)
				{
					showErrorMessage("No Zebra Printers could be found!");
					hideLoading();
					$('#print_form').hide();
					return;
				}
				else if(selected_printer == null)
				{
					default_mode = false;
					changePrinter();
					$('#print_form').show();
					hideLoading();
				}
			}, undefined, 'printer');
	}, 
	function(error_response)
	{
		showBrowserPrintNotFound();
	});
};
function showBrowserPrintNotFound()
{
	showErrorMessage("An error occured while attempting to connect to your Zebra Printer. You may not have Zebra Browser Print installed, or it may not be running. Install Zebra Browser Print, or start the Zebra Browser Print Service, and try again.");

};

function zebraLabelCut(variant) {
	console.log("Saying what the variant is, then cutting");
	build_string += '^XA^MMC^LH30,30^FO20,10^AFN,56,30^FR^FD' + variant + ' ^FS^XZ';
}

function zebraPrintTag(label, barcode_value) {
	console.log("Trying to send " + format_start_precode + label + format_middle + barcode_value + format_end + " to " + selected_printer.name);
	build_string += format_start_precode + label + format_middle + barcode_value + format_end, printComplete, printerError;
}

function printBuildString() {
	console.log("Sending string: " + build_string);
	selected_printer.sendThenRead(build_string, function(response) {
		console.log("Response: " + response);
        build_string="";
	});
    build_string="";
}

function sendData()
{
	showLoading("Printing...");
	checkPrinterStatus( function (text){
		if (text == "Ready to Print")
		{
			selected_printer.send(format_start + $('#entered_name').val() + format_end, printComplete, printerError);
		}
		else
		{
			printerError(text);
		}
	});
};
function checkPrinterStatus(finishedFunction)
{
	selected_printer.sendThenRead("~HQES", 
				function(text){
						var that = this;
						var statuses = new Array();
						var ok = false;
						var is_error = text.charAt(70);
						var media = text.charAt(88);
						var head = text.charAt(87);
						var pause = text.charAt(84);
						// check each flag that prevents printing
						if (is_error == '0')
						{
							ok = true;
							statuses.push("Ready to Print");
						}
						if (media == '1')
							statuses.push("Paper out");
						if (media == '2')
							statuses.push("Ribbon Out");
						if (media == '4')
							statuses.push("Media Door Open");
						if (media == '8')
							statuses.push("Cutter Fault");
						if (head == '1')
							statuses.push("Printhead Overheating");
						if (head == '2')
							statuses.push("Motor Overheating");
						if (head == '4')
							statuses.push("Printhead Fault");
						if (head == '8')
							statuses.push("Incorrect Printhead");
						if (pause == '1')
							statuses.push("Printer Paused");
						if ((!ok) && (statuses.Count == 0))
							statuses.push("Error: Unknown Error");
						finishedFunction(statuses.join());
			}, printerError);
};
function hidePrintForm()
{
	$('#print_form').hide();
};
function showPrintForm()
{
	$('#print_form').show();
};
function showLoading(text)
{
	$('#loading_message').text(text);
	$('#printer_data_loading').show();
	hidePrintForm();
	$('#printer_details').hide();
	$('#printer_select').hide();
};
function printComplete()
{
	console.log("Printing complete");
}
function hideLoading()
{
	$('#printer_data_loading').hide();
	if(default_mode == true)
	{
		showPrintForm();
		$('#printer_details').show();
	}
	else
	{
		$('#printer_select').show();
		showPrintForm();
	}
};
function changePrinter()
{
	default_mode = false;
	selected_printer = null;
	$('#printer_details').hide();
	if(available_printers == null)
	{
		showLoading("Finding Printers...");
		$('#print_form').hide();
		setTimeout(changePrinter, 200);
		return;
	}
	$('#printer_select').show();
	onPrinterSelected();
	
}
function onPrinterSelected()
{
	selected_printer = available_printers[$('#printers')[0].selectedIndex];
}
function showErrorMessage(text)
{
	$('#main').hide();
	$('#error_div').show();
	$('#error_message').html(text);
}
function printerError(text)
{
	console.log("An error occurred while printing. Please try again." + text);
}
function trySetupAgain()
{
	$('#main').show();
	$('#error_div').hide();
	setup_web_print();
	//hideLoading();
}

