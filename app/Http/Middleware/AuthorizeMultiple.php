<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;

class AuthorizeMultiple
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$permissions)
    {
        if (! Auth::check()) {
            $this->deny();
        }

        $hasPermission = false;

        foreach ($permissions as $permission) {
            if (Auth::user()->can($permission)) {
                $hasPermission = true;
                break;
            }
        }

        if (! $hasPermission) {
            $this->deny();
        }

        return $next($request);
    }

    /**
     * Throws an unauthorized exception.
     *
     * @param  string  $message
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function deny($message = 'This action is unauthorized.')
    {
        throw new AuthorizationException($message);
    }
}
