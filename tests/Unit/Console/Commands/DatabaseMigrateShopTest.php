<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\DatabaseMigrateShop;
use Mockery;

class DatabaseMigrateShopTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(DatabaseMigrateShop::class, new DatabaseMigrateShop);
    }
}
