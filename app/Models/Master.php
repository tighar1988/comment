<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Redis;

class Master extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'master';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    public function enabledShops()
    {
        return Master::distinct()->whereNotNull('shop_name')
            ->where('superadmin', true)
            ->where('is_enabled', '=', '1')
            ->pluck('shop_name')->toArray();
    }

    public static function shopsList()
    {
        return Master::distinct()->whereNotNull('shop_name')->pluck('shop_name')->toArray();
    }

    public static function shopsListAndUsers()
    {
        return Master::distinct()->whereNotNull('shop_name')->where('superadmin', true)->get();
    }

    /**
     * Find the shop database from the domain.
     *
     * @param string $domain The shop domain without http.
     * @return string
     */
    public static function findShopByDomain($domain)
    {
        $domain = str_replace('www.', '', $domain);
        $domain = trim($domain, '/\\ ');

        // try to get it from redis
        $shop = Redis::connection('cache')->get("domain:{$domain}");
        if (! is_null($shop)) {
            return $shop;
        }

        // get it from the db, where it ends with this domain
        $account = static::where('superadmin', '=', true)->where('shop_domain', 'like', "%{$domain}")->first();
        $shop = $account->shop_name ?? null;

        // store it in redis
        Redis::connection('cache')->set("domain:{$domain}", $shop);

        return $shop;
    }

    public static function findDomainByShop($shop)
    {
        $shop = str_replace('_', '-', $shop);

        // try to get it from redis
        $domain = Redis::connection('cache')->get("shop-name:{$shop}");
        if (! is_null($domain)) {
            return $domain;
        }

        // get it from the db, where it ends with this domain
        $account = static::where('superadmin', '=', true)->where('shop_name', '=', "{$shop}")->first();
        $domain = $account->shop_domain ?? null;

        // if found in the db, store it in redis
        Redis::connection('cache')->set("shop-name:{$shop}", $domain);

        return $domain;
    }

    public static function findByShop($shop)
    {
        $shop = str_replace('_', '-', $shop);

        return static::where('superadmin', '=', true)->where('shop_name', '=', "{$shop}")->first();
    }
}
