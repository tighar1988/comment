<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\ImportExternalImages;
use Mockery;

class ImportExternalImagesTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(ImportExternalImages::class, new ImportExternalImages);
    }
}
