<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Inventory;

class InventoryTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Inventory::class, new Inventory);
    }

    /** @test */
    public function it_sets_weight_with_oz()
    {
        $inventory = new Inventory;
        $inventory->setWeightInOz(10, 'oz');

        $this->assertEquals($inventory->weight, 10);
    }

    /** @test */
    public function it_sets_weight_with_wrong_unit()
    {
        $inventory = new Inventory;
        $inventory->setWeightInOz(10, 'bad unit');

        $this->assertEquals($inventory->weight, 0);
    }

    /** @test */
    public function it_sets_weight_with_grams()
    {
        $inventory = new Inventory;
        $inventory->setWeightInOz(100, 'g');

        $this->assertEquals($inventory->weight, 4);
    }

    /** @test */
    public function it_sets_weight_with_kg()
    {
        $inventory = new Inventory;
        $inventory->setWeightInOz(0.5, 'kg');

        $this->assertEquals($inventory->weight, 18);
    }

    /** @test */
    public function it_sets_weight_with_lb()
    {
        $inventory = new Inventory;
        $inventory->setWeightInOz(1.5, 'lb');

        $this->assertEquals($inventory->weight, 24);
    }

    /** @test */
    public function it_gets_weight_in_oz()
    {
        $this->assertEquals(Inventory::getWeightInOz(1.5, 'lb'), 24);
    }
}
