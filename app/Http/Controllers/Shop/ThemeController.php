<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Collection;
use App\Models\Product;
use App\Models\TagGroup;
use App\Models\Tag;
use App\Models\Cart;
use App\Models\EmailList;
use Validator;
use Illuminate\Support\Facades\Auth;

class ThemeController extends Controller
{
    /**
     * The store home page.
     */
    public function index()
    {
        $collection = Collection::handle('new-arrivals')->first();
        $products = $collection ? $collection->products()->with('inventory', 'images')->take(8)->get() : [];
        return theme('templates.index', compact('products', 'collection'));
    }

    /**
     * The login page.
     */
    public function login()
    {
        if (Auth::check()) {
            return shop_redirect('/account');
        }

        $shopName = shop_name();
        return view('shop.welcome', compact('shopName'));
    }

    /**
     * Subscribe to the email list.
     */
    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:email_list',
        ], ['unique' => 'You are already subscribed!']);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()]);
        }

        EmailList::create(['email' => request('email')]);
        return response()->json(['message' => 'You are now subscribed. Awesome!']);
    }
}
