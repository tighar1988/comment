@extends('admin.setup.setup')

@section('section', 'Instagram')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                @if (shop_setting('instagram.token'))
                    <span class="label label-success">CONNECTED</span>
                    <span class="settings-info">Your Instagram account is connected!</span><br/><br/>

                    <h4>You are connected as <strong>{{ shop_setting('instagram.nickname') }}</strong></h4>
                    <img class="thumbnail" src="{{ shop_setting('instagram.avatar') }}">

                    <a class="btn btn-info btn-lg" href="/admin/setup/instagram-connect">Re-connect Instagram</a>
                @else
                    <span class="settings-info">Connect Your Instagram Account</span><br/>
                    <a class="btn btn-info btn-lg" href="/admin/setup/instagram-connect">Connect Instagram</a>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
