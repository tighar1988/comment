<?php

namespace App\Jobs;

use App\Mails\ShopMailer;
use App\Models\Cart;
use Carbon\Carbon;
use Exception;
use DB;
use App\Exceptions\ShopifyWebhookLockedException;

class ShoppingCartExpire
{
    /**
     * Expire job.
     *
     * @return void
     */
    public function handle()
    {
        $expire = expire();
        $remind = remind();
        $now = Carbon::now();
        $mailer = new ShopMailer;

        $cartItems = Cart::all();

        foreach ($cartItems as $item) {

            $expireAt = $item->created_at->addHours($expire);

            // item is expired
            if ($expireAt->lessThan($now)) {
                log_info("[expire] Expiring cart item with inventory #{$item->inventory_id}. CID #{$item->user_id}");

                try {
                    $item->variant->incrementQuantity("Expired user's #{$item->user_id} shopping cart item. Adding item back to inventory.");
                    $item->delete();
                } catch (ShopifyWebhookLockedException $e) {
                    ShopMailer::notifyAdmin($e);
                    log_error("[expire][shopify] $e");
                    continue;
                }

            } else {
                // how many max emails to send
                $howmany = $expire > $remind ? floor($expire / $remind) : 1;

                for ($i = 1; $i <= $howmany; $i++) {
                    $nextEmailAt = $item->created_at->addHours($i * $remind);

                    if($now->lessThan($nextEmailAt)) {
                        break;

                    } elseif (is_null($item->reminded_at) || $nextEmailAt->greaterThan($item->reminded_at)) {
                        $mailer->sendExpiryReminderEmail($item->customer, $item->variant->product, $item->variant);
                        $item->reminded_at = $nextEmailAt;
                        $item->save();
                        break;
                    }
                }
            }
        }
    }
}
