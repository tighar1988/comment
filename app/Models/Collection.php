<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;
use DB;

class Collection extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'collections';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content', // shopify body_html
        'seo_title',
        'seo_description',
        'url', // shopify handle
        'image', // shopify src
        'image_created_at',
        'image_width',
        'image_height',
        'weight',
        'published_at',
        'sort_order',
        'template_suffix',
        'published_scope',
        'shopify_collection_id',
        'smart_collection',
        'smart_collection_rules',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'smart_collection' => 'boolean',
    ];

    public function imageUrl()
    {
        if (! is_null($this->shopify_collection_id)) {
            return $this->image;
        }

        if (! is_null($this->image)) {
            return Storage::url(shop_id() . '/collection/' . $this->image);
        }

        return null;
    }

    public function scopeHandle($query, $handle)
    {
        if ($handle == 'all') {
            return $query;
        }

        $handle = trim($handle, '/ ');
        return $query->where('url', '=', $handle);
    }

    public function getUrl()
    {
        return '/collections/' . trim($this->url, ' /');
    }

    public function getUrlAttribute($value)
    {
        return trim($value);
    }

    public function storeProducts()
    {
        $query = $this->products()
            ->whereHas('image');

        if (shop_id() == 'glamourfarms') {
            // Show product as "sold out" only if the product was published within the last 7 days.
            // Otherwise, just hide products that are completely sold out
            $daysAgo = time() - (60*60*24 * 7);
            $query->whereRaw("(
                COALESCE((SELECT SUM(quantity) FROM inventory WHERE products.id = inventory.product_id
                    GROUP BY inventory.product_id), 0) > 0
                OR products.published_at > {$daysAgo}
            )");
            $query->orderBy('products.id');
        }

        return $query;
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->published();
    }

    public function productsCount()
    {
        return $this->belongsToMany('App\Models\Product')->count();
    }

    public function getPreviousFor($product)
    {
        $result = $this->products()
            ->where('product_id', '<', $product->id)
            ->where('collection_id', '=', $this->id)
            ->orderBy('product_id', 'ASC')
            ->take(1)->get();

        return $result->first();
    }

    public function getNextFor($product)
    {
        $result = $this->products()
            ->where('product_id', '>', $product->id)
            ->where('collection_id', '=', $this->id)
            ->orderBy('product_id', 'ASC')
            ->take(1)->get();

        return $result->first();
    }

    /**
     * First 8 products in the collection (without the current product)
     */
    public function moreGreatFinds($product)
    {
        return $this->products()->with('image', 'inventoryQuantity', 'price')
            ->where('product_id', '!=', $product->id)
            ->where('collection_id', '=', $this->id)
            ->orderBy('product_id', 'ASC')
            ->take(8)->get();
    }

    public function allTags()
    {
        $collectionId = $this->id;

        return DB::connection('shop')->table('tagging_tags')
            ->select("tagging_tags.name")
            ->join('tagging_tagged', function ($join) {
                $join->on('tagging_tags.id', '=', 'tagging_tagged.tag_id')
                    ->where('tagging_tagged.taggable_type', '=', 'product');
            })
            ->join('collection_product', function ($join) use ($collectionId) {
                $join->on('tagging_tagged.taggable_id', '=', 'collection_product.product_id')
                    ->where('collection_product.collection_id', '=', $collectionId);
            })
            ->distinct()
            ->get();
    }
}
