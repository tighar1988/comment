<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FacebookPostFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'group'   => 'required|numeric',
            'message' => 'required',
            'timing'  => 'required|in:1,2',
        ];

        // todo: validate correct dates, hour, min input values
        if ($this->timing == '2') {
            $rules['schedule_date'] = 'required';
            $rules['hour']          = 'required';
            $rules['min']           = 'required';
            $rules['ampm']          = 'required|in:am,pm';
        }

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $data = $validator->getData();
            if (isset($data['timing']) && $data['timing'] == 2 && ! shop_setting('shop.timezone')) {
                $validator->errors()->add('timing', 'To schedule a post, please select your Timezone in Setup!');
            }
        });
    }
}
