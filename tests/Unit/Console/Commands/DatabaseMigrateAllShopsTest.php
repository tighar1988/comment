<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\DatabaseMigrateAllShops;
use Mockery;

class DatabaseMigrateAllShopsTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(DatabaseMigrateAllShops::class, new DatabaseMigrateAllShops);
    }
}
