<?php

namespace Tests\Unit\Reports;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Reports\Report;

class ReportTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Report::class, new Report);
    }
}
