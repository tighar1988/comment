@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Dashboard</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Dashboard</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body lr-m-15">
                    @can('view-dashboard')
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-body alert-info">
                                    <div class="text-center">
                                        <div class="h1">{{ $totalOrders }}</div>
                                        <div class="text-uppercase">Total Orders</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-body alert-danger">
                                    <div class="text-center">
                                        <div class="h1">{{ $totalCustomers }}</div>
                                        <div class="text-uppercase">Total Customers</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-body alert-success">
                                    <div class="text-center">
                                        <div class="h1">${{ amount($totalRevenue) }}</div>
                                        <div class="text-uppercase">Total Revenue</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-body alert-warning">
                                    <div class="text-center">
                                        <div class="h1">${{ amount($totalPotential) }}</div>
                                        <div class="text-uppercase">Pending Revenue</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <h2>Latest Orders</h2>
                            <div class="table-responsive">
                                <table id="latest-orders" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Customer</th>
                                            <th>Order Date</th>
                                            <th>Fulfillment</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($orders->isEmpty())
                                            <tr><td colspan="4">No orders found.</td></tr>
                                        @endif
                                        @foreach ($orders as $order)
                                            <tr>
                                                <td>#{{ $order->id }}</td>
                                                <td><a href="/admin/customers/{{ $order->customer->id ?? null }}">{{ $order->customer->name ?? null }}</a></td>
                                                <td>{{ $order->created_at }}</td>
                                                <td>
                                                    @if ($order->isPaid())
                                                        @if ($order->isProcessing())
                                                            <span class="label label-danger">Processing</span>
                                                        @elseif ($order->isPrinted())
                                                            <span class="label label-info">Label Printed</span>
                                                        @else
                                                            <span class="label label-warning">Unfulfilled</span>
                                                        @endif
                                                    @elseif ($order->isFulfilled())
                                                        <span class="label label-success">Fulfilled</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <a class="pull-right" href="/admin/orders">View All Orders</a>
                        </div>
                    </div>
                    @else
                        Hello, {{ Auth::user()->name }}
                        <br/><br/><br/>
                    @endcan

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection
