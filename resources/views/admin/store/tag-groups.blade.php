@extends('admin.store.store')

@section('section', 'Tag Groups')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body r-p">
                <a href="/admin/store/tags/add" class="btn btn-success add-btn btn-sm">Add Tag Group</a>
                <table id="tags-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Tags</th>
                            <th>Last Modified</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tagGroups as $tagGroup)
                            <tr>
                                <td>{{ $tagGroup->name }}</td>
                                <td>{{ $tagGroup->description }}</td>
                                <td>
                                    @if ($tagGroup->tags->isEmpty())
                                        There are no tags.
                                    @endif
                                    @foreach ($tagGroup->tags as $tag)
                                        <span class="label label-danger">{{ $tag->name }}</span>
                                        @if ($loop->iteration > 10)
                                            <span>(and {{ $loop->remaining }} others)</span>
                                            @break
                                        @endif
                                    @endforeach
                                </td>
                                <td>{{ style_date($tagGroup->updated_at) }}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-h fa-lg"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                            <li><a href="/admin/store/tags/edit/{{ $tagGroup->id }}" class="link">Edit</a></li>
                                            <li><a href="/admin/store/tags/delete/{{ $tagGroup->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#tags-table').DataTable({
            "bStateSave": true
        });
    });
</script>
@endsection
