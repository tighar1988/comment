<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\DatabaseCreate;
use Mockery;

class DatabaseCreateTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $mock = Mockery::mock('App\Shop\DatabaseCreator');
        $command = new DatabaseCreate($mock);

        $this->assertInstanceOf(DatabaseCreate::class, $command);
    }

    /** @test */
    public function it_executes()
    {
        $mock = Mockery::mock('App\Shop\DatabaseCreator');
        $mock->shouldReceive('create')->with('my-shop-name');
        app()->instance('App\Shop\DatabaseCreator', $mock);

        $this->artisan('database:create', ['name' => 'my-shop-name']);
    }
}
