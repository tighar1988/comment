@extends('layouts.app')

@section('content')
<div class="login-box">
    <div class="login-box-body">
        <div class="login-logo">
            <a href="/"><img src="{{ cdn('/images/login-logo.png') }}" alt="CommentSold"></a>
        </div>
        <p class="login-box-msg">Log in to start your session</p>
        <form role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <div class="form-group has-feedback @hasError('email')">
                <input type="email" name="email" value="{{ old('email') }}" required autofocus class="form-control" placeholder="E-Mail Address">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @error('email')
            </div>

            <div class="form-group has-feedback @hasError('password')">
                <input type="password" class="form-control" name="password" required placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password')
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat m-b-20">Login</button>
                </div>
            </div>
        </form>
        <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
        <a href="{{ url('/register') }}" class="text-center">Register a new account</a>
    </div>
</div>
@endsection
