@extends('admin.setup.shipping.shipping')

@section('setup')
@include('admin.modals.delete-confirm')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <span class="settings-info">Multiple Locations</span>
                <span class="settings-muted">If you have multiple local pickup locations, you can enable this option to allow the customer to choose which store location he wants to use. The tax applied will be the highest from all the locations entered.</span>
                <hr/>

                @if (local_pickup_enabled())
                    @if (multiple_locations_enabled())
                        <span class="settings-info">Multiple Locations: (Enabled)</span>
                        <form class="" role="form" method="POST" action="{{ route('disable_location') }}">
                            <button class="btn btn-danger test" type="submit">Disable Multiple Locations</button>
                        </form>
                    @else
                        <span class="settings-info">Multiple Locations: (Disabled)</span>
                        <form class="" role="form" method="POST" action="{{ route('enable_location') }}">
                            <button class="btn btn-success test" type="submit">Enable Multiple Locations</button>
                        </form>
                    @endif
                @else
                    <span class="settings-info">To enable multiple locations, first enable local pick-up.</span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <div id="final_msg" style="display:none;" class="alert alert-success">
                    <strong>Success!</strong> Location updated.
                </div>
                <div class="col-xs-12 form-group row">
                    <div id="form-errors"></div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <button type="button" class="btn btn-primary add_button">Add+</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Location</th>
                                <th>State</th>
                                <th>Zip Code</th>
                                <th>Address</th>
                                <th>Default</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($locations as $location)
                            <tr data-id="{{$location->id}}">
                                <td data-info="location">{{ $location->location }}</td>
                                <td data-info="state">{{ $location->state }}</td>
                                <td data-info="zip_code">{{ $location->zip_code }}</td>
                                <td data-info="address">{{ $location->address }}</td>
                                <td><input @if($location->isDefault()) checked @endif name="default" type="radio"></td>
                                <td>
                                    <a class="btn btn-primary edit_form" href="javascript:">Edit</a>
                                    <form method="Post" style="display:inline-block" action="{{ route('locations.destroy', $location->id ) }}">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input data-toggle="modal" data-target="#confirmDelete" class="btn btn-danger delete_tr" type="button" value="Delete">
                                    </form>
                                </td>
                             </tr>
                            <tr data-id="{{$location->id}}" style="display: none">
                                <td><input name="location" type="text" class="form-control" value="{{ $location->location }}"></td>
                                <td>
                                    <select class="form-control" name="state">
                                        @include('common.states', ['selected' => $location->state])
                                    </select>
                                </td>
                                <td><input name="zip_code" type="text" class="form-control" value="{{ $location->zip_code }}"></td>
                                <td><input name="address" type="text" class="form-control" value="{{ $location->address }}"></td>
                                <td></td>
                                <td><input class="btn btn-warning update_form" type="button" value="Update">
                                    <a href="javascript:" class="btn btn-default cancel">Cancel</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <form method="post" action="{{ route('locations.store') }}" class="@if (!count($errors)>0) invisible @endif  form">
                        {{ csrf_field() }}
                        <div class="form-group col-md-3">
                            <label for="location">Location</label>
                            <input name="location" value="{{ old('location') }}" class="form-control" id="location" type="text">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="state">State</label>
                            <select class="form-control" name="state">
                                <?php $selected = old('state', shop_setting('shop.state')); ?>
                                @if (is_null($selected))
                                    <option value="" selected="selected">State..</option>
                                @endif
                                @include('common.states', compact('selected'))
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="zip-code">Zip Code</label>
                            <input name="zip_code" class="form-control" value="{{ old('zip_code') }}" id="zip-code" type="text">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="address">Address</label>
                            <input name="address" class="form-control" value="{{ old('address') }}" id="address" type="text">
                        </div>
                        <div class="form-group col-xs-12">
                            <input class="btn btn-primary" type="submit" value="Save">
                            <a href="javascript:" id="cancel" name="cancel" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/custom_admin.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#confirmDelete').on('show.bs.modal', function (e) {
                $message = $(e.relatedTarget).attr('data-message');
                $(this).find('.modal-body p').text($message);
                $title = $(e.relatedTarget).attr('data-title');
                $(this).find('.modal-title').text($title);
                // Pass form reference to modal for submission on yes/ok
                var form = $(e.relatedTarget).closest('form');
                $(this).find('.modal-footer #confirm').data('form', form);
            });
            // Form confirm (yes/ok) handler, submits form
            $('#confirmDelete').find('.modal-footer #confirm').on('click', function () {
                console.log($(this).data('form'));
                $(this).data('form').submit();
            });
        });
    </script>
@endsection
