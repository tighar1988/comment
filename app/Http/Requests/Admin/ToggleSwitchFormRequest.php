<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class ToggleSwitchFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $keys = [
            'setup.notify-invalid-comment',
            'setup.notify-item-waitlisted',
            'setup.notify-waitlisted-activated',
            'setup.notify-order-without-email',
            'setup.notify-got-order',
            'shipping.auto-buy-prepaid-credit',
            'shipping.combine-orders',
            'shopify.sync-orders',
            'messenger.send-contact-message',
        ];

        return [
            'state' => 'required|in:true,false',
            'key'   => 'required|in:' . implode(',', $keys),
        ];
    }
}
