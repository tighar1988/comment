<?php

namespace Tests\Unit\Shopify;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Instagram\InstagramShopperComment;

class InstagramShopperCommentTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $comment = [
            'id' => '10434324343',
            'text' => 'sold blue M',
        ];

        $this->assertInstanceOf(InstagramShopperComment::class, new InstagramShopperComment($comment));
    }
}
