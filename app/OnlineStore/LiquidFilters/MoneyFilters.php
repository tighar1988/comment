<?php

namespace App\OnlineStore\LiquidFilters;

/**
 * Money filters format prices based on the Currency Formatting found in General Settings.
 */
class MoneyFilters
{
    /**
     * Formats the price based on the shop's HTML without currency setting.
     *
     * Input
     * {{ 145 | money }}
     *
     * Output
     * <!-- if "HTML without currency" is ${{ amount }} -->
     * $1.45
     * <!-- if "HTML without currency" is €{{ amount_no_decimals }} -->
     * €1
     */
    public static function money($amount)
    {
        $amount = (float)$amount / 100;
        return '$'.number_format(floatval($amount), 2);
    }

    /**
     * Formats the price based on the shop's HTML with currency setting.
     *
     * Input
     * {{ 1.45 | money_with_currency }}
     *
     * Output
     * <!-- if "HTML with currency" is ${{ amount }} CAD -->
     * $1.45 CAD
     */
    public static function money_with_currency($amount)
    {
        $amount = (float)$amount / 100;
        return '$'.number_format(floatval($amount), 2) . ' USD';
    }

    /**
     * Formats the price based on the shop's HTML with currency setting and excludes the decimal point and trailing zeros.
     *
     * Input
     * <!-- if "HTML with currency" is ${{ amount }} CAD -->
     * {{ 2000 | money_without_trailing_zeros }}
     *
     * Output
     * $20
     * Only trailing zeros are removed, not other digits:
     *
     * Input
     * <!-- if "HTML with currency" is ${{ amount }} CAD -->
     * {{ 145 | money_without_trailing_zeros }}
     *
     * Output
     * $1.45
     */
    public static function money_without_trailing_zeros($amount)
    {
        $amount = (float)$amount / 100;
        return '$'.number_format(floatval($amount), 0);
    }

    /**
     * Formats the price using a decimal.
     *
     * Input
     * {{ 145 | money_without_currency }}
     *
     * Output
     * 1.45
     */
    public static function money_without_currency($amount)
    {
        $amount = (float)$amount / 100;
        return number_format(floatval($amount), 2);
    }
}


