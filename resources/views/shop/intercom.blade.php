<script>
window.intercomSettings = {
    app_id: "iptppx0i",
    name: "{{ $customer->name }}",
    email: "{{ $customer->email }}",
    user_id: "{{ $customer->id }}",
    customer_link: "https://commentsold.com/admin/customers/{{ $customer->id }}",
    amount_spent: "{{ amount($customer->totalSpent()) }}",
    last_order_date: "{{ $customer->lastOrderTimestamp() }}",
    hide_default_launcher: true
};
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/iptppx0i';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
