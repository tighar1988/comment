<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $shopName }}</title>
        <link href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ cdn('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Styles -->
        <style>
            html, body {
                font-weight: 100;
                height: 100vh;
                margin: 0;
                color: #282c45;
                background: #f4efe5;
                font-size: 16px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 65px;
                font-weight: 800;
                margin-top: -30px;
                margin-bottom: 50px;
                position: relative;
                line-height: 1.1;
                letter-spacing: -1px;
                color: #282c45;
            }
            .description {
                font-size: 1.35em;
                font-weight: 400;
                margin: 0px 0px 50px 0px;
                line-height: 2em;
                color: #282c45;
                max-width: 525px;
            }

            .btn-social {
                position: relative;
                padding-left: 44px;
                text-align: left;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .btn-facebook {
                color: #fff;
                background-color: #3b5998;
                border-color: rgba(0,0,0,0.2);
            }
            .btn-facebook:active,
            .btn-facebook:focus,
            .btn-facebook:hover {
                color: #fff;
                background-color: #2d4373;
                border-color: rgba(0,0,0,0.2);
            }
            .btn-instagram {
                color:#fff;
                background-color: #ea1e63;
                border-color: rgba(0,0,0,0.2);
            }
            .btn-instagram:active,
            .btn-instagram:focus,
            .btn-instagram:hover {
                color:#fff;
                background-color: rgb(239, 62, 122);
                border-color: rgba(0,0,0,0.2);
            }
            .btn-social>:first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 32px;
                line-height: 34px;
                font-size: 1.6em;
                text-align: center;
                border-right: 1px solid rgba(0,0,0,0.2);
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    {{ $shopName }}
                </div>
                <p class="description">
                    {{ shop_description() }}
                </p>

                @if (in_array(shop_id(), ['shop1', 'bktest', 'shopwhiskeydarling', 'classycotton', 'sealwithabow', 'tarahsutton']))
                    {{-- Both Facebook and Instagram Login --}}
                    <div class="links">
                        @include('flash::message')
                        <div class="text-center" style="padding: 10px;">Please select which social network you wish to sign into</div>
                        <div class="row">
                            <div class="col-md-12">
                                <a style="margin: 5px;" class="btn btn-social btn-facebook" href="{{ shop_url('/facebook-login') }}"><span class="fa fa-facebook"></span> Login with Facebook</a>
                                <a  style="margin: 5px;" class="btn btn-social btn-instagram" href="{{ shop_url('/instagram-login') }}"><i class="fa fa-instagram"></i> Login with Instagram</a>
                            </div>
                        </div>
                    </div>
                @elseif (in_array(shop_id(), ['poppyanddot']))
                    {{-- Only Instagram Login --}}
                    <div class="links">
                        @include('flash::message')
                        <a class="btn btn-social btn-instagram" href="{{ shop_url('/instagram-login') }}"><i class="fa fa-instagram"></i> Login with Instagram</a>
                    </div>
                @else
                    {{-- Only Facebook Login --}}
                    <div class="links">
                        @include('flash::message')
                        <a class="btn btn-social btn-facebook" href="{{ shop_url('/facebook-login') }}"><span class="fa fa-facebook"></span> Login with Facebook</a>
                    </div>
                @endif
            </div>
        </div>
        {!! shop_setting('embed-code') !!}
    </body>
</html>
