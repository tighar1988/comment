<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Jobs\SendMessengerText;

class SendMessengerTextTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(SendMessengerText::class, new SendMessengerText('shop', []));
    }
}
