<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ExpireFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expire' => 'required|numeric|min:1|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'expire.required' => 'The expire field is required.',
            'expire.numeric'  => 'The expire field must be numeric.',
            'expire.min'      => 'The expire field must be at least 1 hour.',
        ];
    }
}
