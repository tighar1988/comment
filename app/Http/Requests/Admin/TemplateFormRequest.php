<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class TemplateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $keys = ['invalid_comment', 'item_waitlisted', 'waitlist_activated', 'order_without_email', 'new_order', 'awarded_account_credit'];

        return [
            'key'      => 'required|in:' . implode(',', $keys),
            $this->key => 'required|min:10',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'invalid_comment.required' => 'The template for the invalid comment message is required.',
        ];
    }
}
