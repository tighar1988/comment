<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Checkout\CheckoutException;

class CheckoutController extends Controller
{
    /**
     * Stripe checkout with new card.
     */
    public function checkoutWithNewCard()
    {
        try {
            $customer = request()->user();
            $customer->checkout()->withNewCard(request('token'));
            return response()->json(['status' => 1]);

        } catch(CheckoutException $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Stripe checkout with existing card.
     */
    public function checkoutWithExistingCard()
    {
        try {
            $customer = request()->user();
            $customer->checkout()->withExistingCard(request('card_id'));
            return response()->json(['status' => 1]);

        } catch(CheckoutException $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Checkout fully with account credit.
     */
    public function checkoutWithAccountCredit()
    {
        try {
            $customer = request()->user();
            $customer->checkout()->withAccountCredit();
            return response()->json(['status' => 1]);

        } catch(CheckoutException $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Checkout using the shop owner's PayPal Account.
     */
    public function paypalApprovalLink()
    {
        try {
            $customer = request()->user();
            $link = $customer->checkout()->paypalApprovalLink();
            return response()->json(['status' => 1, 'link' => $link]);

        } catch(CheckoutException $e) {
            return response()->json(['status' => 0, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Paypal success return url. Execute the payment.
     */
    public function paypalConfirm()
    {
        // todo: treat case where user is about to checkout (especially in paypal checkout), but item is activated from the waitlist and added back to the cart; this would mean either that the user would pay more if the item was activated before the user refreshed the page; or it can be the user would get a free item, if he used paypal to pay for items in the cart, then the item would be activated, then the order would be created; maybe we show a confirmation view here instead of automatically chargin them

        try {
            $customer = request()->user();
            $customer->checkout()->checkoutWithPayPal(request('paymentId'), request('PayerID'), $customer);

            // redirect to paypal success view because we don't want the user to reload this confirm url
            return redirect(shop_url('/paypal-order-success'));

        } catch(CheckoutException $e) {
            $errorMessage = $e->getMessage();
            return view('shop.paypal-fail', compact('errorMessage'));
        }
    }

    /**
     * Paypal success view. Show success message.
     */
    public function paypalOrderSuccess()
    {
        return view('shop.paypal-success');
    }

    /**
     * Paypal cancel return url. Cancel the payment.
     */
    public function paypalCancel()
    {
        return view('shop.paypal-cancel');
    }
}
