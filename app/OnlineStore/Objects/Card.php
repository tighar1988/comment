<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Card as CardModel;

class Card extends Drop
{
    protected $card;

    public function __construct(CardModel $card)
    {
        $this->card = $card;
    }

    public function card_id()
    {
        return $this->card->card_id;
    }

    public function last_four()
    {
        return $this->card->last_four;
    }

    public function exp_month()
    {
        return $this->card->exp_month;
    }

    public function exp_year()
    {
        return $this->card->exp_year;
    }

    public function brand()
    {
        return $this->card->brand;
    }
}
