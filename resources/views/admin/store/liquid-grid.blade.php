@extends('admin.store.themes')

@section('section', 'Liquid')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body r-p">
                @if ($type != 'config')
                    <a href="/admin/store/themes/{{ $type }}/add" class="btn btn-success add-btn btn-sm">Add {{ ucfirst($type) }}</a>
                @endif
                <table id="liquid-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>File Name</th>
                            <th>Last Modified</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($liquid as $row)
                            <tr>
                                <td>
                                    @if (! $row->isEditable($type) || $type == 'assets')
                                        <a target="_blank" href="/admin/store/themes/assets/open/{{ $row->file_name }}" class="link">{{ $row->file_name }}</a>

                                    @elseif (! empty($row->id))
                                        <a href="/admin/store/themes/{{ $type }}/edit/{{ $row->id }}" class="link">{{ $row->file_name }}</a>

                                    @else
                                        <a href="/admin/store/themes/{{ $type }}/change/{{ $row->file_name }}" class="link">{{ $row->file_name }}</a>
                                    @endif
                                </td>
                                <td>
                                    @if (! empty($row->updated_at))
                                        {{ style_date($row->updated_at) }}
                                    @endif
                                </td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-h fa-lg"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                            @if ($type == 'assets')
                                                @if (! empty($row->id))
                                                    <li><a href="/admin/store/themes/{{ $type }}/delete/{{ $row->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                                @endif
                                                <li><a target="_blank" href="/admin/store/themes/assets/open/{{ $row->file_name }}" class="link">Open</a></li>

                                            @elseif (! empty($row->id))
                                                <li><a href="/admin/store/themes/{{ $type }}/edit/{{ $row->id }}" class="link">Edit</a></li>
                                                <li><a href="/admin/store/themes/{{ $type }}/delete/{{ $row->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                            @else
                                                <li><a href="/admin/store/themes/{{ $type }}/change/{{ $row->file_name }}" class="link">Edit</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#liquid-table').DataTable({
            "bStateSave": true
        });
    });
</script>
@endsection
