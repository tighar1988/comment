<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Post;
use App\Models\Inventory;
use Mockery;

class PostTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Post::class, new Post);
    }

    /** @test */
    public function it_adds_variants_left_when_supplement_is_missing()
    {
        $product = static::product(new Inventory(['price' => 20, 'quantity' => 5]));

        $post = new Post;
        $post->supplement = null;
        $post->addVariantsLeft($product);

        $newMessage = 'Still available for only $20.00!


';
        $supplement = json_decode($post->supplement);
        $this->assertEquals($supplement->message, $newMessage);
    }

    /** @test */
    public function it_adds_variants_left()
    {
        $message = '
            **CONTEMPORARY--MARKET SPECIAL**
            Best Basic Tee Shirt Tank Dress
            Video: https://youtu.be/SMfH3Skf_bI

            Available in small, medium, large for only $18.99 (final sale)!

            TO ORDER: Comment "Sold, Size, Color (black, charcoal, red, navy, blue, olive), Email Address". One order per comment.

            Go to https://glamourfarms.commentsold.com/account to view/pay your invoice. Invoice must be paid within 24 hours or your item will go to the next person in line.

            Contemporary Fit Measurements
            S: 32" Chest, 37" Length
            M: 34" Chest, 37.5" Length
            L: 36" Chest, 38" Length

            Fabric Details
            67% Polyester
            28% Rayon
            5% Spandex

            Style #sku151
        ';


        $product = static::product(
            new Inventory(['color' => 'black', 'size' => 'small', 'price' => 21.99, 'quantity' => 5]),
            new Inventory(['color' => 'black', 'size' => 'medium', 'price' => 21.99, 'quantity' => 5]),
            new Inventory(['color' => 'black', 'size' => 'large', 'price' => 21.99, 'quantity' => 5]),

            new Inventory(['color' => 'charcoal', 'size' => 'small', 'price' => 21.99, 'quantity' => 5]),
            new Inventory(['color' => 'charcoal', 'size' => 'medium', 'price' => 21.99, 'quantity' => 0]),
            new Inventory(['color' => 'charcoal', 'size' => 'large', 'price' => 21.99, 'quantity' => 5]),

            new Inventory(['color' => 'red', 'size' => 'small', 'price' => 21.99, 'quantity' => 5]),
            new Inventory(['color' => 'red', 'size' => 'medium', 'price' => 21.99, 'quantity' => 5]),
            new Inventory(['color' => 'red', 'size' => 'large', 'price' => 21.99, 'quantity' => 0])
        );

        $post = new Post;
        $post->supplement = ['message' => $message, 'url' => null, 'group' => '1234567890', 'post_comment' => null];
        $post->addVariantsLeft($product);

        $newMessage = '
            **CONTEMPORARY--MARKET SPECIAL**
            Best Basic Tee Shirt Tank Dress
            Video: https://youtu.be/SMfH3Skf_bI

            Still available for only $21.99 (final sale)!

            black: small, medium, large
            charcoal: small, large
            red: small, medium

            TO ORDER: Comment "Sold, Size, Color, Email Address". One order per comment.

            Go to https://glamourfarms.commentsold.com/account to view/pay your invoice. Invoice must be paid within 24 hours or your item will go to the next person in line.

            Contemporary Fit Measurements
            S: 32" Chest, 37" Length
            M: 34" Chest, 37.5" Length
            L: 36" Chest, 38" Length

            Fabric Details
            67% Polyester
            28% Rayon
            5% Spandex

            Style #sku151
        ';

        $supplement = json_decode($post->supplement);
        $this->assertEquals($supplement->message, $newMessage);
    }

    /** @test */
    public function it_adds_variants_left_2()
    {
        $message = '
            Short Sleeve Oil Washed Knit top.

            Available in small, medium, large for only $30.00!

            To order, comment Sold, Size (small, medium, large) and Email Address.

            Style #SKU369
        ';

        $product = static::product(
            new Inventory(['size' => 'small', 'price' => 30, 'quantity' => 5]),
            new Inventory(['size' => 'medium', 'price' => 30, 'quantity' => 5]),
            new Inventory(['size' => 'large', 'price' => 30, 'quantity' => 5])
        );

        $post = new Post;
        $post->supplement = ['message' => $message];
        $post->addVariantsLeft($product);

        $newMessage = '
            Short Sleeve Oil Washed Knit top.

            Still available for only $30.00!

            small, medium, large

            To order, comment Sold, Size and Email Address.

            Style #SKU369
        ';

        $supplement = json_decode($post->supplement);
        $this->assertEquals($supplement->message, $newMessage);
    }

    /** @test */
    public function it_adds_variants_left_3()
    {
        $message = '
            Your Price: $29
            Regular Price $34

            To Purchase Comment: SOLD SIZE (ex SOLD LARGE)

            Available in Small, Medium, Large, XL, 2XL, 3XL

            Measurements (approx.):
            Small: Bust 34", Length 37"
            Medium: Bust 38", Length 37"
            Large: Bust 40", Length 38"



            Material: 100% Polyester



            Size Suggestions:
            Small - 0-4, Medium 6-8, Large 8-10

            Available in Small, Medium, Large for only $32.00!
            To order, comment Sold, Size, Color (White, Navy, Tan) and Email Address.

            Go to https://app.pinkcoconutboutique.com/account to view/pay your invoice. Invoice must be paid within 24 hours or your item will go to the next person in line.

            Style #140232
        ';

        $product = static::product(
            new Inventory(['color' => 'White', 'size' => 'Small', 'price' => 32, 'quantity' => 5]),
            new Inventory(['color' => 'White', 'size' => 'Medium', 'price' => 32, 'quantity' => 5]),
            new Inventory(['color' => 'White', 'size' => 'Large', 'price' => 32, 'quantity' => 5])
        );

        $post = new Post;
        $post->supplement = ['message' => $message];
        $post->addVariantsLeft($product);

        $newMessage = '
            Your Price: $29
            Regular Price $34

            To Purchase Comment: SOLD SIZE (ex SOLD LARGE)

            Still available for only $32.00!

            White: Small, Medium, Large

            Measurements (approx.):
            Small: Bust 34", Length 37"
            Medium: Bust 38", Length 37"
            Large: Bust 40", Length 38"



            Material: 100% Polyester



            Size Suggestions:
            Small - 0-4, Medium 6-8, Large 8-10

            Available in Small, Medium, Large for only $32.00!
            To order, comment Sold, Size, Color and Email Address.

            Go to https://app.pinkcoconutboutique.com/account to view/pay your invoice. Invoice must be paid within 24 hours or your item will go to the next person in line.

            Style #140232
        ';

        $supplement = json_decode($post->supplement);
        $this->assertEquals($supplement->message, $newMessage);
    }

    /** @test */
    public function it_keeps_original_text()
    {
        $message = '
            **MISSY**
            Annalee Floral Tunic
            Video: https://youtu.be/iFvaEJHDT40

            Available for only $23.00!
            blue: small, medium, large

            TO ORDER: Comment "Sold, Size, Color, Email Address". One order per comment.
            Go to https://glamourfarms.commentsold.com/account to view/pay your invoice. Invoice must be paid within 12 hours or your item will go to the next person in line.

            Missy Fit Range
            S: 34"-36" chest
            M: 36"-38" chest
            L: 38"-40" chest

            Exact Measurement
            Small: 36" chest, 28" length
            Add 2" for chest & 0.5" for length per size (approx.)

            Curvy Fit Range
            1XL: 42"-44" chest
            2XL: 44"-46" chest
            3XL: 46"-48" chest

            Exact Measurement
            1XL: 42" chest, 29.5" length
            Add 2" for chest & 0.5" for length per size (approx.)

            Fabric Details
            96% Polyester
            4% Spandex
            Style #sku266
        ';

        $product = static::product(
            new Inventory(['color' => 'blue', 'size' => 'small', 'price' => 31, 'quantity' => 0]),
            new Inventory(['color' => 'blue', 'size' => 'medium', 'price' => 32, 'quantity' => 2]),
            new Inventory(['color' => 'blue', 'size' => 'large', 'price' => 33, 'quantity' => 3]),
            new Inventory(['color' => 'blue', 'size' => '1XL', 'price' => 34, 'quantity' => 0]),
            new Inventory(['color' => 'blue', 'size' => '2XL', 'price' => 35, 'quantity' => 5]),
            new Inventory(['color' => 'blue', 'size' => '3XL', 'price' => 36, 'quantity' => 6])
        );

        $post = new Post;
        $post->supplement = ['message' => $message];
        $post->addVariantsLeft($product);

        $newMessage = '
            **MISSY**
            Annalee Floral Tunic
            Video: https://youtu.be/iFvaEJHDT40

            Still available for only $31.00!

            blue: medium, large, 2XL, 3XL

            TO ORDER: Comment "Sold, Size, Color, Email Address". One order per comment.
            Go to https://glamourfarms.commentsold.com/account to view/pay your invoice. Invoice must be paid within 12 hours or your item will go to the next person in line.

            Missy Fit Range
            S: 34"-36" chest
            M: 36"-38" chest
            L: 38"-40" chest

            Exact Measurement
            Small: 36" chest, 28" length
            Add 2" for chest & 0.5" for length per size (approx.)

            Curvy Fit Range
            1XL: 42"-44" chest
            2XL: 44"-46" chest
            3XL: 46"-48" chest

            Exact Measurement
            1XL: 42" chest, 29.5" length
            Add 2" for chest & 0.5" for length per size (approx.)

            Fabric Details
            96% Polyester
            4% Spandex
            Style #sku266
        ';

        $supplement = json_decode($post->supplement);
        $this->assertEquals($supplement->message, $newMessage);
    }

    /** @test */
    public function check_post_author_is_a_page()
    {
        $post = new Post;
        $post->fb_author_id = '22222222222';

        $this->assertTrue($post->createdBy('22222222222'));
        $this->assertFalse($post->createdBy('33333333333'));
    }
}
