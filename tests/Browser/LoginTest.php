<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\Login;

class LoginTest extends DuskTestCase
{
    /**
     * @group login
     */
    public function testUserLogin()
    {
        $this->browse(function ($browser) {
            $browser->visit(new Login)
                    ->signIn()
                    ->assertPathIs('/admin');
        });
    }
}
