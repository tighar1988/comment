<div class="row">
    <div class="col-sm-6">
        <div class="dataTables_length">
            <label>Show
                <select class="form-control input-sm">
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select> entries
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="dataTables_filter">
            <form class="form-inline" method="GET" action="{{ form_action() }}">
                <label>Search:
                    <div class="input-group">
                        <input type="search" class="form-control input-sm" name="q" value="{{ request('q') }}" placeholder="{{ $placeholder ?? '' }}">
                        {{--
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            </span>
                        --}}
                    </div>
                </label>
            </form>
        </div>
    </div>
</div>
