@component('mail::message', ['shopName' => $shopName, 'shopUrl' => $shopUrl])

<div style="text-align: right">
    <strong>Order Confirmation</strong><br/>
    Order #{{ $orderId }}
</div>

Hello {{ $shipName }}, <br/>

@if ($localPickup)
Thanks for your order! We are putting together your package now and will update you ASAP!
@else
Thanks for your order. We'll let you know once your item(s) have shipped!
@endif

### Order Details
@component('mail::table')
    | Product       | Price         |
    |:------------- | -------------:|
    @foreach ($orderProducts as $product)
        | {{ $product['name'] }} | ${{ amount($product['price']) }} |
    @endforeach
    | | Subtotal: ${{ amount($subtotal) }} |
    | | Shipping: ${{ amount($shipCharged) }} |
    | | Tax: ${{ amount($taxTotal) }} |
    @if ($couponDiscount)
        | | Coupon Discount: -${{ amount($couponDiscount) }} |
    @endif
    @if ($applyBalance)
        | | Account Balance: -${{ amount($applyBalance) }} |
    @endif
    | | Total: <strong>${{ amount($total) }}</strong> |
@endcomponent

@if (! $localPickup)
Your order will be sent to: <br/>
{{ $shipName }} <br/>
{{ $streetAddress }} <br/>
@if ($apartment)
    {{ $apartment }} <br/>
@endif
{{ $city }}, {{ $state }} {{ $zip }} <br/>
U.S. <br/>
@endif

We hope to see you again soon.<br/>
{{ $shopName }}<br/><br/>
@endcomponent
