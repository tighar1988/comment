<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class VariableShippingCostFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'variable_shipping_cost' => 'nullable|numeric|min:0|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'remind.numeric'  => 'The variable shipping cost must be numeric.',
            'remind.min'      => 'The variable shipping cost must be at least 0.',
        ];
    }
}
