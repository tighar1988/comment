<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class BlogPost extends Model
{
    /**
     * Visible.
     *
     * @var integer
     */
    const VISIBILITY_VISIBLE = 1;

    /**
     * Hidden.
     *
     * @var integer
     */
    const VISIBILITY_HIDDEN = 2;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blog_posts';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'blog_id',
        'title',
        'content',
        'excerpt',
        'visibility',
        'featured_image',
        'author',
        'seo_page_title',
        'seo_meta_description',
        'handle',
    ];

    public function imageUrl()
    {
        if (! is_null($this->featured_image)) {
            return Storage::url(shop_id() . '/blog/' . $this->featured_image);
        }

        return null;
    }

    public function blog()
    {
        return $this->belongsTo('App\Models\Blog', 'blog_id', 'id');
    }
}
