<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Order;

class OrderTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Order::class, new Order);
    }

    /** @test */
    public function is_local_pickup()
    {
        $order = new Order;
        $order->local_pickup = 1;

        $this->assertTrue($order->isLocalPickup());
    }

    /** @test */
    public function is_not_local_pickup()
    {
        $order = new Order;
        $order->local_pickup = 0;

        $this->assertFalse($order->isLocalPickup());
    }

    /** @test */
    public function is_picked_up()
    {
        $order = new Order;
        $order->setDetail('picked-up', true);

        $this->assertTrue($order->hasDetail('picked-up'));
    }
}
