#!/bin/bash

docker service create \
--detach \
--env API_KEY=174787655d9e550010b0ad06c27a50cd \
--network cs_sql \
--with-registry-auth \
--mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
--mount type=bind,source=/proc/,target=/host/proc/,ro=true \
--mount type=bind,source=/sys/fs/cgroup/,target=/host/sys/fs/cgroup,ro=true \
--name dd-sql \
--mode global \
--endpoint-mode dnsrr \
--constraint 'node.labels.type == sql' \
brandonkruse/commentsold:dd-agent-sql-3

