<?php

namespace App\OnlineStore\Objects\GlobalObjects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\Objects\Collection;

class Collections extends Drop
{
    protected $cache = [];

    protected $collection;

    public function __construct()
    {
        $this->collection = app('App\Models\Collection');
    }

    /**
     * The collections object returns all the collections in your store.
     *
     * {% for product in collections.frontpage.products %}
     *     {{ product.title }}
     * {% endfor %}
     */
    public function handle_key($key)
    {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        $collection = $this->collection->handle($key)->first();

        $this->cache[$key] = is_null($collection) ? null : new Collection($collection);
        return $this->cache[$key];
    }
}
