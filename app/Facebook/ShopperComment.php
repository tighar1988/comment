<?php

namespace App\Facebook;

use App\Models\Helpers\SoldMatcher;

class ShopperComment
{
    use SoldMatcher;

    public $comment;

    public $createdTime;

    public $fromName;

    public $fromId;

    public $message;

    public $combinedId;

    public $id;

    public $parentId;

    public $postId;

    public function __construct($comment)
    {
        $this->comment = $comment;
    }

    public static function create($comment)
    {
        $shopperComment = new static($comment);

        $parentComment = $comment->getField('parent');
        if ($parentComment) {
            $shopperComment->parentId = $parentComment->getField('id');
        }

        $shopperComment->id          = $comment->getField('id');
        $shopperComment->createdTime = $comment->getField('created_time')->getTimestamp();
        $shopperComment->message     = $comment->getField('message');

        $from = $comment->getField('from');
        if (is_null($from)) {
            return null;
        }

        $shopperComment->fromName    = $from->getField('name');
        $shopperComment->fromId      = $from->getField('id');

        return $shopperComment;
    }

    public static function createFromWebhook($comment)
    {
        $shopperComment = new static($comment);

        if (isset($comment['parent_id']) && ! empty($comment['parent_id'])) {
            $shopperComment->parentId = $comment['parent_id'];
        }

        $shopperComment->combinedId  = $comment['comment_id'];
        $shopperComment->id          = explode('_', $comment['comment_id'])[1];
        $shopperComment->createdTime = $comment['created_time'];
        $shopperComment->message     = $comment['message'];
        $shopperComment->fromName    = $comment['sender_name'];
        $shopperComment->fromId      = $comment['sender_id'];
        $shopperComment->postId      = $comment['post_id'];

        return $shopperComment;
    }

    /**
     * Check if the comment has the full original id.
     *
     * @return boolean
     */
    public function hasCombinedId()
    {
        return ! is_null($this->combinedId);
    }

    /**
     * Is this a comment under the main post, or a reply?
     *
     * @return boolean
     */
    public function isReplyComment()
    {
        if (is_null($this->parentId)) {
            return false;
        }

        // for facebook webhook comments, check the parent id is not the post
        if (! empty($this->postId)) {
            $postId = explode('_', $this->postId);
            $parentId = explode('_', $this->parentId);
            if (isset($postId[1], $parentId[1]) && $postId[1] == $parentId[1]) {
                return false;
            }
        }

        return true;
    }
}
