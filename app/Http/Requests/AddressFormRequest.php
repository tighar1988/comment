<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\TaxRate;

class AddressFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'street_address' => 'required|max:255',
            'city'           => 'required|max:255',
            'country_code'   => 'nullable|in:US,CA',
            'state'          => 'required|max:100',
            'zip'            => 'required|max:100',
            'apartment'      => 'nullable|max:100',
            'phone_number'   => 'nullable|max:100',
        ];

        if ($this->country_code == 'US' || empty($this->country_code)) {
            $rules['state'] = 'required|max:10|in:AL,AK,AS,AZ,AR,AA,AE,AP,CA,CO,CT,DE,DC,FL,GA,GU,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,MP,NS,OH,OK,OR,PW,PA,PR,RI,SC,SD,TN,TX,UT,VT,VI,VA,WA,WV,WI,WY';

        } else {
            $rules['state'] = 'required|max:100';
        }

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        /*
        $validator->after(function ($validator) {
            if (! TaxRate::master()->getTaxRate($this->zip)) {
                $validator->errors()->add('zip', 'The zip code is invalid!');
            }
        });
         */
    }
}
