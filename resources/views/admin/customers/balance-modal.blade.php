<div class="modal fade" id="balance-modal-add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Balance<span class="text-muted"><small class="for-user"></small></span></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Balance:</label>
                        <input type="text" name="balance" data-customerId="" autocomplete="off" class="form-control balance">
                        <span class="help-block" style="display: none"><strong></strong></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Note:</label>
                        <textarea name="note" autocomplete="off" class="form-control note"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" data-loading-text="Adding Balance..." class="btn btn-success">Add Balance</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="balance-modal-subtract" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Subtract Balance<span class="text-muted"><small class="for-user"></small></span></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Balance:</label>
                        <input type="text" name="balance" data-customerId="" autocomplete="off" class="form-control balance">
                        <span class="help-block" style="display: none"><strong></strong></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Note:</label>
                        <textarea name="note" autocomplete="off" class="form-control note"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" data-loading-text="Subtracting Balance..." class="btn btn-danger">Subtract Balance</button>
                </div>
            </form>
        </div>
    </div>
</div>
