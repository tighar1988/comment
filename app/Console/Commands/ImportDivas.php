<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\BalanceMovement;
use App\Models\Card;
use App\Models\Cart;
use App\Models\Comment;
use App\Models\Coupon;
use App\Models\Helpers;
use App\Models\Image;
use App\Models\InstagramComment;
use App\Models\InstagramPost;
use App\Models\Inventory;
use App\Models\InventoryLog;
use App\Models\Master;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Page;
use App\Models\PayPalComissionCharge;
use App\Models\Post;
use App\Models\Product;
use App\Models\ScanLog;
use App\Models\ShopLog;
use App\Models\ShopSettings;
use App\Models\ShopUser;
use App\Models\StripeCharges;
use App\Models\Tag;
use App\Models\TagGroup;
use App\Models\TaxRate;
use App\Models\User;
use App\Models\Waitlist;
use DB;

class ImportDivas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'divas:import {--users}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import from divas all the necessary things for the transition.';

    protected $divas;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting Job..');

        $this->connectDivasDB();

        if ($this->option('users')) {
            $this->info('Importing users..');
            $this->import_shop_users();
        }

        $this->info('Job Complete!');
    }

    public function connectDivasDB()
    {
        set_shop_database('divas');

        config(['database.connections.pgsql.host' => env('DIVAS_IMPORT_HOST')]);
        config(['database.connections.pgsql.port' => '5432']);
        config(['database.connections.pgsql.database' => env('DIVAS_IMPORT_DATABASE')]);
        config(['database.connections.pgsql.username' => env('DIVAS_IMPORT_USERNAME')]);
        config(['database.connections.pgsql.password' => env('DIVAS_IMPORT_PASSWORD')]);

        $this->divas = DB::connection('pgsql');
    }

    public function importTable($table, $orderBy, $importRow)
    {
        $nrRows = $this->divas->table($table)->count();
        $bar = $this->output->createProgressBar($nrRows);

        // retrieve a small chunk of the results at a time
        $this->divas->table($table)->orderBy($orderBy)->chunk(100, function ($rows) use ($importRow, $bar) {
            foreach ($rows as $row) {
                $importRow($row);
                $bar->advance();
            }
        });

        $bar->finish();
        $this->info(PHP_EOL . "Importing {$table} done.");
    }

    public function import_shop_users()
    {
        $this->importTable('customers', 'cid', function($row) {
            $user = ShopUser::where('facebook_id', '=', $row->fbid)->first() ?? new ShopUser;
            $user->facebook_id    = $row->fbid;
            $user->name           = $row->name;
            $user->email          = $row->email;
            $user->street_address = $row->street_address;
            $user->apartment      = $row->apt;
            $user->city           = $row->city;
            $user->state          = $row->state;
            $user->zip            = $row->zip;
            $user->balance        = $row->balance;
            $user->messenger_id   = $row->msgr_id;
            $user->stripe_id      = $row->stripe_id;
            $user->save();
        });
    }

    public function import_coupons()
    {
        $this->importTable('coupons', function($row) {
            $coupon = Coupon::where('id', '=', $row['id'])->first() ?? new Coupon;
            $coupon->id          = null; // int(10) UNSIGNED
            $coupon->code        = null; // varchar(30)
            $coupon->expires_at  = null; // int(11)
            $coupon->max_usage   = null; // int(11)
            $coupon->coupon_type = null; // enum('S','F','P')
            $coupon->amount      = null; // double
            $coupon->use_count   = null; // int(11) NOT NULL DEFAULT '0'
            $coupon->created_at  = null; // timestamp NULL
            $coupon->updated_at  = null; // timestamp NULL
            $coupon->options     = null; // text
            $coupon->save();
        });
    }

    public function import_balance_movement()
    {
        $movement = BalanceMovement::where('id', '=', $table['id'])->first() ?? new BalanceMovement;
        $movement->id          = null; // int(10) UNSIGNED
        $movement->customer_id = null; // int(10) UNSIGNED
        $movement->amount      = null; // double
        $movement->memo        = null; // text
        $movement->created_at  = null; // timestamp NULL
        $movement->updated_at  = null; // timestamp NULL
        $movement->save();
    }

    public function import_cards()
    {
        $card = Card::where('id', '=', $table['id'])->first() ?? new Card;
        $card->id         = null; // int(10) UNSIGNED
        $card->user_id    = null; // int(10) UNSIGNED
        $card->card_id    = null; // varchar(255)
        $card->brand      = null; // varchar(255) NULL
        $card->last_four  = null; // varchar(255) NULL
        $card->exp_month  = null; // varchar(255) NULL
        $card->exp_year   = null; // varchar(255) NULL
        $card->default    = null; // tinyint(1)
        $card->created_at = null; // timestamp NULL
        $card->updated_at = null; // timestamp NULL
        $card->save();
    }

    public function import_comments()
    {
        $comment = Comment::where('id', '=', $table['id'])->first() ?? new Comment;
        $comment->id        = null; // int(10) UNSIGNED
        $comment->post_id   = null; // int(10) UNSIGNED
        $comment->fb_id     = null; // varchar(255)
        $comment->parent_id = null; // varchar(255) NULL
        $comment->stamped   = null; // int(11)
        $comment->user_id   = null; // varchar(255)
        $comment->content   = null; // text
        $comment->is_order  = null; // tinyint(1) NOT NULL DEFAULT '0'
        $comment->status    = null; // int(11) DEFAULT NULL
        $comment->save();
    }

    public function import_images()
    {
        $image = Image::where('id', '=', $table['id'])->first() ?? new Image;
        $image->id               = null; // int(10) UNSIGNED
        $image->product_id       = null; // int(10) UNSIGNED
        $image->filename         = null; // varchar(255)
        $image->is_main          = null; // tinyint(1)
        $image->created_at       = null; // timestamp NULL
        $image->updated_at       = null; // timestamp NULL
        $image->shopify_image_id = null; // varchar(255) NULL
        $image->save();
    }

    public function import_inventory()
    {
        $inventory = Inventory::where('id', '=', $table['id'])->first() ?? new Inventory;
        $inventory->id                     = null; // int(10) UNSIGNED
        $inventory->product_id             = null; // int(10) UNSIGNED
        $inventory->quantity               = null; // int(11) DEFAULT '0'
        $inventory->color                  = null; // text ,
        $inventory->size                   = null; // text ,
        $inventory->weight                 = null; // int(11) DEFAULT '0'
        $inventory->price                  = null; // double DEFAULT '0'
        $inventory->cost                   = null; // double DEFAULT '0'
        $inventory->location               = null; // text ,
        $inventory->shopify_inventory_id   = null; // varchar(255) NULL
        $inventory->shopify_barcode        = null; // varchar(255) NULL
        $inventory->shopify_webhook_status = null; // varchar(255) NULL
        $inventory->save();
    }

    public function import_inventory_log()
    {
        $log = Inventory::where('id', '=', $table['id'])->first() ?? new Inventory;
        $log->id           = null; // int(10) UNSIGNED
        $log->inventory_id = null; // int(10) UNSIGNED
        $log->from_inv     = null; // int(11)
        $log->to_inv       = null; // int(11)
        $log->movement     = null; // text
        $log->created_at   = null; // timestamp NULL
        $log->updated_at   = null; // timestamp NULL
        $log->save();
    }

    public function import_orders()
    {
        $order = Order::where('id', '=', $table['id'])->first() ?? new Order;
        $order->id               = null; // int(10) UNSIGNED
        $order->customer_id      = null; // int(10) UNSIGNED
        $order->street_address   = null; // text
        $order->apartment        = null; // text
        $order->city             = null; // text
        $order->state            = null; // text
        $order->zip              = null; // text
        $order->email            = null; // text
        $order->order_status     = null; // int(11) DEFAULT NULL
        $order->payment_ref      = null; // text
        $order->transaction_id   = null; // varchar(255) NULL
        $order->payment_amt      = null; // double DEFAULT NULL
        $order->ship_charged     = null; // double DEFAULT NULL
        $order->ship_cost        = null; // double DEFAULT NULL
        $order->cs_ship_charged  = null; // double DEFAULT NULL
        $order->subtotal         = null; // double DEFAULT NULL
        $order->total            = null; // double DEFAULT NULL
        $order->fbid             = null; // text
        $order->ship_name        = null; // text
        $order->payment_date     = null; // int(11) DEFAULT NULL
        $order->shipped_date     = null; // int(11) DEFAULT NULL
        $order->tracking_number  = null; // text
        $order->state_tax        = null; // double DEFAULT NULL
        $order->county_tax       = null; // double DEFAULT NULL
        $order->municipal_tax    = null; // double DEFAULT NULL
        $order->tax_total        = null; // double DEFAULT NULL
        $order->coupon           = null; // text
        $order->coupon_discount  = null; // double DEFAULT NULL
        $order->calculated_zip   = null; // text
        $order->apply_balance    = null; // double DEFAULT NULL
        $order->local_pickup     = null; // tinyint(1) NOT NULL DEFAULT '0'
        $order->picker           = null; // text
        $order->packer           = null; // text
        $order->created_at       = null; // timestamp NULL
        $order->updated_at       = null; // timestamp NULL
        $order->label_url        = null; // text
        $order->details          = null; // text
        $order->shopify_order_id = null; // varchar(255) NULL
        $order->save();
    }

    public function import_orders_products()
    {
        $op = OrderProduct::where('id', '=', $table['id'])->first() ?? new OrderProduct;
        $op->id                  = null; // int(10) UNSIGNED
        $op->order_id            = null; // int(10) UNSIGNED
        $op->product_id          = null; // int(10) UNSIGNED DEFAULT NULL
        $op->inventory_id        = null; // int(10) UNSIGNED DEFAULT NULL
        $op->price               = null; // double DEFAULT NULL
        $op->cost                = null; // double DEFAULT NULL
        $op->prod_name           = null; // text
        $op->color               = null; // text
        $op->size                = null; // text
        $op->comment_id          = null; // int(10) UNSIGNED DEFAULT NULL
        $op->returned_date       = null; // int(11) DEFAULT NULL
        $op->refund_issued       = null; // double DEFAULT NULL
        $op->order_source        = null; // int(11) DEFAULT NULL
        $op->created_at          = null; // timestamp NULL
        $op->updated_at          = null; // timestamp NULL
        $op->product_style       = null; // text
        $op->product_brand       = null; // text
        $op->product_brand_style = null; // text
        $op->product_filename    = null; // varchar(255) NULL
        $op->product_description = null; // text
        $op->weight              = null; // int(11) DEFAULT '0'
        $op->shopify_variant_id  = null; // varchar(255) NULL
        $op->save();
    }

    public function import_posts()
    {
        $post = Post::where('id', '=', $table['id'])->first() ?? new Post;
        $post->id             = null; // int(10) UNSIGNED
        $post->prod_id        = null; // int(10) UNSIGNED
        $post->fb_id          = null; // text
        $post->created_time   = null; // int(11)
        $post->scan_time      = null; // int(11)
        $post->image          = null; // text
        $post->scheduled_time = null; // int(11) DEFAULT NULL
        $post->supplement     = null; // text
        $post->created_at     = null; // timestamp NULL
        $post->updated_at     = null; // timestamp NULL
        $post->fb_photo_id    = null; // varchar(255) NULL
        $post->fb_author_id   = null; // varchar(255) NULL
        $post->fb_post_id     = null; // varchar(255) NULL
        $post->save();
    }

    public function import_products()
    {
        $product = Product::where('id', '=', $table['id'])->first() ?? new Product;
        $product->id                 = null; // int(10) UNSIGNED
        $product->product_name       = null; // varchar(255)
        $product->description        = null; // text
        $product->style              = null; // text
        $product->brand              = null; // text
        $product->brand_style        = null; // text
        $product->created_at         = null; // timestamp NULL
        $product->updated_at         = null; // timestamp NULL
        $product->store_description  = null; // text
        $product->url                = null; // varchar(255) NULL
        $product->shopify_product_id = null; // varchar(255) NULL
        $product->charge_taxes       = null; // tinyint(1) DEFAULT NULL
        $product->save();
    }

    public function import_scan_log()
    {
        $scanlog = ScanLog::where('id', '=', $table['id'])->first() ?? new ScanLog;
        $scanlog->id             = null; // int(10) UNSIGNED
        $scanlog->barcode        = null; // varchar(255) NULL
        $scanlog->item_id        = null; // int(11) DEFAULT NULL
        $scanlog->item_type      = null; // varchar(255) NULL
        $scanlog->scan_date      = null; // int(11) DEFAULT NULL
        $scanlog->picker_user_id = null; // int(11) DEFAULT NULL
        $scanlog->result         = null; // text
        $scanlog->created_at     = null; // timestamp NULL
        $scanlog->updated_at     = null; // timestamp NULL
        $scanlog->save();
    }

    public function import_settings()
    {
        // this needs a custom import, not row by row import
        // $model = ShopSettings::where('id', '=', $table['id'])->first() ?? new ShopSettings;
        // $model->id         = null; // int(10) UNSIGNED
        // $model->key        = null; // varchar(255)
        // $model->value      = null; // text
        // $model->created_at = null; // timestamp NULL
        // $model->updated_at = null; // timestamp NULL
        // $model->save();
    }

    public function import_shopping_cart()
    {
        $cart = Cart::where('id', '=', $table['id'])->first() ?? new Cart;
        $cart->id           = null; // int(10) UNSIGNED
        $cart->user_id      = null; // int(10) UNSIGNED
        $cart->product_id   = null; // int(10) UNSIGNED DEFAULT NULL
        $cart->inventory_id = null; // int(10) UNSIGNED
        $cart->order_source = null; // int(11) NOT NULL DEFAULT '0'
        $cart->comment_id   = null; // int(10) UNSIGNED DEFAULT NULL
        $cart->reminded_at  = null; // timestamp NULL
        $cart->created_at   = null; // timestamp NULL
        $cart->updated_at   = null; // timestamp NULL
        $cart->save();
    }

    public function import_waitlist()
    {
        $waitlist = Waitlist::where('id', '=', $table['id'])->first() ?? new Waitlist;
        $waitlist->id           = null; // int(10) UNSIGNED
        $waitlist->user_id      = null; // int(10) UNSIGNED
        $waitlist->product_id   = null; // int(10) UNSIGNED DEFAULT NULL
        $waitlist->inventory_id = null; // int(10) UNSIGNED
        $waitlist->order_source = null; // int(11) NOT NULL DEFAULT '0'
        $waitlist->comment_id   = null; // int(10) UNSIGNED DEFAULT NULL
        $waitlist->created_at   = null; // timestamp NULL
        $waitlist->updated_at   = null; // timestamp NULL
        $waitlist->save();
    }

    public function import_collections()
    {
        // divas has no collections table
    }

    public function import_collection_product()
    {
        // divas has no collections product table
    }

    public function import_email_list()
    {
        // divas has no emai list table
    }

    public function import_failed_jobs()
    {
        // no need to import failed jobs
    }

    public function import_instagram_comments()
    {
        // divas has no instagram comments
    }

    public function import_instagram_posts()
    {
        // divas has no instagram posts
    }

    public function import_tagging_tagged()
    {
        // divas has not tags
    }

    public function import_tagging_tags()
    {
        // divas has not tags
    }

    public function import_tagging_tag_groups()
    {
        // divas has not tags
    }

    public function import_jobs()
    {
        // no need to import jobs
    }

    public function import_logs()
    {
        // no need to import logs
    }

    public function import_migrations()
    {
        // no need to import migrations
    }

    public function import_pages()
    {
        // divas has no static pages
    }

    public function import_paypal_comission_charges()
    {
        // divas has no paypal comission charges
    }

    public function import_sessions()
    {
        // no need to import sessions
    }

    public function import_stripe_charges()
    {
        // divas has no stripe charges
    }
}
