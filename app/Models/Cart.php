<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\InventoryLog;
use DB;
use App\Models\OrderProduct;

class Cart extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shopping_cart';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'reminded_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'product_id',
        'inventory_id',
        'order_source',
        'comment_id',
    ];

    public static function removeItem($itemId, $message = null)
    {
        $item = static::with('variant')->find($itemId);

        if ($item) {
            $item->variant->incrementQuantity($message ?? 'Customer removed item from shopping cart. Adding back to inventory.');
            $item->delete();
        }
    }

    public static function addItem($customer, $commentId, $variant, $message = null)
    {
        // todo: add db transactions here
        $variant->decrementQuantity($message ?? "Adding item to shopping cart for customer #{$customer->id}");

        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => null,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_FACEBOOK_COMMENT,
            'comment_id'   => $commentId,
        ]);
    }

    public static function adminAddItem($customer, $variant, $message = null)
    {
        // todo: add db transactions here
        $variant->decrementQuantity($message ?? "Admin adding item to shopping cart for customer #{$customer->id}");

        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => null,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_ADMIN,
            'comment_id'   => null,
        ]);
    }

    public static function instagramAddItem($customer, $commentId, $variant, $message = null)
    {
        // todo: add db transactions here
        $variant->decrementQuantity($message ?? "[instagram] Adding item to shopping cart for customer #{$customer->id}");

        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => null,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_INSTAGRAM_COMMENT,
            'comment_id'   => $commentId,
        ]);
    }

    public static function storeAddItem($customer, $variant, $message = null)
    {
        // todo: add db transactions here
        $variant->decrementQuantity($message ?? "[store] Customer #{$customer->id} adding item #{$variant->id} to cart");

        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => null,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_STORE,
            'comment_id'   => null,
        ]);
    }

    public function scopeCartQuantity($query, $variantId)
    {
        return $query->where('inventory_id', $variantId)->count();
    }

    public function scopeForCustomer($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function scopeInventoryId($query, $inventoryId)
    {
        return $query->where('inventory_id', $inventoryId);
    }

    public function variant()
    {
        return $this->hasOne('App\Models\Inventory', 'id', 'inventory_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Models\ShopUser', 'id', 'user_id');
    }

    public static function potentialRevenue()
    {
        return static::join('inventory', 'shopping_cart.inventory_id', '=', 'inventory.id')
            ->sum('inventory.price');
    }

    public static function report($sku = null)
    {
        $query = static::select(DB::raw('shopping_cart.inventory_id,
                inventory.product_id, inventory.quantity, inventory.price, inventory.cost,
                inventory.size, inventory.color,
                products.product_name, products.style as sku, products.brand, products.brand_style,
                count(*) as count'))
            ->join('inventory', 'shopping_cart.inventory_id', '=', 'inventory.id')
            ->join('products', 'inventory.product_id', '=', 'products.id')
            ->groupBy('shopping_cart.inventory_id')
            ->orderBy('inventory_id', 'DESC');


        if (! is_null($sku)) {
            $query->where('products.style', 'like', "%{$sku}%");
        }

        return $query;
    }

    public static function mobileAppAddItem($customer, $variant)
    {
        $variant->decrementQuantity("[mobile-app] Customer #{$customer->id} adding item #{$variant->id} to cart");

        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => $variant->product_id,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_MOBILE_APP,
            'comment_id'   => null,
        ]);
    }
}
