<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\InventoryLog;

class InventoryLogTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(InventoryLog::class, new InventoryLog);
    }
}
