<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BlogPostFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                => 'required|max:255',
            'content'              => 'required|min:10',
            'excerpt'              => 'nullable|min:5',
            'visibility'           => 'required|in:1,2',
            'author'               => 'required|max:255',
            'blog_id'              => 'required|numeric',
            'seo_page_title'       => 'required|max:70',
            'seo_meta_description' => 'required|max:160',
            'handle'               => 'required|alpha_dash|max:255',
        ];
    }
}
