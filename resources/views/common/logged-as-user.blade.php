@if (session()->has('logged-as-user'))
    <div style="position:fixed;bottom:10px;left:10px;background:#cb4629;color:#fff;width:210px;height:100px;z-index:99999;text-align:center;padding: 5px 10px;overflow:hidden;text-overflow:ellipsis;">

        <div style="text-align: left; margin-bottom: 3px;">Logged as:</div>
        <strong>{{ Auth::user()->name }}</strong>
        {{ Auth::user()->email }}
        <div style="text-align: right;"><a style="color:#fff;text-decoration:underline;" href="/app-status">Change</a></div>

    </div>
@endif
