<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $resetUrl;

    public $shopFromEmail;

    public $shopEmail;

    public $shopName;

    public function __construct($resetUrl, $shopFromEmail, $shopEmail, $shopName)
    {
        $this->resetUrl      = $resetUrl;
        $this->shopFromEmail = $shopFromEmail;
        $this->shopEmail     = $shopEmail;
        $this->shopName      = $shopName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        config(['app.name' => $this->shopName]);

        return (new MailMessage)
            ->line('You are receiving this email because we received a password reset request for your account.')
            ->action('Reset Password', $this->resetUrl)
            ->line('If you did not request a password reset, no further action is required.')
            ->replyTo($this->shopEmail, $this->shopName)
            ->from($this->shopFromEmail, $this->shopName);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
