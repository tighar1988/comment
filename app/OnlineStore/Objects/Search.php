<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;
use App\OnlineStore\Vendor\Liquid\Tag\TagPaginate;

class Search extends Drop
{
    protected $resultsCount = null;

    /**
     * Returns true if an HTML form with the attribute action="/search" was submitted successfully.
     * This allows you to show content based on whether a search was performed or not.
     *
     * {% if search.performed %}
     *     <!-- Show search results -->
     * {% endif %}
     */
    public function performed()
    {
        if (CurrentView::$isSearch) {
            return true;
        }

        return false;
    }

    /**
     * Returns an array of matching search result items. The items in the array can be a(n):
     *
     * article
     * page
     * product
     *
     * You can access the attributes of individual search results by looping search.results.
     * {% for item in search.results %}
     *   <h3>{{ item.title | link_to: item.url }}</h3>
     * {% endfor %}
     */
    public function results()
    {
        // todo: add support for article and pages search
        $limit = 20;
        if (is_numeric(CurrentView::$numberItems)) {
            $limit = CurrentView::$numberItems;
        }

        $offset = 0;
        if (is_numeric(CurrentView::$currentOffset)) {
            $offset = CurrentView::$currentOffset;
        }

        $items = [];
        $q = CurrentView::$searchQuery;

        // products search
        $products = app('App\Models\Product')
            ->whereHas('image')
            ->where(function ($query) {
                $query->whereNull('publish_product')
                    ->orWhere('publish_product', '=', '1');
            })
            ->where(function ($query) use ($q) {
                $query->where('style', 'like', "%{$q}%")
                    ->orWhere('product_name', 'like', "%{$q}%")
                    ->orWhere('store_description', 'like', "%{$q}%")
                    ->orWhere('brand_style', 'like', "%{$q}%")
                    ->orWhere('brand', 'like', "%{$q}%");
            })
            ->offset($offset)
            ->limit($limit)
            ->get();

        foreach ($products as $product) {
            $items[] = new Product($product);
        }

        TagPaginate::$dbCollectionSize = $this->results_count();
        CurrentView::$totalItems = TagPaginate::$dbCollectionSize;

        return $items;
    }

    /**
     * Returns the number of results found.
     */
    public function results_count()
    {
        if (! is_null($this->resultsCount)) {
            return $this->resultsCount;
        }

        $q = CurrentView::$searchQuery;

        $this->resultsCount = app('App\Models\Product')
            ->whereHas('image')
            ->where(function ($query) {
                $query->whereNull('publish_product')
                    ->orWhere('publish_product', '=', '1');
            })
            ->where(function ($query) use ($q) {
                $query->where('style', 'like', "%{$q}%")
                    ->orWhere('product_name', 'like', "%{$q}%")
                    ->orWhere('store_description', 'like', "%{$q}%")
                    ->orWhere('brand_style', 'like', "%{$q}%")
                    ->orWhere('brand', 'like', "%{$q}%");
            })
            ->count();

        return $this->resultsCount;
    }

    /**
     * Returns the string that was entered in the search input box. Use the highlight filter to
     * apply a different style to any instances in the search results that match up with search.terms.
     *
     * Input
     * {{ item.content | highlight: search.terms }}
     *
     * Output
     * <!-- If the search term was "Yellow" -->
     * <strong class="highlight">Yellow</strong> shirts are the best!
     */
    public function terms()
    {
        return CurrentView::$searchQuery;
    }
}
