<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><hr/><br/>

    This is an invoice for your Subscription Plan.

    Total amount charged is ${{ amount($amount) }}
    <br/><br/>

    Nothing is due at this time.
</div>
