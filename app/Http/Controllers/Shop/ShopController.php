<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\OnlineStore\LiquidTemplate;
use App\OnlineStore\LiquidFileSystem;
use App\OnlineStore\Objects\Shop;

class ShopController extends Controller
{
    public function index()
    {
        if (store_visible()) {
            return response(LiquidTemplate::template('index'), 200)
               ->withHeaders(cache_header('index store'));

        } else {
            $shopName = shop_name();
            return view('shop.welcome', compact('shopName'));
        }
    }
}
