<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="{{ cdn('/favicon.ico') }}" type="image/x-icon" />
<meta name="description" content="CommentSold - Comment Selling for Facebook and Instagram. Say goodbye to manually invoicing forever!">
<meta name="author" content="CommentSold, LLC">
<title>:: CommentSold ::</title>
<link rel="icon" type="image/png" href="{{ cdn('/favicon-32x32.png') }}" sizes="32x32" />
<link rel="icon" type="image/png" href="{{ cdn('/favicon-16x16.png') }}" sizes="16x16" />
<link rel="stylesheet" href="{{ cdn('/css/bootstrap.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ cdn('/css/font-awesome.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ cdn('css/Poppins.css') }}"  type="text/css">
<!-- Main CSS -->
<link rel="stylesheet" href="{{ cdn('/css/main.css?v=2') }}">
<link rel="stylesheet" href="{{ cdn('/css/responsive.css?v=2') }}">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
<![endif]-->
@include('common.admin-intercom')
