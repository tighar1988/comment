<?php

namespace App\Jobs;

use Exception;
use App\Instagram\Instagram;
use App\Models\InstagramPost;
use App\Models\InstagramComment;
use App\Models\ShopUser;
use App\Models\Waitlist;
use App\Models\Cart;
use App\Instagram\InstagramShopperComment;
use App\Mails\ShopMailer;
use App\Exceptions\ShopifyWebhookLockedException;

class InstagramHarvest
{
    /**
     * Instagram harvest job.
     *
     * @param string $shop
     * @return void
     */
    public function handle($shop)
    {
        if (! shop_setting('instagram.token')) {
            log_info("[instagram] Instagram not connected. Exiting..");
            return;
        }

        $posts = InstagramPost::all();
        if ($posts->isEmpty()) {
            log_info("[instagram] No posts are linked to products. Exiting..");
            return;
        }

        $mailer = new ShopMailer;
        $instagram = new Instagram;
        $nrPosts = count($posts);
        $index = 0;

        foreach ($posts as $post) {
            $index++;

            try {
                // get all the instagram comments for this post
                $comments = $instagram->comments($post->instagram_id);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $deletedPostError = '{"code": 400, "error_type": "APINotFoundError", "error_message": "invalid media id"}';
                if (str_contains($e->getMessage(), $deletedPostError)) {
                    log_info("[instagram] Post was deleted from Instagram. Deleting post from the db {$post->instagram_id}");
                    $post->delete();
                    continue;
                }
            }

            if (empty($comments)) {
                log_info("[instagram] [{$index}/{$nrPosts}] No comments for instagram post {$post->instagram_id}");
                continue;
            }

            // filter the comments to keep only one we haven't harvested yet
            $existingIds = InstagramComment::existingCommentsIds($post->id);
            $comments = collect($comments)->filter(function ($comment, $key) use ($existingIds) {
                return ! in_array($comment['id'], $existingIds);
            });

            if ($comments->isEmpty()) {
                log_info("[instagram] [{$index}/{$nrPosts}]  Already seen the latest comment on post {$post->instagram_id}");
                continue;
            }

            log_info("[instagram] [{$index}/{$nrPosts}] Comments to parse: ".count($comments)." on post {$post->instagram_id}");

            $product = $post->product;

            foreach ($comments as $instagramComment) {
                $comment = new InstagramShopperComment($instagramComment);

                // don't store comments from unregisterd customers; when they register we can parse the comment again
                $customer = $this->getCommenter($comment);
                if (is_null($customer)) {
                    continue;
                }

                // don't store comments from users with no email; when they update the email parse the comment again
                if (! $customer->email) {
                    continue;
                }

                // store the comment in our database
                $commentId = InstagramComment::store($post->id, $comment)->id;

                if (! $comment->commentedSold()) {
                    continue;
                }

                $variant = $comment->findVariantMatch($product);
                if (! $variant) {
                    $mailer->sendInvalidInstagramCommentEmail($customer, $product);
                    continue;
                }

                if ($variant->quantity <= 0) {
                    Waitlist::instagramAddItem($customer, $commentId, $variant);
                    $mailer->sendWaitlistEmail($customer, $product, $variant);
                } else {
                    try {
                        Cart::instagramAddItem($customer, $commentId, $variant);
                    } catch (ShopifyWebhookLockedException $e) {
                        ShopMailer::notifyAdmin($e);
                        log_error("[instagram-harvest][shopify] $e");
                        continue;
                    }

                    $mailer->sendItemAddedToCartEmail($customer, $product, $variant);
                }
            }
        }
    }

    public function getCommenter($comment)
    {
        return ShopUser::getByInstagramId($comment->fromId);
    }
}
