<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Product as ProductModel;

class ProductOption extends Drop
{
    /**
     * @var ProductModel
     */
    protected $product;

    /**
     * Can be 'color' or 'size'.
     *
     * @var string
     */
    protected $option;

    public function __construct(ProductModel $product, $option)
    {
        $this->product = $product;
        $this->option = $option;
    }

    /**
     * Returns the option's name.
     */
    public function name()
    {
        return ucfirst($this->option);
    }

    /**
     * Returns the option's position in the product options array.
     *
     * Input
     * <ul>
     *   {% for option in product.options_with_values %}
     *     <li>{{ option.position }} - {{ option.name }} </li>
     *   {% endfor %}
     * </ul>
     *
     * Output
     * <ul>
     *   <li>1 - Color</li>
     *   <li>2 - Size</li>
     * </ul>
     */
    public function position()
    {
        if ($this->option == 'color') {
            return 1;
        }

        return 2;
    }

    /**
     * Returns an array of possible values for this option.
     *
     * Input
     * <ul>
     *   {% for value in option.values %}
     *     <li>{{ value }}</li>
     *   {% endfor %}
     * </ul>
     *
     * Output
     * <ul>
     *   <li>Red</li>
     *   <li>Green</li>
     * </ul>
     */
    public function values()
    {
        $options = [];

        foreach ($this->product->inventory as $variant) {
            if ($this->option == 'color') {
                $options[] = $variant->color;
            } else {
                $options[] = $variant->size;
            }
        }

        return array_unique($options);
    }

    /**
     * Returns the currently selected value for this option.
     *
     * Input
     * <select>
     *   {% for value in option.values %}
     *     <option {% if option.selected_value == value %}selected{% endif %}>
     *       {{ value }}
     *     </option>
     *   {% endfor %}
     * </select>
     *
     * Output
     * <select>
     *   <option selected>Red</option>
     *   <option>Green</option>
     * </select>
     */
    public function selected_value()
    {
        // todo: implement
        return null;
    }
}
