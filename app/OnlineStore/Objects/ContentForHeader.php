<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class ContentForHeader extends Drop
{
    public function __toString()
    {
        $theme = get_theme();

        $path = resource_path("views/themes/{$theme}/content_for_header.liquid");
        if (file_exists($path)) {
            return file_get_contents($path);
        }

        return '';
    }
}
