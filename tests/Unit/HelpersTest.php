<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;

class HelpersTest extends TestCase
{
    /** @test */
    public function it_tests_non_existent_shop_setting()
    {
        $this->mockShopSetting('invalid-key', null);
        $this->assertNull(shop_setting('invalid-key'));
    }

    /** @test */
    public function it_tests_non_existent_shop_setting_and_default_value()
    {
        $this->mockShopSetting('invalid-key', null);
        $this->assertEquals('default-value', shop_setting('invalid-key', 'default-value'));
    }

    /** @test */
    public function it_tests_non_existent_shop_setting_with_existing_value()
    {
        $this->mockShopSetting('key', 'value');
        $this->assertEquals('value', shop_setting('key'));
    }

    /** @test */
    public function it_tests_fb_app_id_with_main_app()
    {
        $this->mockShopSetting('facebook.client_id', null);
        $this->assertEquals(fb_app_id(), '1111111111111111');
    }

    /** @test */
    public function it_tests_fb_app_id_with_custom_app()
    {
        $this->mockShopSetting('facebook.client_id', 'FB_APP_ID');
        $this->assertEquals(fb_app_id(), 'FB_APP_ID');
    }

    /** @test */
    public function it_tests_fb_app_secret_with_main_app()
    {
        $this->mockShopSetting('facebook.client_secret', null);
        $this->assertEquals(fb_app_secret(), '5ee2g533e6eac30d43ba8h470gf6addc');
    }

    /** @test */
    public function it_tests_fb_app_secret_with_custom_app()
    {
        $this->mockShopSetting('facebook.client_secret', 'FB_APP_SECRET');
        $this->assertEquals(fb_app_secret(), 'FB_APP_SECRET');
    }

    /** @test */
    public function it_tests_fb_app_access_token_with_main_app()
    {
        $this->mockShopSetting('facebook.app_access_token', null);
        $this->assertEquals(fb_app_access_token(), '2222222222222222|BpCvDPHvHxS3Fp43Ep5v3U3L4m8');
    }

    /** @test */
    public function it_tests_fb_app_access_token_with_custom_app()
    {
        $this->mockShopSetting('facebook.app_access_token', 'FB_APP_ACCESS_TOKEN');
        $this->assertEquals(fb_app_access_token(), 'FB_APP_ACCESS_TOKEN');
    }

    /** @test */
    public function product_image_returns_default_image()
    {
        $this->assertEquals('http://cdn.commentsold.test/images/default.png', product_image(null));
    }

    /** @test */
    public function it_tests_fb_api_version()
    {
        $this->assertEquals('v2.8', fb_api_version());
    }

    /** @test */
    public function it_returns_fb_page_id()
    {
        $this->mockShopSetting('facebook.page_id', 'page-id');
        $this->assertEquals(fb_page_id(), 'page-id');
    }

    /** @test */
    public function it_returns_fb_page_access_token()
    {
        $this->mockShopSetting('facebook.page_access_token', 'page-access-token');
        $this->assertEquals(fb_page_access_token(), 'page-access-token');
    }

    /** @test */
    public function it_returns_fb_group_owner_id()
    {
        $this->mockShopSetting('facebook.user_id', 'facebook-user-id');
        $this->assertEquals(fb_group_owner_id(), 'facebook-user-id');
    }

    /** @test */
    public function it_returns_enabled_groups_when_none_are_set()
    {
        $this->mockShopSetting('facebook.group_ids', null);
        $this->assertEquals(fb_enabled_group_ids(), []);
    }

    /** @test */
    public function it_returns_enabled_groups_when_one_is_set()
    {
        $this->mockShopSetting('facebook.group_ids', '["1054143263660231"]');
        $this->assertEquals(fb_enabled_group_ids(), ['1054143263660231']);
    }

    /** @test */
    public function it_checks_token_valid_days_with_empty_token()
    {
        $this->mockShopSetting('facebook.time-connected', null);
        $this->assertEquals(fb_token_valid_days(), 0);
    }

    /** @test */
    public function it_checks_token_valid_days_with_valid_token()
    {
        $this->mockShopSetting('facebook.time-connected', time() - 10 * 24*60*60);
        $this->assertEquals(fb_token_valid_days(), 50);
    }

    /** @test */
    public function it_checks_token_valid_days_with_valid_and_expired_token()
    {
        $this->mockShopSetting('facebook.time-connected', time() - 70 * 24*60*60);
        $this->assertEquals(fb_token_valid_days(), 0);
    }

    /** @test */
    public function it_returns_cdn_wrapped_asset_with_empty_asset()
    {
        $this->assertEquals(cdn(null), 'http://cdn.commentsold.test/');
    }

    /** @test */
    public function it_returns_cdn_wrapped_asset_with_asset()
    {
        $this->assertEquals(cdn('/image.jpg'), 'http://cdn.commentsold.test/image.jpg');
    }

    /** @test */
    public function it_returns_amount()
    {
        $this->assertEquals(amount(10), '10.00');
    }

    /** @test */
    public function it_returns_percentage()
    {
        $this->assertEquals(percentage(5), '5.00');
    }

    /** @test */
    public function it_returns_percentage_of_0()
    {
        $this->assertEquals(percentageOf(5, 0), '0.00');
    }

    /** @test */
    public function it_returns_percentage_of_300()
    {
        $this->assertEquals(percentageOf(5, 300), '1.67');
    }

    /** @test */
    public function it_returns_schema_name()
    {
        $this->assertEquals(schema_name('shop-name'), 'shop_name');
    }

    /** @test */
    public function it_returns_shop_url_for_default_subdomain()
    {
        $this->assertEquals(shop_url(), 'http://test-shop.commentsold.test/');
        $this->assertEquals(shop_url('/'), 'http://test-shop.commentsold.test/');
        $this->assertEquals(shop_url(null), 'http://test-shop.commentsold.test/');
        $this->assertEquals(shop_url('/page'), 'http://test-shop.commentsold.test/page');
    }

    /** @test */
    public function it_returns_shop_url_with_custom_domain()
    {
        config(['shop.custom-domain' => 'http://custom-shop.com']);
        $this->assertEquals(shop_url(), 'http://custom-shop.com/');
        $this->assertEquals(shop_url('/page'), 'http://custom-shop.com/page');
    }

    /** @test */
    public function it_returns_shop_id()
    {
        $this->assertEquals(shop_id(), 'test_shop');
    }

    /** @test */
    public function it_sets_custom_shop_db()
    {
        set_shop_database('custom-shop');
        $this->assertEquals(shop_id(), 'custom_shop');
    }

    /** @test */
    public function it_returns_shop_name()
    {
        $this->mockShopSetting('shop.name', null);
        $this->assertEquals(shop_name(), 'My Shop');
    }

    /** @test */
    public function it_returns_shop_description()
    {
        $this->mockShopSetting('shop.description', null);
        $this->assertEquals(shop_description(), 'My Shop Description');
    }

    /** @test */
    public function it_removes_new_lines()
    {
        $this->assertEquals(removeNewLines("My\ntext"), "My text");
    }

    /** @test */
    public function it_returns_local_pickup_enabled()
    {
        $this->mockShopSetting('local-pickup-enabled', null);
        $this->assertEquals(local_pickup_enabled(), false);
    }

    /** @test */
    public function it_returns_free_shipping_24hr_enabled()
    {
        $this->mockShopSetting('24hr-free-shipping-enabled', null);
        $this->assertEquals(free_shipping_24hr_enabled(), false);
    }

    /** @test */
    public function it_returns_expire()
    {
        $this->mockShopSetting('shopping-cart:expire', null);
        $this->assertEquals(expire(), 24);
    }

    /** @test */
    public function it_returns_remind()
    {
        $this->mockShopSetting('shopping-cart:remind', null);
        $this->assertEquals(remind(), 12);
    }

    /** @test */
    public function product_image_returns_amazon_url()
    {
        $url = 'https://s3.amazonaws.com/commentsold/test_shop/products/123456790.jpg';
        $this->assertEquals($url, product_image('123456790.jpg'));
    }

    /** @test */
    public function product_image_returns_shopify_full_url()
    {
        $url = 'https://cdn.shopify.com/s/files/1/2100/5319/products/fruit-pins-on-jean-jacket.jpg?v=1497545545';
        $this->assertEquals($url, product_image($url));
    }

    /** @test */
    public function product_thumb_returns_default_image()
    {
        $this->assertEquals('http://cdn.commentsold.test/images/default.png', product_thumb(null));
    }

    /** @test */
    public function product_thumb_already_resized()
    {
        $url = 'https://cdn.shopify.com/s/files/1/2100/5319/products/jacket_1000x.jpg?v=1497545545';
        $this->assertEquals('https://cdn.shopify.com/s/files/1/2100/5319/products/jacket_290x290.jpg?v=1497545545', product_thumb($url));
    }

    /** @test */
    public function product_thumb_returns_amazon_url()
    {
        $url = 'https://s3.amazonaws.com/commentsold/test_shop/products/thumbs/123456790.jpg';
        $this->assertEquals($url, product_thumb('123456790.jpg'));
    }

    /** @test */
    public function product_thumb_returns_shopify_url()
    {
        $url = 'https://cdn.shopify.com/s/files/1/2100/5319/products/fruit-pins-on-jean-jacket.jpg?v=1497545545';
        $thumb = 'https://cdn.shopify.com/s/files/1/2100/5319/products/fruit-pins-on-jean-jacket_290x290.jpg?v=1497545545';

        $this->assertEquals($thumb, product_thumb($url));
    }

    /** @test */
    public function it_returns_image_with_original_size()
    {
        $this->assertEquals(image('image.jpg'), 'http://cdn.commentsold.test/images/test_shop/image.jpg');
    }

    /** @test */
    public function it_returns_image_with_dynamic_size()
    {
        $this->assertEquals(image('image.jpg', '100x'), 'http://cdn.commentsold.test/images/test_shop/image.jpg?size=100x');
    }

    /** @test */
    public function it_returns_current_day()
    {
        $this->assertEquals(current_day(), date('m/d/Y'));
    }

    /** @test */
    public function it_returns_current_time()
    {
        $this->assertEquals(current_time(), date('F jS, Y g:i A'));
    }

    /** @test */
    public function it_tests_get_user_timezone()
    {
        $this->mockShopSetting('shop.timezone', 'America/New_York');
        $this->assertEquals(get_user_timezone(), 'America/New_York');
    }

    /** @test */
    public function it_tests_apply_timezone()
    {
        // $this->mockShopSetting('shop.timezone', null);
        // $this->assertEquals(apply_timezone(1500512751), 'July 19th, 2017 8:05 PM');
        // $this->assertEquals(apply_timezone('July 19th, 2017 8:05 PM'), 'July 19th, 2017 8:05 PM');
    }

    /** @test */
    public function it_tests_style_date()
    {
        $this->assertEquals(style_date(1500512751), 'July 19th, 2017 8:05 PM');
        $this->assertEquals(style_date('July 19th, 2017 8:05 PM'), 'July 19th, 2017 8:05 PM');
    }

    /** @test */
    public function it_tests_shop_redirect()
    {

    }

    /** @test */
    public function it_tests_shop_email()
    {
        $this->mockShopSetting('shop.company_email', null);
        $this->assertEquals(shop_email(), 'support@commentsold.com');
    }

    /** @test */
    public function it_tests_shop_email_with_bad_email()
    {
        $this->mockShopSetting('shop.company_email', 'invalid-email');
        $this->assertEquals(shop_email(), 'support@commentsold.com');
    }

    /** @test */
    public function it_tests_shop_email_with_valid_email()
    {
        $this->mockShopSetting('shop.company_email', 'shop@example.com');
        $this->assertEquals(shop_email(), 'shop@example.com');
    }

    /** @test */
    public function it_tests_log_master_info()
    {
        $log = Mockery::mock('App\Models\AppLog');
        $log->shouldReceive('info')->with('master info message');
        app()->instance('App\Models\AppLog', $log);

        log_master_info('master info message');
    }

    /** @test */
    public function it_tests_log_info()
    {
        $this->mockShopLog('info', 'info message');
        log_info('info message');
    }

    /** @test */
    public function it_tests_log_notice()
    {
        $this->mockShopLog('notice', 'notice message');
        log_notice('notice message');
    }

    /** @test */
    public function it_tests_log_error()
    {
        $this->mockShopLog('error', 'error message');
        log_error('error message');
    }

    /** @test */
    public function it_tests_card_image()
    {
        $this->assertEquals(card_image('Visa'), '/images/icons/Visa-56.png');
        $this->assertEquals(card_image('Mastercard'), '/images/icons/Mastercard-56.png');
        $this->assertEquals(card_image('Amex'), '/images/icons/Amex-56.png');
        $this->assertEquals(card_image('Discover'), '/images/icons/Discover-56.png');
        $this->assertEquals(card_image(null), '');
    }

    /** @test */
    public function it_tests_facebook_profile()
    {
        $this->assertEquals(facebook_profile('1234567890'), 'https://www.facebook.com/1234567890');
    }

    /** @test */
    public function it_tests_pagination_links()
    {

    }

    /** @test */
    public function it_tests_elapsed_time()
    {
        $this->assertEquals(elapsed_time(microtime(true)), '0.0000');
    }

    /** @test */
    public function it_tests_script_raven()
    {

    }

    /** @test */
    public function it_tests_mysql_utf8()
    {
        $this->assertEquals(mysql_utf8('my message'), 'my message');
    }

    /** @test */
    public function it_tests_template()
    {

    }

    /** @test */
    public function it_tests_theme()
    {

    }

    /** @test */
    public function it_tests_get_theme_default()
    {
        $this->assertEquals(get_theme(), null);
    }

    /** @test */
    public function it_tests_get_theme_for_shop()
    {
        set_shop_database('shop1');
        $this->assertNotNull(get_theme());
    }

    /** @test */
    public function it_tests_theme_body_id()
    {
        $this->assertEquals(theme_body_id('My Title'), 'my-title');
    }

    /** @test */
    public function it_tests_shop_seo_description()
    {
        $this->assertTrue(strlen(shop_seo_description()) > 0);
    }

    /** @test */
    public function it_tests_from_email_address()
    {
        $this->assertEquals(from_email_address(), 'support@commentsold.com');

        set_shop_database('pinkcoconut');
        $this->assertEquals('pinkcoconutonline@gmail.com', from_email_address());

        set_shop_database('lindylous');
        $this->assertEquals('smile@lindylousboutique.com', from_email_address());

        set_shop_database('divas');
        $this->assertEquals('support@shopdiscountdivas.com', from_email_address());
    }

    /** @test */
    public function it_tests_dollars_to_cents()
    {
        $this->assertEquals(dollars_to_cents(null), 0);
        $this->assertEquals(dollars_to_cents(10), 1000);
        $this->assertEquals(dollars_to_cents(5.5), 550);
        $this->assertEquals(dollars_to_cents(19.99), 1999);
    }

    /** @test */
    public function it_tests_usps_enabled()
    {
        $this->assertEquals(usps_enabled(), false);

        set_shop_database('bktest');
        $this->assertEquals(usps_enabled(), true);
    }

    /** @test */
    public function it_tests_unique_options()
    {
        $this->assertEquals(unique_options(null), []);
        $this->assertEquals(unique_options(['Red', 'red']), ['Red']);
        $this->assertEquals(unique_options(['Blue', 'Red', 'blue']), ['Blue', 'Red']);
    }

    /** @test */
    public function it_tests_shopify_enabled()
    {
        $this->mockShopSetting('shopify.enabled', null);
        $this->assertEquals(shopify_enabled(), false);
    }

    /** @test */
    public function cache_headers()
    {
        $this->assertEquals(cache_header(), [
            'Cache-Control' => "max-age=3600, stale-while-revalidate=240, stale-if-error=2400, public",
            'Surrogate-Key' => "test_shop",
            'Surrogate-Control' => "max-age=3600, stale-while-revalidate=240, stale-if-error=2400"
        ]);

        $this->assertEquals(cache_header('collections product', 4000), [
            'Cache-Control' => "max-age=4000, stale-while-revalidate=240, stale-if-error=2400, public",
            'Surrogate-Key' => "test_shop test_shop:collections test_shop:product",
            'Surrogate-Control' => "max-age=4000, stale-while-revalidate=240, stale-if-error=2400"
        ]);
    }
}
