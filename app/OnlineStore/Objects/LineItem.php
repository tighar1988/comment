<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\ShoppingCart\CartItem as ShoppingCartItem;

/**
 * A line_item represents a single line in the shopping cart.
 * There is one line item for each distinct product variant in the cart.
 *
 * The line_item object can be accessed in all Liquid templates via cart.items,
 * in notification email templates via line_items, on the order status page of
 * the checkout, as well as in apps such as Order Printer.
 *
 * The line_item object has the following attributes:
 */
class LineItem extends Drop
{
    protected $item;

    protected static $expire;

    protected $quantity = 0;

    protected $variant;

    protected $product;

    public function __construct(ShoppingCartItem $item)
    {
        $this->item = $item;
        $this->quantity = 1;
    }

    public function increaseQuantity()
    {
        $this->quantity++;
    }

    /**
     * Returns any discounts that scripts have applied to the line item.
     * Only has a value if you are using the Script Editor app.
     */
    public function discounts()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the fulfillment of the line item.
     */
    public function fulfillment()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the fulfillment service associated with the line item's variant. Line items
     * that have no fulfillment service will return manual.
     */
    public function fulfillment_service()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns true if the line item's product is a gift card, or false if it is not.
     */
    public function gift_card()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the weight of the line item. Use the weight_with_unit filter to format the weight.
     */
    public function grams()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the line item key, a unique identifier for the line item. The line item key is
     * constructed from the line item's variant ID plus a hash of the line item's properties,
     * even if the item has no additional properties.
     *
     * Input
     * {{ line_item.key }}
     *
     * Output
     * 17285644550:70ff98a797ed385f6ef25e6e974708ca
     */
    public function key()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the combined price of all the items in the line_item. This is equal to
     * line_item.price times line_item.quantity.
     */
    public function line_price()
    {
        return dollars_to_cents($this->item->price() * $this->quantity);
    }

    /**
     * Returns the discount message if a script has applied a discount to the line item.
     * Only has a value if you are using the Script Editor app.
     */
    public function message()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the original price of the line item before discounts were applied. Only has
     * a value if you are using the Script Editor app.
     */
    public function original_line_price()
    {
        return dollars_to_cents($this->item->price);
    }

    /**
     * Returns the original price of the line item before discounts were applied. Only has
     * a value if you are using the Script Editor app.
     */
    public function original_price()
    {
        return dollars_to_cents($this->item->price);
    }

    /**
     * Returns the product of the line item.
     */
    public function product()
    {
        if (is_null($this->variant)) {
            $this->variant = app('App\Models\Inventory')->find($this->item->inventory_id);
            $this->product = $this->variant->product;
        }

        return new Product($this->product);
    }

    /**
     * Returns an array of custom information for an item that has been added to the cart.
     *
     * For more information, see the documentation on customization information for products.
     * When accessed, line_item.properties array elements will be displayed using a for loop.
     *
     * Tip
     * The Liquid code first checks to ensure that the line_item.properties array is not empty.
     * If the array is empty, the loop does not run and nothing is printed in the list.
     *
     * Input
     * {% unless line_item.properties == empty %}
     * <ul>
     *   {% for property in line_item.properties %}
     *     <li>{{ property.first }}: {{ property.last }}</li>
     *   {% endfor %}
     * </ul>
     * {% endunless %}
     *
     * Output
     * <ul>
     *   <li>Monogram: My dog is the cutest</li>
     *   <li>Gift wrap: Yes</li>
     * </ul>
     */
    public function properties()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the quantity of the line item.
     */
    public function quantity()
    {
        return $this->quantity;
    }

    /**
     * Returns true if the variant of the line item requires shipping, or false if it does not.
     */
    public function requires_shipping()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the SKU (stock keeping unit) of the line item's variant.
     */
    public function sku()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the successfully fulfilled quantity of the line item.
     */
    public function successfully_fulfilled_quantity()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns true if taxes are charged on the line item's variant, or false if they are not.
     */
    public function taxable()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the title of the line item. line_item.title combines both the line item's
     * product.title and the line item's variant.title, separated by a hyphen.
     *
     * Input
     * {{ line_item.title }}
     *
     * Output
     * Balloon Shirt - Medium
     * To output just the product title or variant title, you can access the title of the respective variables.
     *
     * Input
     * Product title: {{ line_item.product.title }}
     * Variant title: {{ line_item.variant.title }}
     *
     * Output
     * Product title: Balloon Shirt
     * Variant title: Medium
     */
    public function title()
    {
        $variant = $this->variant();

        return $variant->name();
    }

    /**
     * Returns the total amount of all discounts applied to the line item.
     * Only has a value if you are using the Script Editor app.
     */
    public function total_discount()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the relative URL of the line item's variant. The relative URL does
     * not include your store's root URL (mystore.myshopify.com).
     */
    public function url()
    {
        $product = $this->product();
        return $product->url();
    }

    /**
     * Returns the variant of the line item.
     */
    public function variant()
    {
        if (is_null($this->variant)) {
            $this->variant = app('App\Models\Inventory')->find($this->item->inventory_id);
            $this->product = $this->variant->product;
        }

        return new Variant($this->variant, $this->product);
    }

    /**
     * Returns the ID of the line item's variant.
     */
    public function variant_id()
    {
        return $this->item->inventory_id;
    }

    /**
     * Returns the vendor of the line item's product.
     */
    public function vendor()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the ID of the line item, which is a unique ID generated for the cart.
     */
    public function id()
    {
        return $this->item->id;
    }

    public function name()
    {
        return $this->item->name;
    }

    /**
     * Returns the price of the line item's variant.
     */
    public function price()
    {
        return dollars_to_cents($this->item->price());
    }

    public function cost()
    {
        return $this->item->cost;
    }

    /**
     * Returns the line item's image.
     *
     * You can apply the img_url filter directly to the line item instead of its image attribute.
     * This will generate a working image URL for any object with an image attribute (variant, product,
     * line item, collection), image object, or image src. This is useful for line items, since it will
     * output the item's variant image or the product's featured image if no variant image exists.
     *
     * In the example, note that the output is the same no matter if img_url is used on line_item or line_item.image.
     *
     * Input
     * {{ line_item.image | img_url: 'small' | img_tag }}
     * {{ line_item | img_url: 'small' | img_tag }}
     *
     * Output
     * <img src="//cdn.shopify.com/s/files/1/0159/3350/products/hvt401_red_small.jpg?v=1398706734" />
     * <img src="//cdn.shopify.com/s/files/1/0159/3350/products/hvt401_red_small.jpg?v=1398706734" />
     */
    public function image()
    {
        if (is_null($this->variant)) {
            $this->variant = app('App\Models\Inventory')->find($this->item->inventory_id);
            $this->product = $this->variant->product;
        }

        return new Image($this->product->image, $this->product);
    }

    public function color()
    {
        return $this->item->color;
    }

    public function size()
    {
        return $this->item->size;
    }

    public function created_at()
    {
        return $this->item->created_at;
    }

    /**
     * Returns the ID of the line item's product.
     */
    public function product_id()
    {
        return $this->item->product_id;
    }

    public function inventory_id()
    {
        return $this->item->inventory_id;
    }

    public function comment_id()
    {
        return $this->item->comment_id;
    }

    public function order_source()
    {
        return $this->item->order_source;
    }

    public function product_style()
    {
        return $this->item->product_style;
    }

    public function product_brand()
    {
        return $this->item->product_brand;
    }

    public function product_brand_style()
    {
        return $this->item->product_brand_style;
    }

    public function product_description()
    {
        return $this->item->product_description;
    }

    public function weight()
    {
        return $this->item->weight;
    }

    public function shopify_inventory_id()
    {
        return $this->item->shopify_inventory_id;
    }

    public function charge_taxes()
    {
        return $this->item->charge_taxes;
    }

    public function expire_at()
    {
        if (is_null(static::$expire)) {
            static::$expire = expire();
        }

        return $this->item->expireAt(static::$expire);
    }
}
