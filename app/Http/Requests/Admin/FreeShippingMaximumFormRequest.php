<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FreeShippingMaximumFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_maximum' => 'required|numeric|min:0|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'remind.required' => 'The maximum shipping amount is required.',
            'remind.numeric'  => 'The maximum shipping amount must be numeric.',
            'remind.min'      => 'The maximum shipping amount must be at least 0.',
        ];
    }
}
