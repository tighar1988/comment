<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    /**
     * @group register
     */
    public function testUserRegister()
    {
        $this->browse(function ($browser) {

            $id = time();

            // sign up
            $browser->maximize()
                    ->visit('/register')
                    ->type('name', "John Doe {$id}")
                    ->type('email', "john.doe.{$id}@example.com")
                    ->type('password', 'secret')
                    ->type('password_confirmation', 'secret')
                    ->press('Register')
                    ->assertPathIs('/admin/setup/step-1');

            // don't allow admin access until setup is complete
            $browser->visit('/admin')
                    ->assertPathIs('/admin/setup/step-1');

            // create subdomain
            $browser->visit('/admin/setup/step-1')
                    ->type('shop_name', "john-doe-shop-{$id}")
                    ->press("Ok, let's do this!")
                    ->waitForText('Add a new card', 20)
                    ->assertPathIs('/admin/setup/step-2');

            // enter card details
            $browser->visit('/admin/setup/step-2')
                    ->type('#cc-number', '4242 4242 4242 4242')
                    ->select('.cc-expiration-month', '12')
                    ->select('.cc-expiration-year', '2022')
                    ->type('#cvv', '123')
                    ->press('Save Credit Card')
                    ->waitForText('Dashboard', 10)
                    ->assertPathIs('/admin');
        });
    }
}
