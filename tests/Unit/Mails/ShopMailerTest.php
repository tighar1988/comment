<?php

namespace Tests\Unit\Mails;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use App\Mails\ShopMailer;
use App\Models\ShopUser;
use App\Models\Order;
use App\Models\User;
use App\Models\Post;
use App\Models\Inventory;
use App\Models\Product;
use Mockery;
use App\Mail\NotifyAdmin;
use App\Mail\PayPalFees;
use App\Mail\PayPalFeesFailed;
use App\Mail\NewReposts;
use App\Mail\ExpiryReminder;
use App\Mail\WaitlistEmail;
use App\Mail\WaitlistActivated;
use App\Mail\AddedToCart;
use App\Mail\OrderPaid;
use App\Mail\OrderShipped;
use App\Mail\LocalPickupOrderFulfilled;
use App\Mail\InvalidInstagramComment;
use App\Mail\AwardedAccountCredit;
use App\Mail\PrepaidCredit;
use App\Mail\PrepaidCreditFailed;
use Tests\Helpers\MailFake;
use Exception;

class ShopMailerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        Mail::swap(new MailFake);

        $log = Mockery::mock('App\Models\ShopLog');
        $log->shouldReceive('info');
        app()->instance('App\Models\ShopLog', $log);

        $setting = Mockery::mock('App\Models\ShopSettings');
        $setting->shouldReceive('getSetting')->with('shop.company_email')->andReturn(null);
        $setting->shouldReceive('getSetting')->with('shop.name')->andReturn(null);
        $setting->shouldReceive('getSetting')->with('template.awarded-account-credit')->andReturn(null);
        $setting->shouldReceive('getSetting')->with('shopping-cart:expire')->andReturn(null);
        $setting->shouldReceive('getSetting')->with('template.invalid-comment')->andReturn(null);
        app()->instance('App\Models\ShopSettings', $setting);
    }

    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(ShopMailer::class, new ShopMailer);
    }

    /** @test */
    public function it_mails_notify_admin()
    {
        ShopMailer::notifyAdmin(new Exception('Test Exception'));

        // todo: assert that data has only these keys, and does not have any other
        Mail::assertSent(NotifyAdmin::class, function ($mail) {
                return $mail->data['exception'] == 'Exception' &&
                    TestCase::arrayHasKeys($mail->data, [
                        'notification',
                        'shopDb',
                        'userName',
                        'userEmail',
                        'requestUri',
                        'refererUri',
                        'queryString',
                        'requestMethod',
                        'ajaxRequest',
                        'redirectUrl',
                        'requestScheme',
                        'remoteAddr',
                        'serverAddr',
                        'httpCookie',
                        'httpUserAgent',
                        'serverName',
                        'requestTime',
                        'exception',
                   ]);
                   $mail->hasTo('brandon@bkruse.com');
        });
    }

    /** @test */
    public function it_mails_order_paid_email_with_local_pickup()
    {
        $inventory = new Inventory;
        $inventory->prod_name = 'Product 1';
        $inventory->price = '22';

        // todo add faker library here
        $order = static::mockOrder([
            'id'              => 10,
            'email'           => 'john.doe@example.com',
            'customer_id'     => 1,
            'local_pickup'    => '1',
            'street_address'  => 'Summer street',
            'apartment'       => '',
            'city'            => 'London',
            'state'           => 'CA',
            'zip'             => '123456',
            'ship_charged'    => '4',
            'subtotal'        => '18',
            'total'           => '22',
            'ship_name'       => 'John Doe',
            'tax_total'       => '0',
            'coupon_discount' => '0',
            'apply_balance'   => '0',
            'orderProducts'   => collect([$inventory]),
        ]);

        (new ShopMailer)->sendOrderPaidEmail($order);

        Mail::assertSent(OrderPaid::class, function ($mail) use ($order) {
            return $mail->data['orderId'] === $order->id &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopId',
                        'shopName',
                        'orderProducts',
                        'shopUrl',
                        'orderId',
                        'localPickup',
                        'streetAddress',
                        'apartment',
                        'city',
                        'state',
                        'zip',
                        'email',
                        'shipCharged',
                        'subtotal',
                        'total',
                        'shipName',
                        'taxTotal',
                        'couponDiscount',
                        'applyBalance',
                   ]);
                   isset($mail->data['orderProducts'][0]['name']) &&
                   isset($mail->data['orderProducts'][0]['price']) &&
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_order_shipped_email_for_shipped_order()
    {
        $inventory = new Inventory;
        $inventory->prod_name = 'Product 1';
        $inventory->price = '22';

        $order = static::mockOrder([
            'id'              => 11,
            'email'           => 'john.doe@example.com',
            'customer_id'     => 1,
            'local_pickup'    => '0',
            'street_address'  => 'Summer street',
            'apartment'       => '',
            'city'            => 'London',
            'state'           => 'CA',
            'zip'             => '123456',
            'ship_charged'    => '4',
            'subtotal'        => '18',
            'total'           => '22',
            'ship_name'       => 'John Doe',
            'tax_total'       => '0',
            'coupon_discount' => '0',
            'apply_balance'   => '0',
            'orderProducts'   => collect([$inventory]),
            'tracking_number' => '1234563234234',
        ]);

        (new ShopMailer)->sendOrderShippedEmail($order);

        Mail::assertSent(OrderShipped::class, function ($mail) use ($order) {
            return $mail->data['orderId'] === $order->id &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopName',
                        'orderProducts',
                        'shopUrl',
                        'shopId',
                        'orderId',
                        'streetAddress',
                        'apartment',
                        'city',
                        'state',
                        'zip',
                        'email',
                        'shipCharged',
                        'subtotal',
                        'total',
                        'shipName',
                        'trackingNumber',
                        'taxTotal',
                        'couponDiscount',
                        'applyBalance',
                   ]);
                   isset($mail->data['orderProducts'][0]['name']) &&
                   isset($mail->data['orderProducts'][0]['price']) &&
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_order_shipped_email_for_local_pickup_order()
    {
        $inventory = new Inventory;
        $inventory->prod_name = 'Product 1';
        $inventory->price = '22';

        $order = static::mockOrder([
            'id'              => 12,
            'email'           => 'john.doe@example.com',
            'customer_id'     => 1,
            'local_pickup'    => '1',
            'street_address'  => 'Summer street',
            'apartment'       => '',
            'city'            => 'London',
            'state'           => 'CA',
            'zip'             => '123456',
            'ship_charged'    => '4',
            'subtotal'        => '18',
            'total'           => '22',
            'ship_name'       => 'John Doe',
            'tax_total'       => '0',
            'coupon_discount' => '0',
            'apply_balance'   => '0',
            'orderProducts'   => collect([$inventory]),
            'tracking_number' => '1234563234234',
        ]);

        (new ShopMailer)->sendOrderShippedEmail($order);

        Mail::assertSent(LocalPickupOrderFulfilled::class, function ($mail) use ($order) {
            return $mail->getData()['orderId'] === $order->id &&
                   TestCase::arrayHasKeys($mail->getData(), [
                        'shopFromEmail',
                        'shopEmail',
                        'shopName',
                        'orderProducts',
                        'shopUrl',
                        'shopId',
                        'orderId',
                        'streetAddress',
                        'apartment',
                        'city',
                        'state',
                        'zip',
                        'email',
                        'shipCharged',
                        'subtotal',
                        'total',
                        'shipName',
                        'trackingNumber',
                        'taxTotal',
                        'couponDiscount',
                        'applyBalance',
                   ]);
                   isset($mail->getData()['orderProducts'][0]['name']) &&
                   isset($mail->getData()['orderProducts'][0]['price']) &&
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_paypal_fees_email()
    {
        $shopOwner = new User;
        $shopOwner->name = 'John Doe';
        $shopOwner->email = 'john.doe@example.com';

        $sinceTime = time();
        $feesTotal = 50;

        (new ShopMailer)->sendPayPalFeesEmail($shopOwner, $feesTotal, $sinceTime);

        Mail::assertSent(PayPalFees::class, function ($mail) {
            return $mail->data['shopOwnerName'] === 'John Doe' &&
                   $mail->data['feesTotal'] === 50 &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopOwnerName',
                        'sinceTime',
                        'feesTotal',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_paypal_fees_failed_email()
    {
        $shopOwner = new User;
        $shopOwner->name = 'John Doe';
        $shopOwner->email = 'john.doe@example.com';

        $sinceTime = time();
        $feesTotal = 100;

        (new ShopMailer)->sendPayPalFeesFailedEmail($shopOwner, $feesTotal, $sinceTime);

        Mail::assertSent(PayPalFeesFailed::class, function ($mail) {
            return $mail->data['shopOwnerName'] === 'John Doe' &&
                   $mail->data['feesTotal'] === 100 &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopOwnerName',
                        'sinceTime',
                        'feesTotal',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_prepaid_credit_email()
    {
        $shopOwner = new User;
        $shopOwner->name = 'John Doe';
        $shopOwner->email = 'john.doe@example.com';

        $amount = 70;

        (new ShopMailer)->sendPrepaidCreditEmail($shopOwner, $amount);

        Mail::assertSent(PrepaidCredit::class, function ($mail) {
            return $mail->data['shopOwnerName'] === 'John Doe' &&
                   $mail->data['amount'] === 70 &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopOwnerName',
                        'amount',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_prepaid_credit_failed_email()
    {
        $shopOwner = new User;
        $shopOwner->name = 'John Doe';
        $shopOwner->email = 'john.doe@example.com';

        $amount = 80;

        (new ShopMailer)->sendPrepaidCreditFailedEmail($shopOwner, $amount);

        Mail::assertSent(PrepaidCreditFailed::class, function ($mail) {
            return $mail->data['shopOwnerName'] === 'John Doe' &&
                   $mail->data['amount'] === 80 &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopOwnerName',
                        'amount',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_new_repost_email()
    {
        $shopOwner = new User;
        $shopOwner->name = 'John Doe';
        $shopOwner->email = 'john.doe@example.com';

        $post = new Post;
        $post->id = 10;
        $post->supplement = json_encode(['message' => 'My post message!', 'url' => 'https://example.com/image.jpg']);
        $createdPosts = [$post];

        (new ShopMailer)->sendNewRepostEmail($shopOwner, $createdPosts);

        Mail::assertSent(NewReposts::class, function ($mail) {
            return $mail->data['shopOwnerName'] === 'John Doe' &&
                   count($mail->data['posts']) == 1 &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopOwnerName',
                        'posts',
                   ]);
                   isset($mail->data['posts'][0]['edit_url']) &&
                   isset($mail->data['posts'][0]['message']) &&
                   isset($mail->data['posts'][0]['image']) &&
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_expiry_reminder_email()
    {
        $customer = new ShopUser;
        $customer->id = 101;
        $customer->name = 'John Doe';
        $customer->email = 'john.doe@example.com';

        $product = new Product;
        $product->product_name = 'My Product';

        $variant = new Inventory;
        $variant->id = 301;

        (new ShopMailer)->sendExpiryReminderEmail($customer, $product, $variant);

        Mail::assertSent(ExpiryReminder::class, function ($mail) {
            return $mail->data['customerName'] === 'John Doe' &&
                   $mail->data['productName'] == 'My Product' &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopName',
                        'customerName',
                        'shopUrl',
                        'productName',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_waitlist_email()
    {
        $customer = new ShopUser;
        $customer->id = 101;
        $customer->name = 'John Doe';
        $customer->email = 'john.doe@example.com';
        $customer->street_address = 'Summer Street';
        $customer->apartment = '33';
        $customer->city = 'London';
        $customer->state = 'CA';
        $customer->zip = '333555';

        $product = new Product;
        $product->product_name = 'My Product';

        $variant = new Inventory;
        $variant->id = 301;

        (new ShopMailer)->sendWaitlistEmail($customer, $product, $variant);

        Mail::assertSent(WaitlistEmail::class, function ($mail) {
            return $mail->data['customerName'] === 'John Doe' &&
                   $mail->data['productName'] == 'My Product' &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopId',
                        'shopName',
                        'productName',
                        'customerName',
                        'customerStreetAddress',
                        'customerApartment',
                        'customerCity',
                        'customerState',
                        'customerZip',
                        'shopUrl',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_waitlist_item_activated_email()
    {
        $customer = new ShopUser;
        $customer->id = 101;
        $customer->name = 'John Doe';
        $customer->email = 'john.doe@example.com';
        $customer->street_address = 'Summer Street';
        $customer->apartment = '33';
        $customer->city = 'London';
        $customer->state = 'CA';
        $customer->zip = '333555';

        $product = new Product;
        $product->product_name = 'My Product';

        $variant = new Inventory;
        $variant->id = 301;

        (new ShopMailer)->sendWaitlistItemActivatedEmail($customer, $product, $variant);

        Mail::assertSent(WaitlistActivated::class, function ($mail) {
            return $mail->data['customerName'] === 'John Doe' &&
                   $mail->data['productName'] == 'My Product' &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopName',
                        'productName',
                        'customerName',
                        'customerStreetAddress',
                        'customerApartment',
                        'customerCity',
                        'customerState',
                        'customerZip',
                        'shopUrl',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_item_added_to_cart_email()
    {
        $customer = new ShopUser;
        $customer->id = 101;
        $customer->name = 'John Doe';
        $customer->email = 'john.doe@example.com';
        $customer->street_address = 'Summer Street';
        $customer->apartment = '33';
        $customer->city = 'London';
        $customer->state = 'CA';
        $customer->zip = '333555';

        $product = new Product;
        $product->product_name = 'My Product';

        $variant = new Inventory;
        $variant->id = 301;

        (new ShopMailer)->sendItemAddedToCartEmail($customer, $product, $variant);

        Mail::assertSent(AddedToCart::class, function ($mail) {
            return $mail->data['customerName'] === 'John Doe' &&
                   $mail->data['productName'] == 'My Product' &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopId',
                        'shopName',
                        'productName',
                        'customerName',
                        'customerStreetAddress',
                        'customerApartment',
                        'customerCity',
                        'customerState',
                        'customerZip',
                        'expire',
                        'shopUrl',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_invalid_instagram_comment_email()
    {
        $customer = new ShopUser;
        $customer->id = 101;
        $customer->name = 'John Doe';
        $customer->email = 'john.doe@example.com';
        $customer->street_address = 'Summer Street';
        $customer->apartment = '33';
        $customer->city = 'London';
        $customer->state = 'CA';
        $customer->zip = '333555';

        $product = new Product;
        $product->id = 10;
        $product->product_name = 'My Product';

        (new ShopMailer)->sendInvalidInstagramCommentEmail($customer, $product);

        Mail::assertSent(InvalidInstagramComment::class, function ($mail) {
            return $mail->data['customerName'] === 'John Doe' &&
                   $mail->data['productName'] == 'My Product' &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopName',
                        'productName',
                        'customerName',
                        'shopUrl',
                        'waitlistMessage',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }

    /** @test */
    public function it_sends_awarded_account_credit_email()
    {
        $customer = new ShopUser;
        $customer->name = 'John Doe';
        $customer->email = 'john.doe@example.com';

        $amount = 10;

        (new ShopMailer)->sendAwardedAccountCreditEmail($customer, $amount);

        Mail::assertSent(AwardedAccountCredit::class, function ($mail) use ($amount) {
            return $mail->data['amount'] === '10.00' &&
                   ! empty($mail->data['template']) &&
                   TestCase::arrayHasKeys($mail->data, [
                        'shopFromEmail',
                        'shopEmail',
                        'shopName',
                        'customerName',
                        'shopUrl',
                        'amount',
                        'template',
                   ]);
                   $mail->hasTo('john.doe@example.com');
        });
    }
}
