<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Jobs\CollectPayPalFees;

class CollectPayPalFeesTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(CollectPayPalFees::class, new CollectPayPalFees('shop'));
    }
}
