@extends('admin.setup.templates')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Facebook Template for Invalid Comment</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="setup.notify-invalid-comment" @if(shop_setting('setup.notify-invalid-comment', true)) checked="" @endif class="toggle-switch"></label></div>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/templates/update') }}">
                    <div class="form-group @hasError('invalid_comment')">
                        <label class="control-label">Template</label>

                        <input type="hidden" name="key" value="invalid_comment">
                        <textarea class="form-control" name="invalid_comment">{{ old('invalid_comment', template('invalid-comment')) }}</textarea>

                        <blockquote style="margin-top: 5px;">
                            <p style="white-space: pre;">You can use <code>{variants}</code> in the template. It will be automatically replaced with the available options for each product.<br/><span class="text-muted">For example, it will show: <em>"Colors: red, blue / Sizes: small, medium"</em></span></p>
                        </blockquote>
                        @error('invalid_comment')
                    </div>

                    <button class="btn btn-success" data-loading-text="Updating Template.." type="submit">Update Template</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Facebook Template for Item Waitlisted</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="setup.notify-item-waitlisted" @if(shop_setting('setup.notify-item-waitlisted', true)) checked="" @endif class="toggle-switch"></label></div>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/templates/update') }}">
                    <div class="form-group @hasError('item_waitlisted')">
                        <label class="control-label">Template</label>

                        <input type="hidden" name="key" value="item_waitlisted">
                        <textarea class="form-control" name="item_waitlisted">{{ old('item_waitlisted', template('item-waitlisted')) }}</textarea>
                        @error('item_waitlisted')
                    </div>

                    <button class="btn btn-success" data-loading-text="Updating Template.." type="submit">Update Template</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Facebook Template for Waitlist Item Activated</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="setup.notify-waitlisted-activated" @if(shop_setting('setup.notify-waitlisted-activated', true)) checked="" @endif class="toggle-switch"></label></div>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/templates/update') }}">
                    <div class="form-group @hasError('waitlist_activated')">
                        <label class="control-label">Template</label>

                        <input type="hidden" name="key" value="waitlist_activated">
                        <textarea class="form-control" name="waitlist_activated">{{ old('waitlist_activated', template('waitlist-activated')) }}</textarea>
                        @error('waitlist_activated')
                    </div>

                    <button class="btn btn-success" data-loading-text="Updating Template.." type="submit">Update Template</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Facebook Template for Order Without Email</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="setup.notify-order-without-email" @if(shop_setting('setup.notify-order-without-email', true)) checked="" @endif class="toggle-switch"></label></div>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/templates/update') }}">
                    <div class="form-group @hasError('order_without_email')">
                        <label class="control-label">Template</label>

                        <input type="hidden" name="key" value="order_without_email">
                        <textarea class="form-control" name="order_without_email">{{ old('order_without_email', template('order-without-email')) }}</textarea>
                        @error('order_without_email')
                    </div>

                    <button class="btn btn-success" data-loading-text="Updating Template.." type="submit">Update Template</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Facebook Template for New Order</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="setup.notify-got-order" @if(shop_setting('setup.notify-got-order', true)) checked="" @endif class="toggle-switch"></label></div>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/templates/update') }}">
                    <div class="form-group @hasError('new_order')">
                        <label class="control-label">Template</label>

                        <input type="hidden" name="key" value="new_order">
                        <textarea class="form-control" name="new_order">{{ old('new_order', template('new-order')) }}</textarea>
                        @error('new_order')
                    </div>

                    <button class="btn btn-success" data-loading-text="Updating Template.." type="submit">Update Template</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
