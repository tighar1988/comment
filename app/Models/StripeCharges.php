<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StripeCharges extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stripe_charges';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'charge_id',
        'type',
        'status',
        'details',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'json',
    ];

    public function cardBrand()
    {
        return $this->details['card_brand'] ?? null;
    }

    public function cardLast4()
    {
        return $this->details['card_last_four'] ?? null;
    }

    public function cardError()
    {
        return $this->details['error'] ?? null;
    }

    public static function log($amount, $charge, $shopOwner)
    {
        $details = [
            'card_brand'     => ($shopOwner->card_brand ?? null),
            'card_last_four' => ($shopOwner->card_last_four ?? null),
            'charge'         => json_encode($charge),
        ];

        static::create([
            'amount'    => $amount,
            'charge_id' => $charge->id,
            'type'      => 'prepay-labels',
            'status'    => 'status',
            'details'   => $details,
        ]);
    }

    public static function logSubscription($amount, $charge, $shopOwner, $status = 'paid', $details = [])
    {
        $details = array_merge([
            'card_brand'     => ($shopOwner->card_brand ?? null),
            'card_last_four' => ($shopOwner->card_last_four ?? null),
            'charge'         => json_encode($charge),
        ], $details);

        static::create([
            'amount'    => $amount,
            'charge_id' => ($charge->id ?? null),
            'type'      => 'subscription',
            'status'    => $status,
            'details'   => $details,
        ]);
    }

    public function scopeLabels($query)
    {
        return $query->where('type', '=', 'prepay-labels')->orderBy('id', 'DESC');
    }

    public function scopeSubscription($query)
    {
        return $query->where('type', '=', 'subscription')->orderBy('id', 'DESC');
    }

    public static function lastPaidSubscription()
    {
        return static::where('type', '=', 'subscription')
            ->where('status', '=', 'paid')
            ->orderBy('id', 'DESC')
            ->first();
    }
}
