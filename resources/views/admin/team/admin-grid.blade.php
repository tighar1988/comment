@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Team</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Team</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">
                    <a href="/admin/team/add" class="btn btn-success add-btn btn-sm">Add Member</a>
                    <div class="table-responsive">
                        <table id="team-table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Team Member</th>
                                    <th>Email</th>
                                    <th>Permissions</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            @if ($user->permissions['all-permissions'] ?? null)
                                                All Permissions
                                            @else
                                                @foreach (($user->permissions ?? []) as $permission => $enabled)
                                                    @if ($enabled)
                                                        {{ $permissions[$permission] ?? null }} <br/>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="fa fa-ellipsis-h fa-lg"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                                    <li><a href="/admin/team/edit/{{ $user->id }}" class="link">Edit User</a></li>
                                                    <li><a href="/admin/team/delete/{{ $user->id }}" onclick="return confirm_delete(this);" class="link">Delete User</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#team-table').DataTable({
            "ordering": false,
            "info": false,
            "searching": true,
            "bStateSave": true,
            "oLanguage" : {"sEmptyTable": "No team members found."}
        });
    });
</script>
@endsection
