<div id="address-modal-overlay" style="display:none;"></div>

<div id="address-modal" style="display:none;">
    <p>Please enter your new address&hellip;</p>

    <?php $hasCountries = shop_setting('shop.has-multiple-countries', true); ?>
    <input type="text" placeholder="Street Address" value="{{ $customer->street_address }}" class="form-control" name="modal-street-address" id="modal-street-address">

    <input type="text" placeholder="Apartment/Unit Number" value="{{ $customer->apartment }}" class="form-control" name="modal-apartment" id="modal-apartment">

    <input type="text" placeholder="City" value="{{ $customer->city }}" class="form-control" name="modal-city" id="modal-city">

    @if ($hasCountries)
    <select class="form-control" name="modal-country" id="modal-country">
        <option value="" disabled selected>Country..</option>
        <option @if ($customer->country_code == 'US') selected="selected" @endif value="US">USA</option>
        <option @if ($customer->country_code == 'CA') selected="selected" @endif value="CA">Canada</option>
    </select>

    <select @if ($customer->country_code != 'CA') style="display: none;" @endif class="form-control" name="modal-state-text" id="modal-state-text">
        <option value="" disabled selected>State..</option>
        <option @if ($customer->state == 'AB') selected="selected" @endif value="AB">Alberta</option>
        <option @if ($customer->state == 'BC') selected="selected" @endif value="BC">British Columbia</option>
        <option @if ($customer->state == 'MB') selected="selected" @endif value="MB">Manitoba</option>
        <option @if ($customer->state == 'NB') selected="selected" @endif value="NB">New Brunswick</option>
        <option @if ($customer->state == 'NL') selected="selected" @endif value="NL">Newfoundland</option>
        <option @if ($customer->state == 'NT') selected="selected" @endif value="NT">Northwest Territories</option>
        <option @if ($customer->state == 'NS') selected="selected" @endif value="NS">Nova Scotia</option>
        <option @if ($customer->state == 'NU') selected="selected" @endif value="NU">Nunavut</option>
        <option @if ($customer->state == 'ON') selected="selected" @endif value="ON">Ontario</option>
        <option @if ($customer->state == 'PE') selected="selected" @endif value="PE">Prince Edward Island</option>
        <option @if ($customer->state == 'QC') selected="selected" @endif value="QC">Quebec</option>
        <option @if ($customer->state == 'SK') selected="selected" @endif value="SK">Saskatchewan</option>
        <option @if ($customer->state == 'YT') selected="selected" @endif value="YT">Yukon</option>
    </select>
    @endif

    <select @if ($hasCountries && $customer->country_code == 'CA') style="display: none;" @endif class="form-control" name="modal-state" id="modal-state">
        <option value="" disabled selected>State..</option>
        @include('common.states', ['selected' => $customer->state])
    </select>

    <input type="text" placeholder="Zip" value="{{ $customer->zip }}" class="form-control" name="modal-zip" id="modal-zip">

    <input type="text" placeholder="Phone Number (optional)" value="{{ $customer->phone_number }}" class="form-control" name="modal-phone-number" id="modal-phone-number">

    <button type="button" class="btn btn-close" onclick="closeAddressPopup();">Close</button>
    <button type="button" class="btn btn-save" data-loading-text="Updating.." onclick="saveNewAddress();">Update Address</button>
</div>
