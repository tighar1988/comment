<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\OrderProduct;
use App\Models\Order;
use Mockery;

class OrderProductTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(OrderProduct::class, new OrderProduct);
    }

    /** @test */
    public function it_returns_i_barcode()
    {
        $op = new OrderProduct;

        $this->assertEquals(null, $op->shopifyBarcode());
    }

    // /** @test */
    // public function it_returns_i_barcode_for_empty_shopify_barcode()
    // {
    //     $op = new OrderProduct;
    //     $op->inventory_id = 10;
    //     $op->shopify_variant_id = '1234567890';

    //     $inventory = Mockery::mock('App\Models\Inventory');
    //     $inventory->shouldReceive('getAttribute')->with('shopify_barcode')->andReturn('');

    //     $op = Mockery::mock($op);
    //     $op->shouldReceive('getAttribute')->with('variant')->andReturn($inventory);

    //     // dd($op->variant->shopify_barcode);

    //     $this->assertEquals('i-10', $op->barcodeValue());
    // }
}
