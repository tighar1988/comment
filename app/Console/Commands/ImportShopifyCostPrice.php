<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;

class ImportShopifyCostPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commentsold:import-shopify-cost-price {shop} {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the shopify cost price from the Shopify "Delirious Profit - Add cost price" app csv export. Example: php artisan commentsold:import-shopify-cost-price my-shop "shopify/products-cost-price.csv"';

    protected $validHeader = [
        0 => 'id',
        1 => 'product_name',
        2 => 'product_id',
        3 => 'title',
        4 => 'vendor',
        5 => 'product_type',
        6 => 'sku',
        7 => 'inventory_quantity',
        8 => 'price',
        9 => 'cost_price',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        reconnect_shop_database($this->argument('shop'));
        $file = $this->argument('file');
        $this->info("Trying to import product costs from {$file}.");

        $path = storage_path($this->argument('file'));
        if (! app('files')->exists($path)) {
            $this->error("The file {$path} does not exist.");
            return;
        }

        $csv = Reader::createFromPath($path);
        $header = $csv->fetchOne();
        if ($header != $this->validHeader) {
            $this->error("Invalid header format for file {$file}.");
            return;
        }

        $nrUpdated = 0;
        $rows = $csv->setOffset(1)->fetchAll();
        $bar = $this->output->createProgressBar(count($rows));

        foreach ($rows as $row) {

            $bar->advance(1);

            if (! isset($row[0]) || ! isset($row[9])) {
                continue;
            }

            $variantId = trim($row[0]);
            $costPrice = trim($row[9]);

            if (empty($costPrice) || ! is_numeric($costPrice)) {
                continue;
            }

            $inventory = app('App\Models\Inventory')->where('shopify_inventory_id', '=', $variantId)->first();
            if (is_null($inventory)) {
                continue;
            }

            // don't update if already set in Commentsold
            if (is_numeric($inventory->cost) && $inventory->cost > 0) {
                continue;
            }

            if ($costPrice > 0) {
                $inventory->cost = $costPrice;
                $inventory->save();
                $nrUpdated++;
            }
        }

        $bar->finish();
        $this->line('');
        $this->info("Product costs imported. {$nrUpdated} variants updated.");
    }
}
