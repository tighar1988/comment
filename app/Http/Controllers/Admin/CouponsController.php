<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CouponFormRequest;
use App\Models\Coupon;
use App\Models\ShopUser;
use Datatables;

class CouponsController extends Controller
{
    public function index()
    {
        return view('admin.coupons.coupon-grid');
    }

    /**
     * Search coupons for DataTable.
     */
    public function listCoupons()
    {
        $model = Coupon::query()->select(\DB::raw("coupons.*,
            (CASE
                WHEN starts_at != '0' && starts_at > UNIX_TIMESTAMP() THEN 0
                WHEN expires_at != '0' && expires_at < UNIX_TIMESTAMP() THEN 0
                WHEN max_usage != '0' && use_count >= max_usage THEN 0
                ELSE 1
            END) AS is_active,
            COALESCE((SELECT COUNT(1) FROM orders WHERE orders.coupon = coupons.code), 0) as order_count
        "));

        if (! request()->has('order')) {
            $model->orderBy('coupons.id', 'desc');
        }

        $now = time();
        return Datatables::eloquent($model)
                ->addColumn('now', $now)
                ->addColumn('starts_at_date', '@if ($starts_at != 0) {{ style_date($starts_at) }} @endif')
                ->addColumn('expires_at_date', '@if ($expires_at == 0) Never @else {{ style_date($expires_at) }} @endif')
                ->editColumn('code', '{{ strtoupper($code) }}')
                ->editColumn('amount', '{{ amount($amount) }}')
                ->make(true);
    }

    public function create()
    {
        $customer = null;
        if (request('customer')) {
            $customer = ShopUser::find(request('customer'));
        }

        return view('admin.coupons.coupon-add', compact('customer'));
    }

    public function store(CouponFormRequest $request)
    {
        $coupon = new Coupon($request->input());

        $onlyOnce = false;
        if ($request->has('once_per_user') && $request->input('once_per_user') == '1') {
            $onlyOnce = true;
        }

        $minPurchaseAmount = null;
        if ($request->has('minimum_purchase') && $request->input('minimum_purchase') == '1') {
            $minPurchaseAmount = $request->input('minimum_purchase_amount');
        }

        if ($request->has('customer_id')) {
            // coupon for only a specific user
            $coupon->options = [
                'userId'      => $request->input('customer_id'),
                'userName'    => $request->input('customer_name'),
                'description' => $request->input('description'),
                'onlyOnce'    => $onlyOnce,
                'minPurchaseAmount' => $minPurchaseAmount,
            ];
        } elseif ($request->has('new_users_period') && $request->input('new_users_period') == '1') {
            // coupon for the new users, that can be used only in the first days after registering
            $coupon->options = [
                'newUsersPeriod' => $request->input('days_valid'),
                'description'    => $request->input('description'),
                'onlyOnce'       => $onlyOnce,
                'minPurchaseAmount' => $minPurchaseAmount,
            ];
        } else {
            $coupon->options = [
                'description' => $request->input('description'),
                'onlyOnce'    => $onlyOnce,
                'minPurchaseAmount' => $minPurchaseAmount,
            ];
        }

        $coupon->saveCoupon();

        flash('Coupon Added!');
        return redirect('/admin/coupons');
    }

    public function destroy($id)
    {
        Coupon::destroy($id);
        flash('Coupon Deleted!');
        return redirect('/admin/coupons');
    }
}
