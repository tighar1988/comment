<h3 class="subheading">Return - Products In The Order</h3>

<div class="table-responsive">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
        <tr>
            <th>Image</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Size</th>
            <th>Color</th>
            <th>Manage</th>
        </tr>
        @if ($order->orderProducts->isEmpty())
            <tr><td colspan="6">No products in this order.</td></tr>
        @endif

        @foreach($order->orderProducts as $item)
            <tr>
                <td>
                    <img class="img-circle" width="100px" src="{{ product_image($item->product_filename) }}" alt="{{ $item->prod_name }}">
                </td>
                <td>
                    <a href="/admin/products/{{ $item->product_id }}/variants">{{ $item->prod_name }}</a>
                </td>
                <td>${{ amount($item->price) }}</td>
                <td>{{ $item->size }}</td>
                <td>{{ $item->color }}</td>
                <td>
                    @if ($item->returned_date > 0)
                        <span>Returned on {{ style_date($item->returned_date) }}</span><br/>

                        @if (is_null($item->refund_issued))
                            <button onclick="refundAsAccountCredit({{ $item->id }}, this)" class="btn btn-default btn-sm">Refund as Account Credit (${{ amount($item->getRefundAmount($order)) }})</button>
                        @else
                            <br/> Refunded as account credit ${{ amount($item->refund_issued) }}
                        @endif

                    @else
                        <button class="btn btn-danger btn-sm" data-loading-text="Returning Product.." onclick="returnProduct({{ $item->id }}, this)">Return this product</button>

                        <button onclick="refundAsAccountCredit({{ $item->id }})" class="btn btn-default btn-sm refund-as-credit" style="display: none">Refund as Account Credit (${{ amount($item->getRefundAmount($order)) }})</button>
                        <div class="item-location" style="display: none;"></div>
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>

<div class="row">
    <div class="col-sm-12">
        <button type="button" class="btn btn-default m-r-10" onclick="closeDetails();">Cancel</button>
    </div>
</div>

