@extends('layouts.app')

@section('content')
<div class="login-box">
    <div class="login-box-body">
        <div class="login-logo">
            <a href="/"><img src="{{ cdn('/images/login-logo.png') }}" alt="CommentSold"></a>
        </div>
        <p class="login-box-msg">Register</p>
        <form role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}

            <div class="form-group has-feedback @hasError('name')">
                <input type="text" name="name" value="{{ old('name') }}" required autofocus class="form-control" placeholder="Name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @error('name')
            </div>

            <div class="form-group has-feedback @hasError('email')">
                <input type="email" name="email" value="{{ old('email') }}" required autofocus class="form-control" placeholder="E-Mail Address">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @error('email')
            </div>

            <div class="form-group has-feedback @hasError('password')">
                <input type="password" class="form-control" name="password" required placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password')
            </div>

            <div class="form-group has-feedback @hasError('password_confirmation')">
                <input type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @error('password_confirmation')
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button onClick="fbq('trackCustom', 'Register Click Inline');" type="submit" class="btn btn-primary btn-block btn-flat m-b-20">Register</button>
                </div>
            </div>
        </form>
        <a href="{{ url('/login') }}">Login</a><br>
    </div>
</div>
@include('common.facebook-pixel')
@endsection
