<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metafield extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'metafields';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'namespace',
        'key',
        'value',
        'value_type',
        'description',
        'owner_resource',
        'owner_id',
        'shopify_owner_id',
        'shopify_metafield_id',
    ];

    public function scopeNamespace($query, $namespace)
    {
        return $query->where('namespace', '=', $namespace);
    }

    public function scopeOwnerResource($query, $ownerResource)
    {
        return $query->where('owner_resource', '=', $ownerResource);
    }
}
