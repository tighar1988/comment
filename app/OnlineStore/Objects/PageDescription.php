<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class PageDescription extends Drop
{
    public function __toString()
    {
        if (! isset($this->cache['page_description'])) {
            $this->cache['page_description'] = shop_setting('online-store.page_description', 'Homepage meta description');
        }

        return $this->cache['page_description'];
    }
}
