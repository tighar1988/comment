@extends('admin.setup.facebook-setup')

@section('section', 'Facebook Messenger')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body lr-m-15">

                <h3 style="margin-top: 0px;">Messenger</h3>

                The Facebook Messenger requires a Facebook Page.<br/><br/>
                @if (empty($pages))
                    You don't have access to any pages with your Facebook account. Visit <a href="https://www.facebook.com/">Facebook</a> and create a Facebook page.
                @else
                    Below is a list of pages in your account. Please choose what Page to use for the Facebook Messenger. Only one page can be used.
                    <br/><br/>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Page</th>
                                        <th>Category</th>
                                        <th>Manage</th>
                                    </tr>
                                    @foreach ($pages as $page)
                                        <tr>
                                            <td style="width:33%;">
                                                <a class="" href="https://www.facebook.com/{{ $page['id'] }}" target="_blank">{{ $page['name'] }}</a>
                                                @if ($page['id'] == $pageId)
                                                    &nbsp;<strong>(Enabled)</strong>
                                                @endif
                                            </td>
                                            <td style="width:33%;">{{ $page['category'] }}</td>
                                            <td style="width:33%;">
                                                @if ($page['id'] == $pageId)
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/facebook-setup/messenger/disable-page">
                                                        <input type="hidden" name="page_id" value="{{ $page['id'] }}">
                                                        <button type="submit" data-loading-text="Disabling Page.." class="btn btn-danger">Disable Page</button>
                                                    </form>
                                                @else
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/facebook-setup/messenger/enable-page">
                                                        <input type="hidden" name="page_id" value="{{ $page['id'] }}">
                                                        <button type="submit" data-loading-text="Enabling Page.." class="btn btn-default">Enable Page</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <span class="settings-info">Automated Page Response</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="messenger.send-contact-message" @if(shop_setting('messenger.send-contact-message', false)) checked="" @endif class="toggle-switch"></label></div>

                <span class="settings-muted text-muted">
                    If enabled, when a customer writes a message to your Facebook page, he will get this automated response:
                    "Hello, please contact {{ shop_email() }} for support. Thank you!".
                </span>
            </div>
        </div>
    </div>
</div>
@endsection
