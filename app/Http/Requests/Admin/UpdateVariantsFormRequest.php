<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdateVariantsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        foreach ($this->all() as $key => $value) {
            if (starts_with($key, 'w-')) {
                $rules[$key] = 'required|numeric';

            } elseif (starts_with($key, 'c-')) {
                $rules[$key] = 'required|numeric|min:1';

                if (shop_id() == 'shopentourageclothing') {
                    $rules[$key] = 'required|numeric|min:0';
                }

                if ($this->product_type == 'giftcard') {
                    $rules[$key] = 'required|numeric|min:0';
                }

            } elseif (starts_with($key, 'r-')) {
                $rules[$key] = 'required|numeric|min:1';

                if (shop_id() == 'shopentourageclothing') {
                    $rules[$key] = 'required|numeric|min:0';
                }

            } elseif (starts_with($key, 's-')) {
                // sale price
                $rules[$key] = 'nullable|numeric|min:0';

            } elseif (starts_with($key, 'l-')) {
                $rules[$key] = 'nullable';
            }
        }

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    protected function formatErrors(Validator $validator)
    {
        $errors = $validator->errors()->all();

        foreach ($errors as $key => $value) {
            $value = preg_replace('/w-\d+/', 'weight', $value);
            $value = preg_replace('/c-\d+/', 'cost', $value);
            $value = preg_replace('/r-\d+/', 'price', $value);
            $value = preg_replace('/l-\d+/', 'location', $value);

            $errors[$key] = $value;
        }

        return array_unique($errors);
    }
}
