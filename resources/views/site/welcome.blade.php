<!DOCTYPE html>
<html lang="en">
<head>
    @include('site.web-header')
</head>
<body id="page-top" class="index">
    @include('site.nav')

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="intro-text">
                        <div onClick="fbq('trackCustom', 'Register Click');" class="intro-heading">Stop Manually Sending Invoices<br>
                            The Future Is Here
                        </div>
                        <a onClick="fbq('trackCustom', 'Register Click');" href="/register" class="btn btn-xl">Get Started</a>
                        <a href="#" onclick="Intercom('showNewMessage', 'Hey! I had a question about ... ')" class="btn btn-xl btn-outline">Ask Question</a>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <div class="embed-responsive embed-responsive-16by9">
                    <iframe src="https://player.vimeo.com/video/219155594" width="623" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Features -->
        <section id="services">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-4">
                        <i class="fa fa-newspaper-o fa-3x" aria-hidden="true"></i>
                        <h4 class="service-heading">Automated Invoicing</h4>
                        <p>Send invoices to your customers seconds after they comment, all day, 24/7/365</p>
                    </div>
                    <div class="col-md-4">
                        <i class="fa fa-calendar fa-3x" aria-hidden="true"></i>
                        <h4 class="service-heading">Scheduled Posting</h4>
                        <p>Setup your new product posts days at a time so you never miss a date!</p>
                    </div>
                <div class="col-md-4">
                    <i class="fa fa-file-text-o fa-3x" aria-hidden="true"></i>
                    <h4 class="service-heading">Inventory Management</h4>
                    <p>Sell through Facebook (Groups/Pages), Instagram, Website and handle returns - all with accurate inventory and reporting!</p>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>15-Day Free Trial, Try before you buy it!</h2>
                    <a onClick="fbq('trackCustom', 'Register Click');" href="/register" class="page-scroll btn btn-xl">Get Started</a>
                </div>
            </div>
        </div>
    </section>
    <div><img src="{{ cdn('images/right-pointer.png') }}" alt="" class="img-responsive"></div>

    <!-- Comments -->
    <section class="blue-bg comments">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="section-heading">Comment. Sold. Earn.</h2>
                    <p class="text">CommentSold enables businesses to automate all the tedious tasks involved with running an E-commerce business.<br><br>
                        Automate your business today with scheduled product posts to facebook. Let users purchase products by commenting on the item - nothing more!
                        <br><br>Start your free trial now!
                    </p>
                </div>
                <div class="col-lg-6 text-center counter">
                    <h3>Comments Processed</h3>
                    <h1 class="counter-wrapper"><span class="fbcomts"></span></h1>
                    <img src="{{ cdn('images/comments.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <!-- Testimonials -->
    <section class="testi">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Testimonials</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="testimonial">
                        <img src="{{ cdn('images/1.jpg') }}" class="img-responsive img-circle" alt="">
                        <p><i>"CommentSold helped us scale by spending less time invoicing and more time selling!<br><br>We've grown to over a thousand orders a day with CommentSold!<br><br>They are extremely quick to respond and fix any issues, very professional. I have never dealt with a group that was so efficient and kind"</i></p>
                        <a href="https://www.facebook.com/groups/738452322896604/"><h4>Amanda Halpin-Kruse</h4></a>
                        <a href="https://www.facebook.com/groups/738452322896604/"><p>Discount Divas</p></a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="testimonial">
                        <img src="{{ cdn('images/mary.png') }}" class="img-responsive img-circle" alt="">
                        <p><i>"CommentSold has saved me hours of work and headache. I would send invoices and customers would often call or stop by the store and pay or not pay at all. It created a lot of confusion.<br><br>Now, everything is in one place. I have inventory, invoicing and shipping in one place.<br>If someone doesn't pay the invoice it is gone.<br>So simple!"</i></p>
                        <a href="https://www.facebook.com/groups/1489766108016881/"><h4>Mary Heatherly</h4></a>
                        <a href="https://www.facebook.com/groups/1489766108016881/"><p>Proverbs Woman Design</p></a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="testimonial">
                        <img src="{{ cdn('images/stephanie.png') }}" class="img-responsive img-circle" alt="">
                        <p><i>"We are loving CommentSold. It has given us the opportunity to spend more time on our customers and growing our boutique.<br><br>It is super user friendly, they have seriously thought of everything and if you have questions their customer support is amazing!"</i></p>
                        <a href="https://www.facebook.com/groups/Goldfishboutique/"><h4>Stephanie Olson</h4></a>
                        <a href="https://www.facebook.com/groups/Goldfishboutique/"><p>Goldfish Boutique</p></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <div class="text-right"><img src="{{ cdn('images/left-pointer.png') }}" alt="" class="img-responsive left-arrow"></div>
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Have any question or need help?</h2>
                    <p class="text">Contact our cutomer support team if you have any further questions.<br>
                        We are here to help you out!
                    </p>
                    <a href="/contact" class="page-scroll btn btn-xl">Contact Us</a>
                </div>
            </div>
        </div>
    </section>

    @include('site.footer')
</body>
</html>
