@extends('layouts.admin-setup')

@section('content')
<div class="row">
    <div class="form-group col-md-4 col-md-offset-4">
        <label for="shop-name">Your shop's subdomain url? (shorter is better)</label><br/>
        <div class="input-group">
            <span class="input-group-addon">https://</span>
            <input type="text" class="form-control" name="shop_name" maxlength="30" id="shop-name" placeholder="myshop">
            <span class="input-group-addon">.commentsold.com</span>
        </div>

        <small class="text-muted">This will be used for your site url, ex: myshop.commentsold.com</small><br/>
        <small class="text-muted">The shop url <b>cannot</b> be renamed later.</small>

        <span class="help-block"><strong class="shop-error-message"></strong></span>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">
        <button type="button" id="create-shop" class="btn btn-success btn-lg" data-loading-text="Creating Shop...">Ok, let's do this!</button>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        $('#shop-name').keypress(function (e) {
            if (e.which == 13) {
                $('#create-shop').trigger('click');
                return false;
            }
        });

        $('#create-shop').click(function() {
            $('#create-shop').button('loading');
            $.ajax({
                type: "POST",
                url: '/admin/setup/step-1',
                data: {
                    shop_name: $('#shop-name').val()
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $('#create-shop').text('Shop Created...');
                        window.location.href = '/admin/setup/step-2';
                    } else {
                        $('.shop-error-message').text('Sorry, something went wrong.');
                        $('#create-shop').button('reset');
                    }
                },
                error: function(data) {
                    var message = 'An error occurred.';
                    try { message = data.responseJSON.shop_name[0]; } catch (e) {}
                    $('.shop-error-message').text(message);
                    $('#create-shop').button('reset');
                }
            });
        });
    });
</script>
@endsection
