<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RemindFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'remind' => 'required|numeric|min:1|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'remind.required' => 'The remind field is required.',
            'remind.numeric'  => 'The remind field must be numeric.',
            'remind.min'      => 'The remind field must be at least 1 hour.',
        ];
    }
}
