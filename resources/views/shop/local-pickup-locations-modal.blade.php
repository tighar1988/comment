<div class="modal active fade" id="local-pickup-locations-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="">
                <div class="modal-header">
                    <h4 class="modal-title">Please choose a local pickup location</h4>
                </div>
                @php
                    $locations = \App\Models\MultipleStoreLocation::all();
                @endphp
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Store Locations</label>
                        <br/><br/>
                        <select id="location-select" class="form-control">
                            @foreach($locations as $location)
                                <option @if($location->isDefault()) selected @endif value="{{ $location->id }}">{{ $location->address }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="updateLocalPickupStoreLocation();" data-loading-text="Saving..." class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
