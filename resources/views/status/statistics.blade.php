@extends('status.app-status')

@section('content')
<div class="container">
    @if ($nrJobsLocked > 0)
        <div class="alert status-alert alert-danger">
            There are {{ $nrJobsLocked }} locked <strong>MASTER</strong> jobs (locked for more than 7 minutes).
            <form method="POST" action="/app-status/unlock-jobs/harvest" class="pull-right loading-btn">
                <button data-loading-text="Processing. This may take a minute.." type="submit" class="btn btn-sm btn-default">Unlock Jobs</button>
            </form>
        </div>
    @endif

    Queue Size: <strong>{{ $queueSize }}</strong><br/><br/>
    Delay Size: <strong>{{ $delaySize }}</strong><br/><br/>
    <table class="table table-hover table-bordered">
        <thead>
            <th>Shop</th>
            <th>Name</th>
            <th>Plan</th>
            <th>Locked</th>
            <th>Total Orders</th>
            <th>Total Revenue</th>
            <th>CS Ship Charged</th>
            <th>Options</th>
        </thead>
        <tbody>
            <tr class="active">
                <td>Total</td>
                <td></td>
                <td>${{ amount($MRR) }}</td>
                <td></td>
                <td>{{ $totalOrders }}</td>
                <td>${{ amount($totalRev) }}</td>
                <td>${{ amount($totalShipCharged) }} / ${{ amount($totalCsShipCharged) }}<br>
                <span class="label label-success">${{ amount($totalShipProfit) }}</span></td>
                <td></td>
            </tr>
            @foreach ($shops as $shop)
                <tr @if(! $shop['isEnabled']) class="warning" @elseif($shop['fbDisconnected']) class="danger" @endif>
                    <td>{{ $shop['shop'] }}</td>
                    <td>{{ $shop['name'] }}</td>
                    <td>
                        {{ $shop['plan'] }}
                        @if ($shop['trialDaysLeft'] > 0)
                            <br/>
                            <span class="label label-warning">Free Trial <small class="text-muted">{{ $shop['trialDaysLeft'] }} days left</small></span>
                            <br>{{ $shop['source'] }}
                        @endif
                    </td>
                    <td>
                        @if ($shop['lockedTime'] > 2)
                            {{ $shop['lockedTime'] }} min</td>
                        @endif
                    </td>
                    <td>{{ $shop['totalOrders'] }}</td>
                    <td>${{ amount($shop['totalRevenue']) }}</td>
                    <td>${{ amount($shop['shipCharged']) }} / ${{ amount($shop['csShipCharged']) }}</td>
                    <td>
                        @if ($shop['fbDisconnected'])
                            <div class="pull-left">[FB Disconnected]</div>
                        @else
                            <form method="POST" data-shop="{{ $shop['shop'] }}" action="/app-status/shop/{{ $shop['shop'] }}/disconnect" class="disconnect-shop pull-left">
                                <button data-loading-text="Disconnecting Facebook.." type="submit" title="Disconnect Facebook" class="btn btn-xs btn-danger"><i class="fa fa-facebook" aria-hidden="true"></i></button>
                            </form>
                        @endif

                        @if ($shop['isEnabled'])
                            <form method="POST" data-shop="{{ $shop['shop'] }}" action="/app-status/shop/{{ $shop['shop'] }}/disable" class="disable-shop pull-left">
                                <button data-loading-text="Disabling.." type="submit" title="Disable" class="btn btn-xs btn-info"><i class="fa fa-lock" aria-hidden="true"></i></button>
                            </form>
                        @else
                            <form method="POST" data-shop="{{ $shop['shop'] }}" action="/app-status/shop/{{ $shop['shop'] }}/enable" class="enable-shop pull-left">
                                <button data-loading-text="Enabling.." type="submit" title="Enable" class="btn btn-xs btn-success"><i class="fa fa-unlock-alt" aria-hidden="true"></i></button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.disconnect-shop').submit(function() {
            if (confirm("Are you sure to disconnect Facebook for '"+$(this).attr('data-shop')+"'?")) {
                $('button[type="submit"]', $(this)).button('loading');
                return true;
            } else {
                return false;
            }
        });

        $('.disable-shop').submit(function() {
            if (confirm("Are you sure to disable jobs for '"+$(this).attr('data-shop')+"'?")) {
                $('button[type="submit"]', $(this)).button('loading');
                return true;
            } else {
                return false;
            }
        });

        $('.enable-shop').submit(function() {
            if (confirm("Are you sure to enable jobs for '"+$(this).attr('data-shop')+"'?")) {
                $('button[type="submit"]', $(this)).button('loading');
                return true;
            } else {
                return false;
            }
        });
    });

</script>
@endsection
