<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\FacebookConnectFormRequest;
use App\Http\Requests\Admin\SetFacebookGroupFormRequest;
use App\Http\Requests\Admin\SetFacebookPageFormRequest;
use App\Jobs\HandleFacebookWebhook;
use App\Jobs\HandleFacebookMessage;
use Facebook;
use Redis;
use Log;
use Exception;

class FacebookController extends Controller
{
    /**
     * Facebook Webhooks
     *
     * @return \Illuminate\Http\Response
     */
    public function incoming(Request $request)
    {
        // Validate the webhook
        if ($request->has('hub_challenge') && $request->input('hub_verify_token') == 'cstoken_432352045345') {
            return $request->input('hub_challenge');
        }

        // Ignore reading updates or our responses
        if (! $request->has('entry')) {
            return;
        }

        // Make sure this is a page webhook
        if ($request->input('object') !== 'page') {
            return;
        }

        $input = $request->input();
        if (isset($input['entry'][0]['changes'])) {
            dispatch(new HandleFacebookWebhook($request->all()));
        } else {
            dispatch(new HandleFacebookMessage($request->all()));
        }

        // try {
        //     (new HandleFacebookWebhook($request->all()))->handle();
        // } catch (Exception $e) {
        //     Log::error($e);
        // }
    }

    public function index()
    {
        if (! empty(shop_setting('facebook.user_id'))) {
            $enabledGroups = fb_enabled_group_ids();
            $groups = Facebook::getMerchantGroups();
        } else {
            $groups = [];
            $enabledGroups = [];
        }

        return view('admin.setup.facebook-groups', compact('groups', 'enabledGroups'));
    }

    public function pages()
    {
        if (! empty(shop_setting('facebook.user_id'))) {
            $pageId = shop_setting('facebook.page.page_id');
            $pages = Facebook::getMerchantPages();
        } else {
            $pageId = null;
            $pages = [];
        }

        return view('admin.setup.facebook-pages', compact('pages', 'pageId'));
    }

    public function messenger()
    {
        if (! empty(shop_setting('facebook.user_id'))) {
            $pageId = fb_page_id();
            $pages = Facebook::getMerchantPages();
        } else {
            $pageId = null;
            $pages = [];
        }

        return view('admin.setup.facebook-messenger', compact('pageId', 'pages'));
    }

    public function connectFacebook(FacebookConnectFormRequest $request)
    {
        // exchange short lived token for long lived token
        $token = Facebook::getLongedLivedToken($request->input('access_token'));
        $name = Facebook::getName($token);
        $facebookId = $request->input('user_id');

        // if it's a new facebook id, remove the old facebook groups
        $currentId = shop_setting('facebook.user_id');
        if (! empty($currentId) && $currentId != $facebookId) {
            shop_setting_set('facebook.group_ids', null);
            shop_setting_set('facebook.share-group-id', null);
        }

        // save the facebook settings
        shop_setting_set('facebook.user_id', $facebookId);
        shop_setting_set('facebook.name', $name);
        shop_setting_set('facebook.access_token', $token);
        shop_setting_set('facebook.time-connected', time());

        return response()->json(['status' => 1]);
    }

    public function enableGroup(SetFacebookGroupFormRequest $request)
    {
        $enabledGroups = fb_enabled_group_ids();
        $enabledGroups[] = request('group_id');
        shop_setting_set('facebook.group_ids', json_encode($enabledGroups));
        flash('Group Enabled!');
        return back();
    }

    public function disableGroup(SetFacebookGroupFormRequest $request)
    {
        $enabledGroups = fb_enabled_group_ids();

        if (($key = array_search(request('group_id'), $enabledGroups)) !== false) {
            unset($enabledGroups[$key]);
        }

        shop_setting_set('facebook.group_ids', json_encode($enabledGroups));
        flash('Group Disabled!');
        return back();
    }

    public function setShareGroup(SetFacebookGroupFormRequest $request)
    {
        shop_setting_set('facebook.share-group-id', $request->input('group_id'));
        flash('Sharing Enabled!');
        return back();
    }

    public function enableMessengerPage(SetFacebookPageFormRequest $request)
    {
        $pageId = $request->input('page_id');
        $pages = Facebook::getMerchantPages();

        foreach ($pages as $page) {
            if ($pageId == $page['id']) {

                if (! Facebook::whitelistDomain(shop_url(), $page['access_token'])) {
                    flash('Sorry, an error occured!');
                    return back();
                }

                if (! Facebook::subscribeAppToPage($page['access_token'])) {
                    flash('Sorry, could not connect your page. An error occured!');
                    return back();
                }

                // update the database
                shop_setting_set('facebook.page_id', $page['id']);
                shop_setting_set('facebook.page_category', $page['category']);
                shop_setting_set('facebook.page_name', $page['name']);
                shop_setting_set('facebook.page_access_token', $page['access_token']);

                // update the Redis cache
                Redis::connection('cache')->set("page:{$pageId}", shop_id());

                flash('Page Enabled!');
                return back();
            }
        }

        flash('Sorry, we could not find that page!');
        return back();
    }

    public function disableMessengerPage(SetFacebookPageFormRequest $request)
    {
        shop_setting_set('facebook.page_id', null);
        shop_setting_set('facebook.page_access_token', null);
        flash('Page Disabled!');
        return back();
    }

    public function enablePage(SetFacebookPageFormRequest $request)
    {
        $pageId = $request->input('page_id');
        $pages = Facebook::getMerchantPages();

        foreach ($pages as $page) {
            if ($pageId == $page['id']) {

                // update the database
                shop_setting_set('facebook.page.page_id', $page['id']);
                shop_setting_set('facebook.page.page_category', $page['category']);
                shop_setting_set('facebook.page.page_name', $page['name']);
                shop_setting_set('facebook.page.page_access_token', $page['access_token']);

                Facebook::subscribePageWebhook();

                // update the Redis cache
                Redis::connection('cache')->set("page:{$pageId}", shop_id());

                flash('Page Enabled!');
                return back();
            }
        }

        flash('Sorry, we could not find that page!');
        return back();
    }

    public function disablePage(SetFacebookPageFormRequest $request)
    {
        shop_setting_set('facebook.page.page_id', null);
        shop_setting_set('facebook.page.page_access_token', null);

        Facebook::unsubscribePageWebhook();

        flash('Page Disabled!');
        return back();
    }
}
