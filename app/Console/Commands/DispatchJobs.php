<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ShopMasterJob;

class DispatchJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commentsold:dispatch-jobs {job?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch the master job for all shops.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = $this->argument('job');
        if (is_null($job)) {
            $job = ShopMasterJob::class;
        }

        if (! class_exists($job)) {
            $this->error("The '{$job}' job class does not exist.");
            return;
        }

        $this->info("Dispatching {$job} jobs for all shops..");
        $startTime = microtime(true);

        $shops = app('App\Models\Master')->enabledShops();
        $index = 1;
        $count = count($shops);

        // delay in microconds; 60 seconds, minus 2 seconds for every 100 shops
        $delay = (60 - 2 * intval($count / 100)) * 1000000 / $count;

        foreach ($shops as $shop) {
            log_master_info("[dispatch-jobs][{$index}/{$count}] Dispatching {$job} job for a queue. Shop '{$shop}'");
            dispatch(new $job($shop));
            $index++;

            // even out master dispatches over the full 60 seconds
            usleep($delay);
        }

        log_master_info("[dispatch-jobs] Took (" . elapsed_time($startTime) . ") seconds to complete {$job} job queue push");
        $this->info("Shop {$job} jobs dispatched.");
    }
}
