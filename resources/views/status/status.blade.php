@extends('status.app-status')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            User Statistics
        </div>
        <div class="panel-footer">
            Total Users: <strong>{{ $totalUsers }}</strong>
            <br/><br/>

            Active: <strong>{{ $active }}</strong>
            <br/><br/>

            Super Admins: <strong>{{ $superAdmins }}</strong>
            <br/><br/>

            On step 1: <strong>{{ $onStepOne }}</strong> <span class="text-muted">(have not created a shop)</span>
            <br/><br/>

            On step 2: <strong>{{ $onStepTwo }}</strong> <span class="text-muted">(Card setup/domain setup)</span>
            <br/><br/>

        </div>
    </div>
</div>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            Login as User
        </div>
        <div class="panel-footer">
            @if ($users->isEmpty())
                There are no users.
            @endif

            <form class="" role="form" method="POST" action="/app-status/login-as">
                <div class="form-group">
                    <label>User</label>
                    <select class="form-control select2-users" name="user">
                        <option></option>
                        @foreach ($users as $user)
                            <option @if (Auth::user() && Auth::user()->id == $user->id) selected="selected" @endif value="{{ $user->id }}">{{ $user->name }} ({{ $user->email }} - {{ $user->shop_name }})</option>
                        @endforeach
                    </select>
                    <br/><br/>
                    <button type="submit" class="btn btn-success">Login as User</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2-users').select2({
            placeholder: "Select a user"
        });
    });
</script>
@endsection
