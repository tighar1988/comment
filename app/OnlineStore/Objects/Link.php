<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;
use App\Models\MenuItem;

class Link extends Drop
{
    protected $title;

    protected $type;

    protected $link;

    public function __construct(MenuItem $menuItem)
    {
        $this->title = $menuItem->name;
        $this->type = 'collection_link';
        $this->link = $menuItem->link;
    }

    public function handle()
    {
        $handle = str_replace('collections/', '', $this->link);
        $handle = trim($handle, '/ ');

        $handle = preg_replace("/[^a-zA-Z0-9-]+/", "-", $handle);
        $handle = trim($handle, '-');

        return $handle;
    }

    /**
     * Returns true if the link is active, or false if the link is inactive.
     *
     * If you are on a product page that is collection-aware, link.active will return true for
     * both the collection-aware product URL and the collection-agnostic URL. For example, if
     * you have a link whose URL points to:
     *
     * /products/awesome-product
     *
     * link.active will return true for the following URL, which links to the same product but through a collection:
     *
     * /collections/awesome-collection/products/awesome-product
     *
     * If you are on a collection page filtered with tags, and the link points to the unfiltered collection page, link.active will return true.
     *
     * If you are on an article page and your link points to the blog,  link.active will return true.
     */
    public function active()
    {
        if (empty(CurrentView::$currentPath)) {
            return false;
        }

        $path = trim(CurrentView::$currentPath, '/ ');
        $link = trim($this->link, '/ ');
        if (str_contains($link, $path) || ($link == $path)) {
            return true;
        }

        return false;
    }

    /**
     * Returns the variable associated to the link. The possible types are:
     * product
     * collection
     * page
     * blog
     *
     * Through link.object, you can access any of the attributes that are available in the above three variables.
     *
     * Input
     * <!-- If the product links to a product with a price of $10 -->
     * {{ link.object.price | money }}
     *
     * Output
     * $10
     */
    public function object()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the title of the link.
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * Returns the type of the link. The possible values are:
     *
     * collection_link: if the link points to a collection
     * product_link: if the link points to a product page
     * page_link: if the link points to a page
     * blog_link: if the link points to a blog
     * relative_link: if the link points to the search page, the home page or /collections/all
     * http_link: if the link points to an external web page, or a type or vendor collection (ex: /collections/types?q=Pants)
     */
    public function type()
    {
        // todo: implement
        return $this->type;
    }

    /**
     * Returns the URL of the link.
     */
    public function url()
    {
        if (str_contains($this->link, 'http')) {
            return $this->link;
        }

        return shop_url($this->link);
    }
}
