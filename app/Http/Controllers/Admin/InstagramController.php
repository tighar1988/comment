<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Instagram\Instagram;
use App\Models\Product;
use App\Models\InstagramPost;
use App\Models\Inventory;
use App\Jobs\HandleInstagramWebhook;
use Log;
use Redis;
use App\Mails\ShopMailer;
use Datatables;

class InstagramController extends Controller
{
    /**
     * Instagram Webhook
     *
     * @return \Illuminate\Http\Response
     */
    public function incoming(Request $request)
    {
        // Log::info($request->all());

        // validate the webhook
        if ($request->has('hub_challenge') && $request->input('hub_verify_token') == 'token38234293') {
            return $request->input('hub_challenge');
        }

        dispatch(new HandleInstagramWebhook($request->all()));
        // (new HandleInstagramWebhook($request->all()))->handle();
    }

    /**
     * Redirect shop Admin to Instagram.
     */
    public function connect()
    {
        return (new Instagram)->redirect();
    }

    /**
     * Instagram callback for shop Admin. For normal shop users it redirects them to the shop callback.
     */
    public function callback(Request $request)
    {
        if ($request->has('shop')) {
            // redirect for the individual shop
            set_shop_database($request->get('shop'));
            $url = shop_url('/instagram-callback?'.$request->getQueryString());
            return redirect($url);

        } else {
            // redirect for admin
            return redirect(url('/admin/setup/instagram-callback').'?'.$request->getQueryString());
        }
    }

    public function adminCallback(Request $request)
    {
        if (! $request->has('code') || $request->has('denied')) {
            return redirect('/admin/setup/instagram');
        }

        $user = (new Instagram)->user();

        shop_setting_set('instagram.token', $user->token);
        shop_setting_set('instagram.id', $user->getId());
        shop_setting_set('instagram.nickname', mysql_utf8($user->getNickname()));
        shop_setting_set('instagram.name', mysql_utf8($user->getName()));
        shop_setting_set('instagram.email', $user->getEmail());
        shop_setting_set('instagram.avatar', $user->getAvatar());

        // update the Redis cache
        $instagramId = $user->getId();
        Redis::connection('cache')->set("instagramId:{$instagramId}", shop_id());

        flash('Instagram Connected!');
        return redirect('/admin/setup/instagram');
    }

    public function posts()
    {
        try {
            $posts = (new Instagram)->mediaRecent();
            $connected = InstagramPost::with('product.inventoryQuantity')->get();

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $posts = [];
            $connected = false;

            ShopMailer::notifyAdmin($e);
            log_error("[instagram][posts] $e");
        }

        return view('admin.instagram.instagram-posts', compact('posts', 'connected'));
    }

    public function linkToProduct()
    {
        $product = Product::find(request('product_id'));
        if (is_null($product)) {
            flash('The product you are trying to link no longer exists.', 'danger');
            return back();
        }

        $post = (new Instagram)->mediaById(request('post_id'));
        if (! isset($post['id'])) {
            flash('The post could not be linked.', 'danger');
            return back();
        }

        if (InstagramPost::where('instagram_id', '=', $post['id'])->exists()) {
            flash('The post was already linked to a product.');
            return back();
        }

        InstagramPost::create([
            'product_id'   => $product->id,
            'instagram_id' => $post['id'],
            'created_time' => $post['created_time'] ?? null,
            'image'        => $post['images']['thumbnail']['url'] ?? null,
            'post_data'    => $post,
        ]);

        flash('The post was linked to the product.');
        return back();
    }

    /**
     * Search products for DataTable.
     */
    public function listProducts()
    {
        $model = Product::query()->select(\DB::raw("products.id, products.product_name, products.style,
            (SELECT images.filename FROM images WHERE images.is_main = true AND products.id = images.product_id LIMIT 1) as thumb,
            COALESCE((SELECT SUM(quantity) FROM inventory WHERE products.id = inventory.product_id GROUP BY inventory.product_id), 0) as inventory_quantity
        "));

        return Datatables::eloquent($model)
                ->filterColumn('style', function($query, $keyword)  {
                    $query->where('products.style', 'like', "%{$keyword}%");
                    $query->orWhere('products.product_name', 'like', "%{$keyword}%");
                })
                ->editColumn('thumb', '{{ product_thumb($thumb) }}')
                ->make(true);
    }
}
