@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($user) ? true : false; ?>

<section class="content-header">
    <h1>Add Member</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/team"><i class="fa fa-th-large"></i> Team</a></li>
        <li class="active">@if ($isEdit) Edit Member @else Add New Members @endif</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        <div class="form-group @hasError('name')">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" class="form-control" maxlength="255" name="name" value="{{ old('name', $user->name ?? '') }}">
                            @error('name')
                        </div>

                        <div class="form-group @hasError('email')">
                            <label class="control-label" for="email">Email</label>
                            <input class="form-control" maxlength="255" type="email" name="email" value="{{ old('email', $user->email ?? '') }}">
                            @error('email')
                        </div>

                        @if ($isEdit) Leave passwords blank, if you do not want to change. <br/> @endif

                        <div class="form-group @hasError('password')">
                            <label class="control-label" for="password">Password</label>
                            <input class="form-control" maxlength="255" type="password" name="password" value="{{ old('password') }}">
                            @error('password')
                        </div>

                        <div class="form-group @hasError('password_confirmation')">
                            <label class="control-label" for="password_confirmation">Verify Password</label>
                            <input class="form-control" maxlength="255" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
                            @error('password_confirmation')
                        </div>

                        <div class="checkbox">
                            <?php
                                $acl = $user->permissions ?? [];
                                $hasAllPermissions = (isset($acl['all-permissions']) && $acl['all-permissions'] == true) ? true : false;
                            ?>

                            <label><input type="checkbox" {{ old('all_permissions', $hasAllPermissions) ? 'checked="checked"' : '' }} id="all-permissions" name="all_permissions" value="1">All Permissions</label>

                            <div style="padding-left: 20px;@if(old('all_permissions', $hasAllPermissions)) display: none; @endif" id="permissions">
                                @foreach ($permissions as $permission => $label)
                                    <label><input type="checkbox" name="{{ $permission }}" value="1" {{ old($permission, (($acl[$permission] ?? null) || $hasAllPermissions)) ? 'checked="checked"' : '' }}>{{ $label }}</label>
                                    <br/>
                                @endforeach
                            </div>
                        </div>

                        <br/>
                        <div class="form-group">
                            <button class="btn btn-success" type="submit">@if($isEdit) Update User @else Add User @endif</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#all-permissions').change(function() {
            if ($(this).is(':checked')) {
                $('#permissions').hide();
            } else {
                $('#permissions').show();
            }
        });
    });
</script>
@endsection
