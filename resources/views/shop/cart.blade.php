<section class="invoice-box">
    <div class="row">
        <div class="col-md-8 no-padding">
            <div class="product-list">
                @foreach ($cart->items as $item)
                    <div class="product-box">
                        <div class="product-image">
                            <button class="btn-remove" onclick="removeFromCart('{{ $item->id }}', this);"></button>
                            <img width="290px" height="290px" src="{{ product_image($item->image) }}" alt="{{ $item->name }}">
                        </div>
                        <div class="product-name">{{ $item->name }}</div>
                        <div class="product-price">${{ $item->price() }}</div>
                        <div class="product-name">Color: {{ $item->color }}</div>
                        <div class="product-name">Size: {{ $item->size }}</div>

                        <div class="product-name">
                            Time left to pay:
                            <div class="clock-expire" data-expire="{{ $expire }}" data-expireAt="{{ $item->expireAt($expire) }}" data-itemId="{{ $item->id }}"></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4 no-padding">
            <div class="right-side">
                <div class="info-row">

                    @if ($localPickupEnabled && $customer->local_pickup)

                        Shipping Method: <strong>Local Pickup @if(shop_id() == 'pinkcoconut') (Olive Branch, MS) @endif</strong>
                        @if ($hasMultipleLocations && $customer->hasStoreLocation())
                            <small class="text-muted small">{{ $customer->storeLocation() }}</small>
                        @endif
                    @else
                        <span class="value align-right"><strong>Shipping to:</strong> <br/>
                            @if ($customer->street_address)
                                {{ $customer->street_address}}, <br/>
                                {{ ($customer->apartment != '') ? $customer->apartment . ", " : '' }}
                                {{ $customer->city }}, {{ $customer->state }}, {{ $customer->zip }},
                                {{ $customer->getCountryCode() }}
                            @else
                                We do not have your address!
                            @endif
                        </span>

                        <button class="small" onclick="showAddressPopup();">Update Address</button><br/>

                        <br/><br/>
                        <div style="clear: both;"></div>
                    @endif
                </div>

                @if ($localPickupEnabled)
                    <div class="flex-container switch-buttons">
                        <button class="btn-switch local-pick-up" onclick="deliveryMethod('Local');">
                            <img src="{{ cdn('/images/icon-local-harvest.png') }}">
                            <p>Switch to Local Pickup</p>
                        </button>
                        <button class="btn-switch" onclick="deliveryMethod('Delivery');">
                            <img src="{{ cdn('/images/icon-delivery.png') }}">
                            <p>Switch to Delivery</p>
                        </button>
                    </div>
                @endif

                <div class="calculation">
                    <div class="total-order">Total order</div>
                    <div class="order-value">${{ amount($cart->total) }}</div>
                    <hr/>

                    <div class="item-price">Subtotal: <span>${{ amount($cart->subtotal) }}</span></div>

                    <div class="item-price">Shipping price: <span>{{ $cart->shipping_price == 0 ? 'Free Shipping!' : '$'.amount($cart->shipping_price) }}</span></div>

                    <div class="item-price">Total taxes: <span>${{ amount($cart->tax_total) }}</span></div>

                    @if ($cart->coupon_discount)
                        <div class="item-price">Coupon Discount: <span>- ${{ amount($cart->coupon_discount) }}</span></div>
                    @endif

                    @if ($cart->deduced_balance)
                        <div class="item-price">Account Credit: <span>- ${{ amount($cart->deduced_balance) }}</span></div>
                    @endif

                    <br/><br/>
                    <button class="btn btn-success" onclick="showCouponBox()">Click here to use coupon code</button>
                    <div style="display: none;" id="coupon-wrapper">
                        <input type="text" id="coupon-code" placeholder="enter code" onkeyup="checkCouponApply(event)" />
                        <button onclick="couponApply()">Apply</button>
                    </div>
                    <br/><br/>

                    @if ($customer->hasBalance())
                        <button onclick="applyBalance()" class="btn btn-info">Click here to use account credit</button>
                        <br/>
                    @endif

                    @if ($customer->street_address == '')
                        <a href='javascript: void(0);' onclick="showAddressPopup()"><strong><em> Please update address before continuing</em></strong></a>
                        <br/><br/>

                    @elseif (empty($customer->email))
                        <br/>
                        <strong><em>Please enter your email before continuing</em></strong>
                        <br/><br/>

                    @else
                        <br/>
                        <small class="slink">Choose payment method</small>
                        <br/>
                        <select id="paymentList" style="width:256px;">
                            @if ($cart->isFullyPaidWithBalance)
                                <option value="credit" data-image="{{ cdn('/images/icons/Cash-56.png') }}" data-description="Pay with account credit" selected="selected">Account Credit</option>
                            @else
                                {{-- Only allow stripe or paypal if the order is not fully paid with balance. --}}
                                {{-- todo: add option to delete existing cards from the dropdown (add an X on the select) --}}
                                @if (shop_setting('stripe.access_token'))
                                    @foreach ($customer->cards() as $card)
                                        <option class="existing-card" value="{{ $card->card_id }}" data-description="Ending in: {{ $card->last_four }} Exp: {{ $card->exp_month }}/{{ $card->exp_year }}" data-image="{{ card_image($card->brand) }}">{{ $card->brand }}</option>
                                    @endforeach
                                    <option value="newcc" data-image="{{ cdn('/images/Visa-56.png') }}" data-description="Pay with a credit card">New Card</option>
                                @endif
                                @if (shop_setting('paypal.client_id') || shop_setting('paypal.identity.email'))
                                    <option value="paypal" data-image="{{ cdn('/images/Paypal-56.png') }}">PayPal Account</option>
                                @endif
                            @endif
                        </select>
                        <br/><br/>
                        <div style="clear: both;"></div>

                        <?php $pageId = fb_page_id(); ?>
                        @if ($pageId)
                            <div>
                                <?php $userRef =  time() . rand(0, 100000) . '-' . $customer->id; ?>
                                <div class="fb-messenger-checkbox"
                                        origin="{{ shop_url() }}"
                                        page_id="{{ $pageId }}"
                                        messenger_app_id="{{ fb_app_id() }}"
                                        user_ref="{{ $userRef }}"
                                        prechecked="true"
                                        allow_login="true"
                                        size="large"></div>

                                <script type="text/javascript">
                                    function confirmOptIn() {
                                        if (typeof FB != 'object') {
                                            return;
                                        }

                                        FB.AppEvents.logEvent('MessengerCheckboxUserConfirmation', null, {
                                            'app_id':'{{ fb_app_id() }}',
                                            'page_id':'{{ $pageId }}',
                                            'ref':'{{ shop_id() }}',
                                            'user_ref':'{{ $userRef }}'
                                        });
                                    }
                                </script>
                            </div>
                            <br style="clear: both;" />
                        @endif

                        {{-- todo: make sure the cart reflects the latest changes, else alert: "The cart was updated since the last time you opened the page. Refreshing the page for latest changes..." --}}
                        <button class="pull-left" id="checkoutButton" data-email="{{ $customer->email }}" data-amount="{{ $cart->total }}">Checkout</button>

                    @endif

                    <div style="clear: both;"></div>
                </div>

            </div>
        </div>
    </div>
</section>
