<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Master;
use App\Facebook\FacebookMessenger;
use Facebook\Exceptions\FacebookResponseException;
use App\Jobs\Traits\ReconnectShopDB;
use App\Shopify\ShopifyClient;
use App\Shopify\Shopify;
use App\Mails\ShopMailer;
use Exception;
use DB;

class ReSyncShopify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    protected $shop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop)
    {
        $this->shop = $shop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $startTime = microtime(true);
            $this->reconnectDB($this->shop);
            log_info("[shopify-resync] Re-Syncing up shopify...");

            if (! shop_setting('shopify.enabled') || ! shop_setting('shopify.access_token')) {
                log_info("[shopify-resync] Shopify not enabled. Exiting re-sync...");
                return;
            }

            $shopify = new Shopify;
            $shopify->importProducts();

            log_info("[shopify-resync] Shopify re-sync completed in (" . elapsed_time($startTime) . ") seconds.");

        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shopify-resync] [exception] {$e}");
        }
    }
}
