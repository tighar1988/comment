@extends('admin.setup.shipping.shipping')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <div>Your current prepaid credit is <span class="label label-info" style="font-size: medium; margin-left: 5px;">${{ amount(Auth::user()->prepaid_credit) }}</span></div>
                <br/>

                <span class="settings-info">Purchase prepaid credit for creating labels</span>
                <span class="settings-muted text-muted">This amount will be charged to your credit card.</span>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/buy-prepaid-credit') }}">
                    <div class="form-group @hasError('prepaid_credit')">
                        <label class="control-label">Amount</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" name="prepaid_credit" value="{{ old('prepaid_credit', 100) }}">
                        </div>
                        @error('prepaid_credit')
                    </div>
                    <button class="btn btn-success" data-loading-text="Purchasing Prepaid Credit.." type="submit">Purchase Prepaid Credit</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <span class="settings-info">Auto Purchase Prepaid Credit</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="shipping.auto-buy-prepaid-credit" @if(shop_setting('shipping.auto-buy-prepaid-credit', false)) checked="" @endif class="toggle-switch"></label></div>
                <span class="settings-muted text-muted">If enabled, your card will be automatically charged whenever you want to create labels and you don't have enough prepaid credit.</span>
            </div>
        </div>
    </div>
</div>
@endsection
