<?php

namespace App\Shopify;

use App\Shopify\ShopifyClient;
use App\Models\Order;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Coupon;
use App\Models\User;
use App\Models\Metafield;
use App\Models\Page;
use App\Models\ShopUser;
use App\Models\Collection;
use App\Models\Collect;
use App\Models\TagGroup;
use App\Models\Tag;
use App\Models\Image as ProductImage;
use Exception;
use App\Mails\ShopMailer;
use App\Facebook\FacebookMessenger;
use App\Exceptions\ShopifyWebhookLockedException;

class Shopify
{
    /**
     * @var ShopifyClient
     */
    protected $client = null;

    public function getClient()
    {
        if (is_null($this->client)) {
            $this->client = new ShopifyClient(shop_setting('shopify.shopify-shop-name'));
            $this->client->setToken(shop_setting('shopify.access_token'));
        }

        return $this->client;
    }

    public function getWebhooks()
    {
        $client = $this->getClient();
        $webhooks = $client->callAPI('GET', '/admin/webhooks.json');

        return $webhooks;
    }

    public function createWebhooks()
    {
        $webhookUrl = config('services.shopify.webhook_callback');

        $topics = [
            'orders/create',
            'orders/fulfilled',
            'refunds/create',
            'products/create',
            'products/delete',
            'products/update',
        ];

        foreach ($topics as $topic) {
            $this->createWebhook($topic, $webhookUrl);
        }
    }

    public function createWebhook($topic, $webhookUrl)
    {
        $client = $this->getClient();
        $webhook = $client->callAPI('POST', '/admin/webhooks.json', [
            'webhook' => [
                'topic'   => $topic,
                'address' => $webhookUrl,
                'format'  => 'json',
            ],
        ]);

        return $webhook;
    }

    public function deleteWebhooks()
    {
        foreach ($this->getWebhooks() as $webhook) {
            $this->deleteWebhook($webhook['id']);
        }
    }

    public function deleteWebhook($webhookId)
    {
        $client = $this->getClient();

        return $client->callAPI('DELETE', "/admin/webhooks/{$webhookId}.json");
    }

    public function updateVariantQuantity($variant, $quantity)
    {
        $client = $this->getClient();
        $response = $client->callAPI('PUT', "/admin/variants/{$variant->shopify_inventory_id}.json", [
            'variant' => [
                'id' => $variant->shopify_inventory_id,
                'inventory_quantity_adjustment' => $quantity,
            ]
        ]);

        // shopify does not track inventory for this variant; alert admin
        if (! isset($response['inventory_management']) || is_null($response['inventory_management'])) {

            // notify admin to update inventory tracking in Shopify
            $lastNotified = $variant->shopify_webhook_status;
            if (empty($lastNotified) || (is_numeric($lastNotified) && time() > $lastNotified + 60*60*12)) {
                $variant->timestampShopifyApiCall();
                $shopOwner = User::getByShopName(shop_id());
                (new ShopMailer)->sendShopifyInventoryNotTracked($shopOwner, $variant, $variant->product);
                throw new ShopifyWebhookLockedException("Inventory is not tracked by Shopify for inventory id #{$variant->id}. Notifying shop owner.");
            } else {
                throw new ShopifyWebhookLockedException("Inventory is not tracked by Shopify for inventory id #{$variant->id}.");
            }
        }

        // make sure the return quantity is what we expect
        if (! isset($response['inventory_quantity']) || ! is_numeric($response['inventory_quantity'])) {
            throw new ShopifyWebhookLockedException("Failed to update shopify for inventory id #{$variant->id}, shopify inventory id {$variant->shopify_inventory_id}");
        }

        $expectedQuantity = max($variant->quantity + $quantity, 0);
        if ($expectedQuantity != max($response['inventory_quantity'], 0)) {

            // add back the inventory quantity
            $inverseQuantity = -1 * $quantity;
            $client->callAPI('PUT', "/admin/variants/{$variant->shopify_inventory_id}.json", [
                'variant' => [
                    'id' => $variant->shopify_inventory_id,
                    'inventory_quantity_adjustment' => $inverseQuantity,
                ]
            ]);

            throw new ShopifyWebhookLockedException("Unexpected shopify quantity for inventory #{$variant->id}, shopify inventory id {$variant->shopify_inventory_id}. Expected: {$expectedQuantity}/Received: {$response['inventory_quantity']}");
        }

        // try {
        //     // the webhook faild to update in 60 seconds; try to manually fetch the product
        //     $product = Product::find($variant->product_id);
        //     $product = $client->callAPI('GET', '/admin/products/'.$product->shopify_product_id.'.json');
        //     $this->importProduct($product);

        // } catch (Exception $e) {
        //     throw new ShopifyWebhookLockedException("Timeout, webhook not updated for inventory id #{$variant->id}, shopify inventory id {$variant->shopify_inventory_id}" . $e->getMessage());
        // }
    }

    public function importPages()
    {
        $client = $this->getClient();
        $pagesCount = $client->callAPI('GET', '/admin/pages/count.json');
        $nrPages = ceil($pagesCount / 250);

        for ($pageNr = 1; $pageNr <= $nrPages; $pageNr++) {
            $pages = $client->callAPI('GET', '/admin/pages.json', ['limit' => 250, 'page' => $pageNr]);
            foreach ($pages as $page) {
                $this->importPage($page);
            }
        }
    }

    public function importPage($page)
    {
        $p = new Page;
        $p->title           = $page['title'];
        $p->content         = $page['body_html'];
        $p->seo_title       = substr($page['title'], 0, 70);
        $p->seo_description = substr($page['body_html'], 0, 255);
        $p->url             = $page['handle'];
        $p->save();
    }

    public function importCoupons()
    {
        $client = $this->getClient();

        for ($page = 1; $page <= 1000; $page++) {
            $coupons = $client->callAPI('GET', '/admin/price_rules.json', ['limit' => 200, 'page' => $page]);
            if (count($coupons) == 0) {
                return;
            }

            foreach ($coupons as $coupon) {
                $this->importCoupon($coupon);
            }
        }
    }

    public function importCoupon($coupon)
    {
        // update or create new coupon
        $c = Coupon::where('shopify_coupon_id', '=', $coupon['id'])->first() ?? new Coupon;
        $c->shopify_coupon_id = $coupon['id'];

        $c->code = substr(mysql_utf8($coupon['title']), 0, 30);
        $c->max_usage = is_numeric($coupon['usage_limit']) ? $coupon['usage_limit'] : 0;
        $c->starts_at = empty($coupon['starts_at']) ? 0 : strtotime($coupon['starts_at']);
        if ($c->starts_at < 0) {
            $c->starts_at = 0;
        }

        $c->expires_at = empty($coupon['ends_at']) ? 0 : strtotime($coupon['ends_at']);
        if ($c->expires_at < 0) {
            $c->expires_at = 0;
        }

        $c->amount = abs($coupon['value']);

        if ($coupon['value_type'] == 'fixed_amount') {
            $c->coupon_type = 'F';
        } elseif ($coupon['value_type'] == 'percentage') {
            $c->coupon_type = 'P';
        } elseif ($coupon['value_type'] == 'shipping') {
            $c->coupon_type = 'S';
        }

        $minPurchaseAmount = null;
        if (isset($coupon['prerequisite_subtotal_range']['greater_than_or_equal_to'])) {
            $minPurchaseAmount = $coupon['prerequisite_subtotal_range']['greater_than_or_equal_to'];
        }

        $c->options = [
            'description' => '[Shopify]',
            'onlyOnce'    => (bool) $coupon['once_per_customer'],
            'minPurchaseAmount' => $minPurchaseAmount,
        ];

        if ($c->isActive()) {
            $c->save();
        } elseif (! empty($c->id)) {
            $c->delete();
        }
    }

    public function importMetafields()
    {
        $client = $this->getClient();
        $meatafieldsCount = $client->callAPI('GET', '/admin/metafields/count.json');
        $nrPages = ceil($meatafieldsCount / 250);

        for ($page = 1; $page <= $nrPages; $page++) {
            $metafields = $client->callAPI('GET', '/admin/metafields.json', ['limit' => 250, 'page' => $page]);
            foreach ($metafields as $metafield) {
                $this->importMetafield($metafield);
            }
        }
    }

    public function importMetafield($metafield)
    {
        // update or create a new metafield
        $m = Metafield::where('shopify_metafield_id', '=', $metafield['id'])->first() ?? new Metafield;
        $m->shopify_metafield_id = $metafield['id'];
        $m->namespace            = $metafield['namespace'];
        $m->key                  = $metafield['key'];
        $m->value                = $metafield['value'];
        $m->value_type           = $metafield['value_type'];
        $m->description          = $metafield['description'];
        $m->owner_resource       = $metafield['owner_resource'];
        $m->owner_id             = null;
        $m->shopify_owner_id     = $metafield['owner_id'];
        $m->save();
    }

    public function importSmartCollections()
    {
        $client = $this->getClient();
        $collectionsCount = $client->callAPI('GET', '/admin/smart_collections/count.json');
        $nrPages = ceil($collectionsCount / 250);

        for ($page = 1; $page <= $nrPages; $page++) {
            $collections = $client->callAPI('GET', '/admin/smart_collections.json', ['limit' => 250, 'page' => $page]);
            foreach ($collections as $collection) {
                $this->importSmartCollection($collection);
            }
        }
    }

    public function importSmartCollection($collection)
    {
        // update or create a new collection
        $c = Collection::where('shopify_collection_id', '=', $collection['id'])->first() ?? new Collection;
        $c->shopify_collection_id = $collection['id'];
        $c->title            = $collection['title'] ?? null;
        $c->content          = mysql_utf8($collection['body_html'] ?? null);
        $c->url              = $collection['handle'] ?? null;
        $c->published_at     = isset($collection['published_at']) ? strtotime($collection['published_at']) : null;
        $c->sort_order       = $collection['sort_order'] ?? null;
        $c->template_suffix  = $collection['template_suffix'] ?? null;
        $c->published_scope  = $collection['published_scope'] ?? null;
        $c->updated_at       = isset($collection['updated_at']) ? strtotime($collection['updated_at']) : null;
        $c->smart_collection = true;
        $c->smart_collection_rules = json_encode($collection);
        $c->save();
    }

    public function importCollections()
    {
        $client = $this->getClient();
        $collectionsCount = $client->callAPI('GET', '/admin/custom_collections/count.json');
        $nrPages = ceil($collectionsCount / 250);

        for ($page = 1; $page <= $nrPages; $page++) {
            $collections = $client->callAPI('GET', '/admin/custom_collections.json', ['limit' => 250, 'page' => $page]);
            foreach ($collections as $collection) {
                $this->importCollection($collection);
            }
        }
    }

    public function importCollection($collection)
    {
        // update or create a new collection
        $c = Collection::where('shopify_collection_id', '=', $collection['id'])->first() ?? new Collection;
        $c->shopify_collection_id = $collection['id'];
        $c->title            = $collection['title'] ?? null;
        $c->content          = mysql_utf8($collection['body_html'] ?? null);
        $c->url              = $collection['handle'] ?? null;
        $c->image            = $collection['src'] ?? null;
        $c->image_created_at = $collection['image']['created_at'] ?? null;
        $c->image_width      = $collection['image']['width'] ?? null;
        $c->image_height     = $collection['image']['height'] ?? null;
        $c->published_at     = isset($collection['published_at']) ? strtotime($collection['published_at']) : null;
        $c->sort_order       = $collection['sort_order'] ?? null;
        $c->template_suffix  = $collection['template_suffix'] ?? null;
        $c->published_scope  = $collection['published_scope'] ?? null;
        $c->updated_at       = isset($collection['updated_at']) ? strtotime($collection['updated_at']) : null;
        $c->save();
    }

    public function importCollects()
    {
        $client = $this->getClient();

        $collections = Collection::whereNotNull('shopify_collection_id')->get();
        foreach ($collections as $collection) {
            $shopifyId = $collection->shopify_collection_id;
            log_info("[import-collection] {$collection->title}");

            $collectCount = $client->call('GET', '/admin/collects/count.json?collection_id='.$shopifyId);
            $nrPages = ceil($collectCount / 250);

            for ($page = 1; $page <= $nrPages; $page++) {
                $collects = $client->call('GET', '/admin/collects.json?page='.$page.'&limit=250&collection_id='.$shopifyId);
                foreach ($collects as $collect) {
                    $this->importCollect($collect, $collection->id);
                }
            }
        }
    }

    public function importCollect($collect, $collectionId)
    {
        // create a new collect
        $c = new Collect;

        $product = Product::where('shopify_product_id', '=', $collect['product_id'])->first();
        if (! $product) {
            return;
        }

        $exists = \DB::connection('shop')->table('collection_product')
            ->where('product_id', $product->id)
            ->where('shopify_collect_id', $collect['id'])
            ->exists();

        if ($exists) {
            return;
        }

        $c->product_id         = $product->id;
        $c->collection_id      = $collectionId;
        $c->featured           = (bool) $collect['featured'];
        $c->position           = $collect['position'];
        $c->sort_value         = $collect['sort_value'];
        $c->created_at         = strtotime($collect['created_at']);
        $c->updated_at         = strtotime($collect['updated_at']);
        $c->shopify_collect_id = $collect['id'];
        $c->save();
    }

    public function importTags()
    {
        // get or create the tags group
        $tagGroup = TagGroup::where('name', '=', 'Tags')->first();
        if (is_null($tagGroup)) {
            $tagGroup = TagGroup::create(['name' => 'Tags', 'description' => 'All Tags']);
        }

        $client = $this->getClient();
        $productCount = $client->callAPI('GET', '/admin/products/count.json');
        $nrPages = ceil($productCount / 250);

        for ($page = 1; $page <= $nrPages; $page++) {
            $products = $client->callAPI('GET', '/admin/products.json', ['limit' => 250, 'page' => $page]);
            foreach ($products as $product) {

                // get the shopify product
                $p = Product::where('shopify_product_id', '=', $product['id'])->first();
                if (is_null($p)) {
                    continue;
                }

                $tagNames = explode(',', $product['tags']);
                foreach ($tagNames as $tagName) {
                    $tagName = trim($tagName);

                    if (empty($tagName)) {
                        continue;
                    }

                    // get or create tag
                    $tag = Tag::where('name', '=', $tagName)->where('tag_group_id', '=', $tagGroup->id)->first();
                    if (is_null($tag)) {
                        $tag = Tag::create(['tag_group_id' => $tagGroup->id, 'name' => $tagName]);
                    }

                    $exists = \DB::connection('shop')->table('tagging_tagged')
                        ->where('taggable_id', $p->id)
                        ->where('tag_id', $tag->id)
                        ->where('taggable_type', 'product')
                        ->exists();

                    // tag product
                    if (! $exists) {
                        $p->tags()->attach($tag->id);
                    }
                }
            }
        }
    }

    public function importCustomers()
    {
        $client = $this->getClient();
        $customerCount = $client->callAPI('GET', '/admin/customers/count.json');
        $nrPages = ceil($customerCount / 250);

        for ($page = 1; $page <= $nrPages; $page++) {
            $customers = $client->callAPI('GET', '/admin/customers.json', ['limit' => 250, 'page' => $page]);
            foreach ($customers as $customer) {
                $this->importCustomer($customer);
            }
        }
    }

    public function importCustomer($customer)
    {
        // update or create new shop user
        $c = ShopUser::where('shopify_customer_id', '=', $customer['id'])->first() ?? new ShopUser;
        $c->shopify_customer_id = $customer['id'];
        $c->email               = $customer['email'];
        $c->accepts_marketing   = (bool) $customer['accepts_marketing'];
        $c->created_at          = isset($customer['created_at']) ? strtotime($customer['created_at']) : null;
        $c->updated_at          = isset($customer['updated_at']) ? strtotime($customer['updated_at']) : null;
        $c->first_name          = $customer['first_name'];
        $c->last_name           = $customer['last_name'];
        $c->verified_email      = (bool) $customer['verified_email'];
        $c->name                = $customer['default_address']['name'] ?? null;
        $c->street_address      = $customer['default_address']['address1'] ?? null;
        $c->apartment           = $customer['default_address']['address2'] ?? null;
        $c->city                = $customer['default_address']['city'] ?? null;
        $c->state               = $customer['default_address']['province_code'] ?? null;
        $c->zip                 = $customer['default_address']['zip'] ?? null;
        $c->phone_number        = $customer['default_address']['phone'] ?? null;
        $c->country_code        = $customer['default_address']['country_code'] ?? null;
        $c->password            = null;
        $c->save();
    }

    public function importProducts()
    {
        $client = $this->getClient();
        $productCount = $client->callAPI('GET', '/admin/products/count.json');
        $nrPages = ceil($productCount / 250);

        for ($page = 1; $page <= $nrPages; $page++) {
            $products = $client->callAPI('GET', '/admin/products.json', ['limit' => 250, 'page' => $page]);
            foreach ($products as $product) {
                $this->importProduct($product);
            }
        }
    }

    public function importProduct($product)
    {
        // update or create new product
        $p = Product::where('shopify_product_id', '=', $product['id'])->first() ?? new Product;
        $p->shopify_product_id = $product['id'];
        $p->product_name = substr(mysql_utf8($product['title']), 0, 255);
        $p->description  = mysql_utf8($product['body_html']);
        $p->style        = mysql_utf8($this->getProductSKU($product));
        $p->brand        = mysql_utf8($product['vendor']);
        $p->charge_taxes = $this->chargeTaxes($product['variants']);
        $p->brand_style  = null;
        $p->published_at = empty($product['published_at']) ? null : strtotime($product['published_at']);
        $p->publish_product = empty($product['published_at']) ? false : true;
        $p->save();

        $this->syncProductImages($p, $product['images']);

        $this->syncProductInventory($p, $product['variants'], $product['options']);
    }

    /**
     * Charge tax on the procut if all the variants also charge tax.
     */
    public function chargeTaxes($variants)
    {
        foreach ($variants as $variant) {
            if ($variant['taxable'] == false) {
                return false;
            }
        }

        return true;
    }

    public function getProductSKU($product)
    {
        $skus = [];
        foreach ($product['variants'] as $variant) {
            $skus[] = trim($variant['sku']);
        }

        $common = str_split(array_shift($skus));
        foreach ($skus as $sku) {
            if (empty($sku)) {
                continue;
            }

            $chars = str_split($sku);
            $tmp = [];

            for ($i = 0; $i < count($common) && $i < count($chars); $i++) {
                if ($common[$i] != $chars[$i]) {
                    break;
                }

                $tmp[] = $common[$i];
            }

            $common = $tmp;
        }

        $common = implode('', $common);
        $common = trim($common, '-');

        return empty($common) ? $product['id'] : $common;
    }

    public function syncProductImages($p, $images)
    {
        // remove deleted images
        $ids = [];
        foreach ($images as $image) {
            $ids[] = $image['id'];
        }

        ProductImage::where('product_id', '=', $p->id)->whereNull('shopify_image_id')->delete();
        ProductImage::where('product_id', '=', $p->id)->whereNotIn('shopify_image_id', $ids)->delete();

        // update or create new product image
        foreach ($images as $image) {
            $productImage = ProductImage::where('shopify_image_id', '=', $image['id'])->first();

            if ($productImage) {
                if (str_contains($productImage->filename, ['.shopify.com', '.bigcommerce.com'])) {
                    // update image source only we didn't import the image to our system
                    $productImage->filename = $image['src'];
                    $productImage->save();
                }
            } else {
                ProductImage::newImage($p->id, $image['src'], $image['id']);
            }
        }

        // make sure the product has a main image
        $mainImage = ProductImage::where('product_id', '=', $p->id)->where('is_main', '=', true)->first();
        if (is_null($mainImage)) {
            $image = ProductImage::where('product_id', '=', $p->id)->first();
            if ($image) {
                $image->is_main = true;
                $image->save();
            }
        }
    }

    public function syncProductInventory($p, $variants, $options)
    {
        // remove deleted images
        $ids = [];
        foreach ($variants as $variant) {
            $ids[] = $variant['id'];
        }

        Inventory::where('product_id', '=', $p->id)->whereNull('shopify_inventory_id')->delete();
        Inventory::where('product_id', '=', $p->id)->whereNotIn('shopify_inventory_id', $ids)->delete();

        // update or create new product varaints
        foreach ($variants as $variant) {

            if (isset($options[0]['name']) && strtolower($options[0]['name']) == 'size') {
                $color = $variant['option2'];
                $size = $variant['option1'];
            } else {
                $color = $variant['option1'];
                $size = $variant['option2'];
            }

            if (! is_null($variant['option3'])) {
                if (is_null($color)) {
                    $color = $variant['option3'];
                } elseif (is_null($size)) {
                    $size = $variant['option3'];
                }
            }

            $inventory = Inventory::where('product_id', '=', $p->id)->where('shopify_inventory_id', '=', $variant['id'])->first();

            if ($inventory) {
                $inventory->color    = trim($color);
                $inventory->size     = trim($size);
                $inventory->quantity = $variant['inventory_quantity'];
                $inventory->price    = $variant['price'];
                $inventory->setWeightInOz($variant['weight'], $variant['weight_unit']);
                if (isset($variant['barcode']) && ! empty($variant['barcode'])) {
                    $inventory->shopify_barcode = $variant['barcode'];
                }
                $inventory->sku = $variant['sku'];
                $inventory->save();

                log_info("[shopify] Updating quantity for inventory item #{$inventory->id} to ".$variant['inventory_quantity']);

            } else {
                Inventory::newItem([
                    'shopify_inventory_id' => $variant['id'],
                    'product_id' => $p->id,
                    'color'      => trim($color),
                    'size'       => trim($size),
                    'quantity'   => $variant['inventory_quantity'],
                    'cost'       => 0,
                    'price'      => $variant['price'],
                    'weight'     => Inventory::getWeightInOz($variant['weight'], $variant['weight_unit']),
                    'sku'        => $variant['sku'],
                    'shopify_barcode' => $variant['barcode'] ?? null,
                ]);
            }
        }
    }

    public function deleteProduct($product)
    {
        Product::where('shopify_product_id', '=', $product['id'])->delete();
    }

    /**
     * API reference https://help.shopify.com/api/reference/order
     */
    public function createOrder($order)
    {
        $lineItems = [];
        foreach ($order->orderProducts as $orderProduct) {
            if (! is_null($orderProduct->shopify_variant_id)) {
                $lineItems[] = [
                    'variant_id' => $orderProduct->shopify_variant_id,
                    'quantity' => 1,
                    'requires_shipping' => $order->local_pickup ? false : true,
                    'fulfillment_service' => 'manual',
                    'fulfillment_status' => null,
                    'gift_card' => false,
                    'title' => $orderProduct->prod_name,
                    'name' => $orderProduct->prod_name,
                    'price' => $orderProduct->price,
                ];
            }
        }

        // if the order has no products, or contains original CommentSold products don't sync it
        if (count($lineItems) == 0 || count($lineItems) != count($order->orderProducts)) {
            return 0;
        }

        $discountCodes = [];
        if (! empty($order->coupon)) {
            $coupon = Coupon::getCoupon($order->coupon);
            if ($coupon) {
                $discountCodes[] = [
                    'amount' => $order->coupon_discount,
                    'code'   => $order->coupon,
                    'type'   => $coupon->getShopifyType(),
                ];
            }
        }

        $json = [
            'order' => [
                'email'                    => $order->email,
                'fulfillment_status'       => null,
                'send_receipt'             => false,
                'send_fulfillment_receipt' => false,
                'inventory_behaviour'      => 'bypass',
                'financial_status'         => 'paid',
                'currency'                 => 'USD',
                'note'                     => $order->local_pickup ? 'Local Pickup' : null,
                'order_number'             => $order->id,
                'subtotal_price'           => $order->subtotal,
                'total_price'              => $order->total,
                'total_tax'                => $order->tax_total,
                'taxes_included'           => false,
                'line_items'               => $lineItems,
                'source_name'              => 'CommentSold',
                'shipping_address' => [
                    'address1'      => $order->street_address,
                    'address2'      => $order->apartment,
                    'city'          => $order->city,
                    'country'       => $order->getCountryCode(),
                    // 'province'      => $order->state,
                    'zip'           => $order->zip,
                    'name'          => $order->ship_name,
                    'country_code'  => $order->getCountryCode(),
                    'province_code' => $order->state,
                ],
                'billing_address' => [
                    'address1'      => $order->street_address,
                    'address2'      => $order->apartment,
                    'city'          => $order->city,
                    'company'       => null,
                    'country'       => $order->getCountryCode(),
                    'first_name'    => null,
                    'last_name'     => null,
                    'phone'         => null,
                    // 'province'      => $order->state,
                    'zip'           => $order->zip,
                    'name'          => $order->ship_name,
                    'province_code' => $order->state,
                    'country_code'  => $order->getCountryCode(),
                    'default'       => true
                ],
                'discount_codes' => $discountCodes,
            ]
        ];

        if ($order->tax_total > 0) {
            $taxable = $order->total - $order->tax_total;
            if ($taxable > 0) {
                $json['order']['tax_lines'] = [[
                    'title' => 'Tax Total',
                    'price' => $order->tax_total,
                    'rate'  => $order->tax_total / $taxable,
                ]];
            }
        }

        if ($order->total > 0) {
            $json['order']['transactions'] = [[
                'amount'   => $order->total,
                'kind'     => 'sale',
                'status'   => 'success',
                'currency' => 'USD',
                // 'gateway'     => 'bogus',
                // 'source_name' => 'web',
            ]];
        }

        if (! $order->local_pickup) {
            $json['order']['shipping_lines'] = [[
                'code'   => null,
                'price'  => amount($order->ship_charged),
                'source' => 'shopify',
                'title'  => $order->ship_charged > 0 ? 'Shipping $'.amount($order->ship_charged) : 'Free Shipping',
                'carrier_identifier' => null,
                'requested_fulfillment_service_id' => null,
            ]];
        }

        $client = $this->getClient();
        return $client->callAPI('POST', '/admin/orders.json', $json);
    }

    public function fulfillOrder($shopifyOrder)
    {
        $order = Order::where('shopify_order_id', '=', $shopifyOrder['id'])->with('orderProducts')->first();
        if (is_null($order) || $order->isFulfilled()) {
            return;
        }

        $order->fulfill();

        (new ShopMailer)->sendOrderShippedEmail($order);
        FacebookMessenger::notifyOrderFulfilled($order);
    }
}
