<?php

namespace App\OnlineStore\LiquidTags;

use App\OnlineStore\Vendor\Liquid\AbstractBlock;
use App\OnlineStore\Vendor\Liquid\Context;

class TagLayout extends AbstractBlock
{
    public static $theme = 'theme';

    /**
     * Render the block.
     *
     * @param Context $context
     *
     * @return string
     */
    public function render(Context $context)
    {
        $theme = trim($this->markup);
        $theme = trim($theme, '"\'');

        if ($theme != 'none') {
            static::$theme = $theme;
        } elseif ($theme == 'none') {
            static::$theme = null;
        }

        return parent::render($context);
    }
}
