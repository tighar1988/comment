<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LiquidFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'file_name' => 'required|max:255|unique:shop.liquid_files',
            'content'   => 'required',
        ];

        if ($this->method() == 'PATCH') {
            $rules['file_name'] = 'required|max:255|unique:shop.liquid_files,file_name,' . $this->route('liquidId');
        }

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (strpos($this->file_name, '/') !== false) {
                $validator->errors()->add('file_name', 'The file name must not contain slashes.');
            } elseif (stripos($this->file_name, '.liquid') !== false) {
                $validator->errors()->add('file_name', 'The file name must not contain ".liquid".');
            }
        });
    }
}
