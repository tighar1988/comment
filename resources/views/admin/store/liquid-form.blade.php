@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($liquid) ? true : false; ?>
<section class="content-header">
    <h1>Store</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/pages"><i class="fa fa-th-large"></i>Store</a></li>
        <li><a href="/admin/store/themes/{{ $type }}">{{ ucfirst($type) }} File</a></li>
        @if ($isEdit)
            <li class="active">{{ $liquid->file_name }}</li>
        @else
            <li class="active">Add</li>
        @endif
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">

                    <form class="" role="form" method="POST" @if ($isEdit && empty($liquid->id)) action="/admin/store/themes/{{ $type }}/add" @else action="{{ form_action() }}" @endif>
                        @if ($isEdit && ! empty($liquid->id)) {{ method_field('PATCH') }} @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('file_name')">
                                    <label>File Name</label>
                                    <div class="input-group">
                                        <input type="text" placeholder="my-file" class="form-control" name="file_name" value="{{ old('file_name', $liquid->getFileName() ?? '') }}">
                                        <span class="input-group-addon">.liquid</span>
                                    </div>
                                    @error('file_name')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('content')">
                                    <label>Content</label>
                                    <textarea id="liquid-content" class="form-control" name="content" rows="10">{{ old('content', $liquid->content ?? '') }}</textarea>
                                    @error('content')
                                </div>
                            </div>
                        </div>

                        <button type="submit" data-loading-text="Saving.." class="btn btn-success pull-right">@if($isEdit) Update {{ ucfirst($type) }} File @else Add {{ ucfirst($type) }} File @endif</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ cdn('/codemirror-5.28.0/lib/codemirror.css') }}" type="text/css">
<link rel="stylesheet" href="{{ cdn('/codemirror-5.28.0/theme/monokai.css') }}" type="text/css">
@endsection

@section('scripts')
<script src="{{ cdn('/codemirror-5.28.0/lib/codemirror.js') }}"></script>
<script src="{{ cdn('/codemirror-5.28.0/mode/htmlmixed/htmlmixed.js') }}"></script>
<script src="{{ cdn('/codemirror-5.28.0/mode/xml/xml.js') }}"></script>
<script src="{{ cdn('/codemirror-5.28.0/mode/css/css.js') }}"></script>
<script src="{{ cdn('/codemirror-5.28.0/mode/javascript/javascript.js') }}"></script>
<script type="text/javascript">
    var codeMirror = CodeMirror.fromTextArea(document.getElementById('liquid-content'), {theme: 'monokai'});
</script>
@endsection
