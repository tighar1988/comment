<?php

namespace App\Mails;

use Exception;

class MailThrottler
{
    /**
     * The exceptions list with limit:time (in seconds) values
     *
     * @var array
     */
    protected $exceptions = [
        'Symfony\Component\HttpKernel\Exception\NotFoundHttpException' => '10:60',
        'GuzzleHttp\Exception\ConnectException'                        => '10:60',
        'Facebook\Exceptions\FacebookSDKException'                     => '10:60',
        'Facebook\Exceptions\FacebookResponseException'                => '10:60',
        // 'PayPal\Exception\PayPalConnectionException'                   => '10:60',
    ];

    /**
     * Check if we should allow the excpetion.
     *
     * @param  mixed $exception
     * @return boolean
     */
    public function allowed($exception = null)
    {
        if (! $exception instanceof Exception) {
            return true;
        }

        if ($this->isBlocked($exception)) {
            return false;
        }

        $key = get_class($exception);

        $limitSeconds = $this->get($key);
        if (is_null($limitSeconds)) {
            return true;
        }

        // allow exceptions without time and limit
        $tmp = explode(':', $limitSeconds);
        if (! isset($tmp[0], $tmp[1]) || ! is_numeric($tmp[0]) || ! is_numeric($tmp[1])) {
            return true;
        }

        $limit = intval($tmp[0]);
        $seconds = intval($tmp[1]);
        $minutes = $seconds / 60;

        $throttler = new CacheThrottler(app('cache')->getStore(), $key, $limit, $minutes);
        if (! $throttler->attempt()) {
            // if we reach the limit, allow the email and reset the count
            $throttler->clear();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the limit:time for an exception.
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key)
    {
        if (isset($this->exceptions[$key])) {
            return $this->exceptions[$key];
        }

        return null;
    }

    /**
     * Set the exception types to be checked.
     *
     * @param array $exceptions
     */
    public function setExceptions($exceptions)
    {
        $this->exceptions = $exceptions;
    }

    public function isBlocked($exception)
    {
        if ($exception instanceof \Facebook\Exceptions\FacebookResponseException) {
            if (str_contains($exception->getMessage(), 'Error validating access token: The session has been invalidated because the user changed their password or Facebook has changed the session for security reasons')) {
                return true;
            }

            if (str_contains($exception->getMessage(), 'Sorry, you are blocked from leaving comments due to continued overuse of this feature.')) {
                return true;
            }

            if (str_contains($exception->getMessage(), 'Unsupported get request. Object with ID ') && str_contains($exception->getMessage(), ' does not exist, cannot be loaded due to missing permissions, or does not support this operation. Please read the Graph API documentation at https://developers.facebook.com/docs/graph-api')) {
                return true;
            }

            if (str_contains($exception->getMessage(), '(#230) Requires pages_messaging_subscriptions permission to manage the object')) {
                return true;
            }
        }

        if ($exception instanceof \InvalidArgumentException) {
            if (str_contains($exception->getMessage(), 'The default access token must be of type "string" or Facebook\AccessToken')) {
                return true;
            }
        }

        return false;
    }
}
