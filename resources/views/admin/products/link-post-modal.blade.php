<div id="link-post-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Link Product to an existing Facebook Post</h4>
            </div>
            <div class="modal-body no-search-padding">

                <div class="table-responsive">
                    <table id="table-facebook-posts" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Post Image</th>
                                <th>Post Message</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <form class="form-horizontal" method="POST" action="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
