<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Product as ProductModel;
use App\OnlineStore\CurrentView;

class Product extends Drop
{
    protected $product;

    public function __construct(ProductModel $product)
    {
        $this->product = $product;
    }

    public function toJson()
    {
        $variants = [];
        foreach ($this->variants() as $variant) {
            $variants[] = $variant->toJson();
        }

        $images = [];
        foreach ($this->images() as $image) {
            if (is_object($image)) {
                if (str_contains($image->src(), 'cdn.shopify.com')) {
                    $images[] = $image->src();
                } else {
                    $images[] = cdn_shop($image->src(), 'images');
                }
            }
        }

        $featuredImage = $this->featured_image();
        if (is_object($featuredImage)) {
            if (str_contains($featuredImage->src(), 'cdn.shopify.com')) {
                $featuredImage = $featuredImage->src();
            } else {
                $featuredImage = cdn_shop($featuredImage->src(), 'images');
            }
        }

        return [
            "id"                      => $this->id(),
            "title"                   => $this->title(),
            "handle"                  => $this->handle(),
            "description"             => $this->description(),
            "published_at"            => $this->published_at(),
            "created_at"              => $this->created_at(),
            "vendor"                  => $this->vendor(),
            "type"                    => $this->type(),
            "tags"                    => $this->tags(),
            "price"                   => $this->price(),
            "price_min"               => $this->price_min(),
            "price_max"               => $this->price_max(),
            "available"               => $this->available(),
            "price_varies"            => $this->price_varies(),
            "compare_at_price"        => $this->compare_at_price(),
            "compare_at_price_min"    => $this->compare_at_price_min(),
            "compare_at_price_max"    => $this->compare_at_price_max(),
            "compare_at_price_varies" => $this->compare_at_price_varies(),
            "variants"                => $variants,
            "images"                  => $images,
            "featured_image"          => $featuredImage,
            "options"                 => $this->options(),
            "content"                 => $this->content(),
        ];
    }

    /**
     * Returns true if a product is available for purchase. Returns false if all of the products
     * variants' inventory_quantity values are zero or less, and their inventory_policy is not
     * set to "Allow users to purchase this item, even if it is no longer in stock."
     */
    public function available()
    {
        if (! $this->product->isPublished()) {
            return false;
        }

        if ($this->product->product_type == 'giftcard') {
            return true;
        }

        foreach ($this->product->inventory as $variant) {
            if ($variant->quantity > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns an array of all of the collections a product belongs to.
     *
     * Input
     * This product belongs in the following collections:
     * {% for collection in product.collections %}
     *     {{ collection.title }}
     * {% endfor %}
     *
     * Output
     * This product belongs in the following collections:
     * Sale
     * Shirts
     * Spring
     */
    public function collections()
    {
        $collections = [];

        foreach ($this->product->collections as $collection) {
            $collections[] = new Collection($collection);
        }

        return $collections;
    }

    /**
     * Returns the highest compare at price. Use one of the money filters to return the value in a monetary format.
     */
    public function compare_at_price_max()
    {
        $max = null;

        foreach ($this->product->inventory as $variant) {
            if (is_null($max)) {
                $max = $variant->price;
            }

            $max = max($max, $variant->price);
        }

        return dollars_to_cents($max);
    }

    /**
     * Returns the highest compare at price. Use one of the money filters to return the value in a monetary format.
     */
    public function compare_at_price()
    {
        return $this->compare_at_price_max();
    }

    /**
     * Returns the lowest compare at price. Use one of the money filters to return the value in a monetary format.
     */
    public function compare_at_price_min()
    {
        $min = null;

        foreach ($this->product->inventory as $variant) {
            if (is_null($min)) {
                $min = $variant->priceOrSalePrice();
            }

            $min = min($min, $variant->priceOrSalePrice());
        }

        return dollars_to_cents($min);
    }

    /**
     * Returns true if the compare_at_price_min is different from the compare_at_price_max.
     * Returns false if they are the same.
     */
    public function compare_at_price_varies()
    {
        return false;

        // $max = null;
        // $min = null;

        // foreach ($this->product->inventory as $variant) {
        //     if (is_null($min)) {
        //         $min = $variant->priceOrSalePrice();
        //     }

        //     if (is_null($max)) {
        //         $max = $variant->priceOrSalePrice();
        //     }

        //     $max = max($max, $variant->priceOrSalePrice());
        //     $min = min($min, $variant->priceOrSalePrice());
        // }

        // return $max == $min;
    }

    /**
     * Returns the description of the product. Alias for product.description.
     */
    public function content()
    {
        return $this->product->store_description;
    }

    /**
     * Returns the description of the product.
     */
    public function description()
    {
        return $this->product->store_description;
    }

    /**
     * Returns the relative URL of the product's featured image.
     */
    public function featured_image()
    {
        $image = $this->product->image;
        if (! is_null($image)) {
            return new Image($image, $this->product);
        }

        return null;
    }

    /**
     * Returns the variant object of the first product variant that is available for purchase.
     * In order for a variant to be available, its variant.inventory_quantity must be greater
     * than zero or variant.inventory_policy must be set to continue. A variant with no
     * inventory_policy is considered available.
     */
    public function first_available_variant()
    {
        foreach ($this->product->inventory as $variant) {
            if ($variant->quantity > 0) {
                return new Variant($variant, $this->product);
            }
        }

        return null;
    }

    public function first_variant()
    {
        if (isset($this->product->inventory[0])) {
            return new Variant($this->product->inventory[0], $this->product);
        }

        return null;
    }

    /**
     * Returns the handle of a product.
     */
    public function handle()
    {
        return $this->product->url;
    }

    /**
     * Returns true if the product only has the default variant. This lets you determine
     * whether to show a variant picker in your product forms.
     *
     * Products that don't have customized variants have a single default variant with its "Title" option set to "Default Title".
     *
     * {% if product.has_only_default_variant %}
     *   <input name="id" value="{{ variant.id }}" type="hidden">
     * {% else %}
     *   <select name="id">
     *     {% for variant in product.variants %}
     *       <option value="{{ variant.id }}">{{ variant.title }}</option>
     *     {% endfor%}
     *   </select>
     * {% endif %}
     */
    public function has_only_default_variant()
    {
        if (count($this->product->inventory) > 1) {
            return false;
        }

        return true;
    }

    /**
     * Returns the id of the product.
     */
    public function id()
    {
        return $this->product->id;
    }

    /**
     * Returns an array of the product's images. Use the product_img_url filter to link to the product
     * image on Shopify's Content Delivery Network.
     *
     * Input
     * {% for image in product.images %}
     *     <img src="{{ image.src | product_img_url: 'medium' }}">
     * {% endfor %}
     *
     * Output
     * <img src="//cdn.shopify.com/s/files/1/0087/0462/products/shirt14_medium.jpeg?v=1309278311" />
     * <img src="//cdn.shopify.com/s/files/1/0087/0462/products/nice_shirt_medium.jpeg?v=1331480777">
     * <img src="//cdn.shopify.com/s/files/1/0087/0462/products/aloha_shirt_medium.jpeg?v=1331481001">
     */
    public function images()
    {
        $images = [];

        foreach ($this->product->images as $image) {
            $images[] = new Image($image, $this->product);
        }

        return $images;
    }

    /**
     * Returns an array of the product's option names.
     *
     * Input
     * {% for option in product.options %}
     *     {{ option }}
     * {% endfor %}
     *
     * Output
     * Color Size Material
     *
     * Use size if you need to determine how many options a product has.
     *
     * Input
     * {{ product.options.size }}
     *
     * Output
     * 3
     */
    public function options()
    {
        if ($this->product->product_type == 'giftcard') {
            return ['Amount'];
        }

        if (count($this->product->inventory) <= 1) {
            return [''];
        }

        $hasColor = false;
        $hasSize  = false;
        foreach ($this->product->inventory as $variant) {
            if (! empty($variant->color)) {
                $hasColor = true;
            }

            if (! empty($variant->size)) {
                $hasSize = true;
            }
        }

        if ($hasColor && $hasSize) {
            return ['Color', 'Size'];
        }

        if ($hasColor) {
            return ['Color'];
        }

        if ($hasSize) {
            return ['Size'];
        }

        return [''];
    }

    /**
     * Returns an array of the product's options including their available and currently selected values.
     *
     * Input
     * {% for option in product.options_with_values %}
     *   <label>
     *     {{ option.name }}
     *     <select>
     *       {% for value in option.values %}
     *         <option {% if option.selected_value == value %}selected{% endif %}>
     *           {{ value }}
     *         </option>
     *       {% endfor %}
     *     </select>
     *   </label>
     * {% endfor %}
     *
     * Output
     * <label>
     *   Color
     *   <select>
     *     <option selected>Red</option>
     *     <option>Green</option>
     *   </select>
     * </label>
     */
    public function options_with_values()
    {
        return [
            new ProductOption($this->product, 'color'),
            new ProductOption($this->product, 'size'),
        ];
    }

    /**
     * Returns if the product is on sale.
     */
    public function is_on_sale()
    {
        foreach ($this->product->inventory as $variant) {
            if ($variant->priceOrSalePrice() < $variant->price) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the lowest price of all the product's variants. This is the same as
     * product.min_price. Use a money filter to return the value in a monetary format.
     */
    public function price()
    {
        $min = null;

        foreach ($this->product->inventory as $variant) {
            if (is_null($min)) {
                $min = $variant->priceOrSalePrice();
            }

            $min = min($min, $variant->priceOrSalePrice());
        }

        return dollars_to_cents($min);
    }

    /**
     * Returns the lowest price of the product. Use one of the money filters to
     * return the value in a monetary format.
     */
    public function min_price()
    {
        return $this->price();
    }

    /**
     * Returns the lowest price of the product. Use one of the money filters to
     * return the value in a monetary format.
     */
    public function price_min()
    {
        return $this->price();
    }

    /**
     * Returns the highest price of the product. Use one of the money filters to
     * return the value in a monetary format.
     */
    public function price_max()
    {
        $max = null;

        foreach ($this->product->inventory as $variant) {
            if (is_null($max)) {
                $max = $variant->priceOrSalePrice();
            }

            $max = max($max, $variant->priceOrSalePrice());
        }

        return dollars_to_cents($max);
    }

    /**
     * Returns true if the product's variants have varying prices. Returns false
     * if all of the product's variants have the same price.
     */
    public function price_varies()
    {
        for ($i = 0; $i < count($this->product->inventory) - 1; $i++) {
            $variant = $this->product->inventory[$i];
            $next = $this->product->inventory[$i+1];

            if ($variant->priceOrSalePrice() != $next->priceOrSalePrice()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the variant object of the currently-selected variant if there is a
     * valid ?variant= parameter in the URL. Returns nil if there is not.
     *
     * Input
     * <!-- URL = myshop.myshopify.com/products/shirt?variant=124746062 -->
     * {{ product.selected_variant.id }}
     *
     * Output
     * 124746062
     */
    public function selected_variant()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the variant object of the currently-selected variant if there is
     * a valid ?variant= query parameter in the URL. If there is no selected
     * variant, the first available variant is returned. In order for a variant to
     * be available, its variant.inventory_quantity must be greater than zero or
     * variant.inventory_policy must be set to continue. A variant with no
     * inventory_management is considered available.
     */
    public function selected_or_first_available_variant()
    {
        $variantId = null;
        if (isset(CurrentView::$queryString['variant']) && is_numeric(CurrentView::$queryString['variant'])) {
            $variantId = CurrentView::$queryString['variant'];
        }

        // the selected query string variant
        foreach ($this->product->inventory as $variant) {
            if (! is_null($variantId) && $variant->id == $variantId && $variant->quantity > 0) {
                return new Variant($variant, $this->product);
            }
        }

        // the first available variant
        foreach ($this->product->inventory as $variant) {
            if ($variant->quantity > 0) {
                return new Variant($variant, $this->product);
            }
        }

        if (isset($this->product->inventory[0])) {
            return new Variant($this->product->inventory[0], $this->product);
        }

        return null;
    }

    /**
     * Returns an array of all of the product's tags. The tags are returned in alphabetical order.
     *
     * Input
     * {% for tag in product.tags %}
     *     {{ tag }}
     * {% endfor %}
     *
     * Output
     * new
     * leather
     * sale
     * special
     */
    public function tags()
    {
        $tags = [];

        foreach ($this->product->tags as $tag) {
            $tags[] = $tag->name;
        }

        return $tags;
    }

    /**
     * Returns the name of the custom product template assigned to the product, without
     * the product. prefix nor the .liquid extension. Returns nil if a custom template
     * is not assigned to the product.
     *
     * Input
     * <!-- on product.wholesale.liquid -->
     * {{ product.template_suffix }}
     *
     * Output
     * wholesale
     */
    public function template_suffix()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the title of the product.
     */
    public function title()
    {
        return $this->product->product_name;
    }

    /**
     * Returns the type of the product.
     */
    public function type()
    {
        // todo: add the supported shopify produc types
        return $this->product->product_type;
    }

    /**
     * Returns the relative URL of the product.
     *
     * Input
     * {{ product.url }}
     *
     * Output
     * /products/awesome-shoes
     */
    public function url()
    {
        return '/products/' . $this->product->url;
    }

    /**
     * Returns an array of the product's variants.
     */
    public function variants()
    {
        $variants = [];

        foreach ($this->product->inventory as $variant) {
            $variants[] = new Variant($variant, $this->product);
        }

        return $variants;
    }

    /**
     * Returns the vendor of the product.
     */
    public function vendor()
    {
        return $this->product->brand;
    }

    /**
     * Search results have an additional attribute titled object_type which returns the
     * type of each result. This is useful for writing the logic of your search results loop.
     */
    public function object_type()
    {
        return 'product';
    }

    /**
     * Return the date when the product was published.
     *
     * Format: 2017-09-25T11:25:41-05:00
     */
    public function published_at()
    {
        if (is_numeric($this->product->published_at)) {
            return date('c', $this->product->published_at);
        }

        return date('c', strtotime($this->product->created_at));
    }

    /**
     * Return the date when the product was created.
     *
     * Format: 2017-09-25T11:25:41-05:00
     */
    public function created_at()
    {
        return date('c', strtotime($this->product->created_at));
    }
}
