<?php

namespace App\OnlineStore\LiquidTags;

use Liquid\AbstractBlock;
use Liquid\Decision;
use Liquid\Context;
use Liquid\Liquid;
use Liquid\LiquidException;
use Liquid\FileSystem;
use Liquid\Regexp;

trait RenderTrait
{
    /**
     * OVERWRITE renderAll from AbstractBlock.
     *
     * Renders all the given nodelist's nodes
     *
     * @param array $list
     * @param Context $context
     *
     * @return string
     */
    protected function renderAll(array $list, Context $context) {
        $result = '';

        $before = null;

        foreach ($list as $token) {
            // if a new line follows an {% endif %} remove it
            if ($before == 'TagIf' && is_string($token) && substr($token, 0, 1) == "\n") {
                $token = substr($token, 1);
            }

            $result .= (is_object($token) && method_exists($token, 'render')) ? $token->render($context) : $token;

            // set the previous token type
            if (is_object($token) && get_class($token) == "App\OnlineStore\LiquidTags\TagIf") {
                $before = 'TagIf';
            } else {
                $before = null;
            }

            if (isset($context->registers['break'])) {
                break;
            }
            if (isset($context->registers['continue'])) {
                break;
            }
        }

        return $result;
    }
}
