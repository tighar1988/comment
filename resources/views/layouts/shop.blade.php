<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" / >
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ shop_name() }}</title>

    {{-- todo: refactor meta tags; add custom content for each shop subdomain --}}
    {{--
    <meta property="og:url" content="{{ shop_url() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ shop_name() }}" />
    <meta property="og:description" content="{{ shop_description() }}" />
    <meta property="og:image" content="https://example.com/image.jpg" />
    --}}

    <link rel="stylesheet" href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ cdn('/jquery-ui-1.12.1/jquery-ui.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ cdn('/css/bootstrap.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ cdn('/css/buttons.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ cdn('/css/dd.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ cdn('/css/app.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ cdn('/css/style.css?v=2') }}" type="text/css"/>

    @yield('styles')
</head>
<body>
@yield('content')

<script type="text/javascript" src="{{ cdn('/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ cdn('/js/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ cdn('/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ cdn('/js/jquery.countdown.min.js?v=2') }}"></script>
<script type="text/javascript" src="{{ cdn('/js/jquery.dd.min.js') }}"></script>
<script type="text/javascript" src="https://checkout.stripe.com/checkout.js"></script>
{{ script_raven() }}

<script type="text/javascript">

    var shopId = {!! json_encode(shop_id()) !!};

    window.fbAsyncInit = function() {
        FB.init({
            appId   : '{{ fb_app_id() }}',
            xfbml   : true,
            version : '{{ fb_api_version() }}'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function facebookShare() {
        FB.ui({
            method: 'feed',
            link: 'https://www.facebook.com/groups/{{ shop_setting('facebook.share-group-id') }}/',
            caption: '{{ shop_name() }} - {{ shop_description() }}',
        }, function(response){
            if (response) {
                if (response.error_message) {
                    alert("You did not share the group :(");
                    return true;
                }

                if (response.post_id) {
                    alert("Thanks for sharing!");
                }
            }
       });
    }

    // todo: remove onclick="showCouponBox()" type of functions from onclick="" and add event listeners in $(document).ready() to prevent js errors when users click the buttons on mobiles before the jquery library is loaded
    function showCouponBox() {
        $('#coupon-wrapper').show();
    }

    function couponApply() {
        $.ajax({
            type:'post',
            url:'/api/account/apply-coupon',
            data: {
                code: $('#coupon-code').val()
            },
            success: function(response) {
                if (response.status == 1) {
                    alert('Coupon applied successfully.');
                    window.location.reload();
                } else {
                    if (shopId == 'cheekys') {
                        alert('Hey girl, seems like you have a code error. If its a free gift code remember you have to have a NON Bitty Bag item in the cart and no spaces in the code.  If you are still having issues please give us a call at 208-278-5007 during business hours or shoot us a PM on Facebook. We think the reason the error happened is: ' + response.reason);
                    } else {
                        alert('We are unable to validate this coupon.');
                    }
                }
            }
        });
    }

    function checkCouponApply(e) {
        // if enter was pressed
        if (e.which == 13) {
            couponApply();
        }
    }

    function applyBalance() {
        $.ajax({
            type:'post',
            url:'/api/account/apply-balance',
            data: {},
            success: function(response) {
                if (response.status == 1) {
                    alert(response.message);
                    window.location.reload();
                } else {
                    alert(response.message);
                }
            }
        });
    }

    function removeFromCart(itemId, button) {
        if (! confirm('Are you sure you want to remove this item from your shopping cart?')) {
            return;
        }

        $(button).prop('disabled', true);
        $.ajax({
            type:'post',
            url:'/api/account/remove-from-cart',
            data: {
                item_id: itemId
            },
            success: function(response) {
                if (response.status == 1) {
                    alert('Item removed successfully.');
                    window.location.reload();
                } else if (response.status == 0) {
                    alert(response.message);
                    window.location.reload();

                } else {
                    alert('An error occured.');
                    $(button).prop('disabled', false);
                }
            }
        });
    }

    function removeFromWaitlist(itemId) {
        if (! confirm('Are you sure you want to remove this item from your waitlist?')) {
            return;
        }

        $.ajax({
            type:'post',
            url:'/api/account/remove-from-waitlist',
            data: {
                item_id: itemId
            },
            success: function(response) {
                if (response.status == 1) {
                    alert('Item removed successfully.');
                    window.location.reload();
                } else {
                    alert('An error occured.');
                }
            }
        });
    }

    function closeAddressPopup() {
        $('#address-modal-overlay').hide();
        $('#address-modal').hide();
    }

    function showAddressPopup() {
        $('#address-modal-overlay').show();
        $('#address-modal').show();
    }

    function saveNewAddress() {
        $('#address-modal .btn-save').button('loading');
        var countryCode = $('#modal-country option:selected').val();
        if (countryCode != 'CA' && countryCode != 'US') {
            countryCode = 'US';
        }

        if (countryCode == 'CA') {
            var stateValue = $('#modal-state-text').val();
        } else {
            var stateValue = $('#modal-state option:selected').val();
        }

        $.ajax({
            type:'post',
            url:'/api/account/update-address',
            dataType: 'json',
            data: {
                street_address: $('#modal-street-address').val(),
                city: $('#modal-city').val(),
                country_code: countryCode,
                state: stateValue,
                zip: $('#modal-zip').val(),
                apartment: $('#modal-apartment').val(),
                phone_number: $('#modal-phone-number').val()
            },
            success: function(response) {
                closeAddressPopup();
                alert('Address updated!');
                location.reload();
            },
            error: function(data) {
                $('#address-modal .btn-save').button('reset');
                var errors = data.responseJSON;
                alert(errors[Object.keys(errors)[0]]);
            }
        });
    }

    function confirmOnboardingPopup(event) {
        $('#onboarding-modal button').button('loading');
        $.ajax({
            type:'post',
            url:'/api/account/update-email',
            dataType: 'json',
            data: {
                email: $('#onboarding-modal .onboarding-email').val()
            },
            success: function(response) {
                if (typeof confirmOnboardingOptIn === "function") {
                    confirmOnboardingOptIn();
                }

                location.reload();
            },
            error: function(data) {
                $('#onboarding-modal button').button('reset');
                var errorMessage = 'Invalid email address.';
                try {
                    errorMessage = data.responseJSON[Object.keys(data.responseJSON)[0]];
                } catch (e) {}
                $('#onboarding-modal .help-block').show().html('<strong>'+errorMessage+'</strong>');
            }
        });

        event.preventDefault();
        return false;
    }

    function updateEmail() {
        $.ajax({
            type:'post',
            url:'/api/account/update-email',
            dataType: 'json',
            data: {
                email: $('#customer-email').val()
            },
            success: function(response) {
                alert('Email updated!');
                location.reload();
            },
            error: function(data) {
                $('#address-modal .btn-save').button('reset');
                var errors = data.responseJSON;
                alert(errors[Object.keys(errors)[0]]);
            }
        });
    }

    function enableEmailForm() {
        $('#customer-email').show().attr('class', '').prop('disabled', false);
        $('.edit-email').hide();
        $('#email-form').show();
    }

    function deliveryMethod(method) {
        if (method == 'Local') {
            if (! confirm('Are you sure to switch to Local Pickup?')) {
                return;
            }
            if ($('#local-pickup-locations-modal').length) {
                // confirm store from multiple local pickup locations
                $('#local-pickup-locations-modal').modal('show');
            } else {
                updateDeliveryMethod(method);
            }

        } else {
            updateDeliveryMethod(method);
        }
    }

    function updateLocalPickupStoreLocation() {
        $('#local-pickup-locations-modal button').button('loading');
        var locationId = $('#local-pickup-locations-modal select').val();
        updateDeliveryMethod('Local', locationId);
    }

    function updateDeliveryMethod(method, locationId) {
        $.ajax({
            type:'post',
            url:'/api/account/set-delivery-method',
            data: {
                method: method,
                location_id: locationId
            },
            success: function(response) {
                $(".overlay").hide();
                if (response.status == 1) {
                    alert('Delivery method updated!');
                    location.reload();
                } else {
                    alert('An unexpected error occured.');
                    $('#local-pickup-locations-modal').modal().hide();
                }
            }
        });
    }

    function checkoutWithNewCard(token) {
        $.ajax({
            type:'post',
            url:'/api/checkout/stripe-new-card',
            data: {
                token: token
            },
            success: function(response) {
                $(".overlay").hide();
                if (response.status == 1) {
                    alert("Payment successfully applied! Thanks for your order!");
                    location.reload();
                } else {
                    alert(response.message);
                }
            }
        });
    }

    function checkoutWithStripeCard(cardId) {
        $.ajax({
            type:'post',
            url:'/api/checkout/stripe-existing-card',
            data: {
                card_id: cardId
            },
            success: function(response) {
                $(".overlay").hide();
                if (response.status == 1) {
                    alert("Payment successfully applied! Thanks for your order!");
                    location.reload();
                } else {
                    alert(response.message);
                }
            }
        });
    }

    function checkoutWithPaypal() {
        $.ajax({
            type:'post',
            url:'/api/checkout/paypal-approval-link',
            data:{},
            success: function(response) {
                $(".overlay").hide();
                if (response.status == 1) {
                    if (response.link == 'PAID_TOTAL_0') {
                        alert('Thank you for your order!');
                        window.location.reload();
                    } else {
                        alert('Created Payment using PayPal. Redirecting to PayPal for your approval.');
                        window.location = response.link;
                    }
                } else {
                    alert(response.message);
                }
            }
        });
    }

    function checkoutWithAccountCredit() {
        $.ajax({
            type:'post',
            url:'/api/checkout/account-credit',
            data: {},
            success: function(response) {
                $(".overlay").hide();
                if (response.status == 1) {
                    alert("Credit successfully applied! Thanks for your order!");
                    location.reload();
                } else {
                    alert(response.message);
                }
            }
        });
    }

    function initAccordion(elementId) {
        $(elementId).accordion({
            collapsible: true,
            active: false,
            clearStyle: true,
            autoHeight: false,
            header: '.accordion-head', // point to the class that is used as a header
            heightStyle: 'content',
            icons: false,
            beforeActivate: function (event, ui) {
                var self = $(this);

                // this bit is tricky. Make sure you have no empty space in the body of drop-down
                if (ui.newPanel.html() == '') {
                    // taking data-url parameter from accordion header div
                    // and load the returned contents into the accordion body.
                    // presuming HTML is returned.
                    ui.newPanel.load(self.data('url'));
                }
            }
        });
    }

    function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = degreeToRadians(lat2 - lat1);
        var dLon = degreeToRadians(lon2 - lon1);

        // the haversine formula
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(degreeToRadians(lat1)) * Math.cos(degreeToRadians(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
        return 2 * R * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    }

    function degreeToRadians(deg) {
        return deg * (Math.PI / 180);
    }
</script>
@yield('scripts')
{!! shop_setting('embed-code') !!}
</body>
</html>
