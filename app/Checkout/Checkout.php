<?php

namespace App\Checkout;

use App\Models\ShopUser;
use App\Models\OrderProduct;
use App\Models\Order;
use App\Models\BalanceMovement;
use App\Checkout\CheckoutException;
use App\Checkout\PayPal;
use Exception;
use App\Mails\ShopMailer;
use App\Checkout\Fees;
use App\Facebook\FacebookMessenger;
use App\Events\OrderPaid;
use App\Events\GiftCardProduct;
use App\Jobs\SplitOrder;

class Checkout
{
    /**
     * The customer.
     *
     * @var App\Models\ShopUser
     */
    protected $customer;

    protected $cart;

    public function __construct(ShopUser $customer)
    {
        $this->customer = $customer;

        $this->cart = $customer->shoppingCart();
    }

    protected function validateCartNotEmpty()
    {
        if ($this->cart->count() == 0) {
            throw new CheckoutException('Your shopping cart is empty. Your items may have expired. Please refresh the page and try again.');
        }

        if (shop_id() == 'cheekys') {
            $freeItemVariantId = 2960;
            $hasNonBittyBoxProduct = false;
            $isFreeItemInCart = false;

            foreach ($this->cart->items as $item) {
                if ($freeItemVariantId == $item->inventory_id) {
                    $isFreeItemInCart = true;
                }
            }

            if ($isFreeItemInCart) {
                foreach ($this->cart->items as $item) {
                    $sku = trim(strtolower($item->product_style));
                    $name = trim(strtolower($item->name));

                    if (! empty($sku) && ! str_contains($sku, 'bitty') && ! str_contains($name, 'bitty') && $freeItemVariantId != $item->inventory_id) {
                        // if have a sku and the sku does not contains 'bitty'
                        $hasNonBittyBoxProduct = true;
                    }
                }

                if (! $hasNonBittyBoxProduct) {
                    throw new CheckoutException('To receive the free gift, you must have a non bitty box item in your shopping cart!');
                }
            }
        }
    }

    protected function validateShippingAddress()
    {
        if (local_pickup_enabled() && $this->customer->local_pickup) {
            // local pickup option
            return;
        }

        // verify address is set
        if (empty($this->customer->street_address)) {
            throw new CheckoutException('You have to provide your shipping address to continue');
        }
    }

    /**
     * Add a new card in stripe, set it as default, store the new card to the db, then charge the card.
     */
    public function withNewCard($stripeToken)
    {
        $this->validateShippingAddress();
        $this->validateCartNotEmpty();

        try {
            $cardId = $this->customer->addNewCard($stripeToken);

            if ($this->cart->totalInCents() < 50) {
                $this->createOrder('stripe', 'Stripe New Card Skipped - $0 total');

            } else {
                $feeAmount = (new Fees(shop_id(), shop_setting('shop.fees-billing-plan')))->getFromCart($this->cart);
                $charge = $this->customer->charge($cardId, $this->cart->totalInCents(), $feeAmount);
                $this->createOrder('stripe', $charge->id);
            }

        } catch(\Stripe\Error\Card $e) {
            log_error("[stripe][new-card] $e");
            throw new CheckoutException($e->getMessage());

        } catch(Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[stripe][new-card] $e");
            throw new CheckoutException('An unexpected error occurred');
        }
    }

    /**
     * Charge guest stripe card.
     */
    public function guestWithCard($stripeToken)
    {
        $this->validateShippingAddress();
        $this->validateCartNotEmpty();

        try {

            if ($this->cart->totalInCents() < 50) {
                $this->createOrder('stripe-guest', 'Stripe Guest Card Skipped - $0 total');
            } else {
                $feeAmount = (new Fees(shop_id(), shop_setting('shop.fees-billing-plan')))->getFromCart($this->cart);
                $charge = $this->customer->chargeToken($stripeToken, $this->cart->totalInCents(), $feeAmount);
                $this->createOrder('stripe-guest', $charge->id);
            }

        } catch(\Stripe\Error\Card $e) {
            log_error("[stripe][guest] $e");
            throw new CheckoutException($e->getMessage());

        } catch(Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[stripe][guest] $e");
            throw new CheckoutException('An unexpected error occurred');
        }
    }

    /**
     * Charge existing card.
     */
    public function withExistingCard($cardId)
    {
        $this->validateShippingAddress();
        $this->validateCartNotEmpty();

        // todo: treat case when shop owner manually deletes cards from stripe dirrectly (thus invalidating db cards)
        //      it could cause problems only for a handful of users
        //      check if the card exists when charging it; I think when charging the card id, we
        //      can check if the card exists, so no need for webhooks; in that case remove it and
        //      reload the page for the user with a pretty error message saying the card was invalid;
        //      and this time the card would be removed from the dropdown

        try {
            if ($this->cart->totalInCents() < 50) {
                $this->createOrder('stripe', 'Stripe Card Skipped - $0 total');

            } else {
                $feeAmount = (new Fees(shop_id(), shop_setting('shop.fees-billing-plan')))->getFromCart($this->cart);
                $charge = $this->customer->charge($cardId, $this->cart->totalInCents(), $feeAmount);
                $this->createOrder('stripe', $charge->id);
            }

        } catch(\Stripe\Error\Card $e) {
            log_error("[stripe][existing-card] $e");
            throw new CheckoutException($e->getMessage());

        } catch(Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[stripe][existing-card] $e");
            throw new CheckoutException('An unexpected error occurred');
        }
    }

    /**
     * Checkout fully with account balance.
     */
    public function withAccountCredit()
    {
        $this->validateShippingAddress();
        $this->validateCartNotEmpty();

        try {
            if (! $this->cart->isFullyPaidWithBalance) {
                throw new Exception('Cart was not fully paid with balance.');
            }

            $this->createOrder('stripe', 'CREDIT');

        } catch(Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[account-credit-checkout] $e");
            throw new CheckoutException('An unexpected error occurred');
        }
    }

    /**
     * Get the approval link for paypal.
     */
    public function paypalApprovalLink()
    {
        $this->validateShippingAddress();
        $this->validateCartNotEmpty();

        try {

            if ($this->cart->total == 0) {
                $this->createOrder('PayPal', 'PayPal Skipped - $0 total');
                return 'PAID_TOTAL_0';

            } else {
                $paypal = new PayPal;
                return $paypal->getApprovalLink($this->cart);
            }

        } catch(Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[paypal] $e");
            throw new CheckoutException('An unexpected error occurred. The administrator for this shop must enable paypal payments first');
        }
    }

    /**
     * PayPal checkout. Execute the PayPal payment.
     */
    public function checkoutWithPayPal($paymentId, $payerID, $customer)
    {
        $this->validateShippingAddress();
        $this->validateCartNotEmpty();

        try {
            $paypal = new PayPal;
            $payment = $paypal->executePayment($paymentId, $payerID, $customer->shoppingCart());

            $transactions = $payment->getTransactions();
            $relatedResources = $transactions[0]->getRelatedResources();
            $sale = $relatedResources[0]->getSale();
            $transactionId = $sale->getId();

            // todo: make sure the payment is executed before creating the order
            $this->createOrder('paypal', $paymentId, $transactionId);

        } catch(Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[paypal] $e");
            throw new CheckoutException('This shop does not accept e-checks or your payment was declined, please try another payment method within PayPal');
        }
    }

    public function checkoutWithPayPalMobile($paymentId, $customer)
    {
        $paypal = new PayPal;

        $transactionId = null;

        try {
            $payment = $paypal->validatePaymentMobile($paymentId, $customer->shoppingCart());

            if (!isset($payment->id)) {
                throw new Exception('no such payment');
            }
            $transactions = $payment->getTransactions();
            $relatedResources = $transactions[0]->getRelatedResources();

            if (isset($relatedResources[0])) {
                $sale = $relatedResources[0]->getSale();
                $transactionId = $sale->getId();
            }

            if (empty($transactionId)) {
                throw new Exception('Transaction id is empty');
            }

            $this->createOrder('paypal', $paymentId, $transactionId);

        } catch (Exception $e) {
            throw new CheckoutException($e->getMessage());
        }
    }

    /**
     * Admin creates and confirms order manually.
     */
    public function withAdminCreatedOrder()
    {
        try {
            $order = $this->storeOrderToDB('ADMIN', Order::STATUS_PAID);
            log_notice("[admin] Admin created order #{$order->id} for {$this->customer->name}. Total: \${$this->cart->total}.");

            event(new OrderPaid($order->id));

            if (shop_id() == 'cheekys') {
                dispatch(new SplitOrder(shop_id(), $order->id));
            }

        } catch(Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[admin] $e");
            throw new CheckoutException('An unexpected error occurred');
        }
    }

    /**
     * @todo - treat case in the UI where the user has no email on file, maybe force the user to create an email by no allowing checkout if he does not have an email or address on file
     *       - treat case where the payment goes through, but there was a problem emptying the cart
     *       and storing the order into the db
     */
    public function createOrder(string $paymentGateway, $paymentRef, $transactionId = null)
    {
        $order = $this->storeOrderToDB($paymentRef, Order::STATUS_PAID, $transactionId);

        log_notice("[$paymentGateway] Customer {$this->customer->name} paid \${$this->cart->total} for their order {$order->id}");

        event(new OrderPaid($order->id));

        (new ShopMailer)->sendOrderPaidEmail($order);
        FacebookMessenger::notifyPaymentReceipt($this->customer, $order);

        if (shop_id() == 'cheekys') {
            dispatch(new SplitOrder(shop_id(), $order->id));
        }
    }

    public function storeOrderToDB($paymentRef, $orderStatus, $transactionId = null)
    {
        $order = Order::create([
            'customer_id'       => $this->customer->id,
            'street_address'    => $this->customer->street_address,
            'apartment'         => $this->customer->apartment,
            'city'              => $this->customer->city,
            'state'             => $this->customer->state,
            'country_code'      => $this->customer->country_code,
            'zip'               => $this->customer->zip,
            'email'             => $this->customer->email,
            'order_status'      => $orderStatus,
            'payment_ref'       => $paymentRef, // can be 'CREDIT', stripe charge_id, paypal id
            'transaction_id'    => $transactionId, // paypal transaction id
            'payment_amt'       => $this->cart->total,
            'ship_charged'      => $this->cart->shipping_price,
            'ship_cost'         => 0, // the shipping cost, added after buying the labels
            'subtotal'          => $this->cart->subtotal,
            'total'             => $this->cart->total,
            'fbid'              => $this->customer->facebook_id,
            'ship_name'         => $this->customer->name,
            'payment_date'      => time(),
            'shipped_date'      => null,
            'tracking_number'   => null,
            'state_tax'         => $this->cart->state_tax,
            'county_tax'        => $this->cart->county_tax,
            'municipal_tax'     => $this->cart->municipal_tax,
            'tax_total'         => $this->cart->tax_total,
            'coupon'            => $this->cart->coupon_code,
            'coupon_discount'   => $this->cart->coupon_discount,
            'calculated_zip'    => null, // todo: calculated after Endicia api call
            'apply_balance'     => $this->cart->deduced_balance,
            'local_pickup'      => $this->getLocalPickup(),
            'store_location_id' => $this->customer->location_id,
        ]);

        foreach ($this->cart->items as $item) {
            OrderProduct::store($order->id, $item);

            if ($item->product_type == 'giftcard') {
                event(new GiftCardProduct($this->customer->id, $item->inventory_id, $order->id));
            }
        }

        $balance = $this->cart->deduced_balance;
        if ($balance > 0) {
            $this->customer->reduceBalance($balance, "Customer used balance {$balance} for order #{$order->id}");
        }

        $unusedAmount = $this->cart->unusedCouponAmount();
        if ($unusedAmount > 0) {
            $this->customer->addBalance($unusedAmount, "Customer was given balance {$unusedAmount} for remaining coupon amount, for order #{$order->id}");
        }

        $this->cart->empty();

        return $order;
    }

    public function getLocalPickup()
    {
        if (local_pickup_enabled()) {
            return $this->customer->local_pickup;
        }

        return false;
    }
}
