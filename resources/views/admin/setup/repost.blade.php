@extends('admin.setup.setup')

@section('section', 'Auto-Scheduler')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                @if (shop_setting('repost-enabled'))
                    <span class="settings-info">Auto Scheduler for Posts <span class="label label-success">Enabled</span></span>
                    <small class="settings-muted text-muted">Old posts will automatically be rescheduled when you have enough inventory.</small>
                    <form class="" role="form" method="POST" action="{{ url('/admin/setup/repost/disable') }}">
                        <button class="btn btn-danger" type="submit">Disable Auto Scheduler</button>
                    </form>
                @else
                    <span class="settings-info">Auto Scheduler for Posts <span class="label label-warning">Disabled</span></span>
                    <small class="settings-muted text-muted">Old posts can be scheduled to be reposted automatically if there is enough inventory.</small>
                    <form class="" role="form" method="POST" action="{{ url('/admin/setup/repost/enable') }}">
                        <button class="btn btn-success" type="submit">Enable Auto Scheduler</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="form" role="form" method="POST" action="{{ url('/admin/setup/repost/min-quantity') }}">
                    <span class="settings-info">Minimum quantity</span>
                    <span class="settings-muted">The minimum number of quantity the product in the post must have in order for the post to be reposted.</span>
                    <div class="form-group @hasError('minimum_quantity')">
                        <label class="control-label">Quantity</label>
                        <input type="text" class="form-control" name="minimum_quantity" value="{{ old('minimum_quantity', shop_setting('repost.minimum-quantity', 10)) }}">
                        @error('minimum_quantity')
                    </div>
                    <button class="btn btn-success" type="submit">Update Quantity</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="form" role="form" method="POST" action="{{ url('/admin/setup/repost/days-since-last-post') }}">
                    <span class="settings-info">Minimum days</span>
                    <span class="settings-muted">The minimum number of days since last (re)post.</span>
                    <div class="form-group @hasError('number_of_days')">
                        <label class="control-label">Number of Days</label>
                        <input type="text" class="form-control" name="number_of_days" value="{{ old('number_of_days', shop_setting('repost.days-since-last-post', 7)) }}">
                        @error('number_of_days')
                    </div>
                    <button class="btn btn-success" type="submit">Update Number of Days</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Schedule time(s)</span>
                <span class="settings-muted">The time(s) in which to schedule reposts daily. You can add multiple times. A single post will be scheduled for each time. If you expect multiple reposts, add more times.</span>

                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <th>Hour</th>
                                    <th>Minute</th>
                                    <th>AM/PM</th>
                                    <th>Manage</th>
                                </thead>
                                <tbody>
                                    @if (empty($times))
                                        <tr>
                                            <td colspan="4">You don't have any times selected.</td>
                                        </tr>
                                    @endif
                                    @foreach ($times as $time)
                                        <tr>
                                            <td>{{ $time->hour }}</td>
                                            <td>{{ $time->min }}</td>
                                            <td>{{ strtoupper($time->ampm) }}</td>
                                            <td>
                                                <form class="form" role="form" method="POST" action="{{ url('/admin/setup/repost/remove-time') }}">
                                                    <input type="hidden" name="hour" value="{{ $time->hour }}">
                                                    <input type="hidden" name="min" value="{{ $time->min }}">
                                                    <input type="hidden" name="ampm" value="{{ $time->ampm }}">
                                                    <button class="btn btn-danger btn-small" type="submit"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <form class="form" role="form" method="POST" action="{{ url('/admin/setup/repost/add-time') }}">
                    <div class="form-group">
                        <label class="control-label"><i>Scheduled Time</i></label>
                        <div class="row">
                            <div class="col-md-2">
                                <select class="form-control" name="hour">
                                    @for ($i = 0; $i <= 12; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" name="min">
                                    @for ($i = 0; $i <= 11; $i++)
                                        <?php
                                            $min = $i * 5;
                                            if (strlen($min) == 1) {
                                                $min = '0' . $min;
                                            }
                                        ?>
                                        <option value="{{$min}}">{{$min}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" name="ampm">
                                    <option value="am">AM</option>
                                    <option value="pm">PM</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-success" type="submit">Add Time</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Facebook Groups</span>
                <span class="settings-muted">What are the groups that will be used. In case of multiple groups, posts will be scheduled only to the same group they were posted in.</span>

                <div class="row">
                    <div class="col-md-4">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th>Group</th>
                                    <th>Manage</th>
                                </thead>
                                <tbody>
                                    @if (empty($enabledGroups))
                                        <tr>
                                            <td colspan="2">You don't have any enabled groups for reposting.</td>
                                        </tr>
                                    @endif
                                    @foreach ($groups as $group)
                                        @if (in_array($group['id'], $enabledGroups))
                                            <tr>
                                                <td>
                                                    <a class="" href="https://www.facebook.com/{{ $group['id'] }}" target="_blank">{{ $group['name'] }}</a>
                                                    @if (in_array($group['id'], $repostGroups))
                                                        &nbsp;<strong>(Enabled)</strong>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (in_array($group['id'], $repostGroups))
                                                        <form class="form-horizontal" role="form" method="POST" action="/admin/setup/repost/disable-group">
                                                            <input type="hidden" name="group_id" value="{{ $group['id'] }}">
                                                            <button type="submit" class="btn btn-danger">Disable Repost</button>
                                                        </form>
                                                    @else
                                                        <form class="form-horizontal" role="form" method="POST" action="/admin/setup/repost/enable-group">
                                                            <input type="hidden" name="group_id" value="{{ $group['id'] }}">
                                                            <button type="submit" class="btn btn-default">Enable Repost</button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
