<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'paypal' => [
        'client_id' => env('PAYPAL_CLIENT_ID'),
        'client_secret' => env('PAYPAL_CLIENT_SECRET'),
    ],

    'shopify' => [
        'app_key' => env('SHOPIFY_APP_KEY'),
        'app_secret' => env('SHOPIFY_APP_SECRET'),
        'redirect' => env('SHOPIFY_REDIRECT_URI', 'https://commentsold.com/admin/setup/shopify-connect/callback'),
        'webhook_callback' => env('SHOPIFY_WEBHOOK_CALLBACK', 'https://commentsold.com/shopify'),
    ],

    'endicia' => [
        'requester_id' => env('ENDICIA_REQUESTER_ID'),
        'account_id' => env('ENDICIA_ACCOUNT_ID'),
        'pass_phrase' => env('ENDICIA_PASS_PHRASE'),
    ],

    'instagram' => [
        'client_id' => env('INSTAGRAM_CLIENT_ID'),
        'client_secret' => env('INSTAGRAM_CLIENT_SECRET'),
        'redirect' => env('INSTAGRAM_REDIRECT_URI', 'https://commentsold.com/instagram-connect/callback'),
    ],

    'facebook' => [
        'client_id' => env('FB_APP_ID'),
        'client_secret' => env('FB_APP_SECRET'),
        'app_access_token' => env('FB_APP_ACCESS_TOKEN'),
        'redirect' => '',
        'webhook_callback' => env('FB_WEBHOOK_CALLBACK', 'https://commentsold.com/facebook'),
    ],

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'     => App\Models\User::class,
        'key'       => env('STRIPE_KEY'),
        'secret'    => env('STRIPE_SECRET'),
        'client_id' => env('STRIPE_CLIENT_ID'),
    ],

];
