<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use PDOException;

class DatabaseMigrateAllShops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:migrate-all-shops';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations for all shops.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Migrating all shops..");

        foreach (all_shops() as $shop) {
            try {
                $this->info("Migrating database: {$shop}");
                $this->call('database:migrate-shop', ['shop' => $shop]);
            } catch (QueryException $e) {
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                $this->error($e->getMessage());
            }
        }

        $this->info("Migration complete..");
    }
}
