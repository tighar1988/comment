<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use App\Models\Master;
use App\Models\ShopUser;
use App\Exceptions\NoShopException;
use Exception;

class Shop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->isShopSubdomain($request)) {
            $rootUrl = env('APP_URL');
            $shop = explode('.', $request->getHost())[0] ?? null;

            // if the shop has a custom domain we will force the user to use it
            $domain = Master::findDomainByShop($shop);
            if (! empty($domain)) {
                return redirect(rtrim($domain, '/') . $request->getRequestUri());
            }

        } else {
            // it's a custom domain, we will search for it in the main database
            $shop = Master::findShopByDomain($request->getHost());
            config(['shop.custom-domain' => $request->getSchemeAndHttpHost()]);
            $rootUrl = $request->getSchemeAndHttpHost();
        }

        if (is_null($shop)) {
            throw new NoShopException;
        }

        // set the default database
        set_shop_database($shop);
        DB::setDefaultConnection('shop');

        try {
            // check that the shop exists
            DB::connection('shop')->getPdo();
        } catch (Exception $e) {
            throw new NoShopException;
        }

        // set facebook login redirect url
        if ($shop == 'divas' || $shop == 'cheekys') {
            $domain = rtrim(env('APP_DOMAIN'), '/');
            $shop = str_replace('_', '-', shop_id());
            $redirectUrl = env('APP_SCHEMA', 'http')."://{$domain}/facebook-login/callback?shop={$shop}";
            Config::set('services.facebook.redirect', $redirectUrl);
        } else {
            $url = url();
            $url->forceRootUrl($rootUrl);
            Config::set('services.facebook.redirect', $url->to('/facebook-login/callback?shop='.$shop));
        }

        // set the default auth eloquent model
        Config::set('auth.providers.users.model', ShopUser::class);

        return $next($request);
    }

    public function isShopSubdomain($request)
    {
        return str_contains($request->url(), env('APP_DOMAIN'));
    }
}
