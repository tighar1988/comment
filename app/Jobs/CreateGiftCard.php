<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Traits\ReconnectShopDB;
use App\Mails\ShopMailer;
use App\Models\ShopUser;
use App\Models\GiftCard;
use App\Models\Inventory;
use Exception;

class CreateGiftCard implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    protected $shop;

    protected $customerId;

    protected $inventoryId;

    protected $orderId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop, $customerId, $inventoryId, $orderId)
    {
        $this->shop        = $shop;
        $this->customerId  = $customerId;
        $this->inventoryId = $inventoryId;
        $this->orderId     = $orderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $startTime = microtime(true);
            $this->reconnectDB($this->shop);
            log_info("[giftcard] Creating gift card...");

            $customer = ShopUser::find($this->customerId);
            $inventory = Inventory::find($this->inventoryId);
            $giftcard = new GiftCard;

            $giftcard->customer_id  = $this->customerId;
            $giftcard->order_id     = $this->orderId;
            $giftcard->inventory_id = $this->inventoryId;
            $giftcard->code         = $giftcard->getUniqueCode();
            $giftcard->hash         = $giftcard->getUniqueHash();
            $giftcard->value        = $inventory->price;
            $giftcard->disabled     = false;
            $giftcard->used         = false;
            $giftcard->save();

            (new ShopMailer)->sendIssuedGiftCardEmail($customer, $giftcard);

            log_info("[giftcard] Issued gift card '{$giftcard->code}' with value \$".amount($giftcard->value).". CID #{$this->customerId}");

        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[giftcard] [exception] {$e}");
        }
    }
}
