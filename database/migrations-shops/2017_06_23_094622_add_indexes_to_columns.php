<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_products', function (Blueprint $table) {
            $table->index(['order_id']);
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->index(['key']);
        });

        Schema::table('scan_log', function (Blueprint $table) {
            $table->index(['item_type', 'item_id']);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->index(['customer_id', 'order_status']);
        });

        Schema::table('shop_users', function (Blueprint $table) {
            $table->index(['instagram_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
