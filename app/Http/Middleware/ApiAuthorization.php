<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ShopUser;

class ApiAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jwt = $request->header('Authorization');

        $userId = cache('jwt:'.$jwt);
        if (empty($userId) || ! is_numeric($userId)) {
            return response()->json(['error' => 'Unauthenticated.'], 403);
        }

        return $next($request);
    }
}
