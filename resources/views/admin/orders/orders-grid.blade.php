@extends('layouts.admin')

@section('content')
<section class="content-header">
    <div class="row">
        <div class="col-sm-2"><h1>Orders</h1></div>
        <div class="col-sm-10">
            <div class="text-right ctrl-btn">
                @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                    <?php $query = request('with-inventory') ? '?with-inventory='.request('with-inventory') : ''; ?>

                    <form class="form page-btn" role="form" method="POST" action="{{ url('/admin/orders/process-all-open-orders') }}">
                        <button type="submit" title="Update to 'Processing' all Open Ordres" class="order-process-btn btn btn-info btn-sm">Process All</button>
                    </form>

                    <a href="/admin/orders/mass-create-labels" target="_blank" title="Create Labels for Processing Orders" class="page-btn btn btn-warning create-labels-btn btn-sm">Create Labels</a>

                    <a href="/admin/orders/open/print-local-pickup-packing-slips{{ $query }}" target="_blank" title="Print all Local Pickup Packing Slips for Processing Orders" class="page-btn btn btn-danger print-local-pickup-btn btn-sm">Print Local Pickup</a>

                    <a href="/admin/orders/open/print-labels{{ $query }}" target="_blank" title="Print all Labels for Processing Orders" class="page-btn btn btn-success print-labels-btn btn-sm">Print Shipping Labels</a>
                    {{--
                        <form class="form page-btn" id="process-selected" role="form" method="POST" action="{{ url('/admin/orders/process-selected-open-orders') }}">
                            <input type="hidden" name="order_ids" value="" id="selected-order-ids">
                            <button type="submit" title="Update to 'Processing' selected Open Ordres" class="order-process-selected-btn btn btn-warning btn-sm">Process Selected</button>
                        </form>
                    --}}
                @endif
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Orders</li>
    </ol>

    @if (isset($variant) && ! is_null($variant))
        <br/>
        Showing orders associated with: <strong>{{ $variant->product->product_name ?? null }}</strong> (Size: {{ $variant->size ?? 'NA' }} / Color: {{ $variant->color ?? 'NA' }})
    @endif
</section>
@include('admin.orders.howler')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom products">
                <ul class="nav nav-tabs">
                    <li class="{{ activeLink('admin/orders') }}"><a href="/admin/orders">All Orders</a></li>
                    <li class="{{ activeLink('admin/orders/open') }}"><a href="/admin/orders/open">Paid</a></li>
                    <li class="{{ activeLink('admin/orders/fulfilled') }}"><a href="/admin/orders/fulfilled">Fulfilled</a></li>
                    <li class="order-search">
                        <form method="GET" action="{{ form_action() }}">
                            <input type="text" name="q" placeholder="Search Order..."  value="{{ request('q') }}" class="form-control input-sm">
                        </form>
                    </li>
                </ul>
                <div class="tab-content r-p">
                    <div class="tab-pane active">
                        <div class="table-responsive">
                            <table id="orders" class="table table-condensed">
                                @if ($orders->isEmpty())
                                    <tr><td class="not-found" colspan="6">No orders found.</td></tr>
                                @endif
                                @foreach ($orders as $order)
                                    <tr class="order-row" height="148" data-orderFulfilled="{{ $order->isFulfilled() }}">
                                        <td class="orderid-column" data-orderId="{{ $order->id }}"><img src="{{ product_thumb($order->orderProducts->first()->product_filename ?? null) }}" alt="" class="img-circle"></td>
                                        <td>
                                            @if ($order->customer_id == 0)
                                                {{-- guest checkout --}}
                                                {{ $order->ship_name }} [Guest Checkout]
                                            @else
                                                <a href="/admin/customers/{{ $order->customer->id ?? null }}">{{ $order->customer->name ?? null }}</a>
                                            @endif
                                            <br/>
                                            Order id: {{ $order->id }} <br/>
                                            {{ style_date($order->created_at) }}
                                        </td>
                                        <td>
                                            {{ count($order->orderProducts) }} Items<br/>
                                            Subtotal: ${{ amount($order->subtotal) }}<br/>
                                            Shipping: ${{ amount($order->ship_charged) }}<br/>
                                            Tax: ${{ amount($order->tax_total) }}<br/>
                                            @if ($order->coupon_discount)
                                                Apply Coupon: -${{ amount($order->coupon_discount) }}
                                                <small class="text-muted">{{ $order->coupon }}</small><br/>
                                            @endif
                                            @if ($order->apply_balance)
                                                Apply Balance: -${{ amount($order->apply_balance) }}<br/>
                                            @endif
                                            Total: ${{ amount($order->total) }}
                                        </td>
                                        <td>
                                            @if ($order->payment_ref != '')
                                                #{{ $order->payment_ref }}
                                                @if ($order->transaction_id)
                                                    <small class="text-muted">{{ $order->transaction_id }}</small>
                                                @endif <br/>

                                                ${{ amount($order->payment_amt) }}
                                            @endif
                                            @if (isset($order->customer) && empty($order->customer->email))
                                                <small data-id="email" class="missing-info"><i class="fa fa-exclamation-triangle"></i> missing e-mail</small>
                                            @endif
                                            @if (isset($order->customer) && empty($order->customer->street_address))
                                                <small data-id="address" class="missing-info"><i class="fa fa-exclamation-triangle"></i> missing address</small>
                                            @endif
                                            @if ($order->local_pickup)
                                                <br/>Delivery: Local Pickup
                                                <a target="_blank" href="/admin/orders/open/print-local-pickup-packing-slips?orders={{ $order->id }}">Packing Slip</a>
                                            @else
                                                <br/>Delivery: Shipped
                                                @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                                                <a target="_blank" href="/admin/orders/open/print-labels?orders={{ $order->id }}">Packing Slip</a>
                                                @endif
                                            @endif
                                            @if ($order->tracking_number)
                                                <br/>Tracking #: {{ $order->tracking_number }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($order->isPaid())
                                                @if ($order->isProcessing())
                                                    <span class="label label-danger">Processing</span>
                                                @elseif ($order->isPrinted())
                                                    <span class="label label-info">Label Printed</span>
                                                @else
                                                    <span class="label label-warning">Unfulfilled</span>
                                                @endif
                                            @elseif ($order->isFulfilled())
                                                <span class="label label-success">Fulfilled</span>
                                                @if ($order->isLocalPickup() && $order->hasDetail('picked-up'))
                                                    <span class="label label-info">Picked Up</span>
                                                @endif
                                            @endif
                                        </td>
                                        <td width="50">
                                            <ul class="controls">
                                                <li onclick="editOrder({{ $order->id }}, this);">
                                                    <a title="Edit Order" class="editOrder"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </li>
                                                <li class="show-products-in-order" onclick="productsOrder({{ $order->id }}, this)">
                                                    <a title="View Products" class="viewProducts"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                </li>
                                                @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                                                <li onclick="createLabel({{ $order->id }}, this)">
                                                    <a title="Create Label" class="createLabel"><i class="fa fa-tag" aria-hidden="true"></i></a>
                                                </li>
                                                @if ($order->isPaid() && shop_id() == 'fashionten')
                                                    @can('view-inventory-logs')
                                                        <li onclick="fulfillOrder({{ $order->id }})">
                                                            <a title="Complete Fulfillment" class="initiateReturn"><i class="fa fa-gift" aria-hidden="true"></i></a>
                                                        </li>
                                                    @endcan
                                                @endif
                                                @if ($order->isPaid() && shop_id() != 'fashionten')
                                                    <li onclick="fulfillOrder({{ $order->id }})">
                                                        <a title="Complete Fulfillment" class="initiateReturn"><i class="fa fa-gift" aria-hidden="true"></i></a>
                                                    </li>
                                                @elseif ($order->isFulfilled())
                                                    <li onclick="returnOrder({{ $order->id }}, this)">
                                                        <a title="Initiate Return" class="initiateReturn"><i class="fa fa-reply" aria-hidden="true"></i></a>
                                                    </li>
                                                @endif
                                                @endif
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{ pagination_links($orders, ['q' => request('q'), 'with-inventory' => request('with-inventory')]) }}

                    </div>
                </div>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('styles')
<style type="text/css">
    .dataTables_wrapper #orders thead {
        display:none;
    }
    div.dataTables_filter {
        display: none;
    }
    #orders .order-row,
    .dataTables_wrapper #orders .order-row {
        height: 149px;
        overflow: hidden;
    }
    #orders .order-row:first-child,
    .dataTables_wrapper #orders .order-row:first-child {
        height: 148px;
    }
    #orders.dataTable {
        margin-top: -1px !important;
        margin-bottom: 20px !important;
    }
    #orders .not-found {
        padding: 20px;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">

    $(document).ready(function() {

        @if (is_numeric(request('order')))
            var $orderRow = $('.orderid-column[data-orderId='+{{ request('order') }}+']').closest('tr');

            @if (! request('scanned'))
                if ($orderRow.attr('data-orderFulfilled')) {
                    alertMessage('This order is already fulfilled!', 'Order Fulfilled');
                }
            @endif

            if ($orderRow.length) {
                productsOrder({{ request('order') }}, $('.show-products-in-order', $orderRow));
            }
        @endif

        var pressed = false;
        var chars = [];
        $(window).keypress(function(e) {
            /* check the keys pressed are numbers */
            chars.push(String.fromCharCode(e.which));
            /* Pressed is initially set to false so we enter - this variable is here to stop us setting a timeout everytime a key is pressed.
            It is easy to see here that this timeout is set to give us 1 second before it resets everything back to normal.
            If the keypresses have not matched the checks in the readBarcodeScanner function below then this is not a barcode */
            if (pressed == false) {
                setTimeout(function(){
                    /* check we have a long length e.g. it is a barcode/order/tracking # */
                    if (chars.length >= 3) {
                        /* join the chars array to make a string of the barcode scanned */
                        var barcode = chars.join("");
                        barcode = barcode.trim();
                        console.log("Barcode Scanned: " + barcode);

                        if (/^order-/i.test(barcode)) {

                            // search only in the order id column and use regex search to find exact match
                            var orderId = parseInt(barcode.substr(6));

                            // don't scan a new order if the current one is not fulfilled
                            if (typeof $orderRow != 'undefined' && ! $orderRow.attr('data-orderFulfilled')) {
                                alertMessage('Please fulfill the current order before scanning the next one!', 'Order Not Fulfilled');
                                errorSound.play();

                            } else if (! isNaN(orderId)) {
                                $.ajax({
                                    type:'post',
                                    url:'/admin/api/scan-barcode',
                                    data: {barcode: barcode},
                                    success: function(response) {
                                        if (response.status == 1) {
                                            window.location = window.location.origin + '/admin/orders' + '?order=' + orderId;
                                        } else {
                                            alert('Unknown scan error occured.');
                                        }
                                    }
                                });
                            }

                        } else if (/^i-/i.test(barcode) || (barcode.replace(/[^0-9]/g,"").length >= 5)) {
                            // check if it's our barcode (starts with i-) or is a shopify barcode (has at least 5 numbers)

                            var inventoryId, $itemRow, $unscannedItem;
                            var scanItem = false;

                            if (/^i-/i.test(barcode)) {
                                inventoryId = parseInt(barcode.substr(2));
                                if (! isNaN(inventoryId)) {
                                    $itemRow = $('.viewProductsData tr[data-inventoryid="'+inventoryId+'"]');
                                    $unscannedItem = $('.viewProductsData tr.unscanned[data-inventoryid="'+inventoryId+'"]');
                                    scanItem = true;
                                }

                                // try to search in the shopify barcode column
                                if ($itemRow.length == 0) {
                                    $itemRow = $('.viewProductsData tr[data-shopifyBarcode="'+barcode+'"]');
                                    $unscannedItem = $('.viewProductsData tr.unscanned[data-shopifyBarcode="'+barcode+'"]');
                                    scanItem = true;
                                }

                            } else {
                                $itemRow = $('.viewProductsData tr[data-shopifyBarcode="'+barcode+'"]');
                                $unscannedItem = $('.viewProductsData tr.unscanned[data-shopifyBarcode="'+barcode+'"]');
                                scanItem = true;
                            }

                            if (typeof $orderRow == 'undefined') {
                                alertMessage('Please scan an order before scanning an inventory item!', 'Order not Selected');

                            } else if (scanItem) {

                                // if there are multiple items with the same inventory id, select the first unscanned one
                                if ($unscannedItem.length > 1) {
                                    $unscannedItem = $($unscannedItem[0]);
                                }

                                if ($itemRow.length == 0) {
                                    alertMessage('The item you scanned does not exist in this order!', 'Wrong Invetory Item');
                                    errorSound.play();

                                } else if ($unscannedItem.length == 0) {
                                    alertMessage('This item was already scanned!', 'Invetory Item Already Scanned');

                                } else {
                                    $.ajax({
                                        type:'post',
                                        url:'/admin/api/scan-barcode',
                                        data: {barcode: barcode},
                                        success: function(response) {
                                            if (response.status == 1) {
                                                $unscannedItem.removeClass('danger').removeClass('unscanned').addClass('success');

                                                // if order is fulfilled
                                                if ($('.viewProductsData tr.unscanned').length == 0) {
                                                    setTimeout(function() {
                                                        fulfillOrderWithScan($('.orderid-column', $orderRow).attr('data-orderId'));
                                                    }, 500);
                                                }

                                            } else {
                                                alert('Unknown scan error occured.');
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                    chars = [];
                    pressed = false;
                }, 1000);
            }
            /* set press to true so we do not reenter the timeout function above */
            pressed = true;
        });
    });

    function fulfillOrderWithScan(orderId) {
        <?php $confirmFulfillment = shop_setting('setup.scan-confirm-fulfillment', true); ?>
        @if ($confirmFulfillment)
            var conf = confirm("All items scanned. Fulfill this order?");
            if (conf == true) {
        @endif
                $.ajax({
                    type:'post',
                    url:'/admin/orders/'+orderId+'/fulfill',
                    success: function(response) {
                        if (response.status == 1) {
                            @if ($confirmFulfillment)
                                alert('Order is fulfilled!');
                            @endif
                            window.location.href = window.location.href + '&scanned=true';
                        } else {
                            alertMessage('This order is already fulfilled! Do not ship this order!', 'Order Fulfilled');
                            errorSound.play();
                        }
                    }
                });
        @if ($confirmFulfillment)
            }
        @endif
    }
</script>

@endsection
