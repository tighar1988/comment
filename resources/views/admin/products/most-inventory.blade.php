@extends('layouts.admin')

@section('content')

<h2><a href="/admin/products">Products</a> &gt; Most Inventory</h2>

@if (count($inventory))
    @foreach($inventory as $item)
        Quantity: {{ $item->sum }} - Product: <a href='/admin/products/edit/{{ $item->product_id }}'>{{ $item->style }}</a><br/>   
    @endforeach
@else
    There are no products with inventory.
@endif

@endsection
