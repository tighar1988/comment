@extends('admin.setup.setup')

@section('section', 'My Invoices')

@section('sub-nav')
<div class="nav-subtabs-custom">
    <a class="{{ activeLink('admin/setup/invoices') }}" href="/admin/setup/invoices">PayPal Fees</a> |
    <a class="{{ activeLink('admin/setup/invoices/subscription-plan') }}" href="/admin/setup/invoices/subscription-plan">Subscription Plan</a>
    @if (usps_enabled())
        | <a class="{{ activeLink('admin/setup/invoices/prepaid-labels') }}" href="/admin/setup/invoices/prepaid-labels">Prepaid Labels</a>
    @endif
</div>
@endsection
