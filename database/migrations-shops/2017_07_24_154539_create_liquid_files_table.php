<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquid_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('theme')->nullable();
            $table->string('type')->nullable();
            $table->string('file_name')->nullable();
            $table->text('content')->nullable();
            $table->timestamps();

            $table->index(['type', 'theme', 'file_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('liquid_files');
    }
}
