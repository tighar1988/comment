<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramComment extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment_id',
        'post_id',
        'from_id',
        'from_username',
        'from_name',
        'from_profile_picture',
        'content',
        'created_time',
    ];

    public static function existingCommentsIds($postId)
    {
        return static::select('comment_id')->where('post_id', '=', $postId)->pluck('comment_id')->toArray();
    }

    public static function store($postId, $comment)
    {
        return static::create([
            'comment_id'           => $comment->id,
            'post_id'              => $postId,
            'from_id'              => $comment->fromId,
            'from_username'        => mysql_utf8($comment->fromUsername),
            'from_name'            => mysql_utf8($comment->fromName),
            'from_profile_picture' => $comment->fromProfilePicture,
            'content'              => mysql_utf8($comment->message),
            'created_time'         => $comment->createdTime,
        ]);
    }
}
