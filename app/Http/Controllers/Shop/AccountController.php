<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddressFormRequest;
use App\Http\Requests\EmailFormRequest;
use App\Http\Requests\DeliveryMethodFormRequest;
use App\Http\Requests\CartRemoveFormRequest;
use App\Http\Requests\WaitlistRemoveFormRequest;
use App\Models\Coupon;
use App\Models\GiftCard;
use App\Models\Waitlist;
use App\Models\ShopUser;
use App\Models\MultipleStoreLocation;
use App\ShoppingCart\ShoppingCart;
use App\Waitlist\ShoppingWaitlist;
use App\Exceptions\ShopifyWebhookLockedException;
use App\Mails\ShopMailer;
use App\OnlineStore\LiquidTemplate;
use App\OnlineStore\Objects\Shop;
use App\OnlineStore\Objects\Customer;
use App\OnlineStore\Objects\Cart;
use App\OnlineStore\Objects\Orders;
use App\OnlineStore\Objects\Waitlist as WaitlistDrop;

class AccountController extends Controller
{
    // todo:
    // - delete old subscription code and db fields relating to that
    // - when returning fullfileed orders via ajax, sort by 'payment_date' in descending order

    public function account(Request $request)
    {
        if (get_theme() == 'shopzigzagstripe') {

            $customer = $request->user();

            LiquidTemplate::setGlobalDrops([
                'shop'     => new Shop,
                'customer' => $customer ? new Customer($customer) : null,
                'cart'     => $customer ? new Cart($customer->shoppingCart()) : null,
            ]);

            $customer->setOption('last-visit-time', time());
            $customer->save();

            return LiquidTemplate::template('account');

        } else {
            $customer = $request->user();
            $waitlistCount = $customer->newWaitlistCount();
            $customer->setOption('last-visit-time', time());

            if (multiple_locations_enabled()) {
                // if the location was deleted, reset it
                if (! empty($customer->location_id) && ! MultipleStoreLocation::where('id', $customer->location_id)->exists()) {
                    $customer->location_id = null;
                    $customer->local_pickup = false;
                }

                // if the customer had local pickup, and we enable locations in admin, reset it
                if ($customer->local_pickup && empty($customer->location_id)) {
                    $customer->local_pickup = false;
                }
            } else {
                $customer->location_id = null;
            }

            $customer->save();
            $cart = $customer->shoppingCart();
            $expire = expire();
            $localPickupEnabled = local_pickup_enabled();
            return view('shop.manage-orders', compact('customer', 'cart', 'expire', 'localPickupEnabled', 'waitlistCount'));
        }
    }

    public function updateEmail(EmailFormRequest $request)
    {
        $user = $request->user();

        $newInstagramUser = false;
        if (! empty($user->instagram_id) && empty($user->email)) {
            $newInstagramUser = true;
        }

        $user->email = trim(request('email'));
        $user->setOption('saw-onboarding-popup', true);
        $user->save();

        if ($newInstagramUser) {
            (new ShopMailer)->sendInstagramWelcomeEmail($user);
        }

        log_info("Customer with fbid {$user->facebook_id} ({$user->name}) updated email to {$user->email}");
        return response()->json(['success' => true]);
    }

    public function updateAddress(AddressFormRequest $request)
    {
        // save the address as a global address
        $user = $request->user();
        $user->fill($request->input());
        $user->save();

        log_info("Customer with fbid {$user->facebook_id} ({$user->name}) updated address to {$user->getAddress()}");
        return response()->json(['success' => true]);
    }

    public function setDeliveryMethod(DeliveryMethodFormRequest $request)
    {
        $method = $request->input('method');

        $user = $request->user();
        $user->local_pickup = $method == 'Local' ? true : false;

        if ($user->local_pickup && ! empty($request->input('location_id'))) {
            $user->location_id = $request->input('location_id');
        }

        $user->save();

        log_info("Delivery method updated to {$method} by customer with fbid {$user->facebook_id} ({$user->name})");
        return response()->json(['status' => 1]);
    }

    public function applyCoupon(Request $request)
    {
        // todo add form request to validate coupon code is not empty
        $code = request('code');
        $reason = "Not sure";

        if (empty($code)) {
            return response()->json(['status' => 0, 'reason' => 'Empty code']);
        }

        $code = strtoupper(trim($code));
        if (shop_id() == 'cheekys' && ($code == 'RELAUNCH' || $code == 'HEADSORTAILS' || $code == 'NOCURRY' || $code == 'LoveFromCheekys' || ($code == 'BORNFREE' && time() < strtotime('2017-11-01 02:00:00')))) {

            $freeItemVariantId = 2960;

            $items = \App\Models\Cart::forCustomer($request->user()->id)->with('variant.product')->get();

            $hasNonBittyBoxProduct = false;
            $isFreeItemInCart = false;

            // check we don't already have the free item added to the cart
            foreach ($items as $item) {
                if ($freeItemVariantId == $item->variant->id) {
                    $isFreeItemInCart = true;
                }
            }

            foreach ($items as $item) {
                $sku = trim(strtolower($item->variant->product->style ?? null));
                $name = trim(strtolower($item->variant->product->product_name ?? null));

                if (! empty($sku) && ! str_contains($sku, 'bitty') && ! str_contains($name, 'bitty') && $freeItemVariantId != $item->variant->id) {
                    // if have a sku and the sku does not contains 'bitty'
                    $hasNonBittyBoxProduct = true;
                }
            }

            if ($hasNonBittyBoxProduct && ! $isFreeItemInCart) {
                $variant = \App\Models\Inventory::find($freeItemVariantId);
                if (! $variant) {
                    $reason = "Cannot find free item variant";
                    return response()->json(['status' => 0, 'reason' => $reason]);
                }

                $message = "[Coupon {$code}] Adding item to shopping cart for customer #".$request->user()->id;
                \App\Models\Cart::adminAddItem($request->user(), $variant, $message);
                return response()->json(['status' => 1]);
            }

            if (! $hasNonBittyBoxProduct) {
                $reason = "You need another item in your cart besides the bitty box";
            }

            if ($isFreeItemInCart) {
                $reason = "Free item already added to cart";
            }

            // This is cheekys-specific, so we can return specific reasons to be used
            return response()->json(['status' => 0, 'reason' => $reason]);
        }

        $coupon = Coupon::getCoupon($code);
        if ($coupon && $coupon->isValid($request->user(), ShoppingCart::subtotalFor($request->user()))) {
            ShoppingCart::applyCoupon($coupon->id);
            return response()->json(['status' => 1]);
        }

        return response()->json(['status' => 0, 'reason' => $reason]);
    }

    public function applyGiftCard(Request $request)
    {
        $customer = $request->user();
        $code = str_replace(' ', '', request('code'));
        if (empty($code)) {
            return response()->json(['status' => 0]);
        }

        $giftcard = GiftCard::code($code)->first();
        if (is_null($giftcard) || ! $giftcard->isValid()) {
            return response()->json(['status' => 0]);
        }

        $giftcard->redeemed_at = time();
        $giftcard->redeemed_by_customer_id = $customer->id;
        $giftcard->used = true;
        $giftcard->save();

        $customer->addBalance($giftcard->value, "Customer redeemed \$".amount($giftcard->value)." giftcard. Giftcard #{$giftcard->id}");

        return response()->json(['status' => 1]);
    }

    public function applyBalance()
    {
        $customer = request()->user();

        if (! $customer->hasBalance()) {
            return response()->json(['status' => 0, 'message' => "You don't have any credit in your account!"]);
        }

        // todo: payment_ref = "CREDIT"; also add a 'mixed' payment_ref in the case where balance was only half applied; maybe leave the payment_ref as stripe charge id, and when displaying in admin, if the order has apply_balance set, show the value

        // apply balance to the current web session
        ShoppingCart::applyBalance();

        $cart = $customer->shoppingCart();
        $cart->applyBalance();

        if ($cart->isFullyPaidWithBalance) {
            return response()->json(['status' => 1, 'message' => 'Credit applied - thanks!']);

        } else {
            $message = 'Your credit has been applied, your remaining balance due is $'.amount($cart->total - $cart->deduced_balance).' - Please note you still need to checkout and pay the remaining balance';
            return response()->json(['status' => 1, 'message' => $message]);
        }
    }

    public function waitlist()
    {
        if (get_theme() == 'shopzigzagstripe') {

            $customer = request()->user();
            return LiquidTemplate::snippet('waitlist', [
                'shop'     => new Shop,
                'customer' => $customer ? new Customer($customer) : null,
                'waitlist' => $customer ? new WaitlistDrop($customer->waitlist()) : null,
            ]);

        } else {
            $waitlist = request()->user()->waitlist();
            return view('shop.waitlist', compact('waitlist'));
        }
    }

    public function removeFromWaitlist(WaitlistRemoveFormRequest $request)
    {
        ShoppingWaitlist::removeItem(request('item_id'));
        return response()->json(['status' => 1]);
    }

    public function paidOrders()
    {
        if (get_theme() == 'shopzigzagstripe') {

            $customer = request()->user();
            return LiquidTemplate::snippet('paid-orders', [
                'shop'   => new Shop,
                'orders' => $customer ? new Orders($customer->ordersPaid) : null,
            ]);

        } else {
            $orders = request()->user()->ordersPaid;
            return view('shop.paid-orders', compact('orders'));
        }
    }

    public function fulfilledOrders()
    {
        if (get_theme() == 'shopzigzagstripe') {

            $customer = request()->user();
            return LiquidTemplate::snippet('fulfilled-orders', [
                'shop'   => new Shop,
                'orders' => $customer ? new Orders($customer->ordersFulfilled) : null,
            ]);

        } else {
            $orders = request()->user()->ordersFulfilled;
            return view('shop.fulfilled-orders', compact('orders'));
        }
    }

    public function removeFromCart(CartRemoveFormRequest $request)
    {
        // if it's a session cart item
        if (starts_with(request('item_id'), 'session')) {
            $inventoryId = str_replace('session', '', request('item_id'));
            session()->forget('online-store.cart.'.$inventoryId);
            return response()->json(['status' => 1]);
        }

        try {
            ShoppingCart::removeItem(request('item_id'));
            return response()->json(['status' => 1]);
        } catch (ShopifyWebhookLockedException $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shopify] $e");
            return response()->json(['status' => 0, 'message' => 'Error, product could not be removed. Please refresh and try again!']);
        }
    }

    public function messengerId(Request $request)
    {
        $customer = $request->user();
        return response()->json(['messenger_id' => $customer->messenger_id]);
    }
}
