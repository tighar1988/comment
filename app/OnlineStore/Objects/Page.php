<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Page as PageModel;

class Page extends Drop
{
    protected $page;

    public function __construct(PageModel $page)
    {
        $this->page = $page;
    }

    /**
     * Returns the author of a page.
     */
    public function author()
    {
        return null;
    }

    /**
     * Returns the content of a page.
     */
    public function content()
    {
        return $this->page->content;
    }

    /**
     * Returns the handle of the page.
     */
    public function handle()
    {
        return $this->page->url;
    }

    /**
     * Returns the id of the page.
     */
    public function id()
    {
        return $this->page->id;
    }

    /**
     * Returns the timestamp of when the page was created. Use the date filter to format the timestamp.
     */
    public function published_at()
    {
        return strtotime($this->page->created_at);
    }

    /**
     * Returns the name of the custom page template assigned to the page, without the page. prefix
     * nor the .liquid extension. Returns nil if a custom template is not assigned to the page.
     *
     * Input
     * <!-- on page.contact.liquid -->
     * {{ page.template_suffix }}
     *
     * Output
     * contact
     */
    public function template_suffix()
    {
        return null;
    }

    /**
     * Returns the title of a page.
     */
    public function title()
    {
        return $this->page->title;
    }

    /**
     * Returns the relative URL of the page.
     *
     * Input
     * {{ page.url }}
     *
     * Output
     * /pages/about-us
     */
    public function url()
    {
        return $this->page->getUrl();
    }

    /**
     * Search results have an additional attribute titled object_type which returns the
     * type of each result. This is useful for writing the logic of your search results loop.
     */
    public function object_type()
    {
        return 'page';
    }
}
