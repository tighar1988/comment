@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($blog) ? true : false; ?>
<section class="content-header">
    <h1>@if ($isEdit) Editing Blog '{{ $blog->title }}' @else Add New Blog @endif</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/blogs"><i class="fa fa-th-large"></i> Blogs</a></li>
        <li class="active">@if ($isEdit) Edit Blog @else Add New Blog @endif</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('title')">
                                    <label>Title</label>
                                    <input type="text" placeholder="Blog Name" class="form-control" name="title" value="{{ old('title', $blog->title ?? '') }}">
                                    @error('title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('feedburner_url')">
                                    <label>Feedburner URL</label>
                                    <select name="feedburner_url" class="form-control">
                                        <option value="">None</option>
                                        <option value="http://feedproxy.google.com/" {{ old('feedburner_url', $blog->feedburner_url ?? null) == 'http://feedproxy.google.com/' ? 'selected' : '' }}>http://feedproxy.google.com/</option>
                                        <option value="http://feeds.feedburner.com/" {{ old('feedburner_url', $blog->feedburner_url ?? null) == 'http://feeds.feedburner.com/' ? 'selected' : '' }}>http://feeds.feedburner.com/</option>
                                    </select>
                                    @error('feedburner_url')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('seo_page_title')">
                                    <label class="control-label">SEO Page Title</label> <small class="text-muted">70 characters allowed</small>
                                    <input type="text" class="form-control" maxlength="70" name="seo_page_title" value="{{ old('seo_page_title', $blog->seo_page_title ?? null) }}">
                                    @error('seo_page_title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('seo_meta_description')">
                                    <label class="control-label">SEO Page Description</label> <small class="text-muted">160 characters allowed</small>
                                    <input type="text" class="form-control" maxlength="160" name="seo_meta_description" value="{{ old('seo_meta_description', $blog->seo_meta_description ?? null) }}">
                                    @error('seo_meta_description')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('handle')">
                                    <label class="control-label">Handle</label>
                                    <input type="text" placeholder="news" class="form-control" maxlength="160" name="handle" value="{{ old('handle', $blog->handle ?? null) }}">
                                    @error('handle')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('comments_type')">
                                    <label class="control-label">Comments</label>
                                    <div class="radio">
                                        <label><input type="radio" @if(old('comments_type', $blog->comments_type ?? null) == 1) checked="checked" @endif value="1" name="comments_type">Comments are disabled.</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" @if(old('comments_type', $blog->comments_type ?? null) == 2) checked="checked" @endif value="2" name="comments_type">Comments are allowed, pending moderation.</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" @if(old('comments_type', $blog->comments_type ?? null) == 3) checked="checked" @endif value="3" name="comments_type">Comments are allowed, and are automatically published.</label>
                                    </div>
                                    @error('comments_type')
                                </div>
                            </div>
                        </div>

                        <button type="submit" data-loading-text="Saving.." class="btn btn-success">Save</button>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection
