<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\SyncMailchimp;
use Mockery;

class SyncMailchimpTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(SyncMailchimp::class, new SyncMailchimp);
    }
}
