<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use Log;
use Facebook;
use Datatables;

class PostsController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.posts.posts-grid');
    }

    public function datatable()
    {
        $model = Post::query()->select(\DB::raw("posts.id as post_id, posts.created_time, posts.scheduled_time, posts.fb_id, posts.image, posts.prod_id as product_id, posts.error_type,
                COALESCE((SELECT SUM(quantity) FROM inventory WHERE inventory.product_id = products.id GROUP BY inventory.product_id), 0) as inventory_quantity,
                products.style as product_style"))
            ->join('products', 'posts.prod_id', '=', 'products.id');

        return Datatables::eloquent($model)
                ->filterColumn('product_style', function($query, $keyword)  {
                    $query->where('products.style', 'like', "%{$keyword}%");
                })
                ->orderColumn('product_style', 'product_id $1')
                ->orderColumn('scheduled_time', 'scheduled_time $1, created_time $1')
                ->orderColumn('post_id', 'scheduled_time $1, created_time $1')
                ->addColumn('thumb', '{{ product_thumb($image) }}')
                ->addColumn('is_scheduled', '{{ ($scheduled_time > 0 ? true : false) }}')
                ->editColumn('scheduled_time', '{{ apply_timezone($scheduled_time) }}')
                ->editColumn('created_time', '{{ apply_timezone($created_time) }}')
                ->make(true);
    }

    public function edit($id)
    {
        $post = Post::find($id);

        // todo: catch error if facebook is not connected, and display a message to the user
        $enabledGroups = fb_enabled_group_ids();
        $groups = Facebook::getMerchantGroups();

        if (is_null($post->scheduled_time)) {
            flash('This post was already published to Facebook!');
            return redirect('/admin/posts');
        }

        $product = $post->product;
        $image = $product->image;
        $supplement = json_decode($post->supplement);

        $scheduleTime = \Carbon\Carbon::createFromTimestamp($post->scheduled_time, shop_setting('shop.timezone'));
        $postDay = $scheduleTime->format('n/j/Y');
        $postHour = intval($scheduleTime->format('h'));
        $postMinute = intval($scheduleTime->format('i'));
        $postAmPm = $scheduleTime->format('a');

        return view('admin.posts.post-edit', compact('post', 'product', 'image', 'groups', 'enabledGroups', 'supplement', 'postDay', 'postHour', 'postMinute', 'postAmPm'));
    }

    /* route /admin/posts/comment */
    /* Comment the latest available inventory on the post */
    public function commentInventory($postID)
    {
        /* Adam XXX plz make this pretty */
        $prodID = Post::grid($postID)->get();
        Log::info(print_r($prodID, true));
        return "true";
        $DBposts = DB::Select("Select * from posts where id={$postID}")[0];
        $fbid = $DBposts->fb_id;
        $prodID = $DBposts->prod_id;
        $DBinv = DB::Select("select id,color,size from inventory where product_id={$prodID} and qty>0 order by id asc");
        $msg = "Hey! The following is still available:\n";
        $sizes = [];
        foreach ($DBinv as $variant) {
            if (isset($sizes[$variant->color])) {
                $sizes[$variant->color] .= ", " . $variant->size;
            } else {
                $sizes[$variant->color] = $variant->size;
            }
        }
        foreach ($sizes as $color=>$size) {
            if (isset($color) && $color != "")
                $msg .= "$color: $size\n";
            else
                $msg .= "$size\n";
        }
        $s = new Setup;
        $data = $s->get([['metric','fbid']]);
        $data = $data[0];
        $v = json_decode($data->value,true);
        $token = $v[1];
        $uid = $v[0];


        $fb = new Facebook;
        $fb->setToken($token);
        $resp = $fb->graph('/' . $fbid . '/comments', ['message'=>$msg]);
        if (!isset($resp['id'])) {
            Log::error("Response from posting comment " . print_r($resp, true));
            return "Hmm, got an eror: " . print_r($resp, true);
        } else {
            return "<html>Commented on the post: <pre>$msg</pre><br><br><a href='http://facebook.com/{$resp['id']}'>Here's the comment</a><br><br><a href='http://facebook.com/{$fbid}'>Here's the post</a></html>";
        }
    }

    public function destroy($id)
    {
        Post::destroy($id);
        flash('Post Deleted!');
        return redirect('/admin/posts');
    }
}
