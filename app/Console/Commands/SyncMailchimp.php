<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ShopUser;
use Log;
use App\Models\Order;

class SyncMailchimp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commentsold:sync-mailchimp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync MailChimp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Syncing MailChimp..');

        set_shop_database('julesandjamesboutique');
        \DB::setDefaultConnection('shop');
        \DB::reconnect();

        // Loop through customers and update mailchimp
        foreach (ShopUser::all() as $customer) {
            if (isset($customer->email) && trim($customer->email) != "") {
                $this->info("[mailchimp] Adding {$customer->name} into mailchimp - {$customer->email}");
                $this->syncMailChimp($customer);
                break;
            }
        }

        $this->info('Sync Complete.');
    }

    public function syncMailChimp($customer)
    {
        $listId = '10219';
        $apiKey = 'd7128422acb41d245cb96a800750e2c6-us16';

        $memberId = md5(strtolower(trim($customer->email)));
        $customerCenter = substr($apiKey, strpos($apiKey,'-') + 1);
        $url = 'https://' . $customerCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

        $lastOrder = Order::where('customer_id', '=', $customer->id)->orderBy('id', 'desc')->first();
        if (! is_null($lastOrder)) {
            $lastOrder = $lastOrder->created_at->toDateTimeString();
        }
        $totalOrders = Order::where('customer_id', '=', $customer->id)->count();
        $amountSpent = Order::where('customer_id', '=', $customer->id)->sum('payment_amt');

        $json = json_encode([
            'email_address' => trim($customer->email),
            'status'        => "subscribed",
            'status_if_new' => "subscribed",
            'merge_fields'  => [
                'NAME'       => $customer->name,
                'JOIN_DATE'  => $customer->created_at->toDateTimeString(),
                'LAST_ORDER' => $lastOrder,
                'TOTALORDER' => $totalOrders,
                'ADDRESS'    => $customer->getAddress(),
                'AMT_SPENT'  => $amountSpent,
                // 'CID'        => $customer->id,
            ]
        ]);

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($httpCode != 200) {
            $this->info(print_r($result, true));
        }

        curl_close($ch);

        return $result;
    }
}
