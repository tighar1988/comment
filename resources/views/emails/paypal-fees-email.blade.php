<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><hr/><br/>

    This is an invoice for your PayPal commission fees since {{ $sinceTime }}.

    Total amount is ${{ amount($feesTotal) }}

    <br/><br/>

    Nothing is due at this time.
    <br/><br/>
</div>
