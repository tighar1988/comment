<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\OrderProduct;
use App\Models\Cart;
use App\Models\ShopUser;
use App\Models\Waitlist;
use App\Http\Requests\CartAddFormRequest;
use App\Http\Requests\WaitlistAddFormRequest;
use App\Http\Requests\CartUpdateFormRequest;
use App\Http\Requests\CartChangeFormRequest;
use Illuminate\Support\Facades\Auth;

class OnlineStoreAjaxController extends Controller
{
    /**
     * https://help.shopify.com/themes/development/getting-started/using-ajax-api#get-cart
     */
    public function cart()
    {
        $cart = session('online-store.cart', []);

        if (empty($cart) && ! Auth::check()) {
            return [
                'token'             => '1d19a32178501c44ef2223d73c54d16d', // hardcoded token
                'note'              => null,
                'attributes'        => [],
                'total_price'       => 0,
                'total_weight'      => 0,
                'item_count'        => 0,
                'items'             => [],
                'requires_shipping' => false,
            ];

        } else {

            $customer = Auth::check() ? Auth::user() : new ShopUser;
            $shoppingCart = $customer->shoppingCart();
            $items = [];

            foreach ($shoppingCart->items as $cartItem) {
                $variant = Inventory::find($cartItem->inventory_id);
                $product = $variant->product;

                if (Auth::check()) {
                    $cartQuantity = Cart::forCustomer($customer->id)->cartQuantity($variant->id);

                } else {
                    if (! isset($cart[$cartItem->inventory_id]['quantity'])) {
                        continue;
                    }
                    $cartQuantity = $cart[$cartItem->inventory_id]['quantity'];
                }

                $itemExists = false;
                foreach ($items as $item) {
                    if (isset($item['id']) && $item['id'] == $variant->id) {
                        $itemExists = true;
                    }
                }

                if ($itemExists) {
                    continue;
                }

                $items[] = [
                    'id'                  => $variant->id,
                    'title'               => $product->product_name,
                    'price'               => dollars_to_cents($variant->price),
                    'line_price'          => dollars_to_cents($variant->price),
                    'quantity'            => $cartQuantity,
                    'sku'                 => $product->style,
                    'grams'               => $variant->weight * 28.3495,
                    'vendor'              => null,
                    'properties'          => null,
                    'variant_id'          => $variant->id,
                    'gift_card'           => false,
                    'url'                 => "/products/".$product->url."?variant=".$variant->id,
                    'image'               => cdn_shop($product->image->filename ?? null, 'images'),
                    'handle'              => $product->url,
                    'requires_shipping'   => true,
                    'product_title'       => $product->product_name,
                    'product_description' => $product->store_description,
                    'product_type'        => $product->product_type,
                    'variant_title'       => $variant->variantName(),
                    'variant_options'     => $variant->variantOptions(),
                ];
            }

            return [
                'token'             => "1d19a32178501c44ef2223d73c54d16d", // hardcoded token
                'note'              => null,
                'attributes'        => [],
                'total_price'       => dollars_to_cents($shoppingCart->total),
                'total_weight'      => 0,
                'item_count'        => $shoppingCart->count(),
                'items'             => $items,
                'requires_shipping' => true,
            ];
        }
    }

    /**
     * https://help.shopify.com/themes/development/getting-started/using-ajax-api#add-to-cart
     */
    public function cartAdd(CartAddFormRequest $request)
    {
        $quantity    = request('quantity');
        $inventoryId = request('id');

        $variant = Inventory::find($inventoryId);

        $customer = $request->user();
        if (is_null($customer)) {
            $cart = session('online-store.cart', []);

            if (! isset($cart[$inventoryId])) {
                $cart[$inventoryId] = [
                    'quantity'     => $quantity,
                    'inventory_id' => $inventoryId,
                    'order_source' => OrderProduct::SOURCE_STORE,
                ];
            } else {
                $cart[$inventoryId]['quantity'] += $quantity;
            }

            $cartQuantity = $cart[$inventoryId]['quantity'];
            session(['online-store.cart' => $cart]);

        } else {
            Cart::storeAddItem($customer, $variant);
            $cartQuantity = Cart::forCustomer($customer->id)->cartQuantity($variant->id);
        }

        $product = $variant->product;

        return [
            'id'                  => $variant->id,
            'title'               => $product->product_name,
            'price'               => dollars_to_cents($variant->price),
            'line_price'          => dollars_to_cents($variant->price),
            'quantity'            => $cartQuantity,
            'sku'                 => $product->style,
            'grams'               => $variant->weight * 28.3495,
            'vendor'              => null,
            'properties'          => null,
            'variant_id'          => $variant->id,
            'gift_card'           => false,
            'url'                 => "/products/".$product->url."?variant=".$variant->id,
            'image'               => cdn_shop($product->image->filename ?? null, 'images'),
            'handle'              => $product->url,
            'requires_shipping'   => true,
            'product_title'       => $product->product_name,
            'product_description' => $product->store_description,
            'product_type'        => $product->product_type,
            'variant_title'       => $variant->variantName(),
            'variant_options'     => $variant->variantOptions(),
        ];
    }

    /**
     * https://help.shopify.com/themes/development/getting-started/using-ajax-api#change-cart
     */
    public function cartChange(CartChangeFormRequest $request)
    {
        $customer = Auth::check() ? Auth::user() : new ShopUser;
        $shoppingCart = $customer->shoppingCart();

        $inventoryId = null;
        $inventoryIds = [];
        $line = 0;
        foreach ($shoppingCart->items as $cartItem) {
            if (in_array($cartItem->inventory_id, $inventoryIds)) {
                // skip
            } else {
                $inventoryIds[] = $cartItem->inventory_id;
                $line++;
            }

            if ($line == request('line')) {
                $inventoryId = $cartItem->inventory_id;
            }
        }

        $quantity = request('quantity');

        $variant = Inventory::find($inventoryId);

        $customer = $request->user();
        if (is_null($customer)) {
            $cart = session('online-store.cart', []);

            if (! isset($cart[$inventoryId])) {
                $cart[$inventoryId] = [
                    'quantity'     => $quantity,
                    'inventory_id' => $inventoryId,
                    'order_source' => OrderProduct::SOURCE_STORE,
                ];
            } else {
                $cart[$inventoryId]['quantity'] = $quantity;
            }

            $cartQuantity = $cart[$inventoryId]['quantity'];
            session(['online-store.cart' => $cart]);

        } else {
            $cartQuantity = Cart::forCustomer($customer->id)->cartQuantity($variant->id);

            if ($cartQuantity < $quantity) {
                // add extra items
                while ($cartQuantity < $quantity) {
                    Cart::storeAddItem($customer, $variant);
                    $cartQuantity++;
                }

            } elseif ($cartQuantity > $quantity) {
                // remove extra items
                while ($cartQuantity > $quantity) {
                    $cartItem = Cart::forCustomer($customer->id)->inventoryId($variant->id)->first();
                    if ($cartItem) {
                        Cart::removeItem($cartItem->id, '[online-store] Remove item.');
                    }
                    $cartQuantity--;
                }
            }
        }

        $product = $variant->product;

        return [
            'id'                  => $variant->id,
            'title'               => $product->product_name,
            'price'               => dollars_to_cents($variant->price),
            'line_price'          => dollars_to_cents($variant->price),
            'quantity'            => $cartQuantity,
            'sku'                 => $product->style,
            'grams'               => $variant->weight * 28.3495,
            'vendor'              => null,
            'properties'          => null,
            'variant_id'          => $variant->id,
            'gift_card'           => false,
            'url'                 => "/products/".$product->url."?variant=".$variant->id,
            'image'               => cdn_shop($product->image->filename ?? null, 'images'),
            'handle'              => $product->url,
            'requires_shipping'   => true,
            'product_title'       => $product->product_name,
            'product_description' => $product->store_description,
            'product_type'        => $product->product_type,
            'variant_title'       => $variant->variantName(),
            'variant_options'     => $variant->variantOptions(),
        ];
    }

    /**
     * https://help.shopify.com/themes/development/getting-started/using-ajax-api#update-cart
     */
    public function cartUpdate(CartUpdateFormRequest $request)
    {
        $quantity    = request('quantity');
        $inventoryId = request('id');

        $variant = Inventory::find($inventoryId);

        $customer = $request->user();
        if (is_null($customer)) {
            $cart = session('online-store.cart', []);

            if (! isset($cart[$inventoryId])) {
                $cart[$inventoryId] = [
                    'quantity'     => $quantity,
                    'inventory_id' => $inventoryId,
                    'order_source' => OrderProduct::SOURCE_STORE,
                ];
            } else {
                $cart[$inventoryId]['quantity'] = $quantity;
            }

            $cartQuantity = $cart[$inventoryId]['quantity'];
            session(['online-store.cart' => $cart]);

        } else {
            $cartQuantity = Cart::forCustomer($customer->id)->cartQuantity($variant->id);

            if ($cartQuantity < $quantity) {
                // add extra items
                while ($cartQuantity < $quantity) {
                    Cart::storeAddItem($customer, $variant);
                    $cartQuantity++;
                }

            } elseif ($cartQuantity > $quantity) {
                // remove extra items
                while ($cartQuantity > $quantity) {
                    $cartItem = Cart::forCustomer($customer)->inventoryId($variant->id)->first();
                    Cart::removeItem($cartItem->id, '[online-store] Remove item.');
                    $cartQuantity--;
                }
            }
        }

        $product = $variant->product;

        return [
            'id'                  => $variant->id,
            'title'               => $product->product_name,
            'price'               => dollars_to_cents($variant->price),
            'line_price'          => dollars_to_cents($variant->price),
            'quantity'            => $cartQuantity,
            'sku'                 => $product->style,
            'grams'               => $variant->weight * 28.3495,
            'vendor'              => null,
            'properties'          => null,
            'variant_id'          => $variant->id,
            'gift_card'           => false,
            'url'                 => "/products/".$product->url."?variant=".$variant->id,
            'image'               => cdn_shop($product->image->filename ?? null, 'images'),
            'handle'              => $product->url,
            'requires_shipping'   => true,
            'product_title'       => $product->product_name,
            'product_description' => $product->store_description,
            'product_type'        => $product->product_type,
            'variant_title'       => $variant->variantName(),
            'variant_options'     => $variant->variantOptions(),
        ];
    }

    public function cartRemove()
    {
        $cart = session('online-store.cart', []);

        $quantity    = request('quantity');
        $inventoryId = request('id');

        if (! isset($cart[$inventoryId])) {
            return;
        } else {
            $cart[$inventoryId]['quantity'] -= $quantity;
            if ($cart[$inventoryId]['quantity'] <= 0) {
                $cart[$inventoryId]['quantity'] = 1;
            }
        }

        session(['online-store.cart' => $cart]);

        return [
            'status' => 1,
        ];
    }

    public function waitlistAdd(WaitlistAddFormRequest $request)
    {
        $inventoryId = request('id');
        $variant = Inventory::find($inventoryId);
        $customer = Auth::user();

        Waitlist::storeAddItem($customer, $variant);

        return [
            'id' => $variant->id,
        ];
    }
}
