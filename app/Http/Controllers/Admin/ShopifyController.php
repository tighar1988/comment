<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\ConnectShopifyFromRequest;
use App\Shopify\ShopifyClient;
use App\Shopify\Shopify;
use App\Jobs\HandleShopifyWebhook;
use App\Jobs\SetupShopify;
use Exception;
use Redis;
use Log;

class ShopifyController extends Controller
{
    /**
     * Shopify Webhook
     *
     * @return \Illuminate\Http\Response
     */
    public function incoming(Request $request)
    {
        $topic  = $request->header('X-Shopify-Topic');
        $sha256 = $request->header('X-Shopify-Hmac-Sha256');
        $shop   = $request->header('X-Shopify-Shop-Domain');
        $data   = file_get_contents('php://input');

        // dispatch(new HandleShopifyWebhook($request->all(), $topic, $sha256, $shop, $data));
        try {

            (new HandleShopifyWebhook($request->all(), $topic, $sha256, $shop, $data))->handle();

        } catch (Exception $e) {
            Log::error($e->getMessage());
            Log::info($shop);
            Log::info($topic);
            Log::info($request->all());
        }
    }

    public function connectShopify(ConnectShopifyFromRequest $request)
    {
        $client = new ShopifyClient(request('shopify_shop_name'));

        $nonce = uniqid();
        shop_setting_set('shopify.nonce', $nonce);

        return redirect($client->getAuthorizeUrl($nonce));
    }

    public function shopifyCallback(Shopify $shopify)
    {
        $client = new ShopifyClient(request('shop'));

        if (request('state') != shop_setting('shopify.nonce') || ! ends_with(request('shop'), '.myshopify.com') ||
            ! $client->validateSignature(request()->input()))
        {
            flash('Could not connect shopify. Security check failed!', 'danger');
            return redirect('/admin/setup/shopify');
        }

        $token = $client->getAccessToken(request('code'));
        if (! is_null($token)) {
            shop_setting_set('shopify.access_token', $token);
            shop_setting_set('shopify.shopify-shop-name', request('shop'));
            shop_setting_set('shopify.connecting', true);
            shop_setting_set('shopify.enabled', true);

            dispatch(new SetupShopify(shop_id()));

            // update the Redis cache
            Redis::connection('cache')->set("shopify-shop:".request('shop'), shop_id());

            flash('Shopify connected!');
            return redirect('/admin/setup/shopify');
        }

        flash('Something went wrong. Could not connect shopify.', 'danger');
        return redirect('/admin/setup/shopify');
    }

    public function shopifyConnected()
    {
        if (shop_setting('shopify.connecting') == false) {
            return response()->json(['status' => 1]);
        }

        return response()->json(['status' => 0]);
    }

    public function disconnectShopify()
    {
        try {

            (new Shopify)->deleteWebhooks();
            shop_setting_set('shopify.access_token', null);
            shop_setting_set('shopify.enabled', false);

        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

        flash('Shopify was disconnected.');
        return redirect('/admin/setup/shopify');
    }
}
