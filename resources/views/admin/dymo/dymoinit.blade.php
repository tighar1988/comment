{{--

    For some reason, only document.getElementById works, not $('')

    We can either print barcodes for a whole variant, or print barcodes for the whole product. Only print what's in stock.

    Remember we want to print the text as "sku1234-large-blue" but the actual barcode data will be the unique inventory_id

--}}
<script type="text/javascript">

var printers = null;

$(document).ready(function() {
    /* Load the Zebra code if label printing is enabled */
    zebra_setup_web_print();

    var printer = document.getElementById('printer');

    dymo.label.framework.trace = 0;
    dymo.label.framework.init(function() {
        try {
            dymo.label.framework.getPrintersAsync().then(function(asyncPrinters) {
                var printer = document.getElementById('printer');
                printers = asyncPrinters;
                if (printer) {
                    if (printers.length == 0 && !printer.innerHTML.match(/found/i) ) {
                        /* DYMO library loaded, but we couldn't find any connected printers */
                        printer.innerHTML = "No label printers found";
                        $('.print').hide();
                    } else {
                        printer.innerHTML = "Found Printer " + printers[0].name;
                        $('.print').show();
                    }
                }
            });
        } catch(e) {
            /* No label printers found, aka the web request failed, so we won't even show it at all */
            console.log(e);
            if (!printer.innerHTML.match(/found/i) ) {
                printer.innerHTML = "No label printers found";
            }
            $('.print').hide();
            $('.print').show();
        }
    });
});

/**
 * Find all variants on the page and print them according to quantity.
 */
function printAllBarcodes() {
    $('.barcode').each(function(i, obj) {
        printForVariant(this);
    });

    return true;
}

/**
 * Find all variants on the page, build the print string, send it to zebra.
 */
function printZebraBarcodes() {
    $('.barcode').each(function(i, obj) {
        printForVariant(this, false, true);
    });
    return true;
}

/**
    * Print a single chunk of barcodes, repetitive function, oh well
 */
function printZebraBarcode_Single(quantity_modal) {
    var quantity = quantity_modal.val();
    var barcode = quantity_modal.attr('data-itemId');
    printForVariant(quantity_modal, true, true);
    console.log(quantity);
    console.log(barcode);
    return true;
    $('.barcode').each(function(i, obj) {
        printForVariant(this, false, true);
    });

    return true;
}



/**
 * Find a single variant on the page and print them according to quantity.
 */
function printBarcode(variant, zebra=false) {
    printForVariant(variant, false, zebra);
    return true;
}

/**
 * Find a single variant on the page and print it only once.
 */
function printOneBarcode(variant, zebra=false) {
    printForVariant(variant, true, zebra);
    return true;
}

function printForVariant(variant, printOne, zebra) {
    var $variant = $(variant);
    var sku = $variant.attr("data-sku");
    var size = $variant.attr("data-size");
    var color = $variant.attr("data-color");
    var inventory_id = $variant.attr("data-id");
    var quantity = printOne ? 1 : $variant.attr("data-qty");

    /* Build string, this is ugly, but it works */
    if (color != "")
        sku += "-" + color;

    if (size != "")
        sku += "-" + size;

    sku = sku.toUpperCase();
    //sku = sku.replace('medium', 'm');
    //sku = sku.replace('small', 's');
    //sku = sku.replace('large', 'l');
    sku = sku.replace(/-$/gi, "");
    sku = sku.replace(/\//gi, "_");

    console.log(sku, inventory_id, quantity);
    if (zebra && quantity > 0) {
        for (var qty = quantity; qty > 0; qty--) {
            zebraPrintTag(sku, inventory_id);
            if (qty == 1)
                zebraLabelCut(sku + " END");    

        }
        printBuildString();
    } else {
        DymoPrint(sku, inventory_id, quantity);
    }

}

function DymoPrint(text, barcode, quantity) {
    var i = 0;
    while (i < quantity) {
        var label = dymo.label.framework.openLabelXml(generateLabel(text, barcode));
        var printParams = {};
        printParams.twinTurboRoll = dymo.label.framework.TwinTurboRoll.Auto;
        label.print(printers[0].name, dymo.label.framework.createLabelWriterPrintParamsXml(printParams));
        i++;
    }
}

function generateLabel(text, barcode) {

var label1D = '<\?xml version="1.0" encoding="utf-8"?>\
<DieCutLabel Version="8.0" Units="twips">\
<PaperOrientation>Landscape</PaperOrientation>\
<Id>Address</Id>\
<PaperName>30252 Address</PaperName>\
<DrawCommands>\
<RoundRectangle X="0" Y="0" Width="1581" Height="5040" Rx="270" Ry="270" />\
</DrawCommands>\
<ObjectInfo>\
<BarcodeObject>\
<Name>BARCODE</Name>\
<ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
<BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
<LinkedObjectName></LinkedObjectName>\
<Rotation>Rotation0</Rotation>\
<IsMirrored>False</IsMirrored>\
<IsVariable>True</IsVariable>\
<Text>'+barcode+'</Text>\
<Type>Code128Auto</Type>\
<Size>Small</Size>\
<TextPosition>None</TextPosition>\
<TextFont Family="Arial" Size="36" Bold="True" Italic="False" Underline="False" Strikeout="False" />\
<CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" />\
<TextEmbedding>None</TextEmbedding>\
<ECLevel>0</ECLevel>\
<HorizontalAlignment>Left</HorizontalAlignment>\
<QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" />\
</BarcodeObject>\
<Bounds X="331" Y="58" Width="4622" Height="717.637362637363" />\
</ObjectInfo>\
<ObjectInfo>\
<TextObject>\
<Name>TEXT</Name>\
<ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
<BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
<LinkedObjectName></LinkedObjectName>\
<Rotation>Rotation0</Rotation>\
<IsMirrored>False</IsMirrored>\
<IsVariable>False</IsVariable>\
<HorizontalAlignment>Left</HorizontalAlignment>\
<VerticalAlignment>Top</VerticalAlignment>\
<TextFitMode>ShrinkToFit</TextFitMode>\
<UseFullFontHeight>True</UseFullFontHeight>\
<Verticalized>False</Verticalized>\
<StyledText>\
<Element>\
<String>'+text+'</String>\
<Attributes>\
<Font Family="Arial" Size="10" Bold="True" Italic="False" Underline="False" Strikeout="False" />\
<ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
</Attributes>\
</Element>\
</StyledText>\
</TextObject>\
<Bounds X="331" Y="773" Width="4622" Height="720" />\
</ObjectInfo>\
</DieCutLabel>';

return label1D;

}

</script>
