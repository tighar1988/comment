<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\DatabaseCreate::class,
        \App\Console\Commands\DatabaseMigrateMaster::class,
        \App\Console\Commands\DatabaseMigrateShop::class,
        \App\Console\Commands\DatabaseMigrateAllShops::class,
        \App\Console\Commands\DatabaseClearFailedJobs::class,
        \App\Console\Commands\DispatchJobs::class,
        \App\Console\Commands\DispatchMainJob::class,
        \App\Console\Commands\ExtractTaxRates::class,
        \App\Console\Commands\UpdateTaxRatesTable::class,
        \App\Console\Commands\SyncMailchimp::class,
        \App\Console\Commands\ImportDivas::class,
        \App\Console\Commands\ImportExternalImages::class,
        \App\Console\Commands\UpdateImagesProportions::class,
        \App\Console\Commands\ImportShopifyCostPrice::class,
        \App\Console\Commands\ImportCustomersCSV::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('commentsold:dispatch-main-job')->daily()
        //         ->withoutOverlapping();

        $schedule->command('commentsold:dispatch-jobs "App\Jobs\ShopMasterJob"')->everyMinute()
                ->withoutOverlapping();

        $schedule->command('commentsold:dispatch-jobs "App\Jobs\CollectPayPalFees"')->dailyAt('21:00')
                ->timezone('CST')
                ->withoutOverlapping();

        $schedule->command('commentsold:dispatch-jobs "App\Jobs\SubscriptionPlanCharge"')->dailyAt('08:00')
                ->timezone('CST')
                ->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
