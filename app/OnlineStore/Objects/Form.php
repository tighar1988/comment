<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

/**
 * The form object is used within the form tag. It contains attributes of its parent form.
 */
class Form extends Drop
{
    protected $formId;

    public function __construct($formId)
    {
        $this->formId = $formId;
    }

    /**
     * Returns the id (unique identifier) of the form.
     */
    public function id()
    {
        return $this->formId;
    }

    /**
     * Returns the name of the author of the blog article comment. Exclusive to form tags with the "article" parameter.
     */
    public function author()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the content of the blog article comment. Exclusive to form tags with the "article" parameter.
     */
    public function body()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the email of the blog article comment's author. Exclusive to form tags with the "article" parameter.
     */
    public function email()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns an array of strings if the form was not submitted successfully. The strings returned
     * depend on which fields of the form were left empty or contained errors. Possible values are:
     * author
     * body
     * email
     * form
     *
     * Input
     * {% for error in form.errors %}
     *     {{ error }}
     * {% endfor %}
     *
     * Output
     * <!-- if the Name field was left empty by the user -->
     * author
     *
     * You can apply the default_errors filter on form.errors to output default error
     * messages without having to loop through the array.
     *
     * Input
     * {% if form.errors %}
     *       {{ form.errors | default_errors }}
     * {% endif %}
     *
     * Output
     * Please enter a valid email address.
     *
     * If you want more control over the markup of the errors, you can loop through the
     * messages and translated_fields arrays that are part of the form.errors object.
     *
     * {% for field in form.errors %}
     *   {% if field == 'form' %}
     *     {{ form.errors.messages[field] }}
     *   {% else %}
     *     {{ form.errors.translated_fields[field] }} - {{ form.errors.messages[field] }}
     *   {% endif %}
     * {% endfor %}
     */
    public function errors()
    {
        return (array) session('forms.'.$this->formId.'.errors', []);
    }

    /**
     * Renders an HTML checkbox that can submit the current form as the customer's default
     * address. Exclusive to form tags with the "address" parameter.
     *
     * Input
     * {{ form.set_as_default_checkbox }}
     *
     * Output
     * <input type="checkbox" id="address_default_address_12345678" name="address[default]" value="1">
     */
    public function set_as_default_checkbox()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the first name associated with the address. Exclusive to form tags with the "address" parameter.
     */
    public function first_name()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the last name associated with the address. Exclusive to form tags with the "address" parameter.
     */
    public function last_name()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the company name associated with the address, if it exists. Exclusive to form
     * tags with the "address" parameter.
     */
    public function company()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the first address line associated with the address. Exclusive to form tags with the "address" parameter.
     */
    public function address1()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the second address line associated with the address, if it exists. Exclusive
     * to form tags with the "address" parameter.
     */
    public function address2()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the city associated with the address. Exclusive to form tags with the "address" parameter.
     */
    public function city()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the province or state associated with the address. Exclusive to form tags with the "address" parameter.
     *
     * Input
     * {{ form.city }}, {{ form.province }}
     *
     * Output
     * San Francisco, California
     */
    public function province()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the country associated with the address. Exclusive to form tags with the "address" parameter.
     */
    public function country()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the zip code or postal code associated with the address. Exclusive to form
     * tags with the "address" parameter.
     */
    public function zip()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the telephone number associated with the address, if it exists. Exclusive to
     * form tags with the "address" parameter.
     */
    public function phone()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns true if the form was submitted successfully, or false if the form contained
     * errors. All forms but the address form set that property. The address form is
     * always submitted successfully.
     *
     * {% if form.posted_successfully? %}
     *     Comment posted successfully!
     * {% else %}
     *     {{ form.errors | default_errors }}
     * {% endif %}
     */
    public function posted_successfully()
    {
        return (bool) session('forms.'.$this->formId.'.posted_successfully', false);
    }

    public function password_needed()
    {
        return true;
    }
}
