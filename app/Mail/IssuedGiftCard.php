<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class IssuedGiftCard extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $shopFromEmail = $this->data['shopFromEmail'] ?? config('mail.from.address');

        return $this->subject('Your '.$this->data['shopName'].' gift card is ready!')
            ->replyTo($this->data['shopEmail'], $this->data['shopName'])
            ->from($shopFromEmail, $this->data['shopName'])
            ->view('emails.issue-gift-card')
            ->with($this->data);
    }
}
