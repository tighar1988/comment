@extends('admin.setup.shipping.shipping')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                @if (! usps_enabled())
                    <form class="form" role="form" method="POST" action="{{ url('/admin/setup/goshippo-api-token') }}">
                        <span class="settings-info">Setup your <a href="http://lets.goshippo.com/commentsold">goShippo.com</a> API Token. You can find it in your <a href="http://lets.goshippo.com/commentsold">goShippo.com</a> account.</span>
                        <span class="settings-muted">Pricing: It's absolutely free to have a Shippo account - they only charge when you create a shipping label. There are no setup or monthly fees.</span>

                        <div class="form-group @hasError('api_token')">
                            <label class="control-label">API Live Token</label>
                            <input type="text" class="form-control" name="api_token" value="{{ old('api_token', shop_setting('goshippo.api_token')) }}">
                            @error('api_token')
                        </div>

                        <button class="btn btn-success" type="submit">Update goShippo API Token</button>
                    </form>
                    <hr/>
                @endif

                <span class="settings-info">Setup your shipping cost. This is what will be charged to your customers.</span>
                <span class="settings-muted text-muted">This will be applied to the whole order.</span>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/shipping-cost') }}">
                    <div class="form-group @hasError('shipping_cost')">
                        <label class="control-label">Shipping Cost</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" name="shipping_cost" value="{{ old('shipping_cost', shop_setting('shop.shipping-cost', 10)) }}">
                        </div>
                        @error('shipping_cost')
                    </div>
                    <button class="btn btn-success" type="submit">Update Shipping Cost</button>
                </form>

                <hr/>

                <span class="settings-info">(Optional) Setup a variable shipping cost.</span>
                <span class="settings-muted text-muted">
                    Normally the shipping cost is applied to the whole order. If you want to apply the shipping cost only to the first item in the order, <br/>
                    then add a different cost for every extra item, you should use the variable shipping cost. <br/><br/>

                    Example: For a $4 flat rate shipping cost, plus a $1 variable shipping cost for each additional item in that same order. <br/>
                            If the customer has 3 items in the order, it will be $4 + $1 + $1 = $6 for shipping.
                </span>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/variable-shipping-cost') }}">
                    <div class="form-group @hasError('variable_shipping_cost')">
                        <label class="control-label">Variable Shipping Cost <small class="text-muted">(Optional)</small></label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" name="variable_shipping_cost" value="{{ old('variable_shipping_cost', shop_setting('shop.variable-shipping-cost')) }}">
                        </div>
                        @error('variable_shipping_cost')
                    </div>
                    <button class="btn btn-success" type="submit">Update Variable Shipping Cost</button>
                </form>

                <hr/>

                <span class="settings-info">Setup your Free Shipping maximum.</span>
                <span class="settings-muted text-muted">Orders with subtotal above this value will get free shipping (after coupon is applied). If this value is 0, it means no free shipping.</span>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/free-shipping-maximum') }}">
                    <div class="form-group @hasError('shipping_maximum')">
                        <label class="control-label">Free Shipping Maximum</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" name="shipping_maximum" value="{{ old('shipping_maximum', shop_setting('shop.free-shipping-maximum', 75)) }}">
                        </div>
                        @error('shipping_maximum')
                    </div>
                    <button class="btn btn-success" data-loading-text="Updating Free Shipping Maximum.." type="submit">Update Free Shipping Maximum</button>
                </form>

                <hr/>

                <span class="settings-info">24 hours Free Shipping</span>
                <span class="settings-muted text-muted">
                    If this option is enabled, the customer will get Free Shipping if they ordered in the last 24 hours.
                </span>

                @if (free_shipping_24hr_enabled())
                    <form class="form-horizontal" role="form" method="POST" action="/admin/setup/24h-free-shipping/disable">
                        <button type="submit" class="btn btn-danger">Disable 24hr Free Shipping</button>
                    </form>
                @else
                    <form class="form-horizontal" role="form" method="POST" action="/admin/setup/24h-free-shipping/enable">
                        <button type="submit" class="btn btn-success">Enable 24hr Free Shipping</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                @if (local_pickup_enabled())
                    <span class="settings-info">Delivery Method: Local Pickup (enabled)</span>
                    <form class="" role="form" method="POST" action="{{ url('/admin/setup/local-pickup/disable') }}">
                        <button class="btn btn-danger" type="submit">Disable Local Pickup</button>
                    </form>
                @else
                    <span class="settings-info">Delivery Method: Local Pickup (disabled)</span>
                    <form class="" role="form" method="POST" action="{{ url('/admin/setup/local-pickup/enable') }}">
                        <button class="btn btn-success" type="submit">Enable Local Pickup</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <span class="settings-info">Combine orders into the same shipment</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="shipping.combine-orders" @if(shop_setting('shipping.combine-orders', false)) checked="" @endif class="toggle-switch"></label></div>

                <span class="settings-muted text-muted">
                    If this option is enabled, when mass buying labels, only a single label will be bought for orders with the same address. Also when mass printing packing slips, the orders will be grouped for the single shipping label.
                </span>
            </div>
        </div>
    </div>
</div>
@endsection
