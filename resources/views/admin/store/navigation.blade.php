@extends('admin.store.store')

@section('section', 'Navigation')

@section('store')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body r-p">
                <a href="/admin/store/navigation/add" class="btn btn-success add-btn btn-sm">Add Menu</a>
                <table id="navigation-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Menu Items</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($menus as $menu)
                            <tr>
                                <td>{{ $menu->title }}</td>
                                <td>{{ $menu->itemNames() }}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-h fa-lg"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                            <li><a href="/admin/store/navigation/edit/{{ $menu->id }}" class="link">Edit</a></li>
                                            <li><a href="/admin/store/navigation/delete/{{ $menu->id }}" class="link" onclick="return confirm_delete(this);">Remove</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#navigation-table').DataTable();
    });
</script>
@endsection
