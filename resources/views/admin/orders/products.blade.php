<h3 class="subheading">Products In The Order</h3>

<div class="table-responsive">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
        <tr>
            <th>Image</th>
            <th>Product Name</th>
            <th>SKU</th>
            <th>Price</th>
            <th>Size</th>
            <th>Color</th>
            <th>Source Comment</th>
        </tr>
        @if ($order->orderProducts->isEmpty())
            <tr><td colspan="6">No products in this order.</td></tr>
        @endif

        @foreach($order->orderProducts as $item)
            <tr class="@if (! $order->isFulfilled()) danger unscanned @else success @endif" data-shopifyBarcode="{{ $item->shopifyBarcode() }}" data-inventoryId="{{ $item->inventory_id }}">
                <td>
                    <img class="img-circle" width="100" src="{{ product_image($item->product_filename) }}" alt="{{ $item->prod_name }}">
                </td>
                <td>
                    <a href="/admin/products/{{ $item->product_id }}/variants">{{ $item->prod_name }}</a>
                    @if (! empty($item->returned_date))
                        <span style="font-size: 9px;" class="label label-danger">RETURNED</span>
                    @endif
                </td>
                <td>{{ $item->product_style }}</td>
                <td>${{ amount($item->price) }}</td>
                <td>{{ $item->size }}</td>
                <td>{{ $item->color }}</td>
                <td>
                    @if ($item->sourceIsFacebookComment())
                        {{ $item->facebookComment->content ?? null }}
                        @if (isset($item->facebookComment->stamped))
                            <small class="text-muted">({{ style_date($item->facebookComment->stamped) }})</small>
                        @endif
                    @elseif ($item->sourceIsInstagramComment())
                        [Instagram] {{ $item->instagramComment->content ?? null }}
                    @elseif ($item->sourceIsOnlineStore())
                        [Online Store]
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>

@if (! $scanLogs->isEmpty())
    <div class="row">
        <div class="col-sm-12">
            <table class="table">
                <tr>
                    <th>Scan Date</th>
                    <th>Result</th>
                </tr>
                @foreach($scanLogs as $scanLog)
                    <tr>
                        <td>{{ style_date($scanLog->scan_date) }}</td>
                        <td>{{ $scanLog->result }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-sm-12">
        <button type="button" class="btn btn-default m-r-10" onclick="closeDetails();">Cancel</button>
        @if ($order->isPaid() && shop_id() == 'fashionten')
            @can('view-inventory-logs')
                <button type="button" class="btn btn-success" onclick="fulfillOrder({{ $order->id }})">Mark As Fulfilled</button>
            @endcan
        @endif
        @if ($order->isPaid() && shop_id() != 'fillyflair' && shop_id() != 'fashionten')
            <button type="button" class="btn btn-success" onclick="fulfillOrder({{ $order->id }})">Mark As Fulfilled</button>
        @elseif ($order->isLocalPickup() && ! $order->hasDetail('picked-up'))
            <button type="button" class="btn btn-info" onclick="pickUpOrder({{ $order->id }})">Mark As Picked Up</button>
        @endif
    </div>
</div>
