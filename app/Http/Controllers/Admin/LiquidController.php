<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Liquid;
use App\Http\Requests\Admin\LiquidFormRequest;
use Image;
use Storage;

class LiquidController extends Controller
{
    protected $liquid;

    public function __construct(Liquid $liquid)
    {
        $this->liquid = $liquid;
    }

    /**
     * Show liquid files grid.
     */
    public function index($type)
    {
        $liquid = $this->liquid->getFiles($type, get_theme());
        return view('admin.store.liquid-grid', compact('liquid', 'type'));
    }

    /**
     * Show the form to add a new file.
     */
    public function create($type)
    {
        if ($type == 'assets') {
            return view('admin.store.liquid-form-assets');
        } else {
            $liquid = $this->liquid;
            return view('admin.store.liquid-form', compact('liquid', 'type'));
        }
    }

    /**
     * Store new file to the database.
     */
    public function store($type, LiquidFormRequest $request)
    {
        $this->liquid->fill($request->input());
        $this->liquid->type = $type;
        $this->liquid->theme = get_theme();
        $this->liquid->save();
        flash(ucfirst($type).' file saved!');
        return redirect("/admin/store/themes/{$type}");
    }

    /**
     * Edit liquid file.
     */
    public function edit($type, $liquidId)
    {
        $liquid = $this->liquid->find($liquidId);
        return view('admin.store.liquid-form', compact('liquid', 'type'));
    }

    /**
     * Open a default liquid file from the theme.
     */
    public function change($type, $fileName, Liquid $liquid)
    {
        $content = app('files')->get(resource_path("views/themes/".get_theme()."/{$type}/{$fileName}"));

        $liquid->file_name = $fileName;
        $liquid->content = $content;

        return view('admin.store.liquid-form', compact('liquid', 'type'));
    }

    /**
     * Update a liquid file.
     */
    public function update($type, $liquidId, LiquidFormRequest $request)
    {
        $liquid = $this->liquid->find($liquidId);
        $liquid->fill($request->input());
        $liquid->save();
        flash(ucfirst($type).' file updated!');
        return redirect("/admin/store/themes/{$type}");
    }

    /**
     * Delete a file.
     */
    public function destroy($type, $liquidId)
    {
        $this->liquid->destroy($liquidId);
        flash(ucfirst($type).' file deleted!');
        return redirect("/admin/store/themes/{$type}");
    }

    /**
     * Open an asset file.
     */
    public function open($fileName)
    {
        $liquid = $this->liquid->type('assets')->file($fileName)->first();
        if ($liquid) {
            // temporarily download the file
            $contents = Storage::get(shop_id()."/assets/{$fileName}");
            Storage::disk('local')->put("tmp/".shop_id()."/assets/{$fileName}", $contents);

            // download the file and delete it
            $pathToFile = storage_path("app/tmp/".shop_id()."/assets/{$fileName}");
            return response()->file($pathToFile)->deleteFileAfterSend(true);

        } else {
            $pathToFile = resource_path("views/themes/".get_theme()."/assets/{$fileName}");
            return response()->file($pathToFile);
        }
    }

    /**
     * Upload new asset file.
     */
    public function upload(Request $request)
    {
        $response = [];

        foreach ($request->file('files') as $file) {

            $fileName = $file->getClientOriginalName();
            $file->storePubliclyAs(shop_id().'/assets', $fileName);

            $liquid = Liquid::type('assets')->file($fileName)->first() ?? new Liquid;
            $liquid->type = 'assets';
            $liquid->setFileName($fileName, 'assets');
            $liquid->theme = get_theme();
            $liquid->save();

            $response[] = ['name' => $fileName];
        }

        return ['files' => $response];
    }
}
