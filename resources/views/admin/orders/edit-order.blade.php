@if (shop_id() == 'fillyflair' || shop_id() == 'shopzigzagstripe')
    Customer Name: <strong>{{ $order->ship_name }}</strong><br/>
    Email: <strong>{{ $order->email }}</strong><br/>
    Street Address: <strong>{{ $order->street_address }}</strong><br/>
    Apartment: <strong>{{ $order->apartment }}</strong><br/>
    City: <strong>{{ $order->city }}</strong><br/>
    State: <strong>{{ $order->state }}</strong><br/>
    Country: <strong>{{ $order->getCountryCode() }}</strong><br/>
    Zip: <strong>{{ $order->zip }}</strong><br/>
@else

<h3 class="subheading">Edit Delivery Address</h3>

@if ($order->local_pickup)
    <div class="row">
        <div class="col-sm-12">
            <h2>Delivery: Local Pickup</h2>
            @if ($order->hasDetail('local-pickup-location'))
                Store Location: <span class="text-muted">{{ $order->getDetail('local-pickup-location') }}</span>
            @else
                <?php $location = $order->location->location ?? null; ?>
                @if ($location)
                    Store Location: <span class="text-muted">{{ $location }}</span>
                @endif
            @endif
        </div>
    </div>

@else
    <form class="" onsubmit="return updateOrder({{ $order->id }}, this)" role="form" method="POST" action="{{ url('/admin/orders/'.$order->id.'/update') }}">
        <div class="row">
            <div class="col-sm-4">
                <label>Customer Name</label>
                <input type="text" class="form-control" name="ship_name" value="{{ $order->ship_name }}">
            </div>
            <div class="col-sm-4">
                <label>E-mail address</label>
                <input type="text" class="form-control" name="email" value="{{ $order->email }}">
            </div>
            <div class="col-sm-4">
                <label>Street Address</label>
                <input type="text" class="form-control" name="street_address" value="{{ $order->street_address }}">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label>Apt / Unit / Suite</label>
                <input type="text" class="form-control" name="apartment" value="{{ $order->apartment }}">
            </div>
            <div class="col-sm-4">
                <label>City</label>
                <input type="text" class="form-control" name="city" value="{{ $order->city }}">
            </div>
            <div class="col-sm-4">
                <label>Zip</label>
                <input type="text" class="form-control" name="zip" value="{{ $order->zip }}">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label>Country</label>
                <select class="form-control" id="country-form-input" name="country_code">
                    <option @if ($order->country_code == 'US') selected="selected" @endif value="US">USA</option>
                    <option @if ($order->country_code == 'CA') selected="selected" @endif value="CA">Canada</option>
                </select>
            </div>
            <div class="col-sm-4">
                <label>State</label>
                <select @if ($order->country_code != 'CA') style="display: none !important;" @endif id="modal-state-text" class="form-control" name="state_text">
                    <option value="" disabled selected>State..</option>
                    <option @if ($order->state == 'AB') selected="selected" @endif value="AB">Alberta</option>
                    <option @if ($order->state == 'BC') selected="selected" @endif value="BC">British Columbia</option>
                    <option @if ($order->state == 'MB') selected="selected" @endif value="MB">Manitoba</option>
                    <option @if ($order->state == 'NB') selected="selected" @endif value="NB">New Brunswick</option>
                    <option @if ($order->state == 'NL') selected="selected" @endif value="NL">Newfoundland</option>
                    <option @if ($order->state == 'NT') selected="selected" @endif value="NT">Northwest Territories</option>
                    <option @if ($order->state == 'NS') selected="selected" @endif value="NS">Nova Scotia</option>
                    <option @if ($order->state == 'NU') selected="selected" @endif value="NU">Nunavut</option>
                    <option @if ($order->state == 'ON') selected="selected" @endif value="ON">Ontario</option>
                    <option @if ($order->state == 'PE') selected="selected" @endif value="PE">Prince Edward Island</option>
                    <option @if ($order->state == 'QC') selected="selected" @endif value="QC">Quebec</option>
                    <option @if ($order->state == 'SK') selected="selected" @endif value="SK">Saskatchewan</option>
                    <option @if ($order->state == 'YT') selected="selected" @endif value="YT">Yukon</option>
                </select>

                <select @if ($order->country_code == 'CA') style="display: none !important;" @endif id="modal-state" class="form-control" name="state">
                    @include('common.states', ['selected' => $order->state])
                </select>
            </div>
            @if ($order->customer_id != 0)
            <div class="col-sm-4">
                <label>&nbsp;</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="update_customer" checked>
                        If checked, the customer's record will be updated for future orders as well.
                    </label>
                </div>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="button" class="btn btn-default m-r-10" onclick="closeDetails();">Cancel</button>
                <button type="submit" class="btn btn-success">Update Order</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-success message" style="display: none;"></div>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#country-form-input').change(function() {
                var country = $(this).val();
                if (country == 'CA') {
                    $('#modal-state-text').show().val('');
                    $('#modal-state').attr('style', 'display: none !important;');
                } else {
                    $('#modal-state').show();
                    $('#modal-state-text').attr('style', 'display: none !important;');
                }
            });
        });
    </script>
@endif
@endif
