<?php

namespace App\OnlineStore\Vendor\Liquid\Tag;

use App\OnlineStore\Vendor\Liquid\AbstractTag;
use App\OnlineStore\Vendor\Liquid\Document;
use App\OnlineStore\Vendor\Liquid\Context;
use App\OnlineStore\Vendor\Liquid\Liquid;
use App\OnlineStore\Vendor\Liquid\LiquidException;
use App\OnlineStore\Vendor\Liquid\FileSystem;
use App\OnlineStore\Vendor\Liquid\Regexp;
use App\OnlineStore\Vendor\Liquid\Template;
use App\OnlineStore\CurrentView;

class TagSchema extends AbstractTag
{
    /**
     * Constructor
     *
     * @param string $markup
     * @param array $tokens
     * @param FileSystem $fileSystem
     *
     * @throws \Liquid\LiquidException
     */
    public function __construct($markup, array &$tokens, FileSystem $fileSystem = null) {
        parent::__construct($markup, $tokens, $fileSystem);
    }

    /**
     * Parses the tokens
     *
     * @param array $tokens
     *
     * @throws \Liquid\LiquidException
     */
    public function parse(array &$tokens) {
        $tagRegexp = new Regexp('/^' . Liquid::get('TAG_START') . '\s*(\w+)\s*(.*)?' . Liquid::get('TAG_END') . '$/');

        if (! is_array($tokens)) {
            return;
        }

        $json = '';
        $endTag = false;
        while (count($tokens) && ! $endTag) {
            $token = array_shift($tokens);

            if ($tagRegexp->match($token)) {
                if ($tagRegexp->matches[1] == 'endschema') {
                    $endTag = true;
                    CurrentView::$schemas[CurrentView::$sectionName] = json_decode($json);
                }
            }

            $json .= $token;
        }
    }

    /**
     * Renders the node
     *
     * @param Context $context
     *
     * @return string
     */
    public function render(Context $context) {
        return '';
    }
}
