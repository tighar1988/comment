@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Add Coupon</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/coupons"><i class="fa fa-th-large"></i> Coupons</a></li>
        <li class="active">Add New Coupon</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}">

                        @if (isset($customer))
                            <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                            <input type="hidden" name="customer_name" value="{{ $customer->name }}">
                            Creating coupon that can be used only by: <strong>{{ $customer->name }}</strong> (Customer ID: {{ $customer->id }})
                            <br/><br/>

                        @else
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="once_per_user" value="0">
                                            <input type="checkbox" @if(old('once_per_user')) checked="checked" @endif name="once_per_user" value="1"> Only used once per customer?
                                            <span class="text-muted">If checked, the coupon can be applied only once by each customer.</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="new-users-checkbox" @if(old('new_users')) checked="checked" @endif name="new_users"> Coupon for new users?
                                            <span class="text-muted">If checked, the coupon can be applied only in the first days after the customer registers.</span>
                                            @if(old('new_users'))
                                                <input type="hidden" id="new-users-period" name="new_users_period" value="1">
                                            @else
                                                <input type="hidden" id="new-users-period" name="new_users_period" value="0">
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="days-valid-wrapper" @if(! old('new_users')) style="display: none;" @endif>
                                <div class="col-md-6">
                                    <div class="form-group @hasError('days_valid')">
                                        <label>Days Valid</label>
                                        <input type="text" class="form-control" name="days_valid" value="{{ old('days_valid', 7) }}">
                                        @error('days_valid')
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="minimum_purchase" value="0">
                                            <input type="checkbox" id="minimum-purchase-checkbox" @if(old('minimum_purchase')) checked="checked" @endif name="minimum_purchase" value="1"> This discount requires a minimum purchase
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="minimum-purchase-amount-wrapper" @if(! old('minimum_purchase')) style="display: none;" @endif>
                                <div class="col-md-6">
                                    <div class="form-group @hasError('minimum_purchase_amount')">
                                        <label>Minimum Purchase Amount</label>
                                        <input type="text" class="form-control" name="minimum_purchase_amount" value="{{ old('minimum_purchase_amount') }}">
                                        @error('minimum_purchase_amount')
                                    </div>
                                </div>
                            </div>

                            <hr/>
                        @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('code')">
                                    <label>Coupon Code</label>
                                    <input type="text" class="form-control" maxlength="30" name="code" value="{{ old('code') }}">
                                    @error('code')
                                </div>
                            </div>
                        </div>

                        <div class="row" id="max-usage-wrapper" @if(old('new_users')) style="display: none;" @endif>
                            <div class="col-md-6">
                                <div class="form-group @hasError('max_usage')">
                                    <label>Max Usage <small class="text-muted">- use 0 for unlimited</small></label>
                                    <input class="form-control" type="text" name="max_usage" value="{{ old('max_usage') }}">
                                    @error('max_usage')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('starts_at')">
                                    <label>Start Date <small class="text-muted">- leave blank for no start</small class="text-muted"></label>
                                    <input id="starts_at" class="form-control" type="text" name="starts_at" value="{{ old('starts_at') }}">
                                    @error('starts_at')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('expires_at')">
                                    <label>Expiry Date <small class="text-muted">- leave blank for no expiry</small class="text-muted"></label>
                                    <input id="expires_at" class="form-control" type="text" name="expires_at" value="{{ old('expires_at') }}">
                                    @error('expires_at')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('coupon_type')">
                                    <label>Coupon Type</label>
                                    <select id="coupon-type" name="coupon_type" class="form-control">
                                        <option value="">Select a coupon type..</option>
                                        <option value="S" {{ old('coupon_type') == 'S' ? 'selected' : '' }}>Free Shipping</option>
                                        <option value="F" {{ old('coupon_type') == 'F' ? 'selected' : '' }}>Flat Amount Off</option>
                                        <option value="P" {{ old('coupon_type') == 'P' ? 'selected' : '' }}>Percentage Off</option>
                                    </select>
                                    @error('coupon_type')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div @if (old('coupon_type') == 'S') style="display: none;" @endif class="amount-input form-group @hasError('amount')">
                                    <label>Amount <small>- use only numbers</small></label>
                                    <input class="form-control" type="text" name="amount" value="{{ old('amount') }}">
                                    @error('amount')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('description')">
                                    <label>Description <small class="text-muted">(optional)</small></label>
                                    <textarea maxlength="255" class="form-control" name="description" rows="3">{{ old('description') }}</textarea>
                                    @error('description')
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-success pull-left" type="submit">Add Coupon</button>
                    </form>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ cdn('/datetimepicker-master/build/jquery.datetimepicker.min.css') }}" type="text/css">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ cdn('/datetimepicker-master/build/jquery.datetimepicker.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#minimum-purchase-checkbox').change(function() {
            if($(this).is(":checked")) {
                $('#minimum-purchase-amount-wrapper').show();
            } else {
                $('#minimum-purchase-amount-wrapper').hide();
            }
        });

        $('#new-users-checkbox').change(function() {
            if($(this).is(":checked")) {
                $('#new-users-period').val(1);
                $('#days-valid-wrapper').show();
                $('#max-usage-wrapper').hide();
            } else {
                $('#new-users-period').val(0);
                $('#days-valid-wrapper').hide();
                $('#max-usage-wrapper').show();
            }
        });

        $('#coupon-type').change(function() {
            if ($(this).val() == 'S') {
                $('.amount-input').hide();
            } else {
                $('.amount-input').show();
            }
        });

        $('#starts_at, #expires_at').datetimepicker({
            // format: 'mm/dd/yyyy',
            format:'m/d/Y H:00:00',
            autoclose: true,
            onSelect: function(dateText) {
                $(this).change();
            }
        });
    });
</script>
@endsection
