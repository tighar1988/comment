<?php

namespace Tests\Unit\Shopify;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Shopify\ShopifyCurlException;

class ShopifyCurlExceptionTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(ShopifyCurlException::class, new ShopifyCurlException);
    }
}
