@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Customer ({{ $customer->name }})</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/customers"><i class="fa fa-th-large"></i> Customers</a></li>
        <li><a href="/admin/customers/{{ $customer->id }}">{{ $customer->name }}</a></li>
        <li class="active">Details</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">

                    @include('admin.customers.customer-shopping-cart')
                    <br/><br/><br/>

                    <h3 style="padding-left: 10px;">Paid Orders</h3>
                    @include('admin.customers.orders', ['orders' => $customer->ordersPaid])
                    <br/><br/><br/>

                    <h3 style="padding-left: 10px;">Fulfilled Orders <small class="text-muted">(Return Percentage: {{ percentage($customer->returnPercentage()) }}%)</small></h3>
                    @include('admin.customers.orders', ['orders' => $customer->ordersFulfilled])
                    <br/><br/><br/>

                    <h3 style="padding-left: 10px;">Waitlist</h3>
                    @include('admin.customers.waitlist')
                    <br/><br/><br/>

                    <h3 style="padding-left: 10px;">Balance Movement <small class="text-muted">(Current Balance: ${{ amount($customer->balance) }})</small></h3>
                    @include('admin.customers.balance-movement', ['balanceMovement' => $customer->balanceMovement])
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    var productsTableLoaded = false;

    $(document).ready(function() {
        $('.add-to-cart-btn').on('click', function() {
            if (productsTableLoaded == false) {
                productsTableLoaded = true;
                $('#products-search').DataTable({
                    "ordering": false,
                    "paging": true,
                    "lengthChange": false,
                    "pageLength": 5,
                    "serverSide": true,
                    "order": [[ 0, "desc" ]],
                    "info": false,
                    "language": {
                        "searchPlaceholder": "SKU / Product Name",
                        "sEmptyTable": "No products found."
                    },
                    "ajax": "/admin/api/products/list",
                    columns: [
                        { data: 'style', 'searchable': true, render: function(data, type, row) {
                            return row.product_name + ' (#' + row.style + ')';
                        }},
                        { data: 'color', 'searchable': false},
                        { data: 'size', 'searchable': false},
                        { data: 'quantity', 'searchable': false},
                        { data: null, 'orderable': false, 'searchable': false, render: function(data) {
                            return '<form method="POST" action="/admin/customers/{{ $customer->id }}/add-to-cart/' + data.id + '"><button type="submit" onclick="button_loading(this);" data-loading-text="Adding.." class="btn btn-sm btn-danger">Add</button></form>';
                        }},
                    ]
                });
            }

            $('#add-to-cart-modal').modal();
        });
    });

    function button_loading(button) {
        $(button).button('loading');
    }
</script>
@endsection
