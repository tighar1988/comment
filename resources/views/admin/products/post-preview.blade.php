@extends('layouts.admin')

@section('content')
@if (empty($groups))
    <div class="row"><div class="top-alert flash-notification alert alert-danger alert-important">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        You have no facebok groups enabled. Go to <a href="/admin/facebook-setup">Facebook Setup</a> to connect your facebook and enable groups.</div></div>
@elseif (empty($image))
    <div class="row"><div class="top-alert flash-notification alert alert-danger alert-important">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        This product has no image. In order to post to Facebook an image is needed. Go to <a href="/admin/products/{{ $product->id }}/images">Product Images</a> to add images.</div></div>
@endif
<section class="content-header">
    <h1>Product ({{ $product->style }})</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/products"><i class="fa fa-th-large"></i> Products</a></li>
        <li><a href="/admin/products/edit/{{ $product->id }}">{{ $product->product_name }}</a></li>
        <li class="active">Post to Facebook</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body lr-m-30">

                    <div class="row">
                        <div class="col-md-6">

                            <form class="form-horizontal" id="post-to-facebook" role="form" method="POST" action="{{ form_action() }}">

                                <div class="form-group">
                                    <label>Select the group to post your product. You have to own the group to post.</label>
                                </div>

                                <div class="form-group @hasError('group')">
                                    <label class="control-label">Facebook Group</label>
                                    <select class="form-control" name="group">
                                        @if (count($enabledGroups) > 1 || empty($enabledGroups))
                                            <option value="">Select a facebook group..</option>
                                        @endif
                                        @foreach ($groups as $group)
                                            @if (in_array($group['id'], $enabledGroups))
                                                <option {{ selected('group', $group['id']) }} value="{{ $group['id'] }}">{{ $group['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @error('group')
                                </div>

                                @if (! empty($pageName))
                                    <div class="form-group @hasError('post_as')">
                                        <label class="control-label">Post As</label>
                                        <div class="radio">
                                            <label><input type="radio" @if(old('post_as', 1) == 1) checked="checked" @endif value="1" name="post_as">Group Admin</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" @if(old('post_as') == 2) checked="checked" @endif value="2" name="post_as">Linked Page "{{ $pageName }}" <small class="text-muted">Please make sure the page is linked to the group.</small></label>
                                        </div>
                                        @error('post_as')
                                    </div>
                                @endif

                                <div class="form-group @hasError('message')">
                                    <label class="control-label">Post Message</label>
                                    @if (\App\Models\Order::count() > 86)
                                        <textarea class="form-control" rows="10" name="message">{{ old('message', $message) }}</textarea>
                                        <blockquote style="margin-top: 5px;"><p style="white-space: pre-wrap;">Copy & paste your shop url to 'Post Message'<br/><span class="text-muted">"Go to {{ shop_url('/account') }} to view/pay your invoice."</span></p></blockquote>
                                    @else
                                        <textarea class="form-control" rows="10" name="message">{{ old('message') }}</textarea>
                                        <blockquote style="margin-top: 5px;">
<p style="white-space: pre-wrap;">Copy & paste to 'Post Message'<br/> <span class="text-muted">
{{ $message }}
@if (shop_id() != 'glamourfarms')
Go to {{ shop_url('/account') }} to view/pay your invoice.
@endif
</span>
</p>
                                    </blockquote>
                                    @endif
                                    @error('message')
                                </div>

                                <div class="form-group @hasError('product_image')">
                                    <img style="max-height: 300px;" src="{{ product_image($image) }}" />
                                    @error('product_image')
                                </div>

                                <div class="form-group @hasError('timing')">
                                    <label class="control-label">Schedule Timing</label>
                                    <select name="timing" class="form-control" onchange="swapSchedule(this)">
                                        <option {{ selected('timing', 1) }} value="1">Post Immediately</option>
                                        <option {{ selected('timing', 2) }} value="2">Schedule Later</option>
                                    </select>
                                    @error('timing')
                                </div>

                                <div id="later-date" @if(old('timing') != '2') style="display: none;" @endif>

                                    @if (! shop_setting('shop.timezone'))
                                        <div class="row">
                                            <div class="alert alert-danger">
                                                In order to calculate the correct post time relative to your timezone, please select your timezone in <a href="/admin/setup#timezone">Setup</a>.
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group @hasError('schedule_date')">
                                        <label class="control-label"><i>Scheduled Date</i></label>
                                        <select class="form-control" name="schedule_date">
                                            @for ($i=0; $i<=30; $i++)
                                                <?php
                                                    $stamp = time() + (60 * 60 * 24 * $i);
                                                    $date = date('n/j/Y', $stamp);
                                                ?>
                                                <option {{ selected('schedule_date', $date) }} value="{{ $date }}">{{ date('F jS', $stamp) }}</option>
                                            @endfor
                                        </select>
                                        @error('schedule_date')
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label"><i>Scheduled Time</i></label>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select class="form-control" name="hour">
                                                    @for ($i = 0; $i <= 12; $i++)
                                                        <option {{ selected('hour', $i) }} value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control" name="min">
                                                    @for ($i = 0; $i <= 11; $i++)
                                                        <?php
                                                            $min = $i * 5;
                                                            if (strlen($min) == 1) {
                                                                $min = '0' . $min;
                                                            }
                                                        ?>
                                                        <option {{ selected('min', $min) }} value="{{$min}}">{{$min}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control" name="ampm">
                                                    <option {{ selected('ampm', 'am') }} value="am">AM</option>
                                                    <option {{ selected('ampm', 'pm') }} value="pm">PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Post Comment <span class="text-muted">(Optional)</span></label>
                                    <input class="form-control" type="text" name="post_comment" value="{{ old('post_comment') }}"/>
                                    <div><small class="text-muted">This will be added as the first comment under the post. It is useful for sharing YouTube videos.</small></div>
                                </div>

                                <br/>

                                <div class="form-group">
                                    <button class="btn btn-success" data-loading-text="Submitting..." type="submit">Submit to Facebook</button>
                                </div>

                            </form>

                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#post-to-facebook').submit(function() {
        $('button[type="submit"]', this).button('loading');
        return true;
    });
</script>
@endsection
