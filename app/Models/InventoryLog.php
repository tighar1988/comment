<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class InventoryLog extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventory_log';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inventory_id',
        'from_inv',
        'to_inv',
        'movement',
    ];

    public static function log($inventoryId, $fromQuantity, $toQuantity, $movement)
    {
        static::create([
            'inventory_id' => $inventoryId,
            'from_inv'     => $fromQuantity,
            'to_inv'       => $toQuantity,
            'movement'     => $movement,
        ]);
    }

    public static function report($search = null)
    {
        $query = static::select(DB::raw('inventory_log.*,
                inventory.product_id, inventory.color, inventory.size, inventory.price,
                products.style as sku, products.brand, products.brand_style, products.product_name'))
            ->join('inventory', 'inventory_log.inventory_id', '=', 'inventory.id')
            ->join('products', 'inventory.product_id', '=', 'products.id')
            ->orderBy('inventory_log.id', 'DESC');

        if (! is_null($search)) {
            $query->where('products.style', 'like', "%{$search}%");
            $query->orWhere('products.brand', 'like', "%{$search}%");
            $query->orWhere('products.brand_style', 'like', "%{$search}%");
            $query->orWhere('products.product_name', 'like', "%{$search}%");
        }

        return $query;
    }
}
