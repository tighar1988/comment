<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\OrderProduct as OrderProductModel;

class OrderProduct extends Drop
{
    protected $orderProduct;

    public function __construct(OrderProductModel $orderProduct)
    {
        $this->orderProduct = $orderProduct;
    }

    public function product_filename()
    {
        return $this->orderProduct->product_filename;
    }

    public function product_name()
    {
        return $this->orderProduct->prod_name;
    }

    public function name()
    {
        return $this->orderProduct->name;
    }

    public function price()
    {
        return $this->orderProduct->price;
    }

    public function color()
    {
        return $this->orderProduct->color;
    }

    public function size()
    {
        return $this->orderProduct->size;
    }
}
