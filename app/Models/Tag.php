<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tagging_tags';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag_group_id',
        'name',
    ];

    public function scopeTag($query, $tag)
    {
        return $query->where('name', '=', $tag);
    }

    public function products()
    {
        return $this->morphedByMany('App\Models\Product', 'taggable', 'tagging_tagged')->published();
    }
}
