<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\NavigationFormRequest;
use App\Http\Requests\Admin\MenuItemFormRequest;
use App\Models\Menu;
use App\Models\MenuItem;

class NavigationController extends Controller
{
    public function index()
    {
        $menus = Menu::with('items')->get();
        return view('admin.store.navigation', compact('menus'));
    }

    public function create()
    {
        return view('admin.store.navigation-form');
    }

    public function store(NavigationFormRequest $request)
    {
        $menu = new Menu([
            'title'  => $request->input('menu_title'),
            'handle' => $request->input('menu_handle'),
        ]);
        $menu->save();
        flash('Menu Saved!');
        return redirect("/admin/store/navigation/edit/{$menu->id}");
    }

    public function edit($menuId)
    {
        $menu = Menu::find($menuId);
        return view('admin.store.navigation-form', compact('menu'));
    }

    public function update($menuId, NavigationFormRequest $request)
    {
        $menu = Menu::find($menuId);
        $menu->title = $request->input('menu_title');
        $menu->handle = $request->input('menu_handle');
        $menu->save();
        flash('Menu Updated!');
        return back();
    }

    public function destroy($menuId)
    {
        Menu::destroy($menuId);
        flash('Menu Deleted!');
        return back();
    }

    public function storeMenuItem(MenuItemFormRequest $request)
    {
        $menu = new MenuItem([
            'menu_id' => $request->input('menu_id'),
            'name'    => $request->input('item_name'),
            'link'    => $request->input('item_link'),
        ]);
        $menu->save();
        flash('Menu Item Saved!');
        return ['status' => 1];
    }

    public function updateMenuItem(MenuItemFormRequest $request)
    {
        $item = MenuItem::find($request->input('item_id'));
        $item->name = $request->input('item_name');
        $item->link = $request->input('item_link');
        $item->save();
        flash('Menu Item Saved!');
        return ['status' => 1];
    }

    public function destroyMenuItem()
    {
        MenuItem::destroy(request('item_id'));
        flash('Menu Item Deleted!');
        return ['status' => 1];
    }

    public function sort()
    {
        $itemIds = request('menu_items');
        $position = 1;

        foreach ($itemIds as $itemId) {
            $menuItem = MenuItem::find($itemId);

            if (! $menuItem) {
                continue;
            }

            $menuItem->position = $position++;
            $menuItem->save();
        }

        return ['status' => 1];
    }
}
