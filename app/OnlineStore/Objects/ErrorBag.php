<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use Illuminate\Support\ViewErrorBag;

class ErrorBag extends Drop
{
    protected $errorBag;

    public function __construct(ViewErrorBag $errorBag)
    {
        $this->errorBag = $errorBag;
    }

    public function handle_key($key)
    {
        $messageBag = $this->errorBag->getBag('default');
        $messages = $messageBag->get($key);

        return $messages[0] ?? null;
    }
}
