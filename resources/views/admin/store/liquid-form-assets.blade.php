@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Store</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/pages"><i class="fa fa-th-large"></i>Store</a></li>
        <li><a href="/admin/store/themes/assets">Assets File</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">

                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Select files...</span>
                        <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <br/>
                    <br/>
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <div id="files" class="files"></div>

                    <br/>
                    <br/>
                    <a href="/admin/store/themes/assets">Back to assets</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ cdn('/vendor/jQuery-File-Upload/css/jquery.fileupload.css') }}" type="text/css">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ cdn('/vendor/jQuery-File-Upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ cdn('/vendor/jQuery-File-Upload/js/jquery.iframe-transport.js') }}"></script>
<script type="text/javascript" src="{{ cdn('/vendor/jQuery-File-Upload/js/jquery.fileupload.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('#fileupload').fileupload({
            url: '/admin/store/themes/assets/upload',
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo('#files');
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        });
    });
</script>
@endsection
