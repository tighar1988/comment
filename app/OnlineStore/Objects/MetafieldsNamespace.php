<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class MetafieldsNamespace extends Drop
{
    /**
     * @var string
     */
    protected $ownerResource;

    protected $metafield;

    protected $cache = [];

    public function __construct($ownerResource)
    {
        $this->ownerResource = $ownerResource;
        $this->metafield = app('App\Models\Metafield');
    }

    public function handle_key($key)
    {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        $metafields = $this->metafield->namespace($key)
            ->ownerResource($this->ownerResource)->get();

        $this->cache[$key] = new Metafields($metafields);
        return $this->cache[$key];
    }
}
