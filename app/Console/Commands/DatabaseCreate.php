<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Shop\DatabaseCreator;

class DatabaseCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:create {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new database.';

    /**
     * The db creator.
     *
     * @var DatabaseCreator
     */
    protected $db;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DatabaseCreator $db)
    {
        parent::__construct();

        $this->db = $db;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->db->create($this->argument('name'));
    }
}
