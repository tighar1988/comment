<?php

namespace Tests\Unit\Shipping;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Shipping\GoShippo;
use Mockery;

class GoShippoTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->mockShopSettings([
            'goshippo.api_token'  => '123456789',
            'shop.from_name'      => 'My Company',
            'shop.street_address' => '1st Street',
            'shop.city'           => 'New York',
            'shop.state'          => 'NY',
            'shop.country_code'   => 'US',
            'shop.zip_code'       => '30003',
            'shop.company_email'  => 'support@example.com',
            'shop.phone'          => '+ (123) 4563 7890',
        ]);
    }

    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(GoShippo::class, new GoShippo);
    }

    /** @test */
    public function it_ruturns_the_phone()
    {
        $this->assertEquals('123 4563 7890', (new GoShippo)->getShopPhone());
    }

    /** @test */
    public function it_ruturns_the_shop_address()
    {
        $this->assertEquals((new GoShippo)->shopFromAddress(), [
            'object_purpose' => 'PURCHASE',
            'name'           => 'My Company',
            'street1'        => '1st Street',
            'city'           => 'New York',
            'state'          => 'NY',
            'zip'            => '30003',
            'country'        => 'US',
            'phone'          => '123 4563 7890',
            'email'          => 'support@example.com',
        ]);
    }

    /** @test */
    public function it_returns_the_to_address()
    {
        $order = $this->mockOrder([
            'ship_name'      => 'John Doe',
            'street_address' => 'Summer street',
            'apartment'      => '32',
            'city'           => 'Clearance',
            'state'          => 'GE',
            'zip'            => '333444',
            'email'          => 'john@example.com',
            'getCountryCode()' => 'US',
        ]);

        $this->assertEquals((new GoShippo)->toAddress($order), [
            'object_purpose' => 'PURCHASE',
            'name'           => 'John Doe',
            'street1'        => 'Summer street',
            'street2'        => '32',
            'city'           => 'Clearance',
            'state'          => 'GE',
            'zip'            => '333444',
            'country'        => 'US',
            'email'          => 'john@example.com',
        ]);
    }

    /** @test */
    public function it_returns_parcel_with_no_dimensions()
    {
        $this->assertEquals((new GoShippo)->getParcel(null), [
            'length'        => '0',
            'width'         => '0',
            'height'        => '0',
            'distance_unit' => 'in',
            'weight'        => '0',
            'mass_unit'     => 'oz',
        ]);
    }

    /** @test */
    public function it_returns_parcel_with_dimensions()
    {
        $dimensions = [
            'length' => 4,
            'width'  => 5,
            'height' => 6,
            'weight' => 7,
        ];

        $this->assertEquals((new GoShippo)->getParcel($dimensions), [
            'length'        => '4',
            'width'         => '5',
            'height'        => '6',
            'distance_unit' => 'in',
            'weight'        => '7',
            'mass_unit'     => 'oz',
        ]);
    }

    /** @test */
    public function it_returns_priority_service_level()
    {
        $dimensions = ['weight' => 25];
        $this->assertEquals('usps_priority', (new GoShippo)->getServiceLevel($dimensions));
    }

    /** @test */
    public function it_returns_first_service_level()
    {
        $dimensions = ['weight' => 15.99];
        $this->assertEquals('usps_first', (new GoShippo)->getServiceLevel($dimensions));
    }
}
