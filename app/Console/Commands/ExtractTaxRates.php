<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
use Comodojo\Zip\Zip;
use League\Csv\Reader;
use Exception;

class ExtractTaxRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commentsold:extract-tax-rates {zipfile}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extract the tax rates from a zip file and save them in a php array.';

    protected $validHeader = [
        0 => 'State',
        1 => 'ZipCode',
        2 => 'TaxRegionName',
        3 => 'StateRate',
        4 => 'EstimatedCombinedRate',
        5 => 'EstimatedCountyRate',
        6 => 'EstimatedCityRate',
        7 => 'EstimatedSpecialRate',
        8 => 'RiskLevel',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $file = storage_path('taxrates/' . $this->argument('zipfile'));
            $this->info("Trying to extract tax rates from {$file}");
            $this->checkFileExists($file);

            $zip = Zip::open($file);
            $zip->extract($folder = storage_path('taxrates/uncompressed/'.uniqid()));

            $taxrates = $this->extractTaxRates($folder, File::allFiles($folder));

            $content = '<?php return ' . var_export($taxrates, true) . ';';
            File::put(storage_path('taxrates/'.time().'.php'), $content);

            $this->info("Tax rates extracted.");

        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function checkFileExists($file)
    {
        if (! File::exists($file)) {
            throw new Exception("The file does not exist " . $file);
        }
    }

    public function extractTaxRates($folder, $files)
    {
        if (! ini_get('auto_detect_line_endings')) {
            ini_set('auto_detect_line_endings', '1');
        }

        $taxrates = [];
        foreach ($files as $file) {
            if ($file->getExtension() == 'csv') {

                $csv = Reader::createFromPath($folder.'/'.$file->getFilename());
                $header = $csv->fetchOne();
                if ($header != $this->validHeader) {
                    throw new Exception("Invalid header format for file {$file->getFilename()}");
                }

                $rows = $csv->setOffset(1)->fetchAll();
                foreach ($rows as $row) {
                    $taxrates[] = $row;
                }
            }
        }

        return $taxrates;
    }
}
