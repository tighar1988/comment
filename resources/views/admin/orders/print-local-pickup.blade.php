<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>:: CommentSold ::</title>
        <link rel="stylesheet" href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" type="text/css">
        <style type="text/css">
            table {
                width: 674px;
                border-collapse: collapse;
                margin-bottom: 5px;
            }
            table, th, td {
                border: 1px solid #ccc;
            }
            th, td {
                padding-left: 15px;
                text-align: left;
            }
            .packing-slip {
                margin-left: 15px;
            }
            .alert {
                background-color: #00c0ef;
                color: #fff;
                border-color: #00acd6;
                border-radius: 3px;
                padding: 15px;
            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: normal;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                cursor: pointer;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #fff;
                background-color: #5cb85c;
                border-color: #4cae4c;
            }
            .text-muted {
                font-size: 15px;
                font-weight: 400;
                color: #777;
            }
        </style>
        <style type="text/css" media="print">
            .no-print {
                display: none;
            }
            @page {
                size: portrait;
            }
        </style>
    </head>
    <body>
        @if (session('local-pickup-printed'))
            <div class="no-print alert">The local pickup orders have been marked as printed!</div>
        @endif

        @if ($orders->isEmpty())
            <h2 class="no-print">You don't have any Local Pickup 'Processing' Orders.</h2>

        @else
            @if (! request('orders'))
                <h2 class="no-print">Total packing slips to be printed: {{ $orders->count() }}</h2>
                <div class="no-print">
                    <form class="" role="form" method="POST" action="/admin/orders/mark-local-pickup-as-printed">
                        <button type="submit" onclick="return confirm('Are you sure to mark the orders as printed? You should only do this after you verified they printed successfully');" class="btn">Mark Local Pickup as Printed</button>
                    </form>
                </div>
            @endif

            <?php
                $hasMultipleLocations = multiple_locations_enabled();
                $hasProductImage = false;
                if (shop_id() == 'cheekys' || shop_id() == 'shopentourageclothing' || shop_id() == 'sweetmsboutique') {
                    $hasProductImage = true;
                }
            ?>
            @foreach ($orders as $order)

                <div class="packing-slip">
                    <h2>Packing Slip: - Order #{{ $order->id }} </h2>

                    Scan this: <br/><br/><img src="data:image/png;base64,{!! $order->getBarcode() !!}"><br/><br/>

                    <h2>
                        {{ $order->ship_name }}
                        @if (shop_id() == 'shopentourageclothing')
                            <span class="text-muted">(Payment Date: {{ style_date($order->created_at) }})</span>
                        @endif
                    </h2>
                    <table>
                        <thead>
                            <th><strong>STYLE</strong></th>
                            <th><strong>PRODUCT</strong></th>
                            <th><strong>COLOR</strong></th>
                            <th><strong>SIZE</strong></th>
                            <th><strong>PRODUCT #</strong></th>
                            <th><strong>LOCATION</strong></th>
                            @if ($hasMultipleLocations)
                                <th><strong>STORE</strong></th>
                            @endif
                            @if ($hasProductImage)
                                <th><strong>PRODUCT IMAGE</strong></th>
                            @endif
                        </thead>
                        <tbody>
                            <?php $index = 1; ?>
                            @foreach($order->orderProducts as $orderProduct)
                                <tr>
                                    <?php
                                        $sku = $orderProduct->variant->sku ?? null;
                                        if (empty($sku)) {
                                            $sku = $orderProduct->product_style;
                                        }
                                    ?>
                                    <td><strong>{{ $index++ }}.</strong> #{{ $sku }}</td>
                                    <td>{{ $orderProduct->prod_name }}</td>
                                    <td>{{ $orderProduct->color }}</td>
                                    <td>{{ $orderProduct->size }}</td>
                                    <td>{{ $orderProduct->product_brand_style }}</td>
                                    <td>{{ $orderProduct->variant->location ?? null }}</td>
                                    @if ($hasMultipleLocations)
                                        <?php $location = $order->location->location ?? null; ?>
                                        @if ($location)
                                            <td>{{ $location }}</td>
                                        @else
                                            <td>{{ $order->details['local-pickup-location'] }}</td>
                                        @endif
                                    @endif
                                    @if ($hasProductImage)
                                        <td style="padding: 0px;"><img src="{{ product_thumb($orderProduct->product_filename ?? null) }}" alt="" width="200px"></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <strong>TOTAL ITEMS: {{ count($order->orderProducts) }}</strong>
                </div>
                <p style="page-break-after:always;"></p>
                    This page is intentionally left blank
                <p style="page-break-after:always;"></p>
            @endforeach

        @endif
    </body>
</html>



