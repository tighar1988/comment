<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeingKeysForTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('prod_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::table('waitlist', function (Blueprint $table) {
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade');
        });

        Schema::table('shopping_cart', function (Blueprint $table) {
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
