<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\BlogFormRequest;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::all();
        return view('admin.store.blog.blogs', compact('blogs'));
    }

    public function create()
    {
        return view('admin.store.blog.blogs-form');
    }

    public function store(BlogFormRequest $request)
    {
        $blog = new Blog($request->input());
        $blog->save();
        flash('Blog Saved!');
        return redirect("/admin/store/blogs");
    }

    public function edit($blogId)
    {
        $blog = Blog::find($blogId);
        return view('admin.store.blog.blogs-form', compact('blog'));
    }

    public function update($blogId, BlogFormRequest $request)
    {
        $blog = Blog::find($blogId);
        $blog->fill($request->input());
        $blog->save();
        flash('Blog Updated!');
        return redirect("/admin/store/blogs");
    }
}
