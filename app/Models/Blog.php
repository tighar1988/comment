<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * Comments are disabled.
     *
     * @var integer
     */
    const COMMENTS_DISABLED = 1;

    /**
     * Comments are allowed, pending moderation.
     *
     * @var integer
     */
    const COMMENTS_PENDING_MODERATION = 2;

    /**
     * Comments are allowed, and are automatically published.
     *
     * @var integer
     */
    const COMMENTS_AUTOMATICALLY_PUBLISHED = 3;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'feedburner_url',
        'seo_page_title',
        'seo_meta_description',
        'handle',
        'comments_type',
    ];

    public function commentsType()
    {
        if ($this->comments_type == static::COMMENTS_DISABLED) {
            return 'Disabled';
        } elseif ($this->comments_type == static::COMMENTS_PENDING_MODERATION) {
            return 'Pending Moderation';
        } elseif ($this->comments_type == static::COMMENTS_AUTOMATICALLY_PUBLISHED) {
            return 'Auto Published';
        }
    }

    // public function scopeHandle($query, $handle)
    // {
    //     return $query->where('handle', '=', $handle);
    // }

    // public function items()
    // {
    //     return $this->hasMany('App\Models\MenuItem', 'menu_id');
    // }
}
