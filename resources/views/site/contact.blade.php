<!DOCTYPE html>
<html lang="en">
<head>
    @include('site.web-header')
</head>
<body id="page-top" class="index">
    @include('site.nav')

    <!-- Header -->
    <header class="inner-header">
        <div class="container">
            <div class="intro-text">
                <div class="intro-heading m-b-n">Get In Touch</div>
                <h3>We'll get back to you ASAP</h3>
            </div>
        </div>
    </header>

    <section class="bs-pricing-four">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
<!--
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
                                </div>
                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input type="email" class="form-control" id="email" placeholder="Enter email" required="required" />
                                </div>
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <select id="subject" name="subject" class="form-control" required="required">
                                        <option value="na" selected="">Choose One:</option>
                                        <option value="service">General Customer Service</option>
                                        <option value="suggestions">Suggestions</option>
                                        <option value="product">Product Support</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Message</label>
                                    <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info btn-block" id="btnContactUs">
                                Send Message</button>
                            </div>
                        </div>
                    </form>
            -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3279.5844280298465!2d-86.60661019999999!3d34.715660299999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88626c90a3608c53%3A0x9310206a0011b201!2sHuntsvilleWest!5e0!3m2!1sen!2sus!4v1489949199111" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-4">
                    <form>
                        <address>
                            <strong>(Fastest) Chat with us</strong><br>
                            <a href="#" onclick="Intercom('showNewMessage', 'Hey! I had a question about ... ')">Click to chat</a>
                        </address>
                        <address>
                            <strong>Write Us</strong><br>
                            <a href="mailto:support@commentsold.com">support@commentsold.com</a>
                            </address>
                            <address>
                            <strong>Visit Us</strong><br>
                            3001 9th Ave Sw<br>
                            Huntsville, AL, 35805<br><br>
                        </address>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <div class="text-right"><img src="images/left-pointer.png" alt="" class="img-responsive left-arrow"></div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3279.596794025544!2d-86.60881388421917!3d34.715348480430755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88626c90a3608c53%3A0x9310206a0011b201!2sHuntsvilleWest!5e0!3m2!1sen!2sus!4v1489949098919" width="100%" height="317" frameborder="0" style="border:0" allowfullscreen></iframe>

    @include('site.footer')
</body>
</html>
