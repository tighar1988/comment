<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Waitlist\ShoppingWaitlist;

class Waitlist extends Drop
{
    protected $waitlist;

    public function __construct(ShoppingWaitlist $waitlist)
    {
        $this->waitlist = $waitlist;
    }

    public function item_count()
    {
        return count($this->waitlist->items);
    }

    public function items()
    {
        $items = [];
        foreach ($this->waitlist->items as $item) {
            $items[] = new WaitlistItem($item);
        }

        return $items;
    }
}
