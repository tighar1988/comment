<td>#{{ $order->id }}</td>
<td><strong>{{ count($order->orderProducts) }}</strong></td>
<td><a href="/admin/customers/{{ $order->customer->id }}">{{ $order->customer->name }}</a></td>
<td>{{ style_date($order->created_at) }}</td>
<td>
    <strong>Subtotal:</strong> ${{ amount($order->subtotal) }} <br/>
    <strong>Shipping:</strong> ${{ amount($order->ship_charged) }} <br/>
    <strong>Tax:</strong> ${{ amount($order->tax_total) }} <br/>
    @if ($order->coupon_discount)
        <strong>Coupon:</strong> -${{ amount($order->coupon_discount) }}<br/>
    @endif
    @if ($order->apply_balance)
        <strong>Account Credit:</strong> -${{ amount($order->apply_balance) }}<br/>
    @endif
    <strong>Total:</strong> <strong>${{ amount($order->total) }}</strong>
</td>
