<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mails\ShopMailer;
use App\Models\Order;
use App\Models\User;
use App\Models\PayPalComissionCharge;
use Exception;
use App\Checkout\Fees;
use App\Jobs\Traits\ReconnectShopDB;
use App\Models\StripeCharges;
use Carbon\Carbon;

class SubscriptionPlanCharge implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    /**
     * The shop database name.
     *
     * @var string
     */
    protected $shop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop)
    {
        $this->shop = $shop;
    }

    /**
     * Collect PayPal Fees job.
     *
     * @return void
     */
    public function handle()
    {
        $shopOwner = User::getByShopName($this->shop);

        if (! $shopOwner->hasSubscriptionPlan() || $shopOwner->monthlyFeeWaived()) {
            // don't charge
            return;
        }

        if (! $shopOwner->trialExpired()) {
            if ($shopOwner->trialDaysLeft() == 3) {
                // send email to notify shop owner
                // (new ShopMailer)->sendTrialExpiresEmail($shopOwner);
            }

            // don't charge
            return;
        }

        // the trial expired, try to charge the shop owner
        $this->reconnectDB($this->shop);

        $lastCharge = StripeCharges::lastPaidSubscription();
        if (is_null($lastCharge) || $this->shouldChargeAgain($lastCharge)) {
            $this->charge($shopOwner);
        }
    }

    public function charge($shopOwner)
    {
        try {
            $amount = $shopOwner->monthlyFee();

            if ($amount == 0) {
                log_info("[subscription-plan] Can't charge $0 monthly fee. Exiting..");
                return;
            }

            // charge the shop owner
            log_info("[subscription-plan] Trying to charge monthly fee \${$amount}.");
            $charge = $shopOwner->charge(dollars_to_cents($amount), [
                'statement_descriptor' => 'CommentSold Monthly'
            ]);

            shop_setting_set('subscription-plan:last-collection-time', time());
            shop_setting_set('subscription-plan:card-declined', false);

            StripeCharges::logSubscription($amount, $charge, $shopOwner, 'paid');
            // (new ShopMailer)->sendSubscriptionPlanEmail($shopOwner, $amount);

        } catch (Exception $e) {

            if ($e instanceof \Stripe\Error\Card && $e->getStripeCode() == 'card_declined') {
                shop_setting_set('subscription-plan:card-declined', true);

                StripeCharges::logSubscription($amount, ($charge ?? null), $shopOwner, 'failed', ['error' => $e->getMessage()]);
                // (new ShopMailer)->sendSubscriptionPlanFailedEmail($shopOwner, $amount);
            }

            ShopMailer::notifyAdmin($e);
            log_error("[subscription-plan] [exception] {$e}");
        }
    }

    public function shouldChargeAgain($lastCharge)
    {
        // check if 30 days passed since last charge
        $now = Carbon::now();
        $daysPassed = $now->diffInDays($lastCharge->created_at);

        if ($daysPassed >= 30) {
            return true;
        }

        return false;
    }
}
