<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;
use App\OnlineStore\Vendor\Liquid\Tag\TagPaginate;

class Customer extends Drop
{
    protected $customer;

    protected $waitlistCount;

    public function __construct($customer)
    {
        $this->customer = $customer;
        $this->waitlistCount = $customer->newWaitlistCount();
    }

    /**
     * Returns true if the customer accepts marketing, returns false if the customer does not.
     */
    public function accepts_marketing()
    {
        return (bool) $this->customer->accepts_marketing;
    }

    /**
     * Returns an array of all addresses associated with a customer. See customer_address for a full list of available attributes.
     *
     * Input
     * {% for address in customer.addresses %}
     *   {{ address.street }}
     * {% endfor %}
     *
     * Output
     * 126 York St, Suite 200 (Shopify Office)
     * 123 Fake St
     * 53 Featherston Lane
     */
    public function addresses()
    {
        // todo: add support for multiple addresses; we support only one right now
        return [
            new CustomerAddress($this->customer),
        ];
    }

    /**
     * Returns the number of addresses associated with a customer.
     */
    public function addresses_count()
    {
        // todo: add support for multiple addresses; we support only one right now
        return 1;
    }

    /**
     * Returns the default customer_address.
     */
    public function default_address()
    {
        return new CustomerAddress($this->customer);
    }

    public function address()
    {
        return new CustomerAddress($this->customer);
    }

    /**
     * Returns the email address of the customer.
     */
    public function email()
    {
        return $this->customer->email ?? null;
    }

    /**
     * Returns the first name of the customer.
     */
    public function first_name()
    {
        return $this->customer->firstName();
    }

    /**
     * Returns true if the email associated with an order is also tied to a customer account.
     * Returns false if it is not. Helpful in email templates. In the theme, that will always be true.
     */
    public function has_account()
    {
        return true;
    }

    /**
     * Returns the id of the customer.
     */
    public function id()
    {
        return $this->customer->id ?? null;
    }

    /**
     * Returns the last name of the customer.
     */
    public function last_name()
    {
        return $this->customer->lastName();
    }

    /**
     * Returns the last order placed by the customer, not including test orders.
     *
     * Input
     * Your last order was placed on: {{ customer.last_order.created_at | date: "%B %d, %Y %I:%M%p" }}
     *
     * Output
     * Your last order was placed on: April 25, 2014 01:49PM
     */
    public function last_order()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the full name of the customer.
     */
    public function name()
    {
        return $this->customer->name ?? null;
    }

    /**
     * Returns an array of all orders placed by the customer.
     *
     * Input
     * {% for order in customer.orders %}
     *   {{ order.id }}
     * {% endfor %}
     *
     * Output
     * #1088
     * #1089
     * #1090
     */
    public function orders()
    {
        // todo: make limit take intou account liquid limit, ex: "{% paginate customer.orders by 20 %}"
        $limit = 20;
        if (is_numeric(CurrentView::$numberItems)) {
            $limit = CurrentView::$numberItems;
        }

        $offset = 0;
        if (is_numeric(CurrentView::$currentOffset)) {
            $offset = CurrentView::$currentOffset;
        }

        $items = [];

        $orders = $this->customer->orders()
            ->offset($offset)
            ->limit($limit)
            ->get();

        foreach ($orders as $order) {
            $items[] = new Order($order);
        }

        TagPaginate::$dbCollectionSize = $this->orders_count();
        CurrentView::$totalItems = TagPaginate::$dbCollectionSize;

        return $items;
    }

    /**
     * Returns the total number of orders a customer has placed.
     */
    public function orders_count()
    {
        return $this->customer->ordersCount();
    }

    /**
     * Returns the list of tags associated with the customer.
     *
     * Input
     * {% for tag in customer.tags %}
     *   {{ tag }}
     * {% endfor %}
     *
     * Output
     * wholesale regular-customer VIP
     */
    public function tags()
    {
        // todo: add support for customer tags
        return [];
    }

    /**
     * Returns the total amount spent on all orders.
     */
    public function total_spent()
    {
        return $this->customer->totalSpent();
    }

    public function has_balance()
    {
        return $this->customer ? $this->customer->hasBalance() : null;
    }

    public function balance()
    {
        return $this->customer->balance ?? null;
    }

    public function instagram_connected()
    {
        return (bool) $this->customer->instagram_id ?? null;
    }

    public function instagram_username()
    {
        if (isset($this->customer->instagram_data['username'])) {
            return $this->customer->instagram_data['username'];
        }

        return null;
    }

    public function new_waitlist_count()
    {
        return $this->waitlistCount;
    }

    public function ordered_in_last_24hours()
    {
        return $this->customer ? $this->customer->orderedInLast24hours() : null;
    }

    public function free_24_shipping_expire_time()
    {
        return $this->customer ? $this->customer->freeShippingExpireTime() : null;
    }

    public function saw_onboarding_modal()
    {
        return $this->customer ? $this->customer->sawOnboardingPopup() : null;
    }

    public function messenger_user_ref()
    {
        $customerId = $this->customer->id ?? null;

        return time() . rand(0, 100000) . '-' . $customerId;
    }

    public function wants_local_pickup()
    {
        return (bool) $this->customer->local_pickup ?? null;
    }

    public function cards()
    {
        $cards = [];

        foreach ($this->customer->cards() as $card) {
            $cards[] = new Card($card);
        }

        return $cards;
    }
}
