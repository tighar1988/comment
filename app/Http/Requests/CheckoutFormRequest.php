<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\TaxRate;

class CheckoutFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email'                   => 'required|email',
            'buyer_accepts_marketing' => 'required|in:0,1',
            'first_name'              => 'required|max:255',
            'last_name'               => 'required|max:255',
            'address1'                => 'required|max:255',
            'address2'                => 'max:255',
            'city'                    => 'required|max:255',
            'country_code'            => '',
            'state'                   => '',
            'region'                  => '',
            'zip'                     => 'required|max:100',
            'phone'                   => 'max:100',
        ];

        if ($this->has_countries == 1) {
            $rules['country_code'] = 'required|in:US,CA';

            if ($this->country_code == 'US' || empty($this->country_code)) {
                $rules['state'] = 'required|in:AL,AK,AS,AZ,AR,AA,AE,AP,CA,CO,CT,DE,DC,FL,GA,GU,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,MP,NS,OH,OK,OR,PW,PA,PR,RI,SC,SD,TN,TX,UT,VT,VI,VA,WA,WV,WI,WY';

            } elseif ($this->country_code == 'CA') {
                $rules['region'] = 'required|in:AB,BC,MB,NB,NL,NT,NS,NU,ON,PE,QC,SK,YT';
            }
        }

        return $rules;
    }
}
