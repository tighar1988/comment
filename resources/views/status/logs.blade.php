<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" / >
    <title>tail {{ $shop }} logs</title>
    <link rel="icon" href="/images/status-favicon.ico">
    <link href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ cdn('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body {
            padding-top: 4em;
            background-color: #2F3238;
        }

        .no-topbar {
            padding-top: 10px;
        }

        .navbar {
            background-color: #26292E;
            border: 0;
        }

        .form-control {
            border: 0;
            color: #7F8289;
            background-color: #2F3238;
        }

        .log {
            color: #7F8289;
            font-size: .85em;
            background: inherit;
            border: 0;
            padding: 0;
        }

        .line {
            color: lightgrey;
            padding-left: 20px;
        }
    </style>
</head>
<body>
    <nav class="topbar navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <span class="navbar-brand" href="#">tail</span>
            {{--
            <form class="navbar-form navbar-right" role="search" onkeypress="return event.keyCode != 13;">
                <div class="form-group">
                    <input type="text" class="form-control query" placeholder="Filter" tabindex="1">
                </div>
            </form>
            --}}
        </div>
    </nav>

    <pre class="log" id="app">
        <div class="line" v-show="logs.length == 0">There are no logs.</div>
        <div class="line" v-for="log in logs">[@{{ log.created_at }}] @{{ log.level }}: @{{ log.message }}</div>
    </pre>

    <script src="{{ cdn('/js/jquery.min.js') }}"></script>
    <script src="{{ cdn('/js/vue.min.js') }}"></script>
    <script src="{{ cdn('/js/tinycon.min.js') }}"></script>
    <script type="text/javascript">

        var logs = [];
        var count = 0;
        var offset = 0;

        var app = new Vue({
            el: '#app',
            data: {
                logs: logs
            }
        });

        $(document).ready(function() {
            Tinycon.setOptions({
                width: 7,
                height: 9,
                font: '10px arial',
                color: '#ff00ff',
                background: '#0191C8',
                fallback: true
            });

            $(window).focus(function() {
                count = 0;
                Tinycon.setBubble(count);
            });

            $(window).mousemove(function() {
                count = 0;
                Tinycon.setBubble(count);
            });

        });

        (function worker() {
            $.getJSON('/app-status/api/logs?shop={{ $shop }}&offset='+offset, function(data) {
                if (data.length > 0) {
                    offset = data[0].id;
                    for (var i = data.length-1; i >= 0; i--) {

                        if (logs.count > 2000) {
                            logs.shift();
                        }
                        logs.push(data[i]);

                        if (count < 100) {
                            count++;
                            Tinycon.setBubble(count);
                        }
                    }
                }

                setTimeout(worker, 1000);
            });
        })();

    </script>
</body>
