<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderProduct extends Model
{
    /**
     * The product was added from a Facebook comment.
     *
     * @var integer
     */
    const SOURCE_FACEBOOK_COMMENT = 1;

    /**
     * The product was added from the Facebook Messenger.
     *
     * @var integer
     */
    const SOURCE_FACEBOOK_MESSENGER = 2;

    /**
     * The product was added by the admin.
     *
     * @var integer
     */
    const SOURCE_ADMIN = 3;

    /**
     * The product was added from an Instagram comment.
     *
     * @var integer
     */
    const SOURCE_INSTAGRAM_COMMENT = 4;

    /**
     * The product was added from the online store.
     *
     * @var integer
     */
    const SOURCE_STORE = 5;

    /**
     * The product was added from the mobile app.
     *
     * @var integer
     */
    const SOURCE_MOBILE_APP = 6;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders_products';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'inventory_id',
        'price',
        'cost',
        'prod_name',
        'color',
        'size',
        'comment_id',
        'returned_date',
        'refund_issued',
        'order_source',
        'product_style',
        'product_brand',
        'product_brand_style',
        'product_filename',
        'product_description',
        'weight',
        'shopify_variant_id',
    ];

    /**
     * Get refund amount only for one product in the order.
     */
    public function getRefundAmount($order)
    {
        if ($this->price == $order->subtotal) {
            // we only have one product in the order
            $refund = $order->subtotal + $order->tax_total - $order->coupon_discount;

        } else {
            // we have multiple producs in the order
            $percentage = $order->subtotal == 0 ? 0 : ($this->price / $order->subtotal);
            $refund = $this->price + ($percentage * $order->tax_total) - ($percentage * $order->coupon_discount);
        }

        if ($refund < 0) {
            $refund = 0;
        }

        return floatval(round($refund, 2));
    }

    public function sourceIsFacebookComment()
    {
        return $this->order_source == static::SOURCE_FACEBOOK_COMMENT;
    }

    public function sourceIsInstagramComment()
    {
        return $this->order_source == static::SOURCE_INSTAGRAM_COMMENT;
    }

    public function sourceIsOnlineStore()
    {
        return $this->order_source == static::SOURCE_STORE;
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function variant()
    {
        return $this->hasOne('App\Models\Inventory', 'id', 'inventory_id');
    }

    public function facebookComment()
    {
        return $this->hasOne('App\Models\Comment', 'id', 'comment_id');
    }

    public function instagramComment()
    {
        return $this->hasOne('App\Models\InstagramComment', 'id', 'comment_id');
    }

    public static function store($orderId, $item)
    {
        static::create([
            'order_id'            => $orderId,
            'product_id'          => $item->product_id,
            'inventory_id'        => $item->inventory_id,
            'price'               => $item->price,
            'cost'                => $item->cost,
            'prod_name'           => $item->name,
            'color'               => $item->color,
            'size'                => $item->size,
            'comment_id'          => $item->comment_id,
            'returned_date'       => null,
            'refund_issued'       => null,
            'order_source'        => $item->order_source,
            'product_style'       => $item->product_style,
            'product_brand'       => $item->product_brand,
            'product_brand_style' => $item->product_brand_style,
            'product_filename'    => $item->image,
            'product_description' => $item->product_description,
            'weight'              => $item->weight,
            'shopify_variant_id'  => $item->shopify_inventory_id,
        ]);
    }

    public static function reportVendorReturns($start, $end)
    {
        return static::select(DB::raw('product_brand as brand, COUNT(1) as count, COUNT(returned_date) as returns'))
            ->join('orders', 'orders.id', '=', 'orders_products.order_id')
            ->whereBetween('orders.payment_date', [$start, $end])
            ->groupBy('product_brand')
            ->orderBy('count', 'ASC')
            ->get();
    }

    public static function reportProducts($start, $end)
    {
        return static::join('orders', 'orders.id', '=', 'orders_products.order_id')
            ->whereBetween('orders.payment_date', [$start, $end])
            ->get();
    }

    public function sizeColor()
    {
        $options = [];

        if (! empty($this->size)) {
            $options[] = $this->size;
        }
        if (! empty($this->color)) {
            $options[] = $this->color;
        }

        return implode('/', $options);
    }

    public function shopifyBarcode()
    {
        if (! empty($this->shopify_variant_id)) {
            return $this->variant->shopify_barcode ?? null;
        }

        if (shop_id() == 'glamourfarms') {
            return $this->variant->shopify_barcode ?? null;
        }

        return null;
    }

    public function sortKey()
    {
        $location = $this->variant->location ?? '';
        $style = $this->product_style ?? '';
        $color = $this->color ?? '';
        $size = $this->size ?? '';
        return strtolower($location . $style . $color . $size);
    }
}
