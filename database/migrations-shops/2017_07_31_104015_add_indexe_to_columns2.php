<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexeToColumns2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->index(['shopify_coupon_id']);
        });

        Schema::table('waitlist', function (Blueprint $table) {
            $table->index(['user_id', 'created_at']);
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->index(['scheduled_time', 'fb_post_id']);
        });

        Schema::table('images', function (Blueprint $table) {
            $table->index(['is_main', 'product_id']);
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->index(['prod_id', 'scheduled_time']);
        });

        Schema::table('inventory', function (Blueprint $table) {
            $table->index(['product_id', 'shopify_inventory_id']);
        });

        Schema::table('cards', function (Blueprint $table) {
            $table->index(['user_id', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
