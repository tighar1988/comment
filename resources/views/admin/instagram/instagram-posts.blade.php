@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Instagram Posts</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Instagram Posts</li>
    </ol>
</section>

@if ($connected === false)
    <div class="row">
        <div class="top-alert alert alert-danger" style="display: table;">You Instagram access token has expired. Please reconnect your Instagram account in <a href="/admin/setup/instagram">Setup</a></div>
    </div>
@endif

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">
                    <div class="table-responsive">
                        <table id="instagram-posts-table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Image Posted</th>
                                    <th>Product (Quantity)</th>
                                    <th>Link to Post</th>
                                    <th>Posted</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    @if (isset($post['id']))
                                        <tr>
                                            <td><img width="150px" height="150px" src="{{ $post['images']['thumbnail']['url'] ?? null }}"></td>
                                            <td>
                                                @if ($connected->contains('instagram_id', $post['id']))
                                                    <?php $p = $connected->where('instagram_id', $post['id'])->first();  ?>
                                                    <span class="label label-success">Linked</span>
                                                    #{{ $p->product->style ?? null }} (Quantity: {{ $p->product->inventoryQuantity->quantity ?? 0 }})

                                                @else
                                                    <span class="label label-warning">Not Linked</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ $post['link'] ?? null }}" target="_blank">View</a>
                                            </td>
                                            <td>{{ apply_timezone($post['created_time'] ?? null) }}</td>
                                            <td class="text-center">
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                        <i class="fa fa-ellipsis-h fa-lg"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                                        <li><a href="#" data-postId="{{ $post['id'] }}" onclick="return openProductsModal(this);" class="link">Link to Product</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>

<div id="link-products-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Search for products to link</h4>
            </div>
            <div class="modal-body no-search-padding">

                <div class="table-responsive">
                    <table id="products-search" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <form class="form-horizontal" method="POST" action="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var selectedPostId = null;

    var productsTableLoaded = false;

    function openProductsModal(element) {
        selectedPostId = $(element).attr('data-postId');

        if (productsTableLoaded == false) {
            productsTableLoaded = true;
            $('#products-search').DataTable({
                "ordering": false,
                "paging": true,
                "lengthChange": false,
                "pageLength": 5,
                "serverSide": true,
                "order": [[ 0, "desc" ]],
                "info": false,
                "language": {
                    "searchPlaceholder": "SKU / Product Name",
                    "sEmptyTable": "No products found."
                },
                "ajax": "/admin/api/instagram-posts/products",
                columns: [
                    { data: 'style', 'searchable': true, render: function(data, type, row) {
                        return '<img width="100" src="' + row.thumb + '"/>';
                    }},
                    { data: null, 'searchable': false, render: function(data) {
                        return data.product_name + ' (#' + data.style + ')';
                    }},
                    { data: 'inventory_quantity', 'searchable': false},
                    { data: null, 'orderable': false, 'searchable': false, render: function(data) {
                        return '<form method="POST" onsubmit="return linkToProduct(this);" action="/admin/instagram-posts/link-to-product"><input type="hidden" name="post_id" class="instagram-post-id" value=""><input type="hidden" name="product_id" value="' + data.id + '"><button data-loading-text="Linking.." type="submit" class="btn btn-sm btn-danger">Link</button></form>';
                    }},
                ]
            });
        }

        $('#link-products-modal').modal();
        return false;
    }

    function linkToProduct(form) {
        $('.instagram-post-id', $(form)).val(selectedPostId);
        $('button', $(form)).button('loading');
        return true;
    }

    $(document).ready(function() {
        $('#instagram-posts-table').DataTable();
    });
</script>
@endsection

