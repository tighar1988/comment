@extends('layouts.admin')

@section('content')
<?php
    $shopifyEnabled = shopify_enabled();
    if (shop_id() == 'glamourfarms') {
        $shopifyEnabled = false;
    }
?>
<section class="content-header">
    <h1>Product ({{ $product->style }})</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/products"><i class="fa fa-th-large"></i> Products</a></li>
        <li><a href="/admin/products/edit/{{ $product->id }}">{{ $product->product_name }}</a></li>
        <li class="active">Images</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">

                            @if (count($product->images))
                                <div id="image-sorting">
                                    @foreach ($product->images as $image)
                                        <div class="image-wrapper">
                                            <a href="/admin/products/{{ $product->id }}/images/{{ $image->id }}/delete" onclick="return confirm_delete(this);" class="btn-remove-img btn btn-default btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            @if ($image->is_main)
                                                <img data-id="{{ $image->id }}" src="{{ product_thumb($image->filename) }}" class="main-image product-image" title="Main Image"/>
                                            @else
                                                <img data-id="{{ $image->id }}" src="{{ product_thumb($image->filename) }}" class="product-image" title="Click to make this the Main Image" />
                                            @endif
                                            <br/><br/>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                @if (! $shopifyEnabled)
                                    <div>This product has no images. Add image:</div>
                                @else
                                    This product has no images.
                                @endif
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            @if (! $shopifyEnabled || ($shopifyEnabled && empty($product->shopify_product_id)))
                            <div>
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Upload New Images</span>
                                    <input id="fileupload" type="file" name="files[]" multiple>
                                </span>
                                <br/>
                                <br/>
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                                <div id="files" class="files"></div>
                            </div>
                            @endif

                            @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                            <br/>
                            <a class="" href="/admin/products/{{ $product->id }}/variants">Done uploading images, continue to inventory</a>
                            @endif
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ cdn('/vendor/jQuery-File-Upload/css/jquery.fileupload.css') }}" type="text/css">
<style type="text/css">
    .image-wrapper {
        float: left;
        position: relative;
    }
    .btn-remove-img {
        z-index: 100;
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ cdn('/vendor/jQuery-File-Upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ cdn('/vendor/jQuery-File-Upload/js/jquery.iframe-transport.js') }}"></script>
<script type="text/javascript" src="{{ cdn('/vendor/jQuery-File-Upload/js/jquery.fileupload.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#fileupload').fileupload({
            url: '/admin/products/{{ $product->id }}/images',
            dataType: 'json',
            start: function (e) {
                $('.fileinput-button span').html('Uploading..');
                $('#fileupload').prop('disabled', true);
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo('#files');
                });

                window.location.reload();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                $('.fileinput-button span').html('Upload New Images');
                $('#fileupload').prop('disabled', false);

                var errors = data.jqXHR.responseJSON;
                $.each(errors, function (key, value) {
                    $('<p class="alert alert-danger"/>').text(value).appendTo('#files');
                });
            }
        });

        $('.product-image').on('click', function() {
            var imageId = $(this).attr('data-id');
            $.ajax({
                type:'post',
                url:'/admin/products/{{ $product->id }}/images/'+imageId+'/make-main',
                success: function(data) {
                    if (data.status == 1) {
                        $('.product-image').removeClass('main-image');
                        $('.product-image[data-id='+imageId+']').addClass('main-image');
                    }
                }
            });
        });

        $('#image-sorting').sortable({
            stop: function(e, ui) {
                var ids = [];
                $('.product-image').each(function() {
                    ids.push($(this).attr('data-id'));
                });

                $.ajax({
                    type: "POST",
                    url: '/admin/products/{{ $product->id }}/images/sort',
                    data: {
                        image_ids: ids
                    },
                    dataType: 'json',
                    success: function(data) {}
                });
            }
        });
    });
</script>
@endsection
