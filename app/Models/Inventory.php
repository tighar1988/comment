<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
use App\Models\InventoryLog;
use DB;
use App\Shopify\Shopify;

class Inventory extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventory';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'color',
        'size',
        'quantity',
        'price',
        'cost',
        'weight',
        'shopify_inventory_id',
        'shopify_barcode',
        'shopify_webhook_status',
        'sale_price',
        'sku',
    ];

    public function priceOrSalePrice()
    {
        if (! is_null($this->sale_price) && is_numeric($this->sale_price) && ($this->sale_price > 0) && ($this->sale_price < $this->price)) {
            return $this->sale_price;
        }

        return $this->price;
    }

    public function variantName()
    {
        $options = [];
        if (! empty($this->color)) {
            $options[] = $this->color;
        }

        if (! empty($this->size)) {
            $options[] = $this->size;
        }

        return implode('/', $options);
    }

    public function variantOptions()
    {
        $options = [];

        if (! empty($this->color)) {
            $options[] = $this->color;
        }

        if (! empty($this->size)) {
            $options[] = $this->size;
        }

        return $options;
    }

    public static function newItem($options)
    {
        $item = static::create($options);

        $quantity = $options['quantity'] ?? 0;
        InventoryLog::log($item->id, 0, $quantity, 'Admin added new inventory item.');

        return $item;
    }

    public function timestampShopifyApiCall()
    {
        static::where('id', '=', $this->id)->update(['shopify_webhook_status' => time()]);
    }

    public function incrementQuantity($message = 'Increment Quantity', $quantity = 1, $checkShopify = true)
    {
        $quantity = intval(abs($quantity));

        if ($checkShopify && shopify_enabled() && shop_setting('shopify.access_token') && ! is_null($this->shopify_inventory_id)) {
            (new Shopify)->updateVariantQuantity($this, $quantity);
        }

        static::whereId($this->id)->increment('quantity', $quantity);

        InventoryLog::log($this->id, $this->quantity, $this->quantity + $quantity, $message);
        $this->quantity += $quantity;
    }

    public function decrementQuantity($message = 'Decrement Quantity', $quantity = 1, $checkShopify = true)
    {
        $quantity = intval(abs($quantity));

        if ($checkShopify && shopify_enabled() && shop_setting('shopify.access_token') && ! is_null($this->shopify_inventory_id)) {
            (new Shopify)->updateVariantQuantity($this, (-1) * $quantity);
        }

        static::whereId($this->id)->where('quantity', '>', 0)->update([
            'quantity' => DB::raw("GREATEST(quantity - {$quantity}, 0)"),
        ]);

        $newQuantity = max($this->quantity - $quantity, 0);
        InventoryLog::log($this->id, $this->quantity, $newQuantity, $message);
        $this->quantity = $newQuantity;
    }

    public function setWeightInOz($weight, $unit)
    {
        $this->weight = static::getWeightInOz($weight, $unit);
    }

    public static function getWeightInOz($weight, $unit)
    {
        if ($unit == 'oz') {
            return round($weight);

        } elseif ($unit == 'g') {
            return round($weight / 28.3495); // 1 oz = 28.3495 g

        } elseif ($unit == 'kg') {
            return round($weight / 0.0283495); // 1 oz = 0.0283495 kg

        } elseif ($unit == 'lb') {
            return round($weight / 0.0625); // 1 oz = 0.0625 lb

        } else {
            return 0;
        }
    }

    public function latestQuantity()
    {
        $quantity = static::select('quantity')->whereId($this->id)->first();
        return $quantity->quantity ?? 0;
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    public function waitlistCount()
    {
        return $this->hasOne('App\Models\Waitlist', 'inventory_id', 'id')
            ->selectRaw('inventory_id, count(*) as count')
            ->groupBy('inventory_id');
    }

    public function cartCount()
    {
        return $this->hasOne('App\Models\Cart', 'inventory_id', 'id')
            ->selectRaw('inventory_id, count(*) as count')
            ->groupBy('inventory_id');
    }


    public function soldCount()
    {
        return $this->hasOne('App\Models\OrderProduct', 'inventory_id', 'id')
            ->selectRaw('inventory_id, count(*) as count')
            ->groupBy('inventory_id');
    }

    public function waitlist()
    {
        return $this->hasMany('App\Models\Waitlist', 'inventory_id', 'id')->orderBy('id', 'ASC');
    }

    public function scopeThatExists($query)
    {
        return $query->where('quantity', '>', 0);
    }

    /**
     * Find all items that are deduced from inventory but not gone from the
     * store - aka open invoices and paid but not shipped/packaged.
     *
     * This will give the shop owners an idea of what should be on the shelf
     */
    public function onShelfCount()
    {
        return $this->hasOne('App\Models\OrderProduct', 'inventory_id', 'id')
            ->selectRaw('inventory_id, count(*) as count')
            ->join('orders', 'orders_products.order_id', '=', 'orders.id')
            ->where('orders.order_status', '=', Order::STATUS_PAID)
            ->orWhere('orders.order_status', '=', Order::STATUS_PROCESSING)
            ->orWhere('orders.order_status', '=', Order::STATUS_PRINTED)
            ->groupBy('inventory_id');
    }

    public static function searchProducts()
    {
        return static::selectRaw('inventory.color, inventory.size, inventory.quantity, products.style, products.product_name, inventory.id')->join('products', 'inventory.product_id', '=', 'products.id');
    }
}
