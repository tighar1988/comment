#!/bin/bash

DOTOKEN=`cat do_token`

NODES=3

for i in `seq $NODES`; do docker-machine create --driver digitalocean \
--digitalocean-image  ubuntu-16-04-x64 \
--digitalocean-private-networking \
--digitalocean-size "4gb" \
--digitalocean-backups \
--digitalocean-access-token $DOTOKEN cs-sql-$i; done
