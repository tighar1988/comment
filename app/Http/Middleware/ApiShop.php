<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use App\Models\ShopUser;
use App\Exceptions\NoShopException;
use Exception;

class ApiShop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $shop = $request->route('shop');

        set_shop_database($shop);
        DB::setDefaultConnection('shop');

        try {
            // check that the shop exists
            DB::connection('shop')->getPdo();
        } catch (Exception $e) {
            throw new NoShopException;
        }

        // set the default auth eloquent model
        Config::set('auth.providers.users.model', ShopUser::class);

        return $next($request);
    }
}
