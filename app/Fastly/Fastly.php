<?php

namespace App\Fastly;

use Exception;
use Log;

class Fastly {
	
	private $apiKey = env('FASTLY_API_KEY', ''); 

	/* The API key will be the same for both api-cdn and cdn.commentsold.com but the ServiceID will be different */
	private $serviceID = env('FASTLY_SERVICE_ID', ''); 

	function purge($key) {
		$result = $this->sendIM($key);
		return true;
	}

	function purgeSurrogate($key) {
		$header = "POST /service/{$this->serviceID}/purge/{$key} HTTP/1.0\r\n";
		$header .= "Fastly-Key: {$this->apiKey}\r\n";
		$header .= "Accept: application/json\r\n";
		$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,"https://api.fastly.com");
		curl_setopt($ch,CURLOPT_TIMEOUT,50);
		curl_setopt($ch,CURLOPT_HEADER,false);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_CUSTOMREQUEST,$header); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		$data=curl_exec($ch);
		$error = json_decode($data);
		//Log::notice("[Fastly] PURGE response for $key is : " . print_r($data, true));
	}

	/* pre-load so it gets loaded back into cache after we PURGE 
		XXX - we will want this in a seperate thread in the future because of the potential execution delay*/
	function preLoadProduct($id) {
		$file = file_get_contents("https://cdn.shopdiscountdivas.com/account/api/getProductDetails?prod_id={$id}");
	}

	function preLoadFeed($category=1) {
		$get = file_get_contents("https://cdn.shopdiscountdivas.com/account/api/getFeed?category={$category}");
	}
	
	function sendIM($key) {
		$ch = curl_init("http://cdn.shopdiscountdivas.com/account/api/$key");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PURGE");
		$data=curl_exec($ch);
		$error=json_decode($data);
		//Log::notice("[API][FASTLY] Purging key $key");

		$ch = curl_init("https://cdn.shopdiscountdivas.com/account/api/$key");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PURGE");
		$data=curl_exec($ch);
		$error=json_decode($data);
		//Log::notice("[API][FASTLY] Purging key $key (https)");
		return($data);

	}
	
}





