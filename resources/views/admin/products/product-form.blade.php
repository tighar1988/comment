@extends('layouts.admin')

@section('content')
<?php
    $isEdit = isset($product) ? true : false;
    $storeEnabled = shop_setting('store.enabled');
    $isGiftCard = (request('type') == 'giftcard' || ($isEdit && $product->isGiftCard())) ? true : false;
?>

<section class="content-header">
    <h1>
        @if ($isEdit) Editing Product {{ $product->style }} @else Add New Products @endif
        @if ($isGiftCard) (Gift Card) @endif
        @if ($isEdit && $isGiftCard)
            <a class="" style="font-size: 15px;" target="_blank" href="/admin/giftcards/{{ $product->id }}/preview">Preview</a>
        @endif
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/products"><i class="fa fa-th-large"></i> Products</a></li>
        <li class="active">@if ($isEdit) Edit Product @else Add New Products @endif</li>
    </ol>
@if ($isEdit)
<br />
<a class="pull-left" href="/admin/products/{{ $product->id }}/variants">Go to product inventory</a>
@endif
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        @if ($isGiftCard) <input type="hidden" name="product_type" value="giftcard"> @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('style')">
                                    <label>Product SKU / Style # @if (isset($sku)) - Suggesting {{ $sku }} @endif</label>
                                    <input type="text" class="form-control" name="style" value="{{ old('style', $product->style ?? '') }}">
                                    @error('style')
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @hasError('product_name')">
                                    <label>Product Name</label>
                                    <input type="text" class="form-control" name="product_name" value="{{ old('product_name', $product->product_name ?? '') }}">
                                    @error('product_name')
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('brand')">
                                    <label>Brand</label>
                                    <input type="text" class="form-control" name="brand" value="{{ old('brand', $product->brand ?? '') }}">
                                    @error('brand')
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @hasError('brand_style')">
                                    <label>Brand Style #</label>
                                    <input type="text" class="form-control" name="brand_style" value="{{ old('brand_style', $product->brand_style ?? '') }}">
                                    @error('brand_style')
                                </div>
                            </div>
                        </div>

                        @if(! $isEdit)
                            @if (! $isGiftCard)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group @hasError('size')">
                                        <label>Size</label>
                                        <select class="form-control select2-box product-option" name="size[]" multiple="multiple" data-placeholder="Select Sizes" style="width: 100%;">
                                            @foreach (old('size', []) as $option)
                                                <option selected="selected">{{ $option }}</option>
                                            @endforeach
                                            @foreach ($previousSizes as $size)
                                                @if (! empty($size) && ! in_array($size, old('size', [])))
                                                    <option>{{ $size }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @error('size')
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group @hasError('color')">
                                        <label>Colors</label>
                                        <select class="form-control select2-box product-option" name="color[]" multiple="multiple" data-placeholder="Select Colors" style="width: 100%;">
                                            @foreach (old('color', []) as $option)
                                                <option selected="selected">{{ $option }}</option>
                                            @endforeach
                                            @foreach ($previousColors as $color)
                                                @if (! empty($color) && ! in_array($color, old('color', [])))
                                                    <option>{{ $color }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @error('color')
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group @hasError('cost')">
                                        <label>Cost</label>
                                        <input type="text" name="cost" value="{{ old('cost') }}" class="form-control">
                                        @error('cost')
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group @hasError('retail_price')">
                                        <label>Retail Price</label>
                                        <input type="text" name="retail_price" value="{{ old('retail_price') }}" class="form-control">
                                        @error('retail_price')
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('charge_taxes')">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="charge_taxes" value="0">
                                            <input type="checkbox" name="charge_taxes" value="1" @if(old('charge_taxes', $product->charge_taxes ?? true)) checked="checked" @endif> <strong>Charge taxes on this product</strong>
                                        </label>
                                        @error('charge_taxes')
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('received_product')">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="received_product" value="0">
                                            <input type="checkbox" name="received_product" value="1" @if(old('received_product', $product->received_product ?? true)) checked="checked" @endif> <strong>Received Product</strong> <small class="text-muted">If unchecked, orders that contain this product won't be changed to 'Processing' status.</small>
                                        </label>
                                        @error('received_product')
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('description')">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" rows="3">{{ old('description', $product->description ?? '') }}</textarea>
                                    @error('description')
                                </div>
                            </div>
                        </div>

                        @if ($storeEnabled)
                            <br/>
                            <fieldset>
                                <legend>Online Store</legend>

                                <div class="row">
                                    <div class="col-md-6 select2-collections">
                                        <div class="form-group @hasError('collections')">
                                            <label>Collections</label>
                                            <div class="pull-right"><a href="/admin/store/collections">View all collections?</a></div>
                                            <select class="form-control select2-box product-collections" name="collections[]" multiple="multiple" data-placeholder="Select collections" style="width: 100%;">
                                                <?php
                                                    $collections = \App\Models\Collection::all();
                                                    $collectionIds = old('collections', (isset($product) ? $product->collections->pluck('id')->toArray() : []));
                                                ?>
                                                @foreach ($collections as $collection)
                                                    <option @if(in_array($collection->id, $collectionIds)) selected="selected" @endif value="{{ $collection->id }}">{{ $collection->title }}</option>
                                                @endforeach
                                            </select>
                                            @error('collections')
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 select2-tags">
                                        <div class="form-group @hasError('tags')">
                                            <label>Tags</label>
                                            <div class="pull-right"><a href="/admin/store/tags">View all tags?</a></div>
                                            <select class="form-control select2-box product-tags" name="tags[]" multiple="multiple" data-placeholder="Select Tags" style="width: 100%;">
                                                <?php
                                                    $tagGroups = \App\Models\TagGroup::with('tags')->get();
                                                    $tagIds = old('tags', (isset($product) ? $product->tags->pluck('id')->toArray() : []));
                                                ?>
                                                @foreach ($tagGroups as $tagGroup)
                                                    <optgroup label="{{ $tagGroup->name }}">
                                                        @foreach ($tagGroup->tags as $tag)
                                                            <option @if(in_array($tag->id, $tagIds)) selected="selected" @endif value="{{ $tag->id }}">{{ $tag->name }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                            @error('tags')
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @hasError('store_description')">
                                            <label>Description</label>
                                            <textarea class="form-control" name="store_description" rows="3">{{ old('store_description', $product->store_description ?? '') }}</textarea>
                                            @error('store_description')
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group @hasError('publish_product')">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="hidden" name="publish_product" value="0">
                                                    <input type="checkbox" name="publish_product" value="1" @if(old('publish_product', $product->publish_product ?? true)) checked="checked" @endif> <strong>Publish this product</strong>
                                                </label>
                                                @error('publish_product')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        @endif

                        <button type="submit" data-loading-text="Saving.." class="btn btn-success pull-right">@if($isEdit) Update Product @else Add Product @endif</button>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            @if ($isEdit)
                <a class="pull-right" href="/admin/products/{{ $product->id }}/variants">Go to product inventory</a>
            @endif

        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
@if ($storeEnabled)
    <script src="{{ cdn('/ckeditor/ckeditor.js') }}"></script>
    <script>CKEDITOR.replace('store_description');</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {

        $('.product-option').select2({
            tags: true
        });

        @if ($storeEnabled)
            $('.product-collections').select2({tags: false});
            $('.product-tags').select2({tags: false});
        @endif

        // show dropdown only if focus comes from tab (keyboard)
        var $select = $('.select2-box').next('.select2').find('.select2-selection');
        $select.on('mousedown', function() {
            $(this).data('mousedown', true);
        }).on('blur', function() {
            $(this).data('mousedown', false);
        }).on('focus', function() {
            if (! $(this).data('mousedown')) {
                $(this).closest('.select2').prev('select').select2('open');
            }
        });

        // don't order the tags, append them at the end
        $('.select2-box').on('select2:select', function (evt) {
            var element = evt.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger('change');
            $(this).next('.select2').find('.select2-search input').focus();
        });
    });
</script>
@endsection
