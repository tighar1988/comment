<div style="font-family: Verdana; font-size: 12px;">

    Hello {{ $customerName }},
    <br/><hr/><br/>

    Your waitlisted item is now available. Thank you for your order.
    <br/><br/>

    @if ($customerStreetAddress)
        <strong>Shipping Address:</strong>
        <br/>

        {{ $customerStreetAddress }}<br/>

        @if ($customerApartment)
            {{ $customerApartment }}<br/>
        @endif

        {{ $customerCity }} {{ $customerState }} {{ $customerZip }}
        <br/><br/>

        If you want to change your address, please login at <a href="{{ $shopUrl }}">My Account</a> and update.
    @else
        At this time, we do not have your shipping address, please login at <a href="{{ $shopUrl }}">My Account</a> and update your shipping address.
    @endif

    <br/><br/>
    Please login at <a href="{{ $shopUrl }}">My Account</a> to update or pay for your order.
</div>
