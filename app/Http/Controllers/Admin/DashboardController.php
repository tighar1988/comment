<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\ShopUser;
use App\Models\Cart;
use Facebook;

class DashboardController extends Controller
{
    public function index()
    {
        $orders = Order::with('customer', 'orderProducts')->latest()->take(10)->get();
        $totalOrders = Order::count();
        $totalRevenue = Order::sum('payment_amt');
        $totalPotential = Cart::potentialRevenue();
        $totalCustomers = ShopUser::count();

        // wait for dashboard design to show active customers
        // $nrWithEmail = ShopUser::withEmailOnFile()->count();
        // todo: add back when adding messenger support
        // $nrWithMessenger = ShopUser::withFacebookMessenger()->count();

        return view('admin.dashboard.dashboard', compact('orders', 'totalOrders', 'totalRevenue', 'totalCustomers', 'totalPotential'));
    }

    public function pendingFacebookMembers()
    {
        // todo: show this on the dashboard page, or reports page; load it by ajax for speed
        $groups = [];

        foreach (fb_enabled_group_ids() as $groupId) {
            $groups[$key] = Facebook::groupMemberRequestCount($groupId);
        }

        return $groups;
    }
}
