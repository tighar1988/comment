<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class Theme extends Drop
{
    protected $shopId;

    public function __construct($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * Returns the theme's ID. This is useful for when you want to link a user directly to the theme's Customize theme page.
     *
     * Input
     * Visit your <a href="/admin/themes/{{ theme.id }}/settings">Customize theme page</a> to change your logo.
     *
     * Output
     * Visit your <a href="/admin/themes/8196497/settings">Customize theme page</a> to change your logo.
     */
    public function id()
    {
        return get_theme($this->shopId);
    }

    /**
     * Returns the name of the theme.
     */
    public function name()
    {
        return get_theme($this->shopId);
    }
}
