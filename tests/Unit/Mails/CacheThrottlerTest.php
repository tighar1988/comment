<?php

namespace Tests\Unit\Mails;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Mails\CacheThrottler;
use Illuminate\Contracts\Cache\Store;
use Mockery;
use Exception;

class CacheThrottlerTest extends TestCase
{
    protected function getThrottler()
    {
        $store = Mockery::mock(Store::class);
        $key = 'abc';
        $limit = 10;
        $time = 60;
        return new CacheThrottler($store, $key, $limit, $time);
    }

    /** @test */
    public function it_attempts()
    {
        $throttler = $this->getThrottler();
        $throttler->getStore()->shouldReceive('get')->once()->with('abc');
        $throttler->getStore()->shouldReceive('put')->once()->with('abc', 1, 60);
        $return = $throttler->attempt();
        $this->assertTrue($return);
    }

    /** @test */
    public function it_counts_hit()
    {
        $throttler = $this->getThrottler();
        $throttler->getStore()->shouldReceive('get')->once()->with('abc');
        $throttler->getStore()->shouldReceive('put')->once()->with('abc', 1, 60);
        $return = $throttler->hit();
        $this->assertInstanceOf(CacheThrottler::class, $return);
        $return = $throttler->count();
        $this->assertSame(1, $return);
    }

    /** @test */
    public function it_counts_clear()
    {
        $throttler = $this->getThrottler();
        $throttler->getStore()->shouldReceive('put')->once()->with('abc', 0, 60);
        $return = $throttler->clear();
        $this->assertInstanceOf(CacheThrottler::class, $return);
        $return = $throttler->count();
        $this->assertSame(0, $return);
    }

    /** @test */
    public function it_counts_check_true()
    {
        $throttler = $this->getThrottler();
        $throttler->getStore()->shouldReceive('get')->once()->with('abc');
        $return = $throttler->count();
        $this->assertSame(0, $return);
        $return = $throttler->check();
        $this->assertTrue($return);
    }

    /** @test */
    public function it_count_check_edge()
    {
        $throttler = $this->getThrottler();
        $throttler->getStore()->shouldReceive('get')->once()->with('abc')->andReturn(9);
        $return = $throttler->count();
        $this->assertSame(9, $return);
        $return = $throttler->check();
        $this->assertTrue($return);
    }

    /** @test */
    public function it_count_check_false()
    {
        $throttler = $this->getThrottler();
        $throttler->getStore()->shouldReceive('get')->once()->with('abc')->andReturn(10);
        $return = $throttler->count();
        $this->assertSame(10, $return);
        $return = $throttler->check();
        $this->assertFalse($return);
    }

    /** @test */
    public function it_is_countable()
    {
        $throttler = $this->getThrottler();
        $throttler->getStore()->shouldReceive('get')->once()->with('abc')->andReturn(42);
        $this->assertInstanceOf('Countable', $throttler);
        $this->assertCount(42, $throttler);
    }
}
