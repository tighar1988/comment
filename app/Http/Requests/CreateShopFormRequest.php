<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateShopFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // don't let the user create multiple shops
        if ($this->user()->shop_name) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_name' => 'required|min:3|max:30|alpha_dash|no_underscore|only_lowercase|unique:users,shop_name|regex:/^[[:alnum:]].*[[:alnum:]]$/',
        ];
    }

    public function messages()
    {
        return [
            'shop_name.regex' => 'The shop name must begin and end with an alphanumeric character.',
        ];
    }
}
