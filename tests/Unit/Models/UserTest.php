<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class UserTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(User::class, new User);
    }

    /** @test */
    public function it_checks_billing_plan_fee()
    {
        $user = new User;

        $user->billing_plan = null;
        $this->assertEquals(0, $user->monthlyFee());

        $user->billing_plan = 'starter';
        $this->assertEquals(49, $user->monthlyFee());

        $user->billing_plan = 'business';
        $this->assertEquals(149, $user->monthlyFee());

        $user->billing_plan = 'boutique_hub_starter';
        $this->assertEquals(24.99, $user->monthlyFee());

        $user->billing_plan = 'boutique_hub_business';
        $this->assertEquals(99, $user->monthlyFee());
    }
}
