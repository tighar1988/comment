@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Create Labels</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Create Labels</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p mass-buy-labels">
                    <button title="Check Prices" id="check-prices-btn" class="btn btn-success btn-sm">Calculate Prices</button>
                    <button title="Confirm & Buy Labels" id="confirm-buy-btn" class="btn btn-info btn-sm">Confirm & Buy Labels</button>
                    <div class="table-responsive">

<table class="table table-bordered">
    <thead>
        <th class="text-center"><input type="checkbox" id="toggle-orders"></th>
        <th class="status-column">Status</th>
        <th>Order ID</th>
        <th>Total Items</th>
        <th>Customer</th>
        <th>Order Date</th>
        <th>Details</th>
        <th>Parcel Weight (oz)</th>
        <th>Parcel Length (in)</th>
        <th>Parcel Width (in)</th>
        <th>Parcel Height (in)</th>
    </thead>
    <tbody>
        @if ($orders->isEmpty())
            <tr><td colspan="12">There are no 'Processing' orders without labels.</td></tr>
        @endif
        @foreach ($orders as $key => $order)
            <?php
                $isMulti = false;
                $mutliOrders = [];
                $totalWeight = 0;
                if ($order instanceof \Illuminate\Support\Collection) {
                    if (count($order) > 1) {
                        $isMulti = true;
                        $mutliOrders = $order;
                    } else {
                        $order = $order->first();
                    }
                }
            ?>
            @if ($isMulti)
                @foreach ($mutliOrders as $order)
                    <?php $totalWeight += $order->total_weight; ?>
                    <tr>
                        @if ($loop->first)
                            <?php $orderId = $order->id; ?>
                            <td rowspan="{{ count($mutliOrders) + 1 }}" class="text-center checkbox-column">
                                <input type="checkbox" data-multiOrderIds="{{ $mutliOrders->implode('id', ',') }}" data-isMulti="1" class="checkbox-row" name="" value="{{ $order->id }}">
                            </td>
                            <td rowspan="{{ count($mutliOrders) + 1 }}">
                                <div class="label-preloader"><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></div>
                                <div class="label-status"></div>
                                <div class="label-confirmed"></div>
                            </td>
                        @endif
                        @include('admin.orders.labels-order-columns')
                        <td>{{ $order->total_weight }}</td>
                        <td colspan="3"></td>
                    </tr>
                @endforeach
                <tr data-parcel="{{ $orderId }}">
                    <td colspan="5"></td>
                    <td><input value="{{ $totalWeight }}" type="text" class="parcel-weight form-control" placeholder="Weight (oz)" name="parcel_weight"></td>
                    <td><input value="12" type="text" class="parcel-length form-control" placeholder="Length (in)" name="parcel_length"></td>
                    <td><input value="10" type="text" class="parcel-width form-control" placeholder="Width (in)" name="parcel_width"></td>
                    <td><input value="1" type="text" class="parcel-height form-control" placeholder="Height (in)" name="parcel_height"></td>
                </tr>

            @else
                <tr>
                    <td class="text-center checkbox-column">
                        <input type="checkbox" class="checkbox-row" name="" value="{{ $order->id }}">
                    </td>
                    <td>
                        <div class="label-preloader"><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></div>
                        <div class="label-status"></div>
                        <div class="label-confirmed"></div>
                    </td>
                    @include('admin.orders.labels-order-columns')
                    <td><input type="text" class="parcel-weight form-control" value="{{ $order->total_weight }}" placeholder="Weight (oz)" name="parcel_weight"></td>
                    <td><input value="12" type="text" class="parcel-length form-control" placeholder="Length (in)" name="parcel_length"></td>
                    <td><input value="10" type="text" class="parcel-width form-control" placeholder="Width (in)" name="parcel_width"></td>
                    <td><input value="1" type="text" class="parcel-height form-control" placeholder="Height (in)" name="parcel_height"></td>
                </tr>
            @endif
        @endforeach
    </tbody>
</table>

                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('styles')
<style type="text/css">
    #check-prices-btn,
    #confirm-buy-btn {
        margin: 0px 0px 10px 10px;
    }
    .mass-buy-labels input[type="text"] {
        width: 120px;
    }
    .mass-buy-labels .status-column {
        min-width: 100px;
    }
    .mass-buy-labels .label-preloader {
        display: none;
        text-align: center;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        $('#toggle-orders').change(function() {
            if($(this).is(":checked")) {
                $('.checkbox-row').prop('checked', true)
            } else {
                $('.checkbox-row').prop('checked', false);
            }
        });

        $('#check-prices-btn').on('click', function() {

            $('.checkbox-row:checked').each(function() {
                var checkbox = this;

                if ($(checkbox).attr('data-processing') == true) {
                    return;
                }

                $(checkbox).attr('data-processing', true);

                var orderId = $(checkbox).val();
                var row = $(checkbox).closest('tr');
                $(row).find('.label-preloader').show();
                $(checkbox).attr('data-price', null);
                $(checkbox).attr('data-rateId', null);

                var isMulti, parcelRow;
                if ($(checkbox).attr('data-isMulti') == '1') {
                    isMulti = 1;
                    parcelRow = $('tr[data-parcel="'+orderId+'"]');
                } else {
                    isMulti = 0;
                    parcelRow = row;
                }

                $.ajax({
                    type:'post',
                    url:'/admin/orders/'+orderId+'/buy-label',
                    data: {
                        weight: $(parcelRow).find('.parcel-weight').val(),
                        length: $(parcelRow).find('.parcel-length').val(),
                        width: $(parcelRow).find('.parcel-width').val(),
                        height: $(parcelRow).find('.parcel-height').val(),
                        bypass_validation: 0,
                        is_multi: isMulti,
                        multi_order_ids: $(checkbox).attr('data-multiOrderIds')
                    },
                    success: function(response) {
                        $(row).find('.label-preloader').hide();
                        $(checkbox).attr('data-processing', false);

                        if (response.status == 1) {
                            if (response.rate.errors) {
                                var errors = '';
                                for (var i = 0; i < response.rate.errors.length; i++) {
                                    errors += '<strong style="color:red;">'+response.rate.errors[i]+'</strong>';
                                }
                                $(row).find('.label-status').html(errors);

                            } else {
                                var message = "Price: $" + response.rate.amount + " from " + response.rate.provider + " (" + response.rate.servicelevel_name + ")";
                                $(row).find('.label-status').html(message);

                                $(checkbox).attr('data-price', response.rate.amount);
                                $(checkbox).attr('data-rateId', response.rate.object_id);

                            }
                        } else {
                            $(row).find('.label-status').text('An unexpected error occured.');
                        }
                    }
                });
            });
        });

        // confirm for all selected checkboxes that have no errors
        $('#confirm-buy-btn').on('click', function() {
            var totalCost = 0;
            var nrLabels = 0;

            // calculate total cost
            $('.checkbox-row:checked').each(function() {
                if ($(this).attr('data-processing') == true) {
                    return;
                }
                if ($(this).attr('data-price') != null && $(this).attr('data-rateId') != null) {
                    totalCost += parseFloat($(this).attr('data-price'));
                    nrLabels++;
                }
            });

            if (nrLabels == 0) {
                alert('There are no selected labels with the price calculated. Please calculate the price before buying the labels.')
                return;
            }

            totalCost = parseFloat(Math.round(totalCost * 100) / 100).toFixed(2);
            if (confirm("Are you sure to buy "+nrLabels+" labels for a total of $" + totalCost + "?")) {
                $.ajax({
                    type:'post',
                    url:'/admin/orders/buy-prepaid-credit',
                    data: { rate_amount: totalCost },
                    success: function(response) {
                        $('.checkbox-row:checked').each(function() {
                            var checkbox = this;

                            if ($(checkbox).attr('data-processing') == true) {
                                return;
                            }

                            if ($(checkbox).attr('data-price') != null && $(checkbox).attr('data-rateId') != null) {

                                $(checkbox).attr('data-processing', true);
                                var orderId = $(checkbox).val();
                                var row = $(checkbox).closest('tr');
                                $(row).find('.label-preloader').show();

                                var isMulti, parcelRow;
                                if ($(checkbox).attr('data-isMulti') == '1') {
                                    isMulti = 1;
                                    parcelRow = $('tr[data-parcel="'+orderId+'"]');
                                } else {
                                    isMulti = 0;
                                    parcelRow = row;
                                }

                                $.ajax({
                                    type:'post',
                                    url:'/admin/orders/'+orderId+'/confirm-buy-label',
                                    data: {
                                        rate_id: $(checkbox).attr('data-rateId'),
                                        weight: $(parcelRow).find('.parcel-weight').val(),
                                        length: $(parcelRow).find('.parcel-length').val(),
                                        width: $(parcelRow).find('.parcel-width').val(),
                                        height: $(parcelRow).find('.parcel-height').val(),
                                        is_multi: isMulti,
                                        multi_order_ids: $(checkbox).attr('data-multiOrderIds')
                                    },
                                    success: function(response) {
                                        $(row).find('.label-preloader').hide();
                                        if (response.label.errors) {
                                            var errors = '';
                                            for (var i = 0; i < response.label.errors.length; i++) {
                                                errors += '<strong style="color:red;">'+response.label.errors[i]+'</strong>';
                                            }
                                            $(row).find('.label-confirmed').html(errors);
                                            $(checkbox).attr('data-processing', false);

                                        } else {
                                            if (response.status == 1) {

                                                $(parcelRow).find('.parcel-weight').prop('disabled', true);
                                                $(parcelRow).find('.parcel-length').prop('disabled', true);
                                                $(parcelRow).find('.parcel-width').prop('disabled', true);
                                                $(parcelRow).find('.parcel-height').prop('disabled', true);

                                                $(checkbox).remove();
                                                $(row).find('.checkbox-column').html('<i class="fa fa-check-circle-o" aria-hidden="true"></i>');

                                                var html = 'Label purchased! <br/>';
                                                if (typeof response.label != 'undefined') {
                                                    html += 'Tracking Number: ' + response.label.tracking_number + '<br/>';
                                                }
                                                html += 'Open Label: <a target="_blank" href="/admin/orders/' + orderId + '/print-label">Open</a><br/>';
                                                $(row).find('.label-confirmed').html(html);

                                            } else {
                                                $(row).find('.label-confirmed').html('An unexpected error occured.');
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }

        });
    });
</script>
@endsection
