<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class Metafields extends Drop
{
    protected $metafields;

    public function __construct($metafields)
    {
        $this->metafields = $metafields;
    }

    public function handle_key($key)
    {
        foreach ($this->metafields as $metafield) {
            if ($metafield->key == $key) {
                return new Metafield($metafield);
            }
        }

        return null;
    }
}
