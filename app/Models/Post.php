<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Helpers\RepostText;
use DB;

class Post extends Model
{
    /**
     * The order 'paid' status.
     *
     * @var integer
     */
    const POST_IMMEDIATELY = 1;

    /**
     * The order 'fulfilled' (or shipped) status.
     *
     * @var integer
     */
    const POST_SCHEDULE_LATER = 2;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prod_id',
        'fb_id',
        'created_time',
        'scan_time',
        'image',
        'scheduled_time',
        'supplement',
    ];

    public static function postImmediately($option)
    {
        return $option == static::POST_IMMEDIATELY;
    }

    public static function scheduleLater($option)
    {
        return $option == static::POST_SCHEDULE_LATER;
    }

    public function scheduledCount()
    {
        return $this->scheduled()->count();
    }

    public function scopeScheduled($query)
    {
        return $query->whereNotNull('scheduled_time');
    }

    public function scopeFbid($query, $postIds)
    {
        return $query->whereIn('fb_post_id', $postIds);
    }

    public function latestComment()
    {
        // return $this->hasMany('App\Models\Comment', 'post_id', 'id')
        //     ->selectRaw('post_id, MAX(stamped) AS max')
        //     ->groupBy('post_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'prod_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'post_id', 'id');
    }

    public function addVariantsLeft($product)
    {
        $supplement = $this->supplement;

        if (is_array($supplement)) {
            $supplement = json_encode($supplement);
        }

        if (is_null($supplement)) {
            $supplement = new \stdClass;
        } elseif (is_string($supplement)) {
            $supplement = json_decode($supplement);
        }

        $message = $supplement->message ?? '';

        $supplement->message = (new RepostText($product, $message))->getRepostMessage();
        $this->supplement = json_encode($supplement);
    }

    public function createdBy($authorId)
    {
        return trim($this->fb_author_id) == trim($authorId);
    }
}
