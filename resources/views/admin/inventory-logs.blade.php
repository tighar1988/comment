@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Inventory Logs</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Inventory Logs</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">

                        <div class="dataTables_filter">
                            <form class="form-inline text-left lr-m-15" method="GET" action="{{ form_action() }}">
                                <label>Debug inventory logs:
                                    <div class="input-group">
                                        <input type="search" required="" class="form-control input-sm" name="q" value="{{ request('q') }}" placeholder="Search for SKU">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default btn-sm" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                        </span>
                                    </div>
                                </label>
                            </form>
                        </div>

                        @if (! empty(request('q')))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table id="inventory-logs" class="table table-striped no-footer">
                                            <thead>
                                                <tr>
                                                    <th>Log ID</th>
                                                    <th>SKU</th>
                                                    <th>Color</th>
                                                    <th>Size</th>
                                                    <th>Retail Price</th>
                                                    <th>Start Inventory</th>
                                                    <th>Finish Inventory</th>
                                                    <th>Memo</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($logs) == 0)
                                                    <tr><td colspan="9">No results found.</td></tr>
                                                @endif
                                                @foreach ($logs as $log)
                                                    <tr>
                                                        {{-- todo: add link to specific inventory item --}}
                                                        <td>#{{ $log->id }}</td>
                                                        <td><a href="/admin/products/{{ $log->product_id }}/variants">{{ $log->sku }}</a></td>
                                                        <td>{{ $log->color }}</td>
                                                        <td>{{ $log->size }}</td>
                                                        <td>${{ amount($log->price) }}</td>
                                                        <td>{{ $log->from_inv }}</td>
                                                        <td>{{ $log->to_inv }}</td>
                                                        <td>{{ $log->movement }}</td>
                                                        <td>{{ style_date($log->created_at) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                           {{ pagination_links($logs, request('q')) }}
                        @endif

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection
