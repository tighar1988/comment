<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\ExtractTaxRates;
use Mockery;

class ExtractTaxRatesTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(ExtractTaxRates::class, new ExtractTaxRates);
    }
}
