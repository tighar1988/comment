<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ShippingCostFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_cost' => 'required|numeric|min:0|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'remind.required' => 'The shipping cost is required.',
            'remind.numeric'  => 'The shipping cost must be numeric.',
            'remind.min'      => 'The shipping cost must be at least 0.',
        ];
    }
}
