@if (count($waitlist->items))
    <br/>
    <section class="invoice-box">
        <div class="row">
            <div class="col-md-12 no-padding">
                <div class="product-list">
                    @foreach ($waitlist->items as $item)
                        <div class="product-box">
                            <div class="product-image">
                                <button class="btn-remove" onclick="removeFromWaitlist({{ $item->id }})"></button>
                                <img width="290px" height="290px" src="{{ product_image($item->image) }}" alt="{{ $item->name }}">
                            </div>
                            <div class="product-name">{{ $item->name }}</div>
                            <div class="product-price">${{ amount($item->price) }}</div>
                            <div class="product-name">Color: {{ $item->color }}</div>
                            <div class="product-name">Size: {{ $item->size }}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@else
    You have no products in your waitlist at the moment.<br/><br/><br/>
@endif
