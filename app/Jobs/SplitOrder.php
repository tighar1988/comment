<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Traits\ReconnectShopDB;
use App\Models\Order;
use App\Models\OrderProduct;

class SplitOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    protected $shop;

    protected $orderId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop, $orderId)
    {
        $this->shop = $shop;
        $this->orderId = $orderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->reconnectDB($this->shop);

        $order = Order::find($this->orderId);
        if (! $order) {
            log_info("[split-order] Could not find order #{$this->orderId}");
            return;
        }

        foreach ($order->orderProducts as $op) {
            // if order has only one product left, don't split it
            if (OrderProduct::where('order_id', '=', $order->id)->count() == 1) {
                return;
            }

            if (str_contains(strtolower($op->product_style), 'bitty') || str_contains(strtolower($op->prod_name), 'bitty')) {

                log_info("[split-order] Splitting order product #{$op->inventory_id} from order #{$order->id}");

                // delete order product from the original order
                OrderProduct::where('id', '=', $op->id)->delete();

                // create new order for bitty bag with single order product
                $newOrder = new Order;
                $newOrder->customer_id       = $order->customer_id;
                $newOrder->street_address    = $order->street_address;
                $newOrder->apartment         = $order->apartment;
                $newOrder->city              = $order->city;
                $newOrder->state             = $order->state;
                $newOrder->country_code      = $order->country_code;
                $newOrder->zip               = $order->zip;
                $newOrder->email             = $order->email;
                $newOrder->order_status      = $order->order_status;
                $newOrder->payment_ref       = $order->payment_ref;
                $newOrder->transaction_id    = $order->transaction_id;
                $newOrder->payment_amt       = null;
                $newOrder->ship_charged      = null;
                $newOrder->ship_cost         = null;
                $newOrder->subtotal          = null;
                $newOrder->total             = null;
                $newOrder->fbid              = $order->fbid;
                $newOrder->ship_name         = $order->ship_name;
                $newOrder->payment_date      = $order->payment_date;
                $newOrder->shipped_date      = $order->shipped_date;
                $newOrder->tracking_number   = $order->tracking_number;
                $newOrder->state_tax         = null;
                $newOrder->county_tax        = null;
                $newOrder->municipal_tax     = null;
                $newOrder->tax_total         = null;
                $newOrder->coupon            = null;
                $newOrder->coupon_discount   = null;
                $newOrder->calculated_zip    = null;
                $newOrder->apply_balance     = null;
                $newOrder->local_pickup      = $order->local_pickup;
                $newOrder->store_location_id = $order->store_location_id;
                $newOrder->save();

                // create order product in new order
                $newOp = new OrderProduct;
                $newOp->order_id            = $newOrder->id;
                $newOp->product_id          = $op->product_id;
                $newOp->inventory_id        = $op->inventory_id;
                $newOp->price               = $op->price;
                $newOp->cost                = $op->cost;
                $newOp->prod_name           = $op->prod_name;
                $newOp->color               = $op->color;
                $newOp->size                = $op->size;
                $newOp->comment_id          = $op->comment_id;
                $newOp->returned_date       = null;
                $newOp->refund_issued       = null;
                $newOp->order_source        = $op->order_source;
                $newOp->product_style       = $op->product_style;
                $newOp->product_brand       = $op->product_brand;
                $newOp->product_brand_style = $op->product_brand_style;
                $newOp->product_filename    = $op->product_filename;
                $newOp->product_description = $op->product_description;
                $newOp->weight              = $op->weight;
                $newOp->shopify_variant_id  = $op->shopify_inventory_id;
                $newOp->save();
            }
        }
    }
}
