<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Blade;
use DB;
use Hash;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '89.136.40.171') {
        //     \Log::info('----------------------------------------');
        //     DB::listen(function ($query) {
        //         \Log::info($query->sql);
        //         // \Log::info($query->bindings);
        //         \Log::info($query->time);
        //     });
        // }

        // map type for the 'tagging_tagged' table
        Relation::morphMap([
            'product' => 'App\Models\Product',
        ]);

        if ($this->app->environment('testing')) {
            $this->app->register(DuskServiceProvider::class);
        }

        Validator::extend('unique_coupon', function($attribute, $value, $parameters, $validator) {
            $coupons = DB::connection('shop')->table('coupons')
                ->where(DB::raw('UPPER(code)'), strtoupper(trim($value)))
                ->get();
            return count($coupons) ? false : true;
        }, 'This coupon code is already taken.');

        Validator::extend('only_lowercase', function($attribute, $value, $parameters, $validator) {
            if (preg_match('/[A-Z]/', $value)) {
                return false;
            }
            return true;
        }, 'Only lowercase letters are allowed.');

        Validator::extend('no_underscore', function($attribute, $value, $parameters, $validator) {
            if (preg_match('/_/', $value)) {
                return false;
            }
            return true;
        }, 'No underscores are allowed.');

        Validator::extend('hash', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, $parameters[0]);
        }, 'The old passward does not match our current records.');

        Blade::directive('error', function($expression) {
            return '<?php if ($errors->has('.$expression.')): ?>' .
                        '<span class="help-block">' .
                            '<strong><?php echo $errors->first('.$expression.'); ?></strong>' .
                        '</span>' .
                    '<?php endif; ?>';
        });

        Blade::directive('hasError', function($expression) {
            return '<?php echo $errors->has('.$expression.') ? "has-error" : ""; ?>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
