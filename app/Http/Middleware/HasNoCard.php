<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasNoCard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // don't let the user access step-2 again
        if (Auth::user()->stripe_id || ! Auth::user()->superadmin) {
            return redirect('/admin');
        }

        return $next($request);
    }
}
