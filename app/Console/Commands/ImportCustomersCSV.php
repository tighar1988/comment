<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;
use App\Models\ShopUser;

class ImportCustomersCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commentsold:import-customers-csv {shop} {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the customers from the csv export. Example: php artisan commentsold:import-customers-csv my-shop "customers.csv"';

    protected $validHeader = [
        0 => 'Name',
        1 => 'Facebook ID',
        2 => 'Instagram Username',
        3 => 'Email',
        4 => 'Invoices',
        5 => 'Paid Invoices',
        6 => 'Amount',
        7 => 'Paid Amount',
        8 => 'Last Invoiced At',
        9 => 'Registered At',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        reconnect_shop_database($this->argument('shop'));
        $file = $this->argument('file');
        $this->info("Trying to import customers from {$file}.");

        $path = storage_path($this->argument('file'));
        if (! app('files')->exists($path)) {
            $this->error("The file {$path} does not exist.");
            return;
        }

        $csv = Reader::createFromPath($path);
        $header = $csv->fetchOne();
        if ($header != $this->validHeader) {
            $this->error("Invalid header format for file {$file}.");
            return;
        }

        $nrUpdated = 0;
        $rows = $csv->setOffset(1)->fetchAll();
        $bar = $this->output->createProgressBar(count($rows));

        foreach ($rows as $row) {

            $bar->advance(1);

            if (! isset($row[0], $row[2], $row[3], $row[9])) {
                continue;
            }

            $name          = mysql_utf8(trim($row[0]));
            $instagramName = mysql_utf8(trim($row[2]));
            $email         = mysql_utf8(trim($row[3]));
            $registeredAt  = ! empty(trim($row[9])) ? strtotime(trim($row[9])) : null;

            if (empty($instagramName) || ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
                continue;
            }

            $customer = app('App\Models\ShopUser')->where('email', '=', $email)->first();
            if (is_null($customer)) {
                $customer = new ShopUser;
                $customer->balance = 0;
                $customer->local_pickup = 0;
            }

            // skip users where we converted the instagram username to an instagram id
            if (is_numeric($customer->instagram_id)) {
                continue;
            }

            $customer->name         = $name;
            $customer->instagram_id = $instagramName;
            $customer->email        = $email;
            $customer->created_at   = $registeredAt;

            $customer->save();
            $nrUpdated++;
        }

        $bar->finish();
        $this->line('');
        $this->info("Import complete. {$nrUpdated} customers imported.");
    }
}
