<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Image</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Color</th>
                <th>Size</th>
                <th>Options</th>
                <th>Choose card</th>
            </tr>
        </thead>
        <tbody>
            @if (count($waitlist->items))
                @foreach ($waitlist->items as $item)
                    <tr>
                        <td><img width="125px" height="125px" src="{{ product_thumb($item->image) }}" alt="{{ $item->name }}"></td>
                        <td>{{ $item->name }}</td>
                        <td>${{ amount($item->price) }}</td>
                        <td>{{ $item->color }}</td>
                        <td>{{ $item->size }}</td>
                        <td class="text-center">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-ellipsis-h fa-lg"></i>
                                </a>
                                <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                    <li><a href="/admin/customers/remove-from-waitlist/{{ $item->id }}" class="link" onclick="return confirm_delete(this);">Remove from Waitlist</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="5">The customer has no products in his waitlist at the moment.</td></tr>
            @endif
        <tr>
            <select>
                {{--@if (shop_setting('stripe.access_token'))--}}
                    @foreach ($customer->cards() as $card)
                        <option class="existing-card" value="{{ $card->card_id }}" data-description="Ending in: {{ $card->last_four }} Exp: {{ $card->exp_month }}/{{ $card->exp_year }}" data-image="{{ card_image($card->brand) }}">{{ $card->brand }}</option>
                    @endforeach
                {{--@endif--}}
            </select>
        </tr>
        </tbody>
    </table>
</div>
