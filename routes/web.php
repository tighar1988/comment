<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Admin routes that can be accessed only from the non-www main domain.
|
*/

Route::group(['domain' => env('APP_DOMAIN')], function() {

    // Facebook Messenger Webhooks
    Route::get('/messenger', 'Messenger\MessengerController@incoming');
    Route::post('/messenger', 'Messenger\MessengerController@incoming');
    Route::get('/messenger-test', 'Messenger\MessengerTestController@incoming');
    Route::post('/messenger-test', 'Messenger\MessengerTestController@incoming');

    // Facebook Webhooks
    Route::get('/facebook', 'Admin\FacebookController@incoming');
    Route::post('/facebook', 'Admin\FacebookController@incoming');

    // Instagram Webhooks
    Route::get('/instagram', 'Admin\InstagramController@incoming');
    Route::post('/instagram', 'Admin\InstagramController@incoming');

    // Shopify Webhooks
    Route::get('/shopify', 'Admin\ShopifyController@incoming');
    Route::post('/shopify', 'Admin\ShopifyController@incoming');

    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register-invite', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::get('register-boutiquehub', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('/', 'Admin\WelcomeController@index');
    Route::get('contact', 'Admin\WelcomeController@contact');
    Route::get('pricing', 'Admin\WelcomeController@pricing');

    Route::get('facebook-login/callback', 'Shop\SocialAuthController@redirectToShop');
    Route::get('instagram-connect/callback', 'Admin\InstagramController@callback');

    Route::group(['middleware' => ['auth', 'shop.not-created']], function() {
        Route::get('admin/setup/step-1', 'Admin\CreateShopController@index');
        Route::post('admin/setup/step-1', 'Admin\CreateShopController@newShop');
        Route::post('admin/setup/hear-about-us', 'Admin\CreateShopController@setHearAboutUs');
        Route::post('admin/setup/billing-plan', 'Admin\CreateShopController@setBillingPlan');
    });

    Route::group(['middleware' => ['auth', 'shop.created', 'shop.has-no-card']], function() {
        Route::get('admin/setup/step-2', 'Admin\CreateShopController@card');
        Route::post('admin/setup/step-2', 'Admin\CreateShopController@setCard');
    });

    Route::group(['middleware' => ['auth', 'shop.created', 'shop.has-card']], function() {

        Route::get('admin', 'Admin\DashboardController@index');

        Route::group(['middleware' => ['can:superadmin']], function() {
            Route::get('admin/setup', 'Admin\SettingsController@shop');
            Route::get('admin/setup/shipping', 'Admin\SettingsController@shipping');
            Route::get('admin/setup/shipping/prepay-labels', 'Admin\SettingsController@prepayLabels');
            Route::get('admin/setup/account', 'Admin\SettingsController@account');
            Route::get('admin/setup/billing', 'Admin\SettingsController@billing');
            Route::get('admin/setup/invoices', 'Admin\SettingsController@invoices');
            Route::get('admin/setup/invoices/subscription-plan', 'Admin\SettingsController@subscriptionPlan');
            Route::get('admin/setup/invoices/prepaid-labels', 'Admin\SettingsController@stripeCharges');
            Route::get('admin/setup/instagram', 'Admin\SettingsController@instagram');
            Route::get('admin/setup/templates', 'Admin\SettingsController@templatesFacebook');
            Route::get('admin/setup/templates-emails', 'Admin\SettingsController@templatesEmails');
            Route::get('admin/setup/embed', 'Admin\SettingsController@embed');
            Route::get('admin/setup/labels', 'Admin\SettingsController@labels');
            Route::get('admin/setup/repost', 'Admin\SettingsController@repost');
            Route::get('admin/setup/shopify', 'Admin\SettingsController@shopify');
            Route::resource('admin/setup/locations', 'Admin\LocationSettingsController');
            Route::post('admin/setup/locations/enabled', 'Admin\LocationSettingsController@enableMultipleLocations')->name('enable_location');
            Route::post('admin/setup/locations/disabled', 'Admin\LocationSettingsController@disableMultipleLocations')->name('disable_location');
            Route::post('admin/setup/change-default-location', 'Admin\LocationSettingsController@changeDefaultLocation');
            Route::post('admin/setup/shop-name', 'Admin\SettingsController@shopName');
            Route::post('admin/setup/shop-description', 'Admin\SettingsController@shopDescription');
            Route::post('admin/setup/shop-address', 'Admin\SettingsController@shopAddress');
            Route::post('admin/setup/timezone', 'Admin\SettingsController@timezone');
            Route::post('admin/setup/expire', 'Admin\SettingsController@expire');
            Route::post('admin/setup/remind', 'Admin\SettingsController@remind');
            Route::post('admin/setup/shipping-cost', 'Admin\SettingsController@shippingCost');
            Route::post('admin/setup/variable-shipping-cost', 'Admin\SettingsController@variableShippingCost');
            Route::post('admin/setup/free-shipping-maximum', 'Admin\SettingsController@freeShippingMaximum');
            Route::post('admin/setup/password', 'Admin\SettingsController@updatePassword');
            Route::post('admin/setup/paypal-keys', 'Admin\SettingsController@paypalKeys');
            Route::post('admin/setup/goshippo-api-token', 'Admin\SettingsController@goshippoApiToken');
            Route::get('admin/setup/stripe-connect', 'Admin\SettingsController@stripeConnect');
            Route::post('admin/setup/local-pickup/enable', 'Admin\SettingsController@enableLocalPickup');
            Route::post('admin/setup/local-pickup/disable', 'Admin\SettingsController@disableLocalPickup');
            Route::post('admin/setup/24h-free-shipping/enable', 'Admin\SettingsController@enable24hrFreeShipping');
            Route::post('admin/setup/24h-free-shipping/disable', 'Admin\SettingsController@disable24hrFreeShipping');
            Route::post('admin/setup/update-card', 'Admin\SettingsController@updateCard');
            Route::post('admin/setup/templates/update', 'Admin\SettingsController@updateTemplate');
            Route::post('admin/setup/embed/update', 'Admin\SettingsController@updateEmbedCode');
            Route::post('admin/setup/labels/enable', 'Admin\SettingsController@enableLabels');
            Route::post('admin/setup/labels/disable', 'Admin\SettingsController@disableLabels');
            Route::post('admin/setup/repost/enable', 'Admin\SettingsController@enableRepost');
            Route::post('admin/setup/repost/disable', 'Admin\SettingsController@disableRepost');
            Route::post('admin/setup/repost/min-quantity', 'Admin\SettingsController@repostMinQuantity');
            Route::post('admin/setup/repost/days-since-last-post', 'Admin\SettingsController@repostDaysSinceLastPost');
            Route::post('admin/setup/repost/add-time', 'Admin\SettingsController@repostAddTime');
            Route::post('admin/setup/repost/remove-time', 'Admin\SettingsController@repostRemoveTime');
            Route::post('admin/setup/repost/enable-group', 'Admin\SettingsController@repostEnableGroup');
            Route::post('admin/setup/repost/disable-group', 'Admin\SettingsController@repostDisableGroup');
            Route::post('admin/setup/toggle-switch', 'Admin\SettingsController@toggleSwitch');
            Route::post('admin/setup/buy-prepaid-credit', 'Admin\SettingsController@buyPrepaidCredit');

            Route::post('admin/setup/connect-shopify', 'Admin\ShopifyController@connectShopify');
            Route::get('admin/setup/shopify-connect/callback', 'Admin\ShopifyController@shopifyCallback');
            Route::get('admin/setup/shopify-connected', 'Admin\ShopifyController@shopifyConnected');
            Route::post('admin/setup/shopify-unsync', 'Admin\ShopifyController@disconnectShopify');

            Route::get('admin/setup/instagram-connect', 'Admin\InstagramController@connect');
            Route::get('admin/setup/instagram-callback', 'Admin\InstagramController@adminCallback');

            Route::get('admin/setup/paypal-identity', 'Admin\PayPalController@identity');
            Route::get('admin/setup/paypal-identity-callback', 'Admin\PayPalController@identityCallback');

            Route::get('admin/facebook-setup', 'Admin\FacebookController@index');
            Route::get('admin/facebook-setup/messenger', 'Admin\FacebookController@messenger');
            Route::post('admin/facebook-setup/enable-group', 'Admin\FacebookController@enableGroup');
            Route::post('admin/facebook-setup/disable-group', 'Admin\FacebookController@disableGroup');
            Route::post('admin/facebook-setup/set-share-group', 'Admin\FacebookController@setShareGroup');
            Route::post('admin/api/facebook-setup/connect', 'Admin\FacebookController@connectFacebook');
            Route::post('admin/facebook-setup/messenger/enable-page', 'Admin\FacebookController@enableMessengerPage');
            Route::post('admin/facebook-setup/messenger/disable-page', 'Admin\FacebookController@disableMessengerPage');
            Route::get('admin/facebook-setup/pages', 'Admin\FacebookController@pages');
            Route::post('admin/facebook-setup/pages/enable-page', 'Admin\FacebookController@enablePage');
            Route::post('admin/facebook-setup/pages/disable-page', 'Admin\FacebookController@disablePage');
        });

        Route::group(['middleware' => ['can:manage-store-themes']], function() {
            Route::get('admin/store/pages', 'Admin\PagesController@index');
            Route::get('admin/store/pages/add', 'Admin\PagesController@create');
            Route::post('admin/store/pages/add', 'Admin\PagesController@store');
            Route::get('admin/store/pages/edit/{pageId}', 'Admin\PagesController@edit');
            Route::patch('/admin/store/pages/edit/{pageId}', 'Admin\PagesController@update');
            Route::post('admin/store/pages/delete/{pageId}', 'Admin\PagesController@destroy');

            Route::get('admin/store/collections', 'Admin\CollectionsController@index');
            Route::get('admin/store/collections/add', 'Admin\CollectionsController@create');
            Route::post('admin/store/collections/add', 'Admin\CollectionsController@store');
            Route::get('admin/store/collections/edit/{collectionId}', 'Admin\CollectionsController@edit');
            Route::patch('/admin/store/collections/edit/{collectionId}', 'Admin\CollectionsController@update');
            Route::post('admin/store/collections/delete/{collectionId}', 'Admin\CollectionsController@destroy');

            Route::get('admin/store/tags', 'Admin\TagsController@index');
            Route::get('admin/store/tags/add', 'Admin\TagsController@create');
            Route::post('admin/store/tags/add', 'Admin\TagsController@store');
            Route::get('admin/store/tags/edit/{tagGroupId}', 'Admin\TagsController@edit');
            Route::patch('/admin/store/tags/edit/{tagGroupId}', 'Admin\TagsController@update');
            Route::post('admin/store/tags/delete/{tagGroupId}', 'Admin\TagsController@destroy');

            Route::get('admin/store/themes/{type}', 'Admin\LiquidController@index');
            Route::get('admin/store/themes/{type}/add', 'Admin\LiquidController@create');
            Route::post('admin/store/themes/{type}/add', 'Admin\LiquidController@store');
            Route::get('admin/store/themes/{type}/edit/{liquidId}', 'Admin\LiquidController@edit');
            Route::get('admin/store/themes/{type}/change/{fileName}', 'Admin\LiquidController@change');
            Route::patch('admin/store/themes/{type}/edit/{liquidId}', 'Admin\LiquidController@update');
            Route::post('admin/store/themes/{type}/delete/{liquidId}', 'Admin\LiquidController@destroy');
            Route::get('admin/store/themes/assets/open/{fileName}', 'Admin\LiquidController@open');
            Route::post('admin/store/themes/assets/upload', 'Admin\LiquidController@upload');

            Route::get('admin/store/preferences', 'Admin\PreferencesController@index');
            Route::post('admin/store/preferences', 'Admin\PreferencesController@save');

            Route::get('admin/store/navigation', 'Admin\NavigationController@index');
            Route::get('admin/store/navigation/add', 'Admin\NavigationController@create');
            Route::post('admin/store/navigation/add', 'Admin\NavigationController@store');
            Route::get('admin/store/navigation/edit/{menuId}', 'Admin\NavigationController@edit');
            Route::patch('admin/store/navigation/edit/{menuId}', 'Admin\NavigationController@update');
            Route::post('admin/store/navigation/delete/{menuId}', 'Admin\NavigationController@destroy');
            Route::post('admin/store/navigation/menu-item/add', 'Admin\NavigationController@storeMenuItem');
            Route::post('admin/store/navigation/menu-item/update', 'Admin\NavigationController@updateMenuItem');
            Route::post('admin/store/navigation/menu-item/delete', 'Admin\NavigationController@destroyMenuItem');
            Route::post('admin/store/navigation/sort', 'Admin\NavigationController@sort');

            Route::get('admin/store/blog-posts', 'Admin\BlogPostsController@index');
            Route::get('admin/store/blog-posts/add', 'Admin\BlogPostsController@create');
            Route::post('admin/store/blog-posts/add', 'Admin\BlogPostsController@store');
            Route::get('admin/store/blog-posts/edit/{blogPostId}', 'Admin\BlogPostsController@edit');
            Route::patch('admin/store/blog-posts/edit/{blogPostId}', 'Admin\BlogPostsController@update');
            Route::post('admin/store/blog-posts/delete/{blogPostId}', 'Admin\BlogPostsController@destroy');

            Route::get('admin/store/blogs', 'Admin\BlogController@index');
            Route::get('admin/store/blogs/add', 'Admin\BlogController@create');
            Route::post('admin/store/blogs/add', 'Admin\BlogController@store');
            Route::get('admin/store/blogs/edit/{blogId}', 'Admin\BlogController@edit');
            Route::patch('admin/store/blogs/edit/{blogId}', 'Admin\BlogController@update');
        });

        Route::group(['middleware' => ['can:manage-team']], function() {
            Route::get('admin/team', 'Admin\TeamController@index');
            Route::get('admin/team/add', 'Admin\TeamController@create');
            Route::post('admin/team/add', 'Admin\TeamController@store');
            Route::get('admin/team/edit/{id}', 'Admin\TeamController@edit');
            Route::patch('admin/team/edit/{id}', 'Admin\TeamController@update');
            Route::post('admin/team/delete/{id}', 'Admin\TeamController@destroy');
        });

        Route::group(['middleware' => ['can:manage-products']], function() {
            Route::get('admin/products', 'Admin\ProductsController@index');
            Route::get('admin/products/datatable', 'Admin\ProductsController@datatable');
            Route::get('admin/products/add', 'Admin\ProductsController@create');
            Route::post('admin/products/add', 'Admin\ProductsController@store');
            Route::get('admin/products/edit/{id}', 'Admin\ProductsController@edit');
            Route::patch('/admin/products/edit/{id}', 'Admin\ProductsController@update');
            Route::post('admin/products/delete/{id}', 'Admin\ProductsController@destroy');
            Route::get('admin/api/products/list', 'Admin\ProductsController@listProducts');
            Route::get('admin/products/{productId}/copy-text', 'Admin\ProductsController@copyText');
            Route::get('admin/products/giftcard/add', 'Admin\ProductsController@giftcardCreate');

            // Route::get('admin/products/most-inventory', 'Admin\ProductsController@mostInventory');
            Route::get('admin/products/{id}/variants', 'Admin\ProductVariantsController@index');
            Route::post('admin/products/{id}/variants/update', 'Admin\ProductVariantsController@updateVariants');
            Route::post('admin/variants/{id}/add-quantity', 'Admin\ProductVariantsController@addQuantity');
            Route::post('admin/variants/{id}/subtract-quantity', 'Admin\ProductVariantsController@subtractQuantity');
            Route::post('admin/variants/update/{inventoryId}', 'Admin\ProductVariantsController@updateVariant');
            Route::post('admin/variants/delete/{inventoryId}', 'Admin\ProductVariantsController@deleteVariant');
            Route::post('admin/variants/store/{productId}', 'Admin\ProductVariantsController@storeVariant');

            Route::get('admin/products/{id}/images', 'Admin\ProductImagesController@index');
            Route::post('admin/products/{id}/images', 'Admin\ProductImagesController@store');
            Route::post('admin/products/{productId}/images/{imageId}/make-main', 'Admin\ProductImagesController@makeMain');
            Route::post('admin/products/{productId}/images/sort', 'Admin\ProductImagesController@sort');
            Route::post('admin/products/{productId}/images/{imageId}/delete', 'Admin\ProductImagesController@destroy');

            Route::get('admin/giftcards', 'Admin\GiftCardController@index');
            Route::get('admin/giftcards/datatable', 'Admin\GiftCardController@datatable');
            Route::post('admin/giftcards/enable/{giftcardId}', 'Admin\GiftCardController@enable');
            Route::post('admin/giftcards/disable/{giftcardId}', 'Admin\GiftCardController@disable');
            Route::get('admin/giftcards/{productId}/preview', 'Admin\GiftCardController@preview');
        });

        Route::group(['middleware' => ['can:manage-posts']], function() {
            Route::get('admin/products/{productId}/facebook-post', 'Admin\ProductsController@showPostPreview');
            Route::post('admin/products/{productId}/facebook-post', 'Admin\ProductsController@postToFacebook');

            Route::get('admin/products/facebook-posts', 'Admin\ProductsController@facebookPosts');
            Route::post('admin/products/{productId}/link-to-facebook-post', 'Admin\ProductsController@linkToFacebookPost');
            Route::get('admin/products/facebook-pages-posts', 'Admin\ProductsController@facebookPagesPosts');
            Route::post('admin/products/{productId}/link-to-facebook-page', 'Admin\ProductsController@linkToFacebookPagePost');

            Route::get('admin/posts', 'Admin\PostsController@index');
            Route::get('admin/posts/datatable', 'Admin\PostsController@datatable');
            Route::get('admin/posts/edit/{id}', 'Admin\PostsController@edit');
            Route::get('admin/posts/comment/{id}', 'Admin\PostsController@commentInventory');
            Route::post('admin/posts/delete/{id}', 'Admin\PostsController@destroy');

            Route::get('admin/instagram-posts', 'Admin\InstagramController@posts');
            Route::get('admin/api/instagram-posts/products', 'Admin\InstagramController@listProducts');
            Route::post('admin/instagram-posts/link-to-product', 'Admin\InstagramController@linkToProduct');
        });

        Route::group(['middleware' => ['can:manage-orders']], function() {
            Route::get('admin/orders', 'Admin\OrdersController@index');
            Route::get('admin/orders/open', 'Admin\OrdersController@open');
            Route::get('admin/orders/fulfilled', 'Admin\OrdersController@fulfilled');
            Route::get('admin/orders/{id}/products', 'Admin\OrdersController@products');
            Route::get('admin/orders/{id}/edit', 'Admin\OrdersController@edit');
            Route::post('admin/orders/{id}/update', 'Admin\OrdersController@update');
            Route::post('admin/orders/{id}/fulfill', 'Admin\OrdersController@fulfill');
            Route::post('admin/orders/{id}/pick-up', 'Admin\OrdersController@pickUp');
            Route::get('admin/orders/{id}/create-label', 'Admin\OrdersController@createLabel');
            Route::post('admin/orders/{id}/buy-label', 'Admin\OrdersController@buyLabel');
            Route::post('admin/orders/buy-prepaid-credit', 'Admin\OrdersController@autoBuyPrepaidCredit');
            Route::post('admin/orders/{id}/confirm-buy-label', 'Admin\OrdersController@confirmBuyLabel');
            Route::post('admin/orders/{id}/refund-label', 'Admin\OrdersController@refundLabel');
            Route::get('admin/orders/{id}/return', 'Admin\OrdersController@return');
            Route::post('admin/orders/return-product/{id}', 'Admin\OrdersController@returnProduct');
            Route::post('admin/orders/refund-product/{id}', 'Admin\OrdersController@refundProduct');
            Route::post('admin/orders/process-all-open-orders', 'Admin\OrdersController@processAllOpenOrders');
            Route::post('admin/orders/process-selected-open-orders', 'Admin\OrdersController@processSelectedOpenOrders');
            Route::get('admin/orders/open/print-local-pickup-packing-slips', 'Admin\OrdersController@printLocalPickup');
            Route::get('admin/orders/open/print-labels', 'Admin\OrdersController@printLabels');
            Route::get('admin/orders/{orderId}/print-label', 'Admin\OrdersController@printLabel');
            Route::get('admin/orders/mass-create-labels', 'Admin\OrdersController@massCreateLabels');
            Route::post('admin/orders/mark-labels-as-printed', 'Admin\OrdersController@markLabelsAsPrinted');
            Route::post('admin/orders/mark-local-pickup-as-printed', 'Admin\OrdersController@markLocalPickupAsPrinted');
            Route::post('admin/api/scan-barcode', 'Admin\OrdersController@scanBarcode');
        });

        Route::group(['middleware' => ['can:manage-customers']], function() {
            Route::get('admin/customers', 'Admin\CustomersController@index');
            Route::get('admin/customers/datatable', 'Admin\CustomersController@datatable');
            Route::get('admin/customers/{id}', 'Admin\CustomersController@detail');
            Route::post('admin/customers/remove-from-waitlist/{waitlistId}', 'Admin\CustomersController@removeFromWaitlist');
            Route::post('admin/customers/remove-from-cart/{cartId}', 'Admin\CustomersController@removeFromCart');
            Route::post('admin/customers/{customerId}/add-to-cart/{inventoryId}', 'Admin\CustomersController@addToCart');
            Route::post('admin/customers/{customerId}/create-and-confirm-order', 'Admin\CustomersController@createAndConfirmOrder');
            Route::post('admin/customers/{customerId}/add-balance', 'Admin\CustomersController@addBalance');
            Route::post('admin/customers/{customerId}/subtract-balance', 'Admin\CustomersController@subtractBalance');
        });

        Route::group(['middleware' => ['can:manage-coupons']], function() {
            Route::get('admin/coupons', 'Admin\CouponsController@index');
            Route::get('admin/coupons/datatable', 'Admin\CouponsController@listCoupons');
            Route::get('admin/coupons/add', 'Admin\CouponsController@create');
            Route::post('admin/coupons/add', 'Admin\CouponsController@store');
            Route::post('admin/coupons/delete/{id}', 'Admin\CouponsController@destroy');
        });

        Route::group(['middleware' => ['canOr:view-reporting,view-reporting-packers']], function() {
            Route::get('admin/reports', 'Admin\ReportsController@index');
            Route::post('admin/reports', 'Admin\ReportsController@retrieve');
        });

        Route::group(['middleware' => ['can:view-waitlist']], function() {
            Route::get('admin/waitlist', 'Admin\ReportsController@waitlistByProduct');
            Route::get('admin/waitlist/by-variant', 'Admin\ReportsController@waitlistByVariant');
        });

        Route::group(['middleware' => ['can:view-shopping-cart']], function() {
            Route::get('admin/shopping-cart', 'Admin\ReportsController@shoppingCart');
        });

        Route::group(['middleware' => ['can:view-inventory-logs']], function() {
            Route::get('admin/inventory-logs', 'Admin\ReportsController@inventoryLogs');
        });

    });
});


/*
|--------------------------------------------------------------------------
| CDN Routes
|--------------------------------------------------------------------------
|
| CDN routes that can be accessed only from the cdn domain.
|
*/

Route::group(['domain' => env('CDN_DOMAIN', 'cdn.'.env('APP_DOMAIN'))], function() {

    // Online Store Assets
    Route::get('{shopId}/assets/{asset}', 'Shop\OnlineStoreController@asset');

    // CDN Product Images
    // XXX todo: add a middleware to respond with cache headers for this
    Route::get('{shopId}/images/{filename}', 'Shop\OnlineStoreController@image');
});


/*
|--------------------------------------------------------------------------
| Shop Routes
|--------------------------------------------------------------------------
|
| Routes for the shop pages. They are applied to all domains or subdomains.
|
*/

Route::group(['middleware' => 'shop'], function () {

    Route::get('giftcards/{hash}', 'Shop\OnlineStoreController@giftcard');
    Route::get('store', 'Shop\OnlineStoreController@index');
    Route::get('cart', 'Shop\OnlineStoreController@cart');
    Route::post('cart', 'Shop\OnlineStoreController@cart');
    Route::get('cart/change', 'Shop\OnlineStoreController@cartChange');
    Route::get('pages/{page}', 'Shop\OnlineStoreController@page');
    Route::get('collections/{collectionHandle}', 'Shop\OnlineStoreController@collection');
    Route::get('collections/{collectionHandle}/{tagId}', 'Shop\OnlineStoreController@collection');
    Route::get('collections/{collectionHandle}/products/{productHandle}', 'Shop\OnlineStoreController@collectionProduct');
    Route::get('products/{productHandle}', 'Shop\OnlineStoreController@product');
    Route::get('search', 'Shop\OnlineStoreController@search');

    Route::get('checkout', 'Shop\OnlineStoreController@checkout');
    Route::post('checkout', 'Shop\OnlineStoreController@checkoutAddress');
    Route::get('checkout/privacy-policy', 'Shop\OnlineStoreController@privacyPolicy');
    Route::get('checkout/refund-policy', 'Shop\OnlineStoreController@refundPolicy');
    Route::get('checkout/terms-of-service', 'Shop\OnlineStoreController@termsOfService');
    Route::post('checkout/apply-coupon', 'Shop\OnlineStoreController@applyCoupon');
    Route::post('checkout/stripe', 'Shop\OnlineStoreController@checkoutAndCreateOrder');
    Route::get('checkout/paypal-payment', 'Shop\OnlineStoreController@checkoutAndCreateOrder');
    Route::get('checkout/paypal-approval-link', 'Shop\OnlineStoreController@paypalApprovalLink');
    Route::get('checkout/complete', 'Shop\OnlineStoreController@checkoutComplete');

    Route::get('cart.js', 'Shop\OnlineStoreAjaxController@cart');
    Route::post('cart.js', 'Shop\OnlineStoreAjaxController@cart');
    Route::post('cart/add.js', 'Shop\OnlineStoreAjaxController@cartAdd');
    Route::post('cart/update.js', 'Shop\OnlineStoreAjaxController@cartUpdate');
    Route::get('cart/update.js', 'Shop\OnlineStoreAjaxController@cartUpdate');
    Route::post('cart/remove.js', 'Shop\OnlineStoreAjaxController@cartRemove');
    Route::post('cart/change.js', 'Shop\OnlineStoreAjaxController@cartChange');
    Route::post('waitlist/add.js', 'Shop\OnlineStoreAjaxController@waitlistAdd');

    Route::get('home', 'Shop\ThemeController@index');
    Route::get('login', 'Shop\ThemeController@login');
    // Route::post('subscribe', 'Shop\ThemeController@subscribe');

    Route::get('instagram-callback', 'Shop\SocialAuthController@instagramCallback');

    Route::group(['middleware' => 'shop.guest'], function() {
        Route::get('/', 'Shop\ShopController@index');
        Route::post('/', 'Shop\ShopController@index');
        Route::get('facebook-login', 'Shop\SocialAuthController@redirect');
        Route::get('facebook-login/callback', 'Shop\SocialAuthController@callback');

        Route::get('instagram-login', 'Shop\SocialAuthController@instagramLoginRedirect');

        Route::get('customers/login', 'Shop\OnlineStoreController@loginForm');
        Route::post('customers/login', 'Shop\OnlineStoreController@loginUser');
        Route::get('customers/register', 'Shop\OnlineStoreController@registerForm');
        Route::post('customers/register', 'Shop\OnlineStoreController@registerUser');
        Route::post('customers/recover-password', 'Shop\OnlineStoreController@recoverPassword');
        Route::get('customers/reset-password/{token}', 'Shop\OnlineStoreController@resetPasswordForm');
        Route::post('customers/reset-password/{token}', 'Shop\OnlineStoreController@resetPassword');
    });

    Route::group(['middleware' => 'shop.auth'], function() {
        Route::get('account', 'Shop\AccountController@account');
        Route::post('account', 'Shop\AccountController@account');

        Route::post('logout', 'Shop\SocialAuthController@logout');
        Route::get('logout', 'Shop\SocialAuthController@logout');

        Route::get('customers/account', 'Shop\OnlineStoreController@account');
        Route::get('customers/account/orders/{orderId}', 'Shop\OnlineStoreController@order');
        Route::get('customers/account/addresses', 'Shop\OnlineStoreController@addresses');

        Route::get('account/instagram-connect', 'Shop\SocialAuthController@instagramRedirect');

        Route::post('api/account/update-email', 'Shop\AccountController@updateEmail');
        Route::post('api/account/update-address', 'Shop\AccountController@updateAddress');
        Route::post('api/account/set-delivery-method', 'Shop\AccountController@setDeliveryMethod');
        Route::post('api/account/apply-coupon', 'Shop\AccountController@applyCoupon');
        Route::post('api/account/apply-gift-card', 'Shop\AccountController@applyGiftCard');
        Route::post('api/account/apply-balance', 'Shop\AccountController@applyBalance');
        Route::post('api/account/remove-from-cart', 'Shop\AccountController@removeFromCart');
        Route::post('api/account/remove-from-waitlist', 'Shop\AccountController@removeFromWaitlist');
        Route::get('api/account/waitlist', 'Shop\AccountController@waitlist');
        Route::post('api/account/messenger-id', 'Shop\AccountController@messengerId');

        Route::get('api/account/paid-orders', 'Shop\AccountController@paidOrders');
        Route::get('api/account/fulfilled-orders', 'Shop\AccountController@fulfilledOrders');

        Route::post('api/checkout/stripe-new-card', 'Shop\CheckoutController@checkoutWithNewCard');
        Route::post('api/checkout/stripe-existing-card', 'Shop\CheckoutController@checkoutWithExistingCard');
        Route::post('api/checkout/account-credit', 'Shop\CheckoutController@checkoutWithAccountCredit');
        Route::post('api/checkout/paypal-approval-link', 'Shop\CheckoutController@paypalApprovalLink');

        // paypal return urls for success and cancel
        Route::get('paypal-payment', 'Shop\CheckoutController@paypalConfirm');
        Route::get('paypal-payment-cancel', 'Shop\CheckoutController@paypalCancel');
        Route::get('paypal-order-success', 'Shop\CheckoutController@paypalOrderSuccess');
    });
});


/*
|--------------------------------------------------------------------------
| Status Routes
|--------------------------------------------------------------------------
|
| Routes for monitoring the application status.
|
*/

Route::group(['domain' => env('APP_DOMAIN'), 'middleware' => 'auth.super-admin'], function () {
    Route::get('app-status', 'StatusController@index');
    Route::post('app-status/login-as', 'StatusController@loginAs');
    Route::get('app-status/logs', 'StatusController@logs');
    Route::get('app-status/statistics', 'StatusController@statistics');
    Route::get('app-status/api/logs', 'StatusController@apiLogs');
    Route::get('app-status/phpinfo', 'StatusController@phpinfo');
    Route::get('app-status/debug', 'StatusController@debug');
    Route::get('app-status/debug-facebook', 'StatusController@debugFacebook');
    Route::get('app-status/shipping', 'StatusController@shipping');
    Route::post('app-status/unlock-jobs/{type}', 'StatusController@unlockJobs');
    Route::post('app-status/shop/{shop}/disconnect', 'StatusController@disconnectShop');
    Route::post('app-status/shop/{shop}/disable', 'StatusController@disableShop');
    Route::post('app-status/shop/{shop}/enable', 'StatusController@enableShop');
});
