<?php

namespace App\OnlineStore\Objects\GlobalObjects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\Objects\Page;

class Pages extends Drop
{
    protected $cache = [];

    protected $page;

    public function __construct()
    {
        $this->page = app('App\Models\Page');
    }

    /**
     * The pages object returns a list of all the pages in your store.
     *
     * <h1>{{ pages.about.title }}</h1>
     * <p>{{ pages.about.author }} says...</p>
     * <div>{{ pages.about.content }}</div>
     */
    public function handle_key($key)
    {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        $page = $this->page->handle($key)->first();

        $this->cache[$key] = is_null($page) ? null : new Page($page);
        return $this->cache[$key];
    }
}
