<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\BlogPostFormRequest;
use App\Models\Blog;
use App\Models\BlogPost;

class BlogPostsController extends Controller
{
    public function index()
    {
        $posts = BlogPost::with('blog')->get();
        return view('admin.store.blog.blog-posts', compact('posts'));
    }

    public function create()
    {
        $blogs = Blog::all();
        $user = request()->user();
        return view('admin.store.blog.blog-posts-form', compact('blogs', 'user'));
    }

    public function store(BlogPostFormRequest $request)
    {
        $blogPost = new BlogPost($request->except('featured_image'));

        if ($request->hasFile('featured_image')) {
            $file = $request->file('featured_image');
            $fileName = $file->getClientOriginalName();
            $file->storePubliclyAs(shop_id().'/blog', $fileName);
            $blogPost->featured_image = $fileName;
        }

        $blogPost->save();
        flash('Blog Post Saved!');
        return redirect("/admin/store/blog-posts");
    }

    public function edit($blogPostId)
    {
        $blogPost = BlogPost::find($blogPostId);
        $blogs = Blog::all();
        $user = request()->user();
        return view('admin.store.blog.blog-posts-form', compact('blogPost', 'blogs', 'user'));
    }

    public function update($blogPostId, BlogPostFormRequest $request)
    {
        $blogPost = BlogPost::find($blogPostId);
        $blogPost->fill($request->except('featured_image'));

        if ($request->hasFile('featured_image')) {
            $file = $request->file('featured_image');
            $fileName = $file->getClientOriginalName();
            $file->storePubliclyAs(shop_id().'/blog', $fileName);
            $blogPost->featured_image = $fileName;
        }

        $blogPost->save();
        flash('Blog Post Updated!');
        return redirect("/admin/store/blog-posts");
    }

    public function destroy($blogPostId)
    {
        BlogPost::destroy($blogPostId);
        flash('Blog Post Deleted!');
        return back();
    }
}
