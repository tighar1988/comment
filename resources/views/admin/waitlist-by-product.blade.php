@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Waitlist</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Waitlist</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom products">
                <ul class="nav nav-tabs">
                    <li class="{{ activeLink('admin/waitlist') }}"><a href="/admin/waitlist">Grouped by Product</a></li>
                    <li class="{{ activeLink('admin/waitlist/by-variant') }}"><a href="/admin/waitlist/by-variant">Grouped by Variant</a></li>
                </ul>
                <div class="tab-content r-p">
                    <div class="tab-pane active">
                        <br/>
                        <div class="table-responsive">
                            <table id="waitlits-table" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>SKU</th>
                                        <th># of waitlist</th>
                                        <th>Name</th>
                                        <th>Vendor</th>
                                        <th>Vendor-style</th>
                                        <th>Last Post</th>
                                        <th>Estimated Re-order Profit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{--
                                        @if ($waitlist->isEmpty())
                                            <tr><td colspan="6">No results found.</td></tr>
                                        @endif
                                    --}}
                                    @foreach ($waitlist as $item)
                                        <tr>
                                            <td>
                                                <a href="/admin/products/{{ $item->product_id }}/variants">
                                                    <img width="125px" height="125px" src="{{ product_image($item->thumb) }}" alt="{{ $item->sku }}">
                                                </a>
                                            </td>
                                            <td><a href="/admin/products/edit/{{ $item->product_id }}">{{ $item->sku }}</a></td>
                                            <td><a href="/admin/products/{{ $item->product_id }}/variants">{{ $item->on_waitlist }}</a></td>
                                            <td>{{ $item->product_name }}</td>
                                            <td>{{ $item->brand }}</td>
                                            <td>{{ $item->brand_style }}</td>
                                            <td data-order="{{ $item->latest_post }}">
                                                @if ($item->latest_post)
                                                    {{ apply_timezone($item->latest_post) }}
                                                @endif
                                            </td>
                                            <td>${{ amount($item->reorder_profit) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#waitlits-table').DataTable({
            "bStateSave": true,
            "order": [[ 2, "desc" ]]
        });
    });
</script>
@endsection
