<?php

namespace App\Listeners;

use App\Events\OrderPaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\CreateShopifyOrder;

class SyncShopifyOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPaid  $event
     * @return void
     */
    public function handle(OrderPaid $event)
    {
        if (! shop_setting('shopify.sync-orders')) {
            return;
        }

        if (! shopify_enabled()) {
            return;
        }

        if (! shop_setting('shopify.access_token')) {
            return;
        }

        dispatch(new CreateShopifyOrder(shop_id(), $event->orderId));
    }
}
