<?php

namespace App\Jobs;

use App\Models\Post;
use App\Models\Product;
use App\Facebook\Facebook;
use App\Facebook\ShopperComment;
use App\Facebook\CommentHarvester;
use Facebook\Exceptions\FacebookResponseException;

class FacebookHarvestGroups
{
    /**
     * Harvest Facebook Groups job.
     *
     * @param string $shop
     * @return void
     */
    public function handle($shop)
    {
        if (Post::count() == 0) {
            log_notice("[harvest] No posts in the shop database. Exiting..");
            return;
        }

        if (empty(shop_setting('facebook.user_id'))) {
            log_notice("[harvest] No facebook user id set. Exiting..");
            return;
        }

        $groupIds = fb_enabled_group_ids();
        if (empty($groupIds)) {
            log_notice("[harvest] No facebook group ids set by admin. Exiting..");
            return;
        }

        $checkAll = false;
        if (random_int(1, 20) == 1) {
            $checkAll = true;
        }

        foreach ($groupIds as $groupId) {

            log_info("[harvest] Checking group: {$groupId}");

            $lastHarvestTime = time();
            $facebook = new Facebook;
            $harvester = new CommentHarvester;

            if (random_int(1, 15) == 1) {
                //$pollTime = 1481398228; // the beginning of 2017
                $pollTime = (time() - (60 * 60 * 12));
            } else {
                $pollTime = (shop_setting("{$groupId}:last-harvest-time") - (10 * 60)); // 10 minutes ago
            }

            try {
                log_info("[harvest][debug] Asking for updates since {$pollTime}");
                $postIds = $facebook->updatedPostsSince($pollTime, $groupId);
            } catch (FacebookResponseException $e) {
                if (str_contains($e->getMessage(), 'Error validating access token: Session has expired on ')) {
                    // if the shop did not reconnect their Facebook account in the last 60 days, disconnect their Facebook
                    $facebookId = shop_setting('facebook.user_id');
                    shop_setting_set('facebook.user_id.disconnected', $facebookId);
                    shop_setting_set('facebook.user_id', null);
                }

                throw $e;
            }

            // if (empty($postIds)) {
            //     log_info("[harvest] No new posts to check");

            //     if (! $checkAll) {
            //         continue;
            //     }
            // }

            log_info("[harvest][debug] Post fb ids: " . implode(', ', $postIds));

            if ($checkAll) {
                $posts = Post::whereNotNull('fb_id')->get();
            } elseif (empty($postIds)) {
                $posts = Post::whereNotNull('fb_id')->orderBy('id', 'DESC')->limit(10)->get();
            } else {
                $posts = Post::fbid($postIds)->get();
            }
            $totalPosts = count($posts);
            $postCounter = 0;

            foreach ($posts as $post) {

                $postCounter++;

                // post was deleted from Facebook
                if ($post->scan_time == 101) {
                    // continue;
                }

                try {
                    $comments = $facebook->comments($post->fb_id);

                } catch (FacebookResponseException $e) {
                    if (str_contains($e->getMessage(), 'Unsupported get request.')) {
                        // the post was deleted from Facebook, disable it on our end also
                        $post->scan_time = 101;
                        $post->save();

                        $comments = [];
                    } else {
                        throw $e;
                    }
                }

                if (empty($comments)) {
                    log_info("[harvest] [{$postCounter}/{$totalPosts}] Already seen the latest comment: post {$post->fb_id}");
                    continue;
                }

                log_info("[harvest] [{$postCounter}/{$totalPosts}] New Comments: ".count($comments)." on post {$post->fb_id}");

                $product = Product::find($post->prod_id);

                foreach ($comments as $facebookComment) {
                    $comment = ShopperComment::create($facebookComment);
                    if (is_null($comment)) {
                        continue;
                    }

                    if (! $harvester->harvest($post, $product, $comment)) {
                        continue;
                    }
                }

                $post->scan_time = time();
                $post->save();

                // todo: Why do we need to pause and flush after each post? To spread out API requests?
                log_info("[harvest] Pausing 1 second.");
                sleep(1);
                flush();
            }

            shop_setting_set("{$groupId}:last-harvest-time", $lastHarvestTime);
        }
    }
}
