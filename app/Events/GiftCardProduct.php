<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GiftCardProduct
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $customerId;

    public $inventoryId;

    public $orderId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($customerId, $inventoryId, $orderId)
    {
        $this->customerId  = $customerId;
        $this->inventoryId = $inventoryId;
        $this->orderId     = $orderId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
