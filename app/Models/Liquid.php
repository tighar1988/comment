<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Liquid extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'liquid_files';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'theme',
        'type',
        'file_name',
        'content',
    ];

    public function isEditable($type)
    {
        if ($type != 'assets') {
            return true;
        }

        return ends_with($this->file_name, ['.liquid', '.js', '.css', '.json', '.scss', '.svg']);
    }

    public function getFileName()
    {
        return rtrim($this->file_name, '.liquid');
    }

    /**
     * Set the file name.
     *
     * @param  string  $value
     * @return void
    */
    public function setFileNameAttribute($value)
    {
        $this->attributes['file_name'] = rtrim($value, '.liquid').'.liquid';
    }

    public function setFileName($fileName, $type)
    {
        if ($type == 'assets' || $type == 'config' || $type == 'locales') {
            $this->attributes['file_name'] = $fileName;
        } else {
            $this->file_name = $fileName;
        }
    }

    public function getFiles($type, $theme)
    {
        $path = resource_path("views/themes/{$theme}/{$type}");

        // get all filenames from the local disk theme folder
        $viewFiles = collect();
        if (is_dir($path)) {
            foreach (app('files')->allFiles($path) as $file) {
                $liquid = new static;
                $liquid->setFileName($file->getRelativePathname(), $type);
                $viewFiles->push($liquid);
            }
        }

        // get all db liquid files for the theme
        $dbFiles = $this->type($type)->theme($theme)->get();

        // keep only the disk files that were not overwritten
        $fileNames = $dbFiles->pluck('file_name')->toArray();
        $viewFiles = $viewFiles->filter(function ($item) use ($fileNames) {
            return ! in_array($item->file_name, $fileNames);
        });

        $files = $viewFiles->merge($dbFiles);

        return $files;
    }

    public function scopeFile($query, $fileName)
    {
        return $query->where('file_name', '=', $fileName);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', '=', $type);
    }

    public function scopeTheme($query, $theme)
    {
        return $query->where('theme', '=', $theme);
    }
}
