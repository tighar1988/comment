<?php

namespace App\Facebook;

use Facebook\Facebook as FacebookSDK;
use App\Models\Comment;
use App\Facebook\Template;
use App\Jobs\SendFacebookComment;
use Facebook\Exceptions\FacebookResponseException;

class Facebook
{
    protected $fb;

    public function __construct()
    {
        $this->fb = new FacebookSDK([
            'app_id' => fb_app_id(),
            'app_secret' => fb_app_secret(),
            'default_graph_version' => fb_api_version(),
            'http_client_handler' => new CurlHttpClient,
        ]);
    }

    public function getClient()
    {
        return $this->fb;
    }

    public function allComments($postFbid)
    {
        $this->useMerchantAccessToken();
        $comments = [];

        // get all comments
        $response = $response = $this->fb->get('/'.$postFbid.'/comments?fields=created_time,from,message,id,parent&filter=stream&order=chronological');
        $graphEdge = $response->getGraphEdge();
        foreach ($graphEdge->all() as $comment) {
            $comments[] = $comment;
        }

        while ($after = $graphEdge->getNextCursor()) {
            $response = $this->fb->get('/'.$postFbid.'/comments?fields=created_time,from,message,id,parent&filter=stream&order=chronological&limit=100&after='.$after);
            $graphEdge = $response->getGraphEdge();
            foreach ($graphEdge->all() as $comment) {
                $comments[] = $comment;
            }
        }

        return $comments;
    }

    public function comments($postFbid)
    {
        $this->useMerchantAccessToken();
        $comments = [];

        // see if we have the latest comment
        $response = $this->fb->get('/'.$postFbid.'/comments?fields=created_time,from,message,id,parent&filter=stream&order=reverse_chronological&limit=1');
        $graphEdge = $response->getGraphEdge();
        if (isset($graphEdge[0]) && Comment::exists($graphEdge[0]->getField('id'))) {
            return [];
        }

        // get all comments
        $response = $response = $this->fb->get('/'.$postFbid.'/comments?fields=created_time,from,message,id,parent&filter=stream&order=chronological');
        $graphEdge = $response->getGraphEdge();
        foreach ($graphEdge->all() as $comment) {
            $comments[] = $comment;
        }

        while ($after = $graphEdge->getNextCursor()) {
            $response = $this->fb->get('/'.$postFbid.'/comments?fields=created_time,from,message,id,parent&filter=stream&order=chronological&limit=100&after='.$after);
            $graphEdge = $response->getGraphEdge();
            foreach ($graphEdge->all() as $comment) {
                $comments[] = $comment;
            }
        }

        return $comments;
    }

    public function updatedPostsSince($time, $groupId)
    {
        // todo: make sure $time is in correct EPOCH time for facebook
        $this->useMerchantAccessToken();
        $time = $time ?? strtotime('1 January 2016');

        $response = $this->fb->get('/'.$groupId.'/feed?fields=id&since='.$time);

        $postIds = [];
        $graphEdge = $response->getGraphEdge();

        do {
            foreach ($graphEdge as $graphNode) {
                $realPost = explode('_', $graphNode->asArray()['id']);
                $postIds[] = $realPost[1];
            }
        } while($graphEdge = $this->fb->next($graphEdge));

        return $postIds;
    }

    public function lastPosts($groupId)
    {
        $this->useMerchantAccessToken();

        $response = $this->fb->get('/'.$groupId.'/feed?fields=id,message,picture&limit=100');

        $posts = [];
        $graphEdge = $response->getGraphEdge();

        // do {
            foreach ($graphEdge as $graphNode) {
                $postData = $graphNode->asArray();
                $postData['group_id'] = $groupId;
                if (isset($postData['message'], $postData['picture'])) {
                    $posts[] = $postData;
                }
            }
        // } while($graphEdge = $this->fb->next($graphEdge));

        return $posts;
    }

    public function lastPagesPosts($pageId)
    {
        $this->useMerchantAccessToken();

        $response = $this->fb->get('/'.$pageId.'/feed?fields=id,message,picture&limit=100');

        $posts = [];
        $graphEdge = $response->getGraphEdge();

        foreach ($graphEdge as $graphNode) {
            $postData = $graphNode->asArray();
            if (isset($postData['message'])) {
                $posts[] = $postData;
            }
        }

        return $posts;
    }

    public function getPost($postId)
    {
        $this->useMerchantAccessToken();
        $response = $this->fb->get('/'.$postId);
        return $response->getGraphNode();
    }

    public function post($groupId, $postMessage, $imageUrl, $postAsPage = false)
    {
        if ($postAsPage) {
            $this->fb->setDefaultAccessToken(shop_setting('facebook.page.page_access_token'));
        } else {
            $this->useMerchantAccessToken();
        }

        $response = $this->fb->post("/{$groupId}/photos", [
            'message' => $postMessage,
            'url'     => $imageUrl,
        ]);

        return $response->getGraphNode();
    }

    public function commentOnPost($postId, $postComment, $delaySeconds = 0)
    {
    	$job = (new SendFacebookComment(shop_id(), $postId, $postComment))->delay($delaySeconds);
        dispatch($job);
    }

    public function getMerchantGroups()
    {
        try {

            $this->useMerchantAccessToken();
            $response = $this->fb->get('/'.shop_setting('facebook.user_id').'/groups');

            $groups = [];
            $graphEdge = $response->getGraphEdge();

            do {
                foreach ($graphEdge as $graphNode) {
                    $groups[] = $graphNode->asArray();
                }
            } while($graphEdge = $this->fb->next($graphEdge));

            return $groups;

        } catch (FacebookResponseException $e) {
            log_error($e);
            return [];
        }
    }

    public function getMerchantPages()
    {
        try {

            $this->useMerchantAccessToken();
            $response = $this->fb->get('/'.shop_setting('facebook.user_id').'/accounts');

            $pages = [];
            foreach ($response->getGraphEdge() as $status) {
                $pages[] = $status->asArray();
            }
            return $pages;

        } catch (FacebookResponseException $e) {
            log_error($e);
            return [];
        }
    }

    public function getLongedLivedToken($accessToken)
    {
        $auth = $this->fb->getOAuth2Client();
        return $auth->getLongLivedAccessToken($accessToken)->getValue();
    }

    public function getName($accessToken)
    {
        $response = $this->fb->get('/me?fields=id,name', $accessToken);
        $user = $response->getGraphUser();
        return $user['name'] ?? null;
    }

    public function groupMemberRequestCount($groupId)
    {
        $this->useMerchantAccessToken();
        $response = $this->fb->get('/'.$groupId.'?fields=member_request_count');
        $node = $response->getGraphNode();
        return $node['member_request_count'] ?? 0;
    }

    protected function useMerchantAccessToken()
    {
        $this->fb->setDefaultAccessToken(shop_setting('facebook.access_token'));
    }

    public function replyForInvalidComment($comment, $post, $privateReply = false)
    {
        $message = (new Template)->forInvalidComment($post->product);
        $this->replyToComment($comment, $message, $privateReply);
    }

    public function replyItemWaitlisted($comment, $privateReply = false)
    {
        $this->replyToComment($comment, template('item-waitlisted'), $privateReply);
    }

    public function replyWaitlistItemActivated($comment, $delaySeconds = 0)
    {
        $commentId = is_null($comment->parent_id) ? $comment->fb_id : $comment->parent_id;
        $this->postComment($commentId, template('waitlist-activated'), $delaySeconds);
    }

    public function replyNoEmail($comment, $privateReply = false)
    {
        $this->replyToComment($comment, template('order-without-email'), $privateReply);
    }

    public function replyGotOrder($comment, $privateReply = false)
    {
        $this->replyToComment($comment, template('new-order'), $privateReply);
    }

    protected function replyToComment($comment, $message, $privateReply = false)
    {
        if ($comment->isReplyComment()) {
            $commentId = $comment->parentId;
            $message = $comment->fromName . '. ' . $message;

        } elseif ($comment->hasCombinedId()) {
            $commentId = $comment->combinedId;

        } else {
            $commentId = $comment->id;
        }

        if ($privateReply) {
            $this->privateReply($commentId, $message);
        } else {
            $this->postComment($commentId, $message);
        }
    }

    public function privateReply($commentId, $message)
    {
        try {
            $pageToken = shop_setting('facebook.page.page_access_token');
            if (empty($pageToken)) {
                return;
            }

            $this->fb->setDefaultAccessToken($pageToken);

            $response = $this->fb->post("/{$commentId}/private_replies", [
                'message' => $message,
            ]);
        } catch (\Exception $e) {
            log_error($e);
        }
    }

    protected function postComment($commentId, $message, $delaySeconds = 0)
    {
    	$job = (new SendFacebookComment(shop_id(), $commentId, $message))->delay($delaySeconds);
        dispatch($job);

    	return true;
    }

    public function whitelistDomain($domain, $pageAccessToken)
    {
        $this->fb->setDefaultAccessToken($pageAccessToken);

        $response = $this->fb->post("/me/messenger_profile", ['whitelisted_domains' => [$domain, "https://commentsold.com"]]);
        $response = $response->getDecodedBody();

        if (isset($response['result']) && $response['result'] == 'success') {
            return true;
        }

        return false;
    }

    public function subscribeAppToPage($pageAccessToken)
    {
        $this->fb->setDefaultAccessToken($pageAccessToken);

        $response = $this->fb->post('/me/subscribed_apps');
        $response = $response->getDecodedBody();

        if (isset($response['success']) && $response['success']) {
            return true;
        }

        return false;
    }

    public function subscribePageWebhook($fields = 'feed')
    {
        $this->fb->setDefaultAccessToken(fb_app_access_token());

        $response = $this->fb->post('/'.fb_app_id().'/subscriptions', [
            'object'       => 'page',
            'callback_url' => config('services.facebook.webhook_callback'),
            'fields'       => $fields,
            'verify_token' => 'cstoken_432352045345',
        ]);

        $response = $response->getDecodedBody();
        if (isset($response['success']) && $response['success']) {
            return true;
        }

        return false;
    }

    public function unsubscribePageWebhook()
    {
        return $this->subscribePageWebhook(null);
    }
}
