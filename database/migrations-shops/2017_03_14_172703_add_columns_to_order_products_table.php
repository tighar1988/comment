<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_products', function (Blueprint $table) {
            $table->text('product_style')->nullable();
            $table->text('product_brand')->nullable();
            $table->text('product_brand_style')->nullable();
            $table->string('product_filename')->nullable();
            $table->text('product_description')->nullable();
            $table->integer('weight')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_products', function (Blueprint $table) {
            $table->dropColumn([
                'product_style',
                'product_brand',
                'product_brand_style',
                'product_filename',
                'product_description',
                'weight',
            ]);
        });
    }
}
