<option @if (isset($selected) && $selected == 'AL') selected="selected" @endif value="AL">Alabama</option>
<option @if (isset($selected) && $selected == 'AK') selected="selected" @endif value="AK">Alaska</option>
<option @if (isset($selected) && $selected == 'AS') selected="selected" @endif value="AS">American Samoa</option>
<option @if (isset($selected) && $selected == 'AZ') selected="selected" @endif value="AZ">Arizona</option>
<option @if (isset($selected) && $selected == 'AR') selected="selected" @endif value="AR">Arkansas</option>
<option @if (isset($selected) && $selected == 'AA') selected="selected" @endif value="AA">Armed Forces America</option>
<option @if (isset($selected) && $selected == 'AE') selected="selected" @endif value="AE">Armed Forces Europe</option>
<option @if (isset($selected) && $selected == 'AP') selected="selected" @endif value="AP">Armed Forces Pacific</option>
<option @if (isset($selected) && $selected == 'CA') selected="selected" @endif value="CA">California</option>
<option @if (isset($selected) && $selected == 'CO') selected="selected" @endif value="CO">Colorado</option>
<option @if (isset($selected) && $selected == 'CT') selected="selected" @endif value="CT">Connecticut</option>
<option @if (isset($selected) && $selected == 'DE') selected="selected" @endif value="DE">Delaware</option>
<option @if (isset($selected) && $selected == 'DC') selected="selected" @endif value="DC">District Of Columbia</option>
<option @if (isset($selected) && $selected == 'FL') selected="selected" @endif value="FL">Florida</option>
<option @if (isset($selected) && $selected == 'GA') selected="selected" @endif value="GA">Georgia</option>
<option @if (isset($selected) && $selected == 'GU') selected="selected" @endif value="GU">Guam</option>
<option @if (isset($selected) && $selected == 'HI') selected="selected" @endif value="HI">Hawaii</option>
<option @if (isset($selected) && $selected == 'ID') selected="selected" @endif value="ID">Idaho</option>
<option @if (isset($selected) && $selected == 'IL') selected="selected" @endif value="IL">Illinois</option>
<option @if (isset($selected) && $selected == 'IN') selected="selected" @endif value="IN">Indiana</option>
<option @if (isset($selected) && $selected == 'IA') selected="selected" @endif value="IA">Iowa</option>
<option @if (isset($selected) && $selected == 'KS') selected="selected" @endif value="KS">Kansas</option>
<option @if (isset($selected) && $selected == 'KY') selected="selected" @endif value="KY">Kentucky</option>
<option @if (isset($selected) && $selected == 'LA') selected="selected" @endif value="LA">Louisiana</option>
<option @if (isset($selected) && $selected == 'ME') selected="selected" @endif value="ME">Maine</option>
<option @if (isset($selected) && $selected == 'MD') selected="selected" @endif value="MD">Maryland</option>
<option @if (isset($selected) && $selected == 'MA') selected="selected" @endif value="MA">Massachusetts</option>
<option @if (isset($selected) && $selected == 'MI') selected="selected" @endif value="MI">Michigan</option>
<option @if (isset($selected) && $selected == 'MN') selected="selected" @endif value="MN">Minnesota</option>
<option @if (isset($selected) && $selected == 'MS') selected="selected" @endif value="MS">Mississippi</option>
<option @if (isset($selected) && $selected == 'MO') selected="selected" @endif value="MO">Missouri</option>
<option @if (isset($selected) && $selected == 'MT') selected="selected" @endif value="MT">Montana</option>
<option @if (isset($selected) && $selected == 'NE') selected="selected" @endif value="NE">Nebraska</option>
<option @if (isset($selected) && $selected == 'NV') selected="selected" @endif value="NV">Nevada</option>
<option @if (isset($selected) && $selected == 'NH') selected="selected" @endif value="NH">New Hampshire</option>
<option @if (isset($selected) && $selected == 'NJ') selected="selected" @endif value="NJ">New Jersey</option>
<option @if (isset($selected) && $selected == 'NM') selected="selected" @endif value="NM">New Mexico</option>
<option @if (isset($selected) && $selected == 'NY') selected="selected" @endif value="NY">New York</option>
<option @if (isset($selected) && $selected == 'NC') selected="selected" @endif value="NC">North Carolina</option>
<option @if (isset($selected) && $selected == 'ND') selected="selected" @endif value="ND">North Dakota</option>
<option @if (isset($selected) && $selected == 'MP') selected="selected" @endif value="MP">Northern Mariana Is</option>
<option @if (isset($selected) && $selected == 'NS') selected="selected" @endif value="NS">Nova Scotia</option>
<option @if (isset($selected) && $selected == 'OH') selected="selected" @endif value="OH">Ohio</option>
<option @if (isset($selected) && $selected == 'OK') selected="selected" @endif value="OK">Oklahoma</option>
<option @if (isset($selected) && $selected == 'OR') selected="selected" @endif value="OR">Oregon</option>
<option @if (isset($selected) && $selected == 'PW') selected="selected" @endif value="PW">Palau</option>
<option @if (isset($selected) && $selected == 'PA') selected="selected" @endif value="PA">Pennsylvania</option>
<option @if (isset($selected) && $selected == 'PR') selected="selected" @endif value="PR">Puerto Rico</option>
<option @if (isset($selected) && $selected == 'RI') selected="selected" @endif value="RI">Rhode Island</option>
<option @if (isset($selected) && $selected == 'SC') selected="selected" @endif value="SC">South Carolina</option>
<option @if (isset($selected) && $selected == 'SD') selected="selected" @endif value="SD">South Dakota</option>
<option @if (isset($selected) && $selected == 'TN') selected="selected" @endif value="TN">Tennessee</option>
<option @if (isset($selected) && $selected == 'TX') selected="selected" @endif value="TX">Texas</option>
<option @if (isset($selected) && $selected == 'UT') selected="selected" @endif value="UT">Utah</option>
<option @if (isset($selected) && $selected == 'VT') selected="selected" @endif value="VT">Vermont</option>
<option @if (isset($selected) && $selected == 'VI') selected="selected" @endif value="VI">Virgin Islands</option>
<option @if (isset($selected) && $selected == 'VA') selected="selected" @endif value="VA">Virginia</option>
<option @if (isset($selected) && $selected == 'WA') selected="selected" @endif value="WA">Washington</option>
<option @if (isset($selected) && $selected == 'WV') selected="selected" @endif value="WV">West Virginia</option>
<option @if (isset($selected) && $selected == 'WI') selected="selected" @endif value="WI">Wisconsin</option>
<option @if (isset($selected) && $selected == 'WY') selected="selected" @endif value="WY">Wyoming</option>
