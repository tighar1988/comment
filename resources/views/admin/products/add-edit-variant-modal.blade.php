<div class="modal fade" id="edit-variant-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="">
                @if ($isGiftCard) <input type="hidden" name="product_type" value="giftcard"> @endif
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Variant</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">@if($isGiftCard) Denomination: @else Color: @endif</label>
                        <input type="text" name="color" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group" @if($isGiftCard) style="display: none;" @endif>
                        <label class="control-label">Size:</label>
                        <input type="text" name="size" autocomplete="off" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" data-loading-text="Updating Variant.." class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add-variant-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" class="ajax-form" action="/admin/variants/store/{{ $product->id }}">
                @if ($isGiftCard) <input type="hidden" name="product_type" value="giftcard"> @endif
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Variant</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">@if($isGiftCard) Denomination: @else Color: @endif</label>
                        <input type="text" name="color" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group" @if($isGiftCard) style="display: none;" @endif>
                        <label class="control-label">Size:</label>
                        <input type="text" name="size" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Cost:</label>
                        <input type="text" name="cost" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Retail Price:</label>
                        <input type="text" name="price" autocomplete="off" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" data-loading-text="Adding Variant.." class="btn btn-success">Add Variant</button>
                </div>
            </form>
        </div>
    </div>
</div>
