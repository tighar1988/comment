@extends('admin.setup.facebook-setup')

@section('section', 'Facebook Groups')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body lr-m-15">

                <h3 style="margin-top: 0px;">Groups</h3>

                @if (empty($groups))
                    You don't have access to any groups with your facebook account. Visit <a href="https://facebook.com/">facebook</a> and create or join an existing facebook group as an admininstrator.
                @else
                    Below is a list of groups in your account. You need to be an administrator to post to these groups.<br/><br/>
                    To create a post, go to <a href="/admin/products">Products</a> and click on the "Post to Facebook" option to post that product.<br/><br/>

                    You can enable/disable multiple facebook groups where comments will be read from. You can also choose what facebook group to enable for the Facebook Share Button on the shop /account page. (only one group can be shared)<br/><br/>

                    @if (empty($enabledGroups))
                        You need to specify the group or groups that will be used for this shop.<br/><br/>
                    @endif

                    @foreach ($groups as $group)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td style="width:33%;">
                                                <a class="" href="https://www.facebook.com/{{ $group['id'] }}" target="_blank">{{ $group['name'] }}</a>
                                                @if (in_array($group['id'], $enabledGroups))
                                                    &nbsp;<strong>(Enabled)</strong>
                                                @endif
                                            </td>
                                            <td style="width:33%;">
                                                @if (in_array($group['id'], $enabledGroups))
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/facebook-setup/disable-group">
                                                        <input type="hidden" name="group_id" value="{{ $group['id'] }}">
                                                        <button type="submit" data-loading-text="Disabling Group.." class="btn btn-danger">Disable Group</button>
                                                    </form>
                                                @else
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/facebook-setup/enable-group">
                                                        <input type="hidden" name="group_id" value="{{ $group['id'] }}">
                                                        <button type="submit" data-loading-text="Enabling Group.." class="btn btn-default">Enable Group</button>
                                                    </form>
                                                @endif
                                            </td>
                                            <!--
                                            <td style="width:33%;">
                                                @if ($group['id'] == shop_setting('facebook.share-group-id'))
                                                    <strong>(Facebook sharing button Enabled)</strong>
                                                @else
                                                    <form class="form-horizontal" role="form" method="POST" action="/admin/facebook-setup/set-share-group">
                                                        <input type="hidden" name="group_id" value="{{ $group['id'] }}">
                                                        <button type="submit" data-loading-text="Enabling Share Button.." class="btn btn-default">Enable Share Button for this Group</button>
                                                    </form>
                                                @endif
                                            </td>
                                            -->
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>
@endsection

