<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:master.users,email',
        ];

        // for update skip unique email on current member, and optional password
        if ($this->method() == 'PATCH') {
            $rules['email'] = 'required|email|max:255|unique:master.users,email,' . $this->route('id');
            $rules['password'] = 'nullable|min:6|confirmed';

        } else {
            $rules['password'] = 'required|min:6|confirmed';
            $rules['password_confirmation'] = 'required';
        }

        return $rules;
    }
}
