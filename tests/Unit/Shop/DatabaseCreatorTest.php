<?php

namespace Tests\Unit\Shop;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use App\Shop\DatabaseCreator;
use DB;

class DatabaseCreatorTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(DatabaseCreator::class, new DatabaseCreator);
    }

    /** @test */
    public function it_creates_a_database()
    {
        $connection = Mockery::mock();
        $connection->shouldReceive('statement')->once()->with('CREATE DATABASE my_shop');

        DB::shouldReceive('connection')->once()->andReturn($connection);

        $creator = new DatabaseCreator;
        $creator->create('my-shop');
    }
}
