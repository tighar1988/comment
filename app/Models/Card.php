<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cards';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'default' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'card_id',
        'brand',
        'last_four',
        'exp_month',
        'exp_year',
        'default',
    ];

    public function hasExpired()
    {
        if (! is_numeric($this->exp_year) || ! is_numeric($this->exp_month)) {
            return false;
        }

        if (date('Y') > $this->exp_year) {
            return true;
        } elseif (date('Y') == $this->exp_year && date('m') > $this->exp_month) {
            return true;
        } else {
            return false;
        }
    }

    public static function store($userId, $card)
    {
        static::where('user_id', $userId)->update(['default' => false]);

        static::create([
            'user_id'   => $userId,
            'card_id'   => $card->id,
            'brand'     => $card->brand,
            'last_four' => $card->last4,
            'exp_month' => $card->exp_month,
            'exp_year'  => $card->exp_year,
            'default'   => true,
        ]);
    }
}
