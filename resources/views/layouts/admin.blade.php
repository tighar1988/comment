<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>:: CommentSold ::</title>
    @include('common.admin-favicons')
    <link rel="stylesheet" href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ cdn('/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ cdn('/jquery-ui-1.12.1/jquery-ui.min.css') }}"  type="text/css"/>
    {{-- todo: add versioning to the css --}}
    <link rel="stylesheet" href="{{ cdn('/css/admin-styles.css?v=23') }}" type="text/css">
    <link rel="stylesheet" href="{{ cdn('/plugins/datatables/dataTables.bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ cdn('/plugins/select/select2.css?v=2') }}" type="text/css">
    <link rel="stylesheet" href="{{ cdn('/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" type="text/css">
    {{-- <link rel="stylesheet" href="{{ cdn('/css/admin.css') }}" type="text/css"> --}}
    @yield('styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="/" class="logo">
                <span class="logo-mini"><img src="/images/logo-small.png" alt=""></span>
                <span class="logo-lg"><img src="/images/logo.png" alt=""></span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <a href="http://helpme.commentsold.com/" title="Help Center" target="_blank" class="help-me-link" role="button"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
            </nav>
        </header>

        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu">
                    <li class="{{ activeLink('admin') }}">
                        <a href="/admin"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                    </li>
                    @can('manage-products')
                        <li class="{{ activeLink('admin/products') }}">
                            <a href="/admin/products"><i class="fa fa-th-large"></i> <span>Products</span> <span class="label label-primary pull-right" title="Total Products">{{ app('App\Models\Product')->count() }}</span></a>
                        </li>
                    @endcan
                    @can('manage-posts')
                        <li class="{{ activeLink('admin/posts') }}">
                            <a href="/admin/posts"><i class="fa fa-rss"></i> <span>Posts</span> <span class="label label-success pull-right" title="Scheduled Posts">{{ app('App\Models\Post')->scheduledCount() }}</span></a>
                        </li>
                        @if (shop_setting('instagram.token'))
                        <li class="{{ activeLink('admin/instagram-posts') }}">
                            <a href="/admin/instagram-posts"><i class="fa fa-instagram"></i> <span>Instagram Posts</span> <span class="label label-success pull-right" title="Connected Posts">{{ app('App\Models\InstagramPost')->count() }}</span></a>
                        </li>
                        @endif
                    @endcan
                    @can('manage-orders')
                        <li class="{{ activeLink(['admin/orders', 'admin/orders/open', 'admin/orders/fulfilled']) }}">
                            <a href="/admin/orders/open"><i class="fa fa-truck"></i> <span>Orders/Fulfillment</span> <span class="label label-warning pull-right" title="Open Orders">{{ app('App\Models\Order')->paidCount() }}</span></a>
                        </li>
                    @endcan
                    @can('manage-coupons')
                        <li class="{{ activeLink('admin/coupons') }}">
                            <a href="/admin/coupons"><i class="fa fa-ticket"></i> <span>Coupons</span> <span class="label label-danger pull-right" title="Total Coupons">{{ app('App\Models\Coupon')->count() }}</span></a>
                        </li>
                    @endcan
                    @can('manage-customers')
                        <li class="{{ activeLink('admin/customers') }}">
                            <a href="/admin/customers"><i class="fa fa-user"></i> <span>Customers</span></a>
                        </li>
                    @endcan
                    @can('superadmin')
                        <li class="{{ activeLink('admin/setup') }}">
                            <a href="/admin/setup"><i class="fa fa-cogs"></i> <span>Setup</span></a>
                        </li>
                    @endcan
                    @can('superadmin')
                        <li class="{{ activeLink('admin/facebook-setup') }}">
                            <a href="/admin/facebook-setup"><i class="fa fa-facebook"></i> <span>Facebook Setup</span></a>
                        </li>
                    @endcan
                    @if (Auth::user()->can('view-reporting') || Auth::user()->can('view-reporting-packers'))
                        <li class="{{ activeLink('admin/reports') }}">
                            <a href="/admin/reports"><i class="fa fa-pie-chart"></i> <span>Reporting</span></a>
                        </li>
                    @endif
                    @can('view-waitlist')
                        <li class="{{ activeLink('admin/waitlist') }}">
                            <a href="/admin/waitlist"><i class="fa fa-clock-o"></i> <span>Waitlists</span></a>
                        </li>
                    @endcan
                    @can('view-shopping-cart')
                        <li class="{{ activeLink('admin/shopping-cart') }}">
                            <a href="/admin/shopping-cart"><i class="fa fa-shopping-cart"></i> <span>Shopping Cart</span></a>
                        </li>
                    @endcan
                    @can('view-inventory-logs')
                        <li class="{{ activeLink('admin/inventory-logs') }}">
                            <a href="/admin/inventory-logs"><i class="fa fa-key"></i> <span>Inventory Logs</span></a>
                        </li>
                    @endcan
                    @can('manage-store-themes')
                        @if (shop_setting('store.enabled'))
                            <li class="{{ activeLink('admin/store/pages') }}">
                                <a href="/admin/store/pages"><i class="fa fa-fw fa-shopping-bag"></i> <span>Online Store</span></a>
                            </li>
                        @endif
                    @endcan
                    @can('manage-team')
                        <li class="{{ activeLink('admin/team') }}">
                            <a href="/admin/team"><i class="fa fa-fw fa-user-secret"></i> <span>Team</span></a>
                        </li>
                    @endcan
                    <li>
                        <a href="/logout"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-fw fa-close"></i> <span>Logout</span>
                        </a>
                        <form id="logout-form" action="/logout" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </section>
        </aside>

        <div class="content-wrapper" style="min-height: 630px;">
            @include('admin.notifications')
            @yield('content')
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                Version 1.0
            </div>
            <strong>Copyright &copy; {{ date('Y') }} <a href="/">CommentSold</a>.</strong> All rights reserved.
        </footer>
        <div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
    </div>

    @include('common.confirm-delete-modal')
    @include('common.confirm-action-modal')
    @include('common.alert-modal')

    @include('common.logged-as-user')
    <script type="text/javascript" src="{{ cdn('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/highcharts.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/plugins/datatables/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/plugins/select/select2.full.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/clipboard.js-master/dist/clipboard.min.js') }}"></script>

    {{ script_raven() }}
    @include('common.admin-intercom')
    <script type="text/javascript" src="{{ cdn('/js/admin.js?v=17') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/admin-js.js?v=4') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#flash-overlay-modal').modal();
            $('.flash-notification').not('.alert-important').delay(5000).fadeOut(350);

            // Set up a global AJAX error handler to handle unauthorized responses.
            $.ajaxSetup({
                statusCode: {
                    403: function() {
                        alert('This action is unauthorized.');
                        window.location.href = window.location.origin;
                    },
                    401: function() {
                        alert('Your session has expired. Plese login again.');
                        window.location.href = window.location.origin;
                    }
                }
            });

            // button loading on form submits
            $('form').submit(function() {
                $('button[type="submit"]', $(this)).button('loading');
            });

            $('.toggle-switch').bootstrapToggle({
                on: 'Enabled',
                off: 'Disabled',
                size: 'small',
                onstyle: 'info',
                offstyle: 'danger'
            });

            $('.toggle-switch').change(function() {
                $.ajax({
                    type:'post',
                    url:'/admin/setup/toggle-switch',
                    data: {
                        state: $(this).prop('checked'),
                        key: $(this).attr('data-key')
                    },
                    success: function(response) {
                        if (response.success != true) {
                            alert('Something went wrong. Please refresh the page and try again.');
                        }
                    }
                });
            });

        });
    </script>
    @yield('scripts')
</body>
</html>
@include('common.facebook-pixel')
