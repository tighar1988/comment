<?php

namespace Tests\Unit\Reports;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Shopify\Shopify;

class ShopifyTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Shopify::class, new Shopify);
    }

    /** @test */
    public function it_gets_product_sku_the_id_when_variants_are_missing()
    {
        $shopify = new Shopify;
        $product = [
            'id' => '123456789',
            'variants' => [],
        ];

        $this->assertEquals('123456789', $shopify->getProductSKU($product));
    }

    /** @test */
    public function it_gets_product_sku_with_one_option()
    {
        $shopify = new Shopify;
        $product = [
            'id' => '123456789',
            'variants' => [
                ['id' => 1, 'sku' => 'SKU-01-M'],
            ],
        ];

        $this->assertEquals('SKU-01-M', $shopify->getProductSKU($product));
    }

    /** @test */
    public function it_gets_product_sku_with_two_options()
    {
        $shopify = new Shopify;
        $product = [
            'id' => '123456789',
            'variants' => [
                ['id' => 1, 'sku' => 'SKU-01-M'],
                ['id' => 2, 'sku' => 'SKU-01-L'],
            ],
        ];

        $this->assertEquals('SKU-01', $shopify->getProductSKU($product));
    }

    /** @test */
    public function it_gets_product_sku_with_three_options()
    {
        $shopify = new Shopify;
        $product = [
            'id' => '123456789',
            'variants' => [
                ['id' => 1, 'sku' => 'SKU-EL-S'],
                ['id' => 2, 'sku' => 'SKU-EL-M'],
                ['id' => 3, 'sku' => 'SKU-EL-L'],
            ],
        ];

        $this->assertEquals('SKU-EL', $shopify->getProductSKU($product));
    }

    /** @test */
    public function it_gets_product_sku_with_three_options_where_one_sku_is_null()
    {
        $shopify = new Shopify;
        $product = [
            'id' => '123456789',
            'variants' => [
                ['id' => 1, 'sku' => 'SKU-E01-S'],
                ['id' => 2, 'sku' => 'SKU-E01-M'],
                ['id' => 3, 'sku' => null],
            ],
        ];

        $this->assertEquals('SKU-E01', $shopify->getProductSKU($product));
    }
}
