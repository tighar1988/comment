<?php

namespace Tests\Unit\Reports;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Checkout\Checkout;
use App\Models\ShopUser;
use Mockery;
use App\Checkout\CheckoutException;

class CheckoutExceptionTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(CheckoutException::class, new CheckoutException);
    }
}
