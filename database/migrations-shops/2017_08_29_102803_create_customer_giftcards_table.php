<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerGiftcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_giftcards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('order_id')->unsigned()->nullable();
            $table->integer('inventory_id')->unsigned()->nullable();
            $table->string('code');
            $table->string('hash');
            $table->integer('expires_at')->nullable();
            $table->double('value')->nullable();
            $table->boolean('disabled')->nullable();
            $table->boolean('used')->nullable();
            $table->integer('redeemed_at')->nullable();
            $table->integer('redeemed_by_customer_id')->nullable()->unsigned();
            $table->timestamps();

            $table->index(['code', 'hash']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_giftcards');
    }
}
