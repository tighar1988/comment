<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'style',
        'product_name',
        'brand',
        'brand_style',
        'description',
        'store_description',
        'url',
        'charge_taxes',
        'publish_product',
        'received_product',
        'product_type',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'charge_taxes' => 'boolean',
        'publish_product' => 'boolean',
        'received_product' => 'boolean',
    ];

    public function isGiftCard()
    {
        return $this->product_type == 'giftcard';
    }

    public function isPublished()
    {
        if (is_null($this->publish_product)) {
            return true;
        }

        return (bool) $this->publish_product;
    }

    public function isTaxable()
    {
        if (is_null($this->charge_taxes)) {
            return true;
        }

        return (bool) $this->charge_taxes;
    }

    public function scopeUrl($query, $url)
    {
        $url = trim($url, '/ ');

        return $query->where('url', '=', $url);
    }

    public function scopeHandle($query, $handle)
    {
        $handle = trim($handle, '/ ');

        return $query->where('url', '=', $handle);
    }

    public function scopePublished($query)
    {
        return $query->whereNull('publish_product')->orWhere('publish_product', '=', '1');
    }

    public function scopeNotReceived($query)
    {
        return $query->whereNotNull('received_product')->where('received_product', '=', '0');
    }

    public function storeColors()
    {
        $colors = [];
        foreach ($this->inventory as $variant) {
            if (! empty($variant->color)) {
                $colors[] = $variant->color;
            }
        }

        return unique_options($colors);
    }

    public function storeSizes()
    {
        $sizes = [];
        foreach ($this->inventory as $variant) {
            if (! empty($variant->size)) {
                $sizes[] = $variant->size;
            }
        }

        return unique_options($sizes);
    }

    /**
     * Set the product's name.
     *
     * @param  string  $name
     * @return void
     */
    public function setProductNameAttribute($name)
    {
        $slug = strtolower($name);

        // make alphanumeric
        $slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);

        // clean multiple dashes or whitespaces
        $slug = preg_replace("/[\s-]+/", " ", $slug);

        // convert whitespaces and underscore to dash
        $slug = preg_replace("/[\s_]/", "-", $slug);

        $slug = str_replace('/', '-', $slug);

        $this->attributes['url'] = mysql_utf8($slug);
        $this->attributes['product_name'] = mysql_utf8($name);
    }

    /**
     * Set the product's description.
     *
     * @param  string  $description
     * @return void
     */
    public function setDescriptionAttribute($description)
    {
        $this->attributes['description'] = mysql_utf8($description);
    }

    public static function suggestedSKU()
    {
        $sku = Product::where('style', 'REGEXP', '.*sku[0-9]*')->orderBy('id', 'desc')->first();

        if (isset($sku->style)) {

            $sku = static::incrementSKU($sku->style);

            // if the sku exists, let's not suggest it
            $exists = Product::where('style', $sku)->first();
            if ($exists) {
                $sku = null;
            }
        }

        return $sku;
    }

    public static function incrementSKU($sku)
    {
        if (is_null($sku)) {
            return 'sku1';
        }

        $sku = filter_var($sku, FILTER_SANITIZE_NUMBER_INT);
        return 'sku' . ($sku + 1);
    }

    public function variants()
    {
        return $this->inventory()->orderBy('size', 'DESC');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image')->orderBy('is_main', 'DESC')->orderBy('position', 'ASC');
    }

    public function image()
    {
        return $this->hasOne('App\Models\Image')->where('is_main', true);
    }

    public function inventory()
    {
        return $this->hasMany('App\Models\Inventory', 'product_id');
    }

    public function inventoryQuantity()
    {
        return $this->hasOne('App\Models\Inventory', 'product_id')
            ->selectRaw('product_id, SUM(quantity) as quantity')
            ->groupBy('product_id');
    }

    public function price()
    {
        return $this->hasOne('App\Models\Inventory', 'product_id')
            ->selectRaw('product_id, SUM(price) as amount')
            ->groupBy('product_id');
    }

    /**
     * Get all of the product's tags.
     */
    public function tags()
    {
        return $this->morphToMany('App\Models\Tag', 'taggable', 'tagging_tagged');
    }

    /**
     * Get all of the product's collections.
     */
    public function collections()
    {
        return $this->belongsToMany('App\Models\Collection');
    }
}
