<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class SchemaImage extends Drop
{
    protected $block;

    public function __construct($block)
    {
        $this->block = $block;
    }

    public function __toString()
    {
        if (isset($this->block['settings']['image'])) {
            return $this->block['settings']['image'];
        }

        return '';
    }

    public function handle_key($key)
    {
        if ($key == 'aspect_ratio') {
            return '3.0';
        } elseif ($key == 'alt') {
            return '';
        }

        if (isset($this->block['settings'][$key])) {
            return $this->block['settings'][$key];
        }

        return null;
    }
}
