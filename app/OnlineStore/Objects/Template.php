<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class Template extends Drop
{
    protected $template;

    public function __construct($template)
    {
        $this->template = $template;
    }

    public function __toString()
    {
        return $this->template;
    }

    public function name()
    {
        return $this->template;
    }
}
