<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class Blog extends Drop
{
    // blog.all_tags
    // blog.articles
    // blog.articles_count
    // blog.comments_enabled?
    // blog.handle
    // blog.id
    // blog.moderated?
    // blog.next_article
    // blog.previous_article
    // blog.tags
    // blog.title
    // blog.url
}
