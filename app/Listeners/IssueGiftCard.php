<?php

namespace App\Listeners;

use App\Events\GiftCardProduct;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\CreateGiftCard;

class IssueGiftCard
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GiftCardProduct  $event
     * @return void
     */
    public function handle(GiftCardProduct $event)
    {
        dispatch(new CreateGiftCard(shop_id(), $event->customerId, $event->inventoryId, $event->orderId));
    }
}
