@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Reports</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Reports</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body lr-m-15 reports">

                    <form class="form-inline" role="form" method="POST" action="/admin/reports">
                        <div class="form-group @hasError('start_date')">
                            <label class="control-label">Start Date</label>
                            <input type="text" class="form-control date" id="start_date" name="start_date" value="{{ old('start_date', $report->startDate) }}" />
                            @error('start_date')
                        </div>

                        <div class="form-group @hasError('end_date')">
                            <label class="control-label">End Date</label>
                            <input type="text" class="form-control date" id="end_date" name="end_date" value="{{ old('end_date', $report->endDate) }}" />
                            @error('end_date')
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Retrieve</button>
                        </div>
                    </form>

                    @can('view-reporting')
                    <h3>Customers (Real-Time)</h3>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Total Customers</th>
                            <td>{{ $report->nrTotalCustomers }}</td>
                        </tr>
                        <tr>
                            <th>Messenger Users</th>
                            <td>{{ $report->nrMessengerUsers }}</td>
                        </tr>
                    </table>


                    <h3>Customers <small>- Showing from {{ $report->startDate }} to {{ $report->endDate }}</small></h3>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>New customers</th>
                            <td>{{ $report->nrNewCustomers }}</td>
                        </tr>
                    </table>


                    <h3>Orders <small>- Showing from {{ $report->startDate }} to {{ $report->endDate }}</small></h3>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Number of Orders</th>
                            <td>
                                Total: <strong>{{ $report->nrTotalOrders }}</strong>
                                ({{ $report->nrOpTotal }} products) <br/>

                                Paid: <strong>{{ $report->nrOrdersPaid }}</strong>
                                ({{ $report->nrOpPaid }} products) <br/>

                                Fulfilled: <strong>{{ $report->nrOrdersFulfilled }}</strong>
                                ({{ $report->nrOpFulfilled }} products) <br/>

                            </td>
                        </tr>
                        <tr>
                            <th>Average Order</th>
                            <td>${{ amount($report->averageOrder) }}</td>
                        </tr>
                        <tr>
                            <th>Number of Products in the Orders</th>
                            <td>
                                Created by Admin: {{ $report->adminOrders }}<br/>
                                Facebook Messenger: {{ $report->msgrOrders }}<br/>
                                Facebook Comments: {{ $report->commentOrders }}<br/>
                                Instagram Comments: {{ $report->instagramCommentOrders }}<br/>
                                @if (shop_setting('store.enabled'))
                                    Online Store: {{ $report->onlineStoreOrders }}<br/>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Orders Products Breakout</th>
                            <td>
                                Added: {{ percentage($report->nrOrderProductsPercentage) }}% /
                                Waitlist: {{ percentage($report->nrWaitlistPercentage) }}% <br/>

                                Added: {{ number_format($report->nrOrderProducts) }} / Waitlist: {{ number_format($report->nrWaitlist) }}
                            </td>
                        </tr>
                        <tr>
                            <th>Shipping Overview</th>
                            <td>
                                Shipping Revenue: ${{ amount($report->shippingCharged) }}<br/>
                                @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                                    Shipping Cost: ${{ amount($report->shippingCost) }}
                                @endif
                            </td>
                        </tr>
                        @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                        <tr>
                            <th>Total Returns ($)</th>
                            <td>${{ amount($report->returnsTotal) }}&nbsp;&nbsp;&nbsp; (Total: {{ $report->returnsCount }})</td>
                        </tr>
                        @endif
                    </table>


                    <h3>Payments Report <small>- Showing from {{ $report->startDate }} to {{ $report->endDate }}</small></h3>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Total Payments</th>
                            <td>
                                ${{ amount($report->ordersRevenue) }}<br/>

                                Stripe: ${{ amount($report->stripeRevenue) }} / PayPal: ${{ amount($report->paypalRevenue) }}
                            </td>
                        </tr>
                        <tr>
                            <th>Product Revenue</th>
                            <td>${{ amount($report->revenue) }}</td>
                        </tr>
                        <tr>
                            <th>Shipping Revenue</th>
                            <td>${{ amount($report->shippingCharged) }}</td>
                        </tr>
                        <tr>
                            <th>State Tax</th>
                            <td>${{ amount($report->stateTax) }}</td>
                        </tr>
                        <tr>
                            <th>County Tax</th>
                            <td>${{ amount($report->countyTax) }}</td>
                        </tr>
                        <tr>
                            <th>Municipal Tax</th>
                            <td>${{ amount($report->municipalTax) }}</td>
                        </tr>
                        <tr>
                            <th>Cost of Goods Sold</th>
                            <td>${{ amount($report->cogs) }}</td>
                        </tr>
                        @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                        <tr>
                            <th>Shipping Cost</th>
                            <td>${{ amount($report->shippingCost) }}</td>
                        </tr>
                        <tr>
                            <th>Gross Profit</th>
                            <td>${{ amount($report->grossProfit) }}</td>
                        </tr>
                        @endif
                    </table>
                    @endcan


                    @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                    @can('view-reporting')
                        <h3>Vendor Returns <small>- Showing from {{ $report->startDate }} to {{ $report->endDate }}</small></h3>
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Brand</th>
                                <th>Returns</th>
                            </tr>
                            @if ($report->vendorReturns->isEmpty())
                                <td colspan="2">No returns found.</td>
                            @endif
                            @foreach($report->vendorReturns as $vendor)
                                <tr>
                                    <th>{{ $vendor->brand }}</th>
                                    <td>
                                        {{ $vendor->count }} products - {{ $vendor->returns }} returned.
                                        Return percentage: {{ percentageOf($vendor->returns, $vendor->count) }}%
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                        <br/>
                    @endcan

                    @if (Auth::user()->can('view-reporting') || Auth::user()->can('view-reporting-packers'))
                        <h3>Packer overview &nbsp;&nbsp;    -   &nbsp; &nbsp;{{ $report->nrFulfilled }} fulfilled <small>- Showing from {{ $report->startDate }} to {{ $report->endDate }}</small></h3>
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Packer</th>
                                <th>Fulfilled</th>
                            </tr>
                            @if ($report->packers->isEmpty())
                                <td colspan="2">No packers found.</td>
                            @endif
                            @foreach ($report->packers as $packer)
                                <tr>
                                    <th>{{ $packer->name }}</th>
                                    <td>{{ $packer->fulfilled }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                    @endif

                    <br/>

                    @can('view-reporting')
                    <h3>Inventory Overview (Real-time)</h3>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Inventory Retail Value</th>
                            <td>${{ amount($report->inventoryValue) }}</td>
                        </tr>
                        <tr>
                            <th>Inventory Cost</th>
                            <td>${{ amount($report->inventoryCost) }}</td>
                        </tr>
                        <tr>
                            <th>Total Customer Credits</th>
                            <td>${{ amount($report->totalCustomerCredits) }}</td>
                        </tr>
                    </table>

                    <br/>

                    <div id="payments-chart" style="width:100%; height:400px;"></div>
                    @endcan

                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){

		$("#start_date").datepicker({
        	numberOfMonths: 2,
        	onSelect: function(selected) {
       	 		$("#end_date").datepicker("option","minDate", selected)
        	}
    	});
		$("#end_date").datepicker({
        	numberOfMonths: 2,
			minDate: 0,
        	onSelect: function(selected) {
           		$("#start_date").datepicker("option","maxDate", selected)
        	}
    	});

        @can('view-reporting')
        $('#payments-chart').highcharts({
            title: {
                text: 'Payments broken out per hour',
                x: -20 //center
            },
            xAxis: {
                categories: ['Midnight', '1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am', '11am', 'Noon', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm', 'Midnight']
            },
            yAxis: {
                title: {
                    text: '$$$'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valuePrefix: '$$'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Dollars per hour',
                //data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                data: [{{ $report->orderHR }}]
            }]
        });
        @endcan
    });
</script>
@endsection
