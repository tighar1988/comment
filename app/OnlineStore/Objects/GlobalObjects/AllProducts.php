<?php

namespace App\OnlineStore\Objects\GlobalObjects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\Objects\Product;

class AllProducts extends Drop
{
    protected $cache = [];

    protected $product;

    public function __construct()
    {
        $this->product = app('App\Models\Product');
    }

    /**
     * The all_products object contains a list of all the products in your store.
     * You can use all_products to access products by their handles.
     *
     * {{ all_products['wayfarer-shades'].title }}
     *
     * Note
     * The all_products object has a limit of 20 unique handles per page. If you
     * want more than 20 products, then consider using a collection instead.
     */
    public function handle_key($key)
    {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        $product = $this->product->handle($key)->with('items')->first();
        dd($product);

        // $this->cache[$key] = is_null($product) ? null : new Product($collection);
        // return $this->cache[$key];
    }
}
