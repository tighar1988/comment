<?php

namespace App\Jobs;

use App\Mails\ShopMailer;
use App\Models\Post;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\ShopSettings;
use App\Models\User;
use Facebook;

class FacebookScheduleRepost
{
    /**
     * Schedule Re-post job.
     *
     * @param string $shop
     * @return void
     */
    public function handle($shop)
    {
        if (! shop_setting('repost-enabled')) {
            log_notice("[repost] Reposting is disabled. Exiting..");
            return;
        }

        $groupIds = $this->getRepostGroups();
        if (empty($groupIds)) {
            log_notice("[repost] No facebook groups set by admin. Exiting..");
            return;
        }

        $times = ShopSettings::repostTimes();
        if (empty($times)) {
            log_notice("[repost] No schedule times set. Exiting..");
            return;
        }

        $minInventory = shop_setting('repost.minimum-quantity', 10);
        $minDays = shop_setting('repost.days-since-last-post', 7);

        foreach ($groupIds as $groupId) {
            $this->repost($groupId, $minInventory, $minDays, $times, $shop);
        }
    }

    public function getRepostGroups()
    {
        $repostGroups = ShopSettings::repostGroups();
        $groupIds = [];

        foreach (fb_enabled_group_ids() as $enabledGroup) {
            if (in_array($enabledGroup, $repostGroups)) {
                $groupIds[] = $enabledGroup;
            }
        }

        return $groupIds;
    }

    public function repost($groupId, $minInventory, $minDays, $times, $shop)
    {
        log_info("[repost] Checking reposts for group: {$groupId}");

        $now = time();
        $minLastPost = ($now - ($minDays * 24*60*60));

        // we want to start with the highest quantity items and work our way down
        $items = Inventory::selectRaw('product_id, SUM(quantity) as quantity')
            ->groupBy('product_id')
            ->orderBy('quantity', 'DESC')
            ->get();

        // narrow down stuff that doesn't qualify
        $posts = [];
        $postsCount = 0;
        foreach($items as $item) {

            // we got enough posts to repost
            if ($postsCount == count($times)) {
                break;
            }

            if ($item->quantity < $minInventory)  {
                log_info("[repost] #{$item->product_id} skipped since inventory is less than {$minInventory}");
                continue;
            }

            $post = Post::where('prod_id', '=', $item->product_id)->orderBy('id', 'desc')->first();
            if (is_null($post)) {
                log_info("[repost] #{$item->product_id} skipped since it was not posted before");
                continue;
            }

            if ($post->created_time > $minLastPost) {
                log_info("[repost] #{$post->prod_id} skipped since {$minDays} days not passed since last post");
                continue;
            }

            if (is_numeric($post->scheduled_time)) {
                log_info("[repost] #{$post->prod_id} skipped since it's scheduled to post");
                continue;
            }

            $supplement = json_decode($post->supplement);
            if (isset($supplement->group) && $supplement->group != $groupId) {
                log_info("[repost] Skipping #{$post->prod_id} since it was posted in a different group");
                continue;
            }

            $posts[] = $post;
            $postsCount++;
        }

        $createdPosts = [];
        $shopTimezone = shop_setting('shop.timezone');

        foreach ($posts as $key => $post) {
            if (! isset($times[$key])) {
                break;
            }

            $hour = $times[$key]->hour;
            $min  = $times[$key]->min;
            $ampm = $times[$key]->ampm;

            if (strlen($hour) == 1) $hour = '0'.$hour;
            if (strlen($min) == 1) $min = '0'.$min;
            $date = date('m/d/Y', strtotime('+24 hours')).' '.$hour.':'.$min.' '.$ampm;

            // create the post time in the shop's timezone
            $postTime = \Carbon\Carbon::createFromFormat('n/j/Y H:i a', $date, $shopTimezone);

            // convert it to our app timezone
            $postTime->timezone(config('app.timezone'));
            $stampedPostTime = strtotime($postTime->toDateTimeString());

            // create schedule post
            $newPost = new Post;
            $newPost->prod_id = $post->prod_id;
            $newPost->created_time = time();
            $newPost->scan_time = time();
            $newPost->scheduled_time = $stampedPostTime;
            $newPost->image = $post->image;
            $newPost->supplement = $post->supplement;

            $newPost->addVariantsLeft($post->product);
            $newPost->save();

            log_info("[repost] Creating post for {$hour}:{$min} {$ampm} ({$stampedPostTime}) on product #{$post->prod_id}");
            $createdPosts[] = $newPost;
        }

        $shopOwner = User::getByShopName($shop);
        (new ShopMailer)->sendNewRepostEmail($shopOwner, $createdPosts);
    }
}
