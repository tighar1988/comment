<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'id'             => 1,
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'superadmin'     => 1,
        'permissions'    => null,
        'shop_name'      => 'test-shop',
        'remember_token' => str_random(10),
        'created_at'     => time(),
        'updated_at'     => time(),
        'stripe_id'      => 'stripe-id',
        'card_brand'     => 'Visa',
        'card_last_four' => '4242',
        'trial_ends_at'  => null,
        'shop_domain'    => null,
        'prepaid_credit' => null,
    ];
});

// $factory->defineAs(App\Models\User::class, 'admin', function ($faker) use ($factory) {
//     $user = $factory->raw(App\Models\User::class);

//     return array_merge($user, ['name' => 'Admin', 'email' => 'admin@example.com']);
// });
