<?php

namespace App\Http\Controllers\Admin;

use App\Console\Commands\DatabaseMigrateShop;
use App\Http\Requests\LocationSettingsRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MultipleStoreLocation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LocationSettingsController extends Controller
{
    public function index()
    {
        $locations = MultipleStoreLocation::orderBy('id', 'asc')->get();
        return view('admin.setup.locations', compact('locations'));
    }

    public function store(LocationSettingsRequest $request)
    {
        MultipleStoreLocation::create($request->all());
        return redirect()->back();
    }

    public function update(LocationSettingsRequest $request, $id)
    {
        $location = MultipleStoreLocation::findOrFail($request->id);
        if ($location->update($request->all())) {
            return response()->json('updated', 200);
        } else {
            return response()->json('error occurred', 404);
        }
    }

    public function destroy($id)
    {
        $location = MultipleStoreLocation::findOrFail($id);
        $location->delete();

        return redirect()->back();
    }

    public function changeDefaultLocation(Request $request)
    {
        MultipleStoreLocation::where('default_address', '1')->update(['default_address' => null]);
        $location = MultipleStoreLocation::findOrFail($request->id);
        $location->default_address = 1;
        $location->update();

        return response()->json('success', 200);
    }

    public function enableMultipleLocations()
    {
        shop_setting_set('multiple-locations-enabled', true);
        flash('Multiple Locations Enabled!');
        return back();
    }

    public function disableMultipleLocations()
    {
        shop_setting_set('multiple-locations-enabled', false);
        flash('Multiple Locations Disabled!');
        return back();
    }
}
