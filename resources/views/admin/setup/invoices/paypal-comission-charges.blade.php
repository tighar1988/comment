@extends('admin.setup.invoices.invoices')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body paypal-invoices">
                <h3>PayPal Charges</h3>
                <p class="text-muted">
                    This is an overview of charges made to your card for PayPal commission fees.<br/>There is a $50 minimum, so your card will not be charged until there is $50 worth of commissions from PayPal<br/>
                </p>

                <div class="table-responsive">
                    <table id="invoices-table" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Amount</th>
                                <th>Billing Period</th>
                                <th>Date</th>
                                <th>Card</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($charges as $charge)
                                <tr @if (! $charge->isPaid()) class="warning" @endif>
                                    <td>${{ amount($charge->amount) }} ({{ $charge->feePercentage() }} * ${{ amount($charge->ordersTotal()) }})</td>
                                    <td>{{ apply_timezone($charge->start_date) }} - {{ apply_timezone($charge->end_date) }}</td>
                                    <td>{{ apply_timezone($charge->created_at) }}</td>
                                    <td>{{ $charge->cardBrand() }} {{ $charge->cardLast4() }}</td>
                                    <td>
                                        @if ($charge->isPaid())
                                            Paid
                                        @else
                                            Failed - {{ $charge->getError() }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <br/>
                {{ pagination_links($charges) }}

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('styles')
<style type="text/css">
    .paypal-invoices.box-body {
        padding: 0px;
    }
    .paypal-invoices h3,
    .paypal-invoices p {
        padding: 0px 10px;
    }
</style>
@endsection

