@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($page) ? true : false; ?>
<section class="content-header">
    <h1>Store</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/pages"><i class="fa fa-th-large"></i>Store</a></li>
        <li><a href="/admin/store/pages">Pages</a></li>
        @if ($isEdit)
            <li class="active">{{ $page->title }}</li>
        @else
            <li class="active">Add</li>
        @endif
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('title')">
                                    <label>Page Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="e.g. Contact Us, FAQs" value="{{ old('title', $page->title ?? '') }}">
                                    @error('title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('url')">
                                    <label>URL</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ shop_url('pages/') }}</span>
                                        <input type="text" class="form-control" name="url" value="{{ old('url', $page->url ?? '') }}">
                                    </div>
                                    @error('url')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('content')">
                                    <label>Content</label>
                                    <textarea class="form-control" name="content" rows="3">{{ old('content', $page->content ?? '') }}</textarea>
                                    @error('content')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('seo_title')">
                                    <label>SEO Page Title <small class="text-muted">(max 70 characters)</small></label>
                                    <input type="text" class="form-control" name="seo_title" placeholder="" maxlength="70" value="{{ old('seo_title', $page->seo_title ?? '') }}">
                                    @error('seo_title')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('seo_description')">
                                    <label>SEO Meta Description <small class="text-muted">(max 160 characters)</small></label>
                                    <textarea class="form-control" maxlength="160" name="seo_description" rows="3">{{ old('seo_description', $page->seo_description ?? '') }}</textarea>
                                    @error('seo_description')
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success pull-right">@if($isEdit) Update Page @else Add Page @endif</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{ cdn('/ckeditor/ckeditor.js') }}"></script>
<script>CKEDITOR.replace('content');</script>
@endsection
