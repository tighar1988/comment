<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\ProductImageFormRequest;
use App\Models\Image as ProductImage;
use Image;
use Storage;

class ProductImagesController extends Controller
{
    public function index($id)
    {
        $product = Product::with('images')->find($id);
        return view('admin.products.product-images', compact('product'));
    }

    public function store($id, ProductImageFormRequest $request)
    {
        $response = [];

        foreach ($request->file('files') as $file) {

            $originalName = $file->getClientOriginalName();
            $path = $file->storePublicly(shop_id().'/products');
            $fileName = basename($path);

            $image = Image::make(Storage::get($path));
            $originalWidth = $image->width();
            $originalHeight = $image->height();

            if ($image->width() != $image->height()) {
                $cutout = min($image->width(), $image->height());
                $image->crop($cutout, $cutout);
            }

            $image->resize(290, 290);

            Storage::put(shop_id().'/products/thumbs/'.$fileName, $image->stream()->__toString(), 'public');

            ProductImage::newProductImage($id, $fileName, $originalWidth, $originalHeight);

            $response[] = ['name' => $originalName];
        }

        return ['files' => $response];
    }

    public function makeMain($productId, $imageId)
    {
        ProductImage::where('product_id', $productId)->update(['is_main' => false]);

        $updated = ProductImage::where('id', $imageId)->update(['is_main' => true]);

        return ['status' => $updated];
    }

    public function sort()
    {
        $imageIds = request('image_ids');
        $position = 1;

        foreach ($imageIds as $imageId) {
            $image = ProductImage::find($imageId);
            if (! $image) {
                continue;
            }

            $image->position = $position++;
            $image->save();
        }

        return ['status' => 1];
    }

    public function destroy($productId, $imageId)
    {
        $image = ProductImage::find($imageId);
        $image->delete();

        if (! ProductImage::where('product_id', $productId)->where('is_main', true)->exists()) {
            ProductImage::where('product_id', $productId)->limit(1)->update(['is_main' => true]);
        }

        return back();
    }
}
