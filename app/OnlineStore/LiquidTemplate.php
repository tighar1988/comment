<?php

namespace App\OnlineStore;

use App\OnlineStore\Vendor\Liquid\Template as VendorTemplate;
use App\OnlineStore\Vendor\Liquid\Context;
use App\OnlineStore\LiquidFilters\AdditionalFilters;
use App\OnlineStore\LiquidFilters\UrlFilters;
use App\OnlineStore\LiquidTags\TagLayout;
use App\OnlineStore\LiquidFileSystem;
use App\OnlineStore\Objects\Template;
use App\OnlineStore\Objects\GlobalObjects\Linklists;
use App\OnlineStore\Objects\GlobalObjects\Collections;
use App\OnlineStore\Objects\GlobalObjects\Blogs;
use App\OnlineStore\Objects\GlobalObjects\Articles;
use App\OnlineStore\Objects\GlobalObjects\AllProducts;
use App\OnlineStore\Objects\GlobalObjects\Pages;
use App\OnlineStore\Objects\GlobalObjects\Handle;
use App\OnlineStore\Objects\GlobalObjects\Scripts;
use App\OnlineStore\Objects\Settings;
use App\OnlineStore\Objects\Shop;
use App\OnlineStore\Objects\CountryOptionTags;
use App\OnlineStore\Objects\CurrentPage;
use App\OnlineStore\Objects\CurrentTags;
use App\OnlineStore\Objects\PageDescription;
use App\OnlineStore\Objects\PageTitle;
use App\OnlineStore\Objects\Cart;
use App\OnlineStore\Objects\Customer;
use App\OnlineStore\Objects\Theme;
use App\OnlineStore\Objects\Drop;
use App\OnlineStore\Objects\ContentForHeader;
use App\Models\ShopUser;
use App\OnlineStore\CurrentView;

class LiquidTemplate
{
    public static function template($template, $templateDrops = [])
    {
        $customer = request()->user();
        CurrentView::$currentPath = request()->path();
        CurrentView::$fullUrl = request()->fullUrl();
        CurrentView::$queryString = request()->query();

        $globalDrops = [
            'page_description' => new PageDescription,
            'page_title'       => new PageTitle,
            'country_option_tags' => new CountryOptionTags,
            'current_tags'     => new CurrentTags,
            'current_page'     => new CurrentPage,
            'all_products'     => new AllProducts,
            'articles'         => new Articles,
            'blogs'            => new Blogs,
            'collections'      => new Collections,
            'linklists'        => new Linklists,
            'menus'            => new Linklists,
            'pages'            => new Pages,
            'canonical_url'    => shop_url('/store'),
            'cart'             => $customer ? new Cart($customer->shoppingCart()) : new Cart((new ShopUser)->shoppingCart()),
            'customer'         => $customer ? new Customer($customer) : null,
            'handle'           => new Handle,
            'shop'             => new Shop,
            'scripts'          => new Scripts,
            'settings'         => new Settings(LiquidFileSystem::getInstance()),
            'theme'            => new Theme(shop_id()),
            'template'         => new Template($template),

            'content_for_header' => new ContentForHeader,
            'additional_checkout_buttons' => null,
            'content_for_additional_checkout_buttons' => null,
        ];

        Context::setGlobalDrops(array_merge($globalDrops, $templateDrops));

        $fileSystem = LiquidFileSystem::getInstance();

        // get the rendered template content
        $content = $fileSystem->readTemplatesFile($template);
        $rendered = static::render($content);

        // extend theme, or just return template if layout is none
        if (! is_null(TagLayout::$theme)) {
            $theme = $fileSystem->readLayoutFile(TagLayout::$theme);
            return static::render($theme, ['content_for_layout' => $rendered]);
        } else {
            return $rendered;
        }
    }

    public static function snippet($snippet, $data = [])
    {
        $fileSystem = LiquidFileSystem::getInstance();

        $content = $fileSystem->readSnippetsFile($snippet);
        return static::render($content, $data);
    }

    public static function render($content, $data = [])
    {
        $template = new VendorTemplate();
        $fileSystem = LiquidFileSystem::getInstance();

        static::registerCustomTags($template);
        static::registerCustomFilters($template);
        AdditionalFilters::$fileSystem = $fileSystem;

        // set custom file system
        $template->setFileSystem($fileSystem);

        $template->parse($content);
        return $template->render($data);
    }

    /**
     * Register custom tags.
     */
    public static function registerCustomTags($template)
    {
        $template->registerTag('form', 'App\OnlineStore\LiquidTags\TagForm');
        $template->registerTag('layout', 'App\OnlineStore\LiquidTags\TagLayout');
    }

    /**
     * Register custom filters.
     */
    public static function registerCustomFilters($template)
    {
        $template->registerFilter('App\OnlineStore\LiquidFilters\ArrayFilters');
        $template->registerFilter('App\OnlineStore\LiquidFilters\ColorFilters');
        $template->registerFilter('App\OnlineStore\LiquidFilters\HtmlFilters');
        $template->registerFilter('App\OnlineStore\LiquidFilters\MathFilters');
        $template->registerFilter('App\OnlineStore\LiquidFilters\MoneyFilters');
        $template->registerFilter('App\OnlineStore\LiquidFilters\StringFilters');
        $template->registerFilter('App\OnlineStore\LiquidFilters\UrlFilters');
        $template->registerFilter('App\OnlineStore\LiquidFilters\AdditionalFilters');
    }
}
