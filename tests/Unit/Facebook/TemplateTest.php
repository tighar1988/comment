<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Facebook\Template;

class TemplateTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Template::class, new Template);
    }

    /** @test */
    public function it_returns_variants_text_with_only_colors()
    {
        $template = new Template;
        $variantsText = $template->variantsText(['red', 'blue'], []);

        $this->assertEquals('Colors: red, blue', $variantsText);
    }

    /** @test */
    public function it_returns_variants_text_with_size_and_colors()
    {
        $template = new Template;
        $variantsText = $template->variantsText(['Red', 'Blue'], ['Large', 'Medium']);

        $this->assertEquals('Colors: Red, Blue / Sizes: Large, Medium', $variantsText);
    }

    /** @test */
    public function it_returns_variants_text_with_duplicate_color()
    {
        $template = new Template;
        $variantsText = $template->variantsText(['red', 'pink', 'Pink'], []);

        $this->assertEquals('Colors: red, pink', $variantsText);
    }

    /** @test */
    public function it_returns_variants_text_with_duplicate_sizes()
    {
        $template = new Template;
        $variantsText = $template->variantsText(['red', 'pink'], ['Small', 'Medium', 'medium', 'Large']);

        $this->assertEquals('Colors: red, pink / Sizes: Small, Medium, Large', $variantsText);
    }

    /** @test*/
    public function template_does_not_contain_link()
    {
        $message = "Our system can not determine the size or color option for your comment, please re-comment with correct size and color.";
        $this->assertFalse(Template::containsLink($message));
    }

    /** @test*/
    public function template_does_contain_a_link()
    {
        $message = "Thank you for your order. Visit http://my-shop.commentsold.com/account";
        $this->assertTrue(Template::containsLink($message));
    }
}
