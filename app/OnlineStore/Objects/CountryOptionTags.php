<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class CountryOptionTags extends Drop
{
    public function __toString()
    {
        $html = '<option value"" data-provinces="[]">- Please Select --</option>';
        $html .= "<option value=\"USA\" data-provinces=\"['Alabama','Alaska','American Samoa','Arizona','Arkansas','Armed Forces America','Armed Forces Europe','Armed Forces Pacific','California','Colorado','Connecticut','Delaware','District Of Columbia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Is','Nova Scotia','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming']\">USA</option>";

        return $html;
    }
}
