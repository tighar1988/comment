<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Config;
use DB;

class DatabaseMigrateShop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:migrate-shop {shop}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run database migrations for a single shop.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_shop_database($this->argument('shop'));
        DB::setDefaultConnection('shop');
        DB::reconnect();

        $this->call('migrate', ['--database' => 'shop', '--path' => '/database/migrations-shops']);

        DB::setDefaultConnection('master');
    }
}
