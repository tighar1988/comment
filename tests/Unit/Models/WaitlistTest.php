<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Waitlist;

class WaitlistTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Waitlist::class, new Waitlist);
    }
}
