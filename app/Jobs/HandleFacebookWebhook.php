<?php

namespace App\Jobs;

use App\Facebook\FacebookMessenger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facebook\MessengerTemplates;
use App\Jobs\Traits\ReconnectShopDB;
use Log;
use Exception;
use DB;
use App\Models\Master;
use App\Models\ShopUser;
use App\Models\Post;
use App\Models\Product;
use App\Facebook\ShopperComment;
use App\Facebook\CommentHarvester;
use Redis;

class HandleFacebookWebhook implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    /**
     * The $_REQUEST array from the web request
     *
     * @var array
     */
    protected $request;

    /**
     * Create a new job instance.
     *
     * @param array $request
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        // Iterate over each entry - there may be multiple if batched
        foreach ($this->request['entry'] as $entry) {
            if (! isset($entry['changes'])) {
                continue;
            }

            $shop = Redis::connection('cache')->get("page:{$entry['id']}");
            if (empty($shop)) {
                Log::info("[facebook-webhook] Could not map entry id to shop db: " . json_encode($entry));
                continue;
            }

            $this->reconnectDB($shop);

            // Iterate over each change
            foreach ($entry['changes'] as $change) {
                if ($this->isNewComment($change)) {
                    $this->handleNewComment($change);
                } else {
                    log_error("[facebook-webhook] Webhook received unknown event for {$entry['id']}: " . json_encode($change));
                    continue;
                }
            }
        }
    }

    public function isNewComment($change)
    {
        return isset($change['field'], $change['value']['item'])
            && $change['field'] == 'feed'
            && $change['value']['item'] == 'comment';
    }

    public function handleNewComment($change)
    {
        // don't harvest deleted comments
        if ($change['value']['verb'] == 'remove') {
            return;
        }

        // make sure it's a text comment, not a sticker
        if (! isset($change['value']['message'])) {
            return;
        }

        $postId = explode('_', $change['value']['post_id'])[1];
        $commentId = explode('_', $change['value']['comment_id'])[1];

        $post = Post::where('fb_post_id', '=', $postId)->first();
        if (is_null($post)) {
            return;
        }

        $product = Product::find($post->prod_id);
        if (is_null($product)) {
            return;
        }

        $comment = ShopperComment::createFromWebhook($change['value']);

        (new CommentHarvester)->harvest($post, $product, $comment);
    }
}
