@component('mail::message', ['shopName' => $shopName, 'shopUrl' => $shopUrl])

<div style="text-align: right">
    <strong>Fulfillment Confirmation</strong><br/>
    Order #{{ $orderId }}
</div>

Hello {{ $shipName }}, <br/>

Your order has been fulfilled. <br/>
Thank you for your purchase. <br/>

@if ($shopId == 'shopentourageclothing')
Your order has been shipped from our warehouse to your local store. Please allow 3-5 business days before going to pickup.<br/>
@elseif ($shopId == 'nelliemaevip')
Your order will be ready after 5pm today! <br/>
@else
Your order is now ready for pickup! <br/>
@endif


### Order Information
@component('mail::table')
    | Product       | Price         |
    |:------------- | -------------:|
    @foreach ($orderProducts as $product)
        | {{ $product['name'] }} | ${{ amount($product['price']) }} |
    @endforeach
    | | Subtotal: ${{ amount($subtotal) }} |
    | | Shipping: ${{ amount($shipCharged) }} |
    | | Tax: ${{ amount($taxTotal) }} |
    @if ($couponDiscount)
        | | Coupon Discount: -${{ amount($couponDiscount) }} |
    @endif
    @if ($applyBalance)
        | | Account Balance: -${{ amount($applyBalance) }} |
    @endif
    | | Total: <strong>${{ amount($total) }}</strong> |
@endcomponent

If you need further assistance with your order, please contact {{ $shopEmail }}.<br/>

We hope to see you again soon.<br/>
{{ $shopName }}<br/><br/>
@endcomponent
