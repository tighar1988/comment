@extends('admin.setup.setup')

@section('section', 'Account')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/password') }}">

                    <span class="settings-info">Change your password.</span>

                    <div class="form-group @hasError('old_password')">
                        <label class="control-label">Current Password</label>
                        <input type="password" class="form-control" name="old_password" value="">
                        @error('old_password')
                    </div>

                    <div class="form-group @hasError('new_password')">
                        <label class="control-label">New Password</label>
                        <input type="password" class="form-control" name="new_password" value="{{ old('new_password') }}">
                        @error('new_password')
                    </div>

                    <div class="form-group @hasError('new_password_confirmation')">
                        <label class="control-label">Repeat New Password</label>
                        <input type="password" class="form-control" name="new_password_confirmation" value="{{ old('new_password_confirmation') }}">
                        @error('new_password_confirmation')
                    </div>

                    <button class="btn btn-success" type="submit">Update Password</button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <span class="settings-info">Update Card
                    @if (! empty($user->card_brand))
                        - <span class="label label-warning">{{ $user->card_brand }} {{ $user->card_last_four }}</span>
                    @endif
                </span>

                <small class="settings-muted text-muted">This card will be used for billing PayPal commission fees.</small><br/>

                <form method="POST" action="/admin/setup/update-card" accept-charset="UTF-8" id="billing-form">

                    <div class="form-group">
                        <label for="cc-number">Credit Card Number</label>
                        <input type="text" id="cc-number" class="form-control" data-stripe="number" required="">
                    </div>

                    <div class="form-group expiration-form-group form-inline">
                        <label>Expiration Date</label>
                        <select class="cc-expiration-month form-control" data-stripe="exp-month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>

                        <select class="cc-expiration-year form-control" data-stripe="exp_year">
                            <?php $year = date('Y'); ?>
                            @for ($i = 0; $i < 16; $i++)
                                <option value="{{$year+$i}}">{{$year+$i}}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="cvv">CVV Number</label>
                        <input type="text" id="cvv" placeholder="" class="form-control" data-stripe="cvc" required="">
                    </div>

                    <div class="card-errors" style="display:none;"></div>

                    <div class="form-group">
                        <button type="submit" id="submit-btn" class="btn btn-success btn-lg" data-loading-text="Updating...">Update Card</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    Stripe.setPublishableKey('{{ config('services.stripe.key') }}');
    $(function() {
        var $form = $('#billing-form');
        $form.submit(function(event) {
            $('#submit-btn').button('loading');
            Stripe.card.createToken($form, stripeResponseHandler);
            return false;
        });
    });

    function stripeResponseHandler(status, response) {
        var $form = $('#billing-form');

        if (response.error) {
            $form.find('.card-errors').text(response.error.message).show();
            $('#submit-btn').button('reset');

        } else {
            var token = response.id;
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            $form.get(0).submit();
        }
    };
</script>
@endsection
