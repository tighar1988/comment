<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\CommentSoldJob;

class DispatchMainJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commentsold:dispatch-main-job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch the job for the main commentsold database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Dispatching main job for the commentsold database..");

        dispatch(new CommentSoldJob);

        $this->info("Commentsold main database job dispatched.");
    }
}
