<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Waitlist;

class WaitlistRemoveFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // make sure the user can only delete items from his own waitlist
        $item = Waitlist::find($this->item_id);

        return $item && $this->user()->id == $item->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id' => 'required|numeric',
        ];
    }
}
