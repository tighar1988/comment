<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
use DB;

class ScanLog extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scan_log';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'barcode',
        'item_id',
        'item_type',
        'scan_date',
        'picker_user_id',
        'result',
    ];

    public static function log($user, $barcode)
    {
        $itemId = trim(explode('-', $barcode)[1] ?? null);
        if (! is_numeric($itemId)) {
            $itemId = null;
        }

        if (starts_with($barcode, 'order-')) {
            $itemType = 'order';
            $message = "[{$user->name}] Scanned order #{$itemId}.";

        } elseif (starts_with($barcode, 'i-')) {
            $itemType = 'inventory';
            $message = "[{$user->name}] Scanned item with inventory id #{$itemId}.";

        } elseif (strlen($barcode) > 5) {
            $itemType = 'inventory';
            $message = "[{$user->name}] Scanned item with shopify barcode #{$barcode}.";

        } else {
            $itemType = null;
            $message = "[{$user->name}] Scanned wrong barcode.";
        }

        static::create([
            'barcode'        => $barcode,
            'item_id'        => $itemId,
            'item_type'      => $itemType,
            'scan_date'      => time(),
            'picker_user_id' => $user->id,
            'result'         => $message,
        ]);
    }

    public static function logFulfilled($user, $orderId)
    {
        static::create([
            'barcode'        => null,
            'item_id'        => $orderId,
            'item_type'      => 'order-fulfilled',
            'scan_date'      => time(),
            'picker_user_id' => $user->id,
            'result'         => "[{$user->name}] Fulfilled order.",
        ]);
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->whereBetween('scan_log.created_at', [$start, $end]);
    }

    public function scopeOrders($query)
    {
        return $query->where('item_type', '=', 'order');
    }

    /**
     * Get the fulfilled orders count for each packer.
     */
    public function scopePackers($query)
    {
        return $query->select(DB::raw('picker_user_id,
                ANY_VALUE(SUBSTR(result, 2, LOCATE("]", result)-2)) as name,
                count(item_id) as fulfilled'))
            ->where('scan_log.item_type', '=', 'order-fulfilled')
            ->groupBy('picker_user_id');
    }

    /**
     * Get the nr of fulfilled orders by the packers.
     */
    public function scopeNrFulfilled($query)
    {
        return $query->select(DB::raw('count(item_id) as count'))
            ->where('scan_log.item_type', '=', 'order-fulfilled');
    }
}
