@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Variants for (<a href="/admin/products/edit/{{ $product->id }}">{{ $product->style }}</a>)</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/products"><i class="fa fa-th-large"></i> Products</a></li>
        <li><a href="/admin/products/edit/{{ $product->id }}">{{ $product->product_name }}</a></li>
        <li class="active">Variants</li>
    </ol>
</section>

<?php $isGiftCard = $product->isGiftCard() ? true : false; ?>
@include('admin.products.quantity-modal')
@include('admin.products.add-edit-variant-modal')
<?php
    $labelsEnabled = shop_setting('labels-enabled');
    $shopifyEnabled = shopify_enabled();
    if ($shopifyEnabled && empty($product->shopify_product_id)) {
        $shopifyEnabled = false;
    }

    if (shop_id() == 'glamourfarms') {
        $shopifyEnabled = false;
    }

    $hasSalePrice = false;
    if (shop_id() == 'shop1' || shop_id() == 'cheekys' || shop_id() == 'glamourfarms') {
        $hasSalePrice = true;
    }

    $canEditBarcode = false;
    if (shop_id() == 'glamourfarms') {
        $canEditBarcode = true;
    }
?>

@if (! $shopifyEnabled)
    @foreach ($product->inventory as $item)
        @if ($item->cost > $item->price)
            <div class="row">
                <div class="top-alert alert alert-danger" style="display: table;">Warning, for one or several variants, the cost is greater than the price.</div>
            </div>
            <?php break; ?>
        @endif
    @endforeach
@endif

@if ($labelsEnabled)
    <div class="row">
        <div id="printer" class="print top-alert alert alert-warning" style="display: none; display: table;"></div>
    </div>
@endif

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @include('common.errors')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">

                    @if (! $shopifyEnabled)
                        <button type="button" style="margin-left: 15px;" class="btn pull-left btn-default btn-sm" id="add-variant-btn" title="Add Variant"><i class="fa fa-plus"></i> Add Variant</button>
                    @endif

                    <form class="form-horizontal" id="update-variants-form" role="form" method="POST" action="/admin/products/{{ $product->id }}/variants/update">
                        <div class="add-btn">
                            @if (zebra_enabled())
                                <button type="button" onclick="printZebraBarcodes()" class="print btn btn-warning btn-sm" id="zebra" style="display: none;">Print Zebra Barcodes</button>
                            @endif
                            @if ($labelsEnabled)
                                <button type="button" onclick="printAllBarcodes()" class="print btn btn-warning btn-sm" style="display: none;">Print All Barcodes</button>
                            @endif

                            @if ($isGiftCard) <input type="hidden" name="product_type" value="giftcard"> @endif
                            <button type="submit" data-loading-text="Updating Options.." class="btn btn-success btn-sm">Update Options</button>
                            @if ($shopifyEnabled)
                                <input type="hidden" name="shopify_enabled" id="shopify-enabled-input" value="1">
                            @endif
                        </div>

                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                            <div class="row" style="margin-top: 35px;">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table id="products" class="table table-striped dataTable">
                                            <thead>
                                                <tr>
                                                    <th>@if($isGiftCard) Denomination @else Color @endif</th>
                                                    <th>@if(! $isGiftCard) Size @endif</th>
                                                    <th>
                                                        @if ($isGiftCard)
                                                            Quantity <br/>
                                                            <small class="text-muted">Gift Cards have unlimited quantity!</small>
                                                        @elseif (! $shopifyEnabled)
                                                            Add/ Remove Stock
                                                        @else
                                                            Quantity
                                                        @endif
                                                    </th>
                                                    <th>Weight (in OZ)</th>
                                                    <th>Cost</th>
                                                    <th>Retail Price</th>
                                                    @if ($hasSalePrice)
                                                        <th>Sale Price</th>
                                                    @endif
                                                    <th>Location</th>
                                                    @if ($canEditBarcode)
                                                        <th>Custom Barcode</th>
                                                    @endif
                                                    <th>In Cart</th>
                                                    <th>On Waitlist</th>
                                                    <th>Sold</th>
                                                    <th>
                                                        Manage
                                                        @if (! $shopifyEnabled)
                                                            <button title="Edit All Columns" type="button" class="toggle-edit-all btn btn-default btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                        @endif
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (! $shopifyEnabled)
                                                    <tr class="edit-all-row" style="display: none;">
                                                        <td colspan="3" class="text-center">Update all rows at once.</td>
                                                        <td><input type="text" data-target="input-weight" placeholder="Weight" class="form-control edit-input input-sm" name=""></td>
                                                        <td><input type="text" data-target="input-cost" placeholder="Cost" class="form-control edit-input input-sm" name=""></td>
                                                        <td><input type="text" data-target="input-price" placeholder="Retail Price" class="form-control edit-input input-sm" name=""></td>
                                                        @if ($hasSalePrice)
                                                            <td><input type="text" data-target="input-sale-price" placeholder="Sale Price" class="form-control edit-input input-sm" name=""></td>
                                                        @endif
                                                        <td><input type="text" data-target="input-location" placeholder="Location" class="form-control edit-input input-sm" name=""></td>
                                                        <td colspan="3"></td>
                                                        @if ($canEditBarcode)
                                                            <td></td>
                                                        @endif
                                                    </tr>
                                                @endif
                                                @foreach ($product->inventory as $item)
                                                    <tr data-itemId="{{ $item->id }}">
                                                        <td class="color-td">{{ $item->color }}</td>
                                                        <td class="size-td">{{ $item->size }}</td>
                                                        @if (! $shopifyEnabled)
                                                            <td>
                                                                <div class="input-group input-group-sm" data-variantName="{{ $item->variantName() }}">
                                                                    <span class="input-group-btn">
                                                                        <button @if($isGiftCard) disabled="" @endif type="button" data-itemId="{{ $item->id }}" class="subtract-qty btn btn-default value-control" data-action="minus"><span class="glyphicon glyphicon-minus"></span></button>
                                                                    </span>
                                                                    <span class="input-group-addon qty-text" data-itemId="{{ $item->id }}">{{ $item->quantity }}</span>
                                                                    <span class="input-group-btn">
                                                                        <button @if($isGiftCard) disabled="" @endif type="button" data-itemId="{{ $item->id }}" class="add-qty btn btn-default value-control" data-action="plus"><span class="glyphicon glyphicon-plus"></span></button>
                                                                    </span>
        															<div style="display: none;" class="barcode" data-size="{{ $item->size }}" data-color="{{ $item->color }}" data-sold="{{ $item->soldCount->count ?? 0 }}" data-qty="{{ $item->quantity }}" data-sku="{{ $product->style }}" id="i-{{ $item->id }}" data-id="i-{{ $item->id }}"></div>
                                                                </div>
                                                            </td>
                                                            <td><input type="text" class="form-control input-sm input-weight" value="{{ $item->weight }}" name="w-{{ $item->id }}" /></td>
                                                        @else
                                                            <td>
                                                                {{ $item->quantity }}
                                                                <div style="display: none;" class="barcode" data-size="{{ $item->size }}" data-color="{{ $item->color }}" data-sold="{{ $item->soldCount->count ?? 0 }}" data-qty="{{ $item->quantity }}" data-sku="{{ $product->style }}" id="i-{{ $item->id }}" data-id="i-{{ $item->id }}"></div>
                                                            </td>
                                                            <td>{{ $item->weight }}</td>
                                                        @endif

                                                        <td><input type="text" class="form-control input-sm input-cost" value="{{ $item->cost }}" name="c-{{ $item->id }}" /></td>

                                                        @if ($shopifyEnabled)
                                                            <td>${{ amount($item->price) }}</td>
                                                        @else
                                                            <td><input type="text" class="form-control input-sm input-price" value="{{ $item->price }}" name="r-{{ $item->id }}" /></td>
                                                        @endif

                                                        @if ($hasSalePrice)
                                                            @if ($shopifyEnabled)
                                                                <td>${{ amount($item->sale_price) }}</td>
                                                            @else
                                                                <td><input type="text" class="form-control input-sm input-sale-price" value="{{ $item->sale_price }}" name="s-{{ $item->id }}" /></td>
                                                            @endif
                                                        @endif

                                                        <td><input type="text" class="form-control input-sm input-location" value="{{ $item->location }}" name="l-{{ $item->id }}" /></td>
                                                        @if ($canEditBarcode)
                                                            <td><input type="text" class="form-control input-sm" value="{{ $item->shopify_barcode }}" name="b-{{ $item->id }}" /></td>
                                                        @endif
                                                        <td>{{ ($item->cartCount->count ?? 0) }}</td>
                                                        <td>{{ $item->waitlistCount->count ?? 0 }}</td>
                                                        <td>{{ $item->soldCount->count ?? 0 }}</td>
                                                        <td class="text-center">
                                                            <div class="dropdown">
                                                                <a href="#" class="dropdown-toggle" id="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                    <i class="fa fa-ellipsis-h fa-lg"></i>
                                                                </a>
                                                                <ul class="dropdown-menu pull-right" aria-labelledby="options">
                                                                    <li><a href="/admin/orders?with-inventory={{ $item->id }}" class="link">Associated Orders</a></li>
                                                                    @if ($labelsEnabled)
                                                                        <li><a target="_blank" style="cursor: pointer;" onclick="printBarcode('#i-' + {{ $item->id }})" class="print link">Print Barcodes</a></li>
                                                                        <li><a target="_blank" style="cursor: pointer;" onclick="printOneBarcode('#i-' + {{ $item->id }})" class="print link">Print Tag</a></li>
                                                                    @endif
                                                                    @if (zebra_enabled())
                                                                        <li><a target="_blank" style="cursor: pointer;" onclick="printBarcode('#i-' + {{ $item->id }}, true)" class="print link">Print Barcodes (Zebra)</a></li>
                                                                        <li><a target="_blank" style="cursor: pointer;" onclick="printOneBarcode('#i-' + {{ $item->id }}, true)" class="print link">Print Tag (Zebra)</a></li>
                                                                    @endif
                                                                    @if (! $shopifyEnabled)
                                                                        <li><a href="#" class="edit-variant link">Edit Variant</a></li>
                                                                        <li><a data-title="Delete {{ $item->variantName() }}" href="/admin/variants/delete/{{ $item->id }}" onclick="return confirm_delete(this);">Delete Variant</a></li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        $('form.ajax-form').submit(function(e) {
            e.preventDefault();
            var $form = this;
            $('button[type="submit"]', $($form)).button('loading');

            $.ajax({
                type: $($form).attr('method'),
                url: $($form).attr('action'),
                data: $($form).serialize(),
                success: function(data) {
                    window.location.reload();
                },
                error: function(data) {
                    $('button[type="submit"]', $($form)).button('reset');
                    try {
                        if (data.responseJSON) {
                            for (var property in data.responseJSON) {
                                if (! data.responseJSON.hasOwnProperty(property)) {
                                    continue;
                                }

                                if (! data.responseJSON[property] instanceof Array) {
                                    continue;
                                }

                                var formGroup = $('input[name="'+property+'"]', $($form)).closest('.form-group');
                                $(formGroup).addClass('has-error');
                                $(formGroup).find('.help-block').remove();
                                $(formGroup).append('<span class="help-block"><strong>'+data.responseJSON[property][0]+'</strong></span>');
                            }
                        }
                    } catch (e) {}
                }
            });

            return false;
        });

        $('.toggle-edit-all').on('click', function() {
            $('.edit-all-row').toggle();
        });

        $('.edit-input').keyup(function() {
            var target = '.' + $(this).attr('data-target');
            $(target).val($(this).val());
        });

        $('#add-variant-btn').on('click', function() {
            $('#add-variant-modal').modal();
        });

        $('.edit-variant').on('click', function() {
            var $row = $(this).closest('tr');
            var color = $('.color-td', $row).text();
            var size = $('.size-td', $row).text();

            $('#edit-variant-modal').modal();
            $('#edit-variant-modal input[name="color"]').val(color);
            $('#edit-variant-modal input[name="size"]').val(size);
            $('#edit-variant-modal form').attr('action', '/admin/variants/update/'+$($row).attr('data-itemId'));
        });

        $('#quantity-modal-add, #quantity-modal-subtract').on('shown.bs.modal', function () {
            $('.quantity').focus();
        })

        $('.add-qty').on('click', function() {
            $('#quantity-modal-add').modal();
            $('#quantity-modal-add .quantity').val('');
            $('#quantity-modal-add .quantity').attr('data-itemId', $(this).attr('data-itemId'));
            $('#quantity-modal-add .variant-name').text($(this).closest('.input-group').attr('data-variantName'));
            $('#quantity-modal-add form').attr('action', '/admin/variants/'+$(this).attr('data-itemId')+'/add-quantity');
            $('#quantity-modal-add form').attr('item', $(this).attr('data-itemId'));
            $('#quantity-modal-add .form-group').removeClass('has-error');
            $('#quantity-modal-add .help-block').hide().html('');
        });

        $('#quantity-modal-add form').submit(function(event) {
            var $form = $(this);
            $('button[type=submit]', $form).button('loading');
            $('.qty-text').tooltip('destroy');

            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: {quantity: $('.quantity', $form).val() },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $('#quantity-modal-add').modal('hide');
                        var itemId = $('.quantity', $form).attr('data-itemId');

                        $('.qty-text[data-itemId='+itemId+']').text(data.quantity).tooltip({
                            trigger: 'manual',
                            title: 'Quantity Added!'
                        }).tooltip('show');

                    } else {
                        $('.form-group', $form).addClass('has-error');
                        $('.help-block', $form).show().html('<strong>Sorry, something went wrong</strong>');
                    }
                    $('button[type=submit]', $form).button('reset');
                },
                error: function(data) {
                    var message = 'An error occurred.';
                    try { message = data.responseJSON.quantity[0]; } catch (e) {}
                    $('.form-group', $form).addClass('has-error');
                    $('.help-block', $form).show().html('<strong>'+message+'</strong>');
                    $('button[type=submit]', $form).button('reset');
                }
            });

            event.preventDefault();
            return false;
        });

        $('.subtract-qty').on('click', function() {
            $('#quantity-modal-subtract').modal();
            $('#quantity-modal-subtract .quantity').val('');
            $('#quantity-modal-subtract .quantity').attr('data-itemId', $(this).attr('data-itemId'));
            $('#quantity-modal-subtract .variant-name').text($(this).closest('.input-group').attr('data-variantName'));
            $('#quantity-modal-subtract form').attr('action', '/admin/variants/'+$(this).attr('data-itemId')+'/subtract-quantity');
            $('#quantity-modal-subtract .form-group').removeClass('has-error');
            $('#quantity-modal-subtract .help-block').hide().html('');
        });

        $('#quantity-modal-subtract form').submit(function(event) {
            var $form = $(this);
            $('button[type=submit]', $form).button('loading');
            $('.qty-text').tooltip('destroy');

            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: {quantity: $('.quantity', $form).val() },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $('#quantity-modal-subtract').modal('hide');
                        var itemId = $('.quantity', $form).attr('data-itemId');

                        $('.qty-text[data-itemId='+itemId+']').text(data.quantity).tooltip({
                            trigger: 'manual',
                            title: 'Quantity Subtracted!'
                        }).tooltip('show');

                    } else {
                        $('.form-group', $form).addClass('has-error');
                        $('.help-block', $form).show().html('<strong>Sorry, something went wrong</strong>');
                    }
                    $('button[type=submit]', $form).button('reset');
                },
                error: function(data) {
                    var message = 'An error occurred.';
                    try { message = data.responseJSON.quantity[0]; } catch (e) {}
                    $('.form-group', $form).addClass('has-error');
                    $('.help-block', $form).show().html('<strong>'+message+'</strong>');
                    $('button[type=submit]', $form).button('reset');
                }
            });

            event.preventDefault();
            return false;
        });

        $('#update-variants-form').submit(function(event) {
            var $form = $(this);
            $('button[type=submit]', $form).button('loading');

            // php has a max input fields limit
            if ($('input', $form).length > 900) {
                var values = {};
                $.each($form.serializeArray(), function(i, field) {
                    values[field.name] = field.value;
                });

                $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    data: {values: JSON.stringify(values), is_json: true, shopify_enabled: $('#shopify-enabled-input').val() },
                    dataType: 'json',
                    success: function(data) {
                        window.location.reload();
                    },
                    error: function(data) {
                        alert('An unexpected error has occurred. Please refresh and try again!');
                    }
                });

                event.preventDefault();
                return false;
            }

            return true;
        });

    });
</script>

@if ($labelsEnabled)
    {{-- We have to add this to even see if they have a DYMO printer --}}
    <script type="text/javascript" src="{{ cdn('/js/DYMO.Label.Framework.2.0.2.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('/js/BrowserPrint-1.0.4.min.js') }}"></script>
    {{-- Not using CDN for the commentsold code because we have so much to change --}}
    @if (shop_id() == 'cheekys')
        <script type="text/javascript" src="/js/commentsold-zebra-cheekys-0.1.js"></script>
        @include('admin.dymo.dymoinit')
    @elseif (shop_id() == 'divas')
        <script type="text/javascript" src="/js/commentsold-zebra-divas-0.1.js"></script>
        @include('admin.dymo.dymoinit-divas')
    @elseif (shop_id() == 'fashionten' || shop_id() == 'glamourfarms' || shop_id() == 'fashion15' || shop_id() == 'shopbhb')
        <script type="text/javascript" src="/js/commentsold-zebra-divas-0.1.js"></script>
        @include('admin.dymo.dymoinit')
    @else
        <script type="text/javascript" src="/js/commentsold-zebra-0.9.js"></script>
        @include('admin.dymo.dymoinit')
    @endif
@endif

@endsection
