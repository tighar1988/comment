<?php

namespace App\Shop;

use DB;
use Exception;

class DatabaseCreator
{
    protected $invalidSchemas = [
        'sys',
        'mysql',
        'information_schema',
        'performance_schema',
        'test',
        'www',
        'dev',
        'stage',
        'status',
        'api',
        'api-cdn',
        'developer',
        'developers',
        'cdn',
        'app',
        'help',
        'helpme',
        'ajax',
        'commentsold',
    ];

    public function create($name)
    {
        $name = schema_name($name);
        $this->validateName($name);
        DB::connection()->statement("CREATE DATABASE {$name}");
    }

    protected function validateName($name)
    {
        if (in_array($name, $this->invalidSchemas)) {
            throw new Exception("Schema not allowed.");
        }
    }
}
