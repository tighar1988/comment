<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('state')->nullable();
            $table->string('zip_code');
            $table->string('tax_region_name')->nullable();
            $table->string('state_rate')->nullable();
            $table->string('estimated_combined_rate')->nullable();
            $table->string('estimated_county_rate')->nullable();
            $table->string('estimated_city_rate')->nullable();
            $table->string('estimated_special_rate')->nullable();
            $table->string('risk_level')->nullable();
            $table->timestamps();

            $table->index(['zip_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tax_rates');
    }
}
