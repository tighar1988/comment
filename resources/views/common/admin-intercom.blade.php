@if (! session()->has('logged-as-user'))
<script>
    window.intercomSettings = {
        app_id: "c3j24v96"
    };

    @if (Auth::user())
        window.intercomSettings.name = "{{ Auth::user()->name }}"; // Full name
        window.intercomSettings.email = "{{ Auth::user()->email }}"; // Email address
        window.intercomSettings.created_at = "{{ strtotime(Auth::user()->created_at) }}"; // Signup date as a Unix timestamp
        window.intercomSettings.boutique = '{{ Auth::user()->shop_name }}';
        window.intercomSettings.superadmin = '{{ Auth::user()->superadmin ? 'true' : 'false' }}';
        window.intercomSettings.user_ip = "{{ request()->ip() }}";
        @if (! empty(Auth::user()->shop_name) && ! empty(shop_id()))
            window.intercomSettings.billing_plan = "{{ shop_setting('shop.fees-billing-plan') ? shop_setting('shop.fees-billing-plan') : 'N/A' }}";
            window.intercomSettings.facebook_setup = '{{ shop_setting('facebook.access_token') ? 'true' : 'false' }}';
            window.intercomSettings.paypal_setup = '{{ (shop_setting('paypal.client_id') || shop_setting('paypal.identity.email')) ? 'true' : 'false' }}';
            window.intercomSettings.stripe_setup = '{{ shop_setting('stripe.access_token') ? 'true' : 'false' }}';
            window.intercomSettings.shopify_setup = '{{ shop_setting('shopify.access_token') ? 'true' : 'false' }}';
            window.intercomSettings.hear_about_us = '{{ Auth::user()->hear_about_us ? Auth::user()->hear_about_us : 'N/A' }}';
            {{--
                Todo: This may delay page loading, so we can change this to a cron job later XXX. Once we have indexes, the count should be very fast. We'll find out when we profile the site how long this takes, until then, it's useful for intercom campaigns.
            --}}
            window.intercomSettings.first_fulfilled_at = '{{ app('App\Models\Order')->firstFulfilledAt() }}';
            window.intercomSettings.total_orders = '{{ app('App\Models\Order')->fulfilledCount() }}';
        @endif
    @endif
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/c3j24v96';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
@endif
