<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                {{-- <th>Image</th> --}}
                <th>Order ID</th>
                <th>Total</th>
                <th>Subtotal</th>
                <th>Shipping Cost</th>
                <th>Coupon Discount</th>
                <th>Balance Used</th>
                <th>Order Date</th>
            </tr>
        </thead>
        <tbody>
            @if ($orders->isEmpty())
                <tr><td colspan="6">No results found.</td></tr>
            @endif
            @foreach ($orders as $order)
                <tr>
                    {{-- <td><img width="45px" height="45px" src="{{ product_thumb($order->image) }}" alt="{{ $order->name }}"></td> --}}
                    <td><a href="/admin/orders?order={{ $order->id }}&scanned=false">#{{ $order->id }}</a></td>
                    <td>${{ amount($order->total) }}</td>
                    <td>${{ amount($order->subtotal) }}</td>
                    <td>{{ $order->ship_charged == 0 ? 'Free Shipping!' : '$'.$order->ship_charged }}</td>
                    <td>${{ amount($order->coupon_discount) }}</td>
                    <td>${{ amount($order->apply_balance) }}</td>
                    <td>{{ style_date($order->created_at) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
