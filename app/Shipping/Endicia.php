<?php

namespace App\Shipping;

use App\Models\User;
use Exception;
use Log;

class Endicia
{
    protected $requesterId;

    protected $accountId;

    protected $passPhrase;

    protected $endpoint = 'https://labelserver.endicia.com/LabelService/EwsLabelService.asmx';

    protected $shopOwner;

    protected $ratePlan;

    protected $labelFee;

    protected $processingFee;

    protected $bypassValidation = false;

    public function __construct(User $shopOwner)
    {
        $this->requesterId = config('services.endicia.requester_id');
        $this->accountId = config('services.endicia.account_id');
        $this->passPhrase = config('services.endicia.pass_phrase');

        $this->ratePlan = shop_setting('shipping.rateplan', 'Retail');
        $this->labelFee = shop_setting('shipping.label-fee', 0.05);
        $this->processingFee = shop_setting('shipping.processing_fee', 0.03);

        $this->shopOwner = $shopOwner;

        // if (env('APP_ENV') == 'testing') {
        //     $this->endpoint = 'https://elstestserver.endicia.com/LabelService/EwsLabelService.asmx';
        // }
    }

    public function getRate($order, $dimensions)
    {
        $service = $this->getServiceLevel($dimensions);

        $pricing = $this->ratePlan;
        if ($service == 'First' && $pricing == 'CommercialPlus') {
            // Commercial Plus pricing is not offered for First-Class Mail
            $pricing = 'CommercialBase';
        }

        $xml = '<PostageRateRequest>
                    <RequesterID>'.$this->requesterId.'</RequesterID>
                    <CertifiedIntermediary>
                        <AccountID>'.$this->accountId.'</AccountID>
                        <PassPhrase>'.$this->passPhrase.'</PassPhrase>
                    </CertifiedIntermediary>
                    <POZipCode>35805</POZipCode>
                    <MailClass>'.$service.'</MailClass>
                    <DateAdvance>0</DateAdvance>
                    <Pricing>'.$pricing.'</Pricing>
                    <WeightOz>'.$dimensions['weight'].'</WeightOz>
                    <MailpieceShape>'.$this->mailPieceShape($service).'</MailpieceShape>
                    '.$this->mailPieceDimensions($dimensions).'
                    <Stealth>TRUE</Stealth>
                    <Value>0</Value>
                    <Description>Order</Description>
                    <PartnerCustomerID>111</PartnerCustomerID>
                    <PartnerTransactionID>'.$order->id.'</PartnerTransactionID>
                    '.$this->toAddress($order).'
                    '.$this->fromAddress().'
                    <DeliveryConfirmation>ON</DeliveryConfirmation>
                    <Services InsuredMail="OFF" SignatureConfirmation="OFF" DeliveryConfirmation="ON" />
                    '.$this->getValidateAddress().'
                </PostageRateRequest>';

        log_info(print_r($xml, true));
        return $this->callAPI($xml, 'CalculatePostageRateXML', 'postageRateRequestXML');
    }

    public function buyLabelFor($order, $dimensions)
    {
        if (isset($dimensions['bypass_validation']) && $dimensions['bypass_validation']) {
            $this->bypassValidation = true;
        }

        $response = $this->getRate($order, $dimensions);

        $error = $this->getValue('ErrorMessage', $response);
        if (! is_null($error)) {
            return ['errors' => [$error]];
        }

        $rate = $this->getValue('Rate', $response);
        if (! is_numeric($rate)) {
            return ['errors' => ['An error occured. The rate could not be calculated.']];
        }

        $surcharge = $this->surcharge($rate);

        return [
            'object_id'         => 0,
            'amount'            => floatval($surcharge),
            'provider'          => 'USPS',
            'servicelevel_name' => $this->getValue('MailService', $response),
        ];
    }

    public function confirmBuyLabel($order, $dimensions)
    {
        if (isset($dimensions['bypass_validation']) && $dimensions['bypass_validation']) {
            $this->bypassValidation = true;
        }

        // make an API call to get the customer's rate
        $rate = $this->getValue('Rate', $this->getRate($order, $dimensions));
        if (! is_numeric($rate)) {
            return ['errors' => ['An error occured while calculating the rate.']];
        }

        $surcharge = $this->surcharge($rate);

        if (isset($dimensions['auto_charge']) && $dimensions['auto_charge']) {
            $this->shopOwner->autoBuyPrepaidCredit($surcharge);
        }

        if ($this->shopOwner->getPrepaidCredit() < $surcharge) {
            return ['errors' => ["You don't have enough prepaid credit to buy the label (\$".amount($surcharge).")."]];
        }

        $service = $this->getServiceLevel($dimensions);
        $test = env('APP_ENV') == 'testing' ? 'YES' : 'NO';

        $xml = '<LabelRequest Test="'.($test).'" LabelType="Default" LabelSize="4X6" ImageFormat="GIF">
                    <RequesterID>'.$this->requesterId.'</RequesterID>
                    <AccountID>'.$this->accountId.'</AccountID>
                    <PassPhrase>'.$this->passPhrase.'</PassPhrase>
                    <MailClass>'.$service.'</MailClass>
                    <DateAdvance>0</DateAdvance>
                    <WeightOz>'.$dimensions['weight'].'</WeightOz>
                    <MailpieceShape>'.$this->mailPieceShape($service).'</MailpieceShape>
                    '.$this->mailPieceDimensions($dimensions).'
                    <Stealth>TRUE</Stealth>
                    <Services InsuredMail="OFF" SignatureConfirmation="OFF" />
                    <Value>0</Value>
                    <Description>Order</Description>
                    <PartnerCustomerID>111</PartnerCustomerID>
                    <PartnerTransactionID>'.$order->id.'</PartnerTransactionID>
                    '.$this->toAddress($order).'
                    '.$this->fromAddress().'
                    <ResponseOptions PostagePrice="TRUE"/>
                    '.$this->getValidateAddress().'
                </LabelRequest>';

        $response = $this->callAPI($xml, 'GetPostageLabelXML', 'labelRequestXML');

        $error = $this->getValue('ErrorMessage', $response);
        if (! is_null($error)) {
            return ['errors' => [$error]];
        }

        $csShipCharged = floatval($this->getValue('FinalPostage', $response));
        return [
            'image'            => $this->getValue('Base64LabelImage', $response),
            'tracking_number'  => $this->getValue('TrackingNumber', $response),
            'ship_cost'        => $surcharge,
            'cs_ship_charged'  => $csShipCharged,
            'details' => [
                'ship_cost'        => $surcharge,
                'cs_ship_charged'  => $csShipCharged,
                'pic'              => $this->getValue('PIC', $response),
            ],
        ];
    }

    public function refundLabel($order)
    {
        $pic = $order->details['pic'] ?? null;

        $xml = '<RefundRequest>
                    <RequesterID>'.$this->requesterId.'</RequesterID>
                    <RequestID>'.$order->id.'</RequestID>
                    <CertifiedIntermediary>
                        <AccountID>'.$this->accountId.'</AccountID>
                        <PassPhrase>'.$this->passPhrase.'</PassPhrase>
                    </CertifiedIntermediary>
                    <PicNumbers>
                        <PicNumber>'.$pic.'</PicNumber>
                    </PicNumbers>
                </RefundRequest>';

        $response = $this->callAPI($xml, 'GetRefundXML', 'refundRequestXML');

        $status = $this->getValue('RefundStatus', $response);
        if ($status == 'Approved') {
            return true;
        }

        return false;
    }

    /**
     * Add the label fee + the 3% Stripe charges so we don't loose money.
     *
     * Formula for adding 3% back: "surcharge - 3/100 * surcharge = rate"
     * Which is equivalent to: "surcharge = rate / 0.97"
     *
     * @param  float $rate
     * @return float
     */
    public function surcharge($rate)
    {
        $rate = abs(floatval($rate));
        $labelFee = abs(floatval($this->labelFee));
        $processingFee = abs(floatval($this->processingFee));

        // add 3% back
        //$surcharge = $rate / 0.97;
        $surcharge = $rate / (1 - $processingFee);

        // always round up; don't use php's round() because it rounds down sometimes
        $surcharge = (ceil($surcharge * 100) / 100);

        // add label fee
        $surcharge = $surcharge + $labelFee;

        return floatval($surcharge);
    }

    /**
     * "First Class" for less than 15.99 ounces, "Priority" otherwise.
     *
     * @param  array $dimensions
     * @return string
     */
    public function getServiceLevel($dimensions)
    {
        $ounces = $dimensions['weight'] ?? 0;

        if ($ounces <= 15.99) {
            return 'First';
        }

        return 'Priority';
    }

    /**
     * Return the mail pice shape.
     *
     * @param  string $service
     * @return string
     */
    public function mailPieceShape($service)
    {
        if ($service == 'Priority' && shop_id() == 'taralynns') {
            return 'FlatRateEnvelope';
        }

        return 'Parcel';
    }

    /**
     * Return the mail piece dimensions.
     *
     * @param  array $dimensions
     * @return string
     */
    public function mailPieceDimensions($dimensions)
    {
        return '<MailpieceDimensions>
                    <Length>'.$dimensions['length'].'</Length>
                    <Width>'.$dimensions['width'].'</Width>
                    <Height>'.$dimensions['height'].'</Height>
                </MailpieceDimensions>';
    }

    /**
     * Shop from address.
     *
     * @return string
     */
    public function fromAddress()
    {
        return
            '<FromName>'.shop_setting('shop.from_name').'</FromName>
            <ReturnAddress1>'.shop_setting('shop.street_address').'</ReturnAddress1>
            <FromCity>'.shop_setting('shop.city').'</FromCity>
            <FromState>'.shop_setting('shop.state').'</FromState>
            <FromPostalCode>'.shop_setting('shop.zip_code').'</FromPostalCode>
            <FromPhone>'.$this->getShopPhone().'</FromPhone>';
    }

    /**
     * Return the shop's phone number.
     *
     * @return string
     */
    public function getShopPhone()
    {
        $phone = shop_setting('shop.phone');
        $phone = str_replace(['+', '.', '(', ')', '-'], '', $phone);
        $phone = trim($phone);

        return $phone;
    }

    public function getValidateAddress()
    {
        if ($this->bypassValidation) {
            return '<ValidateAddress>FALSE</ValidateAddress>';
        }

        return '';
    }

    /**
     * Return the to address.
     *
     * @param  \App\Models\Order $order
     * @return string
     */
    public function toAddress($order)
    {
        return
            '<ToName>'.$order->ship_name.'</ToName>
            <ToAddress1>'.$order->street_address.'</ToAddress1>
            <ToAddress2>'.$order->apartment.'</ToAddress2>
            <ToCity>'.$order->city.'</ToCity>
            <ToState>'.$order->state.'</ToState>
            <ToPostalCode>'.$order->zip.'</ToPostalCode>
            <ToPhone>6666111111</ToPhone>';
    }

    /**
     * Call the Endicia API.
     *
     * @param  string $xml
     * @param  string $method
     * @param  string $field
     * @return string
     */
    public function callAPI($xml, $method, $field)
    {
        $request = $field.'='.$xml;
        $header = "POST /LabelService/EwsLabelService.asmx/".$method." HTTP/1.1\r\n
            MIME-Version: 1.0\r\n
            Content-type: application/x-www-form-urlencoded
            Content-length: ".strlen($request)."\r\n
            Connection: close \r\n\r\n
            ".$field."=".$xml;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint . '/'.$method);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        $response = curl_exec($ch);
        curl_close($ch);

        if (str_contains($response, ['<soap:Fault>'])) {
            throw new Exception($response);
        }

        return $response;
    }

    /**
     * Parse the xml returend from the api and extract values.
     *
     * @param  string $property
     * @param  string $xml
     * @return string|null
     */
    public function getValue($property, $xml)
    {
        $tmp = explode("<{$property}>", $xml);
        if (! isset($tmp[1])) {
            return null;
        }

        $tmp = explode("</{$property}>", $tmp[1]);
        if (! isset($tmp[0])) {
            return null;
        }

        return trim($tmp[0]);
    }
}
