//07-11-2016
var productVariantSize="";
var productSizeColor="";
var productVariantColor="";
var productPrice="";
var colorContainerArray=[];
jQuery(".myClassSingle").click(function(){
  console.log(jQuery(this).val());
  var productSizeSingleTemp=jQuery(this).val();
  var productSizeSingleAvailable=0;
//   if(jQuery("ul.colorVarientSingle li").length>0)
//   {
//     jQuery("#productSelect option").each(function(index,element){
//       jQuery(this).removeAttr('selected');
//     });
  if(!jQuery(this).hasClass("disabled"))
  {
    jQuery(".productColorAndSizeVariantSingle > option").each(function(index,element){
      if((jQuery(this).attr("data-size")==productSizeSingleTemp) && (jQuery(this).attr("data-available")))
      {
        console.log("get");
        productVariantSize=jQuery(this).attr("data-size");
        productSizeSingleAvailable=1;
        return false;
      }
    });
    if(productSizeSingleAvailable==1)
    {
      if(jQuery("ul.colorVarientSingle li").length>0)
      {
        	//remove all selected attribute from productSelect option
           jQuery("#productSelect option").each(function(index,element){
              jQuery(this).removeAttr('selected');
            });
          colorContainerArray=[];
          jQuery(".productQuickViewSingle").each(function(index,element){
            jQuery(element).removeClass("myElementClass");
          });
          jQuery("label[for='"+jQuery(this).val()+"']").addClass("myElementClass");
          count=0;

          //remove all selected and not selected class from colorVarientSingle option
          jQuery("ul.colorVarientSingle li").each(function(index,element){
            if(jQuery(this).find("a").hasClass("selected"))
            {
              jQuery(this).find("a").removeClass("selected");
            }
            else if(jQuery(this).find("a").hasClass("not-selected"))
            {
              jQuery(this).find("a").removeClass("not-selected");
            }
          });

          jQuery(".productColorAndSizeVariantSingle  option").each(function(index,element){
            if(jQuery(this).attr("data-size")==productVariantSize)
            {
              if(jQuery(this).attr("data-available")==="true")
              {
                if(count==0)
                {
                  console.log("add class selected = "+jQuery(this).attr("data-color"));
                  jQuery("ul.colorVarientSingle li[data-color='" + jQuery(this).attr("data-color") + "']").find("a").addClass("selected");
                  selectedColor=jQuery(this).attr("data-color");
                  productVariantColor=selectedColor;
                  productPrice=jQuery(this).attr("data-price")/100;
                  count++;
                }
                colorContainerArray.push(jQuery(this).attr("data-color"));

              }
            }
          });

          jQuery("ul.colorVarientSingle li").each(function(index,element){
            if(jQuery.inArray( jQuery(this).attr("data-color"), colorContainerArray )==-1)
            {
              jQuery(this).find("a").addClass("not-selected");
            }
          });
          productSizeColor=productVariantSize+" / "+productVariantColor;

          //set product size color combination
          jQuery("#productSelect option").each(function(index,element){
            console.log(jQuery(this).text());
            productSizeColor=productVariantSize+" / "+productVariantColor;
            var dataSku="";
            var dataValue="";
            var targetHtml="";
            if(jQuery(this).text() == productSizeColor)
            {
              dataSku=jQuery(this).attr("data-sku");
              dataValue=jQuery(this).val();
              targetHtml='<option data-sku="'+dataSku+'" value="'+dataValue+'" selected>'+productSizeColor+'</option>';
              jQuery(this).replaceWith(targetHtml);

            }
        });
          //set price
          jQuery("#ProductPrice").html("$ "+productPrice);
          jQuery("#productSelect option").trigger("change");
      }
      else
      {
        //remove all selected attribute from productSelect option
        jQuery("#productSelect option").each(function(index,element){
          jQuery(this).removeAttr('selected');
        });
        jQuery(".productQuickViewSingle").each(function(index,element){
          jQuery(element).removeClass("myElementClass");
        });
        jQuery("label[for='"+jQuery(this).val()+"']").addClass("myElementClass");
        //set product size color combination
          jQuery("#productSelect option").each(function(index,element){
            console.log(jQuery(this).text());
            productSizeColor=productVariantSize;
            var dataSku="";
            var dataValue="";
            var targetHtml="";
            if(jQuery(this).text() == productVariantSize)
            {
              dataSku=jQuery(this).attr("data-sku");
              dataValue=jQuery(this).val();
              targetHtml='<option data-sku="'+dataSku+'" value="'+dataValue+'" selected>'+productSizeColor+'</option>';
              jQuery(this).replaceWith(targetHtml);

            }
        });
        jQuery(".productColorAndSizeVariantSingle  option").each(function(index,element){
          if(jQuery(this).attr("data-size")==productVariantSize)
          {
            if(jQuery(this).attr("data-available")==="true")
            {
              productPrice=jQuery(this).attr("data-price")/100;
              count++;

            }
          }
        });
        jQuery("#ProductPrice").html("$ "+productPrice);
        jQuery("#productSelect option").trigger("change");
      }
    }
  }
//   }
});

//set color
jQuery("ul.colorVarientSingle li").click(function(){
  console.log(jQuery(this).attr("data-color"));
  var productVariantColorTemp=jQuery(this).attr("data-color");
//   productVariantColor=jQuery(this).attr("data-color");
  jQuery(".productQuickViewSingle").each(function(index,element){
    if(jQuery(this).hasClass("myElementClass"))
    {
      productVariantSize=jQuery(this).text();
    }
  });
  console.log(productVariantSize);
  jQuery(".productColorAndSizeVariantSingle option").each(function(index,element){
    if((jQuery(this).attr("data-size")==productVariantSize) && (jQuery(this).attr("data-color")==productVariantColorTemp) && (jQuery(this).attr("data-available")))
    {
      productVariantColor=productVariantColorTemp;
      console.log(jQuery(this));
      jQuery("#productSelect option").each(function(index,element){
        jQuery(this).removeAttr('selected');
      });
      productSizeColor=productVariantSize+" / "+productVariantColor;
      
      productPrice=jQuery(this).attr("data-price")/100;
      jQuery("#ProductPrice").html("$ "+productPrice);
      jQuery("ul.colorVarientSingle li").each(function(index,element){
        if(jQuery(this).find("a").hasClass("selected"))
        {
          jQuery(this).find("a").removeClass("selected");
        }
        else if(jQuery(this).find("a").hasClass("not-selected"))
        {
          jQuery(this).find("a").removeClass("not-selected");
        }
      });
  	  colorContainerArray=[];
      jQuery(".productColorAndSizeVariantSingle  option").each(function(index,element){
        if(jQuery(this).attr("data-size")==productVariantSize)
        {
          if(jQuery(this).attr("data-available")==="true")
          {
            if(jQuery(this).attr("data-color")==productVariantColor)
            {
              console.log("add class selected = "+jQuery(this).attr("data-color"));
              jQuery("ul.colorVarientSingle li[data-color='" + jQuery(this).attr("data-color") + "']").find("a").addClass("selected");

            }
            colorContainerArray.push(jQuery(this).attr("data-color"));

          }
          //             colorContainerArray.push(jQuery(this).attr("data-color"));
        }
      });
      jQuery("ul.colorVarientSingle li").each(function(index,element){
        if(jQuery.inArray( jQuery(this).attr("data-color"), colorContainerArray )==-1)
        {
          jQuery(this).find("a").addClass("not-selected");
        }
      });
      console.log(productVariantSize);
      console.log(productSizeColor);
      
      
    }
  });
  setTimeout(function(){ 
//         alert("Hello"); 
        jQuery("#productSelect option").each(function(index,element){
        var dataSku="";
        var dataValue="";
        var targetHtml="";
        if(jQuery(this).text() == productSizeColor)
        {
//           console.log("get productSizeColor");
//           console.log(jQuery(element));
          dataSku=jQuery(this).attr("data-sku");
          dataValue=jQuery(this).val();
          targetHtml='<option selected="selected" data-sku="'+dataSku+'" value="'+dataValue+'">'+productSizeColor+'</option>';
          jQuery(this).replaceWith(targetHtml);
//           	 jQuery(this).attr("selected","selected");

        }
      });
      jQuery("#productSelect").trigger("change");
    jQuery("#ProductPrice").html("$ "+productPrice);
      }, 100)
});

//set first combination
var size=jQuery(".sizeForSingleProduct").find(".myElementClass").text();
var color="";
var sizeColor="";
if(jQuery(".colorVarientWrapper").length>0)
{
  jQuery(".colorVarientSingle li").each(function(index,element){
    if(jQuery(this).find("a").hasClass("selected"))
    {
      color=jQuery(this).attr("data-color");
    }
  });
  sizeColor=size+" / "+color;
}
else
{
  sizeColor=size;
}
jQuery("#productSelect option").each(function(index,element){
  var dataSku="";
  var dataValue="";
  var targetHtml="";
  if(jQuery(this).text() == sizeColor)
  {
    dataSku=jQuery(this).attr("data-sku");
    dataValue=jQuery(this).val();
    targetHtml='<option selected="selected" data-sku="'+dataSku+'" value="'+dataValue+'">'+sizeColor+'</option>';
    jQuery(this).replaceWith(targetHtml);
  }
});
jQuery("#productSelect").trigger("change");

//color change on hover
var productFeatureImage="";
jQuery(".variantColor").hover(
  function(event){
    var target=jQuery(this).closest("div.grid__item");
    var changeToImage=jQuery(this).closest("li").attr("data-feauture-image");
    var featureImage=jQuery(this).closest("div.grid__item").find(".grid-product-image").attr("src");
    productFeatureImage=featureImage;
    featureImage=featureImage.split('?');
    featureImage=featureImage[0].split('/');
    changeToImage=changeToImage.split('/');
    featureImage[featureImage.length-1]=changeToImage[1];
    featureImage=featureImage.join("/");
    jQuery(target).find(".grid-product-image").attr("src",featureImage);
  },
  function(){
    jQuery(this).closest("div.grid__item").find(".grid-product-image").attr("src",productFeatureImage);
  });