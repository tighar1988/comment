<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HearAboutUsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'hear_about_us' => 'required|max:300',
            'other_source'  => 'max:300',
        ];

        if ($this->hear_about_us == 'other' || $this->hear_about_us == 'from-a-friend') {
            $rules['other_source'] = 'required|max:300';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'hear_about_us.required' => 'Please choose an option.',
            'other_source.required' => 'Plese enter a value.',
        ];
    }
}
