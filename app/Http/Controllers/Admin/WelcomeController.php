<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('site.welcome');
    }

    public function contact()
    {
        return view('site.contact');
    }

    public function pricing()
    {
        return view('site.pricing');
    }
}
