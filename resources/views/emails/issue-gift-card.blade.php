Your gift card is now available!
<br/>

Hi {{ $customerName }}, here is your ${{ $giftcardValue }} gift card. <br/>
Treat yourself, or send it to someone else as a gift.

<a href="{{ $giftcardUrl }}">View your gift card</a> or <a href="{{ $shopUrl }}">Visit our store</a>
