<?php

namespace Tests\Unit\OnlineStore;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use App\OnlineStore\LiquidFilters\UrlFilters;

class UrlFiltersTest extends TestCase
{
    /** @test */
    public function asset_url()
    {
        $response = UrlFilters::asset_url('shop.css');
        $this->assertEquals('//cdn.commentsold.test/test_shop/assets/shop.css?v=1506034852', $response);
    }

    /** @test */
    public function asset_img_url()
    {
        $response = UrlFilters::asset_img_url('logo.png', '300x');
        $this->assertEquals('//cdn.commentsold.test/test_shop/assets/logo_300x.png?v=1506034852', $response);
    }

    /** @test */
    public function img_url()
    {
        $response = UrlFilters::img_url('file.png', '100x100');
        $this->assertEquals('//cdn.commentsold.test/test_shop/images/file_100x100.png?v=1506034852', $response);
    }
}
