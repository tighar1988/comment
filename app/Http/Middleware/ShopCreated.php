<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Config;
use App\Models\Master;

class ShopCreated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->shop_name) {
            // set the shop database for the shop owner
            set_shop_database(Auth::user()->shop_name);

            // if the shop has a custom domain we will use it as the base url
            $domain = Master::findDomainByShop(Auth::user()->shop_name);
            if (! empty($domain)) {
                config(['shop.custom-domain' => $domain]);
            }

        } else {
            // force the user to create a shop
            return redirect('/admin/setup/step-1');
        }

        return $next($request);
    }
}
