@foreach ($orders as $order)
    <section class="invoice-box">
        <div class="row">
            <div class="col-md-8 no-padding">
                <div class="product-list">
                    @foreach ($order->orderProducts as $item)
                        <div class="product-box">
                            <div class="product-image">
                                <img width="290px" height="290px" src="{{ product_image($item->product_filename) }}" alt="{{ $item->prod_name }}">
                            </div>
                            <div class="product-name">{{ $item->name }}</div>
                            <div class="product-price">${{ $item->price }}</div>
                            <div class="product-name">Color: {{ $item->color }}</div>
                            <div class="product-name">Size: {{ $item->size }}</div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-md-4 no-padding">
                <div class="right-side">
                    <div class="info-row">
                        @if ($order->local_pickup)
                            <span class="value align-right"><strong>Shipping: Local Pickup</strong>
                        @else
                            <span class="value align-right"><strong>Shipping to:</strong> <br/>
                                {{ $order->street_address}}, <br/>
                                {{ ($order->apartment != '') ? $order->apartment . ", " : '' }}
                                {{ $order->city }}, {{ $order->state }}, {{ $order->zip }}
                            </span>
                        @endif
                        <br/><br/>
                        <div style="clear: both;"></div>
                    </div>

                    <div class="calculation">
                        <div class="total-order">Total order</div>
                        <div class="order-value">${{ amount($order->total) }}</div>
                        <hr/>

                        <div class="item-price">Subtotal: <span>${{ amount($order->subtotal) }}</span></div>

                        <div class="item-price">Shipping price: <span>{{ $order->ship_charged == 0 ? 'Free Shipping!' : '$'.amount($order->ship_charged) }}</span></div>

                        <div class="item-price">Total taxes: <span>${{ amount($order->tax_total) }}</span></div>

                        {{-- todo: show if items were returned and order refunded --}}

                        {{--
                            todo: show apply blance if used for paid orders
                            @if ($order->apply_balance)
                                <div class="item-price">Account Credit: <span>${{ amount($order->apply_balance) }}</span></div>
                            @endif
                        --}}
                        @if ($order->coupon_discount)
                            <div class="item-price">Coupon Discount: <span>- ${{ amount($order->coupon_discount) }}</span></div>
                        @endif

                        <div style="clear: both;"></div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endforeach
