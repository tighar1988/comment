<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\GiftCard;
use Datatables;

class GiftCardController extends Controller
{
    public function index()
    {
        return view('admin.products.giftcards.giftcards-grid');
    }

    public function datatable()
    {
        $model = GiftCard::query()->select(\DB::raw("customer_giftcards.id, SUBSTRING(customer_giftcards.code, -4) as code, customer_giftcards.value, customer_giftcards.used, customer_giftcards.disabled, shop_users.name as customer_name
        "))->join('shop_users', 'shop_users.id', '=', 'customer_giftcards.customer_id');

        return Datatables::eloquent($model)
            ->addColumn('expires', '-')
            ->editColumn('value', '${{ amount($value) }}')
            ->make(true);
    }

    public function enable($giftcardId)
    {
        $giftcard = GiftCard::find($giftcardId);
        $giftcard->disabled = false;
        $giftcard->save();

        flash('GiftCard Enabled!');
        return redirect('/admin/giftcards');
    }

    public function disable($giftcardId)
    {
        $giftcard = GiftCard::find($giftcardId);
        $giftcard->disabled = true;
        $giftcard->save();

        flash('GiftCard Disabled!');
        return redirect('/admin/giftcards');
    }

    public function preview($productId)
    {
        $giftcard = Product::find($productId);
        $inventory = $giftcard->inventory;
        $denomination = $inventory->sortByDesc('color')->first()->price ?? 0;
        $shopName = shop_name();

        return view('admin.products.giftcards.preview', compact('giftcard', 'denomination', 'shopName'));
    }
}
