<?php

namespace App\BigCommerce;

use Bigcommerce\Api\Client;
use App\Models\Order;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Coupon;
use App\Models\Image as ProductImage;
use App\Mails\ShopMailer;
use Exception;

class BigCommerce
{
    protected $weightUnits;

    public function __construct()
    {
        Client::configure([
            'auth_token' => shop_setting('bigcommerce.access_token'),
            'client_id'  => shop_setting('bigcommerce.client_id'),
            'store_hash' => static::getStoreHash(shop_setting('bigcommerce.api_path')),
        ]);
    }

    public function getStore()
    {
        // currency USD
        // weight_units LBS
        // dimension_units Inches
        return Client::getStore();
    }

    public function getPages()
    {
        return Client::getPages();
    }

    public function importCoupons()
    {
        $couponCount = Client::getCouponsCount();
        $nrPages = ceil($couponCount / 100);

        for ($page = 1; $page <= $nrPages; $page++) {
            $coupons = Client::getCoupons(['limit' => 100, 'page' => $page]);
            foreach ($coupons as $coupon) {
                $this->importCoupon($coupon);
            }
        }
    }

    public function importCoupon($coupon)
    {
        //  Acceptable values for this enumerated field are: per_item_discount, per_total_discount, shipping_discount, free_shipping, or percentage_discount.
        if ($coupon->type == 'per_item_discount' || $coupon->type == 'shipping_discount') {
            // don't import these, we dont' support these types
            return;
        }

        if (! $coupon->enabled) {
            return;
        }

        if (Coupon::where('code', '=', $coupon->code)->first()) {
            return;
        }

        // create new coupon
        $c = new Coupon;
        $c->code = substr(mysql_utf8($coupon->code), 0, 30);
        $c->max_usage = is_numeric($coupon->max_uses) ? $coupon->max_uses : 0;
        $c->use_count = is_numeric($coupon->num_uses) ? $coupon->num_uses : 0;
        $c->starts_at = time();

        $c->expires_at = empty($coupon->ends_at) ? 0 : strtotime($coupon->ends_at);
        if ($c->expires_at < 0) {
            $c->expires_at = 0;
        }

        $c->amount = abs($coupon->amount);

        if ($coupon->type == 'per_total_discount') {
            $c->coupon_type = 'F';
        } elseif ($coupon->type == 'percentage_discount') {
            $c->coupon_type = 'P';
        } elseif ($coupon->type == 'free_shipping') {
            $c->coupon_type = 'S';
        }

        $onlyOnce = false;
        if ($coupon->max_uses_per_customer == 1) {
            $onlyOnce = true;
        }

        $c->options = [
            'description' => '[BigCommerce] ' . $coupon->name,
            'onlyOnce'    => (bool) $onlyOnce,
            'minPurchaseAmount' => null,
        ];

        $c->save();
    }

    public function importProducts()
    {
        $productCount = Client::getProductsCount();
        $nrPages = ceil($productCount / 100);

        for ($page = 1; $page <= $nrPages; $page++) {
            $products = Client::getProducts(['limit' => 100, 'page' => $page]);
            foreach ($products as $product) {
                $this->importProduct($product);
            }
        }
    }

    public function importProduct($product)
    {
        if (Product::where('style', '=', $product->sku)->count() > 1) {
            return;
        }

        $p = Product::where('style', '=', $product->sku)->first();
        if (is_null($p)) {
            return;
        }

        // update sale_price
        Inventory::where('product_id', '=', $p->id)->update(['sale_price' => floatval($product->sale_price)]);

        // reimport images
        $images = (array) $product->images;
        $mainImageId = $product->primary_image->id ?? null;

        // delete old images
        ProductImage::where('product_id', '=', $p->id)->delete();

        // reimport images
        foreach ($images as $image) {
            $productImage = new ProductImage;
            $productImage->product_id = $p->id;
            $productImage->filename = $image->zoom_url;
            $productImage->is_main = $image->id == $mainImageId ? true : false;
            $productImage->save();
        }
    }

    public function syncProductInventory($p, $product)
    {
        $variants = (array) $product->skus;

        $productOptions = $product->options;
        $optionsSetOptions = $product->option_set->options ?? null;

        // create new product varaints
        foreach ($variants as $variant) {

            $vo = $variant->options;
            list($option1, $type1) = $this->option(($vo[0] ?? null), $productOptions, $optionsSetOptions);
            list($option2, $type2) = $this->option(($vo[1] ?? null), $productOptions, $optionsSetOptions);
            if ('color' == strtolower($type1)) {
                $color = $option1;
                $size  = $option2;
            } else {
                $color = $option2;
                $size  = $option1;
            }

            Inventory::newItem([
                'product_id' => $p->id,
                'color'      => trim($color),
                'size'       => trim($size),
                'quantity'   => 0,
                'cost'       => floatval($variant->cost_price),
                'price'      => max(floatval($variant->price), floatval($variant->adjusted_price)),
                'weight'     => Inventory::getWeightInOz($variant->weight, 'lb'),
            ]);
        }
    }

    public function option($variantOption, $productOptions, $optionsSetOptions)
    {
        if (is_null($variantOption)) {
            return [null, null];
        }

        $option = null;
        $type = null;

        $productOption = $this->getOptionById($productOptions, $variantOption->product_option_id, 'id');
        if ($productOption) {
            $type = $productOption->display_name;
            $optionsSet = $this->getOptionById($optionsSetOptions, $productOption->option_id, 'option_id');
            if ($optionsSet) {
                $value = $this->getOptionById($optionsSet->values, $variantOption->option_value_id, 'option_value_id');
                if ($value) {
                    $option = $value->label;
                }
            }
        }

        return [$option, $type];
    }

    public function getOptionById($options, $optionId, $optionName)
    {
        foreach ((array) $options as $option) {
            if ($option->{$optionName} == $optionId) {
                return $option;
            }
        }

        return null;
    }

    public function syncProductImages($p, $product)
    {
        $images = (array) $product->images;
        $mainImageId = $product->primary_image->id ?? null;

        foreach ($images as $image) {
            $productImage = new ProductImage;
            $productImage->product_id = $p->id;
            $productImage->filename = $image->standard_url;
            $productImage->is_main = $image->id == $mainImageId ? true : false;
            $productImage->save();
        }
    }

    public static function keysAreValid($clientId, $clientSecret, $accessToken, $apiPath)
    {
        Client::configure([
            'client_id' => $clientId,
            'auth_token' => $accessToken,
            'store_hash' => static::getStoreHash($apiPath),
        ]);

        $ping = Client::getTime();
        if ($ping instanceof \DateTime) {
            return true;
        }

        return false;
    }

    public static function hasPermissions($clientId, $clientSecret, $accessToken, $apiPath)
    {
        Client::configure([
            'client_id' => $clientId,
            'auth_token' => $accessToken,
            'store_hash' => static::getStoreHash($apiPath),
        ]);

        try {
            Client::failOnError();
            Client::getProductsCount();
            Client::getOrdersCount();
        } catch (Exception $e) {
            log_error($e);
            return false;
        }

        return true;
    }

    /**
     * Extract the store hash from the api path.
     *
     * @param  string $apiPath https://api.bigcommerce.com/stores/sc04g32a80/v3/
     * @return string
     */
    public static function getStoreHash($apiPath)
    {
        if (preg_match('/\/stores\/(.+)\/v[0-9]+/', $apiPath, $mathes) && isset($mathes[1])) {
            return $mathes[1];
        }

        return $apiPath;
    }
}
