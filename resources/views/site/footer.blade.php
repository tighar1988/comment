<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="social-network social-circle">
                    <li><a href="http://facebook.com/commentsold" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="http://twitter.com/commentsold" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                </ul>
                <span class="copyright">Copyright &copy; CommentSold, LLC. All rights reserved.</span>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="{{ cdn('/js/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ cdn('/js/bootstrap.min.js') }}"></script>
<!--JavaScript -->
<script src="{{ cdn('/js/main.js') }}"></script>
<script src="{{ cdn('/js/counter.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#counter-block').ready(function(){
            $('.fbcomts').animationCounter({
                start: 1990483,
                end: 5000000,
                step: 1,
                delay: 200 
            });
        });
    });
</script>
@include('common.facebook-pixel')

