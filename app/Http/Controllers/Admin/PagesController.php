<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Page;
use App\Http\Requests\Admin\PageFormRequest;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('admin.store.pages', compact('pages'));
    }

    public function create()
    {
        return view('admin.store.pages-form');
    }

    public function store(PageFormRequest $request)
    {
        $page = new Page($request->input());
        $page->save();
        flash('Page Saved!');
        return redirect('/admin/store/pages');
    }

    public function edit($pageId)
    {
        $page = Page::find($pageId);
        return view('admin.store.pages-form', compact('page'));
    }

    public function update($pageId, PageFormRequest $request)
    {
        $page = Page::find($pageId);
        $page->fill($request->input());
        $page->save();
        flash('Page Updated!');
        return redirect('/admin/store/pages');
    }

    public function destroy($pageId)
    {
        Page::destroy($pageId);
        flash('Page Deleted!');
        return redirect('/admin/store/pages');
    }
}
