<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\Metafield as MetafieldModel;

class Metafield extends Drop
{
    protected $metafield;

    public function __construct(MetafieldModel $metafield)
    {
        $this->metafield = $metafield;
    }

    public function __toString()
    {
        return $this->metafield->value;
    }
}
