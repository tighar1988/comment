<div class="modal active fade" id="onboarding-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="" onsubmit="return confirmOnboardingPopup(event);">
                <div class="modal-header">
                    <h4 class="modal-title">Welcome! Please confirm your email</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="email" name="email" autocomplete="off" required="" value="{{ $customer->email }}" class="form-control onboarding-email">
                        <span class="help-block" style="display: none"></span>

                        <?php $pageId = fb_page_id(); ?>
                        @if ($pageId)
                            <div>
                                <?php $userRef =  time() . rand(0, 100000) . '-' . $customer->id; ?>
                                <div class="fb-messenger-checkbox"
                                        origin="{{ shop_url() }}"
                                        page_id="{{ $pageId }}"
                                        messenger_app_id="{{ fb_app_id() }}"
                                        user_ref="{{ $userRef }}"
                                        prechecked="true"
                                        allow_login="true"
                                        size="medium"></div>

                                <script type="text/javascript">
                                    function confirmOnboardingOptIn() {
                                        if (typeof FB != 'object') {
                                            return;
                                        }

                                        FB.AppEvents.logEvent('MessengerCheckboxUserConfirmation', null, {
                                            'app_id':'{{ fb_app_id() }}',
                                            'page_id':'{{ $pageId }}',
                                            'ref':'{{ shop_id() }}',
                                            'user_ref':'{{ $userRef }}'
                                        });
                                    }
                                </script>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="confirmOnboardingPopup(event);" data-loading-text="Saving..." class="btn btn-success">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
