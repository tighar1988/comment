<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Inventory;
use Illuminate\Support\Facades\Auth;

class WaitlistAddFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $error = null;

            $variant = Inventory::find($this->id);
            if (! $variant) {
                $error = 'We could not find the product!';

            } elseif (! Auth::check()) {
                $error = "Please login to add items to your waitlist.";
            }

            if (! is_null($error)) {
                $validator->errors()->add('status', 422);
                $validator->errors()->add('message', 'Waitlist Error');
                $validator->errors()->add('description', $error);
            }
        });
    }
}
