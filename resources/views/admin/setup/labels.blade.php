@extends('admin.setup.setup')

@section('section', 'Shipping')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                @if (shop_setting('labels-enabled'))
                    <span class="settings-info">Labels <span class="label label-success">Enabled</span></span>
                    <small class="settings-muted text-muted">You are now able to print labels from the product inventory page.</small>

                    <form class="" role="form" method="POST" action="{{ url('/admin/setup/labels/disable') }}">
                        <button class="btn btn-danger" type="submit">Disable Labels</button>
                    </form>
                @else
                    <span class="settings-info">Labels <span class="label label-warning">Disabled</span></span>
                    <small class="settings-muted text-muted">If you have a DYMO label printer, you can use it to print labels.</small>
                    <form class="" role="form" method="POST" action="{{ url('/admin/setup/labels/enable') }}">
                        <button class="btn btn-success" type="submit">Enable Labels</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
