<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><hr/><br/>

    There was an error charging your card for your PayPal commission fees since {{ $sinceTime }}.

    Total amount due is ${{ amount($feesTotal) }}.<br/><br/>

    Please login to your account and update your card in Setup > Billing.
    <br/><br/>
</div>
