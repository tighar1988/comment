<?php

namespace App\Mails;

use App\Mail\NotifyAdmin;
use App\Mail\PayPalFees;
use App\Mail\PayPalFeesFailed;
use App\Mail\NewReposts;
use App\Mail\ExpiryReminder;
use App\Mail\WaitlistEmail;
use App\Mail\WaitlistActivated;
use App\Mail\AddedToCart;
use App\Mail\OrderPaid;
use App\Mail\OrderShipped;
use App\Mail\LocalPickupOrderFulfilled;
use App\Mail\InvalidInstagramComment;
use App\Mail\AwardedAccountCredit;
use App\Mail\PrepaidCredit;
use App\Mail\PrepaidCreditFailed;
use App\Mail\ShopifyOrderNotSynced;
use App\Mail\ShopifyInventoryNotTracked;
use App\Mail\TrialExpires;
use App\Mail\SubscriptionPlan;
use App\Mail\SubscriptionPlanFailed;
use App\Mail\IssuedGiftCard;
use App\Mail\InstagramWelcomeMessage;
use Mail;
use Exception;
use Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use App\Exceptions\ShopifyWebhookLockedException;
use Facebook\Exceptions\FacebookResponseException;
use PayPal\Exception\PayPalConnectionException;
use App\Shopify\ShopifyApiException;
use App\Facebook\Template;

class ShopMailer
{
    public static function notifyAdmin($alert)
    {
        try {
            // don't send error mails from bots who access the site by ip
            if (isset($_SERVER['SERVER_NAME'])) {
                if (! str_contains($_SERVER['SERVER_NAME'], ['commentsold.com', 'commentsold.app', 'app.pinkcoconutboutique.com', 'app.lindylousboutique.com', 'shopdiscountdivas.com'])) {
                    return;
                }
            }

            if ($alert instanceof MethodNotAllowedHttpException || $alert instanceof NotFoundHttpException) {
                // don't send error mails from known bots
                if (isset($_SERVER['HTTP_USER_AGENT'])) {
                    $bots = ['netcraft ssl', 'yandexbot', 'goglebot', 'msnbot-media', 'bingbot'];
                    $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
                    foreach ($bots as $bot) {
                        if (str_contains($userAgent, $bot)) {
                            return;
                        }
                    }
                }

                // don't send error mails from crawlers
                if (isset($_SERVER['REQUEST_URI'])) {
                    foreach (['wp-login', 'wp-admin'] as $crawler) {
                        if (str_contains($_SERVER['REQUEST_URI'], $crawler)) {
                            return;
                        }
                    }
                }
            }

            // don't send error mails for form validation errors or authentication errors
            if ($alert instanceof ValidationException || $alert instanceof AuthenticationException) {
                return;
            }

            if ($alert instanceof ShopifyWebhookLockedException && str_contains($alert->getMessage(), 'Inventory is not tracked by Shopify')) {
                return;
            }

            $ajaxRequest = false;
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 'XMLHttpRequest' == $_SERVER['HTTP_X_REQUESTED_WITH']) {
                $ajaxRequest = true;
            }

            $data = [
                'shopDb'        => config('database.connections.shop.database'),
                'userName'      => Auth::user()->name ?? null,
                'userEmail'     => Auth::user()->email ?? null,
                'requestUri'    => $_SERVER['REQUEST_URI'] ?? null,
                'refererUri'    => $_SERVER['HTTP_REFERER'] ?? null,
                'queryString'   => $_SERVER['QUERY_STRING'] ?? null,
                'requestMethod' => $_SERVER['REQUEST_METHOD'] ?? null,
                'ajaxRequest'   => $ajaxRequest,
                'redirectUrl'   => $_SERVER['REDIRECT_URL'] ?? null,
                'requestScheme' => $_SERVER['REQUEST_SCHEME'] ?? null,
                'remoteAddr'    => $_SERVER['REMOTE_ADDR'] ?? null,
                'serverAddr'    => $_SERVER['SERVER_ADDR'] ?? null,
                'httpCookie'    => $_SERVER['HTTP_COOKIE'] ?? null,
                'httpUserAgent' => $_SERVER['HTTP_USER_AGENT'] ?? null,
                'serverName'    => $_SERVER['SERVER_NAME'] ?? null,
                'requestTime'   => $_SERVER['REQUEST_TIME'] ?? null,
            ];

            if ($alert instanceof Exception) {
                $data['notification'] = 'Error on line '.$alert->getLine().' in '.$alert->getFile().': '.$alert->getMessage();

                if ($alert instanceof FacebookResponseException) {
                    $data['notification'] .= ' '.$alert->getRawResponse();
                } elseif ($alert instanceof PayPalConnectionException) {
                    $data['notification'] .= ' '.$alert->getData();
                } elseif ($alert instanceof ShopifyApiException) {
                    $data['notification'] .= ' '.json_encode($alert->getResponse());
                }

                $data['exception'] = get_class($alert);
            } else {
                $data['notification'] = $alert;
                $data['exception'] = null;
            }

            $throttle = new MailThrottler;
            if ($throttle->allowed($alert)) {
                Mail::to('brandon@bkruse.com')->queue(new NotifyAdmin($data));
                Mail::to('adam.ludovic.m@gmail.com')->queue(new NotifyAdmin($data));
            }

        } catch (Exception $e) {

        }
    }

    public function sendOrderPaidEmail($order)
    {
        log_info("[email] Sending paid order email to {$order->email}. CID #{$order->customer_id}");

        $products = [];
        foreach ($order->orderProducts as $orderProduct) {
            // todo: add image thumb
            $products[] = [
                'name'  => $orderProduct->prod_name,
                'price' => $orderProduct->price,
            ];
        }

        $this->send($order->email, new OrderPaid([
                'shopFromEmail'  => from_email_address(),
                'shopEmail'      => shop_email(),
                'shopId'         => shop_id(),
                'shopName'       => shop_name(),
                'orderProducts'  => $products,
                'shopUrl'        => shop_url('/account'),
                'orderId'        => $order->id,
                'localPickup'    => $order->local_pickup,
                'streetAddress'  => $order->street_address,
                'apartment'      => $order->apartment,
                'city'           => $order->city,
                'state'          => $order->state,
                'zip'            => $order->zip,
                'email'          => $order->email,
                'shipCharged'    => $order->ship_charged,
                'subtotal'       => $order->subtotal,
                'total'          => $order->total,
                'shipName'       => $order->ship_name,
                'taxTotal'       => $order->tax_total,
                'couponDiscount' => $order->coupon_discount,
                'applyBalance'   => $order->apply_balance,
            ]
        ));
    }

    public function sendOrderShippedEmail($order)
    {
        if ($order->tracking_number) {
            log_info("[email] Sending dispatched order email to {$order->email}. CID #{$order->customer_id}");
        } else {
            log_info("[email] Sending dispatched order email with tracking code to {$order->email}. CID #{$order->customer_id}");
        }

        $products = [];
        foreach ($order->orderProducts as $orderProduct) {
            // todo: add image thumb
            $products[] = [
                'name' => $orderProduct->prod_name,
                'price' => $orderProduct->price,
            ];
        }

        $emailData = [
            'shopFromEmail'  => from_email_address(),
            'shopEmail'      => shop_email(),
            'shopName'       => shop_name(),
            'orderProducts'  => $products,
            'shopUrl'        => shop_url('/account'),
            'shopId'         => shop_id(),
            'orderId'        => $order->id,
            'streetAddress'  => $order->street_address,
            'apartment'      => $order->apartment,
            'city'           => $order->city,
            'state'          => $order->state,
            'zip'            => $order->zip,
            'email'          => $order->email,
            'shipCharged'    => $order->ship_charged,
            'subtotal'       => $order->subtotal,
            'total'          => $order->total,
            'shipName'       => $order->ship_name,
            'trackingNumber' => $order->tracking_number,
            'taxTotal'       => $order->tax_total,
            'couponDiscount' => $order->coupon_discount,
            'applyBalance'   => $order->apply_balance,
        ];

        if ($order->local_pickup) {
            // delay 24 hours
            $this->sendLater($order->email, new LocalPickupOrderFulfilled($emailData), 24 * 60);

        } else {
            $this->send($order->email, new OrderShipped($emailData));
        }

    }

    public function sendPayPalFeesEmail($shopOwner, $feesTotal, $sinceTime)
    {
        log_info("[email] Sending paypal fees email to {$shopOwner->email}");
        $this->send($shopOwner->email, new PayPalFees([
                'shopOwnerName' => $shopOwner->name,
                'sinceTime'     => style_date($sinceTime),
                'feesTotal'     => $feesTotal,
            ]
        ));
    }

    public function sendTrialExpiresEmail($shopOwner)
    {
        log_info("[email] Sending trial expires email to {$shopOwner->email}");
        $this->send($shopOwner->email, new TrialExpires([
                'shopOwnerName' => $shopOwner->name,
            ]
        ));
    }

    public function sendPayPalFeesFailedEmail($shopOwner, $feesTotal, $sinceTime)
    {
        log_info("[email] Sending failed paypal fees email to {$shopOwner->email}");
        $this->send($shopOwner->email, new PayPalFeesFailed([
                'shopOwnerName' => $shopOwner->name,
                'sinceTime'     => style_date($sinceTime),
                'feesTotal'     => $feesTotal,
            ]
        ));
    }

    public function sendPrepaidCreditEmail($shopOwner, $amount)
    {
        log_info("[email] Sending prepaid credit email to {$shopOwner->email}");
        $this->send($shopOwner->email, new PrepaidCredit([
                'shopOwnerName' => $shopOwner->name,
                'amount'        => $amount,
            ]
        ));
    }

    public function sendPrepaidCreditFailedEmail($shopOwner, $amount)
    {
        log_info("[email] Sending failed prepaid credit email to {$shopOwner->email}");
        $this->send($shopOwner->email, new PrepaidCreditFailed([
                'shopOwnerName' => $shopOwner->name,
                'amount'        => $amount,
            ]
        ));
    }

    public function sendSubscriptionPlanEmail($shopOwner, $amount)
    {
        // log_info("[email] Sending subscription plan email to {$shopOwner->email}");
        // $this->send($shopOwner->email, new SubscriptionPlan([
        //         'shopOwnerName' => $shopOwner->name,
        //         'amount'        => $amount,
        //     ]
        // ));
    }

    public function sendSubscriptionPlanFailedEmail($shopOwner, $amount)
    {
        log_info("[email] Sending failed subscription plan email to {$shopOwner->email}");
        $this->send($shopOwner->email, new SubscriptionPlanFailed([
                'shopOwnerName' => $shopOwner->name,
                'amount'        => $amount,
            ]
        ));
    }

    public function sendNewRepostEmail($shopOwner, $createdPosts)
    {
        if (! count($createdPosts)) {
            return;
        }

        log_info("[email] Sending new repost email to {$shopOwner->email}");
        $posts = [];
        foreach ($createdPosts as $post) {
            $supplement = json_decode($post->supplement);
            $posts[] = [
                'edit_url' => url("/admin/posts/edit/{$post->id}"),
                'message'  => $supplement->message,
                'image'    => $supplement->url,
            ];
        }

        $this->send($shopOwner->email, new NewReposts([
                'shopOwnerName' => $shopOwner->name,
                'posts' => $posts,
            ]
        ));
    }

    public function sendShopifyOrderNotSyncedEmail($shopOwner, $orderId)
    {
        log_info("[email] Sending failed shopify order sync email to {$shopOwner->email}");
        $this->send($shopOwner->email, new ShopifyOrderNotSynced([
                'shopOwnerName' => $shopOwner->name,
                'orderId'       => $orderId,
            ]
        ));
    }

    public function sendShopifyInventoryNotTracked($shopOwner, $variant, $product)
    {
        log_info("[email] Sending failed shopify order sync email to {$shopOwner->email}");
        $this->send($shopOwner->email, new ShopifyInventoryNotTracked([
                'shopOwnerName' => $shopOwner->name,
                'productName'   => $product->product_name ?? null,
                'productSku'    => $product->style ?? null,
                'variantName'   => $variant->variantName(),
                'productId'     => $variant->product_id,
            ]
        ));
    }

    public function sendExpiryReminderEmail($customer, $product, $variant)
    {
        log_info("[email] Sending expire email to {$customer->email} for shopping cart expiry on inventory item: {$variant->id}. CID #{$customer->id}");
        $this->send($customer->email, new ExpiryReminder([
                'shopFromEmail' => from_email_address(),
                'shopEmail'     => shop_email(),
                'shopName'      => shop_name(),
                'customerName'  => $customer->name,
                'shopUrl'       => shop_url('/account'),
                'productName'   => $product->product_name ?? null,
            ]
        ));
    }

    public function sendWaitlistEmail($customer, $product, $variant)
    {
        // todo: add to waitlist-email cart items and waitlist items
        log_info("[email] Sending waitlist email to {$customer->email} for waitlist on inventory item: {$variant->id}. CID #{$customer->id}");
        $this->send($customer->email, new WaitlistEmail([
                'shopFromEmail'         => from_email_address(),
                'shopEmail'             => shop_email(),
                'shopId'                => shop_id(),
                'shopName'              => shop_name(),
                'productName'           => $product->product_name,
                'customerName'          => $customer->name,
                'customerStreetAddress' => $customer->street_address,
                'customerApartment'     => $customer->apartment,
                'customerCity'          => $customer->city,
                'customerState'         => $customer->state,
                'customerZip'           => $customer->zip,
                'shopUrl'               => shop_url('/account'),
            ]
        ));
    }

    public function sendWaitlistItemActivatedEmail($customer, $product, $variant)
    {
        log_info("[email] Sending activate waitlist email to {$customer->email} on inventory item: {$variant->id}. CID #{$customer->id}");
        $this->send($customer->email, new WaitlistActivated([
                'shopFromEmail'         => from_email_address(),
                'shopEmail'             => shop_email(),
                'shopName'              => shop_name(),
                'productName'           => $product->product_name,
                'customerName'          => $customer->name,
                'customerStreetAddress' => $customer->street_address,
                'customerApartment'     => $customer->apartment,
                'customerCity'          => $customer->city,
                'customerState'         => $customer->state,
                'customerZip'           => $customer->zip,
                'shopUrl'               => shop_url('/account'),
            ]
        ));
    }

    public function sendItemAddedToCartEmail($customer, $product, $variant)
    {
        log_info("[email] Sending email to {$customer->email} for new item in shopping cart, item: {$variant->id}. CID #{$customer->id}");
        $this->send($customer->email, new AddedToCart([
                'shopFromEmail'         => from_email_address(),
                'shopEmail'             => shop_email(),
                'shopId'                => shop_id(),
                'shopName'              => shop_name(),
                'productName'           => $product->product_name,
                'customerName'          => $customer->name,
                'customerStreetAddress' => $customer->street_address,
                'customerApartment'     => $customer->apartment,
                'customerCity'          => $customer->city,
                'customerState'         => $customer->state,
                'customerZip'           => $customer->zip,
                'expire'                => expire(),
                'shopUrl'               => shop_url('/account'),
            ]
        ));
    }

    public function sendInvalidInstagramCommentEmail($customer, $product)
    {
        log_info("[email] Sending email to {$customer->email} for invalid instagram comment, product: {$product->id}. CID #{$customer->id}");
        $this->send($customer->email, new InvalidInstagramComment([
                'shopFromEmail'   => from_email_address(),
                'shopEmail'       => shop_email(),
                'shopName'        => shop_name(),
                'productName'     => $product->product_name,
                'customerName'    => $customer->name,
                'shopUrl'         => shop_url('/account'),
                'waitlistMessage' => (new Template)->forInvalidComment($product),
            ]
        ));
    }

    public function sendIssuedGiftCardEmail($customer, $giftcard)
    {
        log_info("[email] Sending email to {$customer->email} for new gift card. CID #{$customer->id}");
        $this->send($customer->email, new IssuedGiftCard([
                'shopFromEmail' => from_email_address(),
                'shopEmail'     => shop_email(),
                'shopName'      => shop_name(),
                'customerName'  => $customer->name,
                'shopUrl'       => shop_url('/account'),
                'giftcardValue' => amount($giftcard->value),
                'giftcardUrl'   => shop_url('/giftcards/'.$giftcard->hash),
            ]
        ));
    }

    public function sendAwardedAccountCreditEmail($customer, $amount)
    {
        // send only email when adding balance
        if ($amount <= 0) {
            return;
        }

        log_info("[email] Sending email to {$customer->email} for awarded account credit: \${$amount}. CID #{$customer->id}");
        $this->send($customer->email, new AwardedAccountCredit([
                'shopFromEmail' => from_email_address(),
                'shopEmail'     => shop_email(),
                'shopName'      => shop_name(),
                'customerName'  => $customer->name,
                'shopUrl'       => shop_url('/account'),
                'amount'        => amount($amount),
                'template'      => template('awarded-account-credit'),
            ]
        ));
    }

    public function sendInstagramWelcomeEmail($customer)
    {
        log_info("[email] Sending Instagram welcome email to {$customer->email}. CID #{$customer->id}");
        $this->send($customer->email, new InstagramWelcomeMessage([
                'shopFromEmail' => from_email_address(),
                'shopEmail'     => shop_email(),
                'shopName'      => shop_name(),
                'customerName'  => $customer->name,
                'shopUrl'       => shop_url('/account'),
                'shopInstagram' => shop_setting('instagram.nickname'),
                'template'      => template('instagram-welcome-email'),
            ]
        ));
    }

    protected function send($email, $mailable)
    {
        if (empty($email)) {
            return;
        }

        try {
            // todo: if email is empty, log info and return
            Mail::to($email)->queue($mailable);

        } catch (Exception $e) {
            static::notifyAdmin($e);
            log_error("[email] General email error: {$e}. Email is '{$email}'.");
        }
    }

    protected function sendLater($email, $mailable, $delayMinutes)
    {
        if (empty($email)) {
            return;
        }

        try {
            $when = \Carbon\Carbon::now()->addMinutes($delayMinutes);
            Mail::to($email)->later($when, $mailable);

        } catch (Exception $e) {
            static::notifyAdmin($e);
            log_error("[email] Delay email error: {$e}. Email is '{$email}'.");
        }
    }
}
