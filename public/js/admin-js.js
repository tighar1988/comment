$.admin = {};
$.admin.options = {
    navbarMenuSlimscroll: true,
    navbarMenuSlimscrollWidth: "3px",
    navbarMenuHeight: "200px",
    animationSpeed: 500,
    sidebarToggleSelector: "[data-toggle='offcanvas']",
    sidebarPushMenu: true,
    sidebarSlimScroll: true,
    sidebarExpandOnHover: false,
    enableBoxRefresh: true,
    enableBSToppltip: true,
    enableFastclick: true,
    enableControlSidebar: true,
    controlSidebarOptions: {
        toggleBtnSelector: "[data-toggle='control-sidebar']",
        selector: ".control-sidebar",
        slide: true
    },
    screenSizes: {
        xs: 480,
        sm: 768,
        md: 992,
        lg: 1200
    }
};

$(function() {
    "use strict";

    $("body").removeClass("hold-transition");

    var o = $.admin.options;

    _init();

    $.admin.layout.activate();

    $.admin.tree('.sidebar');

    if (o.enableControlSidebar) {
        $.admin.controlSidebar.activate();
    }
    if (o.navbarMenuSlimscroll && typeof $.fn.slimscroll != 'undefined') {
        $(".navbar .menu").slimscroll({
            height: o.navbarMenuHeight,
            alwaysVisible: false,
            size: o.navbarMenuSlimscrollWidth
        }).css("width", "100%");
    }

    if (o.sidebarPushMenu) {
        $.admin.pushMenu.activate(o.sidebarToggleSelector);
    }


});

function _init() {
    'use strict';
    $.admin.layout = {
        activate: function() {
            var _this = this;
            _this.fix();
            _this.fixSidebar();
            $(window, ".wrapper").resize(function() {
                _this.fix();
                _this.fixSidebar();
            });
        },
        fix: function() {

            var neg = $('.main-header').outerHeight() + $('.main-footer').outerHeight();
            var window_height = $(window).height();
            var sidebar_height = $(".sidebar").height();

            if ($("body").hasClass("fixed")) {
                $(".content-wrapper, .right-side").css('min-height', window_height - $('.main-footer').outerHeight());
            } else {
                var postSetWidth;
                if (window_height >= sidebar_height) {
                    $(".content-wrapper, .right-side").css('min-height', window_height - neg);
                    postSetWidth = window_height - neg;
                } else {
                    $(".content-wrapper, .right-side").css('min-height', sidebar_height);
                    postSetWidth = sidebar_height;
                }


                var controlSidebar = $($.admin.options.controlSidebarOptions.selector);
                if (typeof controlSidebar !== "undefined") {
                    if (controlSidebar.height() > postSetWidth)
                        $(".content-wrapper, .right-side").css('min-height', controlSidebar.height());
                }

            }
        },
        fixSidebar: function() {

            if (!$("body").hasClass("fixed")) {
                if (typeof $.fn.slimScroll != 'undefined') {
                    $(".sidebar").slimScroll({
                        destroy: true
                    }).height("auto");
                }
                return;
            } else if (typeof $.fn.slimScroll == 'undefined' && window.console) {
                window.console.error("Error: the fixed layout requires the slimscroll plugin!");
            }

            if ($.admin.options.sidebarSlimScroll) {
                if (typeof $.fn.slimScroll != 'undefined') {

                    $(".sidebar").slimScroll({
                        destroy: true
                    }).height("auto");

                    $(".sidebar").slimscroll({
                        height: ($(window).height() - $(".main-header").height()) + "px",
                        color: "rgba(0,0,0,0.2)",
                        size: "3px"
                    });
                }
            }
        }
    };


    $.admin.pushMenu = {
        activate: function(toggleBtn) {

            var screenSizes = $.admin.options.screenSizes;


            $(toggleBtn).on('click', function(e) {
                e.preventDefault();


                if ($(window).width() > (screenSizes.sm - 1)) {
                    if ($("body").hasClass('sidebar-collapse')) {
                        $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu');
                    } else {
                        $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    }
                } else {
                    if ($("body").hasClass('sidebar-open')) {
                        $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    } else {
                        $("body").addClass('sidebar-open').trigger('expanded.pushMenu');
                    }
                }
            });

            $(".content-wrapper").click(function() {

                if ($(window).width() <= (screenSizes.sm - 1) && $("body").hasClass("sidebar-open")) {
                    $("body").removeClass('sidebar-open');
                }
            });


            if ($.admin.options.sidebarExpandOnHover ||
                ($('body').hasClass('fixed') &&
                    $('body').hasClass('sidebar-mini'))) {
                this.expandOnHover();
            }
        },
        expandOnHover: function() {
            var _this = this;
            var screenWidth = $.admin.options.screenSizes.sm - 1;

            $('.main-sidebar').hover(function() {
                if ($('body').hasClass('sidebar-mini') &&
                    $("body").hasClass('sidebar-collapse') &&
                    $(window).width() > screenWidth) {
                    _this.expand();
                }
            }, function() {
                if ($('body').hasClass('sidebar-mini') &&
                    $('body').hasClass('sidebar-expanded-on-hover') &&
                    $(window).width() > screenWidth) {
                    _this.collapse();
                }
            });
        },
        expand: function() {
            $("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');
        },
        collapse: function() {
            if ($('body').hasClass('sidebar-expanded-on-hover')) {
                $('body').removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');
            }
        }
    };


    $.admin.tree = function(menu) {
        var _this = this;
        var animationSpeed = $.admin.options.animationSpeed;
        $(document).on('click', menu + ' li a', function(e) {

            var $this = $(this);
            var checkElement = $this.next();


            if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible'))) {

                checkElement.slideUp(animationSpeed, function() {
                    checkElement.removeClass('menu-open');

                });
                checkElement.parent("li").removeClass("active");
            } else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {

                var parent = $this.parents('ul').first();

                var ul = parent.find('ul:visible').slideUp(animationSpeed);

                ul.removeClass('menu-open');

                var parent_li = $this.parent("li");
                checkElement.slideDown(animationSpeed, function() {

                    checkElement.addClass('menu-open');
                    parent.find('li.active').removeClass('active');
                    parent_li.addClass('active');

                    _this.layout.fix();
                });
            }

            if (checkElement.is('.treeview-menu')) {
                e.preventDefault();
            }
        });
    };


    $.admin.controlSidebar = {

        activate: function() {

            var _this = this;

            var o = $.admin.options.controlSidebarOptions;

            var sidebar = $(o.selector);

            var btn = $(o.toggleBtnSelector);


            btn.on('click', function(e) {
                e.preventDefault();

                if (!sidebar.hasClass('control-sidebar-open') &&
                    !$('body').hasClass('control-sidebar-open')) {
                    //Open the sidebar
                    _this.open(sidebar, o.slide);
                } else {
                    _this.close(sidebar, o.slide);
                }
            });


            var bg = $(".control-sidebar-bg");
            _this._fix(bg);


            if ($('body').hasClass('fixed')) {
                _this._fixForFixed(sidebar);
            } else {

                if ($('.content-wrapper, .right-side').height() < sidebar.height()) {
                    _this._fixForContent(sidebar);
                }
            }
        },

        open: function(sidebar, slide) {

            if (slide) {
                sidebar.addClass('control-sidebar-open');
            } else {

                $('body').addClass('control-sidebar-open');
            }
        },

        close: function(sidebar, slide) {
            if (slide) {
                sidebar.removeClass('control-sidebar-open');
            } else {
                $('body').removeClass('control-sidebar-open');
            }
        },
        _fix: function(sidebar) {
            var _this = this;
            if ($("body").hasClass('layout-boxed')) {
                sidebar.css('position', 'absolute');
                sidebar.height($(".wrapper").height());
                $(window).resize(function() {
                    _this._fix(sidebar);
                });
            } else {
                sidebar.css({
                    'position': 'fixed',
                    'height': 'auto'
                });
            }
        },
        _fixForFixed: function(sidebar) {
            sidebar.css({
                'position': 'fixed',
                'max-height': '100%',
                'overflow': 'auto',
                'padding-bottom': '50px'
            });
        },
        _fixForContent: function(sidebar) {
            $(".content-wrapper, .right-side").css('min-height', sidebar.height());
        }
    };
}
