@extends('layouts.admin-setup')

@section('content')
<div class="row">
    <div class="form-group col-md-4 col-md-offset-4">

        <div class="row">
            <h3>Add a new card.</h3>
            <small class="text-muted">This card will be used for billing paypal comission fees.</small><br/>
            <br>You can cancel your trial at anytime</b><br>
        </div>

        <form method="POST" action="/admin/setup/step-2" class="form-horizontal" accept-charset="UTF-8" id="billing-form">

            <div class="form-group">
                <label for="cc-number">Credit Card Number</label>
                <input type="text" id="cc-number" class="form-control" data-stripe="number" required="">
            </div>

            <div class="form-group expiration-form-group form-inline">
                <label>Expiration Date</label>
                <select class="cc-expiration-month form-control" data-stripe="exp-month">
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>

                <select class="cc-expiration-year form-control" data-stripe="exp_year">
                    <?php $year = date('Y'); ?>
                    @for ($i = 0; $i < 16; $i++)
                        <option value="{{$year+$i}}">{{$year+$i}}</option>
                    @endfor
                </select>
            </div>

            <div class="form-group">
                <label for="cvv">CVV Number</label>
                <input type="text" id="cvv" placeholder="" class="form-control" data-stripe="cvc" required="">
            </div>

            <div class="card-errors" style="display:none;"></div>

            <div class="form-group">
                <button type="submit" id="submit-btn" class="btn btn-success btn-lg" data-loading-text="Submiting...">Save Credit Card</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    Stripe.setPublishableKey('{{ config('services.stripe.key') }}');
    $(function() {
        var $form = $('#billing-form');
        $form.submit(function(event) {
            $('#submit-btn').button('loading');
            Stripe.card.createToken($form, stripeResponseHandler);
            return false;
        });
    });

    function stripeResponseHandler(status, response) {
        var $form = $('#billing-form');

        if (response.error) {
            $form.find('.card-errors').text(response.error.message).show();
            $('#submit-btn').button('reset');

        } else {
            var token = response.id;
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            $form.get(0).submit();
        }
    };

</script>
@endsection
