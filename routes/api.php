<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
Route::group(['domain' => 'api.'.env('APP_DOMAIN'), 'prefix' => '/2.0/{shop}', 'middleware' => ['api', 'api-shop']], function () {

    Route::post('signin', 'Shop\APIController@signin');
    Route::get('products/search', 'Shop\APIController@productsSearch');

    Route::get('products/feed', 'Shop\APIController@productsFeed');
    Route::get('products/{productId}', 'Shop\APIController@product');

    Route::group(['middleware' => ['jwt.auth']], function () {

        Route::post('signout', 'Shop\APIController@signout');
        Route::get('user/cart', 'Shop\APIController@cart');
        Route::get('user/waitlist', 'Shop\APIController@waitlist');
        Route::get('user/orders', 'Shop\APIController@orders');
        Route::post('user/cart/products', 'Shop\APIController@cartProducts');
        Route::get('user/delivery-method', 'Shop\APIController@setDeliveryMethod');

        Route::delete('user/cart/products/{cartId}', 'Shop\APIController@removeFromCart');
        Route::post('user/cart/coupon', 'Shop\APIController@applyCoupon');
        Route::delete('user/cart/coupon/{couponId}', 'Shop\APIController@deleteCoupon');

        Route::get('user/cards', 'Shop\APIController@getCards');
        Route::post('user/cards', 'Shop\APIController@addCard');
        Route::delete('user/cards/{cardId}', 'Shop\APIController@removeCard');
        Route::put('user/cards/{cardId}', 'Shop\APIController@updateCard');

        Route::post('checkout/stripe', 'Shop\APIController@checkoutWithExistingCard');
        Route::post('checkout/paypal', 'Shop\APIController@checkoutPaypal');
        Route::post('checkout/account-credit', 'Shop\APIController@checkoutWithAccountCredit');

        Route::get('user/balance', 'Shop\APIController@getBalance');
        Route::post('user/balance', 'Shop\APIController@applyBalance');
        Route::delete('user/balance', 'Shop\APIController@clearBalance');

        Route::put('user/address', 'Shop\APIController@updateAddress');
        Route::get('user/address', 'Shop\APIController@getAddress');
        Route::post('user/address', 'Shop\APIController@setAddress');

        Route::put('user/email', 'Shop\APIController@updateEmail');
        Route::get('user/email', 'Shop\APIController@getEmail');

        Route::get('user/fullfilled-orders', 'Shop\APIController@fulfilledOrders');
        Route::get('user/paid-orders', 'Shop\APIController@paidOrders');
        Route::get('stripe/customer', 'Shop\APIController@stripeCustomerId');
    });
});
