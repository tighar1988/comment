@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Customers</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Customers</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">
                    <div class="table-responsive">
                        <table id="customers-table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Customer ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    @if (shop_id() == 'poppyanddot')
                                        <th>Instagram</th>
                                    @endif
                                    <th>State</th>
                                    <th>City</th>
                                    <th>Street Address</th>
                                    <th>Apartment</th>
                                    <th>Zip</th>
                                    <th>Phone Number</th>
                                    <th>Balance</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>

@include('admin.customers.balance-modal')
@endsection

@section('scripts')
<script type="text/javascript">

    function openAddBalance(element) {
        $('#balance-modal-add').modal();
        $('#balance-modal-add .balance').val('');
        $('#balance-modal-add .note').val('');
        $('#balance-modal-add .for-user').text(' ('+$(element).attr('data-customerName')+')');
        $('#balance-modal-add .balance').attr('data-customerId', $(element).attr('data-customerId'));
        $('#balance-modal-add form').attr('action', '/admin/customers/'+$(element).attr('data-customerId')+'/add-balance');
        $('#balance-modal-add .form-group').removeClass('has-error');
        $('#balance-modal-add .help-block').hide().html('');
        return false;
    }

    function openSubtractBalance(element) {
        $('#balance-modal-subtract').modal();
        $('#balance-modal-subtract .balance').val('');
        $('#balance-modal-subtract .note').val('');
        $('#balance-modal-add .for-user').text(' ('+$(element).attr('data-customerName')+')');
        $('#balance-modal-subtract .balance').attr('data-customerId', $(element).attr('data-customerId'));
        $('#balance-modal-subtract form').attr('action', '/admin/customers/'+$(element).attr('data-customerId')+'/subtract-balance');
        $('#balance-modal-subtract .form-group').removeClass('has-error');
        $('#balance-modal-subtract .help-block').hide().html('');
        return false;
    }

    $(document).ready(function() {
        $('#customers-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/customers/datatable",
            "bStateSave": true,
            columns: [
                { data: 'id', render: function(data, type, row) {
                    return '#'+row.id;
                }},
                { data: 'name', render: function(data, type, row) {
                    return '<a href="/admin/customers/'+row.id+'">'+row.name+'</a>';
                }},
                { data: 'email'},
                @if (shop_id() == 'poppyanddot')
                    { data: null, 'searchable': false, 'orderable': false, render: function(data, type, row) {
                        if (row.instagram_data && row.instagram_data.username) {
                            return '<a target="_blank" href="https://www.instagram.com/'+row.instagram_data.username+'">'+row.instagram_data.username+'</a>';
                        }
                        return '';
                    }},
                @endif
                { data: 'state'},
                { data: 'city'},
                { data: 'street_address'},
                { data: 'apartment'},
                { data: 'zip'},
                { data: 'phone_number'},
                { data: 'balance', className: 'balance-amount'},
                { data: null, 'searchable': false, 'orderable': false, className: 'text-center', render: function(data, type, row) {
                    var html = '';
                    html += '<div class="dropdown">';
                    html +=     '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
                    html +=         '<i class="fa fa-ellipsis-h fa-lg"></i>';
                    html +=     '</a>';
                    html +=     '<ul class="dropdown-menu pull-right">';
                    html +=         '<li><a href="/admin/customers/'+row.id+'">Customer Details</a></li>';
                    if (row.facebook_url.length > 25) {
                        html +=         '<li><a target="_blank" href="'+row.facebook_url+'">Facebook Profile</a></li>';
                    }
                    html +=         '<li><a data-customerId="'+row.id+'" data-customerName="'+row.name+'" onclick="return openAddBalance(this);" href="#">Add Balance</a></li>';
                    html +=         '<li><a data-customerId="'+row.id+'" data-customerName="'+row.name+'" onclick="return openSubtractBalance(this);" href="#">Subtract Balance</a></li>';
                    html +=         '<li><a href="/admin/coupons/add?customer='+row.id+'">Add Coupon</a></li>';
                    html +=     '</ul>';
                    html += '</div>';

                    return html;
                }}
            ]
        });

        $('#balance-modal-add, #balance-modal-subtract').on('shown.bs.modal', function () {
            $('.balance', $(this)).focus();
        })

        $('#balance-modal-add form').submit(function(event) {
            var $form = $(this);
            $('button[type=submit]', $form).button('loading');
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: { balance: $('.balance', $form).val(), note: $('.note', $form).val() },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $('#balance-modal-add').modal('hide');
                        alert('Balance Added!');
                        var customerId = $('.balance', $form).attr('data-customerId');
                        $('tr[data-customerId='+customerId+'] .balance-amount').text('$'+data.balance);
                    } else {
                        $('.form-group', $form).addClass('has-error');
                        $('.help-block', $form).show().html('<strong>Sorry, something went wrong</strong>');
                    }
                    $('button[type=submit]', $form).button('reset');
                },
                error: function(data) {
                    var message = 'An error occurred.';
                    try { message = data.responseJSON.balance[0]; } catch (e) {}
                    $('.form-group', $form).addClass('has-error');
                    $('.help-block', $form).show().html('<strong>'+message+'</strong>');
                    $('button[type=submit]', $form).button('reset');
                }
            });

            event.preventDefault();
            return false;
        });

        $('#balance-modal-subtract form').submit(function(event) {
            var $form = $(this);
            $('button[type=submit]', $form).button('loading');
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: {balance: $('.balance', $form).val(), note: $('.note', $form).val() },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $('#balance-modal-subtract').modal('hide');
                        alert('Balance Subtracted!');
                        var customerId = $('.balance', $form).attr('data-customerId');
                        $('tr[data-customerId='+customerId+'] .balance-amount').text('$'+data.balance);
                    } else {
                        $('.form-group', $form).addClass('has-error');
                        $('.help-block', $form).show().html('<strong>Sorry, something went wrong</strong>');
                    }
                    $('button[type=submit]', $form).button('reset');
                },
                error: function(data) {
                    var message = 'An error occurred.';
                    try { message = data.responseJSON.balance[0]; } catch (e) {}
                    $('.form-group', $form).addClass('has-error');
                    $('.help-block', $form).show().html('<strong>'+message+'</strong>');
                    $('button[type=submit]', $form).button('reset');
                }
            });

            event.preventDefault();
            return false;
        });

    });
</script>
@endsection
