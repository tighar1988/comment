<?php

namespace Tests\Unit\Shipping;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Shipping\Endicia;
use Mockery;

class EndiciaTest extends TestCase
{
    protected $shopOwner;

    public function setUp()
    {
        parent::setUp();

        $this->mockShopSettings([
            'shipping.rateplan'       => 'Retail',
            'shipping.label-fee'      => 0.05,
            'shipping.processing_fee' => 0.03,
            'shop.phone'              => '+ (123) 4363 7890',
            'shop.from_name'          => 'My Company',
            'shop.street_address'   => '1st Street',
            'shop.city'           => 'New York',
            'shop.state'          => 'NY',
            'shop.zip_code'       => '30003',
        ]);

        $this->shopOwner = Mockery::mock('App\Models\User');
    }

    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Endicia::class, new Endicia($this->shopOwner));
    }

    /** @test */
    public function it_ruturns_the_phone()
    {
        $this->assertEquals('123 4363 7890', (new Endicia($this->shopOwner))->getShopPhone());
    }

    /** @test */
    public function it_ruturns_the_from_address()
    {
        $this->assertEquals((new Endicia($this->shopOwner))->fromAddress(),
            '<FromName>My Company</FromName>
            <ReturnAddress1>1st Street</ReturnAddress1>
            <FromCity>New York</FromCity>
            <FromState>NY</FromState>
            <FromPostalCode>30003</FromPostalCode>
            <FromPhone>123 4363 7890</FromPhone>'
        );
    }

    /** @test */
    public function it_returns_the_to_address()
    {
        $order = $this->mockOrder([
            'ship_name'      => 'John Doe',
            'street_address' => 'Summer street',
            'apartment'      => '32',
            'city'           => 'Clearance',
            'state'          => 'GE',
            'zip'            => '333444',
        ]);

        $this->assertEquals((new Endicia($this->shopOwner))->toAddress($order),
            '<ToName>John Doe</ToName>
            <ToAddress1>Summer street</ToAddress1>
            <ToAddress2>32</ToAddress2>
            <ToCity>Clearance</ToCity>
            <ToState>GE</ToState>
            <ToPostalCode>333444</ToPostalCode>
            <ToPhone>6666111111</ToPhone>'
        );
    }

    /** @test */
    public function no_bypass_address_validaion_xml()
    {
        $this->assertEquals('', (new Endicia($this->shopOwner))->getValidateAddress());
    }

    /** @test */
    public function it_returns_priority_service_level()
    {
        $dimensions = ['weight' => 25];
        $this->assertEquals('Priority', (new Endicia($this->shopOwner))->getServiceLevel($dimensions));
    }

    /** @test */
    public function it_returns_first_service_level()
    {
        $dimensions = ['weight' => 15.99];
        $this->assertEquals('First', (new Endicia($this->shopOwner))->getServiceLevel($dimensions));
    }

    /** @test */
    public function it_returns_mail_pice_shape_for_first_class()
    {
        $this->assertEquals('Parcel', (new Endicia($this->shopOwner))->mailPieceShape('First'));
    }

    /** @test */
    public function it_returns_mail_pice_shape_for_priority()
    {
        set_shop_database('taralynns');
        $this->assertEquals('FlatRateEnvelope', (new Endicia($this->shopOwner))->mailPieceShape('Priority'));
    }

    /** @test */
    public function it_returns_mail_piece_dimensions()
    {
        $dimensions = [
            'length' => 5,
            'width'  => 6,
            'height' => 3,
        ];

        $xml = '<MailpieceDimensions>
                    <Length>5</Length>
                    <Width>6</Width>
                    <Height>3</Height>
                </MailpieceDimensions>';

        $this->assertEquals($xml, (new Endicia($this->shopOwner))->mailPieceDimensions($dimensions));
    }

    /** @test */
    public function it_returns_surcharge()
    {
        $rate = 3.97;
        $surcharge = (new Endicia($this->shopOwner))->surcharge($rate);

        $this->assertEquals(4.15, $surcharge);
    }

    /** @test */
    public function it_retuns_an_xml_value()
    {
        $xml = '<LabelRequestResponse>
                    <Rate>10.25</Rate>
                    <Zone></Zone>
                </LabelRequestResponse>';

        $this->assertEquals(10.25, (new Endicia($this->shopOwner))->getValue('Rate', $xml));
        $this->assertEquals(null, (new Endicia($this->shopOwner))->getValue('Price', $xml));
        $this->assertEquals(null, (new Endicia($this->shopOwner))->getValue('Zone', $xml));
    }
}
