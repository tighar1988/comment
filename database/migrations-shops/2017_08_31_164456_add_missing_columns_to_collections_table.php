<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingColumnsToCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->integer('published_at')->nullable();
            $table->string('sort_order')->nullable();
            $table->string('template_suffix')->nullable();
            $table->string('published_scope')->nullable();
            $table->integer('image_created_at')->after('image')->nullable();
            $table->integer('image_width')->after('image')->nullable();
            $table->integer('image_height')->after('image')->nullable();
            $table->string('shopify_collection_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dropColumn(['published_at', 'sort_order', 'template_suffix', 'published_scope', 'shopify_collection_id', 'image_created_at', 'image_width', 'image_height']);
        });
    }
}
