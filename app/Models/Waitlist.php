<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\OrderProduct;

class Waitlist extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'waitlist';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'product_id',
        'inventory_id',
        'order_source',
        'comment_id',
    ];

    public function sourceIsFacebook()
    {
        return $this->order_source == OrderProduct::SOURCE_FACEBOOK_COMMENT;
    }

    public function sourceIsInstagram()
    {
        return $this->order_source == OrderProduct::SOURCE_INSTAGRAM_COMMENT;
    }

    public static function addItem($customer, $commentId, $variant)
    {
        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => null,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_FACEBOOK_COMMENT,
            'comment_id'   => $commentId,
        ]);
    }

    public static function instagramAddItem($customer, $commentId, $variant)
    {
        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => null,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_INSTAGRAM_COMMENT,
            'comment_id'   => $commentId,
        ]);
    }

    public static function storeAddItem($customer, $variant)
    {
        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => null,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_STORE,
        ]);
    }

    public static function mobileAddItem($customer, $variant)
    {
        return static::create([
            'user_id'      => $customer->id,
            'product_id'   => $variant->product_id,
            'inventory_id' => $variant->id,
            'order_source' => OrderProduct::SOURCE_MOBILE_APP,
        ]);
    }

    public function scopeForCustomer($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    public function variant()
    {
        return $this->hasOne('App\Models\Inventory', 'id', 'inventory_id');
    }

    public function comment()
    {
        return $this->hasOne('App\Models\Comment', 'id', 'comment_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Models\ShopUser', 'id', 'user_id');
    }

    public static function reportByProduct($sku = null)
    {
        $query = static::select(DB::raw('COALESCE(count(inventory.id), 0) * (COALESCE(AVG(inventory.price), 0) - COALESCE(AVG(inventory.cost), 0)) as reorder_profit,
                (SELECT images.filename FROM images WHERE images.is_main = true AND products.id = images.product_id LIMIT 1) as thumb,
                (SELECT MAX(created_time) FROM posts WHERE products.id = posts.prod_id AND posts.scheduled_time IS NULL LIMIT 1) as latest_post,
                products.product_name, products.id as product_id, products.style as sku, products.brand,
                products.brand_style, count(inventory.id) as on_waitlist'))
            ->join('inventory', 'waitlist.inventory_id', '=', 'inventory.id')
            ->join('products', 'inventory.product_id', '=', 'products.id')
            ->groupBy('products.id')
            ->orderBy('on_waitlist', 'DESC');

        if (! is_null($sku)) {
            $query->where('products.style', 'like', "%{$sku}%");
        }

        return $query;
    }

    public static function reportByVariant($sku = null)
    {
        $query = static::select(DB::raw('waitlist.inventory_id,
                inventory.product_id, inventory.quantity, inventory.price, inventory.cost, inventory.color, inventory.size,
                products.product_name, products.style as sku, products.brand, products.brand_style,
                count(waitlist.inventory_id) as on_waitlist'))
            ->join('inventory', 'waitlist.inventory_id', '=', 'inventory.id')
            ->join('products', 'inventory.product_id', '=', 'products.id')
            ->groupBy('waitlist.inventory_id')
            ->orderBy('on_waitlist', 'DESC');

        if (! is_null($sku)) {
            $query->where('products.style', 'like', "%{$sku}%");
        }

        return $query;
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->whereBetween('created_at', [$start, $end]);
    }

    public function scopeSince($query, $since)
    {
        return $query->where('created_at', '>=', $since);
    }
}
