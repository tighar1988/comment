<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facebook\Facebook;
use App\Facebook\FacebookMessenger;
use App\Mails\ShopMailer;
use App\Models\Inventory;
use App\Models\Cart;
use Carbon\Carbon;
use Exception;
use DB;
use App\Exceptions\ShopifyWebhookLockedException;
use App\Checkout\Checkout;

class WaitlistActivate
{
    /**
     * Waitlist job.
     *
     * @param string $shop
     * @return void
     */
    public function handle($shop)
    {
        $facebook = new Facebook;
        $mailer = new ShopMailer;
        $notifyWaitlistActivated = shop_setting('setup.notify-waitlisted-activated', true);
        $baseDelay = 0;

        $inventory = Inventory::thatExists()->with('waitlist')->get();

        foreach ($inventory as $variant) {
            foreach ($variant->waitlist as $waitlistItem) {

                if ($variant->latestQuantity() > 0) {

                    $customer = $waitlistItem->customer;
                    $stripe = new Checkout($customer);
                    $cardId = $waitlistItem->card_id;
                    log_info("[waitlist] Activated inventory id #{$variant->id}. CID #{$customer->id}");

                    $message = "Activated waitlist item for customer #{$customer->id}. Adding item to cart.";

                    try {
                        $sourceIsFacebook = false;
                        if ($waitlistItem->sourceIsFacebook()) {
                            $sourceIsFacebook = true;
                            Cart::addItem($customer, $waitlistItem->comment_id, $variant, $message);
                        } elseif ($waitlistItem->sourceIsInstagram()) {
                            Cart::instagramAddItem($customer, $waitlistItem->comment_id, $variant, $message);
                        } else {
                            Cart::storeAddItem($customer, $variant, $message);
                        }

                        if ($cardId != null) {
                            $stripe->withExistingCard($cardId);
                        }


                    } catch (ShopifyWebhookLockedException $e) {
                        ShopMailer::notifyAdmin($e);
                        log_error("[waitlist-activate][shopify] $e");
                        continue 2;
                    }

                    $waitlistItem->delete();

                    try {
                        $mailer->sendWaitlistItemActivatedEmail($customer, $variant->product, $variant);
                        if ($sourceIsFacebook) {
                            FacebookMessenger::notifyWaitlistItemActivated($customer, $variant);
                            if ($notifyWaitlistActivated) {
                                $facebook->replyWaitlistItemActivated($waitlistItem->comment, $baseDelay);

                                // Add extra time to our next message
			                 	$baseDelay = ($baseDelay + rand(6, 30));
                            }
                        }
                    } catch (Exception $e) {
                        ShopMailer::notifyAdmin($e);
                        log_error("[waitlist] [exception] {$e}");
                    }
                }
            }
        }
    }
}
