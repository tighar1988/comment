<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment_id');
            $table->integer('post_id')->unsigned();
            $table->string('from_id');
            $table->string('from_username')->nullable();
            $table->string('from_name')->nullable();
            $table->text('from_profile_picture')->nullable();
            $table->text('content')->nullable();
            $table->integer('created_time')->nullable();
            $table->timestamps();

            $table->index(['comment_id', 'post_id', 'from_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_comments');
    }
}
