@extends('layouts.shop')

@section('content')
<?php $hasMultipleLocations = multiple_locations_enabled(); ?>
<div class="overlay"><div id="loading-img"></div></div>

<div class="container">
    <div class="content-center">
        <div class="shop-name">{{ shop_name() }}</div>
    </div>
    <div style="float: right;">
        <form style="" action="/logout" method="POST">
            {{ csrf_field() }}
            <button class="btn btn-link btn-sm" type="submit"><i class="fa fa-fw fa-close"></i><span>Logout</span></button>
        </form>
        <br/>
    </div>
    <section class="profile-info">
        <p class="wlc-text">Welcome back {{ $customer->name }}!</p>
        <br/>

        @if ($customer->hasBalance())
            <div>
                <div class="flex-container">
                    <span class="value-text">${{ amount($customer->balance) }}</span>
                </div>
                <span class="type-of-value">Available Credit</span>
            </div>
        @endif

        @if ((shop_id() == 'shop1' || shop_id() == 'cheekys') && shop_setting('store.enabled'))
            <div>
                <div class="">
                    <div class="slink" onclick="showGiftCardBox();">Have a gift card?</div>
                    <div style="display:none;" id="gift-card-box">
                        <input type="text" onkeyup="checkGiftCard(event);" placeholder="Gift card code" id="gift-card-code"/>
                        <button class="small" onclick="redeemGiftCard();">Apply</button>
                    </div>
                </div>
            </div>
        @endif

        <div class="pull-left">
            <div class="flex-container">
                <input id="customer-email" placeholder="email@example.com" type="text" disabled="disabled" class="value-text" value="{{ $customer->email }}" />
                <button class="btn-circle edit-email" onclick="enableEmailForm()"></button>
                <span id="email-form" style="display: none">
                    <button onclick="updateEmail()">Update</button>
                </span>
            </div>
            <span class="type-of-value">Email Address</span>
        </div>

        <div class="margin-top">

            @if (shop_setting('facebook.share-group-id'))
                <br/><br/>
                <div class="pull-left">
                    <div class="flex-container">
                        {{-- todo: the image is a bit pixelated; get a better image; ask designer --}}
                        <a href='javascript: void(0);' onClick="facebookShare()"><img src="{{ cdn('/images/I9G5EvC.gif') }}" alt="{{ shop_name() }}"></a>
                    </div>
                </div>
            @endif

            <br/>

            <div class="margin-top">
                <div class="pull-left">
                    <span class="value align-right"><strong>Customer Address:</strong> <br/>
                        @if ($customer->street_address == '')
                            We do not have your address!
                        @else
                            {{ $customer->street_address }}, <br/>
                            {{ (! empty($customer->apartment)) ? $customer->apartment.', ' : '' }}
                            {{ $customer->city }}, {{ $customer->state }}, {{ $customer->zip }},
                            {{ $customer->getCountryCode() }}
                        @endif
                    </span>
                    <br/>
                    <button class="small" onclick="showAddressPopup()">Update Address</button><br/>
                </div>
            </div>

            @if (shop_setting('instagram.token'))
                <div class="pull-right">
                    @if ($customer->instagram_id)
                        @if (isset($customer->instagram_data['username']))
                            Instagram Account Connected ({{ $customer->instagram_data['username'] }})
                            <br/>
                        @endif
                        <a data-loading-text="Connecting.." class="instagram-btn" href="/account/instagram-connect">Re-Connect Instagram</a>
                    @else
                        <span class="">Connect Your Instagram Account</span><br/>
                        <a data-loading-text="Connecting.." class="btn pink instagram-btn" href="/account/instagram-connect"><i class="fa fa-instagram"></i> Connect Instagram</a>
                    @endif
                </div>
            @endif

            <br style="clear: both;" />
        </div>

        @if ($waitlistCount > 0)
            <br/>
            <div class="alert alert-info">You have {{ $waitlistCount }} new waitlisted {{ str_plural('item', $waitlistCount) }}.</div>

        @endif

        <hr class="margin-top"/>
    </section>

    <section>
        <div class="unpaid-order row" style="z-index:10;">
            <div class="col-md-6 col-sm-6">
                <span class="section-title">Shopping Cart</span>
            </div>
        </div>

        @if (shop_id() == 'divas' || shop_id() == 'julesandjamesboutique')
            @if ($cart->hourFreeShipping)
                <br/>
                <div class="text-left">
                    <strong>Free shipping if you pay within:</strong><br />
                    <div data-expireAt="{{ $cart->hourFreeShippingExpireTime }}" id="hour-clock-shipping"></div>
                </div>
            @endif
        @endif

        @if (free_shipping_24hr_enabled() && ! $cart->hourFreeShipping)
            <br/>
            <div class="text-left">
                @if ($customer->orderedInLast24hours())
                    <strong>Free shipping if you pay within:</strong><br />
                    <div data-expireAt="{{ $customer->freeShippingExpireTime() }}" id="clock-shipping"></div>
                @else
                    @if (shop_id() == 'pinkcoconut')
                        <strong>Shipping is free for orders over $75. If not... Shipping is $3.95 on our FB Group. After you have PAID for your order and you want to place another order within 24 hours..... shipping will be free for that order! If you place an order within the next 24 hours your free shipping is reset for another 24 hours. This does not apply to our website as they are 2 different systems. Thank you for shopping PCB!</strong>
                    @else
                        <strong>No free shipping. You will get free shipping when you have a paid order in the last 24 hours.</strong>
                    @endif
                @endif
            </div>
            <br/>
        @endif

        @if (count($cart->items))
            <br/>
            @include('shop.cart')
        @else
            You have no products in your shopping cart at the moment.<br/><br/><br/>
        @endif
    </section>

    <br/><br/><br/>
    <section>
        <div class="unpaid-order row"><div class="col-md-6 col-sm-6"></div></div>
        <div id="accordion-ajax-paid" data-url="/api/account/paid-orders">
            <div class='accordion-head'>
                Paid Orders (click to view)
            </div>
            <div></div>
        </div>
    </section>

    <br/><br/><br/>
    <section>
        <div class="unpaid-order row"><div class="col-md-6 col-sm-6"></div></div>
        <div id="accordion-ajax-fulfilled" data-url="/api/account/fulfilled-orders">
            <div class='accordion-head'>
                Fulfilled Orders (click to view)
            </div>
            <div></div>
        </div>
    </section>

    <br/><br/><br/>
    <section>
        <div class="unpaid-order row"><div class="col-md-6 col-sm-6"></div></div>
        <div id="accordion-ajax-waitlist" data-url="/api/account/waitlist">
            <div class='accordion-head'>
                Waitlist Items / Backordered items (click to view)
            </div>
            <div class="col-xs-6">
                <select>
                    {{--@if (shop_setting('stripe.access_token'))--}}
                    @foreach ($customer->cards() as $card)
                        <option class="existing-card" value="{{ $card->card_id }}" data-description="Ending in: {{ $card->last_four }} Exp: {{ $card->exp_month }}/{{ $card->exp_year }}" data-image="{{ card_image($card->brand) }}">{{ $card->brand }}</option>
                    @endforeach
                    {{--@endif--}}
                </select>
            </div>
            <div>

            </div>
        </div>

    </section>

    @include('shop.update-address-modal')
    @if (! $customer->sawOnboardingPopup())
        {{-- treat case where send to messenger appears in both popup and cart --}}
        @include('shop.onboarding-modal')
    @endif
    @if ($hasMultipleLocations)
        @include('shop.local-pickup-locations-modal')
    @endif
</div>
@endsection

@section('scripts')
@if (shop_id() == 'glamourfarms')
    @include('shop.intercom')
@endif
<script type="text/javascript">
$(document).ready(function() {

    $('#modal-country').change(function() {
        var country = $(this).val();
        if (country == 'CA') {
            $('#modal-state-text').show().val('');
            $('#modal-state').hide();
        } else {
            $('#modal-state').show();
            $('#modal-state-text').hide();
        }
    });

    @if (! $customer->sawOnboardingPopup())
        $('#onboarding-modal').modal({
            backdrop: 'static',
            keyboard: false
        });
    @endif

    // Set up a global AJAX error handler to handle unauthorized responses.
    $.ajaxSetup({
        statusCode: {
            403: function() {
                alert('This action is unauthorized.');
                window.location.reload();
            },
            401: function() {
                alert('Your session has expired. Plese login again.');
                window.location.reload();
            }
        }
    });

    if ($('.instagram-btn').length) {
        $('.instagram-btn').on('click', function() {
            $(this).button('loading');
        });
    }

    // free shipping if you the last order is in the last 24 hours
    if ($('#clock-shipping').length) {
        $('#clock-shipping').countdown($('#clock-shipping').attr('data-expireAt') * 1000, function(event) {
            $('#clock-shipping').html(event.strftime('%H hr %M min %S sec'));
        });
    }

    // free shipping if you pay within 1 hour
    if ($('#hour-clock-shipping').length) {
        $('#hour-clock-shipping').countdown($('#hour-clock-shipping').attr('data-expireAt') * 1000, function(event) {
            $('#hour-clock-shipping').html(event.strftime('%H hr %M min %S sec'));
        });
    }

    $('.clock-expire').each(function() {
        var element = $(this);
        element.countdown(element.attr('data-expireAt') * 1000, function(event) {
            element.html(event.strftime('%I hr %M min %S sec'));
        });
    });

    // if there are items in the cart
    if ($('#paymentList').length) {

        @if ($localPickupEnabled && shop_id() == 'pinkcoconut' && $customer->local_pickup)
            var distanceOver50miles = false;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var lat = position.coords.latitude;
                    var lon = position.coords.longitude;

                    // Olive Branch, MS (latitude: 34.9639650, longitude: -89.8980760)
                    var distance = getDistanceFromLatLonInKm(lat, lon, 34.9639650, -89.8980760);

                    // if distance is greater than 50 miles (50 miles = aprox 80 km)
                    if (distance > 80) {
                        distanceOver50miles = true;
                    }
                });
            }
        @endif

        try {
            $("#paymentList").msDropDown();
        } catch(e) {}

        var stripe = StripeCheckout.configure({
            key: '{{ shop_setting('stripe.stripe_publishable_key') }}',
            // todo: add correct image logo for stripe, for each shop
            {{-- image: '{{ cdn('/images/icons/DD.png') }}', --}}
            locale: 'auto',
            allowRememberMe: false,
            token: function(token) {
                $('.overlay').show();
                checkoutWithNewCard(token.id);
            }
        });

        // Close Checkout on page navigation:
        window.addEventListener('popstate', function() {
            stripe.close();
        });

        // the checkout button
        $('#checkoutButton').on('click', function() {

            if (typeof distanceOver50miles !== 'undefined') {
                if (distanceOver50miles && ! confirm('Are you sure you want to do local pickup? You are more than 50 miles away.')) {
                    return;
                }
            }

            if (typeof confirmOptIn === "function") {
                confirmOptIn();
            }

            $('.overlay').show();
            var amount = $(this).attr('data-amount');
            var email = $(this).attr('data-email');

            // Check the select box to see what payment method to initiate
            var method = $('#paymentList').val();

            if (method == 'paypal') {
                checkoutWithPaypal();

            } else if (method == 'newcc') {
                // Add the card from stripe and charge it
                $('.overlay').hide();

                stripe.open({
                    name: '{{ shop_name() }}',
                    description: '', // todo: add shop description
                    email: email,
                    amount: amount * 100, // to cents
                    zipCode: true,
                    billingAddress: true,
                    allowRememberMe: false,
                });

            } else if (method == 'credit') {
                checkoutWithAccountCredit();

            } else if (method && method.match(/^card_.*/i)) {
                checkoutWithStripeCard(method);

            } else {
                alert('Error: Invalid payment method.');
                window.location.reload();
            }

        });

    }

    initAccordion('#accordion-ajax-paid');
    initAccordion('#accordion-ajax-fulfilled');
    initAccordion('#accordion-ajax-waitlist');
});

function showGiftCardBox() {
    $('#gift-card-box').show();
}

function checkGiftCard(e) {
    if (e.which == 13) {
        redeemGiftCard();
    }
}

function redeemGiftCard() {
    $.ajax({
        type:'post',
        url:'/api/account/apply-gift-card',
        data: {code: $('#gift-card-code').val()},
        success: function(response) {
            if (response.status == 1) {
                alert("Gift Card redeemed successfully!");
                location.reload();
            } else {
                alert('We are unable to validate this gift card!');
            }
        }
    });
}
</script>
@endsection
