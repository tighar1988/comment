@extends('admin.setup.setup')

@section('section', 'Auto-Scheduler')

@section('setup')
<?php $shopifyConnected = shop_setting('shopify.access_token'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <form class="form" role="form" method="POST" action="{{ url('/admin/setup/connect-shopify') }}">

                    @if ($shopifyConnected)
                        <span class="label label-success">CONNECTED</span>
                        <span class="settings-info">Your Shopify account is connected!</span><br/><br/>

                        @if (shop_setting('shopify.connecting'))
                            <div id="connecting-shop">
                                <i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>
                                Setting up your shop. This may take a few minutes...
                                <br/><br/>
                            </div>
                        @else
                            <a href="/admin/setup/shopify-unsync" data-title="Disconnect Shopify" data-body="Are you sure to disconnect Shopify? Your inventory won't be synced anymore." data-button="Disconnect" data-loading-text="Disconnecting.." class="btn btn-danger" onclick="return confirm_action(this);">Disconnect Shopify</a>
                            <hr/>
                        @endif

                        <span class="settings-info">Re-connect Shopify Account</span><br/>
                    @else
                        <span class="settings-info">Connect Your Shopify Account - this will import your Shopify Products</span><br/>
                    @endif

                    <div class="form-group @hasError('shopify_shop_name')">
                        <div class="row">
                             <div class="col-md-4">
                                <label class="control-label">Shopify Shop</label>
                                <div class="input-group">
                                    <span class="input-group-addon">https://</span>
                                    <input type="text" class="form-control" name="shopify_shop_name" maxlength="100" placeholder="shop">
                                    <span class="input-group-addon">.myshopify.com</span>
                                </div>
                                @error('shopify_shop_name')
                             </div>
                        </div>
                    </div>

                    <button class="btn btn-info" data-loading-text="Connecting Shopify.." type="submit">Connect Shopify</button>
                </form>

            </div>
        </div>
    </div>
</div>

@if ($shopifyConnected)
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <span class="settings-info">Send Orders to Shopify</span>

                <div class="checkbox-inline"><label><input type="checkbox" data-key="shopify.sync-orders" @if(shop_setting('shopify.sync-orders', false)) checked="" @endif class="toggle-switch"></label></div>

                <span class="settings-muted text-muted">
                    If you prefer fulfilling orders in shopify, you can enable this option and your orders will appear in Shopify. When you fulfill an order in Shopify, it will be fulfilled in CommentSold as well.
                </span>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@section('scripts')
<script type="text/javascript">

    var connected = false;
    function checkShopifyConnection()
    {
        $.ajax({
            type:'get',
            url:'/admin/setup/shopify-connected',
            success: function(response) {
                if (response.status == 1) {
                    connected = true;
                    $('#connecting-shop').html('<strong>Shop setup complete!</strong><br/><br/>');
                }
            }
        });

        if (! connected) {
            window.setTimeout(checkShopifyConnection, 2000);
        }
    }

    $(document).ready(function() {
        if ($('#connecting-shop').length) {
            window.setTimeout(checkShopifyConnection, 2000);
        }
    });
</script>
@endsection
