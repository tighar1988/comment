<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\Models\ShopUser;

class Checkout extends Drop
{
    protected $customer;

    public function __construct(ShopUser $customer = null)
    {
        $this->customer = $customer;
    }

    public function email()
    {
        $email = $this->customer->email ?? null;

        return old('email', session('checkout.address.email', $email));
    }

    public function first_name()
    {
        $firstName = null;
        if ($this->customer) {
            $firstName = $this->customer->firstName();
        }

        return old('first_name', session('checkout.address.first_name', $firstName));
    }

    public function last_name()
    {
        $lastName = null;
        if ($this->customer) {
            $lastName = $this->customer->lastName();
        }

        return old('last_name', session('checkout.address.last_name', $lastName));
    }

    public function street_address()
    {
        $address1 = $this->customer->street_address ?? null;

        return old('address1', session('checkout.address.address1', $address1));
    }

    public function apartment()
    {
        $address2 = $this->customer->apartment ?? null;

        return old('address2', session('checkout.address.address2', $address2));
    }

    public function city()
    {
        $city = $this->customer->city ?? null;

        return old('city', session('checkout.address.city', $city));
    }

    public function country_code()
    {
        $countryCode = 'US';
        if ($this->customer) {
            $countryCode = $this->customer->getCountryCode();
        }

        return old('country_code', session('checkout.address.country_code', $countryCode));
    }

    public function country()
    {
        $countryCode = $this->country_code();
        if ($countryCode == 'CA') {
            return 'Canada';
        }

        return $countryCode;
    }

    public function zip()
    {
        $zip = $this->customer->zip ?? null;

        return old('zip', session('checkout.address.zip', $zip));
    }

    public function phone()
    {
        $phoneNumber = $this->customer->phone_number ?? null;

        return old('phone_number', session('checkout.address.phone', $phoneNumber));
    }

    public function has_countries()
    {
        return shop_setting('shop.has-multiple-countries', true);
    }

    public function state()
    {
        $countryCode = $this->country_code();
        $state = $this->customer->state ?? null;

        if ($countryCode == 'US') {
            return old('state', session('checkout.address.state', $state));
        } else {
            return old('region', session('checkout.address.region', $state));
        }
    }

    public function buyer_accepts_marketing()
    {
        return old('buyer_accepts_marketing', session('checkout.address.buyer_accepts_marketing', 1));
    }

    public function states()
    {
        return [
            ['code' => 'AL', 'name' => 'Alabama'],
            ['code' => 'AK', 'name' => 'Alaska'],
            ['code' => 'AS', 'name' => 'American Samoa'],
            ['code' => 'AZ', 'name' => 'Arizona'],
            ['code' => 'AR', 'name' => 'Arkansas'],
            ['code' => 'AA', 'name' => 'Armed Forces America'],
            ['code' => 'AE', 'name' => 'Armed Forces Europe'],
            ['code' => 'AP', 'name' => 'Armed Forces Pacific'],
            ['code' => 'CA', 'name' => 'California'],
            ['code' => 'CO', 'name' => 'Colorado'],
            ['code' => 'CT', 'name' => 'Connecticut'],
            ['code' => 'DE', 'name' => 'Delaware'],
            ['code' => 'DC', 'name' => 'District Of Columbia'],
            ['code' => 'FL', 'name' => 'Florida'],
            ['code' => 'GA', 'name' => 'Georgia'],
            ['code' => 'GU', 'name' => 'Guam'],
            ['code' => 'HI', 'name' => 'Hawaii'],
            ['code' => 'ID', 'name' => 'Idaho'],
            ['code' => 'IL', 'name' => 'Illinois'],
            ['code' => 'IN', 'name' => 'Indiana'],
            ['code' => 'IA', 'name' => 'Iowa'],
            ['code' => 'KS', 'name' => 'Kansas'],
            ['code' => 'KY', 'name' => 'Kentucky'],
            ['code' => 'LA', 'name' => 'Louisiana'],
            ['code' => 'ME', 'name' => 'Maine'],
            ['code' => 'MD', 'name' => 'Maryland'],
            ['code' => 'MA', 'name' => 'Massachusetts'],
            ['code' => 'MI', 'name' => 'Michigan'],
            ['code' => 'MN', 'name' => 'Minnesota'],
            ['code' => 'MS', 'name' => 'Mississippi'],
            ['code' => 'MO', 'name' => 'Missouri'],
            ['code' => 'MT', 'name' => 'Montana'],
            ['code' => 'NE', 'name' => 'Nebraska'],
            ['code' => 'NV', 'name' => 'Nevada'],
            ['code' => 'NH', 'name' => 'New Hampshire'],
            ['code' => 'NJ', 'name' => 'New Jersey'],
            ['code' => 'NM', 'name' => 'New Mexico'],
            ['code' => 'NY', 'name' => 'New York'],
            ['code' => 'NC', 'name' => 'North Carolina'],
            ['code' => 'ND', 'name' => 'North Dakota'],
            ['code' => 'MP', 'name' => 'Northern Mariana Is'],
            ['code' => 'NS', 'name' => 'Nova Scotia'],
            ['code' => 'OH', 'name' => 'Ohio'],
            ['code' => 'OK', 'name' => 'Oklahoma'],
            ['code' => 'OR', 'name' => 'Oregon'],
            ['code' => 'PW', 'name' => 'Palau'],
            ['code' => 'PA', 'name' => 'Pennsylvania'],
            ['code' => 'PR', 'name' => 'Puerto Rico'],
            ['code' => 'RI', 'name' => 'Rhode Island'],
            ['code' => 'SC', 'name' => 'South Carolina'],
            ['code' => 'SD', 'name' => 'South Dakota'],
            ['code' => 'TN', 'name' => 'Tennessee'],
            ['code' => 'TX', 'name' => 'Texas'],
            ['code' => 'UT', 'name' => 'Utah'],
            ['code' => 'VT', 'name' => 'Vermont'],
            ['code' => 'VI', 'name' => 'Virgin Islands'],
            ['code' => 'VA', 'name' => 'Virginia'],
            ['code' => 'WA', 'name' => 'Washington'],
            ['code' => 'WV', 'name' => 'West Virginia'],
            ['code' => 'WI', 'name' => 'Wisconsin'],
            ['code' => 'WY', 'name' => 'Wyoming'],
        ];
    }

    public function errors()
    {
        if (session()->has('errors')) {
            return new ErrorBag(session()->get('errors'));
        }

        return null;
    }

    public function step()
    {
        if (request()->get('step') == 'shipping') {
            return 'shipping';
        }

        if (request()->get('step') == 'payment') {
            return 'payment';
        }

        return 'address';
    }

    public function card_years()
    {
        $years = [];
        $year = date('Y');

        for ($i = 0; $i < 16; $i++) {
            $years[] = $year + $i;
        }

        return $years;
    }

    public function coupon_code()
    {
        if (! empty(session('shopping-cart.coupon')) && ! empty(session('shopping-cart.coupon-code'))) {
            return session('shopping-cart.coupon-code');
        }

        return null;
    }

    public function stripe_key()
    {
        return config('services.stripe.key');
    }
}
