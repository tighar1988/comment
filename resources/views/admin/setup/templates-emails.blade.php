@extends('admin.setup.templates')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">

                <span class="settings-info">Email Template for Awarding Account Credit</span>

                <form class="" role="form" method="POST" action="{{ url('/admin/setup/templates/update') }}">
                    <div class="form-group @hasError('awarded_account_credit')">
                        <label class="control-label">Template</label>

                        <input type="hidden" name="key" value="awarded_account_credit">
                        <textarea class="form-control" rows="10" name="awarded_account_credit">{{ old('awarded_account_credit', template('awarded-account-credit')) }}</textarea>

                        <blockquote style="margin-top: 5px;">
                            <table class="table table-condensed">
                                <caption>Available variables for this email template:</caption>
                                <tr>
                                    <th>Variable</th>
                                    <th>Example Value</th>
                                </tr>
                                <tr>
                                    <td><code>@{{ shopName }}</code></td>
                                    <td>My Shop</td>
                                </tr>
                                <tr>
                                    <td><code>@{{ customerName }}</code></td>
                                    <td>John Doe</td>
                                </tr>
                                <tr>
                                    <td><code>@{{ shopUrl }}</code></td>
                                    <td>https://my-shop.commentsold.com/account</td>
                                </tr>
                                <tr>
                                    <td><code>@{{ amount }}</code></td>
                                    <td>10.00</td>
                                </tr>
                            </table>
                        </blockquote>
                        @error('awarded_account_credit')
                    </div>

                    <button class="btn btn-success" data-loading-text="Updating Template.." type="submit">Update Template</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
