<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopSettings extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
    ];

    public function getSetting($key)
    {
        return static::where('key', $key)->first()->value ?? null;
    }

    public static function repostGroups()
    {
        $groups = json_decode(shop_setting('repost.facebook-groups'));
        $groups = is_array($groups) ? $groups : [];
        return array_unique($groups);
    }

    public static function repostTimes()
    {
        $times = json_decode(shop_setting('repost.times'));
        $times = is_array($times) ? $times : [];

        usort($times, function($a, $b) {
            if ($a->ampm != $b->ampm) {
                return $a->ampm > $b->ampm;
            }

            if ($a->hour != $b->hour) {
                return $a->hour > $b->hour;
            }

            return $a->min > $b->min;
        });

        return $times;
    }
}
