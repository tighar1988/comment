<?php

namespace Tests\Unit\Facebook;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Facebook\ShopperComment;
use App\Models\Inventory;
use Mockery;

class ShopperCommentTest extends TestCase
{
    /**
     * Helper test function.
     */
    public static function build($commentText = 'sold medium blue')
    {
        $from = Mockery::mock('Facebook\GraphNodes\Collection');
        $from->shouldReceive('getField')->with('name')->andReturn('John Doe');
        $from->shouldReceive('getField')->with('id')->andReturn('232323232323232');

        $comment = Mockery::mock('Facebook\GraphNodes\Collection');
        $comment->shouldReceive('getField')->with('parent')->andReturn(null);
        $comment->shouldReceive('getField')->with('id')->andReturn('1234567890');
        $comment->shouldReceive('getField')->with('created_time')->andReturn(new \DateTime('NOW'));
        $comment->shouldReceive('getField')->with('message')->andReturn($commentText);
        $comment->shouldReceive('getField')->with('from')->andReturn($from);

        return ShopperComment::create($comment);
    }

    /** @test */
    public function it_is_initializable()
    {
        $comment = static::build();
        $this->assertInstanceOf(ShopperComment::class, $comment);
    }

    /** @test */
    public function it_commented_sold()
    {
        $comment = static::build('sold medium blue');
        $this->assertTrue($comment->commentedSold());
    }

    /** @test */
    public function it_commented_uppercase_sold()
    {
        $comment = static::build('SOLD medium blue');
        $this->assertTrue($comment->commentedSold());
    }

    /** @test */
    public function it_did_not_commented_sold()
    {
        $comment = static::build('What a nice day.');
        $this->assertFalse($comment->commentedSold());
    }

    /** @test */
    public function it_has_no_email()
    {
        $comment = static::build('sold medium blue');
        $this->assertNull($comment->extractEmail());
    }

    /** @test */
    public function it_extracts_email_correctly()
    {
        $comment = static::build('Sold. 11/12. john.doe@comcast.net');
        $this->assertEquals($comment->extractEmail(), 'john.doe@comcast.net');
    }

    /** @test */
    public function it_extracts_normal_email()
    {
        $comment = static::build('Sold. 11/12. john.doe@comcast.net');
        $this->assertEquals($comment->extractEmail(), 'john.doe@comcast.net');
    }

    /** @test */
    public function it_extracts_email_with_uppercase_letters()
    {
        $comment = static::build('10/12 sold John.k.Doe@gmail.com');
        $this->assertEquals($comment->extractEmail(), 'John.k.Doe@gmail.com');
    }

    /** @test */
    public function it_extracts_email_with_new_line_before()
    {
        $comment = static::build("14/16 sold\nJen.Doe@example.com");
        $this->assertEquals($comment->extractEmail(), 'Jen.Doe@example.com');
    }

    /** @test */
    public function it_extracts_email_with_new_line_after()
    {
        $comment = static::build("12/16 Jane.Doe@example.com\nsold");
        $this->assertEquals($comment->extractEmail(), 'Jane.Doe@example.com');
    }

    /** @test */
    public function it_extracts_email_with_on_the_third_line()
    {
        $comment = static::build("sold\n12/16\njenny@mysite.net");
        $this->assertEquals($comment->extractEmail(), 'jenny@mysite.net');
    }

    /** @test */
    public function it_extracts_email_with_two_spaces_before_and_after()
    {
        $comment = static::build('sold purple  jenny@mysite.net  ');
        $this->assertEquals($comment->extractEmail(), 'jenny@mysite.net');
    }

    /** @test */
    public function it_extracts_email_with_space_and_newline_before()
    {
        $comment = static::build("sold purple  \njohn@gmail.com");
        $this->assertEquals($comment->extractEmail(), 'john@gmail.com');
    }

    /** @test */
    public function it_extracts_email_with_comma_before()
    {
        $comment = static::build('sold 10/12,john@gmail.com');
        $this->assertEquals($comment->extractEmail(), 'john@gmail.com');
    }

    /** @test */
    public function it_extracts_email_with_comma_and_space_after()
    {
        $comment = static::build('Sold 10/12 carrie@gmail.com, pick up');
        $this->assertEquals($comment->extractEmail(), 'carrie@gmail.com');
    }

    /** @test */
    public function it_extracts_email_with_comma_and_word_after()
    {
        $comment = static::build('Sold 10/12 carrie@gmail.com,pick up');
        $this->assertEquals($comment->extractEmail(), 'carrie@gmail.com');
    }

    /** @test */
    public function it_extracts_email_with_comma_before_and_after()
    {
        $comment = static::build('Sold 10/12 turquoise,Stacy.johnes@hotmail.com,ship');
        $this->assertEquals($comment->extractEmail(), 'Stacy.johnes@hotmail.com');
    }

    /** @test */
    public function it_extracts_email_with_space_and_dot_after()
    {
        $comment = static::build('sold. jane@gmail.com. brown.');
        $this->assertEquals($comment->extractEmail(), 'jane@gmail.com');
    }

    /** @test */
    public function it_extracts_email_with_space_and_dot_before()
    {
        $comment = static::build('sold. brown .jane@gmail.com');
        $this->assertEquals($comment->extractEmail(), 'jane@gmail.com');
    }

    /** @test */
    public function it_extracts_email_with_space_and_dot_before_and_after()
    {
        $comment = static::build('sold 10/12 .tom.johnes@yahoo.com.');
        $this->assertEquals($comment->extractEmail(), 'tom.johnes@yahoo.com');
    }

    /** @test */
    public function it_extracts_email_in_single_quotes()
    {
        $comment = static::build("sold 10/12 'tom.johnes@yahoo.com'");
        $this->assertEquals($comment->extractEmail(), 'tom.johnes@yahoo.com');
    }

    /** @test */
    public function it_extracts_email_in_double_quotes()
    {
        $comment = static::build('sold 10/12 "tom.johnes@yahoo.com"');
        $this->assertEquals($comment->extractEmail(), 'tom.johnes@yahoo.com');
    }

    /** @test */
    public function it_extracts_email_in_parenthesis()
    {
        $comment = static::build('sold 10/12 (tom.johnes@yahoo.com)');
        $this->assertEquals($comment->extractEmail(), 'tom.johnes@yahoo.com');
    }

    /** @test */
    public function it_matches_single_variant_that_has_no_options()
    {
        $comment = static::build('sold john@yahoo.com');
        $v1 = new Inventory;

        $this->assertEquals($comment->findVariantMatch(static::product($v1)), $v1);
    }

    /** @test */
    public function it_matches_single_variant_when_the_single_option_is_specified()
    {
        $comment = static::build('sold Blue john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1)), $v1);
    }

    /** @test */
    public function it_matches_single_variant_when_options_are_omitted()
    {
        $comment = static::build('sold john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1)), $v1);
    }

    /** @test */
    public function it_matches_variant_with_only_colors()
    {
        $comment = static::build('sold green john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue']);
        $v2 = new Inventory(['color' => 'green']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_does_not_match_variants_with_only_colors_when_option_is_missing()
    {
        $comment = static::build('sold john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue']);
        $v2 = new Inventory(['color' => 'green']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), false);
    }

    /** @test */
    public function it_does_not_match_variants_with_one_size_one_color_when_options_are_missing()
    {
        $comment = static::build('sold john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue', 'size' => 'Medium']);
        $v2 = new Inventory(['color' => 'green', 'size' => 'Large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), false);
    }

    /** @test */
    public function it_matches_variants_when_correct_options_specified()
    {
        $comment = static::build('sold green medium john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue', 'size' => 'Medium']);
        $v2 = new Inventory(['color' => 'blue', 'size' => 'Large']);
        $v3 = new Inventory(['color' => 'green', 'size' => 'Medium']);
        $v4 = new Inventory(['color' => 'green', 'size' => 'Large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3, $v4)), $v3);
    }

    /** @test */
    public function it_matches_variant_with_a_single_second_option_when_both_options_are_specified()
    {
        $comment = static::build('sold green medium john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue', 'size' => 'Medium']);
        $v2 = new Inventory(['color' => 'green', 'size' => 'Medium']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_matches_variant_with_a_single_second_option_when_second_option_is_omitted()
    {
        $comment = static::build('sold green john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue', 'size' => 'Medium']);
        $v2 = new Inventory(['color' => 'green', 'size' => 'Medium']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_matches_variant_with_xl_sizes()
    {
        $comment = static::build('sold 3xl john@yahoo.com');
        $v1 = new Inventory(['size' => 'xl']);
        $v2 = new Inventory(['size' => '2xl']);
        $v3 = new Inventory(['size' => '3xl']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_med_for_medium()
    {
        $comment = static::build('sold med john@yahoo.com');
        $v1 = new Inventory(['size' => 'Small']);
        $v2 = new Inventory(['size' => 'Medium']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_matches_lg_for_large()
    {
        $comment = static::build('sold lg john@yahoo.com');
        $v1 = new Inventory(['size' => 'Small']);
        $v2 = new Inventory(['size' => 'Medium']);
        $v3 = new Inventory(['size' => 'Large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_1x_for_1xl()
    {
        $comment = static::build('sold 1x john@yahoo.com');
        $v1 = new Inventory(['size' => '1XL']);
        $v2 = new Inventory(['size' => '2XL']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v1);
    }

    /** @test */
    public function it_matches_variant_with_s_m_l_sizes()
    {
        $comment = static::build('sold L john@yahoo.com');
        $v1 = new Inventory(['size' => 'S']);
        $v2 = new Inventory(['size' => 'M']);
        $v3 = new Inventory(['size' => 'L']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_grey_for_gray()
    {
        $comment = static::build('sold grey john@yahoo.com');
        $v1 = new Inventory(['color' => 'gray']);
        $v2 = new Inventory(['color' => 'blue']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v1);
    }

    /** @test */
    public function it_matches_gray_for_grey()
    {
        $comment = static::build('sold GRAY john@yahoo.com');
        $v1 = new Inventory(['color' => 'blue']);
        $v2 = new Inventory(['color' => 'grey']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_matches_variant_with_m_when_blue_is_present()
    {
        $comment = static::build('sold blue m john@yahoo.com');
        $v1 = new Inventory(['color' => 'red', 'size' => 'S']);
        $v2 = new Inventory(['color' => 'red', 'size' => 'M']);
        $v3 = new Inventory(['color' => 'red', 'size' => 'L']);
        $v4 = new Inventory(['color' => 'blue', 'size' => 'S']);
        $v5 = new Inventory(['color' => 'blue', 'size' => 'M']);
        $v6 = new Inventory(['color' => 'blue', 'size' => 'L']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3, $v4, $v5, $v6)), $v5);
    }

    /** @test */
    public function it_matches_variant_with_xl_at_the_end()
    {
        $comment = static::build('sold XL');

        $v1 = new Inventory(['size' => 'small']);
        $v2 = new Inventory(['size' => 'medium']);
        $v3 = new Inventory(['size' => 'large']);
        $v4 = new Inventory(['size' => 'xl']);
        $v5 = new Inventory(['size' => '2xl']);
        $v6 = new Inventory(['size' => '3xl']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3, $v4, $v5, $v6)), $v4);
    }

    /** @test */
    public function it_matches_variant_with_s_and_with_email_at_the_end()
    {
        $comment = static::build('sold S example@gmail.com');

        $v1 = new Inventory(['size' => 'L']);
        $v2 = new Inventory(['size' => 'M']);
        $v3 = new Inventory(['size' => 'S']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_variant_with_m_with_l_at_the_end()
    {
        $comment = static::build('sold M teal');

        $v1 = new Inventory(['size' => 'L']);
        $v2 = new Inventory(['size' => 'M']);
        $v3 = new Inventory(['size' => 'S']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v2);
    }

    /** @test */
    public function it_matches_variant_that_starts_with_xl()
    {
        $comment = static::build('xl sold blue');

        $v1 = new Inventory(['size' => 'M']);
        $v2 = new Inventory(['size' => 'L']);
        $v3 = new Inventory(['size' => 'XL']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_variant_that_has_slash()
    {
        $comment = static::build('sold 1/3 blue');
        $v1 = new Inventory(['size' => '1/2']);
        $v2 = new Inventory(['size' => '1/3']);
        $v3 = new Inventory(['size' => '1/4']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v2);
    }

    /** @test */
    public function it_matches_case_insensitive_single_color()
    {
        $comment = static::build('sold large');
        $v1 = new Inventory(['color' => 'Sage', 'size' => 'small']);
        $v2 = new Inventory(['color' => 'sage', 'size' => 'large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_matches_dark_olive_when_olive_exists()
    {
        $comment = static::build('sold large dark olive');
        $v1 = new Inventory(['color' => 'olive', 'size' => 'large']);
        $v2 = new Inventory(['color' => 'dark olive', 'size' => 'large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_matches_olive_when_dark_olive_exists()
    {
        $comment = static::build('sold large olive');
        $v1 = new Inventory(['color' => 'dark olive', 'size' => 'large']);
        $v2 = new Inventory(['color' => 'olive', 'size' => 'large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v2);
    }

    /** @test */
    public function it_matches_olive_when_dark_olive_does_exist()
    {
        $comment = static::build('sold large dark olive');
        $v1 = new Inventory(['color' => 'olive', 'size' => 'large']);
        $v2 = new Inventory(['color' => 'pink', 'size' => 'large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v1);
    }

    /** @test */
    public function it_matches_number_sizes()
    {
        $comment = static::build('Sold 7 brown');
        $v1 = new Inventory(['size' => '5']);
        $v2 = new Inventory(['size' => '5.6']);
        $v3 = new Inventory(['size' => '6']);
        $v4 = new Inventory(['size' => '6.5']);
        $v5 = new Inventory(['size' => '7']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3, $v4, $v5)), $v5);
    }

    /** @test */
    public function it_matches_8_point_5()
    {
        $comment = static::build('Sold!!! 8.5 amymarieb629@gmail.com');
        $v1 = new Inventory(['color' => '7']);
        $v2 = new Inventory(['color' => '8']);
        $v3 = new Inventory(['color' => '8.5']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_small_for_s_variant()
    {
        $comment = static::build('sold small');
        $v1 = new Inventory(['size' => 'S']);
        $v2 = new Inventory(['size' => 'M']);
        $v3 = new Inventory(['size' => 'L']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v1);
    }

    /** @test */
    public function it_matches_medium_for_m_variant()
    {
        $comment = static::build('sold medium');
        $v1 = new Inventory(['size' => 'S']);
        $v2 = new Inventory(['size' => 'M']);
        $v3 = new Inventory(['size' => 'L']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v2);
    }

    /** @test */
    public function it_matches_large_for_l_variant()
    {
        $comment = static::build('sold large');
        $v1 = new Inventory(['size' => 'S']);
        $v2 = new Inventory(['size' => 'M']);
        $v3 = new Inventory(['size' => 'L']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_small()
    {
        $comment = static::build('Sold small baby_kakes_2005@yahoo.com');
        $v1 = new Inventory(['size' => 'small']);
        $v2 = new Inventory(['size' => 'med']);
        $v3 = new Inventory(['size' => 'large']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v1);
    }

    /** @test */
    public function it_matches_pound_sign()
    {
        $comment = static::build('Sold #21');
        $v1 = new Inventory(['size' => '21']);
        $v2 = new Inventory(['size' => '22']);
        $v3 = new Inventory(['size' => '23']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v1);
    }

    /** @test */
    public function it_matches_with_period_and_no_spaces()
    {
        $comment = static::build('Sold.Regular.johndoe@gmail.com');
        $v1 = new Inventory(['size' => 'Regular']);
        $v2 = new Inventory(['size' => 'Medium']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2)), $v1);
    }

    /** @test */
    public function it_matches_with_dashes()
    {
        $comment = static::build('Sold/striped/s/ johndoe@example.net');
        $v1 = new Inventory(['size' => 'S', 'color' => 'Denver']);
        $v2 = new Inventory(['size' => 'M', 'color' => 'Denver']);
        $v3 = new Inventory(['size' => 'S', 'color' => 'Striped']);
        $v4 = new Inventory(['size' => 'M', 'color' => 'Striped']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3, $v4)), $v3);
    }

    /** @test */
    public function it_matches_with_email()
    {
        $comment = static::build('Sold 2XL Paulasmallwooddobs@gmail.com');
        $v1 = new Inventory(['size' => 'Small']);
        $v2 = new Inventory(['size' => 'Medium']);
        $v3 = new Inventory(['size' => '2XL']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_xl_for_xlarge()
    {
        $comment = static::build('Sold xl james@gmail.com');
        $v1 = new Inventory(['size' => 'MEDIUM']);
        $v2 = new Inventory(['size' => 'LARGE']);
        $v3 = new Inventory(['size' => 'XLARGE']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_matches_xlarge_for_xl()
    {
        $comment = static::build('Sold.xlarge.tom@yahoo.com');
        $v1 = new Inventory(['size' => 'M']);
        $v2 = new Inventory(['size' => 'L']);
        $v3 = new Inventory(['size' => 'XL']);

        $this->assertEquals($comment->findVariantMatch(static::product($v1, $v2, $v3)), $v3);
    }

    /** @test */
    public function it_creates_from_webhook_comment()
    {
        $comment = ShopperComment::createFromWebhook([
            'sender_name'  => 'John Doe',
            'sender_id'    => 196242312464794,
            'comment_id'   => '305964449740374_506523963221560',
            'parent_id'    => '455840964418394_505994489740374',
            'created_time' => 1500405927,
            'message'      => 'hello',
            'post_id'      => '',
        ]);

        $this->assertInstanceOf(ShopperComment::class, $comment);
    }

    /** @test */
    public function it_matches_3xl()
    {
        $comment = static::build('Sold, 3XL, Red');

        $v1 = new Inventory(['color' => 'RED', 'size' => 'SMALL']);
        $v2 = new Inventory(['color' => 'RED', 'size' => 'MED']);
        $v3 = new Inventory(['color' => 'RED', 'size' => 'LARGE']);
        $v4 = new Inventory(['color' => 'RED', 'size' => 'XLARGE']);
        $v5 = new Inventory(['color' => 'RED', 'size' => '2XL']);
        $v6 = new Inventory(['color' => 'RED', 'size' => '3XL']);

        $v7 = new Inventory(['color' => 'GREY', 'size' => 'SMALL']);
        $v8 = new Inventory(['color' => 'GREY', 'size' => 'MED']);
        $v9 = new Inventory(['color' => 'GREY', 'size' => 'LARGE']);
        $v10 = new Inventory(['color' => 'GREY', 'size' => 'XLARGE']);
        $v11 = new Inventory(['color' => 'GREY', 'size' => '2XL']);
        $v12 = new Inventory(['color' => 'GREY', 'size' => '3XL']);

        $product = static::product($v1, $v2, $v3, $v4, $v5, $v6, $v7, $v8, $v9, $v10, $v11, $v12);
        $this->assertEquals($comment->findVariantMatch($product), $v6);
    }

    /** @test */
    public function it_does_not_matches_1xs()
    {
        $comment = static::build('Sold, 1XS, blue');

        $v1 = new Inventory(['color' => 'Blue', 'size' => 'Medium']);
        $v2 = new Inventory(['color' => 'Blue', 'size' => '1XS']);

        $product = static::product($v1, $v2);
        $this->assertEquals($comment->findVariantMatch($product), $v2);
    }

    /** @test */
    public function it_does_not_match_1xl_for_1xs()
    {
        $comment = static::build('Sold, 1XS, blue');

        $v1 = new Inventory(['color' => 'Blue', 'size' => '1XL']);
        $v2 = new Inventory(['color' => 'Blue', 'size' => '1XS']);

        $product = static::product($v1, $v2);
        $this->assertEquals($comment->findVariantMatch($product), $v2);
    }
}
