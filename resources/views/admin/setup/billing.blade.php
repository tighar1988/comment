@extends('admin.setup.setup')

@section('section', 'Billing')

@section('setup')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                @if (shop_setting('stripe.access_token'))
                    <span class="label label-success">CONNECTED</span>
                    <span class="settings-info">Your Stripe account is connected!</span><br/><br/>

                    <span class="settings-info">Re-connect Stripe Account</span><br/>
                @else
                    <span class="settings-info">Connect Your Stripe Account</span><br/>
                @endif
                <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id={{ config('services.stripe.client_id') }}&scope=read_write"><img src="{{ cdn('/images/blue-on-light.png') }}"></a>
            </div>
        </div>
    </div>
</div>

@if (empty(shop_setting('paypal.client_id')))
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <?php $paypalEmail = shop_setting('paypal.identity.email'); ?>
                @if ($paypalEmail)
                    <span class="label label-success">CONNECTED</span>
                    <span class="settings-info">Your PayPal account is connected! (<strong>{{ $paypalEmail }}</strong>)</span><br/>
                    <br/>

                    <span class="settings-info">Re-connect PayPal Account</span>
                @else
                    <span class="settings-info">Connect Your PayPal Account</span>
                @endif
                <br/>
                <a href="/admin/setup/paypal-identity"><img src="https://www.paypalobjects.com/webstatic/en_US/developer/docs/lipp/loginwithpaypalbutton.png"></a>
            </div>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form class="form" role="form" method="POST" action="{{ url('/admin/setup/paypal-keys') }}">
                    <span class="settings-info">Setup your PayPal API keys. You can find them in your PayPal dashboard<br/>(message us if you need help)</span>

                    <div class="form-group @hasError('client_id')">
                        <label class="control-label">PayPal Client ID</label>
                        <input type="text" class="form-control" name="client_id" value="{{ old('client_id', shop_setting('paypal.client_id')) }}">
                        @error('client_id')
                    </div>

                    {{-- todo: for security reasons, hide the client secret, show only the first few characters, then show '****' --}}
                    <div class="form-group @hasError('client_secret')">
                        <label class="control-label">PayPal Client Secret</label>
                        <input type="text" class="form-control" name="client_secret" value="{{ old('client_secret', shop_setting('paypal.client_secret')) }}">
                        @error('client_secret')
                    </div>

                    <button class="btn btn-success" type="submit">Update PayPal Keys</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endif
@endsection
