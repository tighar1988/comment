<?php

namespace App\ShoppingCart;

class CartItem
{
    /**
     * Cart item ID.
     *
     * @var int
     */
    public $id;

    public $name;

    public $product_type;

    public $price;

    public $sale_price;

    public $cost;

    public $image;

    public $color;

    public $size;

    public $created_at;

    public $product_id;

    public $inventory_id;

    public $comment_id;

    public $order_source;

    public $product_style;

    public $product_brand;

    public $product_brand_style;

    public $product_description;

    public $weight;

    public $shopify_inventory_id;

    public $charge_taxes;

    public function __construct($item)
    {
        // init the data
        $this->id           = $item->id ?? null;
        $this->name         = $item->variant->product->product_name ?? null;
        $this->product_type = $item->variant->product->product_type ?? null;
        $this->price        = $item->variant->price ?? null;
        $this->sale_price   = $item->variant->sale_price ?? null;
        $this->cost         = $item->variant->cost ?? null;
        $this->image        = $item->variant->product->image->filename ?? null;
        $this->color        = $item->variant->color ?? null;
        $this->size         = $item->variant->size ?? null;
        $this->created_at   = $item->created_at ?? null;
        $this->product_id   = $item->variant->product->id ?? null;
        $this->inventory_id = $item->variant->id ?? null;
        $this->comment_id   = $item->comment_id ?? null;
        $this->order_source = $item->order_source ?? null;

        // add extra data for the product
        $this->product_style       = $item->variant->product->style ?? null;
        $this->product_brand       = $item->variant->product->brand ?? null;
        $this->product_brand_style = $item->variant->product->brand_style ?? null;
        $this->product_description = $item->variant->product->description ?? null;
        $this->weight              = $item->variant->weight ?? null;
        $this->shopify_inventory_id = $item->variant->shopify_inventory_id ?? null;

        $this->charge_taxes = $item->variant->product->charge_taxes ?? null;
        if (is_null($this->charge_taxes)) {
            $this->charge_taxes = true; // charge taxes for old products
        }

        // format the data
        $this->price = floatval($this->price);
        $this->created_at = strtotime($this->created_at);
    }

    public function expireAt($hours)
    {
        return $this->created_at + (60 * 60 * $hours);
    }

    public function price()
    {
        if (! is_null($this->sale_price) && is_numeric($this->sale_price) && ($this->sale_price > 0) && ($this->sale_price < $this->price)) {
            return $this->sale_price;
        }

        return $this->price;
    }
}
