<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayPalComissionCharge extends Model
{
    /**
     * The charge was successful.
     *
     * @var integer
     */
    const STATUS_PAID = 1;

    /**
     * The charge failed, and we show the reason to the customer.
     *
     * @var integer
     */
    const STATUS_FAILED = 2;

    /**
     * The charge failed because a system issue and we don't show the message to the customer.
     *
     * @var integer
     */
    const STATUS_FAILED_HIDDEN = 3;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'paypal_comission_charges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'start_date',
        'end_date',
        'status',
        'details',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'json',
    ];

    public function isPaid()
    {
        return $this->status == static::STATUS_PAID;
    }

    public function cardBrand()
    {
        return $this->details['card_brand'] ?? null;
    }

    public function cardLast4()
    {
        return $this->details['card_last_four'] ?? null;
    }

    public function ordersTotal()
    {
        return $this->details['orders_total'] ?? null;
    }

    public function feePercentage()
    {
        return $this->details['fee_percentage'] ?? null;
    }

    public function getError()
    {
        return $this->details['error'] ?? null;
    }

    public function scopeVisible($query)
    {
        return $query->where('status', static::STATUS_PAID)
            ->orWhere('status', static::STATUS_FAILED);
    }

    public static function logPaid($job)
    {
        static::log($job, static::STATUS_PAID);
    }

    public static function logFailed($job, $error)
    {
        static::log($job, static::STATUS_FAILED, ['error' => $error]);
    }

    public static function logFailedHidden($job, $error)
    {
        static::log($job, static::STATUS_FAILED_HIDDEN, ['error' => $error]);
    }

    public static function log($job, $status, $extra = [])
    {
        $details = [
            'card_brand'     => ($job->shopOwner->card_brand ?? null),
            'card_last_four' => ($job->shopOwner->card_last_four ?? null),
            'orders_total'   => $job->ordersTotal,
            'fee_percentage' => $job->feePercentage,
        ];

        $details = array_merge($details, $extra);

        static::create([
            'amount'     => $job->feesTotal,
            'start_date' => $job->lastTime,
            'end_date'   => $job->newLastTime,
            'status'     => $status,
            'details'    => $details,
        ]);
    }
}
