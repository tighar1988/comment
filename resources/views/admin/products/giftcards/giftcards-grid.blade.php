@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Gift Cards</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Gift Cards</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">
                     <div class="table-responsive">
                        <table id="giftcards" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Customer</th>
                                    <th>Expires</th>
                                    <th>Value</th>
                                    <th>Status</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#giftcards').DataTable({
            "oLanguage" : {"sEmptyTable": "No gift cards found. Gift cards will appear here after their order is paid."},
            "processing": true,
            "serverSide": true,
            "order": [[ 0, "desc" ]],
            "ajax": "/admin/giftcards/datatable",
            "bStateSave": true,
            columns: [
                { data: 'code', 'searchable': false, render: function(data, type, row) {
                    return '**** **** **** ' + row.code;
                }},
                { data: 'customer_name'},
                { data: 'expires'},
                { data: 'value'},
                { data: 'used', 'searchable': false, render: function(data, type, row) {
                    var status = '';
                    if (row.disabled == '1') {
                        status += '<span class="label label-warning">Disabled</span>';
                    }
                    if (row.used == '1') {
                        status += ' <span class="label label-danger">Used</span>';
                    }
                    return status;
                }},
                { data: null, 'searchable': false, 'orderable': false, className: 'text-center', render: function(data, type, row) {
                    var html = '';
                    html += '<div class="dropdown">';
                    html +=     '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
                    html +=         '<i class="fa fa-ellipsis-h fa-lg"></i>';
                    html +=     '</a>';
                    html +=     '<ul class="dropdown-menu pull-right">';
                    if (row.disabled == '1') {
                        html +=         '<li><a href="/admin/giftcards/enable/'+row.id+'" class="link" onclick="return confirm_action(this);">Enable</a></li>';
                    } else {
                        html +=         '<li><a href="/admin/giftcards/disable/'+row.id+'" class="link" onclick="return confirm_action(this);">Disable</a></li>';
                    }
                    html +=     '</ul>';
                    html += '</div>';
                    return html;
                }}
            ]
        });
    });
</script>
@endsection
