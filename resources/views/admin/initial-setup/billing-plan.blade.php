@extends('layouts.admin-setup')

@section('content')
<div class="row">
    <div class="title-text col-md-6 col-md-offset-3 text-center">
        <h2>Commentsold</h2>
        <h3>Which billing plan suits your needs?</h3>
        <br/>
    </div>
</div>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <form class="" role="form" method="POST" action="/admin/setup/billing-plan">

            <div class="form-group @hasError('billing_plan')">
                <select class="form-control" name="billing_plan">
                    <option value="">Please Select</option>
                    @if ($billingPlan == 'boutique_hub')
                        <option value="boutique_hub_starter">Starter ($24.99/month and 5% fees)</option>
                        <option value="boutique_hub_business">Business ($99/month and 3% fees)</option>
                    @else
                        <option value="starter">Starter ($49/month and 5% fees)</option>
                        <option value="business">Business ($149/month and 3% fees)</option>
                    @endif
                </select>
                @error('billing_plan')
            </div>

            <button type="submit" style="margin-top: 0px;" class="btn btn-success" data-loading-text="Loading..">Next</button>
        </form>
    </div>
</div>
@endsection
