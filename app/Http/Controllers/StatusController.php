<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Master;
use App\Models\AppLog;
use App\Models\ShopLog;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\ShopSettings;
use App\Models\Waitlist;
use Carbon\Carbon;
use Exception;
use DB;
use Auth;
use Log;
use Facebook\Facebook as FacebookSDK;
use Redis;
use App\Checkout\Fees;

class StatusController extends Controller
{
    public function index()
    {
        $users = User::all();
        $totalUsers = User::count();
        $admins = User::where('superadmin', '=', true);
        $superAdmins = $admins->count();
        $onStepOne = $admins->whereNull('shop_name')->count();
        $onStepTwo = User::where('superadmin', '=', true)->whereNotNull('stripe_id')->count();
        $active = User::whereNotNull('stripe_id')->count();
        return view('status.status', compact('users', 'totalUsers', 'onStepOne', 'onStepTwo', 'active', 'superAdmins'));
    }

    public function loginAs()
    {
        $user = User::findOrFail(request('user'));
        session(['logged-as-user' => true]);
        Auth::login($user);
        return redirect('/admin');
    }

    public function logs()
    {
        $shop = request('shop');
        return view('status.logs', compact('shop'));
    }

    public function shipping()
    {
        $shops = [];
        ini_set('memory_limit', '712M');

        $since = strtotime('last Monday');
        $end = strtotime('last Sunday');
        $end = ($end + (60*60*24));


        foreach (all_shops_and_users() as $shopArr) {
            if ($shopArr['superadmin'] != true)
                continue;

            $shop = $shopArr['shop_name'];

            reconnect_shop_database($shop);

            $email = $shopArr['email'];
            //$shipSQL = DB::Select("select count(1) as shipment_count, sum(ship_cost) as ship_cost, sum(cs_ship_charged) as cs_ship_charged from orders where (payment_date > '" . ($since - (60*60*24*12)) . "' OR (shipped_date BETWEEN '{$since}' AND '{$end}')) AND cs_ship_charged is not null")[0];
            $shipSQL = DB::Select("select count(1) as shipment_count, sum(ship_cost) as ship_cost, sum(cs_ship_charged) as cs_ship_charged from orders where shipped_date BETWEEN '{$since}' AND '{$end}' AND cs_ship_charged is not null")[0];
            $total_orders = $shipSQL->shipment_count;
            $csShipCharged = $shipSQL->cs_ship_charged;
            $shipCharged = $shipSQL->ship_cost;
            if ($total_orders <= 0) {
                continue;
            }


            $singleSQL = DB::Select("select sum(payment_amt) as payments from orders")[0];

            $shops[] = array(
                'email' => $email,
                'total_orders' => $total_orders,
                'ship_cost' => $shipCharged,
                'ship_charged' => $csShipCharged,


            );
        }

        return view('status.shipping', compact('shops', 'since', 'end'));
    }

    public function statistics()
    {
        $queueSize = Redis::connection('jobs')->llen('queues:jobs');
        $delaySize = Redis::connection('jobs')->zcount('queues:jobs:delayed', '-inf', '+inf');

        $shops = [];
        $totalRev = 0;
        $totalOrders = 0;
        $totalCsShipCharged = 0;
        $totalShipCharged = 0;
        $totalShipCost = 0;
        $nrJobsLocked = 0;
        $MRR = 0;

        foreach (all_shops_and_users() as $shopArr) {
            $shop = $shopArr['shop_name'];
            $name = $shopArr['name'];
            $source = $shopArr['hear_about_us'];
            reconnect_shop_database($shop);

            $lockedTime = $this->getLockedTime('shopbot:job-locked');
            if (is_numeric($lockedTime) && $lockedTime > 2) {
                $nrJobsLocked++;
            }

            if(date('D', time()) === 'Mon') {
                $since = strtotime('Monday');
            } else {
                $since = strtotime('last Monday');
            }
            $singleSQL = DB::Select("select sum(payment_amt) as payments, count(payment_amt) as payment_count, sum(cs_ship_charged) as cs_ship_charged from orders where payment_date > '{$since}'")[0];
            /* Hack to monitor shipping since we weren't using shipped_date */
            $shipSQL = DB::Select("select sum(ship_cost) as ship_cost, sum(cs_ship_charged) as cs_ship_charged from orders where shipped_date > '{$since}' AND cs_ship_charged is not null")[0];
            //$shipSQL = DB::Select("select sum(ship_cost) as ship_cost, sum(cs_ship_charged) as cs_ship_charged from orders where (payment_date > '" . ($since - (60*60*24*7)) . "' OR shipped_date > '{$since}') AND cs_ship_charged is not null")[0];
            $revenue = $singleSQL->payments;
            $orders = $singleSQL->payment_count;
            $csShipCharged = $shipSQL->cs_ship_charged;
            $shipCharged = $shipSQL->ship_cost;


            // get subscription plan
            $fees = new Fees($shop, $shopArr->billing_plan);
            $plan = strtolower(trim($shopArr->billing_plan));
            if ($plan == 'starter') {
                $plan = '$49/month';
                if ($shopArr['is_enabled']) {
                    $MRR += 49;
                }
            } elseif ($plan == 'business') {
                $plan = '$149/month';
                if ($shopArr['is_enabled']) {
                    $MRR += 149;
                }
            } elseif ($plan == 'boutique_hub_starter') {
                $plan = '$24.99/month';
                if ($shopArr['is_enabled']) {
                    $MRR += 24.99;
                }
            } elseif ($plan == 'boutique_hub_business') {
                $plan = '$99/month';
                if ($shopArr['is_enabled']) {
                    $MRR += 99;
                }
            } else {
                $plan = '$0/month';
            }
            $plan .= ' ' . ($fees->getFeesPercentage() * 100) . '% fees';

            // get trial period
            if (empty($shopArr->trial_started_at) || ! is_numeric($shopArr->trial_started_at)) {
                $trialDaysLeft = 0;
            } else {
                $trialLenght = 15;
                if ($shop == 'kimonokorner') {
                    $trialLenght = 30;
                }
                $trialDaysLeft = max(0, $trialLenght - intval((time() - $shopArr->trial_started_at) / (60*60*24)));
            }

            $shops[] = [
                'shop'           => $shop,
                'lockedTime'     => $lockedTime,
                'totalOrders'    => $orders,
                'totalRevenue'   => $revenue,
                'csShipCharged'  => $csShipCharged,
                'shipCharged'    => $shipCharged,
                'name'           => $name,
                'fbDisconnected' => empty(shop_setting('facebook.user_id')) ? true : false,
                'isEnabled'      => $shopArr['is_enabled'],
                'plan'           => $plan,
                'trialDaysLeft'  => $trialDaysLeft,
                'source'        => $source,
                'MRR'           => $MRR,
            ];

            $totalRev += $revenue;
            $totalOrders += $orders;
            $totalCsShipCharged += $csShipCharged;
            $totalShipCharged   += $shipCharged;
        }

        usort($shops, function($a, $b) {
            if ($a['isEnabled'] != $b['isEnabled']) {
                return (int) $a['isEnabled'] < (int) $b['isEnabled'];
            }

            if ($a['fbDisconnected'] != $b['fbDisconnected']) {
                return (int) $a['fbDisconnected'] > (int) $b['fbDisconnected'];
            }

            return $a['totalRevenue'] <= $b['totalRevenue'];
        });

        $totalShipProfit = ($totalShipCharged - $totalCsShipCharged);
        return view('status.statistics', compact('shops', 'totalRev', 'totalOrders', 'totalCsShipCharged', 'totalShipCharged', 'totalShipProfit', 'queueSize', 'delaySize', 'nrJobsLocked', 'MRR'));
    }

    public function disconnectShop($shop)
    {
        reconnect_shop_database($shop);
        $facebookId = shop_setting('facebook.user_id');
        shop_setting_set('facebook.user_id.disconnected', $facebookId);
        shop_setting_set('facebook.user_id', null);
        return back();
    }

    public function disableShop($shop)
    {
        $shop = Master::findByShop($shop);
        $shop->is_enabled = false;
        $shop->save();

        return back();
    }

    public function enableShop($shop)
    {
        $shop = Master::findByShop($shop);
        $shop->is_enabled = true;
        $shop->save();

        return back();
    }

    public function getLockedTime($type)
    {
        // the amount of time the job is locked
        $lockedTime = false;
        $key = ShopSettings::where('key', $type)->first();
        if (isset($key->value, $key->updated_at) && $key->value && $key->updated_at) {
            if (Carbon::now()->greaterThan($key->updated_at)) {
                $lockedTime = Carbon::now()->diffInMinutes($key->updated_at);
            }
        }

        return $lockedTime;
    }

    public function phpinfo()
    {
        phpinfo();
    }

    public function apiLogs()
    {
        $offset = request('offset');
        $shop = request('shop');

        if (is_null($shop)) {
            $logger = new AppLog;
        } else {
            set_shop_database($shop);
            $logger = new ShopLog;
        }

        if ($offset == 0) {
            return $logger->orderBy('id', 'desc')->limit(10)->get();
        } else {
            return $logger->orderBy('id', 'desc')->where('id', '>', $offset)->limit(100)->get();
        }
    }

    public function unlockJobs($type)
    {
        $shops = all_shops();
        foreach ($shops as $shop) {
            reconnect_shop_database($shop);

            $type = 'shopbot:job-locked';

            // the amount of time the job is locked
            $lockedTime = false;
            $key = ShopSettings::where('key', $type)->first();

            if (isset($key->value, $key->updated_at) && $key->value && $key->updated_at) {
                if (Carbon::now()->greaterThan($key->updated_at)) {
                    $lockedTime = Carbon::now()->diffInMinutes($key->updated_at);
                }
            }

            if ($lockedTime > 7) {
                shop_setting_set($type, false);
            }
        }

        return back();
    }

    public function debug()
    {
        // return $this->debugMails();
        // return $this->unlockJobs();
        return $this->debugShopify();
        // return $this->importCSV();

        try {

            set_shop_database('pinkcoconut');
            \DB::setDefaultConnection('shop');
            \DB::reconnect();

            // pinkcoconut
            // transaction ID  : 6E141855JM644491F
            $paymentId = 'PAY-86D015941J1360803LHH24QA';
            $payerID = 'HY7W6NHA59G8U';

            // kaleyjase
            // $paymentId = 'PAY-1YJ02900PR0254339LHESX5I';
            // $payerID = '4HY3S2JHRQKP2';

            $paypal = new \App\Checkout\PayPal;
            $cart = new \stdClass;
            $cart->total = 43.94; //22.72;
            $payment = $paypal->executePayment($paymentId, $payerID, $cart);
            dd($payment);

            $clientId = shop_setting('paypal.client_id');
            $clientSecret = shop_setting('paypal.client_secret');
            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential($clientId, $clientSecret)
            );

            $apiContext->setConfig([
                'mode'           => 'LIVE',
                'log.LogEnabled' => true,
                'log.FileName'   => storage_path('paypal/PayPal.log'),
                'log.LogLevel'   => 'INFO', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
            ]);

            $payment = \PayPal\Api\Payment::get($paymentId, $apiContext);
            $transactions = $payment->getTransactions();
            // $relatedResources = $transactions[0]->getRelatedResources();
            // $sale = $relatedResources[0]->getSale();
            // $transactionId = $sale->getId();

            dd($payment, $transactions); //, $relatedResources, $sale, $transactionId);

            dd('--');

        } catch (Exception $e) {
            dd($e);
        }
    }

    public function debugMails()
    {
        set_shop_database('shop1');
        \DB::setDefaultConnection('shop');
        \DB::reconnect();

        $mailer = new \App\Mails\ShopMailer;
        $customer = \App\Models\ShopUser::where('email', 'ludovic_tm@yahoo.com')->first();
        $variant = \App\Models\Inventory::first();
        $product = $variant->product;

        // dd($variant, $product, $customer);
        $mailer->sendItemAddedToCartEmail($customer, $product, $variant);
    }

    public function importCSV()
    {
        try {
            set_shop_database('shopentourageclothing');
            \DB::setDefaultConnection('shop');
            \DB::reconnect();

            $csvFile = storage_path('UPCentourage.csv');

            $importedCount = 0;
            $row = 1;
            if (($handle = fopen($csvFile, "r")) !== false) {
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    if ($row > 1) {
                        $sku = $data[0];
                        $size = $data[3];
                        $barcode = $data[6];

                        if ($size == 'XXL') {
                            $size = '2XL';
                        } elseif ($size == 'XXXL') {
                            $size = '3XL';
                        } elseif ($size == 'S') {
                            $size = 'Small';
                        } elseif ($size == 'M') {
                            $size = 'Medium';
                        } elseif ($size == 'L') {
                            $size = 'Large';
                        }

                        $product = Product::where('style', 'like', "%{$sku}%")->first();
                        if (! is_null($product)) {
                            $variants = $product->variants;
                            $matchVariant = false;
                            foreach ($variants as $variant) {
                                if (! empty($variant->shopify_barcode)) {
                                    $row++;
                                    continue 2;
                                }

                                if (trim(strtolower($variant->size)) == trim(strtolower($size))) {
                                    $matchVariant = true;

                                    Inventory::where('id', $variant->id)->update(['shopify_barcode' => $barcode]);

                                    $importedCount++;
                                    echo "Imported sku {$sku}, size {$size}, inventory id #{$variant->id}.<br/>";
                                    break;
                                }
                            }

                            if (! $matchVariant && $row > 100) {
                                dd($product, $variants, $sku, $size, $barcode);
                                echo "Variants don't match for sku {$sku}.<br/>";
                            }

                        } else {
                            echo "Skipping sku {$sku}.<br/>";
                        }
                    }

                    $row++;
                }
                fclose($handle);
            }

            d('Total Imported: '.$importedCount);
            dd('--');

        } catch (Exception $e) {
            dd($e);
        }
    }

    public function checkShopifyInventory()
    {
        $shops = ['charliroseandco'];

        foreach ($shops as $shop) {
            echo $shop.'<br/>';

            set_shop_database($shop);
            \DB::setDefaultConnection('shop');
            \DB::reconnect();

            if (! shopify_enabled()) {
                continue;
            }

            if (! shop_setting('shopify.access_token')) {
                continue;
            }

            $issueIds = [];
            $inventoryIds = \App\Models\InventoryLog::distinct()->pluck('inventory_id');
            foreach ($inventoryIds as $inventoryId) {
                $logs = \App\Models\InventoryLog::where('inventory_id', '=', $inventoryId)->get();
                $count = count($logs);
                if ($count <= 1) {
                    continue;
                }

                for ($i=0; $i < $count - 1; $i++) {
                    if ($logs[$i]->from_inv == $logs[$i+1]->from_inv) {
                        if (! in_array($logs[$i]->inventory_id, $issueIds)) {
                            $issueIds[] = $logs[$i]->inventory_id;
                            echo $logs[$i]->id . ' : ' . $logs[$i+1]->id . '<br/>';
                            echo $logs[$i]->inventory_id . ' : ' . $logs[$i+1]->inventory_id . '<br/>';
                            echo $logs[$i]->from_inv . ' : ' . $logs[$i+1]->from_inv . '<br/>';
                            echo $logs[$i]->to_inv . ' : ' . $logs[$i+1]->to_inv . '<br/>';
                            echo $logs[$i]->movement . ' : ' . $logs[$i+1]->movement . '<br/>';
                            echo '----------------------------------------<br/>';
                        }
                    }
                }

            }
        }

        d($issueIds);

        $keep = [];
        foreach ($issueIds as $inventoryId) {
            $inventory = \App\Models\Inventory::find($inventoryId);
            $inCart = $inventory->cartCount->count ?? 0;
            if ($inventory->quantity == 0 && $inCart == 0) {
                // pass
            } else {
                // product_id
                $keep[] = $inventoryId;
            }
        }

        d($keep);
        dd('----------------------------------------');
    }

    public function resyncShopifyShops()
    {
        // foreach (all_shops() as $shop) {
        //     set_shop_database($shop);
        //     \DB::setDefaultConnection('shop');
        //     \DB::reconnect();

        //     if (! shopify_enabled()) {
        //         continue;
        //     }

        //     if (! shop_setting('shopify.access_token')) {
        //         continue;
        //     }

        //     dispatch(new \App\Jobs\ReSyncShopify($shop));
        // }
    }

    public function debugShopify()
    {
        try {
            // dispatch(new \App\Jobs\SetupShopify('shopohdarlin'));
            // dispatch(new \App\Jobs\ReSyncShopify('classycotton'));
            // dd('---');

            // dd(shop_setting('shopify.shopify-shop-name'));
            set_shop_database('oliveandruststudio');
            \DB::setDefaultConnection('shop');
            \DB::reconnect();

            $shopify = new \App\Shopify\Shopify;
            $client = $shopify->getClient();

            // $shop = $client->callAPI('GET', '/admin/shop.json');
            //d($shopify->getWebhooks());

            // $inventory = \App\Models\Inventory::find(13490);
            // d($inventory->color, $inventory->size, 'quantity: '.$inventory->quantity);
            // d('cart: '.($inventory->cartCount->count ?? 0));
            // d('product_id: '. $inventory->product_id);

            // $response = $client->call('PUT', "/admin/variants/{$inventory->shopify_inventory_id}.json", [
            //     'variant' => [
            //         'id' => $inventory->shopify_inventory_id,
            //         'inventory_quantity_adjustment' => 3,
            //     ]
            // ]);
            // d($response);
            $order = \App\Models\Order::find(13);
            d($order);

            $o = $client->call('GET', '/admin/orders/'.$order->shopify_order_id.'.json');
            dd($o);


            $product = Product::find(2012); //$inventory->product_id);
            $p = $client->call('GET', '/admin/products/'.$product->shopify_product_id.'.json');
            // $shopify->importProduct($p);
            dd($product, $product->variants, $p);

            $variant = $p['variants'][2];
            $inventory = \App\Models\Inventory::where('shopify_inventory_id', '=', $variant['id'])->first();
            dd($inventory);

            $s = (new \App\Shopify\Shopify);
            $c = $s->getClient();

            // (new \App\Shopify\Shopify)->syncProductInventory($product, $p['variants'], $p['options']);


            dd('--');

        } catch (Exception $e) {
            dd($e);
        }
    }

    public function debugFacebook()
    {
        try {

            set_shop_database('ruralbliss');
            \DB::setDefaultConnection('shop');
            \DB::reconnect();

            $fb = new FacebookSDK([
                'app_id' => fb_app_id(),
                'app_secret' => fb_app_secret(),
                'default_graph_version' => 'v2.8',
            ]);

            $token = shop_setting('facebook.access_token');
            $fb->setDefaultAccessToken($token);

            $response = $fb->get('/'.shop_setting('facebook.user_id').'/groups');

            $groups = [];
            $graphEdge = $response->getGraphEdge();

            do {
                foreach ($graphEdge as $graphNode) {
                    $groups[] = $graphNode->asArray();
                }
            } while($graphEdge = $fb->next($graphEdge));

            dd($groups);


            // $response = $fb->get('/980608365405358_509814959352898/comments?fields=created_time,from,message,id,parent&filter=stream&order=reverse_chronological&limit=1');

            // Unsupported get request. Object with ID '980608365405358_509814959352898' does not exist, cannot be loaded due to missing permissions, or does not support this operation. Please read the Graph API documentation at https://developers.facebook.com/docs/graph-api

            d($response);
            dd('----------------------------------------');

            // $customer = \App\Models\ShopUser::find(1);
            // $order = \App\Models\Order::find(20);
            // $post = \App\Models\Post::find(16);
            // d($customer, $order);

            // $message = (new \App\Facebook\MessengerTemplates)->paymentReceipt($customer, $order);
            // $message = (new \App\Facebook\MessengerTemplates)->noEmail($customer);
            // $message = (new \App\Facebook\MessengerTemplates)->invalidComment($customer, $post);
            // $message = (new \App\Facebook\MessengerTemplates)->itemWaitlisted($customer);
            // $product = (new \App\Facebook\MessengerTemplates)->gotOrder($customer);
            // $button = (new \App\Facebook\MessengerTemplates)->payOrderButton($customer);

            // $senderId = '1221917631258581'; // Brandon's id
            // $senderId = '1453789984667908'; // my id
            $senderId = '1854800481204790'; // my second facebook account id
            // $senderId = '1382810725101194'; // FB reviewer id

            $message = [
                'recipient' => [
                    'id' => $senderId,
                ],
                'message' => [
                    // 'text' => 'Your bill is $20. We will remind you again later.',
                    'text' => 'Time is up! Your bill is now due - please go to commentsold.com to pay.',
                ],
            ];

            $messenger = new \App\Facebook\FacebookMessenger(fb_page_access_token());
            $response = $messenger->callSendAPI($message);

            dd($response);


            // $token = shop_setting('facebook.access_token');
            // $appSecretProof = hash_hmac('sha256', $token, fb_app_secret());

            // $fb->setDefaultAccessToken($token);
            // $userId = shop_setting('facebook.user_id');

            // $response = $fb->get('/'.$userId.'/groups');
            // $graphEdge = $response->getGraphEdge();

            // do {
            //     foreach ($graphEdge as $graphNode) {
            //         d($graphNode);
            //     }
            // } while($graphEdge = $fb->next($graphEdge));

            // $response = $fb->get('/'.$userId.'/permissions');
            // $response = $fb->get('/499941070130866/feed?since='.(time() - 60*60*24*100));

            dd('--');

        } catch (Exception $e) {
            dd($e);
        }
    }
}
