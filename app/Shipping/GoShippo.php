<?php

namespace App\Shipping;

use Shippo;
use Shippo_Shipment;
use Shippo_Transaction;
use Shippo_InvalidRequestError;
use Shippo_Rate;

class GoShippo
{
    public function __construct()
    {
        Shippo::setApiKey(shop_setting('goshippo.api_token'));
        Shippo::setApiVersion('2016-10-25');
    }

    public function buyLabelFor($order, $dimensions)
    {
        try {
            $details = [
                'object_purpose' => 'PURCHASE',
                'address_from'   => $this->shopFromAddress(),
                'address_to'     => $this->toAddress($order),
                'parcel'         => $this->getParcel($dimensions),
                'async'          => false,
            ];

            if ($dimensions['bypass_validation']) {
                $details['extra'] = ['bypass_address_validation' => true];
            }

            $shipment = Shippo_Shipment::create($details);
        } catch (Shippo_InvalidRequestError $e) {
            log_error("[go-shippo] $e");
            $data = $e->getJsonBody();
            return ['errors' => ($data['parcel'][0]['__all__'] ?? [])];
        }

        $rate = $this->getRate($shipment, $this->getServiceLevel($dimensions));

        return [
            'object_id'         => $rate['object_id'],
            'amount'            => floatval($rate['amount']),
            'provider'          => $rate['provider'],
            'provider_image_75' => $rate['provider_image_75'],
            'servicelevel_name' => $rate['servicelevel_name'],
        ];
    }

    public function confirmBuyLabel($order, $rateId)
    {
        $transaction = Shippo_Transaction::create(array(
            'rate'            => $rateId,
            'async'           => false,
            'partner_code'    => 'COMMENTSOLD',
            'metadata'        => "Order ID {$order->id}",
            'label_file_type' => 'PNG',
        ));

        if ($transaction['object_status'] == 'SUCCESS') {

            $rate = Shippo_Rate::retrieve($rateId);

            return [
                'label_url'       => $transaction['label_url'],
                'tracking_number' => $transaction['tracking_number'],
                'ship_cost'       => floatval($rate['amount']),
            ];

        } else {
            $errors = [];
            foreach ($transaction['messages'] as $message) {
                $errors[] = $message['text'];
            }
            return ['errors' => $errors];
        }
    }

    /**
     * Return the correct rate for the specified service level.
     *
     * @param  object $shipment
     * @param  string $serviceLevel
     * @return object
     */
    public function getRate($shipment, $serviceLevel)
    {
        foreach ($shipment['rates_list'] as $rate) {
            if ($rate['servicelevel_token'] == $serviceLevel) {
                return $rate;
            }
        }
    }

    /**
     * "First Class" for less than 15.99 ounces, "Priority" otherwise.
     *
     * @param  array $dimensions
     * @return string
     */
    public function getServiceLevel($dimensions)
    {
        $ounces = $dimensions['weight'] ?? 0;

        if ($ounces <= 15.99) {
            return 'usps_first';
        }

        return 'usps_priority';
    }

    /**
     * The complete reference for parcel object is here: https://goshippo.com/docs/reference#parcels
     *
     * @param  App\Models\Order $order
     * @return array
     */
    public function getParcel($dimensions)
    {
        return [
            'length'        => $dimensions['length'] ?? 0,
            'width'         => $dimensions['width'] ?? 0,
            'height'        => $dimensions['height'] ?? 0,
            'distance_unit' => 'in',
            'weight'        => $dimensions['weight'] ?? 0,
            'mass_unit'     => 'oz',
        ];
    }

    /**
     * The complete refence for the address object is available here: https://goshippo.com/docs/reference#addresses
     *
     * @param  App\Models\Order $order
     * @return array
     */
    public function toAddress($order)
    {
        return [
            'object_purpose' => 'PURCHASE',
            'name'           => $order->ship_name,
            // 'company'        => '-',
            'street1'        => $order->street_address,
            'street2'        => $order->apartment,
            'city'           => $order->city,
            'state'          => $order->state,
            'zip'            => $order->zip,
            'country'        => $order->getCountryCode(),
            // 'phone'          => '-',
            'email'          => $order->email,
        ];
    }

    /**
     * The complete refence for the address object is available here: https://goshippo.com/docs/reference#addresses
     *
     * @return array
     */
    public function shopFromAddress()
    {
        return [
            'object_purpose' => 'PURCHASE',
            'name'           => shop_setting('shop.from_name'),
            //'company'        => shop_setting('shop.company_name'),
            'street1'        => shop_setting('shop.street_address'),
            'city'           => shop_setting('shop.city'),
            'state'          => shop_setting('shop.state'),
            'zip'            => shop_setting('shop.zip_code'),
            'country'        => shop_setting('shop.country_code', 'US'),
            'phone'          => $this->getShopPhone(),
            'email'          => shop_setting('shop.company_email'),
        ];
    }

    /**
     * Return the shop's phone number.
     *
     * @return string
     */
    public function getShopPhone()
    {
        $phone = shop_setting('shop.phone');
        $phone = str_replace(['+', '.', '(', ')', '-'], '', $phone);
        $phone = trim($phone);

        return $phone;
    }
}
