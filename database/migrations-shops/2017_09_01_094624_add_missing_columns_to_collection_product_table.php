<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingColumnsToCollectionProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collection_product', function (Blueprint $table) {
            $table->boolean('featured')->nullable();
            $table->integer('position')->nullable();
            $table->string('sort_value')->nullable();
            $table->integer('created_at')->nullable();
            $table->integer('updated_at')->nullable();
            $table->string('shopify_collect_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collection_product', function (Blueprint $table) {
            $table->dropColumn(['featured', 'position', 'sort_value', 'created_at', 'updated_at', 'shopify_collect_id']);
        });
    }
}
