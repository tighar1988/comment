<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Movement Id</th>
                <th>Amount</th>
                <th>Note</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            @if ($balanceMovement->isEmpty())
                <tr><td colspan="4">The customer has no balance movement.</td></tr>
            @endif

            @foreach ($balanceMovement as $movement)
                <tr>
                    <td>{{ $movement->id }}</td>
                    <td>
                        @if ($movement->amount > 0)
                            + ${{ amount($movement->amount) }}
                        @else
                            - ${{ amount(abs($movement->amount)) }}
                        @endif
                    </td>
                    <td>{{ $movement->memo }}</td>
                    <td>{{ $movement->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
