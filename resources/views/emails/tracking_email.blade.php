<div style="font-family: Verdana; font-size: 12px;">

    Hello {{ $order['ship_name'] }}
    <br/><hr/><br/>

    We have shipped your order. Your tracking number is {{ $order['tracking_number'] }}. 
    Please <a href="https://tools.usps.com/go/TrackConfirmAction?qtc_tLabels1={{ $order['tracking_number'] }}">click here to track it.</a>
    <br/><br/>

    <b>PLEASE NOTE</b> it may take up to 12 hours for the tracking number to start working

    <br/><br/>
</div>
