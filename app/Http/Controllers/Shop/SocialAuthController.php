<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use App\Models\ShopUser;
use App\Instagram\Instagram;
use Auth;

class SocialAuthController extends Controller
{
    /**
     * Redirect the shop user to Facebook to accept permissions.
     */
    public function redirect(Request $request)
    {
        config(['services.facebook.client_id' => fb_app_id()]);
        config(['services.facebook.client_secret' => fb_app_secret()]);
        config(['services.facebook.app_access_token' => fb_app_access_token()]);
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * The Facebook callback for the base domain. We redirect to each shop subdomain.
     */
    public function redirectToShop(Request $request)
    {
        set_shop_database($request->get('shop'));
        $url = shop_url('/facebook-login/callback?'.$request->getQueryString());

        return redirect($url);
    }

    /**
     * Facebook callback for each specific shop.
     */
    public function callback(Request $request)
    {
        if (! $request->has('code') || $request->has('denied')) {
            return shop_redirect('/');
        }

        config(['services.facebook.client_id' => fb_app_id()]);
        config(['services.facebook.client_secret' => fb_app_secret()]);
        config(['services.facebook.app_access_token' => fb_app_access_token()]);
        $user = ShopUser::findOrCreateUser(Socialite::driver('facebook')->user());

        Auth::login($user);

        return shop_redirect('/account');
    }

    /**
     * Logout current customer.
     */
    public function logout(Request $request)
    {
        Auth::logout($request->user());
        return shop_redirect('/');
    }

    /**
     * Redirect the guest to Instagram to login.
     */
    public function instagramLoginRedirect()
    {
        $shop = str_replace('_', '-', shop_id());
        return (new Instagram)->redirectShopUser($shop);
    }

    /**
     * Redirect the shop user to Instagram when he presses a button.
     */
    public function instagramRedirect()
    {
        $shop = str_replace('_', '-', shop_id());
        return (new Instagram)->redirectShopUser($shop);
    }

    /**
     * The 'guest' or 'shop user' is redirected here after accepting the permissions on Instagram.
     */
    public function instagramCallback(Request $request)
    {
        if (! $request->has('code') || $request->has('denied')) {
            return shop_redirect('/');
        }

        $user = (new Instagram)->shopUser($request->get('shop'));

        $shopUser = $request->user();
        if (is_null($shopUser)) {
            // find or create a new user
            $shopUser = ShopUser::where('instagram_id', $user->getId())->first() ?? new ShopUser;
            $shouldLogin = true;
        } else {
            // it's a logged in user who connects his Instagram
            $shouldLogin = false;
        }

        // update user data
        $shopUser->instagram_id = $user->getId();
        $shopUser->instagram_data = [
            'token'    => $user->token,
            'username' => mysql_utf8($user->getNickname()),
            'name'     => mysql_utf8($user->getName()),
            'email'    => $user->getEmail(),
            'avatar'   => $user->getAvatar(),
        ];
        if (empty($shopUser->email)) {
            $shopUser->email = $user->getEmail();
        }
        if (empty($shopUser->nickname)) {
            $shopUser->nickname = mysql_utf8($user->getNickname());
        }
        if (empty($shopUser->name)) {
            $shopUser->name = mysql_utf8($user->getName());
        }
        if (empty($shopUser->avatar)) {
            $shopUser->avatar = $user->getAvatar();
        }

        $shopUser->save();

        if ($shouldLogin) {
            log_info("[Instagram] User {$shopUser->name} just logged in.");
            Auth::login($shopUser);
        }

        return shop_redirect('/account');
    }
}
