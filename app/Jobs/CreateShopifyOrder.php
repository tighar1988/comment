<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Master;
use App\Facebook\FacebookMessenger;
use Facebook\Exceptions\FacebookResponseException;
use App\Jobs\Traits\ReconnectShopDB;
use App\Shopify\ShopifyClient;
use App\Shopify\Shopify;
use App\Mails\ShopMailer;
use App\Models\Order;
use App\Models\User;
use Exception;
use DB;

class CreateShopifyOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    protected $shop;

    protected $orderId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop, $orderId)
    {
        $this->shop = $shop;
        $this->orderId = $orderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $startTime = microtime(true);
            $this->reconnectDB($this->shop);
            log_info("[shopify-order] Creating shopify order...");

            $order = Order::find($this->orderId);

            $shopify = new Shopify;
            $response = $shopify->createOrder($order);

            if ($response == 0) {
                $shopOwner = User::getByShopName($this->shop);
                (new ShopMailer)->sendShopifyOrderNotSyncedEmail($shopOwner, $order->id);
                log_info("[shopify-order] Could not sync order - contains CommentSold products.");

            } else {
                $order->shopify_order_id = $response['id'];
                $order->save();

                log_info("[shopify-order] Shopify order created in (" . elapsed_time($startTime) . ") seconds.");
            }

        } catch (Exception $e) {
            ShopMailer::notifyAdmin($e);
            log_error("[shopify-order] [exception] {$e}");
        }
    }
}
