<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Cart;
use App\Models\Product;
use App\Models\ShopUser;
use App\Models\Inventory;
use Illuminate\Support\Facades\Auth;

class CartChangeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|numeric|min:1',
            'line' => 'required|numeric|min:1',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $error = null;

            $customer = Auth::check() ? Auth::user() : new ShopUser;
            $shoppingCart = $customer->shoppingCart();

            $variant = null;
            $inventoryIds = [];
            $line = 0;
            foreach ($shoppingCart->items as $cartItem) {
                if (in_array($cartItem->inventory_id, $inventoryIds)) {
                    // skip
                } else {
                    $inventoryIds[] = $cartItem->inventory_id;
                    $line++;
                }

                if ($line == $this->line) {
                    $variant = Inventory::find($cartItem->inventory_id);
                }
            }

            if (! $variant) {
                $error = 'We could not find the product!';

            } else {

                $cart = session('online-store.cart', []);
                $sessionCartCount = isset($cart[$variant->id]) ? ($cart[$variant->id]['quantity'] ?? 0) : 0;
                $dbCartCount = Auth::check() ? Cart::forCustomer(Auth::user()->id)->cartQuantity($variant->id) : 0;

                $cartCount = $sessionCartCount + $dbCartCount;
                $availableCount = $cartCount + $variant->quantity;

                if ($availableCount <= 0) {
                    $productName = $variant->product->product_name ?? null;
                    $error = "The product {$productName} - ".$variant->variantName()." is already sold out.";

                } elseif ($availableCount < $this->quantity) {
                    $productName = $variant->product->product_name ?? null;
                    $error = "You can only add in total {$availableCount} {$productName} to the cart.";
                }
            }

            if (! is_null($error)) {
                $validator->errors()->add('status', 422);
                $validator->errors()->add('message', 'Cart Error');
                $validator->errors()->add('description', $error);
            }
        });
    }
}
