<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class ShopAddress extends Drop
{
    /**
     * Returns a summary of the shop's address:
     * 150 Elgin Street, Ottawa, Ontario, Canada
     * The summary takes the form street, city, state/province, country.
     */
    public function summary()
    {
        return $this->invokeDrop('street').', '.$this->invokeDrop('city').', '.$this->invokeDrop('province').', '.$this->invokeDrop('country');
    }

    /**
     * Returns the shop's street address:
     * 150 Elgin Street
     */
    public function street()
    {
        return shop_setting('shop.street_address');
    }

    /**
     * Returns the city in the shop's address:
     * Ottawa
     */
    public function city()
    {
        return shop_setting('shop.city');
    }

    /**
     * Returns the state or province in the shop's address:
     * Ontario
     */
    public function province()
    {
        $states = [
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AS' => 'American Samoa',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'AA' => 'Armed Forces America',
            'AE' => 'Armed Forces Europe',
            'AP' => 'Armed Forces Pacific',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District Of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'GU' => 'Guam',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'MP' => 'Northern Mariana Is',
            'NS' => 'Nova Scotia',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PW' => 'Palau',
            'PA' => 'Pennsylvania',
            'PR' => 'Puerto Rico',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VI' => 'Virgin Islands',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        ];

        $code = $this->invokeDrop('province_code');

        return $states[$code] ?? null;
    }

    /**
     * Returns an abbreviated form of the state or province in the shop's address:
     * ON
     */
    public function province_code()
    {
        return shop_setting('shop.state');
    }

    /**
     * Returns the country in the shop's address:
     * Canada
     */
    public function country()
    {
        return 'USA';
    }

    /**
     * Returns the country in the shop's address using uppercase letters:
     * CANADA
     * The result is identical to using the upcase filter on shop.address.country.
     */
    public function country_upper()
    {
        return 'USA';
    }

    /**
     * Returns the ZIP or postal code in the shop's address:
     * K2P 1L4
     */
    public function zip()
    {
        return shop_setting('shop.zip_code');
    }
}
