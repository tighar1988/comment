<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Card;

class CardTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Card::class, new Card);
    }

    /** @test */
    public function it_casts_default_attribute_to_boolean()
    {
        $card = new Card;
        $card->default = 1;

        $this->assertSame(true, $card->default);
    }
}
