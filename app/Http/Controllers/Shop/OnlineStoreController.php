<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\OnlineStore\LiquidTemplate;
use App\OnlineStore\CurrentView;
use App\OnlineStore\Objects\Page;
use App\OnlineStore\Objects\Collection;
use App\OnlineStore\Objects\Product;
use App\OnlineStore\Objects\Search;
use App\OnlineStore\Objects\Checkout;
use App\OnlineStore\Objects\Order as OrderDrop;
use App\Models\GiftCard;
use App\Models\Order;
use App\Models\Inventory;
use App\Models\ShopUser;
use App\Models\Coupon;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests\CheckoutFormRequest;
use App\Checkout\Checkout as CustomerCheckout;
use App\Checkout\CheckoutException;
use Image;
use Validator;

class OnlineStoreController extends Controller
{
    /**
     * The /store route
     */
    public function index()
    {
        if (store_visible()) {
            return shop_redirect('/');
        }

        return response(LiquidTemplate::template('index'), 200)
           ->withHeaders(cache_header('index store'));
    }

    public function cart()
    {
        return LiquidTemplate::template('cart');
    }

    public function cartChange()
    {
        $itemId = request('line');

        $customer = Auth::check() ? Auth::user() : new ShopUser;
        $shoppingCart = $customer->shoppingCart();

        foreach ($shoppingCart->items as $item) {
            if ($item->id == $itemId) {
                if (starts_with($itemId, 'session')) {
                    session()->forget('online-store.cart.'.$item->inventory_id);
                } else {
                    // delete from db cart
                    Cart::removeItem($item->id);
                }
            }
        }

        return shop_redirect('/cart');
    }

    public function page($page)
    {
        $page = app('App\Models\Page')->handle($page)->first();
        if (is_null($page)) {
            return LiquidTemplate::template('404');
        }

        return LiquidTemplate::template('page', [
            'page' => new Page($page),
        ]);
    }

    public function collection($collectionHandle, $tagId = null)
    {
        $collection = app('App\Models\Collection')->handle($collectionHandle)->first();
        if (is_null($collection)) {
            return response(LiquidTemplate::template('404'), 404)
               ->withHeaders(cache_header($collectionHandle, 60));
        }

        CurrentView::$collectionHandle = $collectionHandle;

        return response(LiquidTemplate::template('collection', [
            'collection' => new Collection($collection),
        ]), 200)->withHeaders(cache_header("collections {$collectionHandle}"));

        // $collectionModel = Collection::url($collection)->firstOrFail();
        // $tagGroups = TagGroup::with('tags')->get();

        // if (! is_null($tagId)) {
        //     $tag = Tag::find($tagId);
        //     if (is_null($tag)) {
        //         $ids = [];
        //     } else {
        //         $ids = $tag->products()->pluck('products.id')->toArray();
        //     }
        //     $products = $collectionModel->products()->wherePivotIn('product_id', $ids)->with('inventory')->paginate(24);

        // } else {
        //     $products = $collectionModel->products()->with('inventory')->paginate(24);
        //     if ($collection == 'new-arrivals' && count($products) == 0) {
        //         $products = Product::published()->with('inventory')->paginate(24);
        //     }
        // }

        // $collection = $collectionModel;
        // return theme('templates.collection', compact('collection', 'products', 'tagGroups', 'tagId'));
    }

    public function collectionProduct($collectionHandle, $productHandle)
    {
        $collection = app('App\Models\Collection')->handle($collectionHandle)->first();
        if (is_null($collection)) {
            return shop_redirect("/products/{$productHandle}");
        }

        $product = app('App\Models\Product')->handle($productHandle)->published()->first();
        if (is_null($product)) {
            return LiquidTemplate::template('404');
        }

        if (! in_array($collectionHandle, $product->collections->pluck('url')->toArray())) {
            return shop_redirect("/products/{$productHandle}");
        }

        CurrentView::$collectionProduct = $product;
        CurrentView::$productHandle     = $productHandle;
        CurrentView::$collectionHandle  = $collectionHandle;

        return response(LiquidTemplate::template('product', [
            'product'    => new Product($product),
            'collection' => new Collection($collection),
        ]), 200)->withHeaders(cache_header("collections products product-$productHandle collection-$collectionHandle"));

        // $collection = Collection::url($collection)->firstOrFail();
        // $product = Product::url($productHandle)->published()->with('inventory', 'images')->firstOrFail();

        // $previous = $collection->getPreviousFor($product);
        // $next = $collection->getNextFor($product);
        // $moreGreatFinds = $collection->moreGreatFinds($product);

        // return theme('templates.product', compact('collection', 'product', 'previous', 'next', 'moreGreatFinds'));
    }

    public function product($productHandle)
    {
        $product = app('App\Models\Product')->handle($productHandle)->published()->first();
        if (is_null($product)) {
            return LiquidTemplate::template('404');
        }

        CurrentView::$productHandle = $productHandle;
        return response(LiquidTemplate::template('product', [
            'product' => new Product($product),
        ]), 200)->withHeaders(cache_header("products product-$productHandle"));
    }

    public function asset($shopId, $asset)
    {
        set_shop_database($shopId);
        $pathToFile = resource_path("views/themes/".get_theme($shopId)."/assets/{$asset}");

        if (ends_with($pathToFile, '.css')) {
            $contentType = 'text/css';

        } elseif (ends_with($pathToFile, '.js')) {
            $contentType = 'application/javascript';

        } elseif (ends_with($pathToFile, '.svg')) {
            $contentType = 'image/svg+xml';
        }

        $fileType = ! isset($contentType) ? File::mimetype($pathToFile) : $contentType;

        $response = response(File::get($pathToFile), 200)->header('Content-Type', $fileType);
        return $response->withHeaders(cache_header());
    }

    public function image($shopId, $filename)
    {
        set_shop_database($shopId);
        $width = null;
        $height = null;
        $poz1 = strrpos($filename, '_');
        if ($poz1 !== false) {
            $poz2 = strrpos($filename, '.');
            $size = substr($filename, $poz1+1, $poz2-$poz1-1);

            $filename = substr_replace($filename, '', $poz1, $poz2-$poz1);

            if (str_contains($size, 'x')) {
                $sizes = explode('x', $size);
                $width = isset($sizes[0]) && ! empty(trim($sizes[0])) ? intval($sizes[0]) : null;
                $height = isset($sizes[1]) && ! empty(trim($sizes[1])) ? intval($sizes[1]) : null;
            } else {
                $size = trim($size);
                if ($size == 'pico') {
                    $width = 16; $height = 16;
                } elseif ($size == 'icon') {
                    $width = 32; $height = 32;
                } elseif ($size == 'thumb') {
                    $width = 50; $height = 50;
                } elseif ($size == 'small') {
                    $width = 100; $height = 100;
                } elseif ($size == 'compact') {
                    $width = 160; $height = 160;
                } elseif ($size == 'medium') {
                    $width = 240; $height = 240;
                } elseif ($size == 'large') {
                    $width = 480; $height = 480;
                } elseif ($size == 'grande') {
                    $width = 600; $height = 600;
                } elseif ($size == 'original') {
                    $width = 1024; $height = 1024;
                } elseif ($size == 'master') {
                    $width = null; $height = null;
                }
            }
        }

        // retrieve the original image
        $image = Image::make(Storage::get("{$shopId}/products/{$filename}"));

        // resize it in the requested size
        if (! is_null($width) && ! is_null($height)) {
            $aspectRatio = $image->width() / $image->height();

            if ($width > 900 || $height > 900) {
                $image->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

            } else {
                $minWidth = $aspectRatio * $height;
                $minHeight = $width / $aspectRatio;

                if ($minHeight < $height) {
                    // resize by height
                    $image->resize(null, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                } else {
                    // resize by width
                    $image->resize($width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                $image->crop($width, $height);
            }

        } elseif (! is_null($width) || ! is_null($height)) {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        // return the resized image which will be cached by the cdn
        return $image->response()->withHeaders(cache_header('', 86400));
    }

    public function giftcard($hash)
    {
        $giftcard = GiftCard::hash($hash)->first();
        $inventory = Inventory::find($giftcard->inventory_id);
        $image = $inventory->product->image ?? null;
        if ($image) {
            $image = product_image($image->filename);
        }

        return view('shop.gift-card-view', compact('giftcard', 'image'));
    }

    public function search()
    {
        CurrentView::$isSearch = true;
        CurrentView::$searchQuery = request()->query('q');
        return LiquidTemplate::template('search', [
            'search' => new Search,
        ]);
    }

    public function account()
    {
        CurrentView::$pageTitle = 'Account';
        return LiquidTemplate::template('customers/account');
    }

    public function order($orderId)
    {
        $customer = request()->user();
        $order = Order::find($orderId);
        if (is_null($order) || $order->customer_id != $customer->id) {
            return LiquidTemplate::template('404');
        }

        return LiquidTemplate::template('customers/order', [
            'order' => new OrderDrop($order),
        ]);
    }

    public function addresses()
    {
        // todo: implement
        return LiquidTemplate::template('customers/addresses');
    }

    public function loginForm()
    {
        return LiquidTemplate::template('customers/login');
    }

    public function loginUser()
    {
        $email = request('customer.email');
        $password = request('customer.password');

        // check if the email exists (and it's not a facebook account)
        $user = ShopUser::where('email', '=', $email)->whereNull('facebook_id')->first();
        if (! $user) {
            session()->flash('forms.customer_login.errors', 'Invalid login credentials.');
            session()->flash('forms.customer_login.posted_successfully', false);
            return back();
        }

        // if it's an imported user, set the password on the first login
        if (empty($user->password) && ! empty($password) && ! empty($user->shopify_customer_id)) {
            session()->flash('forms.customer_login.errors', 'For security reasons, your password was invalidated. Please reset your password using the link below.');
            session()->flash('forms.customer_login.posted_successfully', false);
            return back();
        }

        // check if the provided password matches the db password
        if (! password_verify($password, $user->password)) {
            session()->flash('forms.customer_login.errors', 'Invalid login credentials.');
            session()->flash('forms.customer_login.posted_successfully', false);
            return back();
        }

        // login the user
        Auth::login($user);

        session()->flash('forms.customer_login.posted_successfully', true);
        return shop_redirect('/account');
    }

    public function registerForm()
    {
        return LiquidTemplate::template('customers/register');
    }

    public function registerUser()
    {
        $firstName = request('customer.first_name');
        $lastName  = request('customer.last_name');
        $email     = request('customer.email');
        $password  = request('customer.password');

        $validator = Validator::make(request()->all(), [
            'customer.email'      => 'required|email',
            'customer.first_name' => 'required',
            'customer.last_name'  => 'required',
            'customer.password'   => 'required|min:4',
        ]);

        // validate the register
        if ($validator->fails()) {
            session()->flash('forms.create_customer.errors', $validator->errors()->all());
            session()->flash('forms.create_customer.posted_successfully', false);
            return back();
        }

        // check if the email exists (and it's not a facebook account)
        $user = ShopUser::where('email', '=', $email)->whereNull('facebook_id')->first();
        if ($user) {
            session()->flash('forms.create_customer.errors', 'This email address is already associated with an account. If this account is yours, you can <a href="/customers/reset-password">reset your password</a>.');
            session()->flash('forms.create_customer.posted_successfully', false);
            return back();
        }

        // create new user
        $user = new ShopUser;
        $user->first_name = $firstName;
        $user->last_name  = $lastName;
        $user->name       = $firstName.' '.$lastName;
        $user->email      = $email;
        $user->password   = bcrypt($password);
        $user->save();

        // login the user
        Auth::login($user);

        session()->flash('forms.create_customer.posted_successfully', true);
        return shop_redirect('/account');
    }

    public function checkout()
    {
        $customer = Auth::user();
        $cart = $customer ? $customer->shoppingCart() : (new ShopUser)->shoppingCart();
        if ($cart->count() == 0) {
            return shop_redirect('/cart');
        }

        return LiquidTemplate::template('checkout/customer-information', [
            'checkout' => new Checkout($customer),
        ]);
    }

    public function checkoutAddress(CheckoutFormRequest $request)
    {
        session(['checkout.address' => $request->input()]);

        return shop_redirect('/checkout?step=shipping');
    }

    public function privacyPolicy()
    {
        return LiquidTemplate::template('checkout/privacy-policy');
    }

    public function refundPolicy()
    {
        return LiquidTemplate::template('checkout/refund-policy');
    }

    public function termsOfService()
    {
        return LiquidTemplate::template('checkout/terms-of-service');
    }

    public function checkoutComplete()
    {
        return LiquidTemplate::template('checkout/complete');
    }

    public function applyCoupon()
    {
        $code = request('code');
        if (empty($code)) {
            return response()->json(['status' => 0]);
        }

        $customer = Auth::check() ? Auth::user() : new ShopUser;
        $shoppingCart = $customer->shoppingCart();

        $coupon = Coupon::getCoupon($code);
        if ($coupon && $coupon->isValid($customer, $shoppingCart->subtotal)) {
            session(['shopping-cart.coupon' => $coupon->id]);
            session(['shopping-cart.coupon-code' => $coupon->code]);
            return response()->json(['status' => 1]);
        }

        return response()->json(['status' => 0]);
    }

    public function checkoutAndCreateOrder()
    {
        $isPayPal = request()->has('paymentId');

        try {
            if (Auth::check()) {
                // logged in user stripe checkout
                $customer = request()->user();
                if ($isPayPal) {
                    $customer->checkout()->checkoutWithPayPal(request('paymentId'), request('PayerID'), $customer);
                } else {
                    $customer->checkout()->withNewCard(request('stripeToken'));
                }

                return shop_redirect('/checkout/complete');

            } else {
                // guest checkout
                $customer = new ShopUser;
                $customer->id = 0;
                $customer->email          = session('checkout.address.email');
                $customer->first_name     = session('checkout.address.first_name');
                $customer->last_name      = session('checkout.address.last_name');
                $customer->name           = $customer->first_name . ' ' . $customer->last_name;
                $customer->street_address = session('checkout.address.address1');
                $customer->apartment      = session('checkout.address.address2');
                $customer->city           = session('checkout.address.city');
                $customer->country_code   = session('checkout.address.country_code');
                if ($customer->country_code == 'US') {
                    $customer->state = session('checkout.address.state');
                } else {
                    $customer->state = session('checkout.address.region');
                }
                $customer->zip          = session('checkout.address.zip');
                $customer->phone_number = session('checkout.address.phone');
                $customer->local_pickup = 0;

                // check if some items are not available anymore
                $cart = $customer->shoppingCart();
                foreach ($cart->items as $item) {
                    $variant = Inventory::find($item->inventory_id);
                    if ($variant->quantity <= 0) {
                        return shop_redirect('/checkout?step=payment')->withErrors(['checkout_error' => "{$item->name} is sold out. Please remove it from your cart before checkout!"]);
                    }
                }

                // remove items from inventory
                foreach ($cart->items as $item) {
                    $variant = Inventory::find($item->inventory_id);
                    $variant->decrementQuantity("[guest-checkout] Removing item from inventory.");
                }

                $checkout = new CustomerCheckout($customer);
                if ($isPayPal) {
                    $checkout->checkoutWithPayPal(request('paymentId'), request('PayerID'), $customer);
                } else {
                    $checkout->guestWithCard(request('stripeToken'));
                }

                return shop_redirect('/checkout/complete');
            }

        } catch(CheckoutException $e) {
            // add items back to inventory
            foreach ($cart->items as $item) {
                $variant = Inventory::find($item->inventory_id);
                $variant->incrementQuantity("[guest-checkout-error] Adding item back to inventory.");
            }

            return shop_redirect('/checkout?step=payment')->withErrors(['checkout_error' => $e->getMessage()]);
        }
    }

    public function paypalApprovalLink()
    {
        $customer = Auth::check()? request()->user() : new ShopUser;
        $cart = $customer->shoppingCart();

        $paypal = new \App\Checkout\PayPal;
        return redirect($paypal->getApprovalLink($cart, '/checkout/paypal-payment'));
    }

    public function recoverPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            session()->flash('forms.recover_customer_password.errors', $validator->errors()->all());
            session()->flash('forms.recover_customer_password.posted_successfully', false);
            return shop_redirect('/customers/login#recover');
        }

        $broker = \Illuminate\Support\Facades\Password::broker();
        $response = $broker->sendResetLink($request->only('email'));

        if ($response == \Illuminate\Support\Facades\Password::RESET_LINK_SENT) {
            session()->flash('forms.recover_customer_password.posted_successfully', true);

        } else {
            session()->flash('forms.recover_customer_password.errors', [trans($response)]);
            session()->flash('forms.recover_customer_password.posted_successfully', false);
        }

        return shop_redirect('/customers/login#recover');
    }

    public function resetPasswordForm($token)
    {
        return LiquidTemplate::template('customers/reset_password');
    }

    public function resetPassword($token, Request $request)
    {
        config(['auth.providers.users.model' => \App\Models\PasswordShopUser::class]);

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            session()->flash('forms.reset_customer_password.errors', $validator->errors()->all());
            session()->flash('forms.reset_customer_password.posted_successfully', false);
            return back();
        }

        $broker = \Illuminate\Support\Facades\Password::broker();
        $credentials = $request->only('email', 'password', 'password_confirmation', 'token');
        $response = $broker->reset($credentials, function ($user, $password) {
            $user->forceFill([
                'password' => bcrypt($password),
                'remember_token' => \Illuminate\Support\Str::random(60),
            ])->save();

            \Illuminate\Support\Facades\Auth::login($user);
        });

        if ($response == \Illuminate\Support\Facades\Password::PASSWORD_RESET) {
            session()->flash('forms.reset_customer_password.posted_successfully', true);

        } else {
            session()->flash('forms.reset_customer_password.errors', [trans($response)]);
            session()->flash('forms.reset_customer_password.posted_successfully', false);
        }

        return back();
    }
}
