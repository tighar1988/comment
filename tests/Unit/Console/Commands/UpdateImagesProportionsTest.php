<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\UpdateImagesProportions;
use Mockery;

class UpdateImagesProportionsTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(UpdateImagesProportions::class, new UpdateImagesProportions);
    }
}
