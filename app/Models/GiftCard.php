<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiftCard extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_giftcards';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'order_id',
        'inventory_id',
        'code',
        'expires_at',
        'value',
        'disabled',
        'used',
        'redeemed_at',
        'redeemed_by_customer_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'disabled' => 'boolean',
        'used' => 'boolean',
    ];

    /**
     * Genereate a 16 characters code. Example:
     *
     * A1B2 C3D4 E5F6 G7H8
     */
    public function generateCode()
    {
        $letters = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';
        $numbers = '123456789';
        $code = '';

        for ($i = 0; $i < 16; $i++) {
            if ($i % 2 == 0) {
                $code .= $letters[rand(0, 24)];
            } else {
                $code .= $numbers[rand(0, 8)];
            }
        }

        return $code;
    }

    public function getUniqueCode()
    {
        do {
            $code = $this->generateCode();
        } while (static::where('code', '=', $code)->exists());

        return $code;
    }

    public function getUniqueHash()
    {
        do {
            $hash = md5(str_random());
        } while (static::where('hash', '=', $hash)->exists());

        return $hash;
    }

    public function isUsed()
    {
        return (bool) $this->used;
    }

    public function isDisabled()
    {
        return (bool) $this->disabled;
    }

    public function isValid()
    {
        if (! $this->isDisabled() && ! $this->isUsed()) {
            return true;
        }

        return false;
    }

    public function scopeHash($query, $hash)
    {
        return $query->where('hash', '=', $hash);
    }

    public function scopeCode($query, $code)
    {
        return $query->where('code', '=', $code);
    }

    /**
     * Split the code into 4 parts with spaces betweent them. Example:
     *
     * A2B1 4C3D 5F6E 7G8H
     */
    public function previewCode()
    {
        $parts = str_split($this->code, 4);

        return implode(' ', $parts);
    }
}
