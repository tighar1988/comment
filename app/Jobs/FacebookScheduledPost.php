<?php

namespace App\Jobs;

use App\Mails\ShopMailer;
use App\Models\Post;
use App\Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;

class FacebookScheduledPost
{
    /**
     * Schedule Post job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty(fb_group_owner_id())) {
            log_notice("[schedule] No facebook user id set. Exiting..");
            return;
        }

        $time = time();
        $posts = Post::whereNotNull('scheduled_time')->where('scheduled_time', '<=', time())->get();
        $facebook = new Facebook;

        foreach ($posts as $post) {
            try {
                if (empty($post->image)) {
                    continue;
                }

                log_info("[schedule] Posting scheduled post with id #{$post->id} to Facebook");

                $supplement = json_decode($post->supplement);
                if (! isset($supplement->group)) {
                    continue;
                }

                $postAsPage = false;
                if (isset($supplement->post_as_page) && $supplement->post_as_page) {
                    $postAsPage = true;
                }

                $node = $facebook->post($supplement->group, $supplement->message, $supplement->url, $postAsPage);

                if (isset($node['post_id']) && ! empty($node['post_id'])) {
                    $postData = explode('_', $node['post_id']);

                    $post->fb_id = $node['post_id'];
                    $post->fb_photo_id = $node['id'];
                    $post->fb_author_id = trim($postData[0]);
                    $post->fb_post_id = trim($postData[1]);
                    $post->scheduled_time = null;
                    $post->created_time = time();
                    $post->error_type = null;
                    $post->save();

                    if (isset($supplement->post_comment) && ! empty($supplement->post_comment)) {
                        $facebook->commentOnPost($node['post_id'], $supplement->post_comment);
                    }
                }
            } catch (FacebookResponseException $e) {
                if (str_contains($e->getMessage(), "(#324) Missing or invalid image file")) {
                    log_info("[schedule] Could not publish scheduled post with id #{$post->id} to Facebook");
                    if (str_contains($post->image, 'shopify.com')) {
                        $post->image = '';
                        $post->save();
                    }
                } elseif (str_contains($e->getMessage(), "Permissions error") && isset($postAsPage) && $postAsPage) {
                    log_info("[schedule] Permission error for post with id #{$post->id}");
                    $post->error_type = 'permission_error';
                    $post->save();
                } else {
                    throw $e;
                }
            }
        }
    }
}
