<?php

namespace App\Models;

use App\Models\ShopUser;
use Illuminate\Database\Eloquent\Builder;

class PasswordShopUser extends ShopUser
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_users';

    /**
     * The "booting" method of the model.
     *
     * @return void
    */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('facebook_id', function (Builder $builder) {
            $builder->whereNull('facebook_id');
        });
    }
}
