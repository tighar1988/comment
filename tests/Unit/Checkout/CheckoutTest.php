<?php

namespace Tests\Unit\Reports;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Checkout\Checkout;
use App\Models\ShopUser;
use Mockery;

class CheckoutTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $customer = $this->mockShopUser(['shoppingCart()' => null]);
        $checkout = new Checkout($customer);

        $this->assertInstanceOf(Checkout::class, $checkout);
    }

    /**
     * @test
     * @expectedException \App\Checkout\CheckoutException
     */
    public function it_validates_cart_not_empty()
    {
        $this->mockShopSettings(['local-pickup-enabled' => null]);

        $shoppingCart = $this->mockShoppingCart(['count' => 0]);
        $customer = $this->mockShopUser(['shoppingCart()' => $shoppingCart, 'street_address' => 'Summer Street']);

        $checkout = new Checkout($customer);
        $checkout->withAccountCredit();
    }

    /**
     * @test
     * @expectedException \App\Checkout\CheckoutException
     */
    public function it_validates_shipping_address_not_empty()
    {
        $this->mockShopSettings(['local-pickup-enabled' => null]);

        $customer = $this->mockShopUser(['shoppingCart()' => null, 'street_address' => null]);

        $checkout = new Checkout($customer);
        $checkout->withAccountCredit();
    }

    /** @test */
    public function it_gets_local_pickup_option_when_true()
    {
        $this->mockShopSettings(['local-pickup-enabled' => true]);
        $customer = $this->mockShopUser(['shoppingCart()' => null, 'local_pickup' => true]);

        $checkout = new Checkout($customer);
        $this->assertTrue($checkout->getLocalPickup());
    }

    /** @test */
    public function it_gets_local_pickup_option_when_false()
    {
        $this->mockShopSettings(['local-pickup-enabled' => false]);
        $customer = $this->mockShopUser(['shoppingCart()' => null]);

        $checkout = new Checkout($customer);
        $this->assertFalse($checkout->getLocalPickup());
    }
}
