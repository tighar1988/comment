<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class CurrentPage extends Drop
{
    public function __toString()
    {
        return '1';
    }
}
