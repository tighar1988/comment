<?php

namespace Tests\Unit\OnlineStore;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use App\OnlineStore\LiquidTemplate;

class LiquidTemplateTest extends TestCase
{
    /** @test */
    public function it_runs_text_with_no_tags()
    {
        // $content = '<html>Static text</html>';
        // $parsed = (new LiquidTemplate)->render($content);

        // $this->assertEquals($content, $parsed);
    }

    // /** @test */
    // public function it_replaces_variable()
    // {
    //     $content = 'Hello {{ name }}';
    //     $parsed = (new LiquidTemplate)->render($content, ['name' => 'John Doe']);

    //     $this->assertEquals('Hello John Doe', $parsed);
    // }

    // /** @test */
    // public function it_assigns_a_variable()
    // {
    //     $content = '{% assign my_variable = "tomato" %}{{ my_variable }}';
    //     $parsed = (new LiquidTemplate)->render($content);

    //     $this->assertEquals('tomato', $parsed);
    // }

    // /** @test */
    // public function it_continues_in_for_loop()
    // {
    //     // $content = '
    //     //     {% for i in (1..5) %}
    //     //       {% if i == 4 %}
    //     //         {% continue %}
    //     //         Not displayed
    //     //       {% else %}
    //     //         {{ i }}
    //     //       {% endif %}
    //     //     {% endfor %}
    //     // ';

    //     // $parsed = (new LiquidTemplate)->render($content);

    //     // $this->assertEquals('1 2 3 5', $parsed);
    // }
}
