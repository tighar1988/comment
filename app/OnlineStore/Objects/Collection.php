<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;
use App\Models\Collection as CollectionModel;
use App\OnlineStore\Vendor\Liquid\Tag\TagPaginate;

class Collection extends Drop
{
    protected $collection;

    protected $resultsCount = null;

    protected $resultsProducts = null;

    public function __construct(CollectionModel $collection)
    {
        $this->collection = $collection;
    }

    /**
     * Return the collection handle.
     */
    public function __toString()
    {
        return $this->collection->url;
    }

    /**
     * Returns a list of all product tags in a collection. collection.all_tags will return
     * the full list of tags even when the collection view is filtered.
     *
     * collection.all_tags will return at most 1,000 tags.
     *
     * In comparison, collection.tags returns all tags for a collection for the current view.
     * For example, if a collection is filtered by tag, collection.tags returns only the tags
     * that match the current filter.
     */
    public function all_tags()
    {
        return $this->collection->allTags()->pluck('name')->toArray();
    }

    /**
     * Returns a list of all product types in a collection.
     *
     * Input
     * {% for product_type in collection.all_types %}
     *     {{ product_type | link_to_type }}
     * {% endfor %}
     *
     * Output
     * <a href="/collections/types?q=Accessories" title="Accessories">Accessories</a>
     * <a href="/collections/types?q=Chairs" title="Chairs">Chairs</a>
     * <a href="/collections/types?q=Shoes" title="Shoes">Shoes</a>
     */
    public function all_types()
    {
        return null;
    }

    /**
     * Returns the number of products in a collection. collection.all_products_count will return
     * the total number of products even when the collection view is filtered.
     *
     * In comparison, collection.products_count returns all tags for a collection for the current view.
     * For example, if a collection is filtered by tag, collection.products_count returns the number
     * of products that match the current filter.
     *
     * Input
     * {{ collection.all_products_count }} total products in this collection
     *
     * Output
     * 24 total products in this collection
     */
    public function all_products_count()
    {
        return $this->collection->productsCount();
    }

    /**
     * Returns a list of all product vendors in a collection.
     *
     * Input
     * {% for product_vendor in collection.all_vendors %}
     *     {{ product_vendor | link_to_vendor }}
     * {% endfor %}
     *
     * Output
     * <a href="/collections/vendors?q=Shopify" title="Shopify">Shopify</a>
     * <a href="/collections/vendors?q=Shirt+Company" title="Shirt Company">Shirt Company</a>
     * <a href="/collections/vendors?q=Montezuma" title="Montezuma">Montezuma</a>
     */
    public function all_vendors()
    {
        return null;
    }

    /**
     * Returns the product type on a /collections/types?q=TYPE collection page. For example,
     * an automatic "Shirts" collection lists all products of type "Shirts" in the store:
     * yourstore.myshopify.com/collections/types?q=Shirts.
     *
     * Input
     * {% if collection.current_type %}
     * Browse all our {{ collection.current_type | downcase }}.
     * {% endif %}
     *
     * Output
     * Browse all our shirts.
     */
    public function current_type()
    {
        return null;
    }

    /**
     * Returns the product vendor on a /collections/vendores?q=VENDOR collection page.
     * For example, an automatic "ApparelCo" collection lists all products with the
     * vendor "ApparelCo" in the store: yourstore.myshopify.com/collections/vendors?q=ApparelCo.
     *
     * Input
     * {% if collection.current_vendor %}
     * All products by {{ collection.current_vendor }}.
     * {% endif %}
     *
     * Output
     * All products by ApparelCo.
     */
    public function current_vendor()
    {
        return null;
    }

    /**
     * Return the current collection sort by value, from the url option '?sort_by='
     */
    public function sort_by()
    {
        $sort = null;
        if (isset(CurrentView::$queryString['sort_by'])) {
            $sort = CurrentView::$queryString['sort_by'];
        }

        if (in_array($sort, ['manual', 'best-selling', 'title-ascending', 'title-descending', 'price-descending', 'price-ascending', 'created-descending', 'created-ascending'])) {
            return $sort;
        }

        return 'manual';
    }

    /**
     * Returns the sort order of the collection, which is set on the
     * collection's pagein your Shopify admin.
     *
     * The possible sort orders are:
     *
     * manual
     * best-selling
     * title-ascending
     * title-descending
     * price-ascending
     * price-descending
     * created-ascending
     * created-descending
     */
    public function default_sort_by()
    {
        $sort = $this->collection->sort_order;

        if ($sort == 'manual') {
            return 'manual';

        } elseif ($sort == 'best-selling') {
            return 'best-selling';

        } elseif ($sort == 'alpha-asc') {
            return 'title-ascending';

        } elseif ($sort == 'alpha-desc') {
            return 'title-descending';

        } elseif ($sort == 'price-desc') {
            return 'price-descending';

        } elseif ($sort == 'price-asc') {
            return 'price-ascending';

        } elseif ($sort == 'created-desc') {
            return 'created-descending';

        } elseif ($sort == 'created') {
            return 'created-ascending';
        }

        return 'manual';
    }

    /**
     * Returns the description of the collection.
     */
    public function description()
    {
        return $this->collection->content;
    }

    /**
     * Returns the collection's handle.
     */
    public function handle()
    {
        return $this->collection->url;
    }

    /**
     * Returns the ID number of the collection.
     */
    public function id()
    {
        return $this->collection->id;
    }

    /**
     * Returns the collection image. Use the img_url filter to load the image file from the Shopify
     * content delivery network (CDN). Use an if tag to check for the presence of the image first.
     *
     * Input
     * {% if collection.image %}{{ collection.image | img_url: 'medium' }}{% endif %}
     *
     * Output
     * //cdn.shopify.com/s/files/1/0087/0462/collections/collection-image_medium.png?v=1337103726
     */
    public function image()
    {
        return $this->collection->imageUrl();
    }

    /**
     * If the user is on a collection product page (ie with /collections/collection-handle/products/product-handle)
     * in the URL, we can show next/previous links to other products in the collection.
     *
     * Returns the next product in the collection. Returns nil if there is no next product.
     * {% if collection.next_product %}
     *     {{ 'Next product' | link_to: collection.next_product.url, collection.next_product.title }}
     * {% endif %}
     *
     * Tip
     * You can use collection.next_product.url to return the URL of the next product in the collection.
     */
    public function next_product()
    {
        if (CurrentView::$collectionProduct) {
            $next = $this->collection->getNextFor(CurrentView::$collectionProduct);
            if ($next) {
                return new Product($next);
            }
        }

        return null;
    }

    /**
     * Returns the previous product in the collection. Returns nil if there is no previous product.
     *
     * Tip
     * You can use collection.previous_product.url to return the URL of the previous product in the collection.
     */
    public function previous_product()
    {
        if (CurrentView::$collectionProduct) {
            $previous = $this->collection->getPreviousFor(CurrentView::$collectionProduct);
            if ($previous) {
                return new Product($previous);
            }
        }

        return null;
    }

    /**
     * Returns all of the products in a collection. You can show a maximum of 50 products per page.
     *
     * Use the paginate tag to choose how many products are shown per page.
     */
    public function products()
    {
        // todo: select only items with images:
        // if (isset($item->image->filename)) {
        // } -> in count as well

        if (! is_null($this->resultsProducts)) {
            return $this->resultsProducts;
        }

        $limit = 50;
        if (is_numeric(CurrentView::$numberItems)) {
            $limit = CurrentView::$numberItems;
        }

        $offset = 0;
        if (is_numeric(CurrentView::$currentOffset)) {
            $offset = CurrentView::$currentOffset;
        }

        $query = $this->collection->storeProducts();

        if (shop_id() == 'cheekys') {
            $query->selectRaw("`products`.*, `collection_product`.`collection_id` as `pivot_collection_id`,
                `collection_product`.`product_id` as `pivot_product_id`,
                (SELECT SUM(quantity) FROM inventory WHERE products.id = inventory.product_id
                    GROUP BY inventory.product_id) as inventory_quantity");
            $query->orderBy('inventory_quantity', 'DESC');
            $query->orderBy('products.id', 'DESC');
        }

        $items = $query->with('inventory', 'images', 'image')
            ->offset($offset)
            ->limit($limit)
            ->get();

        $this->resultsProducts = [];
        foreach ($items as $item) {
            $this->resultsProducts[] = new Product($item);
        }

        TagPaginate::$dbCollectionSize = $this->products_count();
        CurrentView::$totalItems = TagPaginate::$dbCollectionSize;

        return $this->resultsProducts;
    }

    /**
     * Returns the number of products in a collection that match the current view. For example,
     * if you are viewing a collection filtered by tag, collection.products_count will return
     * the number of products that match the chosen tag.
     *
     * Input
     * {{ collection.products_count }} products
     *
     * Output
     * 6 products
     */
    public function products_count()
    {
        // todo: add filter by tags
        if (! is_null($this->resultsCount)) {
            return $this->resultsCount;
        }

        $this->resultsCount = $this->collection->storeProducts()->count();

        return $this->resultsCount;
    }

    /**
     * Returns the date and time when the collection was published. You can set this information on
     * the collection's page in your Shopify admin by the Set publish date calendar icon.
     *
     * You can use a date filter to format the date.
     */
    public function published_at()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the name of the custom collection template assigned to the collection, without the
     * collection. prefix or the .liquid extension. Returns nil if a custom template is not
     * assigned to the collection.
     *
     * Input
     * {{ collection.template_suffix }}
     *
     * Output
     * no-price
     */
    public function template_suffix()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the title of the collection.
     *
     * Input
     * <h1>{{ collection.title }}</h1>
     *
     * Output
     * <h1>Frontpage</h1>
     */
    public function title()
    {
        return $this->collection->title;
    }

    /**
     * Returns the tags of products in a collection that match the current view. For example, if you are
     * viewing a collection filtered by tag, collection.tags will return the tags for the products that
     * match the current filter.
     */
    public function tags()
    {
        // todo: implement
        return null;
    }

    /**
     * Returns the URL of the collection.
     */
    public function url()
    {
        return $this->collection->getUrl();
    }
}
