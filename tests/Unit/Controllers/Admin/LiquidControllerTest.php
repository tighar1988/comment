<?php

namespace Tests\Unit\Controllers\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;

class LiquidControllerTest extends TestCase
{
    // public function mockForAdminViews()
    // {
    //     // mock for views
    //     $this->mockShopSettings([
    //         'instagram.token'       => 'instagram-token',
    //         'store.enabled'         => true,
    //         'paypal:card-declined'  => null,
    //         'facebook.user_id'      => null,
    //         'stripe.stripe_user_id' => null,
    //         'paypal.client_id'      => null,
    //         'shop.shipping-cost'    => null,
    //         'shop.zip_code'         => null,
    //         'facebook.access_token' => null,
    //         'paypal.client_id'      => null,
    //         'stripe.access_token'   => null,
    //         'shopify.access_token'  => null,
    //     ]);

    //     $model = Mockery::mock('App\Models\Product');
    //     $model->shouldReceive('count')->once()->andReturn(0);
    //     $this->app->instance('App\Models\Product', $model);

    //     $model = Mockery::mock('App\Models\Post');
    //     $model->shouldReceive('scheduledCount')->once()->andReturn(0);
    //     $this->app->instance('App\Models\Post', $model);

    //     $model = Mockery::mock('App\Models\InstagramPost');
    //     $model->shouldReceive('count')->once()->andReturn(0);
    //     $this->app->instance('App\Models\InstagramPost', $model);

    //     $model = Mockery::mock('App\Models\Order');
    //     $model->shouldReceive('paidCount')->once()->andReturn(0);
    //     $model->shouldReceive('fulfilledCount')->once()->andReturn(0);
    //     $model->shouldReceive('firstFulfilledAt')->once()->andReturn(123456789);
    //     $this->app->instance('App\Models\Order', $model);

    //     $model = Mockery::mock('App\Models\Coupon');
    //     $model->shouldReceive('count')->once()->andReturn(0);
    //     $this->app->instance('App\Models\Coupon', $model);
    // }

    // /** @test */
    // public function test_index()
    // {
    //     $this->mockForAdminViews();

    //     $model = Mockery::mock('\App\Models\Liquid');
    //     $model->shouldReceive('type')->with('layouts')->andReturnSelf()
    //         ->shouldReceive('all')->once()->andReturn([]);
    //     $this->app->instance('App\Models\Liquid', $model);

    //     $response = $this->actingAs($this->makeAdmin())
    //         ->get('/admin/store/themes/layouts');

    //     $response->assertStatus(200)
    //         ->assertViewHas('liquid', []);
    // }

    // /** @test */
    // public function test_create()
    // {
    //     $this->mockForAdminViews();

    //     $response = $this->actingAs($this->makeAdmin())
    //         ->get('/admin/store/liquid/add');

    //     $response->assertStatus(200);
    // }

    // /** @test */
    // public function test_store()
    // {
    //     $this->mockUniqueValidation();

    //     $model = Mockery::mock('\App\Models\Liquid');
    //     $model->shouldReceive('fill')->once()->with(['file_name' => 'my-file', 'content' => 'html content']);
    //     $model->shouldReceive('save')->once();

    //     $this->app->instance('App\Models\Liquid', $model);

    //     $response = $this->actingAs($this->makeAdmin())
    //         ->post('/admin/store/liquid/add', ['file_name' => 'my-file', 'content' => 'html content']);

    //     $response->assertRedirect('/admin/store/liquid');
    // }

    // /** @test */
    // public function test_edit()
    // {
    //     $this->mockForAdminViews();

    //     $model = Mockery::mock('\App\Models\Liquid');
    //     $model->shouldReceive('find')->once()->with(1);

    //     $this->app->instance('App\Models\Liquid', $model);


    //     $response = $this->actingAs($this->makeAdmin())
    //         ->get('/admin/store/liquid/edit/1');

    //     $response->assertStatus(200)
    //         ->assertViewHas('liquid');
    // }

    // /** @test */
    // public function test_update()
    // {
    //     $this->mockUniqueValidation();

    //     $liquid = Mockery::mock('\App\Models\Liquid');
    //     $liquid->shouldReceive('fill')->once()->with(['file_name' => 'file-2', 'content' => 'content 2']);
    //     $liquid->shouldReceive('save')->once();

    //     $model = Mockery::mock('\App\Models\Liquid');
    //     $model->shouldReceive('find')->once()->with(1)->andReturn($liquid);

    //     $this->app->instance('App\Models\Liquid', $model);

    //     $response = $this->actingAs($this->makeAdmin())
    //         ->patch('/admin/store/liquid/edit/1', ['file_name' => 'file-2', 'content' => 'content 2']);

    //     $response->assertRedirect('/admin/store/liquid');
    // }

    /** @test */
    public function test_delete()
    {
        $model = Mockery::mock('\App\Models\Liquid');
        $model->shouldReceive('destroy')->once()->with(1);

        $this->app->instance('App\Models\Liquid', $model);

        $response = $this->actingAs($this->makeAdmin())
            ->post('/admin/store/themes/templates/delete/1');

        $response->assertRedirect('/admin/store/themes/templates');
    }
}
