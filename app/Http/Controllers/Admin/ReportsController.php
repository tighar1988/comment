<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ReportsFormRequest;
use App\Reports\Report;
use App\Models\Waitlist;
use App\Models\Cart;
use App\Models\InventoryLog;

class ReportsController extends Controller
{
    public function index()
    {
        $report = new Report;
        $report->generate();
        return view('admin.reports', compact('report'));
    }

    public function retrieve(ReportsFormRequest $request)
    {
        if (shop_id() == 'fashionten' || shop_id() == 'julesandjamesboutique') {
            ini_set('memory_limit', '712M');
            ini_set('max_execution_time', 120);
        }
        $report = new Report(request('start_date'), request('end_date'));
        $report->generate();
        return view('admin.reports', compact('report'));
    }

    public function waitlistByProduct()
    {
        $waitlist = Waitlist::reportByProduct(request('q'))->get(); //->paginate(10);
        return view('admin.waitlist-by-product', compact('waitlist'));
    }

    public function waitlistByVariant()
    {
        $waitlist = Waitlist::reportByVariant(request('q'))->get(); //->paginate(10);
        return view('admin.waitlist-by-variant', compact('waitlist'));
    }

    public function shoppingCart()
    {
        $shoppingCart = Cart::report(request('q'))->get(); //->paginate(10);
        return view('admin.shopping-cart', compact('shoppingCart'));
    }

    public function inventoryLogs()
    {
        $search = request('q');
        $logs = [];

        if (! empty($search)) {
            $logs = InventoryLog::report($search)->paginate(10);
        }

        return view('admin.inventory-logs', compact('logs'));
    }
}
