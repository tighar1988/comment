@extends('admin.store.store')

@section('sub-nav')
<div class="nav-subtabs-custom">
    <a class="{{ activeLink('admin/store/themes/assets') }}" href="/admin/store/themes/assets">Assets</a> |
    <a class="{{ activeLink('admin/store/themes/config') }}" href="/admin/store/themes/config">Config</a> |
    <a class="{{ activeLink('admin/store/themes/layout') }}" href="/admin/store/themes/layout">Layout</a> |
    <a class="{{ activeLink('admin/store/themes/locales') }}" href="/admin/store/themes/locales">Locales</a> |
    <a class="{{ activeLink('admin/store/themes/snippets') }}" href="/admin/store/themes/snippets">Snippets</a> |
    <a class="{{ activeLink('admin/store/themes/templates') }}" href="/admin/store/themes/templates">Templates</a> |
    <a class="{{ activeLink('admin/store/themes/sections') }}" href="/admin/store/themes/sections">Sections</a>
</div>
@endsection
