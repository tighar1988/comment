<?php

use Illuminate\Database\Seeder;

class SetupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setup')->insert([
            ['metric' => 'admin', 'value' => '{"user":"manager","password":"fe1f6a58fbe4936b200ca33fc1129d8c"}', 'stamped' => 0],
            ['metric' => 'super', 'value' => '{"startTime":1475412850,"totalPosts":0}', 'stamped' => 0],
            ['metric' => 'fbid', 'value' => '["904361466363382","EAAQ2kKqOC7wBAKoIAEVpOFBfOhMXFfe5ZCRLNO889eECg88NiwB0mB2qiwsdDIAIT53KJm16qEPhUOJBwH6VZBvblTCoTqV4x5YHNS0QvKEnhe3mGwu9ZAD5A7cWITluLTLakDdqd98QHa81j6zX7OXY2lbqm4ZD"]', 'stamped' => 1475966031],
        ]);
    }
}
