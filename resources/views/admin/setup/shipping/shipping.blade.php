@extends('admin.setup.setup')

@section('section', 'Shipping')

@section('sub-nav')
    <div class="nav-subtabs-custom">
        <a class="{{ activeLink('admin/setup/shipping') }}" href="/admin/setup/shipping">Shipping Cost</a> |
        <a class="{{ activeLink('admin/setup/locations') }}" href="/admin/setup/locations">Multiple Locations</a>
        @if (usps_enabled())
            | <a class="{{ activeLink('admin/setup/shipping/prepay-labels') }}" href="/admin/setup/shipping/prepay-labels">Prepay Labels</a>
        @endif
    </div>
@endsection
