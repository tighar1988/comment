@extends('layouts.admin')

@section('content')
<?php
    $shopifyEnabled = shopify_enabled();
    if (shop_id() == 'glamourfarms') {
        $shopifyEnabled = false;
    }
?>
<section class="content-header">
    <div class="row">
        <div class="col-sm-2"><h1>Products</h1></div>
        <div class="col-sm-10">
            <div class="text-right ctrl-btn">
                @if ((shop_id() == 'shop1' || shop_id() == 'cheekys' || shop_id() == 'glamourfarms') && shop_setting('store.enabled'))
                    <a href="/admin/giftcards" class="page-btn btn btn-link btn-sm">Gift cards</a>
                    <a href="/admin/products/add?type=giftcard" class="page-btn btn btn-link btn-sm">Add gift card product</a>
                @endif
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Products</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">

                    @if ($shopifyEnabled && shop_id() != 'joeyeric')
                        <a href="https://{{ shopify_url() }}/admin/products/new" class="btn btn-success add-btn btn-sm" target="_blank">Add Product</a>
                    @else
                        <a href="/admin/products/add" class="btn btn-success add-btn btn-sm">Add Product</a>
                    @endif
                    <div class="table-responsive">
                        <table id="products" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>SKU</th>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Last Post</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>

<div class="modal fade" id="clipboard-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Copy to clipboard?</h4>
            </div>
            <div class="modal-body"><p id="copied-text" style="white-space: pre-wrap;"></p></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary copy-btn" data-clipboard-target="#copied-text" onclick="$('#clipboard-modal').modal('hide')">Copy Text</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $pageId = shop_setting('facebook.page.page_id'); ?>
@include('admin.products.link-post-modal')
@include('admin.products.link-page-post-modal')
@endsection

@section('scripts')
<script type="text/javascript">
    var clipboard = new Clipboard('.copy-btn');

    function link_to_facebook_page_post(button) {
        $('#link-page-post-modal').modal();
        var href = $(button).attr('href');

        if ($.fn.DataTable.isDataTable("#table-facebook-page-posts")) {
            $('#table-facebook-page-posts').DataTable().clear().destroy();
        }

        $('#table-facebook-page-posts').DataTable({
            "oLanguage" : {"sEmptyTable": "No Facebook Page posts found."},
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            "ajax": "/admin/products/facebook-pages-posts",
            columns: [
                { data: 'picture', 'searchable': false, render: function(data, type, row) {
                    if (row.picture) {
                        return '<img width="100" src="'+row.picture+'" alt="">';
                    } else {
                        return '<img width="100" src="/images/default.png" alt="">';
                    }
                }},
                { data: 'message', 'searchable': false, render: function(data, type, row) {
                    if (row.message) {
                        return row.message.substring(0, 100) + '...';
                    } else {
                        return '';
                    }
                }},
                { data: null, 'searchable': false, 'orderable': false, className: 'text-center', render: function(data, type, row) {
                    return '<form class="form-horizontal" method="POST" action="'+href+'"><input type="hidden" value="'+row.id+'" name="post_id"><input type="hidden" value="'+row.page_id+'" name="page_id"><button type="submit" class="btn btn-danger" data-loading-text="Linking.." onclick="$(this).button(\'loading\');">Link</button></form>';
                }}
            ]
        });

        return false;
    }

    function link_to_facebook_post(button) {
        $('#link-post-modal').modal();
        var href = $(button).attr('href');

        if ($.fn.DataTable.isDataTable("#table-facebook-posts")) {
            $('#table-facebook-posts').DataTable().clear().destroy();
        }

        $('#table-facebook-posts').DataTable({
            "oLanguage" : {"sEmptyTable": "No Facebook posts found."},
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            "ajax": "/admin/products/facebook-posts",
            columns: [
                { data: 'picture', 'searchable': false, render: function(data, type, row) {
                    return '<img width="100" src="'+row.picture+'" alt="">';
                }},
                { data: 'message', 'searchable': false, render: function(data, type, row) {
                    if (row.message) {
                        return row.message.substring(0, 100) + '...';
                    } else {
                        return '';
                    }
                }},
                { data: null, 'searchable': false, 'orderable': false, className: 'text-center', render: function(data, type, row) {
                    return '<form class="form-horizontal" method="POST" action="'+href+'"><input type="hidden" value="'+row.id+'" name="post_id"><input type="hidden" value="'+row.group_id+'" name="group_id"><button type="submit" class="btn btn-danger" data-loading-text="Linking.." onclick="$(this).button(\'loading\');">Link</button></form>';
                }}
            ]
        });

        return false;
    }

    function copy_text_to_clipboard(button) {
        $.ajax({
            type:'get',
            url: $(button).attr('href'),
            success: function(response) {
                $('#clipboard-modal .modal-body p').text(response.text);
                $('#clipboard-modal').modal('show');
            }
        });

        return false;
    }

    @if ($shopifyEnabled)
        var shopify_is_enabled = true;
    @else
        var shopify_is_enabled = false;
    @endif

    $(document).ready(function() {
        $('#products').DataTable({
            "oLanguage" : {"sEmptyTable": "No products found. Please <a href='/admin/products/add'>Create one</a>"},
            "processing": true,
            "serverSide": true,
            "order": [[ 0, "desc" ]],
            "ajax": "/admin/products/datatable",
            "bStateSave": true,
            columns: [
                { data: 'id', 'searchable': false, render: function(data, type, row) {
                    return '<img width="125px" height="125px" src="'+row.thumb+'" alt="'+row.product_name+'">';
                }},
                { data: 'style', 'searchable': true, render: function(data, type, row) {
                    if (shopify_is_enabled) {
                        return '<a href="/admin/products/'+row.id+'/variants">'+row.style+'</a>';
                    } else {
                        return '<a href="/admin/products/edit/'+row.id+'">'+row.style+'</a>';
                    }
                }},
                { data: 'product_name'},
                { data: 'inventory_quantity', 'searchable': false, render: function(data, type, row) {
                    return '<a href="/admin/products/'+row.id+'/variants">'+row.inventory_quantity+'</a>';

                }},

                { data: 'latest_post', 'searchable': false},
                { data: null, 'searchable': false, 'orderable': false, className: 'text-center', render: function(data, type, row) {
                    var html = '';
                    html += '<div class="dropdown">';
                    html +=     '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
                    html +=         '<i class="fa fa-ellipsis-h fa-lg"></i>';
                    html +=     '</a>';
                    html +=     '<ul class="dropdown-menu pull-right">';

                    // allow edit for when shopify is disabled; or enabled and it's an old product
                    @if (! $shopifyEnabled)
                        html +=         '<li><a href="/admin/products/edit/'+row.id+'" class="link">Edit</a></li>';
                    @else
                        if (! row.shopify_product_id) {
                            html +=         '<li><a href="/admin/products/edit/'+row.id+'" class="link">Edit</a></li>';
                        }
                    @endif

                    html +=         '<li><a href="/admin/products/'+row.id+'/images" class="link">Edit Images</a></li>';

                    @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                        html +=         '<li><a href="/admin/products/'+row.id+'/variants" class="link">Inventory</a></li>';
                    @endif

                    html +=         '<li><a href="/admin/products/'+row.id+'/facebook-post" class="link">Post to Facebook</a></li>';
                    html +=         '<li><a href="/admin/products/'+row.id+'/link-to-facebook-post" class="link" onclick="return link_to_facebook_post(this);">Link to Post</a></li>';

                    @if ($pageId)
                        html +=     '<li><a href="/admin/products/'+row.id+'/link-to-facebook-page" class="link" onclick="return link_to_facebook_page_post(this);">Link to Page Post</a></li>';
                    @endif

                    if (row.image) {
                        html +=         '<li><a download href="'+row.image+'" class="link">Download Image</a></li>';
                    }
                    html +=         '<li><a href="/admin/products/'+row.id+'/copy-text" class="link" onclick="return copy_text_to_clipboard(this);">Copy Text</a></li>';

                    // allow delete for when shopify is disabled; or enabled and it's an old product
                    @if (! $shopifyEnabled)
                       html +=         '<li><a href="/admin/products/delete/'+row.id+'" class="link" onclick="return confirm_delete(this);">Remove</a></li>';
                    @else
                        if (! row.shopify_product_id) {
                           html +=         '<li><a href="/admin/products/delete/'+row.id+'" class="link" onclick="return confirm_delete(this);">Remove</a></li>';
                        }
                    @endif

                    html +=     '</ul>';
                    html += '</div>';
                    return html;
                }}
            ]
        });
    });
</script>
@endsection
