@extends('layouts.admin')

@section('content')
@if (empty($groups))
    <div class="row"><div class="top-alert flash-notification alert alert-danger alert-important">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        You have no facebook groups enabled. Go to <a href="/admin/facebook-setup">Facebook Setup</a> to connect your facebook and enable groups.</div></div>
@elseif (empty(($image->filename ?? null)))
    <div class="row"><div class="top-alert flash-notification alert alert-danger alert-important">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        This product has no image. In order to post to Facebook an image is needed. Go to <a href="/admin/products/{{ $product->id }}/images">Product Images</a> to add images.</div></div>
@endif
<section class="content-header">
    <h1>Product ({{ $product->style }})</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/products"><i class="fa fa-th-large"></i> Products</a></li>
        <li><a href="/admin/products/edit/{{ $product->id }}">{{ $product->product_name }}</a></li>
        <li class="active">Post to Facebook</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body lr-m-30">

                    <div class="row">
                        <div class="col-md-6">

                            <form class="form-horizontal" id="post-to-facebook" role="form" method="POST" action="/admin/products/{{ $product->id }}/facebook-post">

                                <input type="hidden" name="post_edit" value="{{ $post->id }}">

                                <div class="form-group">
                                    <label>Select the group to post your product. You have to own the group to post.</label>
                                </div>

                                <div class="form-group @hasError('group')">
                                    <label class="control-label">Facebook Group</label>
                                    <select class="form-control" name="group">
                                        @if (count($enabledGroups) > 1 || empty($enabledGroups))
                                            <option value="">Select a facebook group..</option>
                                        @endif
                                        @foreach ($groups as $group)
                                            @if (in_array($group['id'], $enabledGroups))
                                                <option @if(old('group', $supplement->group) == $group['id']) selected="selected" @endif value="{{ $group['id'] }}">{{ $group['name'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @error('group')
                                </div>

                                <div class="form-group @hasError('message')">
                                    <label class="control-label">Post Message</label>
                                    <textarea class="form-control" rows="10" name="message">{{ old('message', $supplement->message) }}</textarea>
                                    <blockquote style="margin-top: 5px;">
                                        <p style="white-space: pre;">Copy & paste your shop url to 'Post Message'<br/><span class="text-muted">"Go to {{ shop_url('/account') }} to view/pay your invoice."</span></p>
                                    </blockquote>
                                    @error('message')
                                </div>

                                <div class="form-group">
                                    <img src="{{ product_thumb(($image->filename ?? null)) }}" />
                                </div>

                                <div class="form-group @hasError('timing')">
                                    <label class="control-label">Schedule Timing</label>
                                    <select name="timing" class="form-control" onchange="swapSchedule(this)">
                                        <option @if(old('timing', 2) == 1) selected="selected" @endif value="1">Post Immediately</option>
                                        <option @if(old('timing', 2) == 2) selected="selected" @endif value="2">Schedule Later</option>
                                    </select>
                                    @error('timing')
                                </div>

                                <div id="later-date" @if(old('timing', 2) != '2') style="display: none;" @endif>

                                    @if (! shop_setting('shop.timezone'))
                                        <div class="row">
                                            <div class="alert alert-danger">
                                                In order to calculate the correct post time relative to your timezone, please select your timezone in <a href="/admin/setup#timezone">Setup</a>.
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group @hasError('schedule_date')">
                                        <label class="control-label"><i>Scheduled Date</i></label>
                                        <select class="form-control" name="schedule_date">
                                            @for ($i=0; $i<=30; $i++)
                                                <?php
                                                    $stamp = time() + (60 * 60 * 24 * $i);
                                                    $date = date('n/j/Y', $stamp);
                                                ?>
                                                <option @if(old('schedule_date', $postDay) == $date) selected="selected" @endif value="{{ $date }}">{{ date('F jS', $stamp) }}</option>
                                            @endfor
                                        </select>
                                        @error('schedule_date')
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label"><i>Scheduled Time</i></label>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select class="form-control" name="hour">
                                                    @for ($i = 0; $i <= 12; $i++)
                                                        <option @if(old('hour', $postHour) == $i) selected="selected" @endif value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control" name="min">
                                                    @for ($i = 0; $i <= 11; $i++)
                                                        <?php
                                                            $min = $i * 5;
                                                            if (strlen($min) == 1) {
                                                                $min = '0' . $min;
                                                            }
                                                        ?>
                                                        <option @if(old('min', $postMinute) == $min) selected="selected" @endif value="{{$min}}">{{$min}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control" name="ampm">
                                                    <option @if(old('ampm', $postAmPm) == 'am') selected="selected" @endif value="am">AM</option>
                                                    <option @if(old('ampm', $postAmPm) == 'pm') selected="selected" @endif value="pm">PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Post Comment <span class="text-muted">(Optional)</span></label>
                                    <input class="form-control" type="text" name="post_comment" value="{{ old('post_comment', $supplement->post_comment ?? null) }}"/>
                                    <div><small class="text-muted">This will be added as the first comment under the post. It is useful for sharing YouTube videos.</small></div>
                                </div>

                                <br/>

                                <div class="form-group">
                                    <button class="btn btn-success" data-loading-text="Submitting..." type="submit">Submit to Facebook</button>
                                </div>

                            </form>

                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#post-to-facebook').submit(function() {
        $('button[type="submit"]', this).button('loading');
        return true;
    });
</script>
@endsection
