<?php

namespace App\Jobs;

use App\Facebook\FacebookMessenger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facebook\MessengerTemplates;
use App\Jobs\Traits\ReconnectShopDB;
use Log;
use Exception;
use DB;
use App\Models\Master;
use App\Models\ShopUser;
use Redis;

class HandleFacebookMessage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    /**
     * The $_REQUEST array from the web request
     *
     * @var array
     */
    protected $request;

    protected $messenger;

    protected $templates;

    /**
     * Create a new job instance.
     *
     * @param array $request
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Handle the incoming facebook message
     *
     * Note this could be several messages wrapped into a single request
     * https://developers.facebook.com/docs/messenger-platform/webhook-reference#batching
     *
     * @return void
     */
    public function handle()
    {
        // Iterate over each entry - there may be multiple if batched
        foreach ($this->request['entry'] as $entry) {

            // Iterate over each messaging event
            foreach ($entry['messaging'] as $event) {

                $shop = Redis::connection('cache')->get("page:{$entry['id']}");
                if (empty($shop)) {
                    Log::info("[messenger] Could not map entry id to shop db: " . json_encode($entry));
                    continue;
                }

                $this->reconnectDB($shop);
                $this->messenger = new FacebookMessenger;
                $this->templates = new MessengerTemplates;
                $this->messenger->setPageAccessToken(fb_page_access_token());

                if (isset($event['message']['text'])) {
                    $this->handleReceivedMessage($event);

                } elseif (isset($event['optin'])) {
                    $this->handleOptin($event);

                } elseif (isset($event['postback'])) {
                    $this->handlePostback($event);

                } else {
                    log_error("[messenger] Webhook received unknown event for page {$entry['id']}: " . json_encode($event));
                    continue;
                }
            }
        }
    }

    /**
     * Handle optin, where the user first connects his messenger.
     */
    public function handleOptin($event)
    {
        $recipientID   = $event['recipient']['id'] ?? null;
        $timeOfMessage = $event['timestamp'] ?? null;
        $userRef       = $event['optin']['user_ref'] ?? null;
        $userId = trim(explode('-', $userRef)[1]);

        $user = ShopUser::find($userId);
        if (! $user) {
            log_info("[messenger][optin] Could not find user with user_ref {$userRef}");
            return;
        }

        // send the first optin message to the user, and save the messenger_id
        if (empty($user->messenger_id)) {
            $response = $this->messenger->sendTextMessage($userRef, $this->templates->firstOptin())
                ->getDecodedBody();
            $user->messenger_id = $response['recipient_id'];
        }

        // set the new updated user_ref every time
        $user->user_ref = $userRef;
        $user->save();

        log_info("[messenger][optin] Customer with user_ref {$userRef} ({$user->name}) connected his messenger {$user->messenger_id}");
    }

    /**
     * Handle when the user sends a normal text message to the page.
     */
    public function handleReceivedMessage($event)
    {
        $senderID      = $event['sender']['id'] ?? null;
        $recipientID   = $event['recipient']['id'] ?? null;
        $timeOfMessage = $event['timestamp'] ?? null;
        $messageId     = $event['message']['mid'] ?? null;
        $messageText   = trim($event['message']['text'] ?? null);

        // if ($recipientID == '1700128273612246') {
        //     \Log::info($event);
        //     $this->messenger->callSendAPI([
        //         'recipient' => [
        //             'id' => $senderID,
        //         ],
        //         'message' => [
        //             'text' => "We will remind you about your outstanding bill later.",
        //         ],
        //     ]);
        //     \App\Mails\ShopMailer::notifyAdmin('Trigger app review message later for Facebook reviewer: '.$senderID);
        // }

        // $user = ShopUser::getByMessengerId($senderID);
        if (strtolower($messageText) == 'post' || shop_setting('messenger.send-contact-message')) {
            $this->messenger->callSendAPI([
                'recipient' => [
                    'id' => $senderID,
                ],
                'message' => [
                    'text' => "Hello, please contact " . shop_email() . " for support. Thank you!",
                ],
            ]);

            log_info("[messenger][received-message] Could not find user with messenger id {$senderID}");
            return;
        }

        // If we receive a text message, check to see if it matches a keyword
        // and send back the example. Otherwise, just echo the text we received.
        // switch ($messageText) {
        //     case 'post':
        //         $messageText = "I got you my friend! The upcoming posts this week start on Friday at 3pm! We will let you know when it's available!";
        //         break;

        //     default:
        //         $messageText = "Hello, please contact " . shop_email() . " for support. Thank you!";
        // }

        // log_info("[messenger][received-message] Sending message to user #{$user->id} ({$user->name})");
        // $this->messenger->sendTextMessage($user->user_ref, $messageText);
    }

    /**
     * Handle postbacks from messenger buttons.
     */
    public function handlePostback($event)
    {
        $senderID       = $event['sender']['id'] ?? null;
        $recipientID    = $event['recipient']['id'] ?? null;
        $timeOfPostback = $event['timestamp'] ?? null;
        $postback       = $event['postback']['payload'] ?? null;

        $user = ShopUser::getByMessengerId($senderID);
        if (! $user) {
            log_info("[messenger][postback] Could not find user with messenger id {$senderID}");
            return;
        }

        switch ($postback) {
            case 'post':
                $messageText = "Postback message";
                break;

            default:
                $messageText = "Unknown postback.";
        }

        log_info("[messenger][postback] Sending message to user #{$user->id} ({$user->name})");
        $this->messenger->sendTextMessage($user->user_ref, $messageText);
    }
}
