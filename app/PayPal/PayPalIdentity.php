<?php

namespace App\PayPal;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\OpenIdSession;
use PayPal\Api\OpenIdTokeninfo;
use PayPal\Api\OpenIdUserinfo;
use Exception;

class PayPalIdentity
{
    protected $clientId;

    protected $clientSecret;

    protected $apiContext;

    public function __construct()
    {
        $this->clientId = config('services.paypal.client_id');
        $this->clientSecret = config('services.paypal.client_secret');

        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->clientId, $this->clientSecret));

        $this->apiContext->setConfig([
            'mode'           => env('APP_ENV') == 'testing' ? 'SANDBOX' : 'LIVE',
            'log.LogEnabled' => true,
            'log.FileName'   => storage_path('paypal/PayPalIdentity.log'),
            'log.LogLevel'   => 'INFO', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
        ]);
    }

    public function getLoginUrl($baseUrl)
    {
        // Get Authorization URL returns the redirect URL that could be used to get user's consent
        $redirectUrl = OpenIdSession::getAuthorizationUrl($baseUrl, [
                'openid',
                'email',
                // 'profile',
                // 'address',
                // 'phone',
                // 'https://uri.paypal.com/services/paypalattributes',
                // 'https://uri.paypal.com/services/expresscheckout',
                // 'https://uri.paypal.com/services/invoicing',
            ], null, null, null, $this->apiContext);

        return $redirectUrl;
    }

    public function getAccessToken($code)
    {
        return OpenIdTokeninfo::createFromAuthorizationCode(['code' => $code], null, null, $this->apiContext);
    }

    public function getUserInfo($accessToken)
    {
        return OpenIdUserinfo::getUserinfo(['access_token' => $accessToken], $this->apiContext);
    }
}
