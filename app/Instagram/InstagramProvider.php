<?php

namespace App\Instagram;

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

class InstagramProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * The separating character for the requested scopes.
     *
     * @var string
     */
    protected $scopeSeparator = ' ';

    /**
     * The scopes being requested.
     *
     * @var array
     */
    protected $scopes = ['basic', 'public_content', 'comments'];

    public function setScopes($scopes)
    {
        $this->scopes = $scopes;
    }

    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://api.instagram.com/oauth/authorize', $state);
    }

    protected function getTokenUrl()
    {
        return 'https://api.instagram.com/oauth/access_token';
    }

    protected function mapUserToObject(array $user)
    {
        return (new User())->setRaw($user)->map([
            'id' => $user['id'], 'nickname' => $user['username'],
            'name' => $user['full_name'], 'email' => null,
            'avatar' => $user['profile_picture'],
        ]);
    }

    public function getAccessToken($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'form_params' => $this->getTokenFields($code),
        ]);
        $this->credentialsResponseBody = json_decode($response->getBody(), true);
        return $this->parseAccessToken($response->getBody());
    }

    protected function getTokenFields($code)
    {
        return array_merge(parent::getTokenFields($code), [
            'grant_type' => 'authorization_code',
        ]);
    }

    protected function getUserByToken($token)
    {
        return $this->getInstagramEndpoint('/users/self', $token);
    }

    public function mediaRecent($token)
    {
        return $this->getInstagramEndpoint('/users/self/media/recent', $token);
    }

    public function mediaById($token, $mediaId)
    {
        return $this->getInstagramEndpoint("/media/{$mediaId}", $token);
    }

    public function comments($token, $mediaId)
    {
        return $this->getInstagramEndpoint("/media/{$mediaId}/comments", $token);
    }

    public function userByName($token, $userName)
    {
        $users = $this->getInstagramEndpoint("/users/search", ['q' => $userName, 'access_token' => $token]);
        return $users[0] ?? null;
    }

    protected function getInstagramEndpoint($endpoint, $token)
    {
        if (is_array($token)) {
            $query = $token;
        } else {
            $query = ['access_token' => $token];
        }
        $signature = $this->generateSignature($endpoint, $query);
        $query['sig'] = $signature;

        $response = $this->getHttpClient()->get(
            'https://api.instagram.com/v1/' . ltrim($endpoint, '/'), [
            'query' => $query,
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true)['data'];
    }

    /**
     * Allows compatibility for signed API requests.
     */
    protected function generateSignature($endpoint, array $params)
    {
        $sig = $endpoint;
        ksort($params);
        foreach ($params as $key => $val) {
            $sig .= "|$key=$val";
        }
        $signing_key = $this->clientSecret;
        return hash_hmac('sha256', $sig, $signing_key, false);
    }
}
