<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Vendor\Liquid\Drop as LiquidDrop;

class Drop extends LiquidDrop
{
    protected $cache = [];

    /**
     * Invoke a specific method
     *
     * @param string $method
     *
     * @return mixed
     */
    public function invokeDrop($method)
    {
        if (! isset($this->cache[$method]) && is_callable(array($this, $method))) {
            $this->cache[$method] = $this->$method();
        } elseif (! isset($this->cache[$method]) && method_exists($this, 'handle_key')) {
            $this->cache[$method] = $this->handle_key($method);
        }

        return $this->cache[$method] ?? null;
    }

    public function toLiquid()
    {
        return $this;
    }

    public function __toString()
    {
        return '';
    }
}
