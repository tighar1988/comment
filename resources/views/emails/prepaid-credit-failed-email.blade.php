<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $shopOwnerName }},
    <br/><hr/><br/>

    There was an error charging your card for your Labels Prepaid Credit (${{ amount($amount) }}).
    <br/><br/>

    Please login to your account and update your card in Setup > Billing.
    <br/><br/>

    The auto charge has been disabled, please enable it again after you update your card.
</div>
