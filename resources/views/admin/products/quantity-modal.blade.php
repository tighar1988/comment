<div class="modal fade" id="quantity-modal-add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="" item="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Quantity</h4><span class="variant-name text-muted"></span>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Quantity:</label>
                        <input type="text" name="quantity" data-itemId="" autocomplete="off" id="incoming_quantity" class="form-control quantity">
                        <span class="help-block" style="display: none"><strong></strong></span>
                    </div>
                </div>
                <div class="modal-footer">
                    @if (shop_id() == 'cheekys')
                    <button type="button" class="btn btn-warning" onclick="printZebraBarcode_Single($('#quantity-modal-add .quantity'))">Print New</button>
                    @endif
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" data-loading-text="Adding Quantity..." class="btn btn-success">Add Quantity</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="quantity-modal-subtract" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Subtract Quantity</h4><span class="variant-name text-muted"></span>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Quantity:</label>
                        <input type="text" name="quantity" data-itemId="" autocomplete="off" class="form-control quantity">
                        <span class="help-block" style="display: none"><strong></strong></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" data-loading-text="Subtracting Quantity..." class="btn btn-danger">Subtract Quantity</button>
                </div>
            </form>
        </div>
    </div>
</div>
