<div style="font-family: Verdana; font-size: 12px;">
    Hello {{ $customerName }},
    <br/><hr/><br/>

    You have pending products in your shopping cart.
    Please login at <a href="{{ $shopUrl }}">My Account</a> to update or pay for your order, otherwise it will be at risk for expiring.
    <br/><br/>
</div>
