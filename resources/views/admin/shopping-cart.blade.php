@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Shopping Cart</h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-th-large"></i>Overview of items still in people's cart, grouped by variant</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body r-p">
                    <div class="table-responsive">
                        <table id="shoppingcart-table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>SKU</th>
                                    <th># of items</th>
                                    <th>Name</th>
                                    <th>Vendor</th>
                                    <th>Vendor-style</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{--
                                    @if ($shoppingCart->isEmpty())
                                        <tr><td colspan="6">No results found.</td></tr>
                                    @endif
                                --}}
                                @foreach ($shoppingCart as $item)
                                    <tr>
                                        <td><a href="/admin/products/{{ $item->product_id }}/variants">{{ $item->size }}</a></td>
                                        <td><a href="/admin/products/{{ $item->product_id }}/variants">{{ $item->color }}</a></td>
                                        <td><a href="/admin/products/{{ $item->product_id }}/variants">{{ $item->sku }}</a></td>
                                        <td>{{ $item->count }}</td>
                                        <td>{{ $item->product_name }}</td>
                                        <td>{{ $item->brand }}</td>
                                        <td>{{ $item->brand_style }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#shoppingcart-table').DataTable({
            "bStateSave": true
        });
    });
</script>
@endsection
