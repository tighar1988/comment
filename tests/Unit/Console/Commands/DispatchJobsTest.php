<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\DispatchJobs;
use Mockery;

class DispatchJobsTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(DispatchJobs::class, new DispatchJobs);
    }
}
