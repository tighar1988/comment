<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $orderId = $this->data['orderId'] ?? null;
        $shopName = $this->data['shopName'] ?? null;
        $shopFromEmail = $this->data['shopFromEmail'] ?? config('mail.from.address');

        return $this->subject("Your {$shopName} order (#{$orderId}) has been dispatched")
            ->replyTo($this->data['shopEmail'], $this->data['shopName'])
            ->from($shopFromEmail, $this->data['shopName'])
            ->markdown('emails.orders.shipped')
            ->with($this->data);
    }
}
