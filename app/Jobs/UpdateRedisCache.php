<?php

namespace App\Jobs;

use Exception;
use Redis;

class UpdateRedisCache
{
    /**
     * Update Redis cache job.
     *
     * @return void
     */
    public function handle()
    {
        // messenger page id
        $pageId = fb_page_id();
        if ($pageId) {
            Redis::connection('cache')->set("page:{$pageId}", shop_id());
        }

        // harvest page id
        $pageId = shop_setting('facebook.page.page_id');
        if ($pageId) {
            Redis::connection('cache')->set("page:{$pageId}", shop_id());
        }

        // instagram id
        $instagramId = shop_setting('instagram.id');
        if ($instagramId) {
            Redis::connection('cache')->set("instagramId:{$instagramId}", shop_id());
        }

        // shopify shop name
        $shopifyShop = shop_setting('shopify.shopify-shop-name');
        if ($shopifyShop) {
            Redis::connection('cache')->set("shopify-shop:{$shopifyShop}", shop_id());
        }

        return;
    }
}
