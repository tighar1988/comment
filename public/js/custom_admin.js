$(document).ready(function () {
    $('.edit_form').on('click', function () {
        $(this).parents('tr').hide();
        $(this).parents('tr').next('tr').show();
    });
    $('.add_button').on('click', function () {
        $('.form').removeClass('invisible');
        $('.add_button').addClass('disabled');
        $('.edit_form').addClass('disabled');
        $('.delete_tr').addClass('disabled');
    });
    $('.cancel').on('click', function () {
        $(this).parents('tr').hide().parents('tr').hide();
        $(this).parents('tr').prev('tr').show();
    });
    $('#cancel').on('click', function () {
        $('.form').addClass('invisible');
        $('.add_button').removeClass('disabled');
        $('.edit_form').removeClass('disabled');
        $('.delete_tr').removeClass('disabled');
    });
    $('.update_form').on('click', function () {
        var element = $(this);
        var element_row = element.parents('tr');
        var data = {
            'location': element_row.find('input[name="location"]').val(),
            'state': element_row.find('select[name="state"]').val(),
            'zip_code': element_row.find('input[name="zip_code"]').val(),
            'address': element_row.find('input[name="address"]').val(),
            'id': element_row.attr('data-id'),
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'Put',
            url: "/admin/setup/locations/" + data.id,
            dataType: 'json',
            data: data,
            success: function (response) {
                element_row.prev('tr').find('td[data-info="location"]').text(data.location);
                element_row.prev('tr').find('td[data-info="state"]').text(data.state);
                element_row.prev('tr').find('td[data-info="zip_code"]').text(data.zip_code);
                element_row.prev('tr').find('td[data-info="address"]').text(data.address);
                $('#final_msg').fadeIn().delay(2000).fadeOut();
            },
            error: function (message) {
                var errors = message.responseJSON;
                var errorsHtml = '<div class="alert alert-danger"><ul>';

                $.each(errors, function (key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                errorsHtml += '</ul></div>';
                $('#form-errors').html(errorsHtml).fadeIn().delay(2000).fadeOut();
            }
        });
    });
    $('input[name=default]').on('change', function () {
        var data_id = $(this).parents('tr').attr('data-id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'Post',
            url: "/admin/setup/change-default-location",
            data: {
                id: data_id
            },
            success: function (response) {

            }
        });
    })
});
