<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Waitlist;
use App\Models\Inventory;
use App\Models\InventoryLog;
use App\Http\Requests\Admin\InventoryQuantityFormRequest;
use App\Http\Requests\Admin\UpdateVariantsFormRequest;
use App\Http\Requests\Admin\AddVariantFormRequest;

class ProductVariantsController extends Controller
{
    public function index($id)
    {
        // if (shop_id() == 'fillyflair' || shop_id() == 'shopzigzagstripe') {
        //     return redirect('/admin/products');
        // }

        $product = Product::find($id);

        if (is_null($product)) {
            flash('The product you are trying to view has been deleted.', 'danger');
            return redirect('/admin/products');
        }

        // todo: add how many items are in the shopping cart also
        // todo: add back hanging 'inventory.onShelfCount'
        $product->load('inventory.waitlistCount', 'inventory.soldCount');
        return view('admin.products.variants-list', compact('product'));
    }

    public function updateVariants($id, UpdateVariantsFormRequest $request)
    {
        $product = Product::find($id);

        if ($request->has('is_json')) {
            $values = json_decode($request->input('values'));
            $inputs = [];
            foreach ($values as $key => $value) {
                $inputs[$key] = $value;
            }
        } else {
            $inputs = $request->input();
        }

        if (is_null($product)) {
            if ($request->has('is_json')) {
                abort(403);
            }

            flash('The product you are trying to update has been deleted.', 'danger');
            return redirect('/admin/products');
        }

        foreach ($inputs as $key => $item) {
            if (substr($key, 0, 2) == 'w-') {
                $inventoryId = substr($key, 2);

                $w = $inputs['w-'.$inventoryId] ?? null; // weight
                $r = $inputs['r-'.$inventoryId] ?? null; // price
                $c = $inputs['c-'.$inventoryId] ?? null; // cost
                $l = $inputs['l-'.$inventoryId] ?? null; // location
                $s = $inputs['s-'.$inventoryId] ?? null; // sale price
                $b = $inputs['b-'.$inventoryId] ?? null; // barcode

                if ($w == '') $w = 0;
                if ($r == '') $r = 0;
                if ($c == '') $c = 0;
                if ($s == '') $s = 0;
                if ($l == '') $l = null;
                if ($b == '') $b = null;

                Inventory::where('id', $inventoryId)->update([
                    'weight'   => $w,
                    'price'    => $r,
                    'cost'     => $c,
                    'location' => $l,
                    'sale_price' => $s,
                ]);

                if (! empty($b)) {
                    Inventory::where('id', $inventoryId)->update(['shopify_barcode' => $b]);
                }

            } elseif (request('shopify_enabled') && substr($key, 0, 2) == 'c-') {
                $inventoryId = substr($key, 2);

                $c = $inputs['c-'.$inventoryId];
                $l = $inputs['l-'.$inventoryId];

                if ($c == '') $c = 0;
                if ($l == '') $l = null;

                Inventory::where('id', $inventoryId)->update([
                    'cost'     => $c,
                    'location' => $l,
                ]);
            }
        }

        flash('Inventory Updated!');
        if ($request->has('is_json')) {
            return ['status' => 1];
        }

        return redirect('/admin/products/'.$id.'/variants');
    }

    public function addQuantity($id, InventoryQuantityFormRequest $request)
    {
        $quantity = request('quantity');
        $inventory = Inventory::find($id);
        $inventory->incrementQuantity("Admin add quantity: {$quantity}.", $quantity);
        return response()->json(['status' => 1, 'quantity' => $inventory->quantity]);
    }

    public function subtractQuantity($id, InventoryQuantityFormRequest $request)
    {
        $quantity = request('quantity');
        $inventory = Inventory::find($id);
        $inventory->decrementQuantity("Admin subtract quantity: {$quantity}.", $quantity);
        return response()->json(['status' => 1, 'quantity' => $inventory->quantity]);
    }

    public function storeVariant($productId, AddVariantFormRequest $request)
    {
        Inventory::newItem([
            'product_id' => $productId,
            'color'      => trim($request->input('color')),
            'size'       => trim($request->input('size')),
            'quantity'   => 0,
            'cost'       => $request->input('cost'),
            'price'      => $request->input('price'),
        ]);

        flash('Variant added!');
        return $request->ajax() ? ['status' => 1] : back();
    }

    public function updateVariant($inventoryId)
    {
        $inventory = Inventory::find($inventoryId);
        $inventory->color = trim(request('color'));
        $inventory->size = trim(request('size'));
        $inventory->save();

        if (shop_setting('labels-enabled')) {
            flash()->overlay('The variant was updated. Please make sure to reprint barcodes for this item.', 'Variant Updated');
        } else {
            flash('The variant was updated!');
        }

        return back();
    }

    public function deleteVariant($inventoryId)
    {
        $inventory = Inventory::find($inventoryId);
        $inventory->delete();
        flash('Inventory Item Deleted!');
        return back();
    }
}
