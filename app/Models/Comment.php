<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'fb_id',
        'parent_id',
        'stamped',
        'user_id',
        'content',
    ];

    public static function exists($fbid)
    {
        return (bool) static::where('fb_id', $fbid)->first();
    }

    public static function alreadyParsed($comment)
    {
        $dbComment = static::where('fb_id', $comment->id)->first();

        // check the comment exists in db, and the message is different (for edited comments)
        if ($dbComment && strpos($dbComment->fb_id, '_') !== false && trim($dbComment->content) == trim(mysql_utf8($comment->message))) {
            return true;
        }

        return false;
    }

    public static function store($postId, $comment)
    {
        if (! $comment->isReplyComment()) {
            $parentId = null;
        } else {
            $parentId = $comment->parentId;
        }

        if ($comment->hasCombinedId()) {
            $commentId = $comment->combinedId;
        } else {
            $commentId = $comment->id;
        }

        return static::create([
            'post_id'   => $postId,
            'fb_id'     => $commentId,
            'parent_id' => $parentId,
            'stamped'   => $comment->createdTime,
            'user_id'   => $comment->fromId,
            'content'   => mysql_utf8($comment->message),
        ]);
    }

    public static function markIsOrder($commentId)
    {
        static::where('id', $commentId)->update(['is_order' => true]);
    }
}
