<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\ShoppingCart\ShoppingCart;
use App\Waitlist\ShoppingWaitlist;
use App\Models\Card;
use App\Models\Waitlist;
use App\Checkout\Checkout;
use App\Models\BalanceMovement;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Cart;
use App\Models\Inventory;
use App\Models\Coupon;
use App\Models\TaxRate;
use App\Models\MultipleStoreLocation;
use Carbon\Carbon;
use App\Models\Helpers\StripeConnectBillable;
use App\ShoppingCart\CartItem;
use DB;

class ShopUser extends Authenticatable
{
    use Notifiable, StripeConnectBillable;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_address',
        'city',
        'state',
        'zip',
        'apartment',
        'phone_number',
        'location_id',
        'country_code',
        'accepts_marketing',
        'verified_email',
        'first_name',
        'last_name',
        'shopify_customer_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'instagram_data'    => 'json',
        'options'           => 'array',
        'accepts_marketing' => 'boolean',
        'verified_email'    => 'boolean',
    ];

    public function shoppingCart()
    {
        return new ShoppingCart($this, new Coupon, new TaxRate);
    }

    public function cartItems()
    {
        // get the session cart items first
        $cart = session('online-store.cart', []);
        $sessionItems = [];
        $sessionVariants = [];
        foreach ($cart as $inventoryId => $cartItem) {
            for ($i=0; $i < $cartItem['quantity']; $i++) {
                $tmp = new \StdClass;
                $tmp->id = 'session'.$inventoryId;
                $tmp->order_source = OrderProduct::SOURCE_STORE;
                $tmp->variant = Inventory::find($inventoryId);
                if (is_null($tmp->variant)) {
                    continue;
                }
                $tmp->product = $tmp->variant->product;
                $sessionItems[] = new CartItem($tmp);
                $sessionVariants[] = $tmp;
            }
        }

        // the user is logged in
        $cartItems = [];
        if (! is_null($this->id) && $this->id != 0) {

            if (! empty($sessionVariants)) {
                // if the customer has items in his session cart, move them to the db cart
                foreach ($sessionVariants as $sessionVariant) {
                    if (! isset($sessionVariant->variant->quantity)) {
                        continue;
                    }

                    if ($sessionVariant->variant->quantity > 0) {
                        Cart::storeAddItem($this, $sessionVariant->variant);
                    } else {
                        Waitlist::storeAddItem($this, $sessionVariant->variant);
                    }
                }

                // reset the session cart
                $sessionItems = [];
                session(['online-store.cart' => []]);
            }

            $items = Cart::forCustomer($this->id)->with('variant.product.image')->get();

            $cartItems = array_map(function($item) {
                return new CartItem($item);
            }, $items->all());
        }

        return array_merge($sessionItems, $cartItems);
    }

    public function waitlist()
    {
        return new ShoppingWaitlist($this);
    }

    public function newWaitlistCount()
    {
        $lastVisit = $this->getOption('last-visit-time');
        if (is_null($lastVisit) || ! is_numeric($lastVisit)) {
            return 0;
        }

        $lastVisit = Carbon::createFromTimestamp($lastVisit)->toDateTimeString();
        return Waitlist::forCustomer($this->id)->since($lastVisit)->count();
    }

    public function checkout()
    {
        return new Checkout($this);
    }

    public static function getByFacebookId($facebookId)
    {
        return static::where('facebook_id', $facebookId)->first();
    }

    public static function getByInstagramId($instagramId)
    {
        return static::where('instagram_id', $instagramId)->first();
    }

    public static function getByMessengerId($messengerId)
    {
        return static::where('messenger_id', $messengerId)->first();
    }

    public function getAddress()
    {
        $address = $this->street_address . ($this->apartment ? $this->apartment . ', ' : '');
        $address .= $this->city.', '.$this->state.', '.$this->zip;

        return $address;
    }

    public function setOption($key, $value)
    {
        $options = $this->options;
        if (! is_array($options)) {
            $options = [];
        }

        $options[$key] = $value;

        $this->options = $options;
    }

    public function getOption($key)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }

        return null;
    }

    public function hasStoreLocation()
    {
        if (empty($this->location_id)) {
            return false;
        }

        return true;
    }

    public function storeLocation()
    {
        if (empty($this->location_id)) {
            return null;
        }

        $location = MultipleStoreLocation::find($this->location_id);
        if (! is_null($location)) {
            return $location->address;
        }

        return null;
    }

    public function sawOnboardingPopup()
    {
        if (isset($this->options['saw-onboarding-popup'])) {
            return true;
        }

        return false;
    }

    public function addBalance($amount, $message)
    {
        $amount = floatval(abs($amount));
        static::whereId($this->id)->increment('balance', $amount);

        BalanceMovement::log($this->id, $amount, $message);
        $this->balance += $amount;
    }

    public function reduceBalance($amount, $message)
    {
        $amount = floatval(abs($amount));

        static::whereId($this->id)->where('balance', '>', 0)->update([
            'balance' => DB::raw("GREATEST(balance - {$amount}, 0)"),
        ]);

        BalanceMovement::log($this->id, -1 * $amount, $message);
        $this->balance = max($this->balance - $amount, 0);
    }

    public function hasBalance()
    {
        return $this->balance > 0;
    }

    public function balanceMovement()
    {
        return $this->hasMany('App\Models\BalanceMovement', 'customer_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id', 'id')->with('orderProducts.product.image');
    }

    public function ordersPaid()
    {
        return $this->orders()->paid();
    }

    public function ordersFulfilled()
    {
        return $this->orders()->fulfilled();
    }

    public function ordersCount()
    {
        return $this->orders()->count();
    }

    public function lastOrderTime()
    {
        return $this->hasOne('App\Models\Order', 'customer_id', 'id')->orderBy('id', 'desc');
    }

    public function lastOrderTimestamp()
    {
        $lastOrder = Order::where('customer_id', '=', $this->id)
            ->orderBy('id', 'desc')
            ->first();

        if ($lastOrder && $lastOrder->created_at instanceof \Carbon\Carbon) {
            return $lastOrder->created_at->getTimestamp();
        }

        return null;
    }

    public function orderedInLast24hours()
    {
        $lastOrder = $this->lastOrderTime;
        $lastOrderTime = $lastOrder->payment_date ?? null;

        if (is_null($lastOrderTime) || ! is_numeric($lastOrderTime)) {
            return false;
        }

        $timePassed = abs(time() - $lastOrderTime);
        if ($timePassed < 24 * 60 * 60) {
            return true;
        }

        return false;
    }

    public function freeShippingExpireTime()
    {
        $lastOrderTime = $this->lastOrderTime->payment_date ?? null;

        return $lastOrderTime + 24 * 60 * 60;
    }

    public function scopeNameOrEmail($query, $search = null)
    {
        if (! empty($search)) {
            $query->where('name', 'like', "%{$search}%")->orWhere('email', 'like', "%{$search}%");
        }

        return $query;
    }

    public function scopeWithEmailOnFile($query)
    {
        return $query->whereNotNull('email')->where('email', '!=', '');
    }

    public function scopeWithFacebookMessenger($query)
    {
        return $query->whereNotNull('messenger_id')->where('messenger_id', '!=', '');
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->whereBetween('created_at', [$start, $end]);
    }

    public function nrProductsReturned()
    {
        return Order::where('customer_id', '=', $this->id)
            ->join('orders_products', 'orders.id', '=', 'orders_products.order_id')
            ->whereNotNull('orders_products.returned_date')
            ->count();
    }

    public function nrProductsOrdered()
    {
        return Order::where('customer_id', '=', $this->id)
            ->join('orders_products', 'orders.id', '=', 'orders_products.order_id')
            ->count();
    }

    public function totalSpent()
    {
        return Order::where('customer_id', '=', $this->id)->sum('total');
    }

    public function returnPercentage()
    {
        $returned = $this->nrProductsReturned();
        $ordered = $this->nrProductsOrdered();

        if ($ordered == 0) {
            return 0;
        }

        return $returned * 100 / $ordered;
    }

    public function firstName()
    {
        $name = preg_replace('/  /', ' ', trim($this->name));
        return explode(' ', $name)[0] ?? null;
    }

    public function lastName()
    {
        $name = preg_replace('/  /', ' ', trim($this->name));
        return explode(' ', $name)[1] ?? null;
    }

    public function stateName()
    {
        $states = ['AL' => 'Alabama','AK' => 'Alaska','AS' => 'American Samoa','AZ' => 'Arizona','AR' => 'Arkansas','AA' => 'Armed Forces America','AE' => 'Armed Forces Europe','AP' => 'Armed Forces Pacific','CA' => 'California','CO' => 'Colorado','CT' => 'Connecticut','DE' => 'Delaware','DC' => 'District Of Columbia','FL' => 'Florida','GA' => 'Georgia','GU' => 'Guam','HI' => 'Hawaii','ID' => 'Idaho','IL' => 'Illinois','IN' => 'Indiana','IA' => 'Iowa','KS' => 'Kansas','KY' => 'Kentucky','LA' => 'Louisiana','ME' => 'Maine','MD' => 'Maryland','MA' => 'Massachusetts','MI' => 'Michigan','MN' => 'Minnesota','MS' => 'Mississippi','MO' => 'Missouri','MT' => 'Montana','NE' => 'Nebraska','NV' => 'Nevada','NH' => 'New Hampshire','NJ' => 'New Jersey','NM' => 'New Mexico','NY' => 'New York','NC' => 'North Carolina','ND' => 'North Dakota','MP' => 'Northern Mariana Is','NS' => 'Nova Scotia','OH' => 'Ohio','OK' => 'Oklahoma','OR' => 'Oregon','PW' => 'Palau','PA' => 'Pennsylvania','PR' => 'Puerto Rico','RI' => 'Rhode Island','SC' => 'South Carolina','SD' => 'South Dakota','TN' => 'Tennessee','TX' => 'Texas','UT' => 'Utah','VT' => 'Vermont','VI' => 'Virgin Islands','VA' => 'Virginia','WA' => 'Washington','WV' => 'West Virginia','WI' => 'Wisconsin','WY' => 'Wyoming'];

        return $states[$this->state] ?? null;
    }

    public function getCountryCode()
    {
        if (empty($this->country_code)) {
            return 'US';
        }

        return $this->country_code;
    }

    public static function findOrCreateUser($facebookUser)
    {
        $user = ShopUser::where('facebook_id', $facebookUser->id)->first();

        if ($user) {

            // todo: update the user information on each login; it's useful when creating accounts for people who comment without an account
            log_info("[FB] User {$user->name} just logged in.");

            // in the case an user has no email, and the new facebook login provides an email, then update the user email
            if (empty($user->email) && ! empty($facebookUser->getEmail())) {
                $user->email = $facebookUser->getEmail();
                $user->save();
                log_info("[FB] Updating email for {$user->name} to {$user->email}.");
            }

            return $user;
        }

        log_info("[FB] User {$facebookUser->getName()} just registered and logged in.");
        return ShopUser::forceCreate([
            'facebook_id' => $facebookUser->getId(),
            'nickname'    => $facebookUser->getNickname(),
            'name'        => $facebookUser->getName(),
            'email'       => $facebookUser->getEmail(),
            'avatar'      => $facebookUser->getAvatar(),
        ]);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $url = shop_url('/customers/reset-password/'.$token);
        $this->notify(new \App\Notifications\ResetPasswordNotification($url, from_email_address(), shop_email(), shop_name()));
    }
}
