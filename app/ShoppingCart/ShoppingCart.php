<?php

namespace App\ShoppingCart;

use App\ShoppingCart\CartItem;
use App\Models\ShopUser;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\TaxRate;
use Illuminate\Support\Facades\Auth;

class ShoppingCart
{
    /**
     * The shop customer.
     *
     * @var App\Models\ShopUser
     */
    protected $customer;

    /**
     * The coupon model.
     *
     * @var App\Models\Coupon
     */
    protected $couponModel;

    /**
     * The tax rate model.
     *
     * @var App\Models\TaxRate
     */
    protected $taxRateModel;

    /**
     * The coupon.
     *
     * @var App\Models\Coupon
     */
    protected $coupon;

    public $coupon_code;

    /**
     * The cart items.
     *
     * @var array of CartItem
     */
    public $items;

    public $subtotal = 0;

    public $shipping_price = 0;

    public $tax_total = 0;

    public $state_tax = 0;

    public $county_tax = 0;

    public $municipal_tax = 0;

    public $coupon_discount = 0;

    public $apply_balance = 0;

    public $deduced_balance = 0;

    public $isFullyPaidWithBalance = false;

    public $total = 0;

    public $hourFreeShipping = false;

    public $hourFreeShippingExpireTime;

    public function __construct(ShopUser $customer, Coupon $couponModel, TaxRate $taxRateModel)
    {
        $this->customer     = $customer;
        $this->couponModel  = $couponModel;
        $this->taxRateModel = $taxRateModel;

        $this->items = $customer->cartItems();

        $this->coupon = $this->getCartCoupon($this->customer);
        $this->coupon_code = $this->coupon->code ?? null;

        $this->calculate();
    }

    protected function calculate()
    {
        // calculate subtotal first
        $this->subtotal = $this->subtotal();

        $this->coupon_discount = $this->getCouponDiscount();

        $this->shipping_price = $this->shippingPrice();

        $this->tax_total = $this->getTaxTotal();

        $this->apply_balance = $this->getApplyBalance();

        // calculate total last
        $this->total = $this->subtotal + $this->shipping_price + $this->tax_total - $this->coupon_discount;
        $this->total = floatval($this->total);
        if ($this->total < 0) {
            // in case of negative number because of coupon set to 0
            $this->total = 0;
        }

        if ($this->apply_balance > 0) {
            if (($this->total - $this->apply_balance) > 0) {
                $this->deduced_balance = $this->apply_balance;
                $this->total -= $this->apply_balance;
            } else {
                $this->deduced_balance = $this->total;
                $this->total = 0;
                $this->isFullyPaidWithBalance = true;
            }
        }
    }

    protected function getCouponDiscount()
    {
        if ($this->coupon && $this->coupon->isTypeFixed()) {
            return floatval($this->coupon->amount);

        } elseif ($this->coupon && $this->coupon->isTypePercentage()) {
            $percentage = $this->coupon->amount / 100;
            return floatval($percentage * $this->subtotal);
        }

        return 0;
    }

    protected function getApplyBalance()
    {
        if ($this->customer->hasBalance()) {
            if (static::shouldApplyBalance() || $this->customer->getOption('shopping-cart.apply-balance')){
                return floatval($this->customer->balance);
            }
        }

        return 0;
    }

    protected function getTaxTotal()
    {
        // 0 tax for customers from a different state
        $shopState = shop_setting('shop.state');
        if ($this->customer->state != $shopState) {
            return 0;
        }

        $taxable = $this->taxableSubtotal();
        if (shop_setting('multiple-locations-enabled')) {
            $taxRate = $this->taxRateModel->getMasterMaxTaxRates();
        } else {
            $taxRate = $this->taxRateModel->getMasterTaxRate(shop_setting('shop.zip_code'));
        }

        // state tax
        $stateTax = max(($taxRate->estimated_special_rate ?? 0), ($taxRate->state_rate ?? 0));
        $this->state_tax = round($taxable * floatval($stateTax), 2);

        // county tax
        $countyTax = $taxRate->estimated_county_rate ?? 0;
        $this->county_tax = round($taxable * floatval($countyTax), 2);

        // municipal tax
        $municipalTax = $taxRate->estimated_city_rate ?? 0;
        $this->municipal_tax = round($taxable * floatval($municipalTax), 2);

        if ($shopState == 'KY') {
            // it's 6% for all KY origin based shops
            $this->state_tax = round($taxable * 0.06, 2);
            $this->county_tax = 0;
            $this->municipal_tax = 0;
        }

        if (shop_id() == 'somethingheliotrope') {
            // use only county tax
            $this->state_tax = 0;
            $this->municipal_tax = 0;
        }

        if (shop_id() == 'pinklionboutique') {
            // it's 8.25% for all Texas customers
            $this->state_tax = round(($taxable + $this->shipping_price) * 0.0825, 2);
            $this->county_tax = 0;
            $this->municipal_tax = 0;
        }

        return round($this->state_tax + $this->county_tax + $this->municipal_tax, 2);
    }

    protected function taxableSubtotal()
    {
        $taxable = 0;

        foreach ($this->items as $item) {
            if ($item->charge_taxes) {
                $taxable += $item->price();
            }
        }

        // apply tax only after discounting
        if ($this->coupon_discount > 0) {
            if ($this->shipping_price > 0 && $this->coupon_discount > $this->shipping_price) {
                $taxable = max($taxable + $this->shipping_price - $this->coupon_discount, 0);
            } else {
                $taxable = max($taxable - $this->coupon_discount, 0);
            }
        }

        return floatval($taxable);
    }

    public function shippingPrice()
    {
        if (local_pickup_enabled() && $this->customer->local_pickup) {
            return 0;
        }

        $shippingPrice = shop_setting('shop.shipping-cost', 10);

        $variableShippingCost = shop_setting('shop.variable-shipping-cost');
        if (! is_null($variableShippingCost) && $variableShippingCost > 0) {
            $shippingPrice += abs($variableShippingCost * ($this->count() - 1));
        }

        // free shipping coupon
        if ($this->coupon && $this->coupon->isTypeShipping()) {
            $shippingPrice = 0;
        }

        // free shipping if the order if larger than $75 by default
        $maximum = shop_setting('shop.free-shipping-maximum', 75);
        if (! is_null($maximum) && is_numeric($maximum) && $maximum > 0) {
            if (($this->subtotal - $this->coupon_discount) >= $maximum) {
                $shippingPrice = 0;
            }
        }

        // free shipping if they paid in the last 24hrs
        if (free_shipping_24hr_enabled() && $this->customer->orderedInLast24hours()) {
            $shippingPrice = 0;
        }

        // give free shipping if they pay within 1 hour
        if (shop_id() == 'divas' || shop_id() == 'julesandjamesboutique') {
            foreach ($this->items as $item) {
                $secondsPassed = abs(time() - $item->created_at);
                if ($secondsPassed < (60 * 60)) {
                    $this->hourFreeShipping = true;
                    $this->hourFreeShippingExpireTime = time() + (60*60 - $secondsPassed);
                }
            }

            if ($this->hourFreeShipping) {
                $shippingPrice = 0;
            }
        }

        if (shop_id() == 'cheekys' && $shippingPrice > 0) {
            $nrBittyBags = 0;
            foreach ($this->items as $item) {
                if (str_contains(strtolower($item->product_style), 'bitty') || str_contains(strtolower($item->name), 'bitty')) {
                    $nrBittyBags++;
                }
            }

            if ($nrBittyBags > 0 && $nrBittyBags == $this->count()) {
                // if all items are bitty bags - shipping is $0
                $shippingPrice = 0;

            } elseif ($nrBittyBags > 0 && $nrBittyBags < $this->count()) {
                // if there are bitty bags and other normal items, don't charge variable shipping cost for the bitty bags
                $shippingPrice -= ($nrBittyBags * $variableShippingCost);

                if ($shippingPrice < 0) {
                    $shippingPrice = 0;
                }
            }
        }
        if (shop_id() == 'poppyanddot') {
            $user = Auth::user();
            if ($user->country_code == 'CA') {
                $shippingPrice = 5;
            } elseif ($user->country_code == 'US') {
                $shippingPrice = 0;
            } else {
                $shippingPrice = 10;
            }
        }
        return floatval($shippingPrice);
    }

    public static function subtotalFor(ShopUser $customer)
    {
        $subtotal = 0;

        foreach ($customer->cartItems() as $item) {
            $subtotal += $item->price();
        }

        return floatval($subtotal);
    }

    protected function subtotal()
    {
        $subtotal = 0;

        foreach ($this->items as $item) {
            $subtotal += $item->price();
        }

        return floatval($subtotal);
    }

    protected function getCartCoupon($customer)
    {
        $couponId = session('shopping-cart.coupon') ?: $customer->getOption('shopping-cart.coupon');

        if (! empty($couponId) && $coupon = $this->couponModel->find($couponId)) {
            if ($coupon->isValid($customer, $this->subtotal())) {
                return $coupon;
            } else {
                session()->forget('shopping-cart.coupon');
                self::setApplyCouponMobile($customer, '');
            }
        }

        return null;
    }

    public static function applyCoupon($couponId)
    {
        // add the coupon to the shopping cart session
        session(['shopping-cart.coupon' => $couponId]);
    }

    public static function setApplyCouponMobile($customer, $couponId = '')
    {
        $customer->setOption('shopping-cart.coupon', $couponId);
        $customer->save();
    }

    public static function applyBalance()
    {
        // add the coupon to the shopping cart session
        session(['shopping-cart.apply-balance' => true]);
    }

    public static function setApplyBalanceMobile($customer, $status = false)
    {
        $customer->setOption('shopping-cart.apply-balance', $status);
        $customer->save();
    }

    protected static function shouldApplyBalance()
    {
        return (bool) session('shopping-cart.apply-balance');
    }

    public static function removeItem($itemId, $message = null)
    {
        Cart::removeItem($itemId, $message);
    }

    public function empty()
    {
        // empty the session cart
        session(['online-store.cart' => []]);

        // delete items from the cart
        Cart::where('user_id', $this->customer->id)->delete();

        // mark the coupon as used in the database
        if ($this->coupon) {
            $this->coupon->increaseUseCount();
        }

        // remove the coupon from the session
        session()->forget('shopping-cart.coupon');
        self::setApplyCouponMobile($this->customer, '');
    }

    public function count()
    {
        return count($this->items);
    }

    public function totalWithoutShippingAndTax()
    {
        $amount = $this->total - $this->shipping_price - $this->tax_total;

        if ($amount < 0) {
            $amount = 0;
        }

        return $amount;
    }

    public function totalInCents()
    {
        return $this->toCents($this->total);
    }

    public function toCents($amount)
    {
        return intval($amount * 100);
    }

    /**
     * If they have a coupon for $30 but the order is $25, what happens to the remaining $5?
     *
     * We just credit the difference to their account.
     */
    public function unusedCouponAmount()
    {
        if ($this->coupon && $this->coupon->isTypeFixed()) {
            $total = $this->subtotal + $this->shipping_price + $this->tax_total;

            if ($this->coupon_discount > $total) {
                return floatval($this->coupon_discount - $total);
            }
        }

        return 0;
    }
}
