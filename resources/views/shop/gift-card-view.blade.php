<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Here's your ${{ amount($giftcard->value) }} gift card for {{ shop_name() }}!</title>
    <meta name="description" content="Your gift card">
    <link rel="stylesheet" href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ cdn('/css/bootstrap.min.css') }}" type="text/css" />
    <style type="text/css">

    </style>
    <style type="text/css" media="print">
        .no-print {
            display: none;
        }
    </style>
</head>
<body class="template-giftcard">
    <header class="text-center" role="banner">
        <div class="h1 text-center">{{ shop_name() }}</div>
        <h1>Your gift card</h1>
    </header>

    <div class="container">
        @if ($giftcard->isUsed())
            <div class="alert alert-danger">This gift card was already used!</div>
        @elseif ($giftcard->isDisabled())
            <div class="alert alert-danger">This gift card was disabled!</div>
        @endif

        <div class="row">
            <div class="col-md-12 text-center">
                <div class="giftcard-wrap">
                    @if ($image)
                        <img style="max-height: 400px; max-width: 500px;" src="{{ $image }}">
                    @else
                        <img src="{{ cdn('/images/gift-card-blue.png') }}">
                    @endif
                    <div class="h1"><strong>${{ amount($giftcard->value) }}</strong></div>
                    <div class="giftcard-code">
                        <div class="giftcard-code-inner">
                            <strong class="giftcard-code-text">{{ $giftcard->previewCode() }}</strong>
                        </div>
                    </div>
                </div>
                <p class="text-center">Use this code at checkout to redeem your ${{ amount($giftcard->value) }} gift card</p>
                <br/>
                <div class="no-print text-center">
                    <p>
                        <a href="{{ shop_url('/') }}" class="btn btn--primary btn-info" target="_blank">
                            Start shopping
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </a>
                    </p>
                    <p>
                        <button type="button" class="btn btn--secondary print-link" onclick="window.print();">Print this gift card</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
