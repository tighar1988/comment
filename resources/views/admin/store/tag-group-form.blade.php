@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($tagGroup) ? true : false; ?>
<section class="content-header">
    <h1>Store</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/tags"><i class="fa fa-th-large"></i>Store</a></li>
        <li><a href="/admin/store/tags">Tag Groups</a></li>
        @if ($isEdit)
            <li class="active">{{ $tagGroup->name }}</li>
        @else
            <li class="active">Add</li>
        @endif
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('name')">
                                    <label>Tag Group Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="" value="{{ old('name', $tagGroup->name ?? '') }}">
                                    @error('name')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('description')">
                                    <label>Tag Group Description</label>
                                    <textarea class="form-control" name="description" rows="3">{{ old('description', $tagGroup->description ?? '') }}</textarea>
                                    @error('description')
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @hasError('tags')">
                                    <label>Tags</label>
                                    <select class="form-control tags-box" name="tags[]" multiple="multiple" data-placeholder="Add Tags" style="width: 100%;">
                                        <?php $options = isset($tagGroup) ? $tagGroup->tags->pluck('name') : []; ?>
                                        @foreach (old('tags', $options) as $option)
                                            <option selected="selected">{{ $option }}</option>
                                        @endforeach
                                    </select>
                                    @error('tags')
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success pull-right">@if($isEdit) Update Tag Group @else Add Tag Group @endif</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        $('.tags-box').select2({
            tags: true
        });

        // show dropdown only if focus comes from tab (keyboard)
        var $select = $('.tags-box').next('.select2').find('.select2-selection');
        $select.on('mousedown', function() {
            $(this).data('mousedown', true);
        }).on('blur', function() {
            $(this).data('mousedown', false);
        }).on('focus', function() {
            if (! $(this).data('mousedown')) {
                $(this).closest('.select2').prev('select').select2('open');
            }
        });

        // don't order the tags, append them at the end
        $('.tags-box').on('select2:select', function (evt) {
            var element = evt.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger('change');
            $(this).next('.select2').find('.select2-search input').focus();
        });
    });
</script>
@endsection
