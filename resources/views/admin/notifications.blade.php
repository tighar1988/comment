@can('superadmin')

    @if (! request()->user()->is_enabled)
        <div class="row"><div class="top-alert alert alert-danger">Your shop is disabled. Please contact us to enable it again.</div></div>
    @endif

    @if (request()->user()->trialDaysLeft() >= 1 && request()->user()->trialDaysLeft() <= 3)
        {{-- <div class="row"><div class="top-alert alert alert-warning">Your trial will expire in {{ request()->user()->trialDaysLeft() }} days. After that your card will be charged monthly.</div></div> --}}
    @endif

    @if (shop_setting('subscription-plan:card-declined') == true && ! app('request')->is('admin/setup/account'))
        {{-- <div class="row"><div class="top-alert alert alert-danger">Your credit card was declined. We could not charge you for your subscription plan. Go to <a href="/admin/setup/account">Setup > Billing</a> to update your card.</div></div> --}}
    @endif

    @if (shop_setting('paypal:card-declined') == true && ! app('request')->is('admin/setup/account'))
        <div class="row"><div class="top-alert alert alert-danger">Your credit card was declined. PayPal comission fees could not be charged. Go to <a href="/admin/setup/account">Setup > Billing</a> to update your card.</div></div>
    @endif

    @if (! shop_setting('facebook.user_id') && ! app('request')->is('admin/facebook-setup'))
        <div class="row"><div class="top-alert alert alert-danger">In order to read Facebook comments, you need to connect your Facebook Account. Go to <a href="/admin/facebook-setup">Facebook Setup</a> to connect your account.</div></div>

    @elseif (shop_setting('facebook.user_id') && ($nrDays = fb_token_valid_days()) <= 10 && ! app('request')->is('admin/facebook-setup'))
        <div class="row"><div class="top-alert alert alert-danger">You need to reconnect your Facebook Account because the facebook access token will expire in {{ $nrDays}} {{ $nrDays > 1 ? 'day' : 'days' }}. Go to <a href="/admin/facebook-setup">Facebook Setup</a> to re-connect your account.</div></div>

    @elseif (! shop_setting('stripe.stripe_user_id') && ! (shop_setting('paypal.client_id') || shop_setting('paypal.identity.email')) && ! app('request')->is('admin/setup'))
        <div class="row"><div class="top-alert alert alert-danger">In order to accept payments, you need to connect your Stripe and PayPal accounts. Go to <a href="/admin/setup">Setup</a> to connect your accounts.</div></div>

    @elseif (shop_setting('shop.shipping-cost') == null && ! app('request')->is('admin/setup'))
        <div class="row"><div class="top-alert alert alert-danger">In order to accept payments, please choose a shipping cost for orders. Go to <a href="/admin/setup">Setup</a> to set your shipping cost.</div></div>

    @elseif (local_pickup_enabled() && ! shop_setting('shop.zip_code') && ! app('request')->is('admin/setup'))
        <div class="row"><div class="top-alert alert alert-danger">You have enabled local delivery. In order to calculate taxes for local pickup, you need to set your shop address. Go to <a href="/admin/setup">Setup</a> to set your shop address.</div></div>

    @endif

@endcan

@include('flash::message')

@if (session('status'))
    <div class="row">
        <div class="top-alert alert alert-info">
           {{ session('status') }}
        </div>
    </div>
@endif
