<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Models\TagGroup;
use App\Http\Requests\Admin\TagFormRequest;

class TagsController extends Controller
{
    public function index()
    {
        $tagGroups = TagGroup::with('tags')->get();
        return view('admin.store.tag-groups', compact('tagGroups'));
    }

    public function create()
    {
        return view('admin.store.tag-group-form');
    }

    public function store(TagFormRequest $request)
    {
        $tagGroup = TagGroup::create($request->only('name', 'description'));

        foreach ((array)$request->input('tags') as $tag) {
            Tag::create(['tag_group_id' => $tagGroup->id, 'name' => $tag]);
        }

        flash('Tag Group Saved!');
        return redirect('/admin/store/tags');
    }

    public function edit($tagGroupId)
    {
        $tagGroup = TagGroup::with('tags')->find($tagGroupId);
        return view('admin.store.tag-group-form', compact('tagGroup'));
    }

    public function update($tagGroupId, TagFormRequest $request)
    {
        $tagGroup = TagGroup::with('tags')->find($tagGroupId);
        $tagGroup->name = $request->input('name');
        $tagGroup->description = $request->input('description');
        $tagGroup->save();

        $existingTags = $tagGroup->tags->unique('name')->pluck('name');
        $updatedTags = collect($request->input('tags'));

        $deletedTags = $existingTags->diff($updatedTags);
        $newTags = $updatedTags->diff($existingTags);

        foreach ($newTags as $newTag) {
            Tag::create(['tag_group_id' => $tagGroupId, 'name' => $newTag]);
        }

        foreach ($deletedTags as $deletedTag) {
            Tag::tag($deletedTag)->delete();
        }

        flash('Tag Group Updated!');
        return redirect('/admin/store/tags');
    }

    public function destroy($tagGroupId)
    {
        TagGroup::destroy($tagGroupId);
        flash('Tag Group Deleted!');
        return redirect('/admin/store/tags');
    }
}
