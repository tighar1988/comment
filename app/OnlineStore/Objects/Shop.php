<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;

class Shop extends Drop
{
    /**
     * The shop's db name.
     */
    public function id()
    {
        return shop_id();
    }

    /**
     * Returns the shop's address.
     */
    public function address()
    {
        return new ShopAddress;
    }

    /**
     * Returns the number of collections in a shop.
     */
    public function collections_count()
    {
        return 'collections_count';
    }

    /**
     * Returns the shop's currency in three-letter format (ex: USD).
     */
    public function currency()
    {
        return 'USD';
    }

    /**
     * Returns the description of the shop.
     */
    public function description()
    {
        return shop_description();
    }

    /**
     * Returns the primary domain of the shop.
     */
    public function domain()
    {
        return shop_url('/');
    }

    /**
     * Returns the shop's email address.
     */
    public function email()
    {
        return shop_setting('shop.company_email');
    }

    /**
     * Returns an array of accepted credit cards for the shop.
     * Use the payment_type_img_url filter to link to the SVG image file of the credit card.
     *
     * @todo Add support for enabled_payment_types and the 'payment_type_img_url' filter.
     */
    public function enabled_payment_types()
    {
        // todo: implement
        return '';
    }

    /**
     * Returns the shop's metafields. Metafields can only be set using the Shopify API.
     */
    public function metafields()
    {
        return new MetafieldsNamespace('shop');
    }

    /**
     * Returns a string that is used by Shopify to format money without showing the currency.
     */
    public function money_format()
    {
        return shop_setting('shopify.money_format', '{{amount}}');
    }

    /**
     * Returns a string that is used by Shopify to format money while also displaying the currency.
     */
    public function money_with_currency_format()
    {
        return '${{amount}}';
    }

    /**
     * Returns the shop's name.
     */
    public function name()
    {
        return shop_name();
    }

    /**
     * Returns an array of your store's refund policy, privacy policy, and terms of service.
     * You can set these policies in your store's Checkout settings.
     *
     * @todo  Add support for policies.
     */
    public function policies()
    {
        // todo: implement
        return '';
    }


    /**
     * Returns your store's refund policy, which you can set in your store's Checkout settings.
     *
     * @todo  Add support for refund_policy.
     */
    public function refund_policy()
    {
        // todo: implement
        return '';
    }

    /**
     * Returns your store's privacy policy, which you can set in your store's Checkout settings.
     *
     * @todo  Add support for privacy_policy.
     */
    public function privacy_policy()
    {
        // todo: implement
        return '';
    }

    /**
     * Returns your store's terms of service (TOS), which you can set in your store's Checkout settings.
     *
     * @todo  Add support for terms_of_service.
     */
    public function terms_of_service()
    {
        // todo: implement
        return '';
    }

    /**
     * Returns the shop's password page message.
     *
     * @todo Add support for password_message.
     */
    public function password_message()
    {
        return 'password_message';
    }

    /**
     * Returns the .commentsold.com URL of a shop.
     */
    public function permanent_domain()
    {
        return shop_url('/');
    }

    /**
     * Returns the number of products in a shop.
     */
    public function products_count()
    {
        return 'products_count';
    }

    /**
     * Returns an array of all unique product types in a shop.
     *
     * @todo  Add support for types.
     */
    public function types()
    {
        // todo: implement
        return '';
    }

    /**
     * Returns the full URL of a shop.
     */
    public function url()
    {
        return shop_url('/');
    }

    /**
     * Returns the full URL of a shop prepended by the https protocol.
     */
    public function secure_url()
    {
        return shop_url('/');
    }

    /**
     * Returns an array of all unique vendors in a shop.
     *
     * @todo  Add support for vendors.
     */
    public function vendors()
    {
        // todo: implement
        return '';
    }

    /**
     * Returns the locale that the shop is currently displayed in (ex: en, fr, pt-BR).
     * See the documentation on theme translations for more details on this feature.
     *
     * @todo  Add support for multiple locales.
     */
    public function locale()
    {
        return 'en';
    }

    public function customer_accounts_enabled()
    {
        return true;
    }

    public function customer_accounts_optional()
    {
        return false;
    }

    public function embed_code()
    {
        return shop_setting('embed-code');
    }

    public function share_group()
    {
        return shop_setting('facebook.share-group-id');
    }

    public function instagram_enabled()
    {
        if (! empty(shop_setting('instagram.token'))) {
            return true;
        }

        return false;
    }

    public function free_24hr_shipping_enabled()
    {
        return free_shipping_24hr_enabled();
    }

    public function facebook_app_id()
    {
        return fb_app_id();
    }

    public function facebook_api_version()
    {
        return fb_api_version();
    }

    public function facebook_share_group()
    {
        return shop_setting('facebook.share-group-id');
    }

    public function stripe_publishable_key()
    {
        return shop_setting('stripe.stripe_publishable_key');
    }

    public function stripe_enabled()
    {
        if (! empty(shop_setting('stripe.access_token'))) {
            return true;
        }

        return false;
    }

    public function paypal_enabled()
    {
        if(shop_setting('paypal.client_id') || shop_setting('paypal.identity.email')) {
            return true;
        }

        return false;
    }

    public function facebook_messenger_page_id()
    {
        return fb_page_id();
    }

    public function local_pickup_enabled()
    {
        return local_pickup_enabled();
    }

    public function expire()
    {
        return expire();
    }
}
