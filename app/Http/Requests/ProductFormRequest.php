<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'style'        => 'required|unique:shop.products,style',
            'product_name' => 'required|max:255',
            'brand'        => '',
            'brand_style'  => '',
            'description'  => '',
            'charge_taxes' => 'nullable|in:1,0',
            'publish_product' => 'nullable|in:1,0',
            'received_product' => 'nullable|in:1,0',
        ];

        // for update skip unique sku on current product
        if ($this->method() == 'PATCH') {
            $rules['style'] = 'required|unique:shop.products,style,' . $this->route('id');
        } else {
            $rules['color']        = '';
            $rules['size']         = '';
            $rules['cost']         = 'required|numeric|min:1';
            $rules['retail_price'] = 'required|numeric|min:1';

            if (shop_id() == 'shopentourageclothing') {
                $rules['cost']         = 'required|numeric|min:0';
                $rules['retail_price'] = 'required|numeric|min:0';
            }

            if ($this->product_type == 'giftcard') {
                $rules['cost'] = 'required|numeric|min:0';
            }
        }

        return $rules;
    }
}
