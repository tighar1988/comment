<?php

namespace App\Facebook;

use App\Facebook\FacebookMessenger;
use App\Exceptions\ShopifyWebhookLockedException;
use App\Mails\ShopMailer;
use App\Models\Post;
use App\Models\Product;
use App\Models\ShopUser;
use App\Models\Comment;
use App\Models\Cart;
use App\Models\Waitlist;
use App\Facebook\ShopperComment;

class CommentHarvester
{
    protected $facebook;

    protected $mailer;

    protected $settings;

    public function getMailer()
    {
        if (is_null($this->mailer)) {
            $this->mailer = new ShopMailer;
        }

        return $this->mailer;
    }

    public function getFacebook()
    {
        if (is_null($this->facebook)) {
            $this->facebook = new Facebook;
        }

        return $this->facebook;
    }

    public function harvest(Post $post, Product $product, ShopperComment $comment)
    {
        // let's check to be safe the comment was not already harvested
        if (Comment::exists($comment->id)) {
            return false;
        }

        // if (Comment::alreadyParsed($comment)) {
        //     return false;
        // }

        $commentId = Comment::store($post->id, $comment)->id;

        if ($comment->isReplyComment() && ($comment->fromId == $this->setting('facebook.user_id') || $comment->fromId == $this->setting('facebook.page.page_id'))) {
            return false;
        }

        if (! $comment->commentedSold()) {
            // todo: send reply message that their comment must contain the word 'sold' (if it has a variant but no 'sold' word)
            return false;
        }

        $customer = $this->getCommenter($comment);
        // todo: if we already have the email on file, reply they don't need to comment their email again

        $variant = $comment->findVariantMatch($product);
        if (! $variant) {
            $this->notifyInvalidComment($customer, $post, $comment);
            return false;
        }

        $waitlist = $variant->quantity <= 0;

        Comment::markIsOrder($commentId);

        if ($waitlist) {
            Waitlist::addItem($customer, $commentId, $variant);
        } else {
            try {
                Cart::addItem($customer, $commentId, $variant);
            } catch (ShopifyWebhookLockedException $e) {
                Comment::find($commentId)->delete();
                ShopMailer::notifyAdmin($e);
                log_error("[harvest][shopify] $e");
                return false;
            }
        }

        if ($waitlist) {
            $this->notifyItemWaitlisted($customer, $comment, $post);

        } elseif (! $customer->email) {
            $this->notifyOrderWithNoEmail($customer, $comment, $post);

        } else {
           $this->notifyGotOrder($customer, $comment, $post);
        }

        if ($customer->email) {
            if ($waitlist) {
                $this->getMailer()->sendWaitlistEmail($customer, $product, $variant);

            } else {
                $this->getMailer()->sendItemAddedToCartEmail($customer, $product, $variant);
            }
        }

        return true;
    }

    public function getCommenter($comment)
    {
        $customer = ShopUser::getByFacebookId($comment->fromId);

        // first time customer
        if (! $customer) {
            $customer = new ShopUser;
            $customer->facebook_id = $comment->fromId;
            $customer->name = $comment->fromName;
            $customer->email = $comment->extractEmail();
            $customer->save();
        }

        // if the customer specifies an email in a later comment
        if (! $customer->email) {
            $customer->email = $comment->extractEmail();
            $customer->save();
        }

        return $customer;
    }

    public function notifyInvalidComment($customer, $post, $comment)
    {
        log_info("[harvest] Invalid product variant comment : " . $comment->message . " CID #{$customer->id}");
        FacebookMessenger::notifyForInvalidComment($customer, $post);
        if ($this->setting('setup.notify-invalid-comment', true)) {
            $this->getFacebook()->replyForInvalidComment($comment, $post);
        }
        if ($post->createdBy($this->setting('facebook.page.page_id'))) {
            $this->getFacebook()->replyForInvalidComment($comment, $post, true);
        }
    }

    public function notifyItemWaitlisted($customer, $comment, $post)
    {
        log_info("[harvest] Results: Sold Out : " . $comment->message . " CID #{$customer->id}");
        FacebookMessenger::notifyItemWaitlisted($customer);
        if ($this->setting('setup.notify-item-waitlisted', true)) {
            $this->getFacebook()->replyItemWaitlisted($comment);
        }
        if ($post->createdBy($this->setting('facebook.page.page_id'))) {
            $this->getFacebook()->replyItemWaitlisted($comment, true);
        }
    }

    public function notifyOrderWithNoEmail($customer, $comment, $post)
    {
        log_info("[harvest] Results: Good order but no email : " . $comment->message . " CID #{$customer->id}");
        FacebookMessenger::notifyNoEmail($customer);
        if ($this->setting('setup.notify-order-without-email', true)) {
            $this->getFacebook()->replyNoEmail($comment);
        }
        if ($post->createdBy($this->setting('facebook.page.page_id'))) {
            $this->getFacebook()->replyNoEmail($comment, true);
        }
    }

    public function notifyGotOrder($customer, $comment, $post)
    {
        log_info("[harvest] Results: Got an order, woo! : " . $comment->message . " CID #{$customer->id}");
        FacebookMessenger::notifyGotOrder($customer);
        if ($this->setting('setup.notify-got-order', true)) {
            $this->getFacebook()->replyGotOrder($comment);
        }
        if ($post->createdBy($this->setting('facebook.page.page_id'))) {
            $this->getFacebook()->replyGotOrder($comment, true);
        }
    }

    public function setting($key, $default = null)
    {
        if (! isset($this->settings[$key])) {
            $this->settings[$key] = shop_setting($key, $default);
        }

        return $this->settings[$key];
    }
}
