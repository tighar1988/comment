<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceMovement extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'shop';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'balance_movement';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'amount',
        'memo',
    ];

    public static function log($customerId, $amount, $memo)
    {
        static::create([
            'customer_id' => $customerId,
            'amount'      => $amount,
            'memo'        => $memo,
        ]);
    }

    public static function logAdminUpdate($customerId, $amount, $note)
    {
        static::create([
            'customer_id' => $customerId,
            'amount'      => $amount,
            'memo'        => $note,
        ]);
    }
}
