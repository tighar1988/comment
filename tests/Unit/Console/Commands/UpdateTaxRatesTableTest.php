<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Console\Commands\UpdateTaxRatesTable;
use Mockery;

class UpdateTaxRatesTableTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(UpdateTaxRatesTable::class, new UpdateTaxRatesTable);
    }
}
