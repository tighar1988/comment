@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Setup</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/setup"><i class="fa fa-th-large"></i>Setup</a></li>
        <li class="active">@yield('section')</li>
    </ol>
</section>
<section class="content settings-boxes">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-pills">
                    <li class="{{ activeLink('admin/setup') }}"><a href="/admin/setup">Shop</a></li>
                    <li class="{{ activeLink(['admin/setup/shipping', 'admin/setup/shipping/prepay-labels', 'admin/setup/locations']) }}"><a href="/admin/setup/shipping">Shipping</a></li>
                    <li class="{{ activeLink('admin/setup/account') }}"><a href="/admin/setup/account">Account</a></li>
                    <li class="{{ activeLink('admin/setup/billing') }}"><a href="/admin/setup/billing">Payment Gateways</a></li>
                    <li class="{{ activeLink(['admin/setup/invoices', 'admin/setup/invoices/prepaid-labels']) }}"><a href="/admin/setup/invoices">My Invoices</a></li>
                    <li class="{{ activeLink('admin/setup/instagram') }}"><a href="/admin/setup/instagram">Instagram</a></li>
                    <li class="{{ activeLink('admin/setup/templates') }}"><a href="/admin/setup/templates">Templates</a></li>
                    <li class="{{ activeLink('admin/setup/embed') }}"><a href="/admin/setup/embed">Embed</a></li>
                    @if (shop_id() != 'fillyflair' && shop_id() != 'shopzigzagstripe')
                        <li class="{{ activeLink('admin/setup/labels') }}"><a href="/admin/setup/labels">Labels</a></li>
                    @endif
                    <li class="{{ activeLink('admin/setup/repost') }}"><a href="/admin/setup/repost">Auto Scheduler</a></li>
                    <li class="{{ activeLink('admin/setup/shopify') }}"><a href="/admin/setup/shopify">Shopify Sync</a></li>
                </ul>
                @yield('sub-nav')
            </div>
        </div>
    </div>

    @yield('setup')

</section>
@endsection
