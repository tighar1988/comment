<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TimezoneFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'timezone' => 'required|string|in:' . $this->validTimezones(),
        ];
    }

    public function validTimezones()
    {
        $timezones = [
            'Etc/UTC',
            'Europe/London',
            'Africa/Casablanca',
            'Europe/Lisbon',
            'Africa/Algiers',
            'Europe/Berlin',
            'Europe/Paris',
            'Europe/Prague',
            'Europe/Athens',
            'Africa/Cairo',
            'EET',
            'Africa/Harare',
            'Asia/Jerusalem',
            'Asia/Baghdad',
            'Europe/Minsk',
            'Europe/Moscow',
            'Asia/Tehran',
            'Asia/Tbilisi',
            'Asia/Yerevan',
            'Asia/Kabul',
            'Asia/Karachi',
            'Asia/Yekaterinburg',
            'Asia/Tashkent',
            'Asia/Calcutta',
            'Asia/Katmandu',
            'Asia/Almaty',
            'Asia/Omsk',
            'Asia/Bangkok',
            'Asia/Krasnoyarsk',
            'Asia/Shanghai',
            'Australia/Perth',
            'Asia/Irkutsk',
            'Asia/Tokyo',
            'Australia/Adelaide',
            'Australia/Darwin',
            'Australia/Brisbane',
            'Pacific/Guam',
            'Asia/Vladivostok',
            'Australia/Sydney',
            'Asia/Yakutsk',
            'Australia/Hobart',
            'Pacific/Kwajalein',
            'Pacific/Fiji',
            'Asia/Kamchatka',
            'Asia/Magadan',
            'Pacific/Auckland',
            'Pacific/Apia',
            'Atlantic/Azores',
            'Atlantic/South_Georgia',
            'America/Buenos_Aires',
            'America/Fortaleza',
            'America/Recife',
            'America/Sao_Paulo',
            'America/St_Johns',
            'America/Halifax',
            'America/La_Paz',
            'America/Caracas',
            'America/Bogota',
            'America/New_York',
            'America/Indiana/Indianapolis',
            'America/Chicago',
            'America/Indiana/Knox',
            'America/Mexico_City',
            'America/Managua',
            'America/Regina',
            'America/Phoenix',
            'America/Denver',
            'America/Los_Angeles',
            'America/Tijuana',
            'America/Nome',
            'Pacific/Honolulu',
            'Pacific/Midway',
        ];

        return implode(',', $timezones);
    }
}
