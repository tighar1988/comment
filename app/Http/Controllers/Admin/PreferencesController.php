<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Liquid;
use App\Http\Requests\Admin\PreferencesFormRequest;

class PreferencesController extends Controller
{
    public function index()
    {
        return view('admin.store.preferences');
    }

    public function save(PreferencesFormRequest $request)
    {
        shop_setting_set('online-store.page_title', request('page_title'));
        shop_setting_set('online-store.page_description', request('page_description'));

        flash('Preferences Updated!');
        return back();
    }
}
