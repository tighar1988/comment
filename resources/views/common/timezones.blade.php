<?php
    $timezones = [
        ['description' => 'UTC-05:00 Eastern Time (US & Canada)', 'name' => 'America/New_York'],
        ['description' => 'UTC-06:00 Central Time (US & Canada)', 'name' => 'America/Chicago'],
        ['description' => 'UTC-07:00 Arizona', 'name' => 'America/Phoenix'],
        ['description' => 'UTC-07:00 Mountain Time (US & Canada)', 'name' => 'America/Denver'],
        ['description' => 'UTC-08:00 Pacific Time (US & Canada); Los Angeles', 'name' => 'America/Los_Angeles'],
        ['description' => 'UTC-09:00 Alaska', 'name' => 'America/Nome'],
        ['description' => 'UTC-10:00 Hawaii', 'name' => 'Pacific/Honolulu'],
    ];
?>
@foreach ($timezones as $timezone)
    <option @if (isset($selected) && $selected == $timezone['name']) selected="selected" @endif value="{{ $timezone['name'] }}">{{ $timezone['description'] }}</option>
@endforeach
