<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Post;
use App\Jobs\HarvestFacebookPagePost;
use App\Http\Requests\ProductFormRequest;
use App\Http\Requests\FacebookPostFormRequest;
use Facebook\Exceptions\FacebookResponseException;
use App\Mails\ShopMailer;
use Facebook;
use Datatables;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.products.product-grid');
    }

    public function datatable()
    {
        $model = Product::query()->select(\DB::raw("products.id, products.product_name, products.style, products.brand, products.brand_style, products.shopify_product_id,
            (SELECT images.filename FROM images WHERE images.is_main = true AND products.id = images.product_id LIMIT 1) as thumb,
            (SELECT MAX(created_time) FROM posts WHERE products.id = posts.prod_id AND posts.scheduled_time IS NULL LIMIT 1) as latest_post,
            COALESCE((SELECT SUM(quantity) FROM inventory WHERE products.id = inventory.product_id GROUP BY inventory.product_id), 0) as inventory_quantity
        "));

        return Datatables::eloquent($model)
                ->filterColumn('style', function($query, $keyword)  {
                    $query->where('style', 'like', "%{$keyword}%");
                    $query->orWhere('brand_style', 'like', "%{$keyword}%");
                    $query->orWhere('brand', 'like', "%{$keyword}%");
                })
                ->addColumn('image', '{{ (empty($thumb) ? "" : product_image($thumb)) }}')
                ->editColumn('thumb', '{{ product_thumb($thumb) }}')
                ->editColumn('product_name', '{{ $product_name }}')
                ->editColumn('latest_post', '@if ($latest_post){{ apply_timezone($latest_post) }}@endif')
                ->make(true);
    }

    public function copyText($productId)
    {
        $product = Product::find($productId);
        if (is_null($product)) {
            return ['text' => ''];
        }

        $sizes = [];
        $colors = [];
        $inventory = $product->inventory()->orderBy('id', 'asc')->get();
        foreach ($inventory as $variant) {
            $colors[] = $variant->color;
            $sizes[] = $variant->size;
        }

        $message = view('admin.products.instagram-post-template', [
            'product' => $product,
            'sizes'   => implode(', ', unique_options($sizes)),
            'colors'  => implode(', ', unique_options($colors)),
            'price'   => $inventory->average('price'),
        ])->render();

        return ['text' => strip_tags($message)];
    }

    public function mostInventory()
    {
        $inventory = \DB::connection('shop')->select("SELECT sum(inventory.quantity) as sum, inventory.product_id, products.style FROM inventory INNER JOIN products ON products.id = inventory.product_id GROUP BY inventory.product_id,products.style ORDER BY sum DESC");

        return view('admin.products.most-inventory', compact('inventory'));
    }

    public function create()
    {
        if (shopify_enabled() && shop_id() != 'glamourfarms' && shop_id() != 'joeyeric') {
            return redirect('/admin/products');
        }

        if (shop_id() == 'kaleyjase') {
            $previousColors = [];
            $previousSizes = [];
        } else {
            $previousColors = Inventory::pluck('color')->unique()->sort();
            $previousSizes = Inventory::pluck('size')->unique()->sort();
        }

        $sku = Product::suggestedSKU();
        return view('admin.products.product-form', compact('sku', 'previousColors', 'previousSizes'));
    }

    public function giftcardCreate()
    {
        return view('admin.products.product-form');
    }

    public function store(ProductFormRequest $request)
    {
        $product = new Product($request->input());
        if ($request->input('publish_product')) {
            $product->published_at = time();
        }
        $product->save();

        $sizes = $request->input('size') ?? [null];
        $colors = $request->input('color') ?? [null];

        // create variants
        foreach ($colors as $color) {
            foreach ($sizes as $size) {
                Inventory::newItem([
                    'product_id' => $product->id,
                    'color'      => trim($color),
                    'size'       => trim($size),
                    'quantity'   => 0,
                    'cost'       => $request->input('cost'),
                    'price'      => $request->input('retail_price'),
                ]);
            }
        }

        if ($request->has('tags')) {
            $product->tags()->sync($request->input('tags'));
        }
        if ($request->has('collections')) {
            $product->collections()->sync($request->input('collections'));
        }

        flash('Product saved! You can upload images for this product now or later!');
        return redirect("/admin/products/{$product->id}/images");
    }

    public function edit($id)
    {
        if (shopify_enabled() && shop_id() != 'glamourfarms' && shop_id() != 'joeyeric') {
            return redirect('/admin/products');
        }

        $product = Product::find($id);
        if (is_null($product)) {
            flash('The product you are trying to edit has been deleted.', 'danger');
            return redirect('/admin/products');
        }

        return view('admin.products.product-form', compact('product'));
    }

    public function update($id, ProductFormRequest $request)
    {
        $product = Product::find($id);
        $product->fill($request->input());
        if ($request->input('publish_product')) {
            $product->published_at = time();
        }
        $product->save();

        if ($request->has('tags')) {
            $product->tags()->sync($request->input('tags'));
        }
        if ($request->has('collections')) {
            $product->collections()->sync($request->input('collections'));
        }

        flash('Product Updated!');
        return redirect('/admin/products');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if (shopify_enabled() && shop_id() != 'glamourfarms' && ! empty($product->shopify_product_id)) {
            return redirect('/admin/products');
        }

        Product::destroy($id);
        flash('Product Deleted!');
        return redirect('/admin/products');
    }

    public function showPostPreview($productId)
    {
        // todo: catch error if facebook is not connected, and display a message to the user
        $enabledGroups = fb_enabled_group_ids();
        $groups = Facebook::getMerchantGroups();

        $product = Product::with('images')->find($productId);
        $image = $product->image->filename ?? null;

        $sizes = [];
        $colors = [];
        $inventory = $product->inventory()->orderBy('id', 'asc')->get();
        foreach ($inventory as $variant) {
            $colors[] = $variant->color;
            $sizes[] = $variant->size;
        }

        $message = view('admin.products.post-template', [
            'product' => $product,
            'sizes'   => implode(', ', unique_options($sizes)),
            'colors'  => implode(', ', unique_options($colors)),
            'price'   => $inventory->average('price'),
        ])->render();

        $pageName = empty(shop_setting('facebook.page.page_id')) ? null : shop_setting('facebook.page.page_name');

        return view('admin.products.post-preview', compact('product', 'image', 'message', 'groups', 'enabledGroups', 'pageName'));
    }

    public function postToFacebook(FacebookPostFormRequest $request, $productId)
    {
        $product = Product::with('images')->find($productId);
        if (is_null($product->image)) {
            return back()->withInput()
                ->withErrors(['product_image' => 'An image is required to post.']);
        }

        $postAsPage = false;
        if (request('post_as') == '2') {
            $postAsPage = true;
        }

        $image = $product->image->filename;
        if (starts_with($image, 'https://cdn.shopify.com')) {
            $sizes = getimagesize($image);
            if (isset($sizes[0]) && $sizes[0] > 1000) {
                $poz = strrpos($image, '.');
                $image = substr_replace($image, '_1000x', $poz, 0);
            }
        }
        $postImage = product_image($image);

        $postId = request('post_edit');
        if (isset($postId) && is_numeric($postId)) {
            // it's an edited post
            $post = Post::find($postId);

            // if the scheduled post was published, don't allow it to be edited
            if (is_null($post->scheduled_time)) {
                return back()->withInput()
                    ->withErrors(['schedule_date' => "The post was already published! It can't be edited anymore."]);
            }

        } else {
            // it's a new post
            $post = new Post;
        }

        $group = request('group');
        $message = request('message');
        $postComment = request('post_comment');
        $supplement = ['message' => $message, 'url' => $postImage, 'group' => $group, 'post_comment' => $postComment, 'post_as_page' => $postAsPage];

        if (Post::postImmediately(request('timing'))) {

            try {
                $node = Facebook::post($group, $message, $postImage, $postAsPage);
            } catch (FacebookResponseException $e) {
                ShopMailer::notifyAdmin($e);
                log_error("[facebook-post] $e");

                $error = $e->getResponseData();
                if (isset($error['error']['error_user_msg'])) {
                    $error = $error['error']['error_user_msg'];
                } else {
                    $error = 'There was an error posting to Facebook. Please try again!';
                }

                flash($error, 'danger');
                return back()->withInput();
            }

            if (isset($node['post_id']) && ! empty($node['post_id'])) {
                $postData = explode('_', $node['post_id']);

                $post->fb_id = $node['post_id'];
                $post->fb_photo_id = $node['id'];
                $post->fb_author_id = trim($postData[0]);
                $post->fb_post_id = trim($postData[1]);
                $post->prod_id = $productId;
                $post->created_time = time();
                $post->scan_time = time();
                $post->scheduled_time = null;
                $post->image = $image;
                $post->supplement = json_encode($supplement);
                $post->save();

                // add the post comment
                if (! empty($postComment)) {
                    Facebook::commentOnPost($node['post_id'], $postComment);
                }

                flash('Your post has been successfully submitted. You can track its progress through <a href="/admin/posts">Posts Manager</a>');
            }
        }

        if (Post::scheduleLater(request('timing'))) {

            $req = $request->input();
            if (strlen($req['hour']) == 1) $req['hour'] = '0'.$req['hour'];
            if (strlen($req['min']) == 1) $req['min'] = '0'.$req['min'];
            $date = $req['schedule_date'].' '.$req['hour'].':'.$req['min'].' '.$req['ampm'];

            // create the post time in the user's timezone
            $postTime = \Carbon\Carbon::createFromFormat('n/j/Y H:i a', $date, shop_setting('shop.timezone'));

            // convert it to our app timezone
            $postTime->timezone(config('app.timezone'));
            $stampedPostTime = strtotime($postTime->toDateTimeString());

            if (time() > $stampedPostTime) {
                $now = \Carbon\Carbon::now(shop_setting('shop.timezone'))->toDateTimeString();
                return back()->withInput()
                    ->withErrors(['schedule_date' => 'The current time ('.$now.') is greater than the scheduled time.']);
            }

            $post->prod_id = $productId;
            $post->created_time = time();
            $post->scan_time = time();
            $post->scheduled_time = $stampedPostTime;
            $post->image = $image;
            $post->supplement = json_encode($supplement);
            $post->save();

            flash('Your post has been successfully scheduled. You can track its progress through <a href="/admin/posts">Posts Manager</a>');
        }

        return redirect('/admin/posts');
    }

    /**
     * Return the latest 10 posts for the page.
     */
    public function facebookPagesPosts()
    {
        $posts = Facebook::lastPagesPosts(shop_setting('facebook.page.page_id'));

        return Datatables::collection(collect($posts))->make(true);
    }

    /**
     * Return the latest 10 posts for each group.
     */
    public function facebookPosts()
    {
        $posts = [];
        foreach (fb_enabled_group_ids() as $groupId) {
            $posts = array_merge($posts, Facebook::lastPosts($groupId));
        }

        return Datatables::collection(collect($posts))->make(true);
    }

    public function linkToFacebookPost($productId)
    {
        $product = Product::with('images')->find($productId);
        if (is_null($product->image)) {
            flash('A product image is required to link a post.', 'danger');
            return back();
        }

        $image = $product->image->filename;
        $postImage = product_image($image);

        $facebookPost = Facebook::getPost(request('post_id'));
        $exists = Post::where('fb_id', '=', $facebookPost['id'])->first();
        if ($exists) {
            flash("You can't link multiple products to a post.", 'danger');
            return back();
        }

        $post = new Post;

        $group = request('group_id');
        $message = $facebookPost['message'];
        $supplement = ['message' => $message, 'url' => $postImage, 'group' => $group, 'post_comment' => null];

        $postData = explode('_', $facebookPost['id']);

        $post->fb_id = $facebookPost['id'];
        $post->fb_photo_id = null;
        $post->fb_author_id = trim($postData[0]);
        $post->fb_post_id = trim($postData[1]);
        $post->prod_id = $productId;
        $post->created_time = time();
        $post->scan_time = time();
        $post->scheduled_time = null;
        $post->image = $image;
        $post->supplement = json_encode($supplement);
        $post->save();

        flash('Your post has been successfully linked. You can track its progress through <a href="/admin/posts">Posts Manager</a>');

        return back();
    }

    public function linkToFacebookPagePost($productId)
    {
        $product = Product::with('images')->find($productId);

        $image = $product->image->filename ?? '';
        $postImage = product_image($image);

        $facebookPost = Facebook::getPost(request('post_id'));
        $exists = Post::where('fb_id', '=', $facebookPost['id'])->first();
        if ($exists) {
            flash("You can't link multiple products to a post.", 'danger');
            return back();
        }

        $post = new Post;

        $supplement = ['message' => $facebookPost['message'], 'url' => $postImage, 'page' => request('group_id')];
        $postData = explode('_', $facebookPost['id']);

        $post->fb_id = $facebookPost['id'];
        $post->fb_photo_id = null;
        $post->fb_author_id = trim($postData[0]);
        $post->fb_post_id = trim($postData[1]);
        $post->prod_id = $productId;
        $post->created_time = time();
        $post->scan_time = time();
        $post->scheduled_time = null;
        $post->image = $image;
        $post->supplement = json_encode($supplement);
        $post->save();

        // harvest the existing comments before the post was linked
        dispatch(new HarvestFacebookPagePost(shop_id(), $post->id));

        flash('Your Page post has been successfully linked. You can track its progress through <a href="/admin/posts">Posts Manager</a>');

        return back();
    }

    /**
     * Search products for DataTable.
     */
    public function listProducts()
    {
        return Datatables::eloquent(Inventory::searchProducts())
            ->filterColumn('style', function($query, $keyword)  {
                $query->where('products.style', 'like', "%{$keyword}%");
                $query->orWhere('products.product_name', 'like', "%{$keyword}%");
            })
            ->make(true);
    }
}
