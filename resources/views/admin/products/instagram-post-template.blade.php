<?php
    if ($colors != '') {
        $colors = ', Color (' . $colors . ')';
    }
?>

{!! strip_tags($product->product_name) !!}. {!! strip_tags($product->description) !!}

@if ($sizes)
Available in {{ $sizes }} for only ${{ amount($price) }}!
@if (shop_id() == 'glamourfarms')

TO ORDER: Comment "Sold, Size{{ $colors }}". One order per comment.
@else
To order, comment Sold, Size{{ $colors }}.
@endif
@else
Available for only ${{ amount($price) }}!
@if (shop_id() == 'glamourfarms')

TO ORDER: Comment "Sold{{ $colors }}". One order per comment.
@else
To order, comment Sold{{ $colors }}.
@endif
@endif

Go to {{ shop_url('/account') }} to view/pay your invoice. Invoice must be paid within {{ expire() }} hours or your item will go to the next person in line.

Style #{{ $product->style }}
