<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Traits\ReconnectShopDB;
use App\Models\Post;
use App\Models\Product;
use App\Facebook\ShopperComment;
use App\Facebook\CommentHarvester;
use App\Facebook\Facebook;

class HarvestFacebookPagePost implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, ReconnectShopDB;

    protected $shop;

    protected $postId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop, $postId)
    {
        $this->shop = $shop;
        $this->postId = $postId;
    }

    /**
     * Harvest the Facebook Page post comments.
     *
     * @return void
     */
    public function handle()
    {
        $this->reconnectDB($this->shop);

        $post = Post::find($this->postId);
        if (is_null($post)) {
            log_notice("[harvest-post] Could not find post #{$postId}. Exiting..");
            return;
        }

        $facebook = new Facebook;
        $harvester = new CommentHarvester;

        $comments = $facebook->allComments($post->fb_id);
        if (empty($comments)) {
            log_info("[harvest-post] No comments to harvest from post {$post->fb_id}");
            return;
        }

        log_info("[harvest-post] New comments: ".count($comments)." on post {$post->fb_id}");
        $product = Product::find($post->prod_id);

        foreach ($comments as $facebookComment) {
            $comment = ShopperComment::create($facebookComment);
            $harvester->harvest($post, $product, $comment);
        }
    }
}
