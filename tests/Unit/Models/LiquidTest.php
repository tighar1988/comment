<?php

namespace Tests\Unit\tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Liquid;

class LiquidTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(Liquid::class, new Liquid);
    }
}
