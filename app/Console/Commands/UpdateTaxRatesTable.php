<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TaxRate;
use File;
use Exception;

class UpdateTaxRatesTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commentsold:update-tax-rates-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the tax rates table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info("Update tax rates from /storage/taxrates/tax-rates.php");
            $this->checkFileExists($file = storage_path('taxrates/tax-rates.php'));

            $taxrates = require $file;

            $count = count($taxrates);
            $bar = $this->output->createProgressBar($count);

            for ($i = 0; $i < $count; $i++) {
                if ($i%100 == 0) {
                    $bar->advance(100);
                }
                TaxRate::master()->updateTaxRate($taxrates[$i]);
            }

            $bar->finish();
            $this->line('');
            $this->info("Tax rates updated.");

        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function checkFileExists($file)
    {
        if (! File::exists($file)) {
            throw new Exception("The file does not exist " . $file);
        }
    }
}
