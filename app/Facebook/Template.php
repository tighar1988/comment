<?php

namespace App\Facebook;

class Template
{
    public function forInvalidComment($product)
    {
        $template = $this->template('invalid-comment');
        if (! str_contains($template, '{variants}')) {
            return $template;
        }

        // replace {variants} with the product's variants
        $variants = $product->variants ?? [];
        $colors = [];
        $sizes = [];
        foreach ($variants as $variant) {
            $colors[] = $variant->color;
            $sizes[] = $variant->size;
        }

        return str_replace('{variants}', $this->variantsText($colors, $sizes), $template);
    }

    public function template($key)
    {
        $key = $this->parse($key);

        return shop_setting($key, $this->defaultTemplate($key));
    }

    public function setTemplate($key, $template)
    {
        shop_setting_set($this->parse($key), $template);
    }

    public function defaultTemplate($key)
    {
        if ($key == 'template.invalid-comment') {
            return 'Our system can not determine the size or color option for your comment, please re-comment with correct size and color.';

        } elseif ($key == 'template.item-waitlisted') {
            return 'Thank you for your order, this item is currently sold out in the color/size you requested. We have placed you on our waitlist and if one goes unpaid or we are able to restock we will send you an invoice and an email notification.';

        } elseif ($key == 'template.waitlist-activated') {
            return 'Congratulations, the item you requested is back in stock! Click the link in the post to view and pay your invoice. Remember, you have '.expire().' hours to pay before the item expires.';

        } elseif ($key == 'template.order-without-email') {
            return 'Welcome new shopper! Please go to ' . shop_url('/account') . ' to login, view your cart and update your email with us. Thanks!';

        } elseif ($key == 'template.new-order') {
            return 'Thank you, we got your order! Please click the link provided in the post to view and pay your order. Remember, you have '.expire().' hours to pay before the item expires.';

        } elseif (in_array($key, ['template.awarded-account-credit', 'template.instagram-welcome-email'])) {
            return file_get_contents(resource_path("views/emails/liquid/{$key}.liquid"));
        }

        return null;
    }

    protected function parse($key)
    {
        $key = str_replace('_', '-', $key);
        return "template.{$key}";
    }

    public function variantsText($colors, $sizes)
    {
        $colors = implode(', ', unique_options($colors));
        $sizes = implode(', ', unique_options($sizes));

        $options = [];

        if (! empty($colors)) {
            $options[] = 'Colors: ' . $colors;
        }
        if (! empty($sizes)) {
            $options[] = 'Sizes: ' . $sizes;
        }

        return implode(' / ', $options);
    }

    public static function containsLink($message)
    {
        $regexUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        if (preg_match($regexUrl, $message)) {
            return true;
        }

        return false;
    }
}
