<?php

namespace App\OnlineStore\Objects;

use App\OnlineStore\Objects\Drop;
use App\OnlineStore\CurrentView;

class SchemaSettings extends Drop
{
    protected $settings;

    protected $schema;

    protected $sectionName;

    public function __construct($settings, $schema, $sectionName)
    {
        $this->settings = $settings;
        $this->schema = $schema;
        $this->sectionName = $sectionName;
    }

    public function handle_key($key)
    {
        if (isset(CurrentView::$settingsData['current']['sections'][$this->sectionName]['settings'][$key])) {
            return CurrentView::$settingsData['current']['sections'][$this->sectionName]['settings'][$key];
        }

        foreach ((array)$this->settings as $setting) {
            if (isset($setting->id) && $setting->id == $key) {
                return new SchemaSetting($setting, $this->schema);
            }
        }

        return null;
    }
}
