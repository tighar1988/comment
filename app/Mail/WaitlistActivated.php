<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WaitlistActivated extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $shopFromEmail = $this->data['shopFromEmail'] ?? config('mail.from.address');

        return $this->subject("Waitlist item now in stock: " . ($this->data['productName'] ?? null))
            ->replyTo($this->data['shopEmail'], $this->data['shopName'])
            ->from($shopFromEmail, $this->data['shopName'])
            ->view('emails.waitlist-activated')
            ->with($this->data);
    }
}
