<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>:: CommentSold ::</title>
        <link rel="stylesheet" href="{{ cdn('/font-awesome/css/font-awesome.min.css') }}" type="text/css">

        <?php $theme = shop_setting('print-labels-theme', 'side-by-side'); ?>

        <style type="text/css">
            .preloader {
                width: 100%;
                height: 100%;
                display: block;
                padding: 0px;
                margin: 0px;
                text-align: center;
                padding-top: 250px;
                opacity: 0.5;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1040;
                background-color: #000;
                color: #fff;
            }
            table {
                width: 674px;
                border-collapse: collapse;
                margin-bottom: 5px;
            }
            table, th, td {
                border: 1px solid #ccc;
            }
            th, td {
                padding-left: 15px;
                text-align: left;
            }
            .packing-slip {
                margin-left: 15px;
            }
            .alert {
                background-color: #00c0ef;
                color: #fff;
                border-color: #00acd6;
                border-radius: 3px;
                padding: 15px;
            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: normal;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                cursor: pointer;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #fff;
                background-color: #5cb85c;
                border-color: #4cae4c;
            }
            .text-muted {
                font-size: 14px;
                font-weight: 400;
                color: #777;
            }
        </style>
        <style type="text/css" media="print">
            .no-print {
                display: none;
            }
            @if ($theme == 'side-by-side')
                @page {
                    size: landscape;
                }
                .packing-slip {
                    float: left;
                    display: inline-block;
                    width: 50%;
                    margin-right: 10px;
                    vertical-align: top;
                    margin-left: 0px;
                    font-size: 10px;
                }
                table {
                    width: 400px;
                    border-collapse: collapse;
                    margin-bottom: 5px;
                }
                th, td {
                    padding-left: 2px;
                    padding-right: 2px;
                }
                .label-item {
                    padding: 0px;
                    margin: 0px;
                    display: inline-block;
                    float: left;
                }
                .label {
                    max-height: 700px;
                }
            @endif
        </style>
        <script type="text/javascript" src="{{ cdn('/js/jquery.min.js') }}"></script>
        <script type="text/javascript">
            $(window).on("load", function() {
                $('.preloader').remove();
            });
        </script>
    </head>
    <body>
        <div class="preloader">
            <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span><br/><br/>

            <div>Loading... Please wait as this may take a few minutes.</div>
        </div>
        @if (session('labels-printed'))
            <div class="no-print alert">The labels have been marked as printed!</div>
        @endif

        @if ($orders->isEmpty())
            <h2 class="no-print">You don't have any labels for 'Processing' Orders.</h2>

        @else
            @if (! request('orders'))
                <h2 class="no-print">Total labels to be printed: {{ $orders->count() }}</h2>
                <div class="no-print">
                    <form class="" role="form" method="POST" action="/admin/orders/mark-labels-as-printed">
                        <button type="submit" onclick="return confirm('Are you sure to mark the orders as printed? You should only do this after you verified they printed successfully');" class="btn">Mark Labels as Printed</button>
                    </form>
                </div>
            @endif

            @foreach ($orders as $key => $multiOrder)
                <div style="clear:both;"></div>
                @foreach ($multiOrder as $order)
                    <?php $isMulti = count($multiOrder) > 1 ? true : false; ?>
                    <div class="packing-slip">
                        <h2>@if ($isMulti) [Combined] @endif Packing Slip: - Order #{{ $order->id }} </h2>

                        Scan this: <br/><br/><img src="data:image/png;base64,{!! $order->getBarcode() !!}"><br/><br/>

                        <h2>
                            {{ $order->ship_name }}
                            @if (shop_id() == 'shopentourageclothing')
                                <span class="text-muted">(Payment Date: {{ style_date($order->created_at) }})</span>
                            @endif
                        </h2>
                        <table>
                            <thead>
                                <tr>
                                    <th><strong>STYLE</strong></th>
                                    <th><strong>PRODUCT</strong></th>
                                    <th><strong>COLOR</strong></th>
                                    <th><strong>SIZE</strong></th>
                                    <th><strong>PRODUCT #</strong></th>
                                    <th><strong>LOCATION</strong></th>
                                    @if (shop_id() == 'cheekys' || shop_id() == 'shopentourageclothing')
                                        <th><strong>PRODUCT IMAGE</strong></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1; ?>
                                @foreach($order->orderProducts as $orderProduct)
                                    <tr>
                                        <?php
                                            $sku = $orderProduct->variant->sku ?? null;
                                            if (empty($sku)) {
                                                $sku = $orderProduct->product_style;
                                            }
                                        ?>
                                        <td><strong>{{ $index++ }}.</strong> #{{ $sku }}</td>
                                        <td>{{ $orderProduct->prod_name }}</td>
                                        <td>{{ $orderProduct->color }}</td>
                                        <td>{{ $orderProduct->size }}</td>
                                        <td>{{ $orderProduct->product_brand_style }}</td>
                                        <td>{{ $orderProduct->variant->location ?? null }}</td>
                                        @if (shop_id() == 'cheekys' || shop_id() == 'shopentourageclothing')
                                            <td style="padding: 0px;"><img src="{{ product_thumb($orderProduct->product_filename ?? null) }}" alt="" style="width: 80px; max-width: 80px; max-height: 80px;"></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <strong>TOTAL ITEMS: {{ count($order->orderProducts) }}</strong>
                    </div>
                @endforeach

                @if (! empty($order->label_url))
                    @if ($theme == 'side-by-side')
                        <a class="label-item" @if($isMulti) style="float:none;" @endif target="_blank" href="{{ $order->label_url }}">
                            <img alt="Label" class="label" src="{{ $order->label_url }}" />
                        </a>
                    @else
                        <p style="page-break-after:always;"></p>
                        <a class="label-item" @if($isMulti) style="float:none;" @endif target="_blank" href="{{ $order->label_url }}">
                            @if ($theme == 'dymo')
                                <img alt="Label" style="max-width: 100%; max-height: 100%;" class="label" src="{{ $order->label_url }}" />
                            @else
                                <img alt="Label" class="label" width="796" height="956" src="{{ $order->label_url }}" />
                            @endif
                        </a>
                    @endif
                @endif

                <p style="page-break-after:always; clear:both;"></p>
            @endforeach

        @endif
    </body>
</html>



