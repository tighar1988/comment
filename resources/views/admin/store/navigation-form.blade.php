@extends('layouts.admin')

@section('content')
<?php $isEdit = isset($menu) ? true : false; ?>
<section class="content-header">
    <h1>@if ($isEdit) Editing Menu '{{ $menu->title }}' @else Add New Menu @endif</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/navigation"><i class="fa fa-th-large"></i> Menus</a></li>
        <li class="active">@if ($isEdit) Edit Menu @else Add New Menu @endif</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form class="" role="form" method="POST" action="{{ form_action() }}">
                        @if ($isEdit) {{ method_field('PATCH') }} @endif

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('menu_title')">
                                    <label>Title</label>
                                    <input type="text" placeholder="Sidebar Menu" class="form-control" name="menu_title" value="{{ old('menu_title', $menu->title ?? '') }}">
                                    @error('menu_title')
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @hasError('menu_handle')">
                                    <label>Handle</label>
                                    <input type="text" placeholder="sidebar-menu" class="form-control" name="menu_handle" value="{{ old('menu_handle', $menu->handle ?? '') }}">
                                    @error('menu_handle')
                                </div>
                            </div>
                        </div>

                        <button type="submit" data-loading-text="Saving.." class="btn btn-success">Save</button>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->

    @if ($isEdit)
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">

                    <span class="settings-info">Menu Items</span>

                    <div class="table-responsive">
                        <table class="table menu-items-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Link</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($menu->items as $item)
                                    <tr class="sortable-tr">
                                        <td class="sort-column"><i class="fa fa-bars" aria-hidden="true"></i></td>
                                        <td>
                                            <input type="text" class="form-control item-name" value="{{ $item->name }}"/>
                                            <input type="hidden" class="item-id" value="{{ $item->id }}">
                                        </td>
                                        <td><input type="text" class="form-control item-link" value="{{ $item->link }}"/></td>
                                        <td>
                                            <button type="button" data-loading-text="Saving.." class="btn edit-item btn-success">Save</button>
                                            <button type="button" data-loading-text="Deleting.." class="btn delete-item btn-default">Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td><input type="text" class="form-control item-name" placeholder="New Menu Item" value=""/></td>
                                    <td><input type="text" class="form-control item-link" placeholder="/new-menu-item" value=""/></td>
                                    <td>
                                        <button type="button" data-loading-text="Adding.." class="btn add-item btn-success">Add</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</section>
@endsection

@section('styles')
<style type="text/css">
    .sort-column {
        width: 15px;
        cursor: move;
    }
    .sort-column:active {
        cursor: grabbing;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        $('tbody').sortable({
            axis: 'y',
            containment: "parent",
            cursor: "move",
            delay: 100,
            distance: 5,
            forceHelperSize: true,
            handle: ".sort-column",
            items: '.sortable-tr',
            helper: 'clone',
            forceHelperSize: true,
            forcePlaceholderSize: true,
            stop: function(e, ui) {
                var ids = [];
                $('.item-id').each(function() {
                    ids.push($(this).val());
                });

                $.ajax({
                    type: "POST",
                    url: '/admin/store/navigation/sort',
                    data: {
                        menu_items: ids
                    },
                    dataType: 'json',
                    success: function(data) {}
                });
            }
        });

        $('.add-item').on('click', function() {
            var $row = $(this).closest('tr');
            var name = $('.item-name', $row).val();
            var link = $('.item-link', $row).val();

            $('button', $row).button('loading');
            $.ajax({
                type: "POST",
                url: '/admin/store/navigation/menu-item/add',
                data: {
                    menu_id: '{{ $menu->id ?? null }}',
                    item_name: name,
                    item_link: link,
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        window.location.reload();
                    } else {
                        alert('Something went wrong!');
                    }
                },
                error: function(data) {
                    var message = 'An error occurred.';
                    try {
                        if (data.responseJSON.item_link[0]) {
                            message = data.responseJSON.item_link[0];
                        } else {
                            message = data.responseJSON.item_name[0];
                        }
                    } catch (e) {}
                    alert(message);
                    $('button', $row).button('reset');
                }
            });
        });

        $('.edit-item').on('click', function() {
            var $row = $(this).closest('tr');
            var name = $('.item-name', $row).val();
            var link = $('.item-link', $row).val();
            var itemId = $('.item-id', $row).val();

            $('.edit-item', $row).button('loading');
            $.ajax({
                type: "POST",
                url: '/admin/store/navigation/menu-item/update',
                data: {
                    item_id: itemId,
                    item_name: name,
                    item_link: link,
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        window.location.reload();
                    } else {
                        alert('Something went wrong!');
                    }
                },
                error: function(data) {
                    var message = 'An error occurred.';
                    try {
                        if (data.responseJSON.item_link[0]) {
                            message = data.responseJSON.item_link[0];
                        } else {
                            message = data.responseJSON.item_name[0];
                        }
                    } catch (e) {}
                    alert(message);
                    $('.edit-item', $row).button('reset');
                }
            });
        });

        $('.delete-item').on('click', function() {
            var $row = $(this).closest('tr');
            var itemId = $('.item-id', $row).val();
            $('.delete-item', $row).button('loading');

            $.ajax({
                type: "POST",
                url: '/admin/store/navigation/menu-item/delete',
                data: {
                    item_id: itemId,
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        window.location.reload();
                    } else {
                        alert('Something went wrong!');
                    }
                },
                error: function(data) {
                    alert('An error occurred.');
                    $('.delete-item', $row).button('reset');
                }
            });
        });
    });
</script>
@endsection
