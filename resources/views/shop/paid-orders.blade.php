@if (count($orders))
    @include('shop.orders-list')
@else
    You have no paid orders at the moment.
@endif
