//Open Cart Drawer After Click
// jQuery(document).ready( function() {
  jQuery(".shoppingBag a").click(function(){
    //Get Cart Details
    getCartItems();
    setTimeout(function(){
      jQuery(".CartDrawerSection").toggleClass("productDrawerOpen");
    }, 200);
//     jQuery(".CartDrawerSection").toggleClass("productDrawerOpen");
  });
  jQuery(".icon-fallback-text").click(function(){
    jQuery(".CartDrawerSection").removeClass("productDrawerOpen");
  });
  
  function setInventory(product_handle_array,product_variation_id)
  {
    var count=0;
    while(count<product_handle_array.length)
    {
      jQuery.getJSON('/products/'+product_handle_array[count]+'.js', function(product3,res) {
        var product_inner_variants=product3.variants;
        console.log(product_inner_variants);
        console.log(res);
        var count2=0;
        console.log(product_inner_variants.length);while(count2<product_inner_variants.length)
        {
          var v_count=0;
          while(v_count<product_variation_id.length)
          {
            if(product_variation_id[v_count]==product_inner_variants[count2]['id'])
            {
             console.log("found = "+product_inner_variants[count2]['inventory_quantity']);
             changeInventory(product_variation_id[v_count],product_inner_variants[count2]['inventory_quantity']);
             break;
            }
            v_count++;
          }
          count2++;
        }
        
      } );
      count++;
    }
  }
// });

function getCartItems()
{
  Shopify.getCart(function(data){
      var cartItems=data;
      console.log(cartItems);
      var item_array=data.items;
      var count=0;
      var html="";
      var product_handle_array=[];
      var product_variation_id=[];
      console.log(item_array);
      if(cartItems.item_count>0)
      {
        var cartHtml='('+cartItems.item_count+')';
        jQuery(".itemCount").html(cartHtml);
      }
      else
      {
        var cartHtml='()';
        jQuery(".itemCount").html(cartHtml);
      }
      while(count<item_array.length)
      {
        var price=item_array[count]['price']/100;
        var data_line=item_array[count]['quantity'];
        var product_handle=item_array[count]['handle'];
        product_handle_array.push(product_handle);
        product_variation_id.push(item_array[count]['id']);
        html=html+'<div class="cartProduct"><div class="gridItem productImage"><a href="#!"><img src="'+item_array[count]['image']+'" alt=""></a></div>';
        html+='<div class="gridItem productDesc"><p><a href="#!" class="productName">'+item_array[count]['product_title']+'</a><span class="productMeta">'+item_array[count]['variant_options'][0]+'</span></p>';
        html+='<div class="gridTableWrapper"><div class="gridTableItem"><div class="cartQty"><span class=""></span><div class="ajaxCartQty"><button type="button" class="ajaxCartQtyAdjust ajaxCartQtyMinus icon-fallback-text" data-qty="0" data-line="'+data_line+'" data-sku="'+item_array[count]['sku']+'" data-price="'+price+'" data-product-handel-name="'+item_array[count]['handle']+'" data-id="'+item_array[count]['variant_id']+'"><span class="icon icon-minus" aria-hidden="true"></span><span class="fallback-text">−</span></button>';
        html+='<input type="text" name="updates[]" class="ajaxCartQtyNum" value="'+item_array[count]['quantity']+'" min="0" data-id="'+item_array[count]['variant_id']+'" data-line="1" data-sku="'+item_array[count]['sku']+'" aria-label="quantity" pattern="[0-9]*">';
        html+='<button type="button" class="ajaxCartQtyAdjust ajaxCartQtyPlus" data-id="'+item_array[count]['variant_id']+'" data-line="'+data_line+'" data-qty="2" data-sku="'+item_array[count]['sku']+'" data-price="'+price+'" data-product-handel-name="'+item_array[count]['handle']+'"><span class="icon icon-plus" aria-hidden="true"></span><span class="fallback-text">+</span></button>';
        html+='</div></div></div><div class="gridTableItem text-right" data-id-price="'+item_array[count]['variant_id']+'">$'+price+'</div></div></div></div>';
        count++;
      }
      var total_price=data.total_price/100;
      jQuery(".subTotal").html("$"+total_price);
      jQuery(".subTotal").attr("data-subtotal",total_price);
      jQuery(".cartProductParent").html(html);
      console.log(product_handle_array);
      console.log(product_variation_id);
      setInventory(product_handle_array,product_variation_id);
        
    })
}

function changeInventory(product_varitation_id,inventory_quantity)
{
  console.log("changeInventory");
  console.log(jQuery(".cartProductParent").find(".cartProduct input[data-id='"+product_varitation_id+"']"));
  jQuery(".cartProductParent").find(".cartProduct button[data-id='"+product_varitation_id+"']").attr("data-sku",inventory_quantity);
}

jQuery("#purchase").click(function(){
    setTimeout(function(){
      jQuery(".shoppingBag a").trigger("click");
    }, 1000);  
});

//Open Quickview Modal
// jQuery( document ).ready( function() {
//   jQuery("span.qview a.call-to-view").click(function(){
// //     jQuery('#myModal').modal('show'); 

//   });
// });

//Increase Cart Item On Click Plus
$("body").on("click",".ajaxCartQtyPlus",function(event){
  var target=event.target;
  console.log(target);
  var variation_id=$(target).closest('button.ajaxCartQtyPlus').attr("data-id");
  var product_handle_name=$(target).closest("button.ajaxCartQtyPlus").attr("data-product-handel-name");
  var data_line=$(target).closest("button.ajaxCartQtyPlus").attr("data-line");
  var data_price=$(target).closest("button.ajaxCartQtyPlus").attr("data-price");
  var total_value=0;
  console.log(data_line);
  if(data_line<$(target).closest("button.ajaxCartQtyPlus").attr("data-sku"))
  {
    data_line=parseInt(data_line)+1;
    $(target).parents("div.cartQty").find("span:first").addClass("loader");
    jQuery.post('/cart/update.js', "updates["+variation_id+"]="+data_line);
    setTimeout(function(){ 
      getCartItems();
    }, 200);
  }
});

//Decrease Cart Item On Click Minus
$("body").on("click",".ajaxCartQtyMinus",function(event){
  var target=event.target;
  console.log(target);
  var variation_id=$(target).closest('button.ajaxCartQtyMinus').attr("data-id");
  var product_handle_name=$(target).closest("button.ajaxCartQtyMinus").attr("data-product-handel-name");
  var data_line=$(target).closest("button.ajaxCartQtyMinus").attr("data-line");
  var data_price=$(target).closest("button.ajaxCartQtyMinus").attr("data-price");
  var total_value=0;
  data_line=parseInt(data_line)-1;
  $(target).parents("div.cartQty").find("span:first").addClass("loader");
  jQuery.post('/cart/update.js', "updates["+variation_id+"]="+data_line);
  setTimeout(function(){ 
    getCartItems();
  }, 200);
});

//PRODUCT VARIATION IN SINGLE PRODUCT
jQuery(".sizeForSingleProduct").on("change",function(){
  console.log(jQuery('input[name=inlineRadioOptions]:checked').val());
  var singleProductSize=jQuery('input[name=inlineRadioOptions]:checked').val();
  var color=jQuery(".singleProductColor li.selected").attr("data-color");
  console.log(color);
  var targetColorHtml="";
  var singleProductSizeHtml="";
  var count=0;
  jQuery("select.productColorAndSizeVariant > option").each(function(index,element) {
    if(singleProductSize==jQuery(element).attr("data-size"))
    {
      console.log(element);
      if(count==0)
      {
        targetColorHtml+='<li data-color="'+jQuery(element).attr("data-color")+'" class="selected"><a href="#!"><span class="variantTwo changeSingleProductColor2" style="background-color:'+jQuery(element).attr("data-color")+'"></span></a></li>';
      }
      else
      {
        targetColorHtml+='<li data-color="'+jQuery(element).attr("data-color")+'"><a href="#!"><span class="variantTwo changeSingleProductColor2" style="background-color:'+jQuery(element).attr("data-color")+'"></span></a></li>';
      }
      count++;
    }
    
      
  });
  console.log("count = "+count);
  jQuery(".singleProductColor").html(targetColorHtml);
  jQuery("select#product-select-option-0 > option").each(function(index,element){
    if(jQuery(element).val()==singleProductSize)
    {
      singleProductSizeHtml+='<option value="'+singleProductSize+'" selected>'+singleProductSize+'</option>';
    }
    else
    {
      singleProductSizeHtml+='<option value="'+jQuery(element).val()+'">'+jQuery(element).val()+'</option>';
    }
  });
  jQuery("select#product-select-option-0").html(singleProductSizeHtml);
  jQuery("select#product-select-option-0").trigger("change");
  var targetColorHtml="";
  jQuery("select#product-select-option-1 > option").each(function(index,element){
    if(index==0)
    {
      targetColorHtml+='<option value="'+jQuery(element).val()+'" selected>'+jQuery(element).val()+'</option>';
    }
    else
    {
      targetColorHtml+='<option value="'+jQuery(element).val()+'">'+jQuery(element).val()+'</option>';
    }
  });
  jQuery("select#product-select-option-1").html(targetColorHtml);
  jQuery("select#product-select-option-1").trigger("change");
});

//COLOR VARIATION IN SINGLE PRODUCT
jQuery("body").on("click",".changeSingleProductColor2",function(){
  jQuery("ul.singleProductColor li.selected").removeClass("selected");
  jQuery(this).closest("li").addClass("selected");
  console.log(jQuery(this).closest("li").attr("data-color"));
  var color=jQuery(this).closest("li").attr("data-color");
  var targetColorHtml="";
  var singleProductColorHtml="";
  jQuery("select#product-select-option-1 > option").each(function(index,element){
    if(jQuery(element).val()==color)
    {
      singleProductColorHtml+='<option value="'+color+'" selected>'+color+'</option>';
      console.log(element);
      var productDataSize=jQuery('.sizeForSingleProduct').find(".sizeSelected input").val();
      var productDataColor=jQuery(".singleProductColor li.selected").attr("data-color");
      var productMax=0;
      jQuery(".productColorAndSizeVariant > option").each(function(){
        if(jQuery(this).attr("data-size")==productDataSize && jQuery(this).attr("data-color")==productDataColor)
        {
          productMax=jQuery(this).attr("inventory_quantity");
        }
        jQuery("#quantity").attr("max",productMax);
      jQuery("#quantity").attr("min",1);
        jQuery("#quantity").val(1);
    });
    }
    else
    {
      singleProductColorHtml+='<option value="'+jQuery(element).val()+'">'+jQuery(element).val()+'</option>';
    }
  });
  jQuery("select#product-select-option-1").html(singleProductColorHtml);
  jQuery("select#product-select-option-0").trigger("change");
});

//CART DRAWER CLOSING AT ANY OUTSIDE MODAL BODY
$(document).on("click", "body", function(event) {
  if($(event.target).closest('.CartDrawerSection').length==0)
  {
    jQuery(".CartDrawerSection").removeClass("productDrawerOpen");
  }
});

// NAVIGATION

  // $( "ul.navbar-nav li:nth-child(2)" ).addClass ("dressesHoverState");
  // $( "ul.navbar-nav li:nth-child(3)" ).addClass ("topHoverState");
  // $( "ul.navbar-nav li:nth-child(4)" ).addClass ("bottomHoverState");
  // $( "ul.navbar-nav li:nth-child(5)" ).addClass ("curvyHoverState");
  // $( "ul.navbar-nav li:nth-child(6)" ).addClass ("basicHoverState");
  // $( "ul.navbar-nav li:nth-child(7)" ).addClass ("footwearHoverState");
  // $( "ul.navbar-nav li:nth-child(8)" ).addClass ("accHoverState");

  // $( "ul.dropdown-menu-item li:nth-child(2)" ).removeClass ("dressesHoverState");
  // $( "ul.dropdown-menu-item li:nth-child(3)" ).removeClass ("topHoverState");
  // $( "ul.dropdown-menu-item li:nth-child(4)" ).removeClass ("bottomHoverState");
  // $( "ul.dropdown-menu-item li:nth-child(5)" ).removeClass ("curvyHoverState");
  // $( "ul.dropdown-menu-item li:nth-child(6)" ).removeClass ("basicHoverState");
  // $( "ul.dropdown-menu-item li:nth-child(7)" ).removeClass ("footwearHoverState");
  // $( "ul.dropdown-menu-item li:nth-child(8)" ).removeClass ("accHoverState");



// FILTER

  jQuery(".filter-group-size").click(function(){
      jQuery(this).find(".scroll-content").toggleClass("scroll-content-open");
    });
   jQuery(".filter-group-color").click(function(){
      jQuery(this).find(".scroll-content").toggleClass("scroll-content-open");
    });
    jQuery(".filter-group-fit").click(function(){
      jQuery(this).find(".scroll-content").toggleClass("scroll-content-open");
    });
    
    

//CHANGE IMAGE ON HOVER
// jQuery(".variantColor").hover(function(){
// //   console.log(jQuery(this).parents("li").find("div.productDetails img").attr("src"));
//   var variantColor=jQuery(this).parents("li").find("div.productDetails img").attr("src");
//   variantColor=variantColor.split('?');
//   console.log(variantColor);
//   var variantColor1=variantColor[0].split("/");
//   console.log(variantColor1);
//   var featureImage=jQuery(this).parent("li").attr("data-feauture-image");
//   featureImage=featureImage.split("/");
//   variantColor1[variantColor1.length-1]=featureImage[1];
//   console.log(variantColor1);
//   variantColor1=variantColor1.join("/");
//   console.log(variantColor1);
//   var variantColor_version=variantColor[1];
//   var source=variantColor1+'?'+variantColor[1];
//   jQuery(this).parents("li").find("div.productDetails img").attr("src",source);
//   console.log(source);
// });

//CHANGE ACCORDIAN
jQuery("#accord1 li").click(function(){
  
  if(jQuery(this).find(".accordHeader").hasClass("active"))
  {
    jQuery(this).find(".accordHeader").removeClass("active");
      jQuery(".accordHeader").each(function(){
      jQuery(this).removeClass("active");
    });
  }
  else
  {
    jQuery(".accordHeader").each(function(){
      jQuery(this).removeClass("active");
    });
    jQuery(this).find(".accordHeader").addClass("active");
  }
});


jQuery(".proDrpdwnSelected a.sidenav__collection").click(function(){
  jQuery(".proDrpdwnSelected ul").toggleClass("dropdown-menu-item-open");
});

jQuery(".filterCategory h3").click(function(){
  jQuery(".filterCategory .filter-menu").toggleClass("filter-menu-open");
});

//MENU SIDEBAR

jQuery(".proDrpdwn").each(function(index,element){
  if(jQuery(element).find("a").hasClass("selected") == true)
  {
    console.log(element);
    jQuery(this).addClass("proDrpdwnSelected");
  }
});

jQuery(".proDrpdwn").click(function(event){
  if(jQuery(window).width() <= 767)
  {
    event.preventDefault();
    jQuery(".proDrpdwn").each(function(index,element){
      if(jQuery(element).find("a").hasClass("selected") == true)
      {
        jQuery(this).find("ul").toggleClass("selectedItems");
      }
    });
  }
});

//IMAGE CHANGE ON HOVER
// jQuery("div.productDetails a").hover(
//   function(){
//     var src=jQuery(this).parents(".productDetails").find(".productSecondImage").attr("src");
//     if(src!="" || src!="undefined")
//     {
//       jQuery(this).find("img:first").css('opacity', '0');
//       jQuery(this).parents(".productDetails").find(".productSecondImage").css('opacity', '1');
//     }
//   },
//   function(){
//     var prevSrc=jQuery(this).parents(".productDetails").find(".productTempImage").attr("src");
//     if(prevSrc!="" || prevSrc!="undefined")
//     {
//       jQuery(this).find("img:first").css('opacity', '1');
//       jQuery(this).parents(".productDetails").find(".productSecondImage").css('opacity', '0');
//     }
//   }
// );

//SET PRODUCT MAX IN SINGLE PRODUCT
jQuery(".sizeForSingleProduct").on("change",function(){
  console.log(jQuery('input[name=inlineRadioOptions]:checked').val());
  console.log(jQuery(".singleProductColor li.selected").attr("data-color"));
  var productDataSize=jQuery('.sizeForSingleProduct').find(".sizeSelected input").val();
  var productDataColor=jQuery(".singleProductColor li.selected").attr("data-color");
  var productMax=0;
  jQuery(".productColorAndSizeVariant > option").each(function(){
    if(productDataColor!=null)
    {
      if(jQuery(this).attr("data-size")==productDataSize && jQuery(this).attr("data-color")==productDataColor)
      {
        productMax=jQuery(this).attr("inventory_quantity");
        console.log(jQuery(this));
      }
    }
    else
    {
      if(jQuery(this).attr("data-size")==productDataSize)
      {
        productMax=jQuery(this).attr("inventory_quantity");
      }
    }
  });
  console.log(productMax);
  jQuery("#quantity").attr("max",productMax);
  jQuery("#quantity").attr("min",1);
  jQuery("#quantity").val(1);
  jQuery("#Quantity").attr("min",1);
  jQuery("#Quantity").attr("max",productMax);
  if(productMax>0)
  {
    jQuery(".quantity-wrap").show();
  }
});

jQuery("a.notifyMe").click(function(){
  jQuery("form.contact-form").slideToggle();
});

//CHANGE SELECTED COLOR FOR SINGLE PRODUCT VARIATION DROPDOWN
jQuery(".sizeForSingleProduct input").click(function(){
  jQuery(".sizeForSingleProduct input").each(function(index,element){
    jQuery(element).closest("label").removeClass("sizeSelected");
  });
  jQuery(this).closest("label").addClass("sizeSelected");
});

//NEW ON 21-10-2016
jQuery(".sizeForSingleProduct").find("label.sizeSelected input").trigger("click");

jQuery(".searchButton").click(function(){
  jQuery("form.navbar-form").slideToggle();
});

//INCREASE PRODUCT COUNT
  var productValue=0;
  var productMax=0;
  jQuery(".qtyAdjustPlus").click(function(event){
    productValue=jQuery(event.target).closest("button.qtyAdjustPlus").prev("input").val();
    productMax=jQuery(event.target).closest("button.qtyAdjustPlus").prev("input").attr("max");
    console.log(productValue);
    if(productValue<productMax)
    {
      productValue=parseInt(productValue)+1;
    }
    jQuery(event.target).closest("button.qtyAdjustPlus").prev("input").val(productValue);
  });
  jQuery(".qtyAdjustMinus").click(function(event){
    productValue=jQuery(event.target).closest("button.qtyAdjustMinus").next("input").val();
    productMax=jQuery(event.target).closest("button.qtyAdjustMinus").next("input").attr("max");
    console.log(productValue);
    if(productValue>1)
    {
      productValue=parseInt(productValue)-1;
    }
    jQuery(event.target).closest("button.qtyAdjustMinus").next("input").val(productValue);
  });

//VIEW BY
jQuery(".shorting .view_by").on("change",function(){
  console.log(jQuery(this).val());
  CartJS.setAttribute('pagination', jQuery(this).val()  );
  setTimeout(function(){ 
    location.reload();
  }, 1000);
  });
  Shopify.getCart(function(data){
    var cartItems=data;
    console.log(cartItems);
    console.log(cartItems.attributes.pagination);
    jQuery(".view_by option[value='"+cartItems.attributes.pagination+"']").attr("selected",true);
  })

  
  
  
  
  jQuery(".mobilenavWrapper li.topHoverState").click(function(){
    jQuery("li.topHoverState").toggleClass("menuOpen");
  });

  jQuery(".mobilenavWrapper li.bottomHoverState").click(function(){
    jQuery("li.bottomHoverState").toggleClass("menuOpen");
  });

  jQuery(".mobilenavWrapper li.dressesHoverState").click(function(){
    jQuery("li.dressesHoverState").toggleClass("menuOpen");
  });

  jQuery(".mobilenavWrapper li.curvyHoverState").click(function(){
    jQuery("li.curvyHoverState").toggleClass("menuOpen");
  });

  jQuery(".mobilenavWrapper li.basicHoverState").click(function(){
    jQuery("li.basicHoverState").toggleClass("menuOpen");
  });

  jQuery(".mobilenavWrapper li.footwearHoverState").click(function(){
    jQuery("li.footwearHoverState").toggleClass("menuOpen");
  });

  jQuery(".mobilenavWrapper li.accHoverState").click(function(){
    jQuery("li.accHoverState").toggleClass("menuOpen");
  });

  jQuery(".navdrawerButton").click(function(){
    jQuery(".mobilenavWrapper").addClass("navDrawerOpen");
  });

  jQuery(".navDrawerClose").click(function(){
    jQuery(".mobilenavWrapper").removeClass("navDrawerOpen");
  });

