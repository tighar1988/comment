<?php

namespace App\Jobs\Traits;

use App\Models\Master;
use DB;

trait ReconnectShopDB
{
    /**
     * Reconnect to the shop's database.
     */
    public function reconnectDB($shop)
    {
        DB::setDefaultConnection('shop');
        set_shop_database($shop);
        DB::reconnect();

        // if the shop has a custom domain we will use it as the base url
        $domain = Master::findDomainByShop($shop);
        if (! empty($domain)) {
            config(['shop.custom-domain' => $domain]);
        } else {
            config(['shop.custom-domain' => null]);
        }
    }
}
