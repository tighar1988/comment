<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\ShopSettings;

class ShopSettingsTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(ShopSettings::class, new ShopSettings);
    }
}
