<?php

namespace Tests\Unit\Mails;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Mails\MailThrottler;
use Illuminate\Contracts\Cache\Store;
use Mockery;
use Exception;

class MailThrottlerTest extends TestCase
{
    /** @test */
    public function it_runs()
    {
        $this->assertInstanceOf(MailThrottler::class, new MailThrottler(null));
    }

    /** @test */
    public function it_allows_strings_to_pass()
    {
        $throttler = new MailThrottler('Text message');
        $this->assertTrue($throttler->allowed());
    }

    /** @test */
    public function it_does_not_allow_first_exception_with_limit_1()
    {
        $throttler = new MailThrottler;
        $throttler->setExceptions(['Exception' => '1:10']);
        $allowed = $throttler->allowed(new Exception('Test exception.'));
        $this->assertFalse($allowed);
    }

    /** @test */
    public function it_allows_second_exception_with_limit_1()
    {
        $throttler = new MailThrottler;
        $throttler->setExceptions(['Exception' => '1:10']);

        $allowed1 = $throttler->allowed(new Exception('Test exception 1.'));
        $allowed2 = $throttler->allowed(new Exception('Test exception 2.'));

        $this->assertFalse($allowed1);
        $this->assertTrue($allowed2);
    }

    /** @test */
    public function it_blocks_exception_1()
    {
        $throttler = new MailThrottler;

        $error = ['error' => ['message' => 'Error validating access token: The session has been invalidated because the user changed their password or Facebook has changed the session for security reasons.']];

        $request = new \Facebook\FacebookRequest;
        $response = new \Facebook\FacebookResponse($request, json_encode($error));
        $this->assertTrue($throttler->isBlocked(new \Facebook\Exceptions\FacebookResponseException($response)));
    }

    /** @test */
    public function it_blocks_exception_2()
    {
        $throttler = new MailThrottler;

        $error = ['error' => ['message' => 'Sorry, you are blocked from leaving comments due to continued overuse of this feature.']];

        $request = new \Facebook\FacebookRequest;
        $response = new \Facebook\FacebookResponse($request, json_encode($error));
        $this->assertTrue($throttler->isBlocked(new \Facebook\Exceptions\FacebookResponseException($response)));
    }

    /** @test */
    public function it_blocks_exception_3()
    {
        $throttler = new MailThrottler;

        $error = ['error' => ['message' => "Unsupported get request. Object with ID '594781224244256' does not exist, cannot be loaded due to missing permissions, or does not support this operation. Please read the Graph API documentation at https://developers.facebook.com/docs/graph-api"]];

        $request = new \Facebook\FacebookRequest;
        $response = new \Facebook\FacebookResponse($request, json_encode($error));
        $this->assertTrue($throttler->isBlocked(new \Facebook\Exceptions\FacebookResponseException($response)));
    }

    /** @test */
    public function it_blocks_exception_4()
    {
        $throttler = new MailThrottler;

        $error = ['error' => ['message' => '(#230) Requires pages_messaging_subscriptions permission to manage the object']];

        $request = new \Facebook\FacebookRequest;
        $response = new \Facebook\FacebookResponse($request, json_encode($error));
        $this->assertTrue($throttler->isBlocked(new \Facebook\Exceptions\FacebookResponseException($response)));
    }

    /** @test */
    public function it_blocks_exception_5()
    {
        $throttler = new MailThrottler;

        $this->assertTrue($throttler->isBlocked(new \InvalidArgumentException('The default access token must be of type "string" or Facebook\AccessToken')));
    }
}
