@extends('layouts.admin')

@section('content')
<section class="content-header">
    <h1>Store</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/store/pages"><i class="fa fa-th-large"></i>Store</a></li>
        <li class="active">@yield('section')</li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-pills">
                    <li class="{{ activeLink('admin/store/pages') }}"><a href="/admin/store/pages">Pages</a></li>
                    <li class="{{ activeLink('admin/store/collections') }}"><a href="/admin/store/collections">Collections</a></li>
                    <li class="{{ activeLink('admin/store/tags') }}"><a href="/admin/store/tags">Tags</a></li>
                    @if (shop_id() == 'shop1' || shop_id() == 'cheekys' || shop_id() == 'glamourfarms')
                        <li class="{{ activeLink(['admin/store/themes/layout', 'admin/store/themes/templates', 'admin/store/themes/sections', 'admin/store/themes/snippets', 'admin/store/themes/assets', 'admin/store/themes/config', 'admin/store/themes/locales']) }}"><a href="/admin/store/themes/assets">Themes</a></li>
                        <li class="{{ activeLink('admin/store/preferences') }}"><a href="/admin/store/preferences">Preferences</a></li>
                        <li class="{{ activeLink('admin/store/navigation') }}"><a href="/admin/store/navigation">Navigation</a></li>
                        {{-- <li class="{{ activeLink('admin/store/blog-posts') }}"><a href="/admin/store/blog-posts">Blog Posts</a></li> --}}
                    @endif
                </ul>
                @yield('sub-nav')
            </div>
        </div>
    </div>

    @yield('store')

</section>
@endsection
