@extends('app-master')

@section('content')
<h2>Edit Post</h2>

{{-- todo: edit posts only for scheduled posts (posts that are posted already should not be editable) --}}
<form method="post" enctype="multipart/form-data" style="width: 500px;">
    @if ($err) <div class="error">{{ $err }}</div> @endif
    @if ($success) <div class="green">{{ $success }}</div> @endif

    @if ($input && $input['post']->scheduled_time > 0)
        Select the group to post your product. You have to own the group to post.<br/><br/>

        <select name="group">
            <option value="">Select</option>
            @foreach ($input['data'] as $x)
                <option value="{{$x['id']}}" @if ($input['post']->supplement->group == $x['id']):?> selected @endif>{{$x['name']}}</option>
            @endforeach
        </select>

        <textarea style="height: 300px;" name="message">{{$input['post']->supplement->message}}</textarea>

        <img src="catalog/thumbs/{{$input['post']->image}}" />
        <br/><br/>

        <div id="later-date" style="display: block;">
            <i>Scheduled date</i><br/><br/>
            <select name="schedule_date">
                @for ($i=0; $i<=30; $i++)
                    <?php $stamp = time() + (60*60*24*$i); ?>
                    <option value="{{ date('n/j/Y', $stamp) }}" @if (date('n/j/Y',$stamp) == date('n/j/Y',$input['post']->scheduled_time)) selected @endif>{{ date('F jS', $stamp) }}</option>
                @endfor
            </select>
            <br/><br/>

            <i>Scheduled time</i>
            <select name="hour">
                @for ($i=0; $i<=12; $i++)
                    <option value="{{$i}}" @if ($i==date('g',$input['post']->scheduled_time)) selected @endif>{{ $i }}</option>
                @endfor
            </select>

            <?php
                $selmin = date('i', $input['post']->scheduled_time);
                if ($selmin[0] == '0') {
                    $selmin = substr($selmin,1);
                }

                $sela = date('A',$input['post']->scheduled_time);
            ?>

            <select name="min">
                @for ($i=0; $i<=11; $i++)
                    <?php
                        $min = $i*5;
                        if (strlen($min) == 1) {
                            $min = '0'.$min;
                        }
                    ?>
                    <option value="{{ $min }}" @if ($min==$selmin) selected @endif>{{ $min }}</option>
                @endfor
            </select>

            <select name="ampm">
                <option @if ($sela == 'AM') selected @endif>AM</option>
                <option @if ($sela == 'PM') selected @endif>PM</option>
            </select>
            <br/><br/>
        </div>

        <button class="small orange" name="var" value="1" >Save</button>
    @else
        You can not edit this post.
    @endif
</form>
@endsection
