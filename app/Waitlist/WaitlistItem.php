<?php

namespace App\Waitlist;

class WaitlistItem
{
    public $id;
    public $name;
    public $price;
    public $image;
    public $color;
    public $size;
    public $created_at;

    public function __construct($item)
    {
        $this->id         = $item->id ?? null;
        $this->name       = $item->variant->product->product_name ?? null;
        $this->price      = $item->variant->price ?? null;
        $this->image      = $item->variant->product->image->filename ?? null;
        $this->color      = $item->variant->color ?? null;
        $this->size       = $item->variant->size ?? null;
        $this->created_at = strtotime($item->created_at ?? null);
    }
}
